
/**
 * NetSuiteServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package com.netsuite;
    /**
     *  NetSuiteServiceSkeleton java skeleton for the axisService
     */
    public class NetSuiteServiceSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param initialize 
             * @return initializeResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE initialize
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Initialize initialize
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#initialize");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param ssoLogin 
             * @return ssoLoginResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws InvalidAccountFault 
             * @throws InvalidVersionFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE ssoLogin
                  (
                  com.netsuite.webservices.platform.messages_2017_2.SsoLogin ssoLogin
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,InvalidAccountFault,InvalidVersionFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#ssoLogin");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getItemAvailability 
             * @return getItemAvailabilityResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE getItemAvailability
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability getItemAvailability
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getItemAvailability");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param checkAsyncStatus 
             * @return checkAsyncStatusResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws AsyncFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse checkAsyncStatus
                  (
                  com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus checkAsyncStatus
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,AsyncFault,ExceededConcurrentRequestLimitFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#checkAsyncStatus");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param searchMore 
             * @return searchMoreResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE searchMore
                  (
                  com.netsuite.webservices.platform.messages_2017_2.SearchMore searchMore
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#searchMore");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getSelectValue 
             * @return getSelectValueResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE getSelectValue
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetSelectValue getSelectValue
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getSelectValue");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param detach 
             * @return detachResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.DetachResponseE detach
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Detach detach
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#detach");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncAddList 
             * @return asyncAddListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse asyncAddList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncAddList asyncAddList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncAddList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param changeEmail 
             * @return changeEmailResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws InvalidAccountFault 
             * @throws InvalidVersionFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE changeEmail
                  (
                  com.netsuite.webservices.platform.messages_2017_2.ChangeEmail changeEmail
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,InvalidAccountFault,InvalidVersionFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#changeEmail");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param updateInviteeStatusList 
             * @return updateInviteeStatusListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE updateInviteeStatusList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList updateInviteeStatusList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#updateInviteeStatusList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncDeleteList 
             * @return asyncDeleteListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse asyncDeleteList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList asyncDeleteList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncDeleteList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getCustomizationId 
             * @return getCustomizationIdResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE getCustomizationId
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId getCustomizationId
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getCustomizationId");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getPostingTransactionSummary 
             * @return getPostingTransactionSummaryResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE getPostingTransactionSummary
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary getPostingTransactionSummary
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getPostingTransactionSummary");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param upsert 
             * @return upsertResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE upsert
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Upsert upsert
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#upsert");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param changePassword 
             * @return changePasswordResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws InvalidAccountFault 
             * @throws InvalidVersionFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE changePassword
                  (
                  com.netsuite.webservices.platform.messages_2017_2.ChangePassword changePassword
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,InvalidAccountFault,InvalidVersionFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#changePassword");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getAll 
             * @return getAllResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE getAll
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetAll getAll
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getAll");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncSearch 
             * @return asyncSearchResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse asyncSearch
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncSearch asyncSearch
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncSearch");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param add 
             * @return addResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AddResponseE add
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Add add
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#add");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param upsertList 
             * @return upsertListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE upsertList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.UpsertList upsertList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#upsertList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncInitializeList 
             * @return asyncInitializeListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse asyncInitializeList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList asyncInitializeList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncInitializeList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getCurrencyRate 
             * @return getCurrencyRateResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE getCurrencyRate
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate getCurrencyRate
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getCurrencyRate");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param attach 
             * @return attachResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AttachResponseE attach
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Attach attach
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#attach");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param searchMoreWithId 
             * @return searchMoreWithIdResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE searchMoreWithId
                  (
                  com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId searchMoreWithId
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#searchMoreWithId");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param addList 
             * @return addListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AddListResponseE addList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AddList addList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#addList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param mapSso 
             * @return mapSsoResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws InvalidAccountFault 
             * @throws InvalidVersionFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE mapSso
                  (
                  com.netsuite.webservices.platform.messages_2017_2.MapSso mapSso
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,InvalidAccountFault,InvalidVersionFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#mapSso");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param searchNext 
             * @return searchNextResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE searchNext
                  (
                  com.netsuite.webservices.platform.messages_2017_2.SearchNext searchNext
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#searchNext");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param updateList 
             * @return updateListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE updateList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.UpdateList updateList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#updateList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param updateInviteeStatus 
             * @return updateInviteeStatusResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE updateInviteeStatus
                  (
                  com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus updateInviteeStatus
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#updateInviteeStatus");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param logout 
             * @return logoutResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE logout
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Logout logout
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#logout");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param search 
             * @return searchResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.SearchResponseE search
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Search search
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#search");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getAsyncResult 
             * @return getAsyncResultResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws AsyncFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE getAsyncResult
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult getAsyncResult
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,AsyncFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getAsyncResult");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param initializeList 
             * @return initializeListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE initializeList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.InitializeList initializeList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#initializeList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncUpsertList 
             * @return asyncUpsertListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse asyncUpsertList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList asyncUpsertList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncUpsertList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param get 
             * @return getResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetResponseE get
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Get get
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#get");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getList 
             * @return getListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetListResponseE getList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetList getList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getDeleted 
             * @return getDeletedResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE getDeleted
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetDeleted getDeleted
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getDeleted");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param update 
             * @return updateResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE update
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Update update
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#update");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getSavedSearch 
             * @return getSavedSearchResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE getSavedSearch
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch getSavedSearch
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getSavedSearch");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param delete 
             * @return deleteResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE delete
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Delete delete
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#delete");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getServerTime 
             * @return getServerTimeResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE getServerTime
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetServerTime getServerTime
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getServerTime");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param login 
             * @return loginResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws InvalidAccountFault 
             * @throws InvalidVersionFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.LoginResponseE login
                  (
                  com.netsuite.webservices.platform.messages_2017_2.Login login
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,InvalidAccountFault,InvalidVersionFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#login");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getDataCenterUrls 
             * @return getDataCenterUrlsResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededRequestSizeFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE getDataCenterUrls
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls getDataCenterUrls
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededRequestSizeFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getDataCenterUrls");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncGetList 
             * @return asyncGetListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse asyncGetList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncGetList asyncGetList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncGetList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param deleteList 
             * @return deleteListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE deleteList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.DeleteList deleteList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#deleteList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param asyncUpdateList 
             * @return asyncUpdateListResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse asyncUpdateList
                  (
                  com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList asyncUpdateList
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#asyncUpdateList");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param getBudgetExchangeRate 
             * @return getBudgetExchangeRateResponse 
             * @throws InsufficientPermissionFault 
             * @throws InvalidCredentialsFault 
             * @throws ExceededConcurrentRequestLimitFault 
             * @throws ExceededRecordCountFault 
             * @throws ExceededRequestSizeFault 
             * @throws ExceededRequestLimitFault 
             * @throws ExceededUsageLimitFault 
             * @throws UnexpectedErrorFault 
             * @throws InvalidSessionFault 
         */
        
                 public com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE getBudgetExchangeRate
                  (
                  com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate getBudgetExchangeRate
                  )
            throws InsufficientPermissionFault,InvalidCredentialsFault,ExceededConcurrentRequestLimitFault,ExceededRecordCountFault,ExceededRequestSizeFault,ExceededRequestLimitFault,ExceededUsageLimitFault,UnexpectedErrorFault,InvalidSessionFault{
                //TODO : fill this with the necessary business logic
                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#getBudgetExchangeRate");
        }
     
    }
    