
/**
 * WorkOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.inventory_2017_2;
            

            /**
            *  WorkOrder bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class WorkOrder extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = WorkOrder
                Namespace URI = urn:inventory_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns30
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingRouting
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localManufacturingRouting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingRoutingTracker = false ;

                           public boolean isManufacturingRoutingSpecified(){
                               return localManufacturingRoutingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getManufacturingRouting(){
                               return localManufacturingRouting;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingRouting
                               */
                               public void setManufacturingRouting(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localManufacturingRoutingTracker = param != null;
                                   
                                            this.localManufacturingRouting=param;
                                    

                               }
                            

                        /**
                        * field for AutoCalculateLag
                        */

                        
                                    protected boolean localAutoCalculateLag ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAutoCalculateLagTracker = false ;

                           public boolean isAutoCalculateLagSpecified(){
                               return localAutoCalculateLagTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAutoCalculateLag(){
                               return localAutoCalculateLag;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AutoCalculateLag
                               */
                               public void setAutoCalculateLag(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAutoCalculateLagTracker =
                                       true;
                                   
                                            this.localAutoCalculateLag=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected java.lang.String localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(java.lang.String param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for TranId
                        */

                        
                                    protected java.lang.String localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTranId(){
                               return localTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranId
                               */
                               public void setTranId(java.lang.String param){
                            localTranIdTracker = param != null;
                                   
                                            this.localTranId=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for AssemblyItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAssemblyItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssemblyItemTracker = false ;

                           public boolean isAssemblyItemSpecified(){
                               return localAssemblyItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAssemblyItem(){
                               return localAssemblyItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AssemblyItem
                               */
                               public void setAssemblyItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAssemblyItemTracker = param != null;
                                   
                                            this.localAssemblyItem=param;
                                    

                               }
                            

                        /**
                        * field for ExpandAssembly
                        */

                        
                                    protected boolean localExpandAssembly ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpandAssemblyTracker = false ;

                           public boolean isExpandAssemblySpecified(){
                               return localExpandAssemblyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExpandAssembly(){
                               return localExpandAssembly;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpandAssembly
                               */
                               public void setExpandAssembly(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExpandAssemblyTracker =
                                       true;
                                   
                                            this.localExpandAssembly=param;
                                    

                               }
                            

                        /**
                        * field for IsWip
                        */

                        
                                    protected boolean localIsWip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsWipTracker = false ;

                           public boolean isIsWipSpecified(){
                               return localIsWipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsWip(){
                               return localIsWip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsWip
                               */
                               public void setIsWip(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsWipTracker =
                                       true;
                                   
                                            this.localIsWip=param;
                                    

                               }
                            

                        /**
                        * field for Quantity
                        */

                        
                                    protected double localQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityTracker = false ;

                           public boolean isQuantitySpecified(){
                               return localQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantity(){
                               return localQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Quantity
                               */
                               public void setQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantity=param;
                                    

                               }
                            

                        /**
                        * field for BillOfMaterials
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillOfMaterials ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillOfMaterialsTracker = false ;

                           public boolean isBillOfMaterialsSpecified(){
                               return localBillOfMaterialsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillOfMaterials(){
                               return localBillOfMaterials;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillOfMaterials
                               */
                               public void setBillOfMaterials(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillOfMaterialsTracker = param != null;
                                   
                                            this.localBillOfMaterials=param;
                                    

                               }
                            

                        /**
                        * field for Units
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTracker = false ;

                           public boolean isUnitsSpecified(){
                               return localUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnits(){
                               return localUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Units
                               */
                               public void setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitsTracker = param != null;
                                   
                                            this.localUnits=param;
                                    

                               }
                            

                        /**
                        * field for TranDate
                        */

                        
                                    protected java.util.Calendar localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTranDate(){
                               return localTranDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranDate
                               */
                               public void setTranDate(java.util.Calendar param){
                            localTranDateTracker = param != null;
                                   
                                            this.localTranDate=param;
                                    

                               }
                            

                        /**
                        * field for OrderStatus
                        */

                        
                                    protected com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderOrderStatus localOrderStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderStatusTracker = false ;

                           public boolean isOrderStatusSpecified(){
                               return localOrderStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderOrderStatus
                           */
                           public  com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderOrderStatus getOrderStatus(){
                               return localOrderStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderStatus
                               */
                               public void setOrderStatus(com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderOrderStatus param){
                            localOrderStatusTracker = param != null;
                                   
                                            this.localOrderStatus=param;
                                    

                               }
                            

                        /**
                        * field for Firmed
                        */

                        
                                    protected boolean localFirmed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirmedTracker = false ;

                           public boolean isFirmedSpecified(){
                               return localFirmedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getFirmed(){
                               return localFirmed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Firmed
                               */
                               public void setFirmed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localFirmedTracker =
                                       true;
                                   
                                            this.localFirmed=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for ItemList
                        */

                        
                                    protected com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItemList localItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemListTracker = false ;

                           public boolean isItemListSpecified(){
                               return localItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItemList
                           */
                           public  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItemList getItemList(){
                               return localItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemList
                               */
                               public void setItemList(com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItemList param){
                            localItemListTracker = param != null;
                                   
                                            this.localItemList=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for SchedulingMethod
                        */

                        
                                    protected com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderSchedulingMethod localSchedulingMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSchedulingMethodTracker = false ;

                           public boolean isSchedulingMethodSpecified(){
                               return localSchedulingMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderSchedulingMethod
                           */
                           public  com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderSchedulingMethod getSchedulingMethod(){
                               return localSchedulingMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SchedulingMethod
                               */
                               public void setSchedulingMethod(com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderSchedulingMethod param){
                            localSchedulingMethodTracker = param != null;
                                   
                                            this.localSchedulingMethod=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamList
                        */

                        
                                    protected com.netsuite.webservices.transactions.inventory_2017_2.SalesTeamList localSalesTeamList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamListTracker = false ;

                           public boolean isSalesTeamListSpecified(){
                               return localSalesTeamListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.inventory_2017_2.SalesTeamList
                           */
                           public  com.netsuite.webservices.transactions.inventory_2017_2.SalesTeamList getSalesTeamList(){
                               return localSalesTeamList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamList
                               */
                               public void setSalesTeamList(com.netsuite.webservices.transactions.inventory_2017_2.SalesTeamList param){
                            localSalesTeamListTracker = param != null;
                                   
                                            this.localSalesTeamList=param;
                                    

                               }
                            

                        /**
                        * field for PartnersList
                        */

                        
                                    protected com.netsuite.webservices.transactions.inventory_2017_2.PartnersList localPartnersList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnersListTracker = false ;

                           public boolean isPartnersListSpecified(){
                               return localPartnersListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.inventory_2017_2.PartnersList
                           */
                           public  com.netsuite.webservices.transactions.inventory_2017_2.PartnersList getPartnersList(){
                               return localPartnersList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnersList
                               */
                               public void setPartnersList(com.netsuite.webservices.transactions.inventory_2017_2.PartnersList param){
                            localPartnersListTracker = param != null;
                                   
                                            this.localPartnersList=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromTracker = false ;

                           public boolean isCreatedFromSpecified(){
                               return localCreatedFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedFrom(){
                               return localCreatedFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFrom
                               */
                               public void setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedFromTracker = param != null;
                                   
                                            this.localCreatedFrom=param;
                                    

                               }
                            

                        /**
                        * field for SourceTransactionId
                        */

                        
                                    protected java.lang.String localSourceTransactionId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceTransactionIdTracker = false ;

                           public boolean isSourceTransactionIdSpecified(){
                               return localSourceTransactionIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSourceTransactionId(){
                               return localSourceTransactionId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceTransactionId
                               */
                               public void setSourceTransactionId(java.lang.String param){
                            localSourceTransactionIdTracker = param != null;
                                   
                                            this.localSourceTransactionId=param;
                                    

                               }
                            

                        /**
                        * field for SourceTransactionLine
                        */

                        
                                    protected long localSourceTransactionLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceTransactionLineTracker = false ;

                           public boolean isSourceTransactionLineSpecified(){
                               return localSourceTransactionLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getSourceTransactionLine(){
                               return localSourceTransactionLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceTransactionLine
                               */
                               public void setSourceTransactionLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localSourceTransactionLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localSourceTransactionLine=param;
                                    

                               }
                            

                        /**
                        * field for SpecialOrder
                        */

                        
                                    protected boolean localSpecialOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSpecialOrderTracker = false ;

                           public boolean isSpecialOrderSpecified(){
                               return localSpecialOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSpecialOrder(){
                               return localSpecialOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SpecialOrder
                               */
                               public void setSpecialOrder(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSpecialOrderTracker =
                                       true;
                                   
                                            this.localSpecialOrder=param;
                                    

                               }
                            

                        /**
                        * field for Buildable
                        */

                        
                                    protected double localBuildable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildableTracker = false ;

                           public boolean isBuildableSpecified(){
                               return localBuildableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getBuildable(){
                               return localBuildable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Buildable
                               */
                               public void setBuildable(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localBuildableTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localBuildable=param;
                                    

                               }
                            

                        /**
                        * field for Options
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localOptions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOptionsTracker = false ;

                           public boolean isOptionsSpecified(){
                               return localOptionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getOptions(){
                               return localOptions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Options
                               */
                               public void setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localOptionsTracker = param != null;
                                   
                                            this.localOptions=param;
                                    

                               }
                            

                        /**
                        * field for Built
                        */

                        
                                    protected double localBuilt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuiltTracker = false ;

                           public boolean isBuiltSpecified(){
                               return localBuiltTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getBuilt(){
                               return localBuilt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Built
                               */
                               public void setBuilt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localBuiltTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localBuilt=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for Revision
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevision ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevisionTracker = false ;

                           public boolean isRevisionSpecified(){
                               return localRevisionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevision(){
                               return localRevision;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Revision
                               */
                               public void setRevision(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevisionTracker = param != null;
                                   
                                            this.localRevision=param;
                                    

                               }
                            

                        /**
                        * field for BillOfMaterialsRevision
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillOfMaterialsRevision ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillOfMaterialsRevisionTracker = false ;

                           public boolean isBillOfMaterialsRevisionSpecified(){
                               return localBillOfMaterialsRevisionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillOfMaterialsRevision(){
                               return localBillOfMaterialsRevision;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillOfMaterialsRevision
                               */
                               public void setBillOfMaterialsRevision(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillOfMaterialsRevisionTracker = param != null;
                                   
                                            this.localBillOfMaterialsRevision=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:inventory_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":WorkOrder",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "WorkOrder",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localManufacturingRoutingTracker){
                                            if (localManufacturingRouting==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingRouting cannot be null!!");
                                            }
                                           localManufacturingRouting.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","manufacturingRouting"),
                                               xmlWriter);
                                        } if (localAutoCalculateLagTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "autoCalculateLag", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("autoCalculateLag cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAutoCalculateLag));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStatusTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "status", xmlWriter);
                             

                                          if (localStatus==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStatus);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranIdTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranId", xmlWriter);
                             

                                          if (localTranId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTranId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localAssemblyItemTracker){
                                            if (localAssemblyItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assemblyItem cannot be null!!");
                                            }
                                           localAssemblyItem.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","assemblyItem"),
                                               xmlWriter);
                                        } if (localExpandAssemblyTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "expandAssembly", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("expandAssembly cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpandAssembly));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsWipTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isWip", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isWip cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsWip));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillOfMaterialsTracker){
                                            if (localBillOfMaterials==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billOfMaterials cannot be null!!");
                                            }
                                           localBillOfMaterials.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","billOfMaterials"),
                                               xmlWriter);
                                        } if (localUnitsTracker){
                                            if (localUnits==null){
                                                 throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                            }
                                           localUnits.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","units"),
                                               xmlWriter);
                                        } if (localTranDateTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranDate", xmlWriter);
                             

                                          if (localTranDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOrderStatusTracker){
                                            if (localOrderStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("orderStatus cannot be null!!");
                                            }
                                           localOrderStatus.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","orderStatus"),
                                               xmlWriter);
                                        } if (localFirmedTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "firmed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("firmed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirmed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMemoTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localItemListTracker){
                                            if (localItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                            }
                                           localItemList.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","itemList"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localSchedulingMethodTracker){
                                            if (localSchedulingMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("schedulingMethod cannot be null!!");
                                            }
                                           localSchedulingMethod.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","schedulingMethod"),
                                               xmlWriter);
                                        } if (localSalesTeamListTracker){
                                            if (localSalesTeamList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamList cannot be null!!");
                                            }
                                           localSalesTeamList.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","salesTeamList"),
                                               xmlWriter);
                                        } if (localPartnersListTracker){
                                            if (localPartnersList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnersList cannot be null!!");
                                            }
                                           localPartnersList.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","partnersList"),
                                               xmlWriter);
                                        } if (localCreatedFromTracker){
                                            if (localCreatedFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                            }
                                           localCreatedFrom.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","createdFrom"),
                                               xmlWriter);
                                        } if (localSourceTransactionIdTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sourceTransactionId", xmlWriter);
                             

                                          if (localSourceTransactionId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("sourceTransactionId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSourceTransactionId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSourceTransactionLineTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sourceTransactionLine", xmlWriter);
                             
                                               if (localSourceTransactionLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sourceTransactionLine cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSourceTransactionLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSpecialOrderTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "specialOrder", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("specialOrder cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSpecialOrder));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBuildableTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "buildable", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localBuildable)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("buildable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBuildable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOptionsTracker){
                                            if (localOptions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                            }
                                           localOptions.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","options"),
                                               xmlWriter);
                                        } if (localBuiltTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "built", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localBuilt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("built cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBuilt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:inventory_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevisionTracker){
                                            if (localRevision==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revision cannot be null!!");
                                            }
                                           localRevision.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","revision"),
                                               xmlWriter);
                                        } if (localBillOfMaterialsRevisionTracker){
                                            if (localBillOfMaterialsRevision==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billOfMaterialsRevision cannot be null!!");
                                            }
                                           localBillOfMaterialsRevision.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","billOfMaterialsRevision"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:inventory_2017_2.transactions.webservices.netsuite.com")){
                return "ns30";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","WorkOrder"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localManufacturingRoutingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "manufacturingRouting"));
                            
                            
                                    if (localManufacturingRouting==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingRouting cannot be null!!");
                                    }
                                    elementList.add(localManufacturingRouting);
                                } if (localAutoCalculateLagTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "autoCalculateLag"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAutoCalculateLag));
                            } if (localStatusTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "status"));
                                 
                                        if (localStatus != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStatus));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                        }
                                    } if (localTranIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranId"));
                                 
                                        if (localTranId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                        }
                                    } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localAssemblyItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "assemblyItem"));
                            
                            
                                    if (localAssemblyItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("assemblyItem cannot be null!!");
                                    }
                                    elementList.add(localAssemblyItem);
                                } if (localExpandAssemblyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "expandAssembly"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpandAssembly));
                            } if (localIsWipTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "isWip"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsWip));
                            } if (localQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                            } if (localBillOfMaterialsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "billOfMaterials"));
                            
                            
                                    if (localBillOfMaterials==null){
                                         throw new org.apache.axis2.databinding.ADBException("billOfMaterials cannot be null!!");
                                    }
                                    elementList.add(localBillOfMaterials);
                                } if (localUnitsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "units"));
                            
                            
                                    if (localUnits==null){
                                         throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                    }
                                    elementList.add(localUnits);
                                } if (localTranDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranDate"));
                                 
                                        if (localTranDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                        }
                                    } if (localOrderStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderStatus"));
                            
                            
                                    if (localOrderStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("orderStatus cannot be null!!");
                                    }
                                    elementList.add(localOrderStatus);
                                } if (localFirmedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "firmed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirmed));
                            } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemList"));
                            
                            
                                    if (localItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                    }
                                    elementList.add(localItemList);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localSchedulingMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "schedulingMethod"));
                            
                            
                                    if (localSchedulingMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("schedulingMethod cannot be null!!");
                                    }
                                    elementList.add(localSchedulingMethod);
                                } if (localSalesTeamListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesTeamList"));
                            
                            
                                    if (localSalesTeamList==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamList cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamList);
                                } if (localPartnersListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "partnersList"));
                            
                            
                                    if (localPartnersList==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnersList cannot be null!!");
                                    }
                                    elementList.add(localPartnersList);
                                } if (localCreatedFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFrom"));
                            
                            
                                    if (localCreatedFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                    }
                                    elementList.add(localCreatedFrom);
                                } if (localSourceTransactionIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "sourceTransactionId"));
                                 
                                        if (localSourceTransactionId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSourceTransactionId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("sourceTransactionId cannot be null!!");
                                        }
                                    } if (localSourceTransactionLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "sourceTransactionLine"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSourceTransactionLine));
                            } if (localSpecialOrderTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "specialOrder"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSpecialOrder));
                            } if (localBuildableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "buildable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBuildable));
                            } if (localOptionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "options"));
                            
                            
                                    if (localOptions==null){
                                         throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                    }
                                    elementList.add(localOptions);
                                } if (localBuiltTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "built"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBuilt));
                            } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localRevisionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "revision"));
                            
                            
                                    if (localRevision==null){
                                         throw new org.apache.axis2.databinding.ADBException("revision cannot be null!!");
                                    }
                                    elementList.add(localRevision);
                                } if (localBillOfMaterialsRevisionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "billOfMaterialsRevision"));
                            
                            
                                    if (localBillOfMaterialsRevision==null){
                                         throw new org.apache.axis2.databinding.ADBException("billOfMaterialsRevision cannot be null!!");
                                    }
                                    elementList.add(localBillOfMaterialsRevision);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static WorkOrder parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            WorkOrder object =
                new WorkOrder();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"WorkOrder".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WorkOrder)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","manufacturingRouting").equals(reader.getName())){
                                
                                                object.setManufacturingRouting(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","autoCalculateLag").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"autoCalculateLag" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAutoCalculateLag(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"status" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStatus(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","assemblyItem").equals(reader.getName())){
                                
                                                object.setAssemblyItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","expandAssembly").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"expandAssembly" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpandAssembly(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","isWip").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isWip" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsWip(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","quantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","billOfMaterials").equals(reader.getName())){
                                
                                                object.setBillOfMaterials(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","units").equals(reader.getName())){
                                
                                                object.setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","orderStatus").equals(reader.getName())){
                                
                                                object.setOrderStatus(com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderOrderStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","firmed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"firmed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFirmed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","itemList").equals(reader.getName())){
                                
                                                object.setItemList(com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","schedulingMethod").equals(reader.getName())){
                                
                                                object.setSchedulingMethod(com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderSchedulingMethod.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","salesTeamList").equals(reader.getName())){
                                
                                                object.setSalesTeamList(com.netsuite.webservices.transactions.inventory_2017_2.SalesTeamList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","partnersList").equals(reader.getName())){
                                
                                                object.setPartnersList(com.netsuite.webservices.transactions.inventory_2017_2.PartnersList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","createdFrom").equals(reader.getName())){
                                
                                                object.setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","sourceTransactionId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sourceTransactionId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSourceTransactionId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","sourceTransactionLine").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sourceTransactionLine" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSourceTransactionLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSourceTransactionLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","specialOrder").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"specialOrder" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSpecialOrder(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","buildable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"buildable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBuildable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBuildable(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","options").equals(reader.getName())){
                                
                                                object.setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","built").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"built" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBuilt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBuilt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","revision").equals(reader.getName())){
                                
                                                object.setRevision(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","billOfMaterialsRevision").equals(reader.getName())){
                                
                                                object.setBillOfMaterialsRevision(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:inventory_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    