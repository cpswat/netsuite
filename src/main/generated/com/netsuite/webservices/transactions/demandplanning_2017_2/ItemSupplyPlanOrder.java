
/**
 * ItemSupplyPlanOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.demandplanning_2017_2;
            

            /**
            *  ItemSupplyPlanOrder bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemSupplyPlanOrder
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemSupplyPlanOrder
                Namespace URI = urn:demandplanning_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns42
                */
            

                        /**
                        * field for OrderLineId
                        */

                        
                                    protected long localOrderLineId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderLineIdTracker = false ;

                           public boolean isOrderLineIdSpecified(){
                               return localOrderLineIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOrderLineId(){
                               return localOrderLineId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderLineId
                               */
                               public void setOrderLineId(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localOrderLineIdTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localOrderLineId=param;
                                    

                               }
                            

                        /**
                        * field for OrderDate
                        */

                        
                                    protected java.util.Calendar localOrderDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderDateTracker = false ;

                           public boolean isOrderDateSpecified(){
                               return localOrderDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getOrderDate(){
                               return localOrderDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderDate
                               */
                               public void setOrderDate(java.util.Calendar param){
                            localOrderDateTracker = param != null;
                                   
                                            this.localOrderDate=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptDate
                        */

                        
                                    protected java.util.Calendar localReceiptDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptDateTracker = false ;

                           public boolean isReceiptDateSpecified(){
                               return localReceiptDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getReceiptDate(){
                               return localReceiptDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptDate
                               */
                               public void setReceiptDate(java.util.Calendar param){
                            localReceiptDateTracker = param != null;
                                   
                                            this.localReceiptDate=param;
                                    

                               }
                            

                        /**
                        * field for SourceLocation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSourceLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceLocationTracker = false ;

                           public boolean isSourceLocationSpecified(){
                               return localSourceLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSourceLocation(){
                               return localSourceLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceLocation
                               */
                               public void setSourceLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSourceLocationTracker = param != null;
                                   
                                            this.localSourceLocation=param;
                                    

                               }
                            

                        /**
                        * field for Quantity
                        */

                        
                                    protected double localQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityTracker = false ;

                           public boolean isQuantitySpecified(){
                               return localQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantity(){
                               return localQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Quantity
                               */
                               public void setQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantity=param;
                                    

                               }
                            

                        /**
                        * field for OrderCreated
                        */

                        
                                    protected boolean localOrderCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderCreatedTracker = false ;

                           public boolean isOrderCreatedSpecified(){
                               return localOrderCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getOrderCreated(){
                               return localOrderCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderCreated
                               */
                               public void setOrderCreated(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localOrderCreatedTracker =
                                       true;
                                   
                                            this.localOrderCreated=param;
                                    

                               }
                            

                        /**
                        * field for OrderType
                        */

                        
                                    protected com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemSupplyPlanOrderType localOrderType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderTypeTracker = false ;

                           public boolean isOrderTypeSpecified(){
                               return localOrderTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemSupplyPlanOrderType
                           */
                           public  com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemSupplyPlanOrderType getOrderType(){
                               return localOrderType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderType
                               */
                               public void setOrderType(com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemSupplyPlanOrderType param){
                            localOrderTypeTracker = param != null;
                                   
                                            this.localOrderType=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:demandplanning_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemSupplyPlanOrder",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemSupplyPlanOrder",
                           xmlWriter);
                   }

               
                   }
                if (localOrderLineIdTracker){
                                    namespace = "urn:demandplanning_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderLineId", xmlWriter);
                             
                                               if (localOrderLineId==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("orderLineId cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderLineId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOrderDateTracker){
                                    namespace = "urn:demandplanning_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderDate", xmlWriter);
                             

                                          if (localOrderDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("orderDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReceiptDateTracker){
                                    namespace = "urn:demandplanning_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "receiptDate", xmlWriter);
                             

                                          if (localReceiptDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("receiptDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSourceLocationTracker){
                                            if (localSourceLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sourceLocation cannot be null!!");
                                            }
                                           localSourceLocation.serialize(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","sourceLocation"),
                                               xmlWriter);
                                        } if (localQuantityTracker){
                                    namespace = "urn:demandplanning_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOrderCreatedTracker){
                                    namespace = "urn:demandplanning_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderCreated", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("orderCreated cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderCreated));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOrderTypeTracker){
                                            if (localOrderType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("orderType cannot be null!!");
                                            }
                                           localOrderType.serialize(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","orderType"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:demandplanning_2017_2.transactions.webservices.netsuite.com")){
                return "ns42";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localOrderLineIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderLineId"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderLineId));
                            } if (localOrderDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderDate"));
                                 
                                        if (localOrderDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("orderDate cannot be null!!");
                                        }
                                    } if (localReceiptDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "receiptDate"));
                                 
                                        if (localReceiptDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("receiptDate cannot be null!!");
                                        }
                                    } if (localSourceLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "sourceLocation"));
                            
                            
                                    if (localSourceLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("sourceLocation cannot be null!!");
                                    }
                                    elementList.add(localSourceLocation);
                                } if (localQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                            } if (localOrderCreatedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderCreated"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderCreated));
                            } if (localOrderTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderType"));
                            
                            
                                    if (localOrderType==null){
                                         throw new org.apache.axis2.databinding.ADBException("orderType cannot be null!!");
                                    }
                                    elementList.add(localOrderType);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemSupplyPlanOrder parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemSupplyPlanOrder object =
                new ItemSupplyPlanOrder();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemSupplyPlanOrder".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemSupplyPlanOrder)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","orderLineId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderLineId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderLineId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOrderLineId(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","orderDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","receiptDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"receiptDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReceiptDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","sourceLocation").equals(reader.getName())){
                                
                                                object.setSourceLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","quantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","orderCreated").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderCreated" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderCreated(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:demandplanning_2017_2.transactions.webservices.netsuite.com","orderType").equals(reader.getName())){
                                
                                                object.setOrderType(com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemSupplyPlanOrderType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    