
/**
 * Budget.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.financial_2017_2;
            

            /**
            *  Budget bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Budget extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Budget
                Namespace URI = urn:financial_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns27
                */
            

                        /**
                        * field for Year
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localYear ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearTracker = false ;

                           public boolean isYearSpecified(){
                               return localYearTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getYear(){
                               return localYear;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Year
                               */
                               public void setYear(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localYearTracker = param != null;
                                   
                                            this.localYear=param;
                                    

                               }
                            

                        /**
                        * field for Customer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerTracker = false ;

                           public boolean isCustomerSpecified(){
                               return localCustomerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomer(){
                               return localCustomer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Customer
                               */
                               public void setCustomer(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomerTracker = param != null;
                                   
                                            this.localCustomer=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for Account
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountTracker = false ;

                           public boolean isAccountSpecified(){
                               return localAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAccount(){
                               return localAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Account
                               */
                               public void setAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAccountTracker = param != null;
                                   
                                            this.localAccount=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for Category
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryTracker = false ;

                           public boolean isCategorySpecified(){
                               return localCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCategory(){
                               return localCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Category
                               */
                               public void setCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCategoryTracker = param != null;
                                   
                                            this.localCategory=param;
                                    

                               }
                            

                        /**
                        * field for BudgetType
                        */

                        
                                    protected com.netsuite.webservices.transactions.financial_2017_2.types.BudgetBudgetType localBudgetType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBudgetTypeTracker = false ;

                           public boolean isBudgetTypeSpecified(){
                               return localBudgetTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.financial_2017_2.types.BudgetBudgetType
                           */
                           public  com.netsuite.webservices.transactions.financial_2017_2.types.BudgetBudgetType getBudgetType(){
                               return localBudgetType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BudgetType
                               */
                               public void setBudgetType(com.netsuite.webservices.transactions.financial_2017_2.types.BudgetBudgetType param){
                            localBudgetTypeTracker = param != null;
                                   
                                            this.localBudgetType=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount1
                        */

                        
                                    protected double localPeriodAmount1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount1Tracker = false ;

                           public boolean isPeriodAmount1Specified(){
                               return localPeriodAmount1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount1(){
                               return localPeriodAmount1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount1
                               */
                               public void setPeriodAmount1(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount1Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount1=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount2
                        */

                        
                                    protected double localPeriodAmount2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount2Tracker = false ;

                           public boolean isPeriodAmount2Specified(){
                               return localPeriodAmount2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount2(){
                               return localPeriodAmount2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount2
                               */
                               public void setPeriodAmount2(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount2Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount2=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount3
                        */

                        
                                    protected double localPeriodAmount3 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount3Tracker = false ;

                           public boolean isPeriodAmount3Specified(){
                               return localPeriodAmount3Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount3(){
                               return localPeriodAmount3;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount3
                               */
                               public void setPeriodAmount3(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount3Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount3=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount4
                        */

                        
                                    protected double localPeriodAmount4 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount4Tracker = false ;

                           public boolean isPeriodAmount4Specified(){
                               return localPeriodAmount4Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount4(){
                               return localPeriodAmount4;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount4
                               */
                               public void setPeriodAmount4(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount4Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount4=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount5
                        */

                        
                                    protected double localPeriodAmount5 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount5Tracker = false ;

                           public boolean isPeriodAmount5Specified(){
                               return localPeriodAmount5Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount5(){
                               return localPeriodAmount5;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount5
                               */
                               public void setPeriodAmount5(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount5Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount5=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount6
                        */

                        
                                    protected double localPeriodAmount6 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount6Tracker = false ;

                           public boolean isPeriodAmount6Specified(){
                               return localPeriodAmount6Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount6(){
                               return localPeriodAmount6;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount6
                               */
                               public void setPeriodAmount6(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount6Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount6=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount7
                        */

                        
                                    protected double localPeriodAmount7 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount7Tracker = false ;

                           public boolean isPeriodAmount7Specified(){
                               return localPeriodAmount7Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount7(){
                               return localPeriodAmount7;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount7
                               */
                               public void setPeriodAmount7(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount7Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount7=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount8
                        */

                        
                                    protected double localPeriodAmount8 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount8Tracker = false ;

                           public boolean isPeriodAmount8Specified(){
                               return localPeriodAmount8Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount8(){
                               return localPeriodAmount8;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount8
                               */
                               public void setPeriodAmount8(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount8Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount8=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount9
                        */

                        
                                    protected double localPeriodAmount9 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount9Tracker = false ;

                           public boolean isPeriodAmount9Specified(){
                               return localPeriodAmount9Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount9(){
                               return localPeriodAmount9;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount9
                               */
                               public void setPeriodAmount9(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount9Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount9=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount10
                        */

                        
                                    protected double localPeriodAmount10 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount10Tracker = false ;

                           public boolean isPeriodAmount10Specified(){
                               return localPeriodAmount10Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount10(){
                               return localPeriodAmount10;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount10
                               */
                               public void setPeriodAmount10(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount10Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount10=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount11
                        */

                        
                                    protected double localPeriodAmount11 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount11Tracker = false ;

                           public boolean isPeriodAmount11Specified(){
                               return localPeriodAmount11Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount11(){
                               return localPeriodAmount11;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount11
                               */
                               public void setPeriodAmount11(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount11Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount11=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount12
                        */

                        
                                    protected double localPeriodAmount12 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount12Tracker = false ;

                           public boolean isPeriodAmount12Specified(){
                               return localPeriodAmount12Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount12(){
                               return localPeriodAmount12;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount12
                               */
                               public void setPeriodAmount12(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount12Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount12=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount13
                        */

                        
                                    protected double localPeriodAmount13 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount13Tracker = false ;

                           public boolean isPeriodAmount13Specified(){
                               return localPeriodAmount13Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount13(){
                               return localPeriodAmount13;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount13
                               */
                               public void setPeriodAmount13(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount13Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount13=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount14
                        */

                        
                                    protected double localPeriodAmount14 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount14Tracker = false ;

                           public boolean isPeriodAmount14Specified(){
                               return localPeriodAmount14Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount14(){
                               return localPeriodAmount14;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount14
                               */
                               public void setPeriodAmount14(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount14Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount14=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount15
                        */

                        
                                    protected double localPeriodAmount15 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount15Tracker = false ;

                           public boolean isPeriodAmount15Specified(){
                               return localPeriodAmount15Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount15(){
                               return localPeriodAmount15;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount15
                               */
                               public void setPeriodAmount15(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount15Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount15=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount16
                        */

                        
                                    protected double localPeriodAmount16 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount16Tracker = false ;

                           public boolean isPeriodAmount16Specified(){
                               return localPeriodAmount16Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount16(){
                               return localPeriodAmount16;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount16
                               */
                               public void setPeriodAmount16(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount16Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount16=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount17
                        */

                        
                                    protected double localPeriodAmount17 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount17Tracker = false ;

                           public boolean isPeriodAmount17Specified(){
                               return localPeriodAmount17Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount17(){
                               return localPeriodAmount17;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount17
                               */
                               public void setPeriodAmount17(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount17Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount17=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount18
                        */

                        
                                    protected double localPeriodAmount18 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount18Tracker = false ;

                           public boolean isPeriodAmount18Specified(){
                               return localPeriodAmount18Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount18(){
                               return localPeriodAmount18;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount18
                               */
                               public void setPeriodAmount18(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount18Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount18=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount19
                        */

                        
                                    protected double localPeriodAmount19 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount19Tracker = false ;

                           public boolean isPeriodAmount19Specified(){
                               return localPeriodAmount19Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount19(){
                               return localPeriodAmount19;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount19
                               */
                               public void setPeriodAmount19(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount19Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount19=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount20
                        */

                        
                                    protected double localPeriodAmount20 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount20Tracker = false ;

                           public boolean isPeriodAmount20Specified(){
                               return localPeriodAmount20Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount20(){
                               return localPeriodAmount20;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount20
                               */
                               public void setPeriodAmount20(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount20Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount20=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount21
                        */

                        
                                    protected double localPeriodAmount21 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount21Tracker = false ;

                           public boolean isPeriodAmount21Specified(){
                               return localPeriodAmount21Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount21(){
                               return localPeriodAmount21;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount21
                               */
                               public void setPeriodAmount21(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount21Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount21=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount22
                        */

                        
                                    protected double localPeriodAmount22 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount22Tracker = false ;

                           public boolean isPeriodAmount22Specified(){
                               return localPeriodAmount22Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount22(){
                               return localPeriodAmount22;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount22
                               */
                               public void setPeriodAmount22(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount22Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount22=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount23
                        */

                        
                                    protected double localPeriodAmount23 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount23Tracker = false ;

                           public boolean isPeriodAmount23Specified(){
                               return localPeriodAmount23Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount23(){
                               return localPeriodAmount23;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount23
                               */
                               public void setPeriodAmount23(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount23Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount23=param;
                                    

                               }
                            

                        /**
                        * field for PeriodAmount24
                        */

                        
                                    protected double localPeriodAmount24 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodAmount24Tracker = false ;

                           public boolean isPeriodAmount24Specified(){
                               return localPeriodAmount24Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPeriodAmount24(){
                               return localPeriodAmount24;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodAmount24
                               */
                               public void setPeriodAmount24(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodAmount24Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPeriodAmount24=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected double localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:financial_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Budget",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Budget",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localYearTracker){
                                            if (localYear==null){
                                                 throw new org.apache.axis2.databinding.ADBException("year cannot be null!!");
                                            }
                                           localYear.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","year"),
                                               xmlWriter);
                                        } if (localCustomerTracker){
                                            if (localCustomer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                            }
                                           localCustomer.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","customer"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localAccountTracker){
                                            if (localAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                            }
                                           localAccount.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","account"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localCategoryTracker){
                                            if (localCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                            }
                                           localCategory.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","category"),
                                               xmlWriter);
                                        } if (localBudgetTypeTracker){
                                            if (localBudgetType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("budgetType cannot be null!!");
                                            }
                                           localBudgetType.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","budgetType"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localPeriodAmount1Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount1", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount1)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount1 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount1));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount2Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount2", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount2)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount2 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount2));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount3Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount3", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount3)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount3 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount3));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount4Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount4", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount4)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount4 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount4));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount5Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount5", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount5)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount5 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount5));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount6Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount6", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount6)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount6 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount6));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount7Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount7", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount7)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount7 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount7));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount8Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount8", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount8)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount8 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount8));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount9Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount9", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount9)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount9 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount9));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount10Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount10", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount10)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount10 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount10));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount11Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount11", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount11)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount11 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount11));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount12Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount12", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount12)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount12 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount12));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount13Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount13", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount13)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount13 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount13));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount14Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount14", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount14)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount14 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount14));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount15Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount15", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount15)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount15 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount15));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount16Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount16", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount16)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount16 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount16));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount17Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount17", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount17)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount17 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount17));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount18Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount18", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount18)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount18 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount18));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount19Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount19", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount19)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount19 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount19));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount20Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount20", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount20)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount20 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount20));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount21Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount21", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount21)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount21 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount21));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount22Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount22", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount22)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount22 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount22));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount23Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount23", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount23)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount23 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount23));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodAmount24Tracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodAmount24", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPeriodAmount24)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodAmount24 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount24));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmountTracker){
                                    namespace = "urn:financial_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:financial_2017_2.transactions.webservices.netsuite.com")){
                return "ns27";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","Budget"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localYearTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "year"));
                            
                            
                                    if (localYear==null){
                                         throw new org.apache.axis2.databinding.ADBException("year cannot be null!!");
                                    }
                                    elementList.add(localYear);
                                } if (localCustomerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "customer"));
                            
                            
                                    if (localCustomer==null){
                                         throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                    }
                                    elementList.add(localCustomer);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "account"));
                            
                            
                                    if (localAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                    }
                                    elementList.add(localAccount);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "category"));
                            
                            
                                    if (localCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                    }
                                    elementList.add(localCategory);
                                } if (localBudgetTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "budgetType"));
                            
                            
                                    if (localBudgetType==null){
                                         throw new org.apache.axis2.databinding.ADBException("budgetType cannot be null!!");
                                    }
                                    elementList.add(localBudgetType);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localPeriodAmount1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount1"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount1));
                            } if (localPeriodAmount2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount2"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount2));
                            } if (localPeriodAmount3Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount3"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount3));
                            } if (localPeriodAmount4Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount4"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount4));
                            } if (localPeriodAmount5Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount5"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount5));
                            } if (localPeriodAmount6Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount6"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount6));
                            } if (localPeriodAmount7Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount7"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount7));
                            } if (localPeriodAmount8Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount8"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount8));
                            } if (localPeriodAmount9Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount9"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount9));
                            } if (localPeriodAmount10Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount10"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount10));
                            } if (localPeriodAmount11Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount11"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount11));
                            } if (localPeriodAmount12Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount12"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount12));
                            } if (localPeriodAmount13Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount13"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount13));
                            } if (localPeriodAmount14Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount14"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount14));
                            } if (localPeriodAmount15Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount15"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount15));
                            } if (localPeriodAmount16Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount16"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount16));
                            } if (localPeriodAmount17Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount17"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount17));
                            } if (localPeriodAmount18Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount18"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount18));
                            } if (localPeriodAmount19Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount19"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount19));
                            } if (localPeriodAmount20Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount20"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount20));
                            } if (localPeriodAmount21Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount21"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount21));
                            } if (localPeriodAmount22Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount22"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount22));
                            } if (localPeriodAmount23Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount23"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount23));
                            } if (localPeriodAmount24Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "periodAmount24"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodAmount24));
                            } if (localAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "amount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                            } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Budget parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Budget object =
                new Budget();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Budget".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Budget)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","year").equals(reader.getName())){
                                
                                                object.setYear(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","customer").equals(reader.getName())){
                                
                                                object.setCustomer(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","account").equals(reader.getName())){
                                
                                                object.setAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","category").equals(reader.getName())){
                                
                                                object.setCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","budgetType").equals(reader.getName())){
                                
                                                object.setBudgetType(com.netsuite.webservices.transactions.financial_2017_2.types.BudgetBudgetType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount1(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount2(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount3").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount3" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount3(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount3(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount4").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount4" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount4(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount4(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount5").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount5" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount5(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount5(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount6").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount6" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount6(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount6(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount7").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount7" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount7(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount7(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount8").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount8" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount8(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount8(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount9").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount9" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount9(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount9(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount10").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount10" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount10(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount10(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount11").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount11" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount11(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount11(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount12").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount12" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount12(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount12(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount13").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount13" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount13(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount13(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount14").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount14" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount14(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount14(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount15").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount15" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount15(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount15(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount16").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount16" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount16(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount16(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount17").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount17" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount17(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount17(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount18").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount18" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount18(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount18(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount19").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount19" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount19(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount19(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount20").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount20" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount20(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount20(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount21").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount21" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount21(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount21(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount22").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount22" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount22(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount22(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount23").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount23" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount23(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount23(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","periodAmount24").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodAmount24" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodAmount24(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodAmount24(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:financial_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    