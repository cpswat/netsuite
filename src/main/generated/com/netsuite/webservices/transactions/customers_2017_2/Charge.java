
/**
 * Charge.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.customers_2017_2;
            

            /**
            *  Charge bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Charge extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Charge
                Namespace URI = urn:customers_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns25
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for SalesOrder
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesOrderTracker = false ;

                           public boolean isSalesOrderSpecified(){
                               return localSalesOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesOrder(){
                               return localSalesOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesOrder
                               */
                               public void setSalesOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesOrderTracker = param != null;
                                   
                                            this.localSalesOrder=param;
                                    

                               }
                            

                        /**
                        * field for BillTo
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillTo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillToTracker = false ;

                           public boolean isBillToSpecified(){
                               return localBillToTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillTo(){
                               return localBillTo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillTo
                               */
                               public void setBillTo(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillToTracker = param != null;
                                   
                                            this.localBillTo=param;
                                    

                               }
                            

                        /**
                        * field for BillingAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingAccountTracker = false ;

                           public boolean isBillingAccountSpecified(){
                               return localBillingAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingAccount(){
                               return localBillingAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingAccount
                               */
                               public void setBillingAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingAccountTracker = param != null;
                                   
                                            this.localBillingAccount=param;
                                    

                               }
                            

                        /**
                        * field for Stage
                        */

                        
                                    protected com.netsuite.webservices.transactions.customers_2017_2.types.ChargeStage localStage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStageTracker = false ;

                           public boolean isStageSpecified(){
                               return localStageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.customers_2017_2.types.ChargeStage
                           */
                           public  com.netsuite.webservices.transactions.customers_2017_2.types.ChargeStage getStage(){
                               return localStage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Stage
                               */
                               public void setStage(com.netsuite.webservices.transactions.customers_2017_2.types.ChargeStage param){
                            localStageTracker = param != null;
                                   
                                            this.localStage=param;
                                    

                               }
                            

                        /**
                        * field for ChargeDate
                        */

                        
                                    protected java.util.Calendar localChargeDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localChargeDateTracker = false ;

                           public boolean isChargeDateSpecified(){
                               return localChargeDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getChargeDate(){
                               return localChargeDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ChargeDate
                               */
                               public void setChargeDate(java.util.Calendar param){
                            localChargeDateTracker = param != null;
                                   
                                            this.localChargeDate=param;
                                    

                               }
                            

                        /**
                        * field for Use
                        */

                        
                                    protected com.netsuite.webservices.transactions.customers_2017_2.types.ChargeUse localUse ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseTracker = false ;

                           public boolean isUseSpecified(){
                               return localUseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.customers_2017_2.types.ChargeUse
                           */
                           public  com.netsuite.webservices.transactions.customers_2017_2.types.ChargeUse getUse(){
                               return localUse;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Use
                               */
                               public void setUse(com.netsuite.webservices.transactions.customers_2017_2.types.ChargeUse param){
                            localUseTracker = param != null;
                                   
                                            this.localUse=param;
                                    

                               }
                            

                        /**
                        * field for ChargeType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localChargeType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localChargeTypeTracker = false ;

                           public boolean isChargeTypeSpecified(){
                               return localChargeTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getChargeType(){
                               return localChargeType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ChargeType
                               */
                               public void setChargeType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localChargeTypeTracker = param != null;
                                   
                                            this.localChargeType=param;
                                    

                               }
                            

                        /**
                        * field for ProjectTask
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProjectTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTaskTracker = false ;

                           public boolean isProjectTaskSpecified(){
                               return localProjectTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProjectTask(){
                               return localProjectTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectTask
                               */
                               public void setProjectTask(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProjectTaskTracker = param != null;
                                   
                                            this.localProjectTask=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for TimeRecord
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTimeRecord ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeRecordTracker = false ;

                           public boolean isTimeRecordSpecified(){
                               return localTimeRecordTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTimeRecord(){
                               return localTimeRecord;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeRecord
                               */
                               public void setTimeRecord(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTimeRecordTracker = param != null;
                                   
                                            this.localTimeRecord=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected java.lang.String localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(java.lang.String param){
                            localRateTracker = param != null;
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for Quantity
                        */

                        
                                    protected double localQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityTracker = false ;

                           public boolean isQuantitySpecified(){
                               return localQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantity(){
                               return localQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Quantity
                               */
                               public void setQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantity=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected double localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for BillingItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingItemTracker = false ;

                           public boolean isBillingItemSpecified(){
                               return localBillingItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingItem(){
                               return localBillingItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingItem
                               */
                               public void setBillingItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingItemTracker = param != null;
                                   
                                            this.localBillingItem=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for Transaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionTracker = false ;

                           public boolean isTransactionSpecified(){
                               return localTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTransaction(){
                               return localTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Transaction
                               */
                               public void setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTransactionTracker = param != null;
                                   
                                            this.localTransaction=param;
                                    

                               }
                            

                        /**
                        * field for TransactionLine
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTransactionLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionLineTracker = false ;

                           public boolean isTransactionLineSpecified(){
                               return localTransactionLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTransactionLine(){
                               return localTransactionLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionLine
                               */
                               public void setTransactionLine(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTransactionLineTracker = param != null;
                                   
                                            this.localTransactionLine=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for SalesOrderLine
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesOrderLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesOrderLineTracker = false ;

                           public boolean isSalesOrderLineSpecified(){
                               return localSalesOrderLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesOrderLine(){
                               return localSalesOrderLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesOrderLine
                               */
                               public void setSalesOrderLine(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesOrderLineTracker = param != null;
                                   
                                            this.localSalesOrderLine=param;
                                    

                               }
                            

                        /**
                        * field for SubscriptionLine
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubscriptionLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubscriptionLineTracker = false ;

                           public boolean isSubscriptionLineSpecified(){
                               return localSubscriptionLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubscriptionLine(){
                               return localSubscriptionLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubscriptionLine
                               */
                               public void setSubscriptionLine(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubscriptionLineTracker = param != null;
                                   
                                            this.localSubscriptionLine=param;
                                    

                               }
                            

                        /**
                        * field for Invoice
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInvoice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInvoiceTracker = false ;

                           public boolean isInvoiceSpecified(){
                               return localInvoiceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInvoice(){
                               return localInvoice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Invoice
                               */
                               public void setInvoice(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInvoiceTracker = param != null;
                                   
                                            this.localInvoice=param;
                                    

                               }
                            

                        /**
                        * field for InvoiceLine
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInvoiceLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInvoiceLineTracker = false ;

                           public boolean isInvoiceLineSpecified(){
                               return localInvoiceLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInvoiceLine(){
                               return localInvoiceLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InvoiceLine
                               */
                               public void setInvoiceLine(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInvoiceLineTracker = param != null;
                                   
                                            this.localInvoiceLine=param;
                                    

                               }
                            

                        /**
                        * field for Rule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRuleTracker = false ;

                           public boolean isRuleSpecified(){
                               return localRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRule(){
                               return localRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rule
                               */
                               public void setRule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRuleTracker = param != null;
                                   
                                            this.localRule=param;
                                    

                               }
                            

                        /**
                        * field for RunId
                        */

                        
                                    protected java.lang.String localRunId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRunIdTracker = false ;

                           public boolean isRunIdSpecified(){
                               return localRunIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRunId(){
                               return localRunId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RunId
                               */
                               public void setRunId(java.lang.String param){
                            localRunIdTracker = param != null;
                                   
                                            this.localRunId=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:customers_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Charge",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Charge",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localSalesOrderTracker){
                                            if (localSalesOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesOrder cannot be null!!");
                                            }
                                           localSalesOrder.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesOrder"),
                                               xmlWriter);
                                        } if (localBillToTracker){
                                            if (localBillTo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billTo cannot be null!!");
                                            }
                                           localBillTo.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billTo"),
                                               xmlWriter);
                                        } if (localBillingAccountTracker){
                                            if (localBillingAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingAccount cannot be null!!");
                                            }
                                           localBillingAccount.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billingAccount"),
                                               xmlWriter);
                                        } if (localStageTracker){
                                            if (localStage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("stage cannot be null!!");
                                            }
                                           localStage.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","stage"),
                                               xmlWriter);
                                        } if (localChargeDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "chargeDate", xmlWriter);
                             

                                          if (localChargeDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("chargeDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localChargeDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUseTracker){
                                            if (localUse==null){
                                                 throw new org.apache.axis2.databinding.ADBException("use cannot be null!!");
                                            }
                                           localUse.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","use"),
                                               xmlWriter);
                                        } if (localChargeTypeTracker){
                                            if (localChargeType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("chargeType cannot be null!!");
                                            }
                                           localChargeType.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","chargeType"),
                                               xmlWriter);
                                        } if (localProjectTaskTracker){
                                            if (localProjectTask==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectTask cannot be null!!");
                                            }
                                           localProjectTask.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","projectTask"),
                                               xmlWriter);
                                        } if (localDescriptionTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTimeRecordTracker){
                                            if (localTimeRecord==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeRecord cannot be null!!");
                                            }
                                           localTimeRecord.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","timeRecord"),
                                               xmlWriter);
                                        } if (localRateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             

                                          if (localRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmountTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillingItemTracker){
                                            if (localBillingItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingItem cannot be null!!");
                                            }
                                           localBillingItem.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billingItem"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localTransactionTracker){
                                            if (localTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                            }
                                           localTransaction.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","transaction"),
                                               xmlWriter);
                                        } if (localTransactionLineTracker){
                                            if (localTransactionLine==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionLine cannot be null!!");
                                            }
                                           localTransactionLine.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","transactionLine"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localSalesOrderLineTracker){
                                            if (localSalesOrderLine==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesOrderLine cannot be null!!");
                                            }
                                           localSalesOrderLine.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesOrderLine"),
                                               xmlWriter);
                                        } if (localSubscriptionLineTracker){
                                            if (localSubscriptionLine==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subscriptionLine cannot be null!!");
                                            }
                                           localSubscriptionLine.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subscriptionLine"),
                                               xmlWriter);
                                        } if (localInvoiceTracker){
                                            if (localInvoice==null){
                                                 throw new org.apache.axis2.databinding.ADBException("invoice cannot be null!!");
                                            }
                                           localInvoice.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","invoice"),
                                               xmlWriter);
                                        } if (localInvoiceLineTracker){
                                            if (localInvoiceLine==null){
                                                 throw new org.apache.axis2.databinding.ADBException("invoiceLine cannot be null!!");
                                            }
                                           localInvoiceLine.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","invoiceLine"),
                                               xmlWriter);
                                        } if (localRuleTracker){
                                            if (localRule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rule cannot be null!!");
                                            }
                                           localRule.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","rule"),
                                               xmlWriter);
                                        } if (localRunIdTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "runId", xmlWriter);
                             

                                          if (localRunId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("runId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRunId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:customers_2017_2.transactions.webservices.netsuite.com")){
                return "ns25";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","Charge"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localSalesOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesOrder"));
                            
                            
                                    if (localSalesOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesOrder cannot be null!!");
                                    }
                                    elementList.add(localSalesOrder);
                                } if (localBillToTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "billTo"));
                            
                            
                                    if (localBillTo==null){
                                         throw new org.apache.axis2.databinding.ADBException("billTo cannot be null!!");
                                    }
                                    elementList.add(localBillTo);
                                } if (localBillingAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingAccount"));
                            
                            
                                    if (localBillingAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingAccount cannot be null!!");
                                    }
                                    elementList.add(localBillingAccount);
                                } if (localStageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "stage"));
                            
                            
                                    if (localStage==null){
                                         throw new org.apache.axis2.databinding.ADBException("stage cannot be null!!");
                                    }
                                    elementList.add(localStage);
                                } if (localChargeDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "chargeDate"));
                                 
                                        if (localChargeDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localChargeDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("chargeDate cannot be null!!");
                                        }
                                    } if (localUseTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "use"));
                            
                            
                                    if (localUse==null){
                                         throw new org.apache.axis2.databinding.ADBException("use cannot be null!!");
                                    }
                                    elementList.add(localUse);
                                } if (localChargeTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "chargeType"));
                            
                            
                                    if (localChargeType==null){
                                         throw new org.apache.axis2.databinding.ADBException("chargeType cannot be null!!");
                                    }
                                    elementList.add(localChargeType);
                                } if (localProjectTaskTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "projectTask"));
                            
                            
                                    if (localProjectTask==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectTask cannot be null!!");
                                    }
                                    elementList.add(localProjectTask);
                                } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localTimeRecordTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "timeRecord"));
                            
                            
                                    if (localTimeRecord==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeRecord cannot be null!!");
                                    }
                                    elementList.add(localTimeRecord);
                                } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                        if (localRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                        }
                                    } if (localQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                            } if (localAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "amount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                            } if (localBillingItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingItem"));
                            
                            
                                    if (localBillingItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingItem cannot be null!!");
                                    }
                                    elementList.add(localBillingItem);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "transaction"));
                            
                            
                                    if (localTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                    }
                                    elementList.add(localTransaction);
                                } if (localTransactionLineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "transactionLine"));
                            
                            
                                    if (localTransactionLine==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionLine cannot be null!!");
                                    }
                                    elementList.add(localTransactionLine);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localSalesOrderLineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesOrderLine"));
                            
                            
                                    if (localSalesOrderLine==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesOrderLine cannot be null!!");
                                    }
                                    elementList.add(localSalesOrderLine);
                                } if (localSubscriptionLineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "subscriptionLine"));
                            
                            
                                    if (localSubscriptionLine==null){
                                         throw new org.apache.axis2.databinding.ADBException("subscriptionLine cannot be null!!");
                                    }
                                    elementList.add(localSubscriptionLine);
                                } if (localInvoiceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "invoice"));
                            
                            
                                    if (localInvoice==null){
                                         throw new org.apache.axis2.databinding.ADBException("invoice cannot be null!!");
                                    }
                                    elementList.add(localInvoice);
                                } if (localInvoiceLineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "invoiceLine"));
                            
                            
                                    if (localInvoiceLine==null){
                                         throw new org.apache.axis2.databinding.ADBException("invoiceLine cannot be null!!");
                                    }
                                    elementList.add(localInvoiceLine);
                                } if (localRuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "rule"));
                            
                            
                                    if (localRule==null){
                                         throw new org.apache.axis2.databinding.ADBException("rule cannot be null!!");
                                    }
                                    elementList.add(localRule);
                                } if (localRunIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "runId"));
                                 
                                        if (localRunId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRunId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("runId cannot be null!!");
                                        }
                                    }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Charge parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Charge object =
                new Charge();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Charge".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Charge)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesOrder").equals(reader.getName())){
                                
                                                object.setSalesOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billTo").equals(reader.getName())){
                                
                                                object.setBillTo(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billingAccount").equals(reader.getName())){
                                
                                                object.setBillingAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","stage").equals(reader.getName())){
                                
                                                object.setStage(com.netsuite.webservices.transactions.customers_2017_2.types.ChargeStage.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","chargeDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"chargeDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setChargeDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","use").equals(reader.getName())){
                                
                                                object.setUse(com.netsuite.webservices.transactions.customers_2017_2.types.ChargeUse.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","chargeType").equals(reader.getName())){
                                
                                                object.setChargeType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","projectTask").equals(reader.getName())){
                                
                                                object.setProjectTask(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","timeRecord").equals(reader.getName())){
                                
                                                object.setTimeRecord(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","quantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billingItem").equals(reader.getName())){
                                
                                                object.setBillingItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","transaction").equals(reader.getName())){
                                
                                                object.setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","transactionLine").equals(reader.getName())){
                                
                                                object.setTransactionLine(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesOrderLine").equals(reader.getName())){
                                
                                                object.setSalesOrderLine(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subscriptionLine").equals(reader.getName())){
                                
                                                object.setSubscriptionLine(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","invoice").equals(reader.getName())){
                                
                                                object.setInvoice(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","invoiceLine").equals(reader.getName())){
                                
                                                object.setInvoiceLine(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","rule").equals(reader.getName())){
                                
                                                object.setRule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","runId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"runId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRunId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    