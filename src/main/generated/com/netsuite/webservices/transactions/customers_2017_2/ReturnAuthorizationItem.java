
/**
 * ReturnAuthorizationItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.customers_2017_2;
            

            /**
            *  ReturnAuthorizationItem bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ReturnAuthorizationItem
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ReturnAuthorizationItem
                Namespace URI = urn:customers_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns25
                */
            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for OrderLine
                        */

                        
                                    protected long localOrderLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderLineTracker = false ;

                           public boolean isOrderLineSpecified(){
                               return localOrderLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOrderLine(){
                               return localOrderLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderLine
                               */
                               public void setOrderLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localOrderLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localOrderLine=param;
                                    

                               }
                            

                        /**
                        * field for Line
                        */

                        
                                    protected long localLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineTracker = false ;

                           public boolean isLineSpecified(){
                               return localLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLine(){
                               return localLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Line
                               */
                               public void setLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLine=param;
                                    

                               }
                            

                        /**
                        * field for Quantity
                        */

                        
                                    protected double localQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityTracker = false ;

                           public boolean isQuantitySpecified(){
                               return localQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantity(){
                               return localQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Quantity
                               */
                               public void setQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantity=param;
                                    

                               }
                            

                        /**
                        * field for QuantityReceived
                        */

                        
                                    protected double localQuantityReceived ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityReceivedTracker = false ;

                           public boolean isQuantityReceivedSpecified(){
                               return localQuantityReceivedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityReceived(){
                               return localQuantityReceived;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityReceived
                               */
                               public void setQuantityReceived(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityReceivedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityReceived=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBilled
                        */

                        
                                    protected double localQuantityBilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBilledTracker = false ;

                           public boolean isQuantityBilledSpecified(){
                               return localQuantityBilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityBilled(){
                               return localQuantityBilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBilled
                               */
                               public void setQuantityBilled(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityBilledTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityBilled=param;
                                    

                               }
                            

                        /**
                        * field for Units
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTracker = false ;

                           public boolean isUnitsSpecified(){
                               return localUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnits(){
                               return localUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Units
                               */
                               public void setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitsTracker = param != null;
                                   
                                            this.localUnits=param;
                                    

                               }
                            

                        /**
                        * field for InventoryDetail
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryDetail localInventoryDetail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryDetailTracker = false ;

                           public boolean isInventoryDetailSpecified(){
                               return localInventoryDetailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryDetail
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryDetail getInventoryDetail(){
                               return localInventoryDetail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryDetail
                               */
                               public void setInventoryDetail(com.netsuite.webservices.platform.common_2017_2.InventoryDetail param){
                            localInventoryDetailTracker = param != null;
                                   
                                            this.localInventoryDetail=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for SerialNumbers
                        */

                        
                                    protected java.lang.String localSerialNumbers ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSerialNumbersTracker = false ;

                           public boolean isSerialNumbersSpecified(){
                               return localSerialNumbersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSerialNumbers(){
                               return localSerialNumbers;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SerialNumbers
                               */
                               public void setSerialNumbers(java.lang.String param){
                            localSerialNumbersTracker = param != null;
                                   
                                            this.localSerialNumbers=param;
                                    

                               }
                            

                        /**
                        * field for Price
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriceTracker = false ;

                           public boolean isPriceSpecified(){
                               return localPriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPrice(){
                               return localPrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Price
                               */
                               public void setPrice(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPriceTracker = param != null;
                                   
                                            this.localPrice=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected java.lang.String localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(java.lang.String param){
                            localRateTracker = param != null;
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected double localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for Options
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localOptions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOptionsTracker = false ;

                           public boolean isOptionsSpecified(){
                               return localOptionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getOptions(){
                               return localOptions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Options
                               */
                               public void setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localOptionsTracker = param != null;
                                   
                                            this.localOptions=param;
                                    

                               }
                            

                        /**
                        * field for RevRecTermInMonths
                        */

                        
                                    protected long localRevRecTermInMonths ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecTermInMonthsTracker = false ;

                           public boolean isRevRecTermInMonthsSpecified(){
                               return localRevRecTermInMonthsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getRevRecTermInMonths(){
                               return localRevRecTermInMonths;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecTermInMonths
                               */
                               public void setRevRecTermInMonths(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localRevRecTermInMonthsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localRevRecTermInMonths=param;
                                    

                               }
                            

                        /**
                        * field for DeferRevRec
                        */

                        
                                    protected boolean localDeferRevRec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferRevRecTracker = false ;

                           public boolean isDeferRevRecSpecified(){
                               return localDeferRevRecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDeferRevRec(){
                               return localDeferRevRec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferRevRec
                               */
                               public void setDeferRevRec(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDeferRevRecTracker =
                                       true;
                                   
                                            this.localDeferRevRec=param;
                                    

                               }
                            

                        /**
                        * field for IsClosed
                        */

                        
                                    protected boolean localIsClosed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsClosedTracker = false ;

                           public boolean isIsClosedSpecified(){
                               return localIsClosedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsClosed(){
                               return localIsClosed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsClosed
                               */
                               public void setIsClosed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsClosedTracker =
                                       true;
                                   
                                            this.localIsClosed=param;
                                    

                               }
                            

                        /**
                        * field for IsDropShipment
                        */

                        
                                    protected boolean localIsDropShipment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDropShipmentTracker = false ;

                           public boolean isIsDropShipmentSpecified(){
                               return localIsDropShipmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsDropShipment(){
                               return localIsDropShipment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDropShipment
                               */
                               public void setIsDropShipment(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsDropShipmentTracker =
                                       true;
                                   
                                            this.localIsDropShipment=param;
                                    

                               }
                            

                        /**
                        * field for CatchUpPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCatchUpPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCatchUpPeriodTracker = false ;

                           public boolean isCatchUpPeriodSpecified(){
                               return localCatchUpPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCatchUpPeriod(){
                               return localCatchUpPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CatchUpPeriod
                               */
                               public void setCatchUpPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCatchUpPeriodTracker = param != null;
                                   
                                            this.localCatchUpPeriod=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimateType
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType localCostEstimateType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTypeTracker = false ;

                           public boolean isCostEstimateTypeSpecified(){
                               return localCostEstimateTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType getCostEstimateType(){
                               return localCostEstimateType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimateType
                               */
                               public void setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType param){
                            localCostEstimateTypeTracker = param != null;
                                   
                                            this.localCostEstimateType=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimate
                        */

                        
                                    protected double localCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTracker = false ;

                           public boolean isCostEstimateSpecified(){
                               return localCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCostEstimate(){
                               return localCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimate
                               */
                               public void setCostEstimate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostEstimateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsReference
                        */

                        
                                    protected java.lang.String localTaxDetailsReference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsReferenceTracker = false ;

                           public boolean isTaxDetailsReferenceSpecified(){
                               return localTaxDetailsReferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTaxDetailsReference(){
                               return localTaxDetailsReference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsReference
                               */
                               public void setTaxDetailsReference(java.lang.String param){
                            localTaxDetailsReferenceTracker = param != null;
                                   
                                            this.localTaxDetailsReference=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for RevRecSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleTracker = false ;

                           public boolean isRevRecScheduleSpecified(){
                               return localRevRecScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecSchedule(){
                               return localRevRecSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecSchedule
                               */
                               public void setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecScheduleTracker = param != null;
                                   
                                            this.localRevRecSchedule=param;
                                    

                               }
                            

                        /**
                        * field for RevRecStartDate
                        */

                        
                                    protected java.util.Calendar localRevRecStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecStartDateTracker = false ;

                           public boolean isRevRecStartDateSpecified(){
                               return localRevRecStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecStartDate(){
                               return localRevRecStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecStartDate
                               */
                               public void setRevRecStartDate(java.util.Calendar param){
                            localRevRecStartDateTracker = param != null;
                                   
                                            this.localRevRecStartDate=param;
                                    

                               }
                            

                        /**
                        * field for RevRecEndDate
                        */

                        
                                    protected java.util.Calendar localRevRecEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecEndDateTracker = false ;

                           public boolean isRevRecEndDateSpecified(){
                               return localRevRecEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecEndDate(){
                               return localRevRecEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecEndDate
                               */
                               public void setRevRecEndDate(java.util.Calendar param){
                            localRevRecEndDateTracker = param != null;
                                   
                                            this.localRevRecEndDate=param;
                                    

                               }
                            

                        /**
                        * field for TaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxCodeTracker = false ;

                           public boolean isTaxCodeSpecified(){
                               return localTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxCode(){
                               return localTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxCode
                               */
                               public void setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxCodeTracker = param != null;
                                   
                                            this.localTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate1
                        */

                        
                                    protected double localTaxRate1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate1Tracker = false ;

                           public boolean isTaxRate1Specified(){
                               return localTaxRate1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate1(){
                               return localTaxRate1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate1
                               */
                               public void setTaxRate1(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate1Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate1=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate2
                        */

                        
                                    protected double localTaxRate2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate2Tracker = false ;

                           public boolean isTaxRate2Specified(){
                               return localTaxRate2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate2(){
                               return localTaxRate2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate2
                               */
                               public void setTaxRate2(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate2Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate2=param;
                                    

                               }
                            

                        /**
                        * field for Tax1Amt
                        */

                        
                                    protected double localTax1Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax1AmtTracker = false ;

                           public boolean isTax1AmtSpecified(){
                               return localTax1AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax1Amt(){
                               return localTax1Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax1Amt
                               */
                               public void setTax1Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax1AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax1Amt=param;
                                    

                               }
                            

                        /**
                        * field for GrossAmt
                        */

                        
                                    protected double localGrossAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGrossAmtTracker = false ;

                           public boolean isGrossAmtSpecified(){
                               return localGrossAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGrossAmt(){
                               return localGrossAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GrossAmt
                               */
                               public void setGrossAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGrossAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGrossAmt=param;
                                    

                               }
                            

                        /**
                        * field for IsTaxable
                        */

                        
                                    protected boolean localIsTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTaxableTracker = false ;

                           public boolean isIsTaxableSpecified(){
                               return localIsTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsTaxable(){
                               return localIsTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTaxable
                               */
                               public void setIsTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsTaxableTracker =
                                       true;
                                   
                                            this.localIsTaxable=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertFrom
                        */

                        
                                    protected java.lang.String localGiftCertFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertFromTracker = false ;

                           public boolean isGiftCertFromSpecified(){
                               return localGiftCertFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertFrom(){
                               return localGiftCertFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertFrom
                               */
                               public void setGiftCertFrom(java.lang.String param){
                            localGiftCertFromTracker = param != null;
                                   
                                            this.localGiftCertFrom=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertRecipientName
                        */

                        
                                    protected java.lang.String localGiftCertRecipientName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertRecipientNameTracker = false ;

                           public boolean isGiftCertRecipientNameSpecified(){
                               return localGiftCertRecipientNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertRecipientName(){
                               return localGiftCertRecipientName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertRecipientName
                               */
                               public void setGiftCertRecipientName(java.lang.String param){
                            localGiftCertRecipientNameTracker = param != null;
                                   
                                            this.localGiftCertRecipientName=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertRecipientEmail
                        */

                        
                                    protected java.lang.String localGiftCertRecipientEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertRecipientEmailTracker = false ;

                           public boolean isGiftCertRecipientEmailSpecified(){
                               return localGiftCertRecipientEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertRecipientEmail(){
                               return localGiftCertRecipientEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertRecipientEmail
                               */
                               public void setGiftCertRecipientEmail(java.lang.String param){
                            localGiftCertRecipientEmailTracker = param != null;
                                   
                                            this.localGiftCertRecipientEmail=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertMessage
                        */

                        
                                    protected java.lang.String localGiftCertMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertMessageTracker = false ;

                           public boolean isGiftCertMessageSpecified(){
                               return localGiftCertMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertMessage(){
                               return localGiftCertMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertMessage
                               */
                               public void setGiftCertMessage(java.lang.String param){
                            localGiftCertMessageTracker = param != null;
                                   
                                            this.localGiftCertMessage=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertNumber
                        */

                        
                                    protected java.lang.String localGiftCertNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertNumberTracker = false ;

                           public boolean isGiftCertNumberSpecified(){
                               return localGiftCertNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertNumber(){
                               return localGiftCertNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertNumber
                               */
                               public void setGiftCertNumber(java.lang.String param){
                            localGiftCertNumberTracker = param != null;
                                   
                                            this.localGiftCertNumber=param;
                                    

                               }
                            

                        /**
                        * field for VsoeSopGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup localVsoeSopGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeSopGroupTracker = false ;

                           public boolean isVsoeSopGroupSpecified(){
                               return localVsoeSopGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup getVsoeSopGroup(){
                               return localVsoeSopGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeSopGroup
                               */
                               public void setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup param){
                            localVsoeSopGroupTracker = param != null;
                                   
                                            this.localVsoeSopGroup=param;
                                    

                               }
                            

                        /**
                        * field for VsoeIsEstimate
                        */

                        
                                    protected boolean localVsoeIsEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeIsEstimateTracker = false ;

                           public boolean isVsoeIsEstimateSpecified(){
                               return localVsoeIsEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeIsEstimate(){
                               return localVsoeIsEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeIsEstimate
                               */
                               public void setVsoeIsEstimate(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeIsEstimateTracker =
                                       true;
                                   
                                            this.localVsoeIsEstimate=param;
                                    

                               }
                            

                        /**
                        * field for VsoePrice
                        */

                        
                                    protected double localVsoePrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePriceTracker = false ;

                           public boolean isVsoePriceSpecified(){
                               return localVsoePriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoePrice(){
                               return localVsoePrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePrice
                               */
                               public void setVsoePrice(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoePriceTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoePrice=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAmount
                        */

                        
                                    protected double localVsoeAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAmountTracker = false ;

                           public boolean isVsoeAmountSpecified(){
                               return localVsoeAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoeAmount(){
                               return localVsoeAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAmount
                               */
                               public void setVsoeAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoeAmount=param;
                                    

                               }
                            

                        /**
                        * field for AltSalesAmt
                        */

                        
                                    protected double localAltSalesAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltSalesAmtTracker = false ;

                           public boolean isAltSalesAmtSpecified(){
                               return localAltSalesAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAltSalesAmt(){
                               return localAltSalesAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltSalesAmt
                               */
                               public void setAltSalesAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAltSalesAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAltSalesAmt=param;
                                    

                               }
                            

                        /**
                        * field for TaxAmount
                        */

                        
                                    protected double localTaxAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxAmountTracker = false ;

                           public boolean isTaxAmountSpecified(){
                               return localTaxAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxAmount(){
                               return localTaxAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxAmount
                               */
                               public void setTaxAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxAmount=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAllocation
                        */

                        
                                    protected double localVsoeAllocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAllocationTracker = false ;

                           public boolean isVsoeAllocationSpecified(){
                               return localVsoeAllocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoeAllocation(){
                               return localVsoeAllocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAllocation
                               */
                               public void setVsoeAllocation(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeAllocationTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoeAllocation=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDeferral
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral localVsoeDeferral ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeferralTracker = false ;

                           public boolean isVsoeDeferralSpecified(){
                               return localVsoeDeferralTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral getVsoeDeferral(){
                               return localVsoeDeferral;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDeferral
                               */
                               public void setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral param){
                            localVsoeDeferralTracker = param != null;
                                   
                                            this.localVsoeDeferral=param;
                                    

                               }
                            

                        /**
                        * field for VsoePermitDiscount
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount localVsoePermitDiscount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePermitDiscountTracker = false ;

                           public boolean isVsoePermitDiscountSpecified(){
                               return localVsoePermitDiscountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount getVsoePermitDiscount(){
                               return localVsoePermitDiscount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePermitDiscount
                               */
                               public void setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount param){
                            localVsoePermitDiscountTracker = param != null;
                                   
                                            this.localVsoePermitDiscount=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDelivered
                        */

                        
                                    protected boolean localVsoeDelivered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeliveredTracker = false ;

                           public boolean isVsoeDeliveredSpecified(){
                               return localVsoeDeliveredTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeDelivered(){
                               return localVsoeDelivered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDelivered
                               */
                               public void setVsoeDelivered(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeDeliveredTracker =
                                       true;
                                   
                                            this.localVsoeDelivered=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:customers_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ReturnAuthorizationItem",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ReturnAuthorizationItem",
                           xmlWriter);
                   }

               
                   }
                if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localOrderLineTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderLine", xmlWriter);
                             
                                               if (localOrderLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("orderLine cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLineTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "line", xmlWriter);
                             
                                               if (localLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("line cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityReceivedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityReceived", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityReceived)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityReceived cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityReceived));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityBilledTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityBilled", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityBilled)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityBilled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBilled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnitsTracker){
                                            if (localUnits==null){
                                                 throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                            }
                                           localUnits.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","units"),
                                               xmlWriter);
                                        } if (localInventoryDetailTracker){
                                            if (localInventoryDetail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryDetail cannot be null!!");
                                            }
                                           localInventoryDetail.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","inventoryDetail"),
                                               xmlWriter);
                                        } if (localDescriptionTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSerialNumbersTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "serialNumbers", xmlWriter);
                             

                                          if (localSerialNumbers==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSerialNumbers);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPriceTracker){
                                            if (localPrice==null){
                                                 throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");
                                            }
                                           localPrice.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","price"),
                                               xmlWriter);
                                        } if (localRateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             

                                          if (localRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmountTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOptionsTracker){
                                            if (localOptions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                            }
                                           localOptions.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","options"),
                                               xmlWriter);
                                        } if (localRevRecTermInMonthsTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecTermInMonths", xmlWriter);
                             
                                               if (localRevRecTermInMonths==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("revRecTermInMonths cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecTermInMonths));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeferRevRecTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "deferRevRec", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("deferRevRec cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsClosedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isClosed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isClosed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsClosed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsDropShipmentTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isDropShipment", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isDropShipment cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDropShipment));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCatchUpPeriodTracker){
                                            if (localCatchUpPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("catchUpPeriod cannot be null!!");
                                            }
                                           localCatchUpPeriod.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","catchUpPeriod"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localCostEstimateTypeTracker){
                                            if (localCostEstimateType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                            }
                                           localCostEstimateType.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","costEstimateType"),
                                               xmlWriter);
                                        } if (localCostEstimateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costEstimate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCostEstimate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("costEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxDetailsReferenceTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxDetailsReference", xmlWriter);
                             

                                          if (localTaxDetailsReference==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTaxDetailsReference);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localRevRecScheduleTracker){
                                            if (localRevRecSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                            }
                                           localRevRecSchedule.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecSchedule"),
                                               xmlWriter);
                                        } if (localRevRecStartDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecStartDate", xmlWriter);
                             

                                          if (localRevRecStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecEndDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecEndDate", xmlWriter);
                             

                                          if (localRevRecEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxCodeTracker){
                                            if (localTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                            }
                                           localTaxCode.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxCode"),
                                               xmlWriter);
                                        } if (localTaxRate1Tracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate1", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate1)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate1 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxRate2Tracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate2", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate2)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate2 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax1AmtTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax1Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax1Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax1Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGrossAmtTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "grossAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGrossAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("grossAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsTaxableTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertFromTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertFrom", xmlWriter);
                             

                                          if (localGiftCertFrom==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertFrom cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertFrom);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertRecipientNameTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertRecipientName", xmlWriter);
                             

                                          if (localGiftCertRecipientName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertRecipientName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertRecipientName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertRecipientEmailTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertRecipientEmail", xmlWriter);
                             

                                          if (localGiftCertRecipientEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertRecipientEmail cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertRecipientEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertMessageTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertMessage", xmlWriter);
                             

                                          if (localGiftCertMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertNumberTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertNumber", xmlWriter);
                             

                                          if (localGiftCertNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeSopGroupTracker){
                                            if (localVsoeSopGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                            }
                                           localVsoeSopGroup.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeSopGroup"),
                                               xmlWriter);
                                        } if (localVsoeIsEstimateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeIsEstimate", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeIsEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeIsEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoePriceTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoePrice", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoePrice)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoePrice cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeAmountTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoeAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltSalesAmtTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altSalesAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAltSalesAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("altSalesAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxAmountTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeAllocationTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeAllocation", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoeAllocation)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeAllocation cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAllocation));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeDeferralTracker){
                                            if (localVsoeDeferral==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                            }
                                           localVsoeDeferral.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeDeferral"),
                                               xmlWriter);
                                        } if (localVsoePermitDiscountTracker){
                                            if (localVsoePermitDiscount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                            }
                                           localVsoePermitDiscount.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoePermitDiscount"),
                                               xmlWriter);
                                        } if (localVsoeDeliveredTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeDelivered", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeDelivered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:customers_2017_2.transactions.webservices.netsuite.com")){
                return "ns25";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localOrderLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderLine"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderLine));
                            } if (localLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "line"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                            } if (localQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                            } if (localQuantityReceivedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityReceived"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityReceived));
                            } if (localQuantityBilledTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityBilled"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBilled));
                            } if (localUnitsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "units"));
                            
                            
                                    if (localUnits==null){
                                         throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                    }
                                    elementList.add(localUnits);
                                } if (localInventoryDetailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "inventoryDetail"));
                            
                            
                                    if (localInventoryDetail==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryDetail cannot be null!!");
                                    }
                                    elementList.add(localInventoryDetail);
                                } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localSerialNumbersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "serialNumbers"));
                                 
                                        if (localSerialNumbers != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSerialNumbers));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                        }
                                    } if (localPriceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "price"));
                            
                            
                                    if (localPrice==null){
                                         throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");
                                    }
                                    elementList.add(localPrice);
                                } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                        if (localRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                        }
                                    } if (localAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "amount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                            } if (localOptionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "options"));
                            
                            
                                    if (localOptions==null){
                                         throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                    }
                                    elementList.add(localOptions);
                                } if (localRevRecTermInMonthsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecTermInMonths"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecTermInMonths));
                            } if (localDeferRevRecTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "deferRevRec"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                            } if (localIsClosedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "isClosed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsClosed));
                            } if (localIsDropShipmentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "isDropShipment"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDropShipment));
                            } if (localCatchUpPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "catchUpPeriod"));
                            
                            
                                    if (localCatchUpPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("catchUpPeriod cannot be null!!");
                                    }
                                    elementList.add(localCatchUpPeriod);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localCostEstimateTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "costEstimateType"));
                            
                            
                                    if (localCostEstimateType==null){
                                         throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                    }
                                    elementList.add(localCostEstimateType);
                                } if (localCostEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "costEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                            } if (localTaxDetailsReferenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsReference"));
                                 
                                        if (localTaxDetailsReference != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsReference));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                        }
                                    } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localRevRecScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecSchedule"));
                            
                            
                                    if (localRevRecSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                    }
                                    elementList.add(localRevRecSchedule);
                                } if (localRevRecStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecStartDate"));
                                 
                                        if (localRevRecStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                        }
                                    } if (localRevRecEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecEndDate"));
                                 
                                        if (localRevRecEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                        }
                                    } if (localTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxCode"));
                            
                            
                                    if (localTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                    }
                                    elementList.add(localTaxCode);
                                } if (localTaxRate1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate1"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                            } if (localTaxRate2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate2"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                            } if (localTax1AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax1Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                            } if (localGrossAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "grossAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                            } if (localIsTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "isTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                            } if (localGiftCertFromTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertFrom"));
                                 
                                        if (localGiftCertFrom != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertFrom));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertFrom cannot be null!!");
                                        }
                                    } if (localGiftCertRecipientNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertRecipientName"));
                                 
                                        if (localGiftCertRecipientName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertRecipientName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertRecipientName cannot be null!!");
                                        }
                                    } if (localGiftCertRecipientEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertRecipientEmail"));
                                 
                                        if (localGiftCertRecipientEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertRecipientEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertRecipientEmail cannot be null!!");
                                        }
                                    } if (localGiftCertMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertMessage"));
                                 
                                        if (localGiftCertMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertMessage cannot be null!!");
                                        }
                                    } if (localGiftCertNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertNumber"));
                                 
                                        if (localGiftCertNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertNumber cannot be null!!");
                                        }
                                    } if (localVsoeSopGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeSopGroup"));
                            
                            
                                    if (localVsoeSopGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                    }
                                    elementList.add(localVsoeSopGroup);
                                } if (localVsoeIsEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeIsEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeIsEstimate));
                            } if (localVsoePriceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoePrice"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                            } if (localVsoeAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAmount));
                            } if (localAltSalesAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "altSalesAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesAmt));
                            } if (localTaxAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                            } if (localVsoeAllocationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeAllocation"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAllocation));
                            } if (localVsoeDeferralTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeDeferral"));
                            
                            
                                    if (localVsoeDeferral==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                    }
                                    elementList.add(localVsoeDeferral);
                                } if (localVsoePermitDiscountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoePermitDiscount"));
                            
                            
                                    if (localVsoePermitDiscount==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                    }
                                    elementList.add(localVsoePermitDiscount);
                                } if (localVsoeDeliveredTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeDelivered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                            } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ReturnAuthorizationItem parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ReturnAuthorizationItem object =
                new ReturnAuthorizationItem();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ReturnAuthorizationItem".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ReturnAuthorizationItem)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","orderLine").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderLine" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOrderLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","line").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"line" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","quantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","quantityReceived").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityReceived" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityReceived(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityReceived(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","quantityBilled").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityBilled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityBilled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityBilled(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","units").equals(reader.getName())){
                                
                                                object.setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","inventoryDetail").equals(reader.getName())){
                                
                                                object.setInventoryDetail(com.netsuite.webservices.platform.common_2017_2.InventoryDetail.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","serialNumbers").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"serialNumbers" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSerialNumbers(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","price").equals(reader.getName())){
                                
                                                object.setPrice(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","options").equals(reader.getName())){
                                
                                                object.setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecTermInMonths").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecTermInMonths" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecTermInMonths(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRevRecTermInMonths(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","deferRevRec").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"deferRevRec" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeferRevRec(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","isClosed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isClosed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsClosed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","isDropShipment").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isDropShipment" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsDropShipment(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","catchUpPeriod").equals(reader.getName())){
                                
                                                object.setCatchUpPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","costEstimateType").equals(reader.getName())){
                                
                                                object.setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","costEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCostEstimate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxDetailsReference").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxDetailsReference" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxDetailsReference(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecSchedule").equals(reader.getName())){
                                
                                                object.setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxCode").equals(reader.getName())){
                                
                                                object.setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxRate1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate1(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxRate2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate2(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","tax1Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax1Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax1Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax1Amt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","grossAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"grossAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGrossAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGrossAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","isTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertFrom").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertFrom" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertFrom(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertRecipientName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertRecipientName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertRecipientName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertRecipientEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertRecipientEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertRecipientEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeSopGroup").equals(reader.getName())){
                                
                                                object.setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeIsEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeIsEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeIsEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoePrice").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoePrice" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoePrice(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoePrice(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoeAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","altSalesAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altSalesAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltSalesAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAltSalesAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeAllocation").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeAllocation" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeAllocation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoeAllocation(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeDeferral").equals(reader.getName())){
                                
                                                object.setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoePermitDiscount").equals(reader.getName())){
                                
                                                object.setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeDelivered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeDelivered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeDelivered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    