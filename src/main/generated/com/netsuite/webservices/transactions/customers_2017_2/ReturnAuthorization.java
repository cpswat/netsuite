
/**
 * ReturnAuthorization.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.customers_2017_2;
            

            /**
            *  ReturnAuthorization bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ReturnAuthorization extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ReturnAuthorization
                Namespace URI = urn:customers_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns25
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for Nexus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localNexus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNexusTracker = false ;

                           public boolean isNexusSpecified(){
                               return localNexusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getNexus(){
                               return localNexus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nexus
                               */
                               public void setNexus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localNexusTracker = param != null;
                                   
                                            this.localNexus=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryTaxRegNum
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiaryTaxRegNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTaxRegNumTracker = false ;

                           public boolean isSubsidiaryTaxRegNumSpecified(){
                               return localSubsidiaryTaxRegNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiaryTaxRegNum(){
                               return localSubsidiaryTaxRegNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryTaxRegNum
                               */
                               public void setSubsidiaryTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTaxRegNumTracker = param != null;
                                   
                                            this.localSubsidiaryTaxRegNum=param;
                                    

                               }
                            

                        /**
                        * field for TaxRegOverride
                        */

                        
                                    protected boolean localTaxRegOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRegOverrideTracker = false ;

                           public boolean isTaxRegOverrideSpecified(){
                               return localTaxRegOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTaxRegOverride(){
                               return localTaxRegOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRegOverride
                               */
                               public void setTaxRegOverride(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRegOverrideTracker =
                                       true;
                                   
                                            this.localTaxRegOverride=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsOverride
                        */

                        
                                    protected boolean localTaxDetailsOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsOverrideTracker = false ;

                           public boolean isTaxDetailsOverrideSpecified(){
                               return localTaxDetailsOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTaxDetailsOverride(){
                               return localTaxDetailsOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsOverride
                               */
                               public void setTaxDetailsOverride(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxDetailsOverrideTracker =
                                       true;
                                   
                                            this.localTaxDetailsOverride=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for VatRegNum
                        */

                        
                                    protected java.lang.String localVatRegNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVatRegNumTracker = false ;

                           public boolean isVatRegNumSpecified(){
                               return localVatRegNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getVatRegNum(){
                               return localVatRegNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VatRegNum
                               */
                               public void setVatRegNum(java.lang.String param){
                            localVatRegNumTracker = param != null;
                                   
                                            this.localVatRegNum=param;
                                    

                               }
                            

                        /**
                        * field for TranDate
                        */

                        
                                    protected java.util.Calendar localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTranDate(){
                               return localTranDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranDate
                               */
                               public void setTranDate(java.util.Calendar param){
                            localTranDateTracker = param != null;
                                   
                                            this.localTranDate=param;
                                    

                               }
                            

                        /**
                        * field for TranId
                        */

                        
                                    protected java.lang.String localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTranId(){
                               return localTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranId
                               */
                               public void setTranId(java.lang.String param){
                            localTranIdTracker = param != null;
                                   
                                            this.localTranId=param;
                                    

                               }
                            

                        /**
                        * field for EntityTaxRegNum
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntityTaxRegNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTaxRegNumTracker = false ;

                           public boolean isEntityTaxRegNumSpecified(){
                               return localEntityTaxRegNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntityTaxRegNum(){
                               return localEntityTaxRegNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityTaxRegNum
                               */
                               public void setEntityTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTaxRegNumTracker = param != null;
                                   
                                            this.localEntityTaxRegNum=param;
                                    

                               }
                            

                        /**
                        * field for Source
                        */

                        
                                    protected java.lang.String localSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceTracker = false ;

                           public boolean isSourceSpecified(){
                               return localSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSource(){
                               return localSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Source
                               */
                               public void setSource(java.lang.String param){
                            localSourceTracker = param != null;
                                   
                                            this.localSource=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for IntercoTransaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIntercoTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoTransactionTracker = false ;

                           public boolean isIntercoTransactionSpecified(){
                               return localIntercoTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIntercoTransaction(){
                               return localIntercoTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoTransaction
                               */
                               public void setIntercoTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIntercoTransactionTracker = param != null;
                                   
                                            this.localIntercoTransaction=param;
                                    

                               }
                            

                        /**
                        * field for IntercoStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus localIntercoStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoStatusTracker = false ;

                           public boolean isIntercoStatusSpecified(){
                               return localIntercoStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus getIntercoStatus(){
                               return localIntercoStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoStatus
                               */
                               public void setIntercoStatus(com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus param){
                            localIntercoStatusTracker = param != null;
                                   
                                            this.localIntercoStatus=param;
                                    

                               }
                            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for Partner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPartner(){
                               return localPartner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Partner
                               */
                               public void setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPartnerTracker = param != null;
                                   
                                            this.localPartner=param;
                                    

                               }
                            

                        /**
                        * field for OtherRefNum
                        */

                        
                                    protected java.lang.String localOtherRefNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOtherRefNumTracker = false ;

                           public boolean isOtherRefNumSpecified(){
                               return localOtherRefNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOtherRefNum(){
                               return localOtherRefNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OtherRefNum
                               */
                               public void setOtherRefNum(java.lang.String param){
                            localOtherRefNumTracker = param != null;
                                   
                                            this.localOtherRefNum=param;
                                    

                               }
                            

                        /**
                        * field for LeadSource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLeadSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceTracker = false ;

                           public boolean isLeadSourceSpecified(){
                               return localLeadSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLeadSource(){
                               return localLeadSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadSource
                               */
                               public void setLeadSource(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLeadSourceTracker = param != null;
                                   
                                            this.localLeadSource=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for DrAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDrAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDrAccountTracker = false ;

                           public boolean isDrAccountSpecified(){
                               return localDrAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDrAccount(){
                               return localDrAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DrAccount
                               */
                               public void setDrAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDrAccountTracker = param != null;
                                   
                                            this.localDrAccount=param;
                                    

                               }
                            

                        /**
                        * field for FxAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localFxAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxAccountTracker = false ;

                           public boolean isFxAccountSpecified(){
                               return localFxAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getFxAccount(){
                               return localFxAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxAccount
                               */
                               public void setFxAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localFxAccountTracker = param != null;
                                   
                                            this.localFxAccount=param;
                                    

                               }
                            

                        /**
                        * field for SalesEffectiveDate
                        */

                        
                                    protected java.util.Calendar localSalesEffectiveDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesEffectiveDateTracker = false ;

                           public boolean isSalesEffectiveDateSpecified(){
                               return localSalesEffectiveDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getSalesEffectiveDate(){
                               return localSalesEffectiveDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesEffectiveDate
                               */
                               public void setSalesEffectiveDate(java.util.Calendar param){
                            localSalesEffectiveDateTracker = param != null;
                                   
                                            this.localSalesEffectiveDate=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromTracker = false ;

                           public boolean isCreatedFromSpecified(){
                               return localCreatedFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedFrom(){
                               return localCreatedFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFrom
                               */
                               public void setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedFromTracker = param != null;
                                   
                                            this.localCreatedFrom=param;
                                    

                               }
                            

                        /**
                        * field for RevRecSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleTracker = false ;

                           public boolean isRevRecScheduleSpecified(){
                               return localRevRecScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecSchedule(){
                               return localRevRecSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecSchedule
                               */
                               public void setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecScheduleTracker = param != null;
                                   
                                            this.localRevRecSchedule=param;
                                    

                               }
                            

                        /**
                        * field for TotalCostEstimate
                        */

                        
                                    protected double localTotalCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalCostEstimateTracker = false ;

                           public boolean isTotalCostEstimateSpecified(){
                               return localTotalCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTotalCostEstimate(){
                               return localTotalCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TotalCostEstimate
                               */
                               public void setTotalCostEstimate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTotalCostEstimateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTotalCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for EstGrossProfit
                        */

                        
                                    protected double localEstGrossProfit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstGrossProfitTracker = false ;

                           public boolean isEstGrossProfitSpecified(){
                               return localEstGrossProfitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstGrossProfit(){
                               return localEstGrossProfit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstGrossProfit
                               */
                               public void setEstGrossProfit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstGrossProfitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstGrossProfit=param;
                                    

                               }
                            

                        /**
                        * field for EstGrossProfitPercent
                        */

                        
                                    protected double localEstGrossProfitPercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstGrossProfitPercentTracker = false ;

                           public boolean isEstGrossProfitPercentSpecified(){
                               return localEstGrossProfitPercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstGrossProfitPercent(){
                               return localEstGrossProfitPercent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstGrossProfitPercent
                               */
                               public void setEstGrossProfitPercent(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstGrossProfitPercentTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstGrossProfitPercent=param;
                                    

                               }
                            

                        /**
                        * field for RevRecStartDate
                        */

                        
                                    protected java.util.Calendar localRevRecStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecStartDateTracker = false ;

                           public boolean isRevRecStartDateSpecified(){
                               return localRevRecStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecStartDate(){
                               return localRevRecStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecStartDate
                               */
                               public void setRevRecStartDate(java.util.Calendar param){
                            localRevRecStartDateTracker = param != null;
                                   
                                            this.localRevRecStartDate=param;
                                    

                               }
                            

                        /**
                        * field for RevRecEndDate
                        */

                        
                                    protected java.util.Calendar localRevRecEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecEndDateTracker = false ;

                           public boolean isRevRecEndDateSpecified(){
                               return localRevRecEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecEndDate(){
                               return localRevRecEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecEndDate
                               */
                               public void setRevRecEndDate(java.util.Calendar param){
                            localRevRecEndDateTracker = param != null;
                                   
                                            this.localRevRecEndDate=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeCommission
                        */

                        
                                    protected boolean localExcludeCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeCommissionTracker = false ;

                           public boolean isExcludeCommissionSpecified(){
                               return localExcludeCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExcludeCommission(){
                               return localExcludeCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeCommission
                               */
                               public void setExcludeCommission(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExcludeCommissionTracker =
                                       true;
                                   
                                            this.localExcludeCommission=param;
                                    

                               }
                            

                        /**
                        * field for ExchangeRate
                        */

                        
                                    protected double localExchangeRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExchangeRateTracker = false ;

                           public boolean isExchangeRateSpecified(){
                               return localExchangeRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getExchangeRate(){
                               return localExchangeRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExchangeRate
                               */
                               public void setExchangeRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localExchangeRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localExchangeRate=param;
                                    

                               }
                            

                        /**
                        * field for CurrencyName
                        */

                        
                                    protected java.lang.String localCurrencyName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyNameTracker = false ;

                           public boolean isCurrencyNameSpecified(){
                               return localCurrencyNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCurrencyName(){
                               return localCurrencyName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CurrencyName
                               */
                               public void setCurrencyName(java.lang.String param){
                            localCurrencyNameTracker = param != null;
                                   
                                            this.localCurrencyName=param;
                                    

                               }
                            

                        /**
                        * field for DiscountItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDiscountItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDiscountItemTracker = false ;

                           public boolean isDiscountItemSpecified(){
                               return localDiscountItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDiscountItem(){
                               return localDiscountItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DiscountItem
                               */
                               public void setDiscountItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDiscountItemTracker = param != null;
                                   
                                            this.localDiscountItem=param;
                                    

                               }
                            

                        /**
                        * field for DiscountRate
                        */

                        
                                    protected java.lang.String localDiscountRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDiscountRateTracker = false ;

                           public boolean isDiscountRateSpecified(){
                               return localDiscountRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDiscountRate(){
                               return localDiscountRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DiscountRate
                               */
                               public void setDiscountRate(java.lang.String param){
                            localDiscountRateTracker = param != null;
                                   
                                            this.localDiscountRate=param;
                                    

                               }
                            

                        /**
                        * field for TaxItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxItemTracker = false ;

                           public boolean isTaxItemSpecified(){
                               return localTaxItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxItem(){
                               return localTaxItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxItem
                               */
                               public void setTaxItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxItemTracker = param != null;
                                   
                                            this.localTaxItem=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate
                        */

                        
                                    protected double localTaxRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRateTracker = false ;

                           public boolean isTaxRateSpecified(){
                               return localTaxRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate(){
                               return localTaxRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate
                               */
                               public void setTaxRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate=param;
                                    

                               }
                            

                        /**
                        * field for ToBePrinted
                        */

                        
                                    protected boolean localToBePrinted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToBePrintedTracker = false ;

                           public boolean isToBePrintedSpecified(){
                               return localToBePrintedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getToBePrinted(){
                               return localToBePrinted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToBePrinted
                               */
                               public void setToBePrinted(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localToBePrintedTracker =
                                       true;
                                   
                                            this.localToBePrinted=param;
                                    

                               }
                            

                        /**
                        * field for ToBeEmailed
                        */

                        
                                    protected boolean localToBeEmailed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToBeEmailedTracker = false ;

                           public boolean isToBeEmailedSpecified(){
                               return localToBeEmailedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getToBeEmailed(){
                               return localToBeEmailed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToBeEmailed
                               */
                               public void setToBeEmailed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localToBeEmailedTracker =
                                       true;
                                   
                                            this.localToBeEmailed=param;
                                    

                               }
                            

                        /**
                        * field for ToBeFaxed
                        */

                        
                                    protected boolean localToBeFaxed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToBeFaxedTracker = false ;

                           public boolean isToBeFaxedSpecified(){
                               return localToBeFaxedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getToBeFaxed(){
                               return localToBeFaxed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToBeFaxed
                               */
                               public void setToBeFaxed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localToBeFaxedTracker =
                                       true;
                                   
                                            this.localToBeFaxed=param;
                                    

                               }
                            

                        /**
                        * field for MessageSel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localMessageSel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageSelTracker = false ;

                           public boolean isMessageSelSpecified(){
                               return localMessageSelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getMessageSel(){
                               return localMessageSel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessageSel
                               */
                               public void setMessageSel(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localMessageSelTracker = param != null;
                                   
                                            this.localMessageSel=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected java.lang.String localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(java.lang.String param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for BillingAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.Address localBillingAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingAddressTracker = false ;

                           public boolean isBillingAddressSpecified(){
                               return localBillingAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.Address
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.Address getBillingAddress(){
                               return localBillingAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingAddress
                               */
                               public void setBillingAddress(com.netsuite.webservices.platform.common_2017_2.Address param){
                            localBillingAddressTracker = param != null;
                                   
                                            this.localBillingAddress=param;
                                    

                               }
                            

                        /**
                        * field for BillAddressList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillAddressList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillAddressListTracker = false ;

                           public boolean isBillAddressListSpecified(){
                               return localBillAddressListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillAddressList(){
                               return localBillAddressList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillAddressList
                               */
                               public void setBillAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillAddressListTracker = param != null;
                                   
                                            this.localBillAddressList=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddressList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipAddressList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressListTracker = false ;

                           public boolean isShipAddressListSpecified(){
                               return localShipAddressListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipAddressList(){
                               return localShipAddressList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddressList
                               */
                               public void setShipAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipAddressListTracker = param != null;
                                   
                                            this.localShipAddressList=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddress
                        */

                        
                                    protected java.lang.String localShipAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressTracker = false ;

                           public boolean isShipAddressSpecified(){
                               return localShipAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShipAddress(){
                               return localShipAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddress
                               */
                               public void setShipAddress(java.lang.String param){
                            localShipAddressTracker = param != null;
                                   
                                            this.localShipAddress=param;
                                    

                               }
                            

                        /**
                        * field for SalesGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesGroupTracker = false ;

                           public boolean isSalesGroupSpecified(){
                               return localSalesGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesGroup(){
                               return localSalesGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesGroup
                               */
                               public void setSalesGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesGroupTracker = param != null;
                                   
                                            this.localSalesGroup=param;
                                    

                               }
                            

                        /**
                        * field for SyncSalesTeams
                        */

                        
                                    protected boolean localSyncSalesTeams ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSyncSalesTeamsTracker = false ;

                           public boolean isSyncSalesTeamsSpecified(){
                               return localSyncSalesTeamsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSyncSalesTeams(){
                               return localSyncSalesTeams;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SyncSalesTeams
                               */
                               public void setSyncSalesTeams(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSyncSalesTeamsTracker =
                                       true;
                                   
                                            this.localSyncSalesTeams=param;
                                    

                               }
                            

                        /**
                        * field for RevenueStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus localRevenueStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueStatusTracker = false ;

                           public boolean isRevenueStatusSpecified(){
                               return localRevenueStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus getRevenueStatus(){
                               return localRevenueStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueStatus
                               */
                               public void setRevenueStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus param){
                            localRevenueStatusTracker = param != null;
                                   
                                            this.localRevenueStatus=param;
                                    

                               }
                            

                        /**
                        * field for RecognizedRevenue
                        */

                        
                                    protected double localRecognizedRevenue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecognizedRevenueTracker = false ;

                           public boolean isRecognizedRevenueSpecified(){
                               return localRecognizedRevenueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRecognizedRevenue(){
                               return localRecognizedRevenue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecognizedRevenue
                               */
                               public void setRecognizedRevenue(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRecognizedRevenueTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRecognizedRevenue=param;
                                    

                               }
                            

                        /**
                        * field for DeferredRevenue
                        */

                        
                                    protected double localDeferredRevenue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferredRevenueTracker = false ;

                           public boolean isDeferredRevenueSpecified(){
                               return localDeferredRevenueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDeferredRevenue(){
                               return localDeferredRevenue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferredRevenue
                               */
                               public void setDeferredRevenue(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDeferredRevenueTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDeferredRevenue=param;
                                    

                               }
                            

                        /**
                        * field for RevRecOnRevCommitment
                        */

                        
                                    protected boolean localRevRecOnRevCommitment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecOnRevCommitmentTracker = false ;

                           public boolean isRevRecOnRevCommitmentSpecified(){
                               return localRevRecOnRevCommitmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRevRecOnRevCommitment(){
                               return localRevRecOnRevCommitment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecOnRevCommitment
                               */
                               public void setRevRecOnRevCommitment(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRevRecOnRevCommitmentTracker =
                                       true;
                                   
                                            this.localRevRecOnRevCommitment=param;
                                    

                               }
                            

                        /**
                        * field for RevCommitStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus localRevCommitStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevCommitStatusTracker = false ;

                           public boolean isRevCommitStatusSpecified(){
                               return localRevCommitStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus getRevCommitStatus(){
                               return localRevCommitStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevCommitStatus
                               */
                               public void setRevCommitStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus param){
                            localRevCommitStatusTracker = param != null;
                                   
                                            this.localRevCommitStatus=param;
                                    

                               }
                            

                        /**
                        * field for PaymentMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPaymentMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentMethodTracker = false ;

                           public boolean isPaymentMethodSpecified(){
                               return localPaymentMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPaymentMethod(){
                               return localPaymentMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentMethod
                               */
                               public void setPaymentMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPaymentMethodTracker = param != null;
                                   
                                            this.localPaymentMethod=param;
                                    

                               }
                            

                        /**
                        * field for CreditCard
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreditCard ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditCardTracker = false ;

                           public boolean isCreditCardSpecified(){
                               return localCreditCardTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreditCard(){
                               return localCreditCard;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditCard
                               */
                               public void setCreditCard(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreditCardTracker = param != null;
                                   
                                            this.localCreditCard=param;
                                    

                               }
                            

                        /**
                        * field for CcNumber
                        */

                        
                                    protected java.lang.String localCcNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcNumberTracker = false ;

                           public boolean isCcNumberSpecified(){
                               return localCcNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcNumber(){
                               return localCcNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcNumber
                               */
                               public void setCcNumber(java.lang.String param){
                            localCcNumberTracker = param != null;
                                   
                                            this.localCcNumber=param;
                                    

                               }
                            

                        /**
                        * field for AltSalesTotal
                        */

                        
                                    protected double localAltSalesTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltSalesTotalTracker = false ;

                           public boolean isAltSalesTotalSpecified(){
                               return localAltSalesTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAltSalesTotal(){
                               return localAltSalesTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltSalesTotal
                               */
                               public void setAltSalesTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAltSalesTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAltSalesTotal=param;
                                    

                               }
                            

                        /**
                        * field for CcExpireDate
                        */

                        
                                    protected java.util.Calendar localCcExpireDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcExpireDateTracker = false ;

                           public boolean isCcExpireDateSpecified(){
                               return localCcExpireDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCcExpireDate(){
                               return localCcExpireDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcExpireDate
                               */
                               public void setCcExpireDate(java.util.Calendar param){
                            localCcExpireDateTracker = param != null;
                                   
                                            this.localCcExpireDate=param;
                                    

                               }
                            

                        /**
                        * field for CcName
                        */

                        
                                    protected java.lang.String localCcName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcNameTracker = false ;

                           public boolean isCcNameSpecified(){
                               return localCcNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcName(){
                               return localCcName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcName
                               */
                               public void setCcName(java.lang.String param){
                            localCcNameTracker = param != null;
                                   
                                            this.localCcName=param;
                                    

                               }
                            

                        /**
                        * field for CcStreet
                        */

                        
                                    protected java.lang.String localCcStreet ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcStreetTracker = false ;

                           public boolean isCcStreetSpecified(){
                               return localCcStreetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcStreet(){
                               return localCcStreet;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcStreet
                               */
                               public void setCcStreet(java.lang.String param){
                            localCcStreetTracker = param != null;
                                   
                                            this.localCcStreet=param;
                                    

                               }
                            

                        /**
                        * field for CcZipCode
                        */

                        
                                    protected java.lang.String localCcZipCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcZipCodeTracker = false ;

                           public boolean isCcZipCodeSpecified(){
                               return localCcZipCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcZipCode(){
                               return localCcZipCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcZipCode
                               */
                               public void setCcZipCode(java.lang.String param){
                            localCcZipCodeTracker = param != null;
                                   
                                            this.localCcZipCode=param;
                                    

                               }
                            

                        /**
                        * field for CcApproved
                        */

                        
                                    protected boolean localCcApproved ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcApprovedTracker = false ;

                           public boolean isCcApprovedSpecified(){
                               return localCcApprovedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCcApproved(){
                               return localCcApproved;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcApproved
                               */
                               public void setCcApproved(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localCcApprovedTracker =
                                       true;
                                   
                                            this.localCcApproved=param;
                                    

                               }
                            

                        /**
                        * field for PnRefNum
                        */

                        
                                    protected java.lang.String localPnRefNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPnRefNumTracker = false ;

                           public boolean isPnRefNumSpecified(){
                               return localPnRefNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPnRefNum(){
                               return localPnRefNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PnRefNum
                               */
                               public void setPnRefNum(java.lang.String param){
                            localPnRefNumTracker = param != null;
                                   
                                            this.localPnRefNum=param;
                                    

                               }
                            

                        /**
                        * field for SubTotal
                        */

                        
                                    protected double localSubTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubTotalTracker = false ;

                           public boolean isSubTotalSpecified(){
                               return localSubTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSubTotal(){
                               return localSubTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubTotal
                               */
                               public void setSubTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSubTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSubTotal=param;
                                    

                               }
                            

                        /**
                        * field for DiscountTotal
                        */

                        
                                    protected double localDiscountTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDiscountTotalTracker = false ;

                           public boolean isDiscountTotalSpecified(){
                               return localDiscountTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDiscountTotal(){
                               return localDiscountTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DiscountTotal
                               */
                               public void setDiscountTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDiscountTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDiscountTotal=param;
                                    

                               }
                            

                        /**
                        * field for Total
                        */

                        
                                    protected double localTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalTracker = false ;

                           public boolean isTotalSpecified(){
                               return localTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTotal(){
                               return localTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Total
                               */
                               public void setTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTotal=param;
                                    

                               }
                            

                        /**
                        * field for CreditCardProcessor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreditCardProcessor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditCardProcessorTracker = false ;

                           public boolean isCreditCardProcessorSpecified(){
                               return localCreditCardProcessorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreditCardProcessor(){
                               return localCreditCardProcessor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditCardProcessor
                               */
                               public void setCreditCardProcessor(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreditCardProcessorTracker = param != null;
                                   
                                            this.localCreditCardProcessor=param;
                                    

                               }
                            

                        /**
                        * field for PayPalAuthId
                        */

                        
                                    protected java.lang.String localPayPalAuthId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayPalAuthIdTracker = false ;

                           public boolean isPayPalAuthIdSpecified(){
                               return localPayPalAuthIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPayPalAuthId(){
                               return localPayPalAuthId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayPalAuthId
                               */
                               public void setPayPalAuthId(java.lang.String param){
                            localPayPalAuthIdTracker = param != null;
                                   
                                            this.localPayPalAuthId=param;
                                    

                               }
                            

                        /**
                        * field for PayPalProcess
                        */

                        
                                    protected boolean localPayPalProcess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayPalProcessTracker = false ;

                           public boolean isPayPalProcessSpecified(){
                               return localPayPalProcessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPayPalProcess(){
                               return localPayPalProcess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayPalProcess
                               */
                               public void setPayPalProcess(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPayPalProcessTracker =
                                       true;
                                   
                                            this.localPayPalProcess=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected java.lang.String localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(java.lang.String param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected java.lang.String localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(java.lang.String param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for DebitCardIssueNo
                        */

                        
                                    protected java.lang.String localDebitCardIssueNo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDebitCardIssueNoTracker = false ;

                           public boolean isDebitCardIssueNoSpecified(){
                               return localDebitCardIssueNoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDebitCardIssueNo(){
                               return localDebitCardIssueNo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DebitCardIssueNo
                               */
                               public void setDebitCardIssueNo(java.lang.String param){
                            localDebitCardIssueNoTracker = param != null;
                                   
                                            this.localDebitCardIssueNo=param;
                                    

                               }
                            

                        /**
                        * field for IsTaxable
                        */

                        
                                    protected boolean localIsTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTaxableTracker = false ;

                           public boolean isIsTaxableSpecified(){
                               return localIsTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsTaxable(){
                               return localIsTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTaxable
                               */
                               public void setIsTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsTaxableTracker =
                                       true;
                                   
                                            this.localIsTaxable=param;
                                    

                               }
                            

                        /**
                        * field for PromoCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPromoCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPromoCodeTracker = false ;

                           public boolean isPromoCodeSpecified(){
                               return localPromoCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPromoCode(){
                               return localPromoCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PromoCode
                               */
                               public void setPromoCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPromoCodeTracker = param != null;
                                   
                                            this.localPromoCode=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected java.lang.String localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(java.lang.String param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for TaxTotal
                        */

                        
                                    protected double localTaxTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxTotalTracker = false ;

                           public boolean isTaxTotalSpecified(){
                               return localTaxTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxTotal(){
                               return localTaxTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxTotal
                               */
                               public void setTaxTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxTotal=param;
                                    

                               }
                            

                        /**
                        * field for Tax2Total
                        */

                        
                                    protected double localTax2Total ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax2TotalTracker = false ;

                           public boolean isTax2TotalSpecified(){
                               return localTax2TotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax2Total(){
                               return localTax2Total;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax2Total
                               */
                               public void setTax2Total(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax2TotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax2Total=param;
                                    

                               }
                            

                        /**
                        * field for ValidFrom
                        */

                        
                                    protected java.util.Calendar localValidFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localValidFromTracker = false ;

                           public boolean isValidFromSpecified(){
                               return localValidFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getValidFrom(){
                               return localValidFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ValidFrom
                               */
                               public void setValidFrom(java.util.Calendar param){
                            localValidFromTracker = param != null;
                                   
                                            this.localValidFrom=param;
                                    

                               }
                            

                        /**
                        * field for OrderStatus
                        */

                        
                                    protected com.netsuite.webservices.transactions.customers_2017_2.types.ReturnAuthorizationOrderStatus localOrderStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderStatusTracker = false ;

                           public boolean isOrderStatusSpecified(){
                               return localOrderStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.customers_2017_2.types.ReturnAuthorizationOrderStatus
                           */
                           public  com.netsuite.webservices.transactions.customers_2017_2.types.ReturnAuthorizationOrderStatus getOrderStatus(){
                               return localOrderStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderStatus
                               */
                               public void setOrderStatus(com.netsuite.webservices.transactions.customers_2017_2.types.ReturnAuthorizationOrderStatus param){
                            localOrderStatusTracker = param != null;
                                   
                                            this.localOrderStatus=param;
                                    

                               }
                            

                        /**
                        * field for SalesRep
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepTracker = false ;

                           public boolean isSalesRepSpecified(){
                               return localSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesRep(){
                               return localSalesRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRep
                               */
                               public void setSalesRep(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesRepTracker = param != null;
                                   
                                            this.localSalesRep=param;
                                    

                               }
                            

                        /**
                        * field for GiftCert
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localGiftCert ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertTracker = false ;

                           public boolean isGiftCertSpecified(){
                               return localGiftCertTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getGiftCert(){
                               return localGiftCert;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCert
                               */
                               public void setGiftCert(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localGiftCertTracker = param != null;
                                   
                                            this.localGiftCert=param;
                                    

                               }
                            

                        /**
                        * field for ContribPct
                        */

                        
                                    protected java.lang.String localContribPct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContribPctTracker = false ;

                           public boolean isContribPctSpecified(){
                               return localContribPctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getContribPct(){
                               return localContribPct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContribPct
                               */
                               public void setContribPct(java.lang.String param){
                            localContribPctTracker = param != null;
                                   
                                            this.localContribPct=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertTotal
                        */

                        
                                    protected double localGiftCertTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertTotalTracker = false ;

                           public boolean isGiftCertTotalSpecified(){
                               return localGiftCertTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGiftCertTotal(){
                               return localGiftCertTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertTotal
                               */
                               public void setGiftCertTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGiftCertTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGiftCertTotal=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertApplied
                        */

                        
                                    protected double localGiftCertApplied ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertAppliedTracker = false ;

                           public boolean isGiftCertAppliedSpecified(){
                               return localGiftCertAppliedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGiftCertApplied(){
                               return localGiftCertApplied;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertApplied
                               */
                               public void setGiftCertApplied(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGiftCertAppliedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGiftCertApplied=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertAvailable
                        */

                        
                                    protected double localGiftCertAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertAvailableTracker = false ;

                           public boolean isGiftCertAvailableSpecified(){
                               return localGiftCertAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGiftCertAvailable(){
                               return localGiftCertAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertAvailable
                               */
                               public void setGiftCertAvailable(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGiftCertAvailableTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGiftCertAvailable=param;
                                    

                               }
                            

                        /**
                        * field for TranIsVsoeBundle
                        */

                        
                                    protected boolean localTranIsVsoeBundle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIsVsoeBundleTracker = false ;

                           public boolean isTranIsVsoeBundleSpecified(){
                               return localTranIsVsoeBundleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTranIsVsoeBundle(){
                               return localTranIsVsoeBundle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranIsVsoeBundle
                               */
                               public void setTranIsVsoeBundle(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTranIsVsoeBundleTracker =
                                       true;
                                   
                                            this.localTranIsVsoeBundle=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAutoCalc
                        */

                        
                                    protected boolean localVsoeAutoCalc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAutoCalcTracker = false ;

                           public boolean isVsoeAutoCalcSpecified(){
                               return localVsoeAutoCalcTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeAutoCalc(){
                               return localVsoeAutoCalc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAutoCalc
                               */
                               public void setVsoeAutoCalc(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeAutoCalcTracker =
                                       true;
                                   
                                            this.localVsoeAutoCalc=param;
                                    

                               }
                            

                        /**
                        * field for SyncPartnerTeams
                        */

                        
                                    protected boolean localSyncPartnerTeams ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSyncPartnerTeamsTracker = false ;

                           public boolean isSyncPartnerTeamsSpecified(){
                               return localSyncPartnerTeamsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSyncPartnerTeams(){
                               return localSyncPartnerTeams;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SyncPartnerTeams
                               */
                               public void setSyncPartnerTeams(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSyncPartnerTeamsTracker =
                                       true;
                                   
                                            this.localSyncPartnerTeams=param;
                                    

                               }
                            

                        /**
                        * field for ItemList
                        */

                        
                                    protected com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItemList localItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemListTracker = false ;

                           public boolean isItemListSpecified(){
                               return localItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItemList
                           */
                           public  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItemList getItemList(){
                               return localItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemList
                               */
                               public void setItemList(com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItemList param){
                            localItemListTracker = param != null;
                                   
                                            this.localItemList=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamList
                        */

                        
                                    protected com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeamList localSalesTeamList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamListTracker = false ;

                           public boolean isSalesTeamListSpecified(){
                               return localSalesTeamListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeamList
                           */
                           public  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeamList getSalesTeamList(){
                               return localSalesTeamList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamList
                               */
                               public void setSalesTeamList(com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeamList param){
                            localSalesTeamListTracker = param != null;
                                   
                                            this.localSalesTeamList=param;
                                    

                               }
                            

                        /**
                        * field for PartnersList
                        */

                        
                                    protected com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationPartnersList localPartnersList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnersListTracker = false ;

                           public boolean isPartnersListSpecified(){
                               return localPartnersListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationPartnersList
                           */
                           public  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationPartnersList getPartnersList(){
                               return localPartnersList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnersList
                               */
                               public void setPartnersList(com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationPartnersList param){
                            localPartnersListTracker = param != null;
                                   
                                            this.localPartnersList=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaxDetailsList localTaxDetailsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsListTracker = false ;

                           public boolean isTaxDetailsListSpecified(){
                               return localTaxDetailsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaxDetailsList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaxDetailsList getTaxDetailsList(){
                               return localTaxDetailsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsList
                               */
                               public void setTaxDetailsList(com.netsuite.webservices.platform.common_2017_2.TaxDetailsList param){
                            localTaxDetailsListTracker = param != null;
                                   
                                            this.localTaxDetailsList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:customers_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ReturnAuthorization",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ReturnAuthorization",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNexusTracker){
                                            if (localNexus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nexus cannot be null!!");
                                            }
                                           localNexus.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","nexus"),
                                               xmlWriter);
                                        } if (localSubsidiaryTaxRegNumTracker){
                                            if (localSubsidiaryTaxRegNum==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryTaxRegNum cannot be null!!");
                                            }
                                           localSubsidiaryTaxRegNum.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subsidiaryTaxRegNum"),
                                               xmlWriter);
                                        } if (localTaxRegOverrideTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRegOverride", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRegOverride cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRegOverride));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxDetailsOverrideTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxDetailsOverride", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxDetailsOverride cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsOverride));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localVatRegNumTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vatRegNum", xmlWriter);
                             

                                          if (localVatRegNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("vatRegNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localVatRegNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranDate", xmlWriter);
                             

                                          if (localTranDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranIdTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranId", xmlWriter);
                             

                                          if (localTranId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTranId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEntityTaxRegNumTracker){
                                            if (localEntityTaxRegNum==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityTaxRegNum cannot be null!!");
                                            }
                                           localEntityTaxRegNum.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","entityTaxRegNum"),
                                               xmlWriter);
                                        } if (localSourceTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "source", xmlWriter);
                             

                                          if (localSource==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSource);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localIntercoTransactionTracker){
                                            if (localIntercoTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoTransaction cannot be null!!");
                                            }
                                           localIntercoTransaction.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","intercoTransaction"),
                                               xmlWriter);
                                        } if (localIntercoStatusTracker){
                                            if (localIntercoStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoStatus cannot be null!!");
                                            }
                                           localIntercoStatus.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","intercoStatus"),
                                               xmlWriter);
                                        } if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localPartnerTracker){
                                            if (localPartner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                            }
                                           localPartner.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","partner"),
                                               xmlWriter);
                                        } if (localOtherRefNumTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "otherRefNum", xmlWriter);
                             

                                          if (localOtherRefNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("otherRefNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOtherRefNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLeadSourceTracker){
                                            if (localLeadSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                            }
                                           localLeadSource.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","leadSource"),
                                               xmlWriter);
                                        } if (localMemoTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDrAccountTracker){
                                            if (localDrAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("drAccount cannot be null!!");
                                            }
                                           localDrAccount.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","drAccount"),
                                               xmlWriter);
                                        } if (localFxAccountTracker){
                                            if (localFxAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxAccount cannot be null!!");
                                            }
                                           localFxAccount.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","fxAccount"),
                                               xmlWriter);
                                        } if (localSalesEffectiveDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "salesEffectiveDate", xmlWriter);
                             

                                          if (localSalesEffectiveDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("salesEffectiveDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalesEffectiveDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedFromTracker){
                                            if (localCreatedFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                            }
                                           localCreatedFrom.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","createdFrom"),
                                               xmlWriter);
                                        } if (localRevRecScheduleTracker){
                                            if (localRevRecSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                            }
                                           localRevRecSchedule.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecSchedule"),
                                               xmlWriter);
                                        } if (localTotalCostEstimateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "totalCostEstimate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTotalCostEstimate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("totalCostEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotalCostEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstGrossProfitTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estGrossProfit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstGrossProfit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estGrossProfit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstGrossProfitPercentTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estGrossProfitPercent", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstGrossProfitPercent)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estGrossProfitPercent cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfitPercent));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecStartDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecStartDate", xmlWriter);
                             

                                          if (localRevRecStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecEndDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecEndDate", xmlWriter);
                             

                                          if (localRevRecEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExcludeCommissionTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "excludeCommission", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("excludeCommission cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeCommission));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExchangeRateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "exchangeRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localExchangeRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("exchangeRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExchangeRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCurrencyNameTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "currencyName", xmlWriter);
                             

                                          if (localCurrencyName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("currencyName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCurrencyName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDiscountItemTracker){
                                            if (localDiscountItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("discountItem cannot be null!!");
                                            }
                                           localDiscountItem.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","discountItem"),
                                               xmlWriter);
                                        } if (localDiscountRateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "discountRate", xmlWriter);
                             

                                          if (localDiscountRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("discountRate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDiscountRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxItemTracker){
                                            if (localTaxItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxItem cannot be null!!");
                                            }
                                           localTaxItem.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxItem"),
                                               xmlWriter);
                                        } if (localTaxRateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localToBePrintedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "toBePrinted", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("toBePrinted cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBePrinted));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localToBeEmailedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "toBeEmailed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("toBeEmailed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeEmailed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localToBeFaxedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "toBeFaxed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("toBeFaxed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeFaxed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMessageSelTracker){
                                            if (localMessageSel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messageSel cannot be null!!");
                                            }
                                           localMessageSel.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","messageSel"),
                                               xmlWriter);
                                        } if (localMessageTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "message", xmlWriter);
                             

                                          if (localMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillingAddressTracker){
                                            if (localBillingAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingAddress cannot be null!!");
                                            }
                                           localBillingAddress.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billingAddress"),
                                               xmlWriter);
                                        } if (localBillAddressListTracker){
                                            if (localBillAddressList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billAddressList cannot be null!!");
                                            }
                                           localBillAddressList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billAddressList"),
                                               xmlWriter);
                                        } if (localShipAddressListTracker){
                                            if (localShipAddressList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddressList cannot be null!!");
                                            }
                                           localShipAddressList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","shipAddressList"),
                                               xmlWriter);
                                        } if (localShipAddressTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipAddress", xmlWriter);
                             

                                          if (localShipAddress==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShipAddress);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesGroupTracker){
                                            if (localSalesGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesGroup cannot be null!!");
                                            }
                                           localSalesGroup.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesGroup"),
                                               xmlWriter);
                                        } if (localSyncSalesTeamsTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "syncSalesTeams", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("syncSalesTeams cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncSalesTeams));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevenueStatusTracker){
                                            if (localRevenueStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueStatus cannot be null!!");
                                            }
                                           localRevenueStatus.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revenueStatus"),
                                               xmlWriter);
                                        } if (localRecognizedRevenueTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recognizedRevenue", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRecognizedRevenue)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("recognizedRevenue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecognizedRevenue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeferredRevenueTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "deferredRevenue", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDeferredRevenue)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("deferredRevenue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferredRevenue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecOnRevCommitmentTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecOnRevCommitment", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("revRecOnRevCommitment cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecOnRevCommitment));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevCommitStatusTracker){
                                            if (localRevCommitStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revCommitStatus cannot be null!!");
                                            }
                                           localRevCommitStatus.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revCommitStatus"),
                                               xmlWriter);
                                        } if (localPaymentMethodTracker){
                                            if (localPaymentMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("paymentMethod cannot be null!!");
                                            }
                                           localPaymentMethod.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","paymentMethod"),
                                               xmlWriter);
                                        } if (localCreditCardTracker){
                                            if (localCreditCard==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditCard cannot be null!!");
                                            }
                                           localCreditCard.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","creditCard"),
                                               xmlWriter);
                                        } if (localCcNumberTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccNumber", xmlWriter);
                             

                                          if (localCcNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltSalesTotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altSalesTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAltSalesTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("altSalesTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcExpireDateTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccExpireDate", xmlWriter);
                             

                                          if (localCcExpireDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccExpireDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcExpireDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcNameTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccName", xmlWriter);
                             

                                          if (localCcName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcStreetTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccStreet", xmlWriter);
                             

                                          if (localCcStreet==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccStreet cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcStreet);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcZipCodeTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccZipCode", xmlWriter);
                             

                                          if (localCcZipCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccZipCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcZipCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcApprovedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccApproved", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("ccApproved cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcApproved));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPnRefNumTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pnRefNum", xmlWriter);
                             

                                          if (localPnRefNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("pnRefNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPnRefNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubTotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "subTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSubTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("subTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDiscountTotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "discountTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDiscountTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("discountTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscountTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "total", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("total cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreditCardProcessorTracker){
                                            if (localCreditCardProcessor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditCardProcessor cannot be null!!");
                                            }
                                           localCreditCardProcessor.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","creditCardProcessor"),
                                               xmlWriter);
                                        } if (localPayPalAuthIdTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "payPalAuthId", xmlWriter);
                             

                                          if (localPayPalAuthId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("payPalAuthId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPayPalAuthId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPayPalProcessTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "payPalProcess", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("payPalProcess cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPayPalProcess));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "email", xmlWriter);
                             

                                          if (localEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFaxTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fax", xmlWriter);
                             

                                          if (localFax==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFax);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDebitCardIssueNoTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "debitCardIssueNo", xmlWriter);
                             

                                          if (localDebitCardIssueNo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("debitCardIssueNo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDebitCardIssueNo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsTaxableTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPromoCodeTracker){
                                            if (localPromoCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("promoCode cannot be null!!");
                                            }
                                           localPromoCode.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","promoCode"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "status", xmlWriter);
                             

                                          if (localStatus==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStatus);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxTotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax2TotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax2Total", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax2Total)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax2Total cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax2Total));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localValidFromTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "validFrom", xmlWriter);
                             

                                          if (localValidFrom==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("validFrom cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localValidFrom));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOrderStatusTracker){
                                            if (localOrderStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("orderStatus cannot be null!!");
                                            }
                                           localOrderStatus.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","orderStatus"),
                                               xmlWriter);
                                        } if (localSalesRepTracker){
                                            if (localSalesRep==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                            }
                                           localSalesRep.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesRep"),
                                               xmlWriter);
                                        } if (localGiftCertTracker){
                                            if (localGiftCert==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giftCert cannot be null!!");
                                            }
                                           localGiftCert.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCert"),
                                               xmlWriter);
                                        } if (localContribPctTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "contribPct", xmlWriter);
                             

                                          if (localContribPct==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("contribPct cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localContribPct);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertTotalTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGiftCertTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("giftCertTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertAppliedTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertApplied", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGiftCertApplied)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("giftCertApplied cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertApplied));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertAvailableTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertAvailable", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGiftCertAvailable)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("giftCertAvailable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertAvailable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranIsVsoeBundleTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranIsVsoeBundle", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tranIsVsoeBundle cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranIsVsoeBundle));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeAutoCalcTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeAutoCalc", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeAutoCalc cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAutoCalc));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSyncPartnerTeamsTracker){
                                    namespace = "urn:customers_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "syncPartnerTeams", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("syncPartnerTeams cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncPartnerTeams));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemListTracker){
                                            if (localItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                            }
                                           localItemList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","itemList"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localSalesTeamListTracker){
                                            if (localSalesTeamList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamList cannot be null!!");
                                            }
                                           localSalesTeamList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesTeamList"),
                                               xmlWriter);
                                        } if (localPartnersListTracker){
                                            if (localPartnersList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnersList cannot be null!!");
                                            }
                                           localPartnersList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","partnersList"),
                                               xmlWriter);
                                        } if (localTaxDetailsListTracker){
                                            if (localTaxDetailsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxDetailsList cannot be null!!");
                                            }
                                           localTaxDetailsList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxDetailsList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:customers_2017_2.transactions.webservices.netsuite.com")){
                return "ns25";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ReturnAuthorization"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localNexusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "nexus"));
                            
                            
                                    if (localNexus==null){
                                         throw new org.apache.axis2.databinding.ADBException("nexus cannot be null!!");
                                    }
                                    elementList.add(localNexus);
                                } if (localSubsidiaryTaxRegNumTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiaryTaxRegNum"));
                            
                            
                                    if (localSubsidiaryTaxRegNum==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryTaxRegNum cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryTaxRegNum);
                                } if (localTaxRegOverrideTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRegOverride"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRegOverride));
                            } if (localTaxDetailsOverrideTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsOverride"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsOverride));
                            } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localVatRegNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vatRegNum"));
                                 
                                        if (localVatRegNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVatRegNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("vatRegNum cannot be null!!");
                                        }
                                    } if (localTranDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranDate"));
                                 
                                        if (localTranDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                        }
                                    } if (localTranIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranId"));
                                 
                                        if (localTranId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                        }
                                    } if (localEntityTaxRegNumTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "entityTaxRegNum"));
                            
                            
                                    if (localEntityTaxRegNum==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityTaxRegNum cannot be null!!");
                                    }
                                    elementList.add(localEntityTaxRegNum);
                                } if (localSourceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "source"));
                                 
                                        if (localSource != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSource));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                        }
                                    } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localIntercoTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "intercoTransaction"));
                            
                            
                                    if (localIntercoTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoTransaction cannot be null!!");
                                    }
                                    elementList.add(localIntercoTransaction);
                                } if (localIntercoStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "intercoStatus"));
                            
                            
                                    if (localIntercoStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoStatus cannot be null!!");
                                    }
                                    elementList.add(localIntercoStatus);
                                } if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localPartnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "partner"));
                            
                            
                                    if (localPartner==null){
                                         throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    }
                                    elementList.add(localPartner);
                                } if (localOtherRefNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "otherRefNum"));
                                 
                                        if (localOtherRefNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOtherRefNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("otherRefNum cannot be null!!");
                                        }
                                    } if (localLeadSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "leadSource"));
                            
                            
                                    if (localLeadSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                    }
                                    elementList.add(localLeadSource);
                                } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localDrAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "drAccount"));
                            
                            
                                    if (localDrAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("drAccount cannot be null!!");
                                    }
                                    elementList.add(localDrAccount);
                                } if (localFxAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "fxAccount"));
                            
                            
                                    if (localFxAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxAccount cannot be null!!");
                                    }
                                    elementList.add(localFxAccount);
                                } if (localSalesEffectiveDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesEffectiveDate"));
                                 
                                        if (localSalesEffectiveDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalesEffectiveDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("salesEffectiveDate cannot be null!!");
                                        }
                                    } if (localCreatedFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFrom"));
                            
                            
                                    if (localCreatedFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                    }
                                    elementList.add(localCreatedFrom);
                                } if (localRevRecScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecSchedule"));
                            
                            
                                    if (localRevRecSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                    }
                                    elementList.add(localRevRecSchedule);
                                } if (localTotalCostEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "totalCostEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotalCostEstimate));
                            } if (localEstGrossProfitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "estGrossProfit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfit));
                            } if (localEstGrossProfitPercentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "estGrossProfitPercent"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfitPercent));
                            } if (localRevRecStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecStartDate"));
                                 
                                        if (localRevRecStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                        }
                                    } if (localRevRecEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecEndDate"));
                                 
                                        if (localRevRecEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                        }
                                    } if (localExcludeCommissionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "excludeCommission"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeCommission));
                            } if (localExchangeRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "exchangeRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExchangeRate));
                            } if (localCurrencyNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "currencyName"));
                                 
                                        if (localCurrencyName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCurrencyName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("currencyName cannot be null!!");
                                        }
                                    } if (localDiscountItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "discountItem"));
                            
                            
                                    if (localDiscountItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("discountItem cannot be null!!");
                                    }
                                    elementList.add(localDiscountItem);
                                } if (localDiscountRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "discountRate"));
                                 
                                        if (localDiscountRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscountRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("discountRate cannot be null!!");
                                        }
                                    } if (localTaxItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxItem"));
                            
                            
                                    if (localTaxItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxItem cannot be null!!");
                                    }
                                    elementList.add(localTaxItem);
                                } if (localTaxRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate));
                            } if (localToBePrintedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "toBePrinted"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBePrinted));
                            } if (localToBeEmailedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "toBeEmailed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeEmailed));
                            } if (localToBeFaxedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "toBeFaxed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeFaxed));
                            } if (localMessageSelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "messageSel"));
                            
                            
                                    if (localMessageSel==null){
                                         throw new org.apache.axis2.databinding.ADBException("messageSel cannot be null!!");
                                    }
                                    elementList.add(localMessageSel);
                                } if (localMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "message"));
                                 
                                        if (localMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                        }
                                    } if (localBillingAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingAddress"));
                            
                            
                                    if (localBillingAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingAddress cannot be null!!");
                                    }
                                    elementList.add(localBillingAddress);
                                } if (localBillAddressListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "billAddressList"));
                            
                            
                                    if (localBillAddressList==null){
                                         throw new org.apache.axis2.databinding.ADBException("billAddressList cannot be null!!");
                                    }
                                    elementList.add(localBillAddressList);
                                } if (localShipAddressListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipAddressList"));
                            
                            
                                    if (localShipAddressList==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddressList cannot be null!!");
                                    }
                                    elementList.add(localShipAddressList);
                                } if (localShipAddressTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipAddress"));
                                 
                                        if (localShipAddress != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipAddress));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                        }
                                    } if (localSalesGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesGroup"));
                            
                            
                                    if (localSalesGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesGroup cannot be null!!");
                                    }
                                    elementList.add(localSalesGroup);
                                } if (localSyncSalesTeamsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "syncSalesTeams"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncSalesTeams));
                            } if (localRevenueStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revenueStatus"));
                            
                            
                                    if (localRevenueStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueStatus cannot be null!!");
                                    }
                                    elementList.add(localRevenueStatus);
                                } if (localRecognizedRevenueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "recognizedRevenue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecognizedRevenue));
                            } if (localDeferredRevenueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "deferredRevenue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferredRevenue));
                            } if (localRevRecOnRevCommitmentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecOnRevCommitment"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecOnRevCommitment));
                            } if (localRevCommitStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "revCommitStatus"));
                            
                            
                                    if (localRevCommitStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("revCommitStatus cannot be null!!");
                                    }
                                    elementList.add(localRevCommitStatus);
                                } if (localPaymentMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentMethod"));
                            
                            
                                    if (localPaymentMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("paymentMethod cannot be null!!");
                                    }
                                    elementList.add(localPaymentMethod);
                                } if (localCreditCardTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "creditCard"));
                            
                            
                                    if (localCreditCard==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditCard cannot be null!!");
                                    }
                                    elementList.add(localCreditCard);
                                } if (localCcNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccNumber"));
                                 
                                        if (localCcNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccNumber cannot be null!!");
                                        }
                                    } if (localAltSalesTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "altSalesTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesTotal));
                            } if (localCcExpireDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccExpireDate"));
                                 
                                        if (localCcExpireDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcExpireDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccExpireDate cannot be null!!");
                                        }
                                    } if (localCcNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccName"));
                                 
                                        if (localCcName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccName cannot be null!!");
                                        }
                                    } if (localCcStreetTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccStreet"));
                                 
                                        if (localCcStreet != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcStreet));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccStreet cannot be null!!");
                                        }
                                    } if (localCcZipCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccZipCode"));
                                 
                                        if (localCcZipCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcZipCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccZipCode cannot be null!!");
                                        }
                                    } if (localCcApprovedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccApproved"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcApproved));
                            } if (localPnRefNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "pnRefNum"));
                                 
                                        if (localPnRefNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPnRefNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("pnRefNum cannot be null!!");
                                        }
                                    } if (localSubTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "subTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubTotal));
                            } if (localDiscountTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "discountTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscountTotal));
                            } if (localTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "total"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotal));
                            } if (localCreditCardProcessorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "creditCardProcessor"));
                            
                            
                                    if (localCreditCardProcessor==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditCardProcessor cannot be null!!");
                                    }
                                    elementList.add(localCreditCardProcessor);
                                } if (localPayPalAuthIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "payPalAuthId"));
                                 
                                        if (localPayPalAuthId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPayPalAuthId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("payPalAuthId cannot be null!!");
                                        }
                                    } if (localPayPalProcessTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "payPalProcess"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPayPalProcess));
                            } if (localEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "email"));
                                 
                                        if (localEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                        }
                                    } if (localFaxTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "fax"));
                                 
                                        if (localFax != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFax));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                        }
                                    } if (localDebitCardIssueNoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "debitCardIssueNo"));
                                 
                                        if (localDebitCardIssueNo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDebitCardIssueNo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("debitCardIssueNo cannot be null!!");
                                        }
                                    } if (localIsTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "isTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                            } if (localPromoCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "promoCode"));
                            
                            
                                    if (localPromoCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("promoCode cannot be null!!");
                                    }
                                    elementList.add(localPromoCode);
                                } if (localStatusTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "status"));
                                 
                                        if (localStatus != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStatus));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                        }
                                    } if (localTaxTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxTotal));
                            } if (localTax2TotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax2Total"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax2Total));
                            } if (localValidFromTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "validFrom"));
                                 
                                        if (localValidFrom != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localValidFrom));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("validFrom cannot be null!!");
                                        }
                                    } if (localOrderStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderStatus"));
                            
                            
                                    if (localOrderStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("orderStatus cannot be null!!");
                                    }
                                    elementList.add(localOrderStatus);
                                } if (localSalesRepTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesRep"));
                            
                            
                                    if (localSalesRep==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                    }
                                    elementList.add(localSalesRep);
                                } if (localGiftCertTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCert"));
                            
                            
                                    if (localGiftCert==null){
                                         throw new org.apache.axis2.databinding.ADBException("giftCert cannot be null!!");
                                    }
                                    elementList.add(localGiftCert);
                                } if (localContribPctTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "contribPct"));
                                 
                                        if (localContribPct != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContribPct));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("contribPct cannot be null!!");
                                        }
                                    } if (localGiftCertTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertTotal));
                            } if (localGiftCertAppliedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertApplied"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertApplied));
                            } if (localGiftCertAvailableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertAvailable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertAvailable));
                            } if (localTranIsVsoeBundleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranIsVsoeBundle"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranIsVsoeBundle));
                            } if (localVsoeAutoCalcTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeAutoCalc"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAutoCalc));
                            } if (localSyncPartnerTeamsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "syncPartnerTeams"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncPartnerTeams));
                            } if (localItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemList"));
                            
                            
                                    if (localItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                    }
                                    elementList.add(localItemList);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localSalesTeamListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesTeamList"));
                            
                            
                                    if (localSalesTeamList==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamList cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamList);
                                } if (localPartnersListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "partnersList"));
                            
                            
                                    if (localPartnersList==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnersList cannot be null!!");
                                    }
                                    elementList.add(localPartnersList);
                                } if (localTaxDetailsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsList"));
                            
                            
                                    if (localTaxDetailsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxDetailsList cannot be null!!");
                                    }
                                    elementList.add(localTaxDetailsList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ReturnAuthorization parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ReturnAuthorization object =
                new ReturnAuthorization();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ReturnAuthorization".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ReturnAuthorization)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","nexus").equals(reader.getName())){
                                
                                                object.setNexus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subsidiaryTaxRegNum").equals(reader.getName())){
                                
                                                object.setSubsidiaryTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxRegOverride").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRegOverride" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRegOverride(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxDetailsOverride").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxDetailsOverride" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxDetailsOverride(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vatRegNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vatRegNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVatRegNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","entityTaxRegNum").equals(reader.getName())){
                                
                                                object.setEntityTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","source").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"source" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSource(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","intercoTransaction").equals(reader.getName())){
                                
                                                object.setIntercoTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","intercoStatus").equals(reader.getName())){
                                
                                                object.setIntercoStatus(com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                                object.setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","otherRefNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"otherRefNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOtherRefNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","leadSource").equals(reader.getName())){
                                
                                                object.setLeadSource(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","drAccount").equals(reader.getName())){
                                
                                                object.setDrAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","fxAccount").equals(reader.getName())){
                                
                                                object.setFxAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesEffectiveDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"salesEffectiveDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSalesEffectiveDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","createdFrom").equals(reader.getName())){
                                
                                                object.setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecSchedule").equals(reader.getName())){
                                
                                                object.setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","totalCostEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"totalCostEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTotalCostEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTotalCostEstimate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","estGrossProfit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estGrossProfit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstGrossProfit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstGrossProfit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","estGrossProfitPercent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estGrossProfitPercent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstGrossProfitPercent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstGrossProfitPercent(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","excludeCommission").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"excludeCommission" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExcludeCommission(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","exchangeRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"exchangeRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExchangeRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setExchangeRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","currencyName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"currencyName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCurrencyName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","discountItem").equals(reader.getName())){
                                
                                                object.setDiscountItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","discountRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"discountRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDiscountRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxItem").equals(reader.getName())){
                                
                                                object.setTaxItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","toBePrinted").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"toBePrinted" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setToBePrinted(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","toBeEmailed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"toBeEmailed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setToBeEmailed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","toBeFaxed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"toBeFaxed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setToBeFaxed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","messageSel").equals(reader.getName())){
                                
                                                object.setMessageSel(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"message" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billingAddress").equals(reader.getName())){
                                
                                                object.setBillingAddress(com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","billAddressList").equals(reader.getName())){
                                
                                                object.setBillAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","shipAddressList").equals(reader.getName())){
                                
                                                object.setShipAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","shipAddress").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipAddress" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipAddress(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesGroup").equals(reader.getName())){
                                
                                                object.setSalesGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","syncSalesTeams").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"syncSalesTeams" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSyncSalesTeams(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revenueStatus").equals(reader.getName())){
                                
                                                object.setRevenueStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","recognizedRevenue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recognizedRevenue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecognizedRevenue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRecognizedRevenue(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","deferredRevenue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"deferredRevenue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeferredRevenue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDeferredRevenue(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revRecOnRevCommitment").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecOnRevCommitment" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecOnRevCommitment(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","revCommitStatus").equals(reader.getName())){
                                
                                                object.setRevCommitStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","paymentMethod").equals(reader.getName())){
                                
                                                object.setPaymentMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","creditCard").equals(reader.getName())){
                                
                                                object.setCreditCard(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ccNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","altSalesTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altSalesTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltSalesTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAltSalesTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ccExpireDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccExpireDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcExpireDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ccName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ccStreet").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccStreet" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcStreet(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ccZipCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccZipCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcZipCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","ccApproved").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccApproved" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcApproved(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","pnRefNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pnRefNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPnRefNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","subTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"subTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSubTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSubTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","discountTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"discountTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDiscountTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDiscountTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","total").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"total" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","creditCardProcessor").equals(reader.getName())){
                                
                                                object.setCreditCardProcessor(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","payPalAuthId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"payPalAuthId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPayPalAuthId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","payPalProcess").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"payPalProcess" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPayPalProcess(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"email" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fax" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFax(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","debitCardIssueNo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"debitCardIssueNo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDebitCardIssueNo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","isTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","promoCode").equals(reader.getName())){
                                
                                                object.setPromoCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"status" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStatus(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","tax2Total").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax2Total" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax2Total(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax2Total(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","validFrom").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"validFrom" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setValidFrom(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","orderStatus").equals(reader.getName())){
                                
                                                object.setOrderStatus(com.netsuite.webservices.transactions.customers_2017_2.types.ReturnAuthorizationOrderStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                
                                                object.setSalesRep(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCert").equals(reader.getName())){
                                
                                                object.setGiftCert(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","contribPct").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"contribPct" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setContribPct(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGiftCertTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertApplied").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertApplied" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertApplied(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGiftCertApplied(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","giftCertAvailable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertAvailable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertAvailable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGiftCertAvailable(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","tranIsVsoeBundle").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranIsVsoeBundle" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranIsVsoeBundle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","vsoeAutoCalc").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeAutoCalc" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeAutoCalc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","syncPartnerTeams").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"syncPartnerTeams" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSyncPartnerTeams(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","itemList").equals(reader.getName())){
                                
                                                object.setItemList(com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","salesTeamList").equals(reader.getName())){
                                
                                                object.setSalesTeamList(com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeamList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","partnersList").equals(reader.getName())){
                                
                                                object.setPartnersList(com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationPartnersList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","taxDetailsList").equals(reader.getName())){
                                
                                                object.setTaxDetailsList(com.netsuite.webservices.platform.common_2017_2.TaxDetailsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customers_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    