
/**
 * ItemReceipt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.purchases_2017_2;
            

            /**
            *  ItemReceipt bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemReceipt extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemReceipt
                Namespace URI = urn:purchases_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns23
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for ExchangeRate
                        */

                        
                                    protected double localExchangeRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExchangeRateTracker = false ;

                           public boolean isExchangeRateSpecified(){
                               return localExchangeRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getExchangeRate(){
                               return localExchangeRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExchangeRate
                               */
                               public void setExchangeRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localExchangeRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localExchangeRate=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for CurrencyName
                        */

                        
                                    protected java.lang.String localCurrencyName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyNameTracker = false ;

                           public boolean isCurrencyNameSpecified(){
                               return localCurrencyNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCurrencyName(){
                               return localCurrencyName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CurrencyName
                               */
                               public void setCurrencyName(java.lang.String param){
                            localCurrencyNameTracker = param != null;
                                   
                                            this.localCurrencyName=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromTracker = false ;

                           public boolean isCreatedFromSpecified(){
                               return localCreatedFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedFrom(){
                               return localCreatedFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFrom
                               */
                               public void setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedFromTracker = param != null;
                                   
                                            this.localCreatedFrom=param;
                                    

                               }
                            

                        /**
                        * field for TranDate
                        */

                        
                                    protected java.util.Calendar localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTranDate(){
                               return localTranDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranDate
                               */
                               public void setTranDate(java.util.Calendar param){
                            localTranDateTracker = param != null;
                                   
                                            this.localTranDate=param;
                                    

                               }
                            

                        /**
                        * field for Partner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPartner(){
                               return localPartner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Partner
                               */
                               public void setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPartnerTracker = param != null;
                                   
                                            this.localPartner=param;
                                    

                               }
                            

                        /**
                        * field for PostingPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPostingPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPostingPeriodTracker = false ;

                           public boolean isPostingPeriodSpecified(){
                               return localPostingPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPostingPeriod(){
                               return localPostingPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PostingPeriod
                               */
                               public void setPostingPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPostingPeriodTracker = param != null;
                                   
                                            this.localPostingPeriod=param;
                                    

                               }
                            

                        /**
                        * field for TranId
                        */

                        
                                    protected java.lang.String localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTranId(){
                               return localTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranId
                               */
                               public void setTranId(java.lang.String param){
                            localTranIdTracker = param != null;
                                   
                                            this.localTranId=param;
                                    

                               }
                            

                        /**
                        * field for InboundShipment
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInboundShipment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInboundShipmentTracker = false ;

                           public boolean isInboundShipmentSpecified(){
                               return localInboundShipmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInboundShipment(){
                               return localInboundShipment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InboundShipment
                               */
                               public void setInboundShipment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInboundShipmentTracker = param != null;
                                   
                                            this.localInboundShipment=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for ItemFulfillment
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItemFulfillment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemFulfillmentTracker = false ;

                           public boolean isItemFulfillmentSpecified(){
                               return localItemFulfillmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItemFulfillment(){
                               return localItemFulfillment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemFulfillment
                               */
                               public void setItemFulfillment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemFulfillmentTracker = param != null;
                                   
                                            this.localItemFulfillment=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for LandedCostMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.LandedCostMethod localLandedCostMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLandedCostMethodTracker = false ;

                           public boolean isLandedCostMethodSpecified(){
                               return localLandedCostMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.LandedCostMethod
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.LandedCostMethod getLandedCostMethod(){
                               return localLandedCostMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LandedCostMethod
                               */
                               public void setLandedCostMethod(com.netsuite.webservices.platform.common_2017_2.types.LandedCostMethod param){
                            localLandedCostMethodTracker = param != null;
                                   
                                            this.localLandedCostMethod=param;
                                    

                               }
                            

                        /**
                        * field for LandedCostPerLine
                        */

                        
                                    protected boolean localLandedCostPerLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLandedCostPerLineTracker = false ;

                           public boolean isLandedCostPerLineSpecified(){
                               return localLandedCostPerLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getLandedCostPerLine(){
                               return localLandedCostPerLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LandedCostPerLine
                               */
                               public void setLandedCostPerLine(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localLandedCostPerLineTracker =
                                       true;
                                   
                                            this.localLandedCostPerLine=param;
                                    

                               }
                            

                        /**
                        * field for ItemList
                        */

                        
                                    protected com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItemList localItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemListTracker = false ;

                           public boolean isItemListSpecified(){
                               return localItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItemList
                           */
                           public  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItemList getItemList(){
                               return localItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemList
                               */
                               public void setItemList(com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItemList param){
                            localItemListTracker = param != null;
                                   
                                            this.localItemList=param;
                                    

                               }
                            

                        /**
                        * field for ExpenseList
                        */

                        
                                    protected com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpenseList localExpenseList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpenseListTracker = false ;

                           public boolean isExpenseListSpecified(){
                               return localExpenseListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpenseList
                           */
                           public  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpenseList getExpenseList(){
                               return localExpenseList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpenseList
                               */
                               public void setExpenseList(com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpenseList param){
                            localExpenseListTracker = param != null;
                                   
                                            this.localExpenseList=param;
                                    

                               }
                            

                        /**
                        * field for LandedCostsList
                        */

                        
                                    protected com.netsuite.webservices.transactions.purchases_2017_2.PurchLandedCostList localLandedCostsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLandedCostsListTracker = false ;

                           public boolean isLandedCostsListSpecified(){
                               return localLandedCostsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.purchases_2017_2.PurchLandedCostList
                           */
                           public  com.netsuite.webservices.transactions.purchases_2017_2.PurchLandedCostList getLandedCostsList(){
                               return localLandedCostsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LandedCostsList
                               */
                               public void setLandedCostsList(com.netsuite.webservices.transactions.purchases_2017_2.PurchLandedCostList param){
                            localLandedCostsListTracker = param != null;
                                   
                                            this.localLandedCostsList=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:purchases_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemReceipt",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemReceipt",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localExchangeRateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "exchangeRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localExchangeRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("exchangeRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExchangeRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localCurrencyNameTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "currencyName", xmlWriter);
                             

                                          if (localCurrencyName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("currencyName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCurrencyName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localCreatedFromTracker){
                                            if (localCreatedFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                            }
                                           localCreatedFrom.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","createdFrom"),
                                               xmlWriter);
                                        } if (localTranDateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranDate", xmlWriter);
                             

                                          if (localTranDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPartnerTracker){
                                            if (localPartner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                            }
                                           localPartner.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","partner"),
                                               xmlWriter);
                                        } if (localPostingPeriodTracker){
                                            if (localPostingPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("postingPeriod cannot be null!!");
                                            }
                                           localPostingPeriod.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","postingPeriod"),
                                               xmlWriter);
                                        } if (localTranIdTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranId", xmlWriter);
                             

                                          if (localTranId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTranId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInboundShipmentTracker){
                                            if (localInboundShipment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inboundShipment cannot be null!!");
                                            }
                                           localInboundShipment.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","inboundShipment"),
                                               xmlWriter);
                                        } if (localMemoTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemFulfillmentTracker){
                                            if (localItemFulfillment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemFulfillment cannot be null!!");
                                            }
                                           localItemFulfillment.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","itemFulfillment"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localLandedCostMethodTracker){
                                            if (localLandedCostMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("landedCostMethod cannot be null!!");
                                            }
                                           localLandedCostMethod.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostMethod"),
                                               xmlWriter);
                                        } if (localLandedCostPerLineTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "landedCostPerLine", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("landedCostPerLine cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLandedCostPerLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemListTracker){
                                            if (localItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                            }
                                           localItemList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","itemList"),
                                               xmlWriter);
                                        } if (localExpenseListTracker){
                                            if (localExpenseList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expenseList cannot be null!!");
                                            }
                                           localExpenseList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","expenseList"),
                                               xmlWriter);
                                        } if (localLandedCostsListTracker){
                                            if (localLandedCostsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("landedCostsList cannot be null!!");
                                            }
                                           localLandedCostsList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostsList"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:purchases_2017_2.transactions.webservices.netsuite.com")){
                return "ns23";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","ItemReceipt"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localExchangeRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "exchangeRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExchangeRate));
                            } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localCurrencyNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "currencyName"));
                                 
                                        if (localCurrencyName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCurrencyName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("currencyName cannot be null!!");
                                        }
                                    } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localCreatedFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFrom"));
                            
                            
                                    if (localCreatedFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                    }
                                    elementList.add(localCreatedFrom);
                                } if (localTranDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranDate"));
                                 
                                        if (localTranDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                        }
                                    } if (localPartnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "partner"));
                            
                            
                                    if (localPartner==null){
                                         throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    }
                                    elementList.add(localPartner);
                                } if (localPostingPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "postingPeriod"));
                            
                            
                                    if (localPostingPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("postingPeriod cannot be null!!");
                                    }
                                    elementList.add(localPostingPeriod);
                                } if (localTranIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranId"));
                                 
                                        if (localTranId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                        }
                                    } if (localInboundShipmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "inboundShipment"));
                            
                            
                                    if (localInboundShipment==null){
                                         throw new org.apache.axis2.databinding.ADBException("inboundShipment cannot be null!!");
                                    }
                                    elementList.add(localInboundShipment);
                                } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localItemFulfillmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemFulfillment"));
                            
                            
                                    if (localItemFulfillment==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemFulfillment cannot be null!!");
                                    }
                                    elementList.add(localItemFulfillment);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localLandedCostMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "landedCostMethod"));
                            
                            
                                    if (localLandedCostMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("landedCostMethod cannot be null!!");
                                    }
                                    elementList.add(localLandedCostMethod);
                                } if (localLandedCostPerLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "landedCostPerLine"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLandedCostPerLine));
                            } if (localItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemList"));
                            
                            
                                    if (localItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                    }
                                    elementList.add(localItemList);
                                } if (localExpenseListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "expenseList"));
                            
                            
                                    if (localExpenseList==null){
                                         throw new org.apache.axis2.databinding.ADBException("expenseList cannot be null!!");
                                    }
                                    elementList.add(localExpenseList);
                                } if (localLandedCostsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "landedCostsList"));
                            
                            
                                    if (localLandedCostsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("landedCostsList cannot be null!!");
                                    }
                                    elementList.add(localLandedCostsList);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemReceipt parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemReceipt object =
                new ItemReceipt();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemReceipt".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemReceipt)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","exchangeRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"exchangeRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExchangeRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setExchangeRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","currencyName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"currencyName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCurrencyName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","createdFrom").equals(reader.getName())){
                                
                                                object.setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                                object.setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","postingPeriod").equals(reader.getName())){
                                
                                                object.setPostingPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","inboundShipment").equals(reader.getName())){
                                
                                                object.setInboundShipment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","itemFulfillment").equals(reader.getName())){
                                
                                                object.setItemFulfillment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostMethod").equals(reader.getName())){
                                
                                                object.setLandedCostMethod(com.netsuite.webservices.platform.common_2017_2.types.LandedCostMethod.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostPerLine").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"landedCostPerLine" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLandedCostPerLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","itemList").equals(reader.getName())){
                                
                                                object.setItemList(com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","expenseList").equals(reader.getName())){
                                
                                                object.setExpenseList(com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpenseList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostsList").equals(reader.getName())){
                                
                                                object.setLandedCostsList(com.netsuite.webservices.transactions.purchases_2017_2.PurchLandedCostList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    