
/**
 * InboundShipmentItems.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.purchases_2017_2;
            

            /**
            *  InboundShipmentItems bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class InboundShipmentItems
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = InboundShipmentItems
                Namespace URI = urn:purchases_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns23
                */
            

                        /**
                        * field for Id
                        */

                        
                                    protected long localId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdTracker = false ;

                           public boolean isIdSpecified(){
                               return localIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getId(){
                               return localId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Id
                               */
                               public void setId(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localIdTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localId=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrder
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPurchaseOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderTracker = false ;

                           public boolean isPurchaseOrderSpecified(){
                               return localPurchaseOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPurchaseOrder(){
                               return localPurchaseOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrder
                               */
                               public void setPurchaseOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPurchaseOrderTracker = param != null;
                                   
                                            this.localPurchaseOrder=param;
                                    

                               }
                            

                        /**
                        * field for ShipmentItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipmentItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipmentItemTracker = false ;

                           public boolean isShipmentItemSpecified(){
                               return localShipmentItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipmentItem(){
                               return localShipmentItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipmentItem
                               */
                               public void setShipmentItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipmentItemTracker = param != null;
                                   
                                            this.localShipmentItem=param;
                                    

                               }
                            

                        /**
                        * field for ShipmentItemDescription
                        */

                        
                                    protected java.lang.String localShipmentItemDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipmentItemDescriptionTracker = false ;

                           public boolean isShipmentItemDescriptionSpecified(){
                               return localShipmentItemDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShipmentItemDescription(){
                               return localShipmentItemDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipmentItemDescription
                               */
                               public void setShipmentItemDescription(java.lang.String param){
                            localShipmentItemDescriptionTracker = param != null;
                                   
                                            this.localShipmentItemDescription=param;
                                    

                               }
                            

                        /**
                        * field for PoVendor
                        */

                        
                                    protected java.lang.String localPoVendor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoVendorTracker = false ;

                           public boolean isPoVendorSpecified(){
                               return localPoVendorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPoVendor(){
                               return localPoVendor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoVendor
                               */
                               public void setPoVendor(java.lang.String param){
                            localPoVendorTracker = param != null;
                                   
                                            this.localPoVendor=param;
                                    

                               }
                            

                        /**
                        * field for ReceivingLocation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localReceivingLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceivingLocationTracker = false ;

                           public boolean isReceivingLocationSpecified(){
                               return localReceivingLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getReceivingLocation(){
                               return localReceivingLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceivingLocation
                               */
                               public void setReceivingLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localReceivingLocationTracker = param != null;
                                   
                                            this.localReceivingLocation=param;
                                    

                               }
                            

                        /**
                        * field for QuantityReceived
                        */

                        
                                    protected double localQuantityReceived ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityReceivedTracker = false ;

                           public boolean isQuantityReceivedSpecified(){
                               return localQuantityReceivedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityReceived(){
                               return localQuantityReceived;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityReceived
                               */
                               public void setQuantityReceived(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityReceivedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityReceived=param;
                                    

                               }
                            

                        /**
                        * field for QuantityExpected
                        */

                        
                                    protected double localQuantityExpected ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityExpectedTracker = false ;

                           public boolean isQuantityExpectedSpecified(){
                               return localQuantityExpectedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityExpected(){
                               return localQuantityExpected;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityExpected
                               */
                               public void setQuantityExpected(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityExpectedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityExpected=param;
                                    

                               }
                            

                        /**
                        * field for QuantityRemaining
                        */

                        
                                    protected double localQuantityRemaining ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityRemainingTracker = false ;

                           public boolean isQuantityRemainingSpecified(){
                               return localQuantityRemainingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityRemaining(){
                               return localQuantityRemaining;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityRemaining
                               */
                               public void setQuantityRemaining(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityRemainingTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityRemaining=param;
                                    

                               }
                            

                        /**
                        * field for Unit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitTracker = false ;

                           public boolean isUnitSpecified(){
                               return localUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnit(){
                               return localUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Unit
                               */
                               public void setUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitTracker = param != null;
                                   
                                            this.localUnit=param;
                                    

                               }
                            

                        /**
                        * field for PoRate
                        */

                        
                                    protected double localPoRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoRateTracker = false ;

                           public boolean isPoRateSpecified(){
                               return localPoRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPoRate(){
                               return localPoRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoRate
                               */
                               public void setPoRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPoRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPoRate=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedRate
                        */

                        
                                    protected double localExpectedRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedRateTracker = false ;

                           public boolean isExpectedRateSpecified(){
                               return localExpectedRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getExpectedRate(){
                               return localExpectedRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedRate
                               */
                               public void setExpectedRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localExpectedRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localExpectedRate=param;
                                    

                               }
                            

                        /**
                        * field for ShipmentItemAmount
                        */

                        
                                    protected double localShipmentItemAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipmentItemAmountTracker = false ;

                           public boolean isShipmentItemAmountSpecified(){
                               return localShipmentItemAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShipmentItemAmount(){
                               return localShipmentItemAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipmentItemAmount
                               */
                               public void setShipmentItemAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipmentItemAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShipmentItemAmount=param;
                                    

                               }
                            

                        /**
                        * field for PoCurrency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPoCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoCurrencyTracker = false ;

                           public boolean isPoCurrencySpecified(){
                               return localPoCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPoCurrency(){
                               return localPoCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoCurrency
                               */
                               public void setPoCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPoCurrencyTracker = param != null;
                                   
                                            this.localPoCurrency=param;
                                    

                               }
                            

                        /**
                        * field for Incoterm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIncoterm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncotermTracker = false ;

                           public boolean isIncotermSpecified(){
                               return localIncotermTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIncoterm(){
                               return localIncoterm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Incoterm
                               */
                               public void setIncoterm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIncotermTracker = param != null;
                                   
                                            this.localIncoterm=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:purchases_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":InboundShipmentItems",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "InboundShipmentItems",
                           xmlWriter);
                   }

               
                   }
                if (localIdTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "id", xmlWriter);
                             
                                               if (localId==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseOrderTracker){
                                            if (localPurchaseOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrder cannot be null!!");
                                            }
                                           localPurchaseOrder.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","purchaseOrder"),
                                               xmlWriter);
                                        } if (localShipmentItemTracker){
                                            if (localShipmentItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipmentItem cannot be null!!");
                                            }
                                           localShipmentItem.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","shipmentItem"),
                                               xmlWriter);
                                        } if (localShipmentItemDescriptionTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipmentItemDescription", xmlWriter);
                             

                                          if (localShipmentItemDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipmentItemDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShipmentItemDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPoVendorTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "poVendor", xmlWriter);
                             

                                          if (localPoVendor==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("poVendor cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPoVendor);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReceivingLocationTracker){
                                            if (localReceivingLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("receivingLocation cannot be null!!");
                                            }
                                           localReceivingLocation.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","receivingLocation"),
                                               xmlWriter);
                                        } if (localQuantityReceivedTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityReceived", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityReceived)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityReceived cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityReceived));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityExpectedTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityExpected", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityExpected)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityExpected cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityExpected));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityRemainingTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityRemaining", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityRemaining)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityRemaining cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityRemaining));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnitTracker){
                                            if (localUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("unit cannot be null!!");
                                            }
                                           localUnit.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","unit"),
                                               xmlWriter);
                                        } if (localPoRateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "poRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPoRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("poRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPoRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExpectedRateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "expectedRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localExpectedRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("expectedRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipmentItemAmountTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipmentItemAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShipmentItemAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipmentItemAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentItemAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPoCurrencyTracker){
                                            if (localPoCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("poCurrency cannot be null!!");
                                            }
                                           localPoCurrency.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","poCurrency"),
                                               xmlWriter);
                                        } if (localIncotermTracker){
                                            if (localIncoterm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("incoterm cannot be null!!");
                                            }
                                           localIncoterm.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","incoterm"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:purchases_2017_2.transactions.webservices.netsuite.com")){
                return "ns23";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
                            } if (localPurchaseOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "purchaseOrder"));
                            
                            
                                    if (localPurchaseOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrder cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrder);
                                } if (localShipmentItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipmentItem"));
                            
                            
                                    if (localShipmentItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipmentItem cannot be null!!");
                                    }
                                    elementList.add(localShipmentItem);
                                } if (localShipmentItemDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipmentItemDescription"));
                                 
                                        if (localShipmentItemDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentItemDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipmentItemDescription cannot be null!!");
                                        }
                                    } if (localPoVendorTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "poVendor"));
                                 
                                        if (localPoVendor != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPoVendor));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("poVendor cannot be null!!");
                                        }
                                    } if (localReceivingLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "receivingLocation"));
                            
                            
                                    if (localReceivingLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("receivingLocation cannot be null!!");
                                    }
                                    elementList.add(localReceivingLocation);
                                } if (localQuantityReceivedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityReceived"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityReceived));
                            } if (localQuantityExpectedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityExpected"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityExpected));
                            } if (localQuantityRemainingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityRemaining"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityRemaining));
                            } if (localUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "unit"));
                            
                            
                                    if (localUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("unit cannot be null!!");
                                    }
                                    elementList.add(localUnit);
                                } if (localPoRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "poRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPoRate));
                            } if (localExpectedRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "expectedRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedRate));
                            } if (localShipmentItemAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipmentItemAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentItemAmount));
                            } if (localPoCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "poCurrency"));
                            
                            
                                    if (localPoCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("poCurrency cannot be null!!");
                                    }
                                    elementList.add(localPoCurrency);
                                } if (localIncotermTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "incoterm"));
                            
                            
                                    if (localIncoterm==null){
                                         throw new org.apache.axis2.databinding.ADBException("incoterm cannot be null!!");
                                    }
                                    elementList.add(localIncoterm);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static InboundShipmentItems parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            InboundShipmentItems object =
                new InboundShipmentItems();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"InboundShipmentItems".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (InboundShipmentItems)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","id").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"id" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setId(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","purchaseOrder").equals(reader.getName())){
                                
                                                object.setPurchaseOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","shipmentItem").equals(reader.getName())){
                                
                                                object.setShipmentItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","shipmentItemDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipmentItemDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipmentItemDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","poVendor").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"poVendor" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPoVendor(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","receivingLocation").equals(reader.getName())){
                                
                                                object.setReceivingLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityReceived").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityReceived" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityReceived(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityReceived(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityExpected").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityExpected" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityExpected(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityExpected(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityRemaining").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityRemaining" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityRemaining(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityRemaining(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","unit").equals(reader.getName())){
                                
                                                object.setUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","poRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"poRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPoRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPoRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","expectedRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"expectedRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpectedRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setExpectedRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","shipmentItemAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipmentItemAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipmentItemAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShipmentItemAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","poCurrency").equals(reader.getName())){
                                
                                                object.setPoCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","incoterm").equals(reader.getName())){
                                
                                                object.setIncoterm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    