
/**
 * VendorBillExpense.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.purchases_2017_2;
            

            /**
            *  VendorBillExpense bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class VendorBillExpense
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = VendorBillExpense
                Namespace URI = urn:purchases_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns23
                */
            

                        /**
                        * field for OrderDoc
                        */

                        
                                    protected long localOrderDoc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderDocTracker = false ;

                           public boolean isOrderDocSpecified(){
                               return localOrderDocTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOrderDoc(){
                               return localOrderDoc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderDoc
                               */
                               public void setOrderDoc(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localOrderDocTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localOrderDoc=param;
                                    

                               }
                            

                        /**
                        * field for OrderLine
                        */

                        
                                    protected long localOrderLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderLineTracker = false ;

                           public boolean isOrderLineSpecified(){
                               return localOrderLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOrderLine(){
                               return localOrderLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderLine
                               */
                               public void setOrderLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localOrderLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localOrderLine=param;
                                    

                               }
                            

                        /**
                        * field for Line
                        */

                        
                                    protected long localLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineTracker = false ;

                           public boolean isLineSpecified(){
                               return localLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLine(){
                               return localLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Line
                               */
                               public void setLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLine=param;
                                    

                               }
                            

                        /**
                        * field for Category
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryTracker = false ;

                           public boolean isCategorySpecified(){
                               return localCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCategory(){
                               return localCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Category
                               */
                               public void setCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCategoryTracker = param != null;
                                   
                                            this.localCategory=param;
                                    

                               }
                            

                        /**
                        * field for Account
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountTracker = false ;

                           public boolean isAccountSpecified(){
                               return localAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAccount(){
                               return localAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Account
                               */
                               public void setAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAccountTracker = param != null;
                                   
                                            this.localAccount=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected double localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for TaxAmount
                        */

                        
                                    protected double localTaxAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxAmountTracker = false ;

                           public boolean isTaxAmountSpecified(){
                               return localTaxAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxAmount(){
                               return localTaxAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxAmount
                               */
                               public void setTaxAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxAmount=param;
                                    

                               }
                            

                        /**
                        * field for Tax1Amt
                        */

                        
                                    protected double localTax1Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax1AmtTracker = false ;

                           public boolean isTax1AmtSpecified(){
                               return localTax1AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax1Amt(){
                               return localTax1Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax1Amt
                               */
                               public void setTax1Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax1AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax1Amt=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for GrossAmt
                        */

                        
                                    protected double localGrossAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGrossAmtTracker = false ;

                           public boolean isGrossAmtSpecified(){
                               return localGrossAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGrossAmt(){
                               return localGrossAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GrossAmt
                               */
                               public void setGrossAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGrossAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGrossAmt=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsReference
                        */

                        
                                    protected java.lang.String localTaxDetailsReference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsReferenceTracker = false ;

                           public boolean isTaxDetailsReferenceSpecified(){
                               return localTaxDetailsReferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTaxDetailsReference(){
                               return localTaxDetailsReference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsReference
                               */
                               public void setTaxDetailsReference(java.lang.String param){
                            localTaxDetailsReferenceTracker = param != null;
                                   
                                            this.localTaxDetailsReference=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for Customer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerTracker = false ;

                           public boolean isCustomerSpecified(){
                               return localCustomerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomer(){
                               return localCustomer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Customer
                               */
                               public void setCustomer(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomerTracker = param != null;
                                   
                                            this.localCustomer=param;
                                    

                               }
                            

                        /**
                        * field for IsBillable
                        */

                        
                                    protected boolean localIsBillable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsBillableTracker = false ;

                           public boolean isIsBillableSpecified(){
                               return localIsBillableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsBillable(){
                               return localIsBillable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsBillable
                               */
                               public void setIsBillable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsBillableTracker =
                                       true;
                                   
                                            this.localIsBillable=param;
                                    

                               }
                            

                        /**
                        * field for ProjectTask
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProjectTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTaskTracker = false ;

                           public boolean isProjectTaskSpecified(){
                               return localProjectTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProjectTask(){
                               return localProjectTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectTask
                               */
                               public void setProjectTask(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProjectTaskTracker = param != null;
                                   
                                            this.localProjectTask=param;
                                    

                               }
                            

                        /**
                        * field for TaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxCodeTracker = false ;

                           public boolean isTaxCodeSpecified(){
                               return localTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxCode(){
                               return localTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxCode
                               */
                               public void setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxCodeTracker = param != null;
                                   
                                            this.localTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate1
                        */

                        
                                    protected double localTaxRate1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate1Tracker = false ;

                           public boolean isTaxRate1Specified(){
                               return localTaxRate1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate1(){
                               return localTaxRate1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate1
                               */
                               public void setTaxRate1(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate1Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate1=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate2
                        */

                        
                                    protected double localTaxRate2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate2Tracker = false ;

                           public boolean isTaxRate2Specified(){
                               return localTaxRate2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate2(){
                               return localTaxRate2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate2
                               */
                               public void setTaxRate2(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate2Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate2=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationSched
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAmortizationSched ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationSchedTracker = false ;

                           public boolean isAmortizationSchedSpecified(){
                               return localAmortizationSchedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAmortizationSched(){
                               return localAmortizationSched;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationSched
                               */
                               public void setAmortizationSched(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAmortizationSchedTracker = param != null;
                                   
                                            this.localAmortizationSched=param;
                                    

                               }
                            

                        /**
                        * field for AmortizStartDate
                        */

                        
                                    protected java.util.Calendar localAmortizStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizStartDateTracker = false ;

                           public boolean isAmortizStartDateSpecified(){
                               return localAmortizStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getAmortizStartDate(){
                               return localAmortizStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizStartDate
                               */
                               public void setAmortizStartDate(java.util.Calendar param){
                            localAmortizStartDateTracker = param != null;
                                   
                                            this.localAmortizStartDate=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationEndDate
                        */

                        
                                    protected java.util.Calendar localAmortizationEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationEndDateTracker = false ;

                           public boolean isAmortizationEndDateSpecified(){
                               return localAmortizationEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getAmortizationEndDate(){
                               return localAmortizationEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationEndDate
                               */
                               public void setAmortizationEndDate(java.util.Calendar param){
                            localAmortizationEndDateTracker = param != null;
                                   
                                            this.localAmortizationEndDate=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationResidual
                        */

                        
                                    protected java.lang.String localAmortizationResidual ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationResidualTracker = false ;

                           public boolean isAmortizationResidualSpecified(){
                               return localAmortizationResidualTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAmortizationResidual(){
                               return localAmortizationResidual;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationResidual
                               */
                               public void setAmortizationResidual(java.lang.String param){
                            localAmortizationResidualTracker = param != null;
                                   
                                            this.localAmortizationResidual=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:purchases_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":VendorBillExpense",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "VendorBillExpense",
                           xmlWriter);
                   }

               
                   }
                if (localOrderDocTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderDoc", xmlWriter);
                             
                                               if (localOrderDoc==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("orderDoc cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderDoc));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOrderLineTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderLine", xmlWriter);
                             
                                               if (localOrderLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("orderLine cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLineTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "line", xmlWriter);
                             
                                               if (localLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("line cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCategoryTracker){
                                            if (localCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                            }
                                           localCategory.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","category"),
                                               xmlWriter);
                                        } if (localAccountTracker){
                                            if (localAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                            }
                                           localAccount.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","account"),
                                               xmlWriter);
                                        } if (localAmountTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxAmountTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax1AmtTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax1Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax1Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax1Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMemoTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGrossAmtTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "grossAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGrossAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("grossAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxDetailsReferenceTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxDetailsReference", xmlWriter);
                             

                                          if (localTaxDetailsReference==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTaxDetailsReference);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localCustomerTracker){
                                            if (localCustomer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                            }
                                           localCustomer.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customer"),
                                               xmlWriter);
                                        } if (localIsBillableTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isBillable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isBillable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsBillable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localProjectTaskTracker){
                                            if (localProjectTask==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectTask cannot be null!!");
                                            }
                                           localProjectTask.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","projectTask"),
                                               xmlWriter);
                                        } if (localTaxCodeTracker){
                                            if (localTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                            }
                                           localTaxCode.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxCode"),
                                               xmlWriter);
                                        } if (localTaxRate1Tracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate1", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate1)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate1 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxRate2Tracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate2", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate2)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate2 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmortizationSchedTracker){
                                            if (localAmortizationSched==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amortizationSched cannot be null!!");
                                            }
                                           localAmortizationSched.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amortizationSched"),
                                               xmlWriter);
                                        } if (localAmortizStartDateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizStartDate", xmlWriter);
                             

                                          if (localAmortizStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("amortizStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmortizationEndDateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizationEndDate", xmlWriter);
                             

                                          if (localAmortizationEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("amortizationEndDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmortizationResidualTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizationResidual", xmlWriter);
                             

                                          if (localAmortizationResidual==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("amortizationResidual cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAmortizationResidual);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:purchases_2017_2.transactions.webservices.netsuite.com")){
                return "ns23";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localOrderDocTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderDoc"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderDoc));
                            } if (localOrderLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderLine"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderLine));
                            } if (localLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "line"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                            } if (localCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "category"));
                            
                            
                                    if (localCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                    }
                                    elementList.add(localCategory);
                                } if (localAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "account"));
                            
                            
                                    if (localAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                    }
                                    elementList.add(localAccount);
                                } if (localAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "amount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                            } if (localTaxAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                            } if (localTax1AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax1Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                            } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localGrossAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "grossAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                            } if (localTaxDetailsReferenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsReference"));
                                 
                                        if (localTaxDetailsReference != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsReference));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                        }
                                    } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localCustomerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "customer"));
                            
                            
                                    if (localCustomer==null){
                                         throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                    }
                                    elementList.add(localCustomer);
                                } if (localIsBillableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "isBillable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsBillable));
                            } if (localProjectTaskTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "projectTask"));
                            
                            
                                    if (localProjectTask==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectTask cannot be null!!");
                                    }
                                    elementList.add(localProjectTask);
                                } if (localTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxCode"));
                            
                            
                                    if (localTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                    }
                                    elementList.add(localTaxCode);
                                } if (localTaxRate1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate1"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                            } if (localTaxRate2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate2"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                            } if (localAmortizationSchedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizationSched"));
                            
                            
                                    if (localAmortizationSched==null){
                                         throw new org.apache.axis2.databinding.ADBException("amortizationSched cannot be null!!");
                                    }
                                    elementList.add(localAmortizationSched);
                                } if (localAmortizStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizStartDate"));
                                 
                                        if (localAmortizStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("amortizStartDate cannot be null!!");
                                        }
                                    } if (localAmortizationEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizationEndDate"));
                                 
                                        if (localAmortizationEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("amortizationEndDate cannot be null!!");
                                        }
                                    } if (localAmortizationResidualTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizationResidual"));
                                 
                                        if (localAmortizationResidual != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationResidual));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("amortizationResidual cannot be null!!");
                                        }
                                    } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static VendorBillExpense parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            VendorBillExpense object =
                new VendorBillExpense();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"VendorBillExpense".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (VendorBillExpense)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","orderDoc").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderDoc" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderDoc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOrderDoc(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","orderLine").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderLine" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOrderLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","line").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"line" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","category").equals(reader.getName())){
                                
                                                object.setCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","account").equals(reader.getName())){
                                
                                                object.setAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","tax1Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax1Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax1Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax1Amt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","grossAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"grossAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGrossAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGrossAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxDetailsReference").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxDetailsReference" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxDetailsReference(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customer").equals(reader.getName())){
                                
                                                object.setCustomer(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","isBillable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isBillable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsBillable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","projectTask").equals(reader.getName())){
                                
                                                object.setProjectTask(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxCode").equals(reader.getName())){
                                
                                                object.setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxRate1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate1(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxRate2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate2(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amortizationSched").equals(reader.getName())){
                                
                                                object.setAmortizationSched(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amortizStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amortizationEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizationEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizationEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amortizationResidual").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizationResidual" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizationResidual(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    