
/**
 * PurchaseOrderItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.purchases_2017_2;
            

            /**
            *  PurchaseOrderItem bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class PurchaseOrderItem
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = PurchaseOrderItem
                Namespace URI = urn:purchases_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns23
                */
            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for Line
                        */

                        
                                    protected long localLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineTracker = false ;

                           public boolean isLineSpecified(){
                               return localLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLine(){
                               return localLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Line
                               */
                               public void setLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLine=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnShipments
                        */

                        
                                    protected double localQuantityOnShipments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnShipmentsTracker = false ;

                           public boolean isQuantityOnShipmentsSpecified(){
                               return localQuantityOnShipmentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnShipments(){
                               return localQuantityOnShipments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnShipments
                               */
                               public void setQuantityOnShipments(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnShipmentsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnShipments=param;
                                    

                               }
                            

                        /**
                        * field for VendorName
                        */

                        
                                    protected java.lang.String localVendorName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorNameTracker = false ;

                           public boolean isVendorNameSpecified(){
                               return localVendorNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getVendorName(){
                               return localVendorName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorName
                               */
                               public void setVendorName(java.lang.String param){
                            localVendorNameTracker = param != null;
                                   
                                            this.localVendorName=param;
                                    

                               }
                            

                        /**
                        * field for QuantityReceived
                        */

                        
                                    protected double localQuantityReceived ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityReceivedTracker = false ;

                           public boolean isQuantityReceivedSpecified(){
                               return localQuantityReceivedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityReceived(){
                               return localQuantityReceived;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityReceived
                               */
                               public void setQuantityReceived(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityReceivedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityReceived=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBilled
                        */

                        
                                    protected double localQuantityBilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBilledTracker = false ;

                           public boolean isQuantityBilledSpecified(){
                               return localQuantityBilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityBilled(){
                               return localQuantityBilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBilled
                               */
                               public void setQuantityBilled(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityBilledTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityBilled=param;
                                    

                               }
                            

                        /**
                        * field for QuantityAvailable
                        */

                        
                                    protected double localQuantityAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityAvailableTracker = false ;

                           public boolean isQuantityAvailableSpecified(){
                               return localQuantityAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityAvailable(){
                               return localQuantityAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityAvailable
                               */
                               public void setQuantityAvailable(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityAvailableTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityAvailable=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnHand
                        */

                        
                                    protected double localQuantityOnHand ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnHandTracker = false ;

                           public boolean isQuantityOnHandSpecified(){
                               return localQuantityOnHandTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnHand(){
                               return localQuantityOnHand;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnHand
                               */
                               public void setQuantityOnHand(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnHandTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnHand=param;
                                    

                               }
                            

                        /**
                        * field for TaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxCodeTracker = false ;

                           public boolean isTaxCodeSpecified(){
                               return localTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxCode(){
                               return localTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxCode
                               */
                               public void setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxCodeTracker = param != null;
                                   
                                            this.localTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate1
                        */

                        
                                    protected double localTaxRate1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate1Tracker = false ;

                           public boolean isTaxRate1Specified(){
                               return localTaxRate1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate1(){
                               return localTaxRate1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate1
                               */
                               public void setTaxRate1(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate1Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate1=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate2
                        */

                        
                                    protected double localTaxRate2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate2Tracker = false ;

                           public boolean isTaxRate2Specified(){
                               return localTaxRate2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate2(){
                               return localTaxRate2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate2
                               */
                               public void setTaxRate2(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate2Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate2=param;
                                    

                               }
                            

                        /**
                        * field for Quantity
                        */

                        
                                    protected double localQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityTracker = false ;

                           public boolean isQuantitySpecified(){
                               return localQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantity(){
                               return localQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Quantity
                               */
                               public void setQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantity=param;
                                    

                               }
                            

                        /**
                        * field for Tax1Amt
                        */

                        
                                    protected double localTax1Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax1AmtTracker = false ;

                           public boolean isTax1AmtSpecified(){
                               return localTax1AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax1Amt(){
                               return localTax1Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax1Amt
                               */
                               public void setTax1Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax1AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax1Amt=param;
                                    

                               }
                            

                        /**
                        * field for GrossAmt
                        */

                        
                                    protected double localGrossAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGrossAmtTracker = false ;

                           public boolean isGrossAmtSpecified(){
                               return localGrossAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGrossAmt(){
                               return localGrossAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GrossAmt
                               */
                               public void setGrossAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGrossAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGrossAmt=param;
                                    

                               }
                            

                        /**
                        * field for Units
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTracker = false ;

                           public boolean isUnitsSpecified(){
                               return localUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnits(){
                               return localUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Units
                               */
                               public void setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitsTracker = param != null;
                                   
                                            this.localUnits=param;
                                    

                               }
                            

                        /**
                        * field for InventoryDetail
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryDetail localInventoryDetail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryDetailTracker = false ;

                           public boolean isInventoryDetailSpecified(){
                               return localInventoryDetailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryDetail
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryDetail getInventoryDetail(){
                               return localInventoryDetail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryDetail
                               */
                               public void setInventoryDetail(com.netsuite.webservices.platform.common_2017_2.InventoryDetail param){
                            localInventoryDetailTracker = param != null;
                                   
                                            this.localInventoryDetail=param;
                                    

                               }
                            

                        /**
                        * field for SerialNumbers
                        */

                        
                                    protected java.lang.String localSerialNumbers ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSerialNumbersTracker = false ;

                           public boolean isSerialNumbersSpecified(){
                               return localSerialNumbersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSerialNumbers(){
                               return localSerialNumbers;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SerialNumbers
                               */
                               public void setSerialNumbers(java.lang.String param){
                            localSerialNumbersTracker = param != null;
                                   
                                            this.localSerialNumbers=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseContract
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPurchaseContract ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseContractTracker = false ;

                           public boolean isPurchaseContractSpecified(){
                               return localPurchaseContractTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPurchaseContract(){
                               return localPurchaseContract;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseContract
                               */
                               public void setPurchaseContract(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPurchaseContractTracker = param != null;
                                   
                                            this.localPurchaseContract=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected java.lang.String localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(java.lang.String param){
                            localRateTracker = param != null;
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected double localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for Options
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localOptions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOptionsTracker = false ;

                           public boolean isOptionsSpecified(){
                               return localOptionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getOptions(){
                               return localOptions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Options
                               */
                               public void setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localOptionsTracker = param != null;
                                   
                                            this.localOptions=param;
                                    

                               }
                            

                        /**
                        * field for TaxAmount
                        */

                        
                                    protected double localTaxAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxAmountTracker = false ;

                           public boolean isTaxAmountSpecified(){
                               return localTaxAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxAmount(){
                               return localTaxAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxAmount
                               */
                               public void setTaxAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxAmount=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for LandedCostCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLandedCostCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLandedCostCategoryTracker = false ;

                           public boolean isLandedCostCategorySpecified(){
                               return localLandedCostCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLandedCostCategory(){
                               return localLandedCostCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LandedCostCategory
                               */
                               public void setLandedCostCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLandedCostCategoryTracker = param != null;
                                   
                                            this.localLandedCostCategory=param;
                                    

                               }
                            

                        /**
                        * field for Customer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerTracker = false ;

                           public boolean isCustomerSpecified(){
                               return localCustomerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomer(){
                               return localCustomer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Customer
                               */
                               public void setCustomer(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomerTracker = param != null;
                                   
                                            this.localCustomer=param;
                                    

                               }
                            

                        /**
                        * field for IsBillable
                        */

                        
                                    protected boolean localIsBillable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsBillableTracker = false ;

                           public boolean isIsBillableSpecified(){
                               return localIsBillableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsBillable(){
                               return localIsBillable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsBillable
                               */
                               public void setIsBillable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsBillableTracker =
                                       true;
                                   
                                            this.localIsBillable=param;
                                    

                               }
                            

                        /**
                        * field for BillVarianceStatus
                        */

                        
                                    protected com.netsuite.webservices.transactions.purchases_2017_2.types.TransactionBillVarianceStatus localBillVarianceStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillVarianceStatusTracker = false ;

                           public boolean isBillVarianceStatusSpecified(){
                               return localBillVarianceStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.purchases_2017_2.types.TransactionBillVarianceStatus
                           */
                           public  com.netsuite.webservices.transactions.purchases_2017_2.types.TransactionBillVarianceStatus getBillVarianceStatus(){
                               return localBillVarianceStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillVarianceStatus
                               */
                               public void setBillVarianceStatus(com.netsuite.webservices.transactions.purchases_2017_2.types.TransactionBillVarianceStatus param){
                            localBillVarianceStatusTracker = param != null;
                                   
                                            this.localBillVarianceStatus=param;
                                    

                               }
                            

                        /**
                        * field for MatchBillToReceipt
                        */

                        
                                    protected boolean localMatchBillToReceipt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatchBillToReceiptTracker = false ;

                           public boolean isMatchBillToReceiptSpecified(){
                               return localMatchBillToReceiptTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getMatchBillToReceipt(){
                               return localMatchBillToReceipt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatchBillToReceipt
                               */
                               public void setMatchBillToReceipt(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localMatchBillToReceiptTracker =
                                       true;
                                   
                                            this.localMatchBillToReceipt=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedReceiptDate
                        */

                        
                                    protected java.util.Calendar localExpectedReceiptDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedReceiptDateTracker = false ;

                           public boolean isExpectedReceiptDateSpecified(){
                               return localExpectedReceiptDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getExpectedReceiptDate(){
                               return localExpectedReceiptDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedReceiptDate
                               */
                               public void setExpectedReceiptDate(java.util.Calendar param){
                            localExpectedReceiptDateTracker = param != null;
                                   
                                            this.localExpectedReceiptDate=param;
                                    

                               }
                            

                        /**
                        * field for IsClosed
                        */

                        
                                    protected boolean localIsClosed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsClosedTracker = false ;

                           public boolean isIsClosedSpecified(){
                               return localIsClosedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsClosed(){
                               return localIsClosed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsClosed
                               */
                               public void setIsClosed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsClosedTracker =
                                       true;
                                   
                                            this.localIsClosed=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsReference
                        */

                        
                                    protected java.lang.String localTaxDetailsReference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsReferenceTracker = false ;

                           public boolean isTaxDetailsReferenceSpecified(){
                               return localTaxDetailsReferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTaxDetailsReference(){
                               return localTaxDetailsReference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsReference
                               */
                               public void setTaxDetailsReference(java.lang.String param){
                            localTaxDetailsReferenceTracker = param != null;
                                   
                                            this.localTaxDetailsReference=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromTracker = false ;

                           public boolean isCreatedFromSpecified(){
                               return localCreatedFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedFrom(){
                               return localCreatedFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFrom
                               */
                               public void setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedFromTracker = param != null;
                                   
                                            this.localCreatedFrom=param;
                                    

                               }
                            

                        /**
                        * field for LinkedOrderList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localLinkedOrderList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLinkedOrderListTracker = false ;

                           public boolean isLinkedOrderListSpecified(){
                               return localLinkedOrderListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getLinkedOrderList(){
                               return localLinkedOrderList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LinkedOrderList
                               */
                               public void setLinkedOrderList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localLinkedOrderListTracker = param != null;
                                   
                                            this.localLinkedOrderList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:purchases_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":PurchaseOrderItem",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "PurchaseOrderItem",
                           xmlWriter);
                   }

               
                   }
                if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localLineTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "line", xmlWriter);
                             
                                               if (localLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("line cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityOnShipmentsTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnShipments", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnShipments)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnShipments cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnShipments));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVendorNameTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vendorName", xmlWriter);
                             

                                          if (localVendorName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("vendorName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localVendorName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityReceivedTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityReceived", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityReceived)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityReceived cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityReceived));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityBilledTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityBilled", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityBilled)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityBilled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBilled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityAvailableTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityAvailable", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityAvailable)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityAvailable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityOnHandTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnHand", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnHand)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnHand cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxCodeTracker){
                                            if (localTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                            }
                                           localTaxCode.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxCode"),
                                               xmlWriter);
                                        } if (localTaxRate1Tracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate1", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate1)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate1 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxRate2Tracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate2", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate2)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate2 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax1AmtTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax1Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax1Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax1Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGrossAmtTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "grossAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGrossAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("grossAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnitsTracker){
                                            if (localUnits==null){
                                                 throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                            }
                                           localUnits.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","units"),
                                               xmlWriter);
                                        } if (localInventoryDetailTracker){
                                            if (localInventoryDetail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryDetail cannot be null!!");
                                            }
                                           localInventoryDetail.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","inventoryDetail"),
                                               xmlWriter);
                                        } if (localSerialNumbersTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "serialNumbers", xmlWriter);
                             

                                          if (localSerialNumbers==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSerialNumbers);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDescriptionTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseContractTracker){
                                            if (localPurchaseContract==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseContract cannot be null!!");
                                            }
                                           localPurchaseContract.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","purchaseContract"),
                                               xmlWriter);
                                        } if (localRateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             

                                          if (localRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmountTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOptionsTracker){
                                            if (localOptions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                            }
                                           localOptions.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","options"),
                                               xmlWriter);
                                        } if (localTaxAmountTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localLandedCostCategoryTracker){
                                            if (localLandedCostCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("landedCostCategory cannot be null!!");
                                            }
                                           localLandedCostCategory.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostCategory"),
                                               xmlWriter);
                                        } if (localCustomerTracker){
                                            if (localCustomer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                            }
                                           localCustomer.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customer"),
                                               xmlWriter);
                                        } if (localIsBillableTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isBillable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isBillable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsBillable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillVarianceStatusTracker){
                                            if (localBillVarianceStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billVarianceStatus cannot be null!!");
                                            }
                                           localBillVarianceStatus.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","billVarianceStatus"),
                                               xmlWriter);
                                        } if (localMatchBillToReceiptTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "matchBillToReceipt", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("matchBillToReceipt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMatchBillToReceipt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExpectedReceiptDateTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "expectedReceiptDate", xmlWriter);
                             

                                          if (localExpectedReceiptDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("expectedReceiptDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedReceiptDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsClosedTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isClosed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isClosed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsClosed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxDetailsReferenceTracker){
                                    namespace = "urn:purchases_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxDetailsReference", xmlWriter);
                             

                                          if (localTaxDetailsReference==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTaxDetailsReference);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedFromTracker){
                                            if (localCreatedFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                            }
                                           localCreatedFrom.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","createdFrom"),
                                               xmlWriter);
                                        } if (localLinkedOrderListTracker){
                                            if (localLinkedOrderList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("linkedOrderList cannot be null!!");
                                            }
                                           localLinkedOrderList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","linkedOrderList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:purchases_2017_2.transactions.webservices.netsuite.com")){
                return "ns23";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "line"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                            } if (localQuantityOnShipmentsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityOnShipments"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnShipments));
                            } if (localVendorNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "vendorName"));
                                 
                                        if (localVendorName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVendorName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("vendorName cannot be null!!");
                                        }
                                    } if (localQuantityReceivedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityReceived"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityReceived));
                            } if (localQuantityBilledTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityBilled"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBilled));
                            } if (localQuantityAvailableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityAvailable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                            } if (localQuantityOnHandTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityOnHand"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                            } if (localTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxCode"));
                            
                            
                                    if (localTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                    }
                                    elementList.add(localTaxCode);
                                } if (localTaxRate1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate1"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                            } if (localTaxRate2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate2"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                            } if (localQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                            } if (localTax1AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax1Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                            } if (localGrossAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "grossAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                            } if (localUnitsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "units"));
                            
                            
                                    if (localUnits==null){
                                         throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                    }
                                    elementList.add(localUnits);
                                } if (localInventoryDetailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "inventoryDetail"));
                            
                            
                                    if (localInventoryDetail==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryDetail cannot be null!!");
                                    }
                                    elementList.add(localInventoryDetail);
                                } if (localSerialNumbersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "serialNumbers"));
                                 
                                        if (localSerialNumbers != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSerialNumbers));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                        }
                                    } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localPurchaseContractTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "purchaseContract"));
                            
                            
                                    if (localPurchaseContract==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseContract cannot be null!!");
                                    }
                                    elementList.add(localPurchaseContract);
                                } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                        if (localRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                        }
                                    } if (localAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "amount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                            } if (localOptionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "options"));
                            
                            
                                    if (localOptions==null){
                                         throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                    }
                                    elementList.add(localOptions);
                                } if (localTaxAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                            } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localLandedCostCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "landedCostCategory"));
                            
                            
                                    if (localLandedCostCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("landedCostCategory cannot be null!!");
                                    }
                                    elementList.add(localLandedCostCategory);
                                } if (localCustomerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "customer"));
                            
                            
                                    if (localCustomer==null){
                                         throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                    }
                                    elementList.add(localCustomer);
                                } if (localIsBillableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "isBillable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsBillable));
                            } if (localBillVarianceStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "billVarianceStatus"));
                            
                            
                                    if (localBillVarianceStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("billVarianceStatus cannot be null!!");
                                    }
                                    elementList.add(localBillVarianceStatus);
                                } if (localMatchBillToReceiptTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "matchBillToReceipt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMatchBillToReceipt));
                            } if (localExpectedReceiptDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "expectedReceiptDate"));
                                 
                                        if (localExpectedReceiptDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedReceiptDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("expectedReceiptDate cannot be null!!");
                                        }
                                    } if (localIsClosedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "isClosed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsClosed));
                            } if (localTaxDetailsReferenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsReference"));
                                 
                                        if (localTaxDetailsReference != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsReference));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                        }
                                    } if (localCreatedFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFrom"));
                            
                            
                                    if (localCreatedFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                    }
                                    elementList.add(localCreatedFrom);
                                } if (localLinkedOrderListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "linkedOrderList"));
                            
                            
                                    if (localLinkedOrderList==null){
                                         throw new org.apache.axis2.databinding.ADBException("linkedOrderList cannot be null!!");
                                    }
                                    elementList.add(localLinkedOrderList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static PurchaseOrderItem parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            PurchaseOrderItem object =
                new PurchaseOrderItem();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"PurchaseOrderItem".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (PurchaseOrderItem)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","line").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"line" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityOnShipments").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnShipments" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnShipments(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnShipments(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","vendorName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vendorName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVendorName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityReceived").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityReceived" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityReceived(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityReceived(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityBilled").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityBilled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityBilled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityBilled(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityAvailable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityAvailable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityAvailable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityAvailable(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantityOnHand").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnHand" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnHand(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnHand(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxCode").equals(reader.getName())){
                                
                                                object.setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxRate1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate1(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxRate2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate2(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","quantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","tax1Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax1Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax1Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax1Amt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","grossAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"grossAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGrossAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGrossAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","units").equals(reader.getName())){
                                
                                                object.setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","inventoryDetail").equals(reader.getName())){
                                
                                                object.setInventoryDetail(com.netsuite.webservices.platform.common_2017_2.InventoryDetail.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","serialNumbers").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"serialNumbers" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSerialNumbers(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","purchaseContract").equals(reader.getName())){
                                
                                                object.setPurchaseContract(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","options").equals(reader.getName())){
                                
                                                object.setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","landedCostCategory").equals(reader.getName())){
                                
                                                object.setLandedCostCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customer").equals(reader.getName())){
                                
                                                object.setCustomer(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","isBillable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isBillable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsBillable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","billVarianceStatus").equals(reader.getName())){
                                
                                                object.setBillVarianceStatus(com.netsuite.webservices.transactions.purchases_2017_2.types.TransactionBillVarianceStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","matchBillToReceipt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"matchBillToReceipt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMatchBillToReceipt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","expectedReceiptDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"expectedReceiptDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpectedReceiptDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","isClosed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isClosed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsClosed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","taxDetailsReference").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxDetailsReference" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxDetailsReference(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","createdFrom").equals(reader.getName())){
                                
                                                object.setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","linkedOrderList").equals(reader.getName())){
                                
                                                object.setLinkedOrderList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:purchases_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    