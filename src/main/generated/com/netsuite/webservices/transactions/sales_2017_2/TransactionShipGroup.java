
/**
 * TransactionShipGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  TransactionShipGroup bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionShipGroup
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TransactionShipGroup
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for Id
                        */

                        
                                    protected long localId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdTracker = false ;

                           public boolean isIdSpecified(){
                               return localIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getId(){
                               return localId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Id
                               */
                               public void setId(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localIdTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localId=param;
                                    

                               }
                            

                        /**
                        * field for IsFulfilled
                        */

                        
                                    protected boolean localIsFulfilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsFulfilledTracker = false ;

                           public boolean isIsFulfilledSpecified(){
                               return localIsFulfilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsFulfilled(){
                               return localIsFulfilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsFulfilled
                               */
                               public void setIsFulfilled(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsFulfilledTracker =
                                       true;
                                   
                                            this.localIsFulfilled=param;
                                    

                               }
                            

                        /**
                        * field for Weight
                        */

                        
                                    protected double localWeight ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWeightTracker = false ;

                           public boolean isWeightSpecified(){
                               return localWeightTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getWeight(){
                               return localWeight;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Weight
                               */
                               public void setWeight(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localWeightTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localWeight=param;
                                    

                               }
                            

                        /**
                        * field for SourceAddressRef
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSourceAddressRef ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceAddressRefTracker = false ;

                           public boolean isSourceAddressRefSpecified(){
                               return localSourceAddressRefTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSourceAddressRef(){
                               return localSourceAddressRef;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceAddressRef
                               */
                               public void setSourceAddressRef(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSourceAddressRefTracker = param != null;
                                   
                                            this.localSourceAddressRef=param;
                                    

                               }
                            

                        /**
                        * field for SourceAddress
                        */

                        
                                    protected java.lang.String localSourceAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceAddressTracker = false ;

                           public boolean isSourceAddressSpecified(){
                               return localSourceAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSourceAddress(){
                               return localSourceAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceAddress
                               */
                               public void setSourceAddress(java.lang.String param){
                            localSourceAddressTracker = param != null;
                                   
                                            this.localSourceAddress=param;
                                    

                               }
                            

                        /**
                        * field for DestinationAddressRef
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDestinationAddressRef ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDestinationAddressRefTracker = false ;

                           public boolean isDestinationAddressRefSpecified(){
                               return localDestinationAddressRefTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDestinationAddressRef(){
                               return localDestinationAddressRef;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DestinationAddressRef
                               */
                               public void setDestinationAddressRef(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDestinationAddressRefTracker = param != null;
                                   
                                            this.localDestinationAddressRef=param;
                                    

                               }
                            

                        /**
                        * field for DestinationAddress
                        */

                        
                                    protected java.lang.String localDestinationAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDestinationAddressTracker = false ;

                           public boolean isDestinationAddressSpecified(){
                               return localDestinationAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDestinationAddress(){
                               return localDestinationAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DestinationAddress
                               */
                               public void setDestinationAddress(java.lang.String param){
                            localDestinationAddressTracker = param != null;
                                   
                                            this.localDestinationAddress=param;
                                    

                               }
                            

                        /**
                        * field for ShippingMethodRef
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShippingMethodRef ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingMethodRefTracker = false ;

                           public boolean isShippingMethodRefSpecified(){
                               return localShippingMethodRefTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShippingMethodRef(){
                               return localShippingMethodRef;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingMethodRef
                               */
                               public void setShippingMethodRef(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShippingMethodRefTracker = param != null;
                                   
                                            this.localShippingMethodRef=param;
                                    

                               }
                            

                        /**
                        * field for ShippingMethod
                        */

                        
                                    protected java.lang.String localShippingMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingMethodTracker = false ;

                           public boolean isShippingMethodSpecified(){
                               return localShippingMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShippingMethod(){
                               return localShippingMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingMethod
                               */
                               public void setShippingMethod(java.lang.String param){
                            localShippingMethodTracker = param != null;
                                   
                                            this.localShippingMethod=param;
                                    

                               }
                            

                        /**
                        * field for IsHandlingTaxable
                        */

                        
                                    protected boolean localIsHandlingTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsHandlingTaxableTracker = false ;

                           public boolean isIsHandlingTaxableSpecified(){
                               return localIsHandlingTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsHandlingTaxable(){
                               return localIsHandlingTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsHandlingTaxable
                               */
                               public void setIsHandlingTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsHandlingTaxableTracker =
                                       true;
                                   
                                            this.localIsHandlingTaxable=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localHandlingTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTaxCodeTracker = false ;

                           public boolean isHandlingTaxCodeSpecified(){
                               return localHandlingTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getHandlingTaxCode(){
                               return localHandlingTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTaxCode
                               */
                               public void setHandlingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localHandlingTaxCodeTracker = param != null;
                                   
                                            this.localHandlingTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTaxRate
                        */

                        
                                    protected java.lang.String localHandlingTaxRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTaxRateTracker = false ;

                           public boolean isHandlingTaxRateSpecified(){
                               return localHandlingTaxRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHandlingTaxRate(){
                               return localHandlingTaxRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTaxRate
                               */
                               public void setHandlingTaxRate(java.lang.String param){
                            localHandlingTaxRateTracker = param != null;
                                   
                                            this.localHandlingTaxRate=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTax2Rate
                        */

                        
                                    protected java.lang.String localHandlingTax2Rate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTax2RateTracker = false ;

                           public boolean isHandlingTax2RateSpecified(){
                               return localHandlingTax2RateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHandlingTax2Rate(){
                               return localHandlingTax2Rate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTax2Rate
                               */
                               public void setHandlingTax2Rate(java.lang.String param){
                            localHandlingTax2RateTracker = param != null;
                                   
                                            this.localHandlingTax2Rate=param;
                                    

                               }
                            

                        /**
                        * field for HandlingRate
                        */

                        
                                    protected double localHandlingRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingRateTracker = false ;

                           public boolean isHandlingRateSpecified(){
                               return localHandlingRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingRate(){
                               return localHandlingRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingRate
                               */
                               public void setHandlingRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingRate=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTaxAmt
                        */

                        
                                    protected double localHandlingTaxAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTaxAmtTracker = false ;

                           public boolean isHandlingTaxAmtSpecified(){
                               return localHandlingTaxAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingTaxAmt(){
                               return localHandlingTaxAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTaxAmt
                               */
                               public void setHandlingTaxAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingTaxAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingTaxAmt=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTax2Amt
                        */

                        
                                    protected double localHandlingTax2Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTax2AmtTracker = false ;

                           public boolean isHandlingTax2AmtSpecified(){
                               return localHandlingTax2AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingTax2Amt(){
                               return localHandlingTax2Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTax2Amt
                               */
                               public void setHandlingTax2Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingTax2AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingTax2Amt=param;
                                    

                               }
                            

                        /**
                        * field for IsShippingTaxable
                        */

                        
                                    protected boolean localIsShippingTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsShippingTaxableTracker = false ;

                           public boolean isIsShippingTaxableSpecified(){
                               return localIsShippingTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsShippingTaxable(){
                               return localIsShippingTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsShippingTaxable
                               */
                               public void setIsShippingTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsShippingTaxableTracker =
                                       true;
                                   
                                            this.localIsShippingTaxable=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShippingTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTaxCodeTracker = false ;

                           public boolean isShippingTaxCodeSpecified(){
                               return localShippingTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShippingTaxCode(){
                               return localShippingTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTaxCode
                               */
                               public void setShippingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShippingTaxCodeTracker = param != null;
                                   
                                            this.localShippingTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTaxRate
                        */

                        
                                    protected java.lang.String localShippingTaxRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTaxRateTracker = false ;

                           public boolean isShippingTaxRateSpecified(){
                               return localShippingTaxRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShippingTaxRate(){
                               return localShippingTaxRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTaxRate
                               */
                               public void setShippingTaxRate(java.lang.String param){
                            localShippingTaxRateTracker = param != null;
                                   
                                            this.localShippingTaxRate=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTax2Rate
                        */

                        
                                    protected java.lang.String localShippingTax2Rate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTax2RateTracker = false ;

                           public boolean isShippingTax2RateSpecified(){
                               return localShippingTax2RateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShippingTax2Rate(){
                               return localShippingTax2Rate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTax2Rate
                               */
                               public void setShippingTax2Rate(java.lang.String param){
                            localShippingTax2RateTracker = param != null;
                                   
                                            this.localShippingTax2Rate=param;
                                    

                               }
                            

                        /**
                        * field for ShippingRate
                        */

                        
                                    protected double localShippingRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingRateTracker = false ;

                           public boolean isShippingRateSpecified(){
                               return localShippingRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingRate(){
                               return localShippingRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingRate
                               */
                               public void setShippingRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingRate=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTaxAmt
                        */

                        
                                    protected double localShippingTaxAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTaxAmtTracker = false ;

                           public boolean isShippingTaxAmtSpecified(){
                               return localShippingTaxAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingTaxAmt(){
                               return localShippingTaxAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTaxAmt
                               */
                               public void setShippingTaxAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingTaxAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingTaxAmt=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTax2Amt
                        */

                        
                                    protected double localShippingTax2Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTax2AmtTracker = false ;

                           public boolean isShippingTax2AmtSpecified(){
                               return localShippingTax2AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingTax2Amt(){
                               return localShippingTax2Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTax2Amt
                               */
                               public void setShippingTax2Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingTax2AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingTax2Amt=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TransactionShipGroup",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TransactionShipGroup",
                           xmlWriter);
                   }

               
                   }
                if (localIdTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "id", xmlWriter);
                             
                                               if (localId==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsFulfilledTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isFulfilled", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isFulfilled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFulfilled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWeightTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "weight", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localWeight)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeight));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSourceAddressRefTracker){
                                            if (localSourceAddressRef==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sourceAddressRef cannot be null!!");
                                            }
                                           localSourceAddressRef.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sourceAddressRef"),
                                               xmlWriter);
                                        } if (localSourceAddressTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sourceAddress", xmlWriter);
                             

                                          if (localSourceAddress==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("sourceAddress cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSourceAddress);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDestinationAddressRefTracker){
                                            if (localDestinationAddressRef==null){
                                                 throw new org.apache.axis2.databinding.ADBException("destinationAddressRef cannot be null!!");
                                            }
                                           localDestinationAddressRef.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","destinationAddressRef"),
                                               xmlWriter);
                                        } if (localDestinationAddressTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "destinationAddress", xmlWriter);
                             

                                          if (localDestinationAddress==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("destinationAddress cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDestinationAddress);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingMethodRefTracker){
                                            if (localShippingMethodRef==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingMethodRef cannot be null!!");
                                            }
                                           localShippingMethodRef.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingMethodRef"),
                                               xmlWriter);
                                        } if (localShippingMethodTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingMethod", xmlWriter);
                             

                                          if (localShippingMethod==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shippingMethod cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShippingMethod);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsHandlingTaxableTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isHandlingTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isHandlingTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsHandlingTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingTaxCodeTracker){
                                            if (localHandlingTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("handlingTaxCode cannot be null!!");
                                            }
                                           localHandlingTaxCode.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTaxCode"),
                                               xmlWriter);
                                        } if (localHandlingTaxRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingTaxRate", xmlWriter);
                             

                                          if (localHandlingTaxRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("handlingTaxRate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHandlingTaxRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingTax2RateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingTax2Rate", xmlWriter);
                             

                                          if (localHandlingTax2Rate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("handlingTax2Rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHandlingTax2Rate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingTaxAmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingTaxAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingTaxAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingTaxAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTaxAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingTax2AmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingTax2Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingTax2Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingTax2Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTax2Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsShippingTaxableTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isShippingTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isShippingTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsShippingTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTaxCodeTracker){
                                            if (localShippingTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingTaxCode cannot be null!!");
                                            }
                                           localShippingTaxCode.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTaxCode"),
                                               xmlWriter);
                                        } if (localShippingTaxRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingTaxRate", xmlWriter);
                             

                                          if (localShippingTaxRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shippingTaxRate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShippingTaxRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTax2RateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingTax2Rate", xmlWriter);
                             

                                          if (localShippingTax2Rate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shippingTax2Rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShippingTax2Rate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTaxAmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingTaxAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingTaxAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingTaxAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTaxAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTax2AmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingTax2Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingTax2Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingTax2Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTax2Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
                            } if (localIsFulfilledTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isFulfilled"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFulfilled));
                            } if (localWeightTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "weight"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeight));
                            } if (localSourceAddressRefTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "sourceAddressRef"));
                            
                            
                                    if (localSourceAddressRef==null){
                                         throw new org.apache.axis2.databinding.ADBException("sourceAddressRef cannot be null!!");
                                    }
                                    elementList.add(localSourceAddressRef);
                                } if (localSourceAddressTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "sourceAddress"));
                                 
                                        if (localSourceAddress != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSourceAddress));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("sourceAddress cannot be null!!");
                                        }
                                    } if (localDestinationAddressRefTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "destinationAddressRef"));
                            
                            
                                    if (localDestinationAddressRef==null){
                                         throw new org.apache.axis2.databinding.ADBException("destinationAddressRef cannot be null!!");
                                    }
                                    elementList.add(localDestinationAddressRef);
                                } if (localDestinationAddressTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "destinationAddress"));
                                 
                                        if (localDestinationAddress != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDestinationAddress));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("destinationAddress cannot be null!!");
                                        }
                                    } if (localShippingMethodRefTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingMethodRef"));
                            
                            
                                    if (localShippingMethodRef==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingMethodRef cannot be null!!");
                                    }
                                    elementList.add(localShippingMethodRef);
                                } if (localShippingMethodTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingMethod"));
                                 
                                        if (localShippingMethod != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingMethod));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shippingMethod cannot be null!!");
                                        }
                                    } if (localIsHandlingTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isHandlingTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsHandlingTaxable));
                            } if (localHandlingTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTaxCode"));
                            
                            
                                    if (localHandlingTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("handlingTaxCode cannot be null!!");
                                    }
                                    elementList.add(localHandlingTaxCode);
                                } if (localHandlingTaxRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTaxRate"));
                                 
                                        if (localHandlingTaxRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTaxRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("handlingTaxRate cannot be null!!");
                                        }
                                    } if (localHandlingTax2RateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTax2Rate"));
                                 
                                        if (localHandlingTax2Rate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTax2Rate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("handlingTax2Rate cannot be null!!");
                                        }
                                    } if (localHandlingRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingRate));
                            } if (localHandlingTaxAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTaxAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTaxAmt));
                            } if (localHandlingTax2AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTax2Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTax2Amt));
                            } if (localIsShippingTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isShippingTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsShippingTaxable));
                            } if (localShippingTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTaxCode"));
                            
                            
                                    if (localShippingTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingTaxCode cannot be null!!");
                                    }
                                    elementList.add(localShippingTaxCode);
                                } if (localShippingTaxRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTaxRate"));
                                 
                                        if (localShippingTaxRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTaxRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shippingTaxRate cannot be null!!");
                                        }
                                    } if (localShippingTax2RateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTax2Rate"));
                                 
                                        if (localShippingTax2Rate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTax2Rate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shippingTax2Rate cannot be null!!");
                                        }
                                    } if (localShippingRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingRate));
                            } if (localShippingTaxAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTaxAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTaxAmt));
                            } if (localShippingTax2AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTax2Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTax2Amt));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionShipGroup parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionShipGroup object =
                new TransactionShipGroup();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TransactionShipGroup".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TransactionShipGroup)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","id").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"id" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setId(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isFulfilled").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isFulfilled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsFulfilled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","weight").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"weight" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setWeight(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setWeight(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sourceAddressRef").equals(reader.getName())){
                                
                                                object.setSourceAddressRef(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sourceAddress").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sourceAddress" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSourceAddress(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","destinationAddressRef").equals(reader.getName())){
                                
                                                object.setDestinationAddressRef(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","destinationAddress").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"destinationAddress" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDestinationAddress(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingMethodRef").equals(reader.getName())){
                                
                                                object.setShippingMethodRef(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingMethod").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingMethod" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingMethod(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isHandlingTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isHandlingTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsHandlingTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTaxCode").equals(reader.getName())){
                                
                                                object.setHandlingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTaxRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingTaxRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingTaxRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTax2Rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingTax2Rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingTax2Rate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTaxAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingTaxAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingTaxAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingTaxAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTax2Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingTax2Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingTax2Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingTax2Amt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isShippingTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isShippingTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsShippingTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTaxCode").equals(reader.getName())){
                                
                                                object.setShippingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTaxRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingTaxRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingTaxRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTax2Rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingTax2Rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingTax2Rate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTaxAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingTaxAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingTaxAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingTaxAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTax2Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingTax2Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingTax2Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingTax2Amt(java.lang.Double.NaN);
                                           
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    