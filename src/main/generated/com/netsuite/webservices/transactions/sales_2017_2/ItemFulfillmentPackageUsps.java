
/**
 * ItemFulfillmentPackageUsps.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  ItemFulfillmentPackageUsps bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemFulfillmentPackageUsps
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemFulfillmentPackageUsps
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for PackageWeightUsps
                        */

                        
                                    protected double localPackageWeightUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageWeightUspsTracker = false ;

                           public boolean isPackageWeightUspsSpecified(){
                               return localPackageWeightUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPackageWeightUsps(){
                               return localPackageWeightUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageWeightUsps
                               */
                               public void setPackageWeightUsps(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageWeightUspsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPackageWeightUsps=param;
                                    

                               }
                            

                        /**
                        * field for PackageDescrUsps
                        */

                        
                                    protected java.lang.String localPackageDescrUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageDescrUspsTracker = false ;

                           public boolean isPackageDescrUspsSpecified(){
                               return localPackageDescrUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPackageDescrUsps(){
                               return localPackageDescrUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageDescrUsps
                               */
                               public void setPackageDescrUsps(java.lang.String param){
                            localPackageDescrUspsTracker = param != null;
                                   
                                            this.localPackageDescrUsps=param;
                                    

                               }
                            

                        /**
                        * field for PackageTrackingNumberUsps
                        */

                        
                                    protected java.lang.String localPackageTrackingNumberUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageTrackingNumberUspsTracker = false ;

                           public boolean isPackageTrackingNumberUspsSpecified(){
                               return localPackageTrackingNumberUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPackageTrackingNumberUsps(){
                               return localPackageTrackingNumberUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageTrackingNumberUsps
                               */
                               public void setPackageTrackingNumberUsps(java.lang.String param){
                            localPackageTrackingNumberUspsTracker = param != null;
                                   
                                            this.localPackageTrackingNumberUsps=param;
                                    

                               }
                            

                        /**
                        * field for PackagingUsps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsPackagingUsps localPackagingUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackagingUspsTracker = false ;

                           public boolean isPackagingUspsSpecified(){
                               return localPackagingUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsPackagingUsps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsPackagingUsps getPackagingUsps(){
                               return localPackagingUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackagingUsps
                               */
                               public void setPackagingUsps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsPackagingUsps param){
                            localPackagingUspsTracker = param != null;
                                   
                                            this.localPackagingUsps=param;
                                    

                               }
                            

                        /**
                        * field for UseInsuredValueUsps
                        */

                        
                                    protected boolean localUseInsuredValueUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseInsuredValueUspsTracker = false ;

                           public boolean isUseInsuredValueUspsSpecified(){
                               return localUseInsuredValueUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseInsuredValueUsps(){
                               return localUseInsuredValueUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseInsuredValueUsps
                               */
                               public void setUseInsuredValueUsps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseInsuredValueUspsTracker =
                                       true;
                                   
                                            this.localUseInsuredValueUsps=param;
                                    

                               }
                            

                        /**
                        * field for InsuredValueUsps
                        */

                        
                                    protected double localInsuredValueUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsuredValueUspsTracker = false ;

                           public boolean isInsuredValueUspsSpecified(){
                               return localInsuredValueUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getInsuredValueUsps(){
                               return localInsuredValueUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsuredValueUsps
                               */
                               public void setInsuredValueUsps(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localInsuredValueUspsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localInsuredValueUsps=param;
                                    

                               }
                            

                        /**
                        * field for Reference1Usps
                        */

                        
                                    protected java.lang.String localReference1Usps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReference1UspsTracker = false ;

                           public boolean isReference1UspsSpecified(){
                               return localReference1UspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getReference1Usps(){
                               return localReference1Usps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reference1Usps
                               */
                               public void setReference1Usps(java.lang.String param){
                            localReference1UspsTracker = param != null;
                                   
                                            this.localReference1Usps=param;
                                    

                               }
                            

                        /**
                        * field for Reference2Usps
                        */

                        
                                    protected java.lang.String localReference2Usps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReference2UspsTracker = false ;

                           public boolean isReference2UspsSpecified(){
                               return localReference2UspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getReference2Usps(){
                               return localReference2Usps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reference2Usps
                               */
                               public void setReference2Usps(java.lang.String param){
                            localReference2UspsTracker = param != null;
                                   
                                            this.localReference2Usps=param;
                                    

                               }
                            

                        /**
                        * field for PackageLengthUsps
                        */

                        
                                    protected long localPackageLengthUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageLengthUspsTracker = false ;

                           public boolean isPackageLengthUspsSpecified(){
                               return localPackageLengthUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageLengthUsps(){
                               return localPackageLengthUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageLengthUsps
                               */
                               public void setPackageLengthUsps(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageLengthUspsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageLengthUsps=param;
                                    

                               }
                            

                        /**
                        * field for PackageWidthUsps
                        */

                        
                                    protected long localPackageWidthUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageWidthUspsTracker = false ;

                           public boolean isPackageWidthUspsSpecified(){
                               return localPackageWidthUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageWidthUsps(){
                               return localPackageWidthUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageWidthUsps
                               */
                               public void setPackageWidthUsps(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageWidthUspsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageWidthUsps=param;
                                    

                               }
                            

                        /**
                        * field for PackageHeightUsps
                        */

                        
                                    protected long localPackageHeightUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageHeightUspsTracker = false ;

                           public boolean isPackageHeightUspsSpecified(){
                               return localPackageHeightUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageHeightUsps(){
                               return localPackageHeightUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageHeightUsps
                               */
                               public void setPackageHeightUsps(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageHeightUspsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageHeightUsps=param;
                                    

                               }
                            

                        /**
                        * field for DeliveryConfUsps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsDeliveryConfUsps localDeliveryConfUsps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeliveryConfUspsTracker = false ;

                           public boolean isDeliveryConfUspsSpecified(){
                               return localDeliveryConfUspsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsDeliveryConfUsps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsDeliveryConfUsps getDeliveryConfUsps(){
                               return localDeliveryConfUsps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeliveryConfUsps
                               */
                               public void setDeliveryConfUsps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsDeliveryConfUsps param){
                            localDeliveryConfUspsTracker = param != null;
                                   
                                            this.localDeliveryConfUsps=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemFulfillmentPackageUsps",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemFulfillmentPackageUsps",
                           xmlWriter);
                   }

               
                   }
                if (localPackageWeightUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageWeightUsps", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPackageWeightUsps)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageWeightUsps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWeightUsps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageDescrUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageDescrUsps", xmlWriter);
                             

                                          if (localPackageDescrUsps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("packageDescrUsps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPackageDescrUsps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageTrackingNumberUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageTrackingNumberUsps", xmlWriter);
                             

                                          if (localPackageTrackingNumberUsps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("packageTrackingNumberUsps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPackageTrackingNumberUsps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackagingUspsTracker){
                                            if (localPackagingUsps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packagingUsps cannot be null!!");
                                            }
                                           localPackagingUsps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packagingUsps"),
                                               xmlWriter);
                                        } if (localUseInsuredValueUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useInsuredValueUsps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useInsuredValueUsps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseInsuredValueUsps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsuredValueUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "insuredValueUsps", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localInsuredValueUsps)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("insuredValueUsps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsuredValueUsps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReference1UspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reference1Usps", xmlWriter);
                             

                                          if (localReference1Usps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("reference1Usps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReference1Usps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReference2UspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reference2Usps", xmlWriter);
                             

                                          if (localReference2Usps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("reference2Usps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReference2Usps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageLengthUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageLengthUsps", xmlWriter);
                             
                                               if (localPackageLengthUsps==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageLengthUsps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageLengthUsps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageWidthUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageWidthUsps", xmlWriter);
                             
                                               if (localPackageWidthUsps==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageWidthUsps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWidthUsps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageHeightUspsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageHeightUsps", xmlWriter);
                             
                                               if (localPackageHeightUsps==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageHeightUsps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageHeightUsps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeliveryConfUspsTracker){
                                            if (localDeliveryConfUsps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deliveryConfUsps cannot be null!!");
                                            }
                                           localDeliveryConfUsps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deliveryConfUsps"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localPackageWeightUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageWeightUsps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWeightUsps));
                            } if (localPackageDescrUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageDescrUsps"));
                                 
                                        if (localPackageDescrUsps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageDescrUsps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("packageDescrUsps cannot be null!!");
                                        }
                                    } if (localPackageTrackingNumberUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageTrackingNumberUsps"));
                                 
                                        if (localPackageTrackingNumberUsps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageTrackingNumberUsps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("packageTrackingNumberUsps cannot be null!!");
                                        }
                                    } if (localPackagingUspsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packagingUsps"));
                            
                            
                                    if (localPackagingUsps==null){
                                         throw new org.apache.axis2.databinding.ADBException("packagingUsps cannot be null!!");
                                    }
                                    elementList.add(localPackagingUsps);
                                } if (localUseInsuredValueUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "useInsuredValueUsps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseInsuredValueUsps));
                            } if (localInsuredValueUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "insuredValueUsps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsuredValueUsps));
                            } if (localReference1UspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "reference1Usps"));
                                 
                                        if (localReference1Usps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReference1Usps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("reference1Usps cannot be null!!");
                                        }
                                    } if (localReference2UspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "reference2Usps"));
                                 
                                        if (localReference2Usps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReference2Usps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("reference2Usps cannot be null!!");
                                        }
                                    } if (localPackageLengthUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageLengthUsps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageLengthUsps));
                            } if (localPackageWidthUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageWidthUsps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWidthUsps));
                            } if (localPackageHeightUspsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageHeightUsps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageHeightUsps));
                            } if (localDeliveryConfUspsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "deliveryConfUsps"));
                            
                            
                                    if (localDeliveryConfUsps==null){
                                         throw new org.apache.axis2.databinding.ADBException("deliveryConfUsps cannot be null!!");
                                    }
                                    elementList.add(localDeliveryConfUsps);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemFulfillmentPackageUsps parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemFulfillmentPackageUsps object =
                new ItemFulfillmentPackageUsps();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemFulfillmentPackageUsps".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemFulfillmentPackageUsps)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageWeightUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageWeightUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageWeightUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageWeightUsps(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageDescrUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageDescrUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageDescrUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageTrackingNumberUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageTrackingNumberUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageTrackingNumberUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packagingUsps").equals(reader.getName())){
                                
                                                object.setPackagingUsps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsPackagingUsps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","useInsuredValueUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useInsuredValueUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseInsuredValueUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","insuredValueUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"insuredValueUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInsuredValueUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setInsuredValueUsps(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","reference1Usps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reference1Usps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReference1Usps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","reference2Usps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reference2Usps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReference2Usps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageLengthUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageLengthUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageLengthUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageLengthUsps(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageWidthUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageWidthUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageWidthUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageWidthUsps(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageHeightUsps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageHeightUsps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageHeightUsps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageHeightUsps(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deliveryConfUsps").equals(reader.getName())){
                                
                                                object.setDeliveryConfUsps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsDeliveryConfUsps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    