
/**
 * TransactionStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2.types;
            

            /**
            *  TransactionStatus bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionStatus
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.sales_2017_2.transactions.webservices.netsuite.com",
                "TransactionStatus",
                "ns20");

            

                        /**
                        * field for TransactionStatus
                        */

                        
                                    protected java.lang.String localTransactionStatus ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected TransactionStatus(java.lang.String value, boolean isRegisterValue) {
                                    localTransactionStatus = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localTransactionStatus, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __billCancelled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billCancelled");
                                
                                    public static final java.lang.String __billOpen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billOpen");
                                
                                    public static final java.lang.String __billPaidInFull =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billPaidInFull");
                                
                                    public static final java.lang.String __billPaymentOnlineBillPayPendingAccountingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billPaymentOnlineBillPayPendingAccountingApproval");
                                
                                    public static final java.lang.String __billPaymentVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billPaymentVoided");
                                
                                    public static final java.lang.String __billPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billPendingApproval");
                                
                                    public static final java.lang.String __billRejected =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billRejected");
                                
                                    public static final java.lang.String __cashSaleDeposited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashSaleDeposited");
                                
                                    public static final java.lang.String __cashSaleNotDeposited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashSaleNotDeposited");
                                
                                    public static final java.lang.String __cashSaleUnapprovedPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashSaleUnapprovedPayment");
                                
                                    public static final java.lang.String __checkOnlineBillPayPendingAccountingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_checkOnlineBillPayPendingAccountingApproval");
                                
                                    public static final java.lang.String __checkVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_checkVoided");
                                
                                    public static final java.lang.String __commissionOverpaid =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionOverpaid");
                                
                                    public static final java.lang.String __commissionPaidInFull =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionPaidInFull");
                                
                                    public static final java.lang.String __commissionPendingAccountingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionPendingAccountingApproval");
                                
                                    public static final java.lang.String __commissionPendingPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionPendingPayment");
                                
                                    public static final java.lang.String __commissionRejectedByAccounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionRejectedByAccounting");
                                
                                    public static final java.lang.String __creditMemoFullyApplied =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditMemoFullyApplied");
                                
                                    public static final java.lang.String __creditMemoOpen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditMemoOpen");
                                
                                    public static final java.lang.String __creditMemoVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditMemoVoided");
                                
                                    public static final java.lang.String __customerDepositDeposited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerDepositDeposited");
                                
                                    public static final java.lang.String __customerDepositFullyApplied =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerDepositFullyApplied");
                                
                                    public static final java.lang.String __customerDepositNotDeposited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerDepositNotDeposited");
                                
                                    public static final java.lang.String __customerDepositUnapprovedPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerDepositUnapprovedPayment");
                                
                                    public static final java.lang.String __customerRefundVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerRefundVoided");
                                
                                    public static final java.lang.String __estimateClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimateClosed");
                                
                                    public static final java.lang.String __estimateExpired =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimateExpired");
                                
                                    public static final java.lang.String __estimateOpen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimateOpen");
                                
                                    public static final java.lang.String __estimateProcessed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimateProcessed");
                                
                                    public static final java.lang.String __estimateVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimateVoided");
                                
                                    public static final java.lang.String __expenseReportApprovedByAccounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportApprovedByAccounting");
                                
                                    public static final java.lang.String __expenseReportApprovedOverriddenByAccounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportApprovedOverriddenByAccounting");
                                
                                    public static final java.lang.String __expenseReportInProgress =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportInProgress");
                                
                                    public static final java.lang.String __expenseReportPaidInFull =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportPaidInFull");
                                
                                    public static final java.lang.String __expenseReportPendingAccountingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportPendingAccountingApproval");
                                
                                    public static final java.lang.String __expenseReportPendingSupervisorApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportPendingSupervisorApproval");
                                
                                    public static final java.lang.String __expenseReportRejectedByAccounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportRejectedByAccounting");
                                
                                    public static final java.lang.String __expenseReportRejectedBySupervisor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportRejectedBySupervisor");
                                
                                    public static final java.lang.String __expenseReportRejectedOverriddenByAccounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportRejectedOverriddenByAccounting");
                                
                                    public static final java.lang.String __expenseReportVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReportVoided");
                                
                                    public static final java.lang.String __invoiceOpen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoiceOpen");
                                
                                    public static final java.lang.String __invoicePaidInFull =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoicePaidInFull");
                                
                                    public static final java.lang.String __invoicePendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoicePendingApproval");
                                
                                    public static final java.lang.String __invoiceRejected =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoiceRejected");
                                
                                    public static final java.lang.String __invoiceVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoiceVoided");
                                
                                    public static final java.lang.String __itemFulfillmentPacked =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemFulfillmentPacked");
                                
                                    public static final java.lang.String __itemFulfillmentPicked =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemFulfillmentPicked");
                                
                                    public static final java.lang.String __itemFulfillmentShipped =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemFulfillmentShipped");
                                
                                    public static final java.lang.String __journalApprovedForPosting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_journalApprovedForPosting");
                                
                                    public static final java.lang.String __journalPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_journalPendingApproval");
                                
                                    public static final java.lang.String __journalRejected =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_journalRejected");
                                
                                    public static final java.lang.String __opportunityClosedLost =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunityClosedLost");
                                
                                    public static final java.lang.String __opportunityClosedWon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunityClosedWon");
                                
                                    public static final java.lang.String __opportunityInProgress =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunityInProgress");
                                
                                    public static final java.lang.String __opportunityIssuedEstimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunityIssuedEstimate");
                                
                                    public static final java.lang.String __paycheckCommitted =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckCommitted");
                                
                                    public static final java.lang.String __paycheckCreated =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckCreated");
                                
                                    public static final java.lang.String __paycheckError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckError");
                                
                                    public static final java.lang.String __paycheckPendingCommitment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckPendingCommitment");
                                
                                    public static final java.lang.String __paycheckPendingTaxCalculation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckPendingTaxCalculation");
                                
                                    public static final java.lang.String __paycheckPreview =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckPreview");
                                
                                    public static final java.lang.String __paycheckReversed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckReversed");
                                
                                    public static final java.lang.String __paymentDeposited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentDeposited");
                                
                                    public static final java.lang.String __paymentNotDeposited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentNotDeposited");
                                
                                    public static final java.lang.String __paymentUnapprovedPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentUnapprovedPayment");
                                
                                    public static final java.lang.String __payrollLiabilityCheckVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollLiabilityCheckVoided");
                                
                                    public static final java.lang.String __purchaseOrderClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderClosed");
                                
                                    public static final java.lang.String __purchaseOrderFullyBilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderFullyBilled");
                                
                                    public static final java.lang.String __purchaseOrderPartiallyReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderPartiallyReceived");
                                
                                    public static final java.lang.String __purchaseOrderPendingBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderPendingBill");
                                
                                    public static final java.lang.String __purchaseOrderPendingBillingPartiallyReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderPendingBillingPartiallyReceived");
                                
                                    public static final java.lang.String __purchaseOrderPendingReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderPendingReceipt");
                                
                                    public static final java.lang.String __purchaseOrderPendingSupervisorApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderPendingSupervisorApproval");
                                
                                    public static final java.lang.String __purchaseOrderRejectedBySupervisor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderRejectedBySupervisor");
                                
                                    public static final java.lang.String __requisitionCancelled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionCancelled");
                                
                                    public static final java.lang.String __requisitionClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionClosed");
                                
                                    public static final java.lang.String __requisitionFullyOrdered =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionFullyOrdered");
                                
                                    public static final java.lang.String __requisitionFullyReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionFullyReceived");
                                
                                    public static final java.lang.String __requisitionPartiallyOrdered =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionPartiallyOrdered");
                                
                                    public static final java.lang.String __requisitionPartiallyReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionPartiallyReceived");
                                
                                    public static final java.lang.String __requisitionPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionPendingApproval");
                                
                                    public static final java.lang.String __requisitionPendingOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionPendingOrder");
                                
                                    public static final java.lang.String __requisitionRejected =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionRejected");
                                
                                    public static final java.lang.String __returnAuthorizationCancelled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationCancelled");
                                
                                    public static final java.lang.String __returnAuthorizationClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationClosed");
                                
                                    public static final java.lang.String __returnAuthorizationPartiallyReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationPartiallyReceived");
                                
                                    public static final java.lang.String __returnAuthorizationPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationPendingApproval");
                                
                                    public static final java.lang.String __returnAuthorizationPendingReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationPendingReceipt");
                                
                                    public static final java.lang.String __returnAuthorizationPendingRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationPendingRefund");
                                
                                    public static final java.lang.String __returnAuthorizationPendingRefundPartiallyReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationPendingRefundPartiallyReceived");
                                
                                    public static final java.lang.String __returnAuthorizationRefunded =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationRefunded");
                                
                                    public static final java.lang.String __salesOrderBilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderBilled");
                                
                                    public static final java.lang.String __salesOrderCancelled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderCancelled");
                                
                                    public static final java.lang.String __salesOrderClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderClosed");
                                
                                    public static final java.lang.String __salesOrderPartiallyFulfilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderPartiallyFulfilled");
                                
                                    public static final java.lang.String __salesOrderPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderPendingApproval");
                                
                                    public static final java.lang.String __salesOrderPendingBilling =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderPendingBilling");
                                
                                    public static final java.lang.String __salesOrderPendingBillingPartiallyFulfilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderPendingBillingPartiallyFulfilled");
                                
                                    public static final java.lang.String __salesOrderPendingFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderPendingFulfillment");
                                
                                    public static final java.lang.String __salesTaxPaymentOnlineBillPayPendingAccountingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesTaxPaymentOnlineBillPayPendingAccountingApproval");
                                
                                    public static final java.lang.String __salesTaxPaymentVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesTaxPaymentVoided");
                                
                                    public static final java.lang.String __statementChargeOpen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_statementChargeOpen");
                                
                                    public static final java.lang.String __statementChargePaidInFull =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_statementChargePaidInFull");
                                
                                    public static final java.lang.String __taxLiabilityChequeVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taxLiabilityChequeVoided");
                                
                                    public static final java.lang.String __tegataPayableEndorsed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataPayableEndorsed");
                                
                                    public static final java.lang.String __tegataPayableIssued =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataPayableIssued");
                                
                                    public static final java.lang.String __tegataPayablePaid =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataPayablePaid");
                                
                                    public static final java.lang.String __tegataReceivablesCollected =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataReceivablesCollected");
                                
                                    public static final java.lang.String __tegataReceivablesDiscounted =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataReceivablesDiscounted");
                                
                                    public static final java.lang.String __tegataReceivablesEndorsed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataReceivablesEndorsed");
                                
                                    public static final java.lang.String __tegataReceivablesHolding =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataReceivablesHolding");
                                
                                    public static final java.lang.String __tegataReceivablesVoided =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataReceivablesVoided");
                                
                                    public static final java.lang.String __transferOrderClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderClosed");
                                
                                    public static final java.lang.String __transferOrderPartiallyFulfilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderPartiallyFulfilled");
                                
                                    public static final java.lang.String __transferOrderPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderPendingApproval");
                                
                                    public static final java.lang.String __transferOrderPendingFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderPendingFulfillment");
                                
                                    public static final java.lang.String __transferOrderPendingReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderPendingReceipt");
                                
                                    public static final java.lang.String __transferOrderPendingReceiptPartiallyFulfilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderPendingReceiptPartiallyFulfilled");
                                
                                    public static final java.lang.String __transferOrderReceived =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderReceived");
                                
                                    public static final java.lang.String __transferOrderRejected =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderRejected");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationCancelled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationCancelled");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationClosed");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationCredited =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationCredited");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationPartiallyReturned =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationPartiallyReturned");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationPendingApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationPendingApproval");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationPendingCredit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationPendingCredit");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationPendingCreditPartiallyReturned =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationPendingCreditPartiallyReturned");
                                
                                    public static final java.lang.String __vendorReturnAuthorizationPendingReturn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorizationPendingReturn");
                                
                                    public static final java.lang.String __workOrderBuilt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderBuilt");
                                
                                    public static final java.lang.String __workOrderCancelled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderCancelled");
                                
                                    public static final java.lang.String __workOrderClosed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderClosed");
                                
                                    public static final java.lang.String __workOrderPartiallyBuilt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderPartiallyBuilt");
                                
                                    public static final java.lang.String __workOrderPendingBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderPendingBuild");
                                
                                    public static final java.lang.String __workOrderPlanned =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderPlanned");
                                
                                public static final TransactionStatus _billCancelled =
                                    new TransactionStatus(__billCancelled,true);
                            
                                public static final TransactionStatus _billOpen =
                                    new TransactionStatus(__billOpen,true);
                            
                                public static final TransactionStatus _billPaidInFull =
                                    new TransactionStatus(__billPaidInFull,true);
                            
                                public static final TransactionStatus _billPaymentOnlineBillPayPendingAccountingApproval =
                                    new TransactionStatus(__billPaymentOnlineBillPayPendingAccountingApproval,true);
                            
                                public static final TransactionStatus _billPaymentVoided =
                                    new TransactionStatus(__billPaymentVoided,true);
                            
                                public static final TransactionStatus _billPendingApproval =
                                    new TransactionStatus(__billPendingApproval,true);
                            
                                public static final TransactionStatus _billRejected =
                                    new TransactionStatus(__billRejected,true);
                            
                                public static final TransactionStatus _cashSaleDeposited =
                                    new TransactionStatus(__cashSaleDeposited,true);
                            
                                public static final TransactionStatus _cashSaleNotDeposited =
                                    new TransactionStatus(__cashSaleNotDeposited,true);
                            
                                public static final TransactionStatus _cashSaleUnapprovedPayment =
                                    new TransactionStatus(__cashSaleUnapprovedPayment,true);
                            
                                public static final TransactionStatus _checkOnlineBillPayPendingAccountingApproval =
                                    new TransactionStatus(__checkOnlineBillPayPendingAccountingApproval,true);
                            
                                public static final TransactionStatus _checkVoided =
                                    new TransactionStatus(__checkVoided,true);
                            
                                public static final TransactionStatus _commissionOverpaid =
                                    new TransactionStatus(__commissionOverpaid,true);
                            
                                public static final TransactionStatus _commissionPaidInFull =
                                    new TransactionStatus(__commissionPaidInFull,true);
                            
                                public static final TransactionStatus _commissionPendingAccountingApproval =
                                    new TransactionStatus(__commissionPendingAccountingApproval,true);
                            
                                public static final TransactionStatus _commissionPendingPayment =
                                    new TransactionStatus(__commissionPendingPayment,true);
                            
                                public static final TransactionStatus _commissionRejectedByAccounting =
                                    new TransactionStatus(__commissionRejectedByAccounting,true);
                            
                                public static final TransactionStatus _creditMemoFullyApplied =
                                    new TransactionStatus(__creditMemoFullyApplied,true);
                            
                                public static final TransactionStatus _creditMemoOpen =
                                    new TransactionStatus(__creditMemoOpen,true);
                            
                                public static final TransactionStatus _creditMemoVoided =
                                    new TransactionStatus(__creditMemoVoided,true);
                            
                                public static final TransactionStatus _customerDepositDeposited =
                                    new TransactionStatus(__customerDepositDeposited,true);
                            
                                public static final TransactionStatus _customerDepositFullyApplied =
                                    new TransactionStatus(__customerDepositFullyApplied,true);
                            
                                public static final TransactionStatus _customerDepositNotDeposited =
                                    new TransactionStatus(__customerDepositNotDeposited,true);
                            
                                public static final TransactionStatus _customerDepositUnapprovedPayment =
                                    new TransactionStatus(__customerDepositUnapprovedPayment,true);
                            
                                public static final TransactionStatus _customerRefundVoided =
                                    new TransactionStatus(__customerRefundVoided,true);
                            
                                public static final TransactionStatus _estimateClosed =
                                    new TransactionStatus(__estimateClosed,true);
                            
                                public static final TransactionStatus _estimateExpired =
                                    new TransactionStatus(__estimateExpired,true);
                            
                                public static final TransactionStatus _estimateOpen =
                                    new TransactionStatus(__estimateOpen,true);
                            
                                public static final TransactionStatus _estimateProcessed =
                                    new TransactionStatus(__estimateProcessed,true);
                            
                                public static final TransactionStatus _estimateVoided =
                                    new TransactionStatus(__estimateVoided,true);
                            
                                public static final TransactionStatus _expenseReportApprovedByAccounting =
                                    new TransactionStatus(__expenseReportApprovedByAccounting,true);
                            
                                public static final TransactionStatus _expenseReportApprovedOverriddenByAccounting =
                                    new TransactionStatus(__expenseReportApprovedOverriddenByAccounting,true);
                            
                                public static final TransactionStatus _expenseReportInProgress =
                                    new TransactionStatus(__expenseReportInProgress,true);
                            
                                public static final TransactionStatus _expenseReportPaidInFull =
                                    new TransactionStatus(__expenseReportPaidInFull,true);
                            
                                public static final TransactionStatus _expenseReportPendingAccountingApproval =
                                    new TransactionStatus(__expenseReportPendingAccountingApproval,true);
                            
                                public static final TransactionStatus _expenseReportPendingSupervisorApproval =
                                    new TransactionStatus(__expenseReportPendingSupervisorApproval,true);
                            
                                public static final TransactionStatus _expenseReportRejectedByAccounting =
                                    new TransactionStatus(__expenseReportRejectedByAccounting,true);
                            
                                public static final TransactionStatus _expenseReportRejectedBySupervisor =
                                    new TransactionStatus(__expenseReportRejectedBySupervisor,true);
                            
                                public static final TransactionStatus _expenseReportRejectedOverriddenByAccounting =
                                    new TransactionStatus(__expenseReportRejectedOverriddenByAccounting,true);
                            
                                public static final TransactionStatus _expenseReportVoided =
                                    new TransactionStatus(__expenseReportVoided,true);
                            
                                public static final TransactionStatus _invoiceOpen =
                                    new TransactionStatus(__invoiceOpen,true);
                            
                                public static final TransactionStatus _invoicePaidInFull =
                                    new TransactionStatus(__invoicePaidInFull,true);
                            
                                public static final TransactionStatus _invoicePendingApproval =
                                    new TransactionStatus(__invoicePendingApproval,true);
                            
                                public static final TransactionStatus _invoiceRejected =
                                    new TransactionStatus(__invoiceRejected,true);
                            
                                public static final TransactionStatus _invoiceVoided =
                                    new TransactionStatus(__invoiceVoided,true);
                            
                                public static final TransactionStatus _itemFulfillmentPacked =
                                    new TransactionStatus(__itemFulfillmentPacked,true);
                            
                                public static final TransactionStatus _itemFulfillmentPicked =
                                    new TransactionStatus(__itemFulfillmentPicked,true);
                            
                                public static final TransactionStatus _itemFulfillmentShipped =
                                    new TransactionStatus(__itemFulfillmentShipped,true);
                            
                                public static final TransactionStatus _journalApprovedForPosting =
                                    new TransactionStatus(__journalApprovedForPosting,true);
                            
                                public static final TransactionStatus _journalPendingApproval =
                                    new TransactionStatus(__journalPendingApproval,true);
                            
                                public static final TransactionStatus _journalRejected =
                                    new TransactionStatus(__journalRejected,true);
                            
                                public static final TransactionStatus _opportunityClosedLost =
                                    new TransactionStatus(__opportunityClosedLost,true);
                            
                                public static final TransactionStatus _opportunityClosedWon =
                                    new TransactionStatus(__opportunityClosedWon,true);
                            
                                public static final TransactionStatus _opportunityInProgress =
                                    new TransactionStatus(__opportunityInProgress,true);
                            
                                public static final TransactionStatus _opportunityIssuedEstimate =
                                    new TransactionStatus(__opportunityIssuedEstimate,true);
                            
                                public static final TransactionStatus _paycheckCommitted =
                                    new TransactionStatus(__paycheckCommitted,true);
                            
                                public static final TransactionStatus _paycheckCreated =
                                    new TransactionStatus(__paycheckCreated,true);
                            
                                public static final TransactionStatus _paycheckError =
                                    new TransactionStatus(__paycheckError,true);
                            
                                public static final TransactionStatus _paycheckPendingCommitment =
                                    new TransactionStatus(__paycheckPendingCommitment,true);
                            
                                public static final TransactionStatus _paycheckPendingTaxCalculation =
                                    new TransactionStatus(__paycheckPendingTaxCalculation,true);
                            
                                public static final TransactionStatus _paycheckPreview =
                                    new TransactionStatus(__paycheckPreview,true);
                            
                                public static final TransactionStatus _paycheckReversed =
                                    new TransactionStatus(__paycheckReversed,true);
                            
                                public static final TransactionStatus _paymentDeposited =
                                    new TransactionStatus(__paymentDeposited,true);
                            
                                public static final TransactionStatus _paymentNotDeposited =
                                    new TransactionStatus(__paymentNotDeposited,true);
                            
                                public static final TransactionStatus _paymentUnapprovedPayment =
                                    new TransactionStatus(__paymentUnapprovedPayment,true);
                            
                                public static final TransactionStatus _payrollLiabilityCheckVoided =
                                    new TransactionStatus(__payrollLiabilityCheckVoided,true);
                            
                                public static final TransactionStatus _purchaseOrderClosed =
                                    new TransactionStatus(__purchaseOrderClosed,true);
                            
                                public static final TransactionStatus _purchaseOrderFullyBilled =
                                    new TransactionStatus(__purchaseOrderFullyBilled,true);
                            
                                public static final TransactionStatus _purchaseOrderPartiallyReceived =
                                    new TransactionStatus(__purchaseOrderPartiallyReceived,true);
                            
                                public static final TransactionStatus _purchaseOrderPendingBill =
                                    new TransactionStatus(__purchaseOrderPendingBill,true);
                            
                                public static final TransactionStatus _purchaseOrderPendingBillingPartiallyReceived =
                                    new TransactionStatus(__purchaseOrderPendingBillingPartiallyReceived,true);
                            
                                public static final TransactionStatus _purchaseOrderPendingReceipt =
                                    new TransactionStatus(__purchaseOrderPendingReceipt,true);
                            
                                public static final TransactionStatus _purchaseOrderPendingSupervisorApproval =
                                    new TransactionStatus(__purchaseOrderPendingSupervisorApproval,true);
                            
                                public static final TransactionStatus _purchaseOrderRejectedBySupervisor =
                                    new TransactionStatus(__purchaseOrderRejectedBySupervisor,true);
                            
                                public static final TransactionStatus _requisitionCancelled =
                                    new TransactionStatus(__requisitionCancelled,true);
                            
                                public static final TransactionStatus _requisitionClosed =
                                    new TransactionStatus(__requisitionClosed,true);
                            
                                public static final TransactionStatus _requisitionFullyOrdered =
                                    new TransactionStatus(__requisitionFullyOrdered,true);
                            
                                public static final TransactionStatus _requisitionFullyReceived =
                                    new TransactionStatus(__requisitionFullyReceived,true);
                            
                                public static final TransactionStatus _requisitionPartiallyOrdered =
                                    new TransactionStatus(__requisitionPartiallyOrdered,true);
                            
                                public static final TransactionStatus _requisitionPartiallyReceived =
                                    new TransactionStatus(__requisitionPartiallyReceived,true);
                            
                                public static final TransactionStatus _requisitionPendingApproval =
                                    new TransactionStatus(__requisitionPendingApproval,true);
                            
                                public static final TransactionStatus _requisitionPendingOrder =
                                    new TransactionStatus(__requisitionPendingOrder,true);
                            
                                public static final TransactionStatus _requisitionRejected =
                                    new TransactionStatus(__requisitionRejected,true);
                            
                                public static final TransactionStatus _returnAuthorizationCancelled =
                                    new TransactionStatus(__returnAuthorizationCancelled,true);
                            
                                public static final TransactionStatus _returnAuthorizationClosed =
                                    new TransactionStatus(__returnAuthorizationClosed,true);
                            
                                public static final TransactionStatus _returnAuthorizationPartiallyReceived =
                                    new TransactionStatus(__returnAuthorizationPartiallyReceived,true);
                            
                                public static final TransactionStatus _returnAuthorizationPendingApproval =
                                    new TransactionStatus(__returnAuthorizationPendingApproval,true);
                            
                                public static final TransactionStatus _returnAuthorizationPendingReceipt =
                                    new TransactionStatus(__returnAuthorizationPendingReceipt,true);
                            
                                public static final TransactionStatus _returnAuthorizationPendingRefund =
                                    new TransactionStatus(__returnAuthorizationPendingRefund,true);
                            
                                public static final TransactionStatus _returnAuthorizationPendingRefundPartiallyReceived =
                                    new TransactionStatus(__returnAuthorizationPendingRefundPartiallyReceived,true);
                            
                                public static final TransactionStatus _returnAuthorizationRefunded =
                                    new TransactionStatus(__returnAuthorizationRefunded,true);
                            
                                public static final TransactionStatus _salesOrderBilled =
                                    new TransactionStatus(__salesOrderBilled,true);
                            
                                public static final TransactionStatus _salesOrderCancelled =
                                    new TransactionStatus(__salesOrderCancelled,true);
                            
                                public static final TransactionStatus _salesOrderClosed =
                                    new TransactionStatus(__salesOrderClosed,true);
                            
                                public static final TransactionStatus _salesOrderPartiallyFulfilled =
                                    new TransactionStatus(__salesOrderPartiallyFulfilled,true);
                            
                                public static final TransactionStatus _salesOrderPendingApproval =
                                    new TransactionStatus(__salesOrderPendingApproval,true);
                            
                                public static final TransactionStatus _salesOrderPendingBilling =
                                    new TransactionStatus(__salesOrderPendingBilling,true);
                            
                                public static final TransactionStatus _salesOrderPendingBillingPartiallyFulfilled =
                                    new TransactionStatus(__salesOrderPendingBillingPartiallyFulfilled,true);
                            
                                public static final TransactionStatus _salesOrderPendingFulfillment =
                                    new TransactionStatus(__salesOrderPendingFulfillment,true);
                            
                                public static final TransactionStatus _salesTaxPaymentOnlineBillPayPendingAccountingApproval =
                                    new TransactionStatus(__salesTaxPaymentOnlineBillPayPendingAccountingApproval,true);
                            
                                public static final TransactionStatus _salesTaxPaymentVoided =
                                    new TransactionStatus(__salesTaxPaymentVoided,true);
                            
                                public static final TransactionStatus _statementChargeOpen =
                                    new TransactionStatus(__statementChargeOpen,true);
                            
                                public static final TransactionStatus _statementChargePaidInFull =
                                    new TransactionStatus(__statementChargePaidInFull,true);
                            
                                public static final TransactionStatus _taxLiabilityChequeVoided =
                                    new TransactionStatus(__taxLiabilityChequeVoided,true);
                            
                                public static final TransactionStatus _tegataPayableEndorsed =
                                    new TransactionStatus(__tegataPayableEndorsed,true);
                            
                                public static final TransactionStatus _tegataPayableIssued =
                                    new TransactionStatus(__tegataPayableIssued,true);
                            
                                public static final TransactionStatus _tegataPayablePaid =
                                    new TransactionStatus(__tegataPayablePaid,true);
                            
                                public static final TransactionStatus _tegataReceivablesCollected =
                                    new TransactionStatus(__tegataReceivablesCollected,true);
                            
                                public static final TransactionStatus _tegataReceivablesDiscounted =
                                    new TransactionStatus(__tegataReceivablesDiscounted,true);
                            
                                public static final TransactionStatus _tegataReceivablesEndorsed =
                                    new TransactionStatus(__tegataReceivablesEndorsed,true);
                            
                                public static final TransactionStatus _tegataReceivablesHolding =
                                    new TransactionStatus(__tegataReceivablesHolding,true);
                            
                                public static final TransactionStatus _tegataReceivablesVoided =
                                    new TransactionStatus(__tegataReceivablesVoided,true);
                            
                                public static final TransactionStatus _transferOrderClosed =
                                    new TransactionStatus(__transferOrderClosed,true);
                            
                                public static final TransactionStatus _transferOrderPartiallyFulfilled =
                                    new TransactionStatus(__transferOrderPartiallyFulfilled,true);
                            
                                public static final TransactionStatus _transferOrderPendingApproval =
                                    new TransactionStatus(__transferOrderPendingApproval,true);
                            
                                public static final TransactionStatus _transferOrderPendingFulfillment =
                                    new TransactionStatus(__transferOrderPendingFulfillment,true);
                            
                                public static final TransactionStatus _transferOrderPendingReceipt =
                                    new TransactionStatus(__transferOrderPendingReceipt,true);
                            
                                public static final TransactionStatus _transferOrderPendingReceiptPartiallyFulfilled =
                                    new TransactionStatus(__transferOrderPendingReceiptPartiallyFulfilled,true);
                            
                                public static final TransactionStatus _transferOrderReceived =
                                    new TransactionStatus(__transferOrderReceived,true);
                            
                                public static final TransactionStatus _transferOrderRejected =
                                    new TransactionStatus(__transferOrderRejected,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationCancelled =
                                    new TransactionStatus(__vendorReturnAuthorizationCancelled,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationClosed =
                                    new TransactionStatus(__vendorReturnAuthorizationClosed,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationCredited =
                                    new TransactionStatus(__vendorReturnAuthorizationCredited,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationPartiallyReturned =
                                    new TransactionStatus(__vendorReturnAuthorizationPartiallyReturned,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationPendingApproval =
                                    new TransactionStatus(__vendorReturnAuthorizationPendingApproval,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationPendingCredit =
                                    new TransactionStatus(__vendorReturnAuthorizationPendingCredit,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationPendingCreditPartiallyReturned =
                                    new TransactionStatus(__vendorReturnAuthorizationPendingCreditPartiallyReturned,true);
                            
                                public static final TransactionStatus _vendorReturnAuthorizationPendingReturn =
                                    new TransactionStatus(__vendorReturnAuthorizationPendingReturn,true);
                            
                                public static final TransactionStatus _workOrderBuilt =
                                    new TransactionStatus(__workOrderBuilt,true);
                            
                                public static final TransactionStatus _workOrderCancelled =
                                    new TransactionStatus(__workOrderCancelled,true);
                            
                                public static final TransactionStatus _workOrderClosed =
                                    new TransactionStatus(__workOrderClosed,true);
                            
                                public static final TransactionStatus _workOrderPartiallyBuilt =
                                    new TransactionStatus(__workOrderPartiallyBuilt,true);
                            
                                public static final TransactionStatus _workOrderPendingBuild =
                                    new TransactionStatus(__workOrderPendingBuild,true);
                            
                                public static final TransactionStatus _workOrderPlanned =
                                    new TransactionStatus(__workOrderPlanned,true);
                            

                                public java.lang.String getValue() { return localTransactionStatus;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localTransactionStatus.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.sales_2017_2.transactions.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":TransactionStatus",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "TransactionStatus",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localTransactionStatus==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("TransactionStatus cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localTransactionStatus);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns20";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionStatus)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static TransactionStatus fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    TransactionStatus enumeration = (TransactionStatus)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static TransactionStatus fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static TransactionStatus fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return TransactionStatus.Factory.fromString(content,namespaceUri);
                    } else {
                       return TransactionStatus.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionStatus parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionStatus object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TransactionStatus" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = TransactionStatus.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = TransactionStatus.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    