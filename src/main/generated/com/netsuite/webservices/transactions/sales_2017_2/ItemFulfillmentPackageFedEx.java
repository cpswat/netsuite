
/**
 * ItemFulfillmentPackageFedEx.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  ItemFulfillmentPackageFedEx bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemFulfillmentPackageFedEx
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemFulfillmentPackageFedEx
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for PackageWeightFedEx
                        */

                        
                                    protected double localPackageWeightFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageWeightFedExTracker = false ;

                           public boolean isPackageWeightFedExSpecified(){
                               return localPackageWeightFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPackageWeightFedEx(){
                               return localPackageWeightFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageWeightFedEx
                               */
                               public void setPackageWeightFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageWeightFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPackageWeightFedEx=param;
                                    

                               }
                            

                        /**
                        * field for DryIceWeightFedEx
                        */

                        
                                    protected double localDryIceWeightFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDryIceWeightFedExTracker = false ;

                           public boolean isDryIceWeightFedExSpecified(){
                               return localDryIceWeightFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDryIceWeightFedEx(){
                               return localDryIceWeightFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DryIceWeightFedEx
                               */
                               public void setDryIceWeightFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDryIceWeightFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDryIceWeightFedEx=param;
                                    

                               }
                            

                        /**
                        * field for PackageTrackingNumberFedEx
                        */

                        
                                    protected java.lang.String localPackageTrackingNumberFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageTrackingNumberFedExTracker = false ;

                           public boolean isPackageTrackingNumberFedExSpecified(){
                               return localPackageTrackingNumberFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPackageTrackingNumberFedEx(){
                               return localPackageTrackingNumberFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageTrackingNumberFedEx
                               */
                               public void setPackageTrackingNumberFedEx(java.lang.String param){
                            localPackageTrackingNumberFedExTracker = param != null;
                                   
                                            this.localPackageTrackingNumberFedEx=param;
                                    

                               }
                            

                        /**
                        * field for PackagingFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPackagingFedEx localPackagingFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackagingFedExTracker = false ;

                           public boolean isPackagingFedExSpecified(){
                               return localPackagingFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPackagingFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPackagingFedEx getPackagingFedEx(){
                               return localPackagingFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackagingFedEx
                               */
                               public void setPackagingFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPackagingFedEx param){
                            localPackagingFedExTracker = param != null;
                                   
                                            this.localPackagingFedEx=param;
                                    

                               }
                            

                        /**
                        * field for AdmPackageTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExAdmPackageTypeFedEx localAdmPackageTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAdmPackageTypeFedExTracker = false ;

                           public boolean isAdmPackageTypeFedExSpecified(){
                               return localAdmPackageTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExAdmPackageTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExAdmPackageTypeFedEx getAdmPackageTypeFedEx(){
                               return localAdmPackageTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AdmPackageTypeFedEx
                               */
                               public void setAdmPackageTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExAdmPackageTypeFedEx param){
                            localAdmPackageTypeFedExTracker = param != null;
                                   
                                            this.localAdmPackageTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for IsNonStandardContainerFedEx
                        */

                        
                                    protected boolean localIsNonStandardContainerFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsNonStandardContainerFedExTracker = false ;

                           public boolean isIsNonStandardContainerFedExSpecified(){
                               return localIsNonStandardContainerFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsNonStandardContainerFedEx(){
                               return localIsNonStandardContainerFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsNonStandardContainerFedEx
                               */
                               public void setIsNonStandardContainerFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsNonStandardContainerFedExTracker =
                                       true;
                                   
                                            this.localIsNonStandardContainerFedEx=param;
                                    

                               }
                            

                        /**
                        * field for IsAlcoholFedEx
                        */

                        
                                    protected boolean localIsAlcoholFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsAlcoholFedExTracker = false ;

                           public boolean isIsAlcoholFedExSpecified(){
                               return localIsAlcoholFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsAlcoholFedEx(){
                               return localIsAlcoholFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsAlcoholFedEx
                               */
                               public void setIsAlcoholFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsAlcoholFedExTracker =
                                       true;
                                   
                                            this.localIsAlcoholFedEx=param;
                                    

                               }
                            

                        /**
                        * field for AlcoholRecipientTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.AlcoholRecipientType localAlcoholRecipientTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAlcoholRecipientTypeFedExTracker = false ;

                           public boolean isAlcoholRecipientTypeFedExSpecified(){
                               return localAlcoholRecipientTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.AlcoholRecipientType
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.AlcoholRecipientType getAlcoholRecipientTypeFedEx(){
                               return localAlcoholRecipientTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AlcoholRecipientTypeFedEx
                               */
                               public void setAlcoholRecipientTypeFedEx(com.netsuite.webservices.platform.common_2017_2.types.AlcoholRecipientType param){
                            localAlcoholRecipientTypeFedExTracker = param != null;
                                   
                                            this.localAlcoholRecipientTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for IsNonHazLithiumFedEx
                        */

                        
                                    protected boolean localIsNonHazLithiumFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsNonHazLithiumFedExTracker = false ;

                           public boolean isIsNonHazLithiumFedExSpecified(){
                               return localIsNonHazLithiumFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsNonHazLithiumFedEx(){
                               return localIsNonHazLithiumFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsNonHazLithiumFedEx
                               */
                               public void setIsNonHazLithiumFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsNonHazLithiumFedExTracker =
                                       true;
                                   
                                            this.localIsNonHazLithiumFedEx=param;
                                    

                               }
                            

                        /**
                        * field for InsuredValueFedEx
                        */

                        
                                    protected double localInsuredValueFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsuredValueFedExTracker = false ;

                           public boolean isInsuredValueFedExSpecified(){
                               return localInsuredValueFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getInsuredValueFedEx(){
                               return localInsuredValueFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsuredValueFedEx
                               */
                               public void setInsuredValueFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localInsuredValueFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localInsuredValueFedEx=param;
                                    

                               }
                            

                        /**
                        * field for UseInsuredValueFedEx
                        */

                        
                                    protected boolean localUseInsuredValueFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseInsuredValueFedExTracker = false ;

                           public boolean isUseInsuredValueFedExSpecified(){
                               return localUseInsuredValueFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseInsuredValueFedEx(){
                               return localUseInsuredValueFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseInsuredValueFedEx
                               */
                               public void setUseInsuredValueFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseInsuredValueFedExTracker =
                                       true;
                                   
                                            this.localUseInsuredValueFedEx=param;
                                    

                               }
                            

                        /**
                        * field for Reference1FedEx
                        */

                        
                                    protected java.lang.String localReference1FedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReference1FedExTracker = false ;

                           public boolean isReference1FedExSpecified(){
                               return localReference1FedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getReference1FedEx(){
                               return localReference1FedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reference1FedEx
                               */
                               public void setReference1FedEx(java.lang.String param){
                            localReference1FedExTracker = param != null;
                                   
                                            this.localReference1FedEx=param;
                                    

                               }
                            

                        /**
                        * field for PriorityAlertTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPriorityAlertTypeFedEx localPriorityAlertTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityAlertTypeFedExTracker = false ;

                           public boolean isPriorityAlertTypeFedExSpecified(){
                               return localPriorityAlertTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPriorityAlertTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPriorityAlertTypeFedEx getPriorityAlertTypeFedEx(){
                               return localPriorityAlertTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PriorityAlertTypeFedEx
                               */
                               public void setPriorityAlertTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPriorityAlertTypeFedEx param){
                            localPriorityAlertTypeFedExTracker = param != null;
                                   
                                            this.localPriorityAlertTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for PriorityAlertContentFedEx
                        */

                        
                                    protected java.lang.String localPriorityAlertContentFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityAlertContentFedExTracker = false ;

                           public boolean isPriorityAlertContentFedExSpecified(){
                               return localPriorityAlertContentFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPriorityAlertContentFedEx(){
                               return localPriorityAlertContentFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PriorityAlertContentFedEx
                               */
                               public void setPriorityAlertContentFedEx(java.lang.String param){
                            localPriorityAlertContentFedExTracker = param != null;
                                   
                                            this.localPriorityAlertContentFedEx=param;
                                    

                               }
                            

                        /**
                        * field for PackageLengthFedEx
                        */

                        
                                    protected long localPackageLengthFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageLengthFedExTracker = false ;

                           public boolean isPackageLengthFedExSpecified(){
                               return localPackageLengthFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageLengthFedEx(){
                               return localPackageLengthFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageLengthFedEx
                               */
                               public void setPackageLengthFedEx(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageLengthFedExTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageLengthFedEx=param;
                                    

                               }
                            

                        /**
                        * field for PackageWidthFedEx
                        */

                        
                                    protected long localPackageWidthFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageWidthFedExTracker = false ;

                           public boolean isPackageWidthFedExSpecified(){
                               return localPackageWidthFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageWidthFedEx(){
                               return localPackageWidthFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageWidthFedEx
                               */
                               public void setPackageWidthFedEx(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageWidthFedExTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageWidthFedEx=param;
                                    

                               }
                            

                        /**
                        * field for PackageHeightFedEx
                        */

                        
                                    protected long localPackageHeightFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageHeightFedExTracker = false ;

                           public boolean isPackageHeightFedExSpecified(){
                               return localPackageHeightFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageHeightFedEx(){
                               return localPackageHeightFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageHeightFedEx
                               */
                               public void setPackageHeightFedEx(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageHeightFedExTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageHeightFedEx=param;
                                    

                               }
                            

                        /**
                        * field for UseCodFedEx
                        */

                        
                                    protected boolean localUseCodFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseCodFedExTracker = false ;

                           public boolean isUseCodFedExSpecified(){
                               return localUseCodFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseCodFedEx(){
                               return localUseCodFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseCodFedEx
                               */
                               public void setUseCodFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseCodFedExTracker =
                                       true;
                                   
                                            this.localUseCodFedEx=param;
                                    

                               }
                            

                        /**
                        * field for CodAmountFedEx
                        */

                        
                                    protected double localCodAmountFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodAmountFedExTracker = false ;

                           public boolean isCodAmountFedExSpecified(){
                               return localCodAmountFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCodAmountFedEx(){
                               return localCodAmountFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodAmountFedEx
                               */
                               public void setCodAmountFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCodAmountFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCodAmountFedEx=param;
                                    

                               }
                            

                        /**
                        * field for CodMethodFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodMethodFedEx localCodMethodFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodMethodFedExTracker = false ;

                           public boolean isCodMethodFedExSpecified(){
                               return localCodMethodFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodMethodFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodMethodFedEx getCodMethodFedEx(){
                               return localCodMethodFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodMethodFedEx
                               */
                               public void setCodMethodFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodMethodFedEx param){
                            localCodMethodFedExTracker = param != null;
                                   
                                            this.localCodMethodFedEx=param;
                                    

                               }
                            

                        /**
                        * field for CodFreightTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodFreightTypeFedEx localCodFreightTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodFreightTypeFedExTracker = false ;

                           public boolean isCodFreightTypeFedExSpecified(){
                               return localCodFreightTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodFreightTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodFreightTypeFedEx getCodFreightTypeFedEx(){
                               return localCodFreightTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodFreightTypeFedEx
                               */
                               public void setCodFreightTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodFreightTypeFedEx param){
                            localCodFreightTypeFedExTracker = param != null;
                                   
                                            this.localCodFreightTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for DeliveryConfFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExDeliveryConfFedEx localDeliveryConfFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeliveryConfFedExTracker = false ;

                           public boolean isDeliveryConfFedExSpecified(){
                               return localDeliveryConfFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExDeliveryConfFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExDeliveryConfFedEx getDeliveryConfFedEx(){
                               return localDeliveryConfFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeliveryConfFedEx
                               */
                               public void setDeliveryConfFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExDeliveryConfFedEx param){
                            localDeliveryConfFedExTracker = param != null;
                                   
                                            this.localDeliveryConfFedEx=param;
                                    

                               }
                            

                        /**
                        * field for SignatureOptionsFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExSignatureOptionsFedEx localSignatureOptionsFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSignatureOptionsFedExTracker = false ;

                           public boolean isSignatureOptionsFedExSpecified(){
                               return localSignatureOptionsFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExSignatureOptionsFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExSignatureOptionsFedEx getSignatureOptionsFedEx(){
                               return localSignatureOptionsFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SignatureOptionsFedEx
                               */
                               public void setSignatureOptionsFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExSignatureOptionsFedEx param){
                            localSignatureOptionsFedExTracker = param != null;
                                   
                                            this.localSignatureOptionsFedEx=param;
                                    

                               }
                            

                        /**
                        * field for SignatureReleaseFedEx
                        */

                        
                                    protected java.lang.String localSignatureReleaseFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSignatureReleaseFedExTracker = false ;

                           public boolean isSignatureReleaseFedExSpecified(){
                               return localSignatureReleaseFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSignatureReleaseFedEx(){
                               return localSignatureReleaseFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SignatureReleaseFedEx
                               */
                               public void setSignatureReleaseFedEx(java.lang.String param){
                            localSignatureReleaseFedExTracker = param != null;
                                   
                                            this.localSignatureReleaseFedEx=param;
                                    

                               }
                            

                        /**
                        * field for AuthorizationNumberFedEx
                        */

                        
                                    protected java.lang.String localAuthorizationNumberFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAuthorizationNumberFedExTracker = false ;

                           public boolean isAuthorizationNumberFedExSpecified(){
                               return localAuthorizationNumberFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAuthorizationNumberFedEx(){
                               return localAuthorizationNumberFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AuthorizationNumberFedEx
                               */
                               public void setAuthorizationNumberFedEx(java.lang.String param){
                            localAuthorizationNumberFedExTracker = param != null;
                                   
                                            this.localAuthorizationNumberFedEx=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemFulfillmentPackageFedEx",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemFulfillmentPackageFedEx",
                           xmlWriter);
                   }

               
                   }
                if (localPackageWeightFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageWeightFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPackageWeightFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageWeightFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWeightFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDryIceWeightFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dryIceWeightFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDryIceWeightFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("dryIceWeightFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDryIceWeightFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageTrackingNumberFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageTrackingNumberFedEx", xmlWriter);
                             

                                          if (localPackageTrackingNumberFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("packageTrackingNumberFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPackageTrackingNumberFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackagingFedExTracker){
                                            if (localPackagingFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packagingFedEx cannot be null!!");
                                            }
                                           localPackagingFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packagingFedEx"),
                                               xmlWriter);
                                        } if (localAdmPackageTypeFedExTracker){
                                            if (localAdmPackageTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("admPackageTypeFedEx cannot be null!!");
                                            }
                                           localAdmPackageTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","admPackageTypeFedEx"),
                                               xmlWriter);
                                        } if (localIsNonStandardContainerFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isNonStandardContainerFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isNonStandardContainerFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsNonStandardContainerFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsAlcoholFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isAlcoholFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isAlcoholFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsAlcoholFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAlcoholRecipientTypeFedExTracker){
                                            if (localAlcoholRecipientTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("alcoholRecipientTypeFedEx cannot be null!!");
                                            }
                                           localAlcoholRecipientTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","alcoholRecipientTypeFedEx"),
                                               xmlWriter);
                                        } if (localIsNonHazLithiumFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isNonHazLithiumFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isNonHazLithiumFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsNonHazLithiumFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsuredValueFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "insuredValueFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localInsuredValueFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("insuredValueFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsuredValueFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUseInsuredValueFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useInsuredValueFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useInsuredValueFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseInsuredValueFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReference1FedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reference1FedEx", xmlWriter);
                             

                                          if (localReference1FedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("reference1FedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReference1FedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPriorityAlertTypeFedExTracker){
                                            if (localPriorityAlertTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priorityAlertTypeFedEx cannot be null!!");
                                            }
                                           localPriorityAlertTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","priorityAlertTypeFedEx"),
                                               xmlWriter);
                                        } if (localPriorityAlertContentFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "priorityAlertContentFedEx", xmlWriter);
                             

                                          if (localPriorityAlertContentFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("priorityAlertContentFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPriorityAlertContentFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageLengthFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageLengthFedEx", xmlWriter);
                             
                                               if (localPackageLengthFedEx==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageLengthFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageLengthFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageWidthFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageWidthFedEx", xmlWriter);
                             
                                               if (localPackageWidthFedEx==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageWidthFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWidthFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageHeightFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageHeightFedEx", xmlWriter);
                             
                                               if (localPackageHeightFedEx==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageHeightFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageHeightFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUseCodFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useCodFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useCodFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseCodFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCodAmountFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "codAmountFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCodAmountFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("codAmountFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodAmountFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCodMethodFedExTracker){
                                            if (localCodMethodFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codMethodFedEx cannot be null!!");
                                            }
                                           localCodMethodFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codMethodFedEx"),
                                               xmlWriter);
                                        } if (localCodFreightTypeFedExTracker){
                                            if (localCodFreightTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codFreightTypeFedEx cannot be null!!");
                                            }
                                           localCodFreightTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codFreightTypeFedEx"),
                                               xmlWriter);
                                        } if (localDeliveryConfFedExTracker){
                                            if (localDeliveryConfFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deliveryConfFedEx cannot be null!!");
                                            }
                                           localDeliveryConfFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deliveryConfFedEx"),
                                               xmlWriter);
                                        } if (localSignatureOptionsFedExTracker){
                                            if (localSignatureOptionsFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("signatureOptionsFedEx cannot be null!!");
                                            }
                                           localSignatureOptionsFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","signatureOptionsFedEx"),
                                               xmlWriter);
                                        } if (localSignatureReleaseFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "signatureReleaseFedEx", xmlWriter);
                             

                                          if (localSignatureReleaseFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("signatureReleaseFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSignatureReleaseFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAuthorizationNumberFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "authorizationNumberFedEx", xmlWriter);
                             

                                          if (localAuthorizationNumberFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("authorizationNumberFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAuthorizationNumberFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localPackageWeightFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageWeightFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWeightFedEx));
                            } if (localDryIceWeightFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "dryIceWeightFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDryIceWeightFedEx));
                            } if (localPackageTrackingNumberFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageTrackingNumberFedEx"));
                                 
                                        if (localPackageTrackingNumberFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageTrackingNumberFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("packageTrackingNumberFedEx cannot be null!!");
                                        }
                                    } if (localPackagingFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packagingFedEx"));
                            
                            
                                    if (localPackagingFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("packagingFedEx cannot be null!!");
                                    }
                                    elementList.add(localPackagingFedEx);
                                } if (localAdmPackageTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "admPackageTypeFedEx"));
                            
                            
                                    if (localAdmPackageTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("admPackageTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localAdmPackageTypeFedEx);
                                } if (localIsNonStandardContainerFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isNonStandardContainerFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsNonStandardContainerFedEx));
                            } if (localIsAlcoholFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isAlcoholFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsAlcoholFedEx));
                            } if (localAlcoholRecipientTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "alcoholRecipientTypeFedEx"));
                            
                            
                                    if (localAlcoholRecipientTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("alcoholRecipientTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localAlcoholRecipientTypeFedEx);
                                } if (localIsNonHazLithiumFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isNonHazLithiumFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsNonHazLithiumFedEx));
                            } if (localInsuredValueFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "insuredValueFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsuredValueFedEx));
                            } if (localUseInsuredValueFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "useInsuredValueFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseInsuredValueFedEx));
                            } if (localReference1FedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "reference1FedEx"));
                                 
                                        if (localReference1FedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReference1FedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("reference1FedEx cannot be null!!");
                                        }
                                    } if (localPriorityAlertTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "priorityAlertTypeFedEx"));
                            
                            
                                    if (localPriorityAlertTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("priorityAlertTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localPriorityAlertTypeFedEx);
                                } if (localPriorityAlertContentFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "priorityAlertContentFedEx"));
                                 
                                        if (localPriorityAlertContentFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPriorityAlertContentFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("priorityAlertContentFedEx cannot be null!!");
                                        }
                                    } if (localPackageLengthFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageLengthFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageLengthFedEx));
                            } if (localPackageWidthFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageWidthFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWidthFedEx));
                            } if (localPackageHeightFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageHeightFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageHeightFedEx));
                            } if (localUseCodFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "useCodFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseCodFedEx));
                            } if (localCodAmountFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "codAmountFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodAmountFedEx));
                            } if (localCodMethodFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "codMethodFedEx"));
                            
                            
                                    if (localCodMethodFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("codMethodFedEx cannot be null!!");
                                    }
                                    elementList.add(localCodMethodFedEx);
                                } if (localCodFreightTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "codFreightTypeFedEx"));
                            
                            
                                    if (localCodFreightTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("codFreightTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localCodFreightTypeFedEx);
                                } if (localDeliveryConfFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "deliveryConfFedEx"));
                            
                            
                                    if (localDeliveryConfFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("deliveryConfFedEx cannot be null!!");
                                    }
                                    elementList.add(localDeliveryConfFedEx);
                                } if (localSignatureOptionsFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "signatureOptionsFedEx"));
                            
                            
                                    if (localSignatureOptionsFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("signatureOptionsFedEx cannot be null!!");
                                    }
                                    elementList.add(localSignatureOptionsFedEx);
                                } if (localSignatureReleaseFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "signatureReleaseFedEx"));
                                 
                                        if (localSignatureReleaseFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSignatureReleaseFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("signatureReleaseFedEx cannot be null!!");
                                        }
                                    } if (localAuthorizationNumberFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "authorizationNumberFedEx"));
                                 
                                        if (localAuthorizationNumberFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAuthorizationNumberFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("authorizationNumberFedEx cannot be null!!");
                                        }
                                    }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemFulfillmentPackageFedEx parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemFulfillmentPackageFedEx object =
                new ItemFulfillmentPackageFedEx();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemFulfillmentPackageFedEx".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemFulfillmentPackageFedEx)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageWeightFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageWeightFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageWeightFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageWeightFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","dryIceWeightFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dryIceWeightFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDryIceWeightFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDryIceWeightFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageTrackingNumberFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageTrackingNumberFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageTrackingNumberFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packagingFedEx").equals(reader.getName())){
                                
                                                object.setPackagingFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPackagingFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","admPackageTypeFedEx").equals(reader.getName())){
                                
                                                object.setAdmPackageTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExAdmPackageTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isNonStandardContainerFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isNonStandardContainerFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsNonStandardContainerFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isAlcoholFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isAlcoholFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsAlcoholFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","alcoholRecipientTypeFedEx").equals(reader.getName())){
                                
                                                object.setAlcoholRecipientTypeFedEx(com.netsuite.webservices.platform.common_2017_2.types.AlcoholRecipientType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isNonHazLithiumFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isNonHazLithiumFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsNonHazLithiumFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","insuredValueFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"insuredValueFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInsuredValueFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setInsuredValueFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","useInsuredValueFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useInsuredValueFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseInsuredValueFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","reference1FedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reference1FedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReference1FedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","priorityAlertTypeFedEx").equals(reader.getName())){
                                
                                                object.setPriorityAlertTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPriorityAlertTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","priorityAlertContentFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"priorityAlertContentFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPriorityAlertContentFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageLengthFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageLengthFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageLengthFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageLengthFedEx(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageWidthFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageWidthFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageWidthFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageWidthFedEx(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageHeightFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageHeightFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageHeightFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageHeightFedEx(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","useCodFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useCodFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseCodFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codAmountFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"codAmountFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCodAmountFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCodAmountFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codMethodFedEx").equals(reader.getName())){
                                
                                                object.setCodMethodFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodMethodFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codFreightTypeFedEx").equals(reader.getName())){
                                
                                                object.setCodFreightTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodFreightTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deliveryConfFedEx").equals(reader.getName())){
                                
                                                object.setDeliveryConfFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExDeliveryConfFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","signatureOptionsFedEx").equals(reader.getName())){
                                
                                                object.setSignatureOptionsFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExSignatureOptionsFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","signatureReleaseFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"signatureReleaseFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSignatureReleaseFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","authorizationNumberFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"authorizationNumberFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAuthorizationNumberFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    