
/**
 * ItemFulfillmentPackageFedExAdmPackageTypeFedEx.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2.types;
            

            /**
            *  ItemFulfillmentPackageFedExAdmPackageTypeFedEx bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemFulfillmentPackageFedExAdmPackageTypeFedEx
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.sales_2017_2.transactions.webservices.netsuite.com",
                "ItemFulfillmentPackageFedExAdmPackageTypeFedEx",
                "ns20");

            

                        /**
                        * field for ItemFulfillmentPackageFedExAdmPackageTypeFedEx
                        */

                        
                                    protected java.lang.String localItemFulfillmentPackageFedExAdmPackageTypeFedEx ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected ItemFulfillmentPackageFedExAdmPackageTypeFedEx(java.lang.String value, boolean isRegisterValue) {
                                    localItemFulfillmentPackageFedExAdmPackageTypeFedEx = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localItemFulfillmentPackageFedExAdmPackageTypeFedEx, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __bag =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bag");
                                
                                    public static final java.lang.String __barrel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_barrel");
                                
                                    public static final java.lang.String __basketOrHamper =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_basketOrHamper");
                                
                                    public static final java.lang.String __box =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_box");
                                
                                    public static final java.lang.String __bucket =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bucket");
                                
                                    public static final java.lang.String __bundle =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bundle");
                                
                                    public static final java.lang.String __cage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cage");
                                
                                    public static final java.lang.String __carton =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_carton");
                                
                                    public static final java.lang.String __case =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_case");
                                
                                    public static final java.lang.String __chest =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chest");
                                
                                    public static final java.lang.String __container =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_container");
                                
                                    public static final java.lang.String __crate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_crate");
                                
                                    public static final java.lang.String __cylinder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cylinder");
                                
                                    public static final java.lang.String __drum =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_drum");
                                
                                    public static final java.lang.String __envelope =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_envelope");
                                
                                    public static final java.lang.String __package =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_package");
                                
                                    public static final java.lang.String __pail =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pail");
                                
                                    public static final java.lang.String __pallet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pallet");
                                
                                    public static final java.lang.String __parcel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_parcel");
                                
                                    public static final java.lang.String __pieces =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pieces");
                                
                                    public static final java.lang.String __reel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reel");
                                
                                    public static final java.lang.String __roll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_roll");
                                
                                    public static final java.lang.String __sack =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sack");
                                
                                    public static final java.lang.String __shrinkWrapped =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shrinkWrapped");
                                
                                    public static final java.lang.String __skid =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_skid");
                                
                                    public static final java.lang.String __tank =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tank");
                                
                                    public static final java.lang.String __toteBin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_toteBin");
                                
                                    public static final java.lang.String __tube =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tube");
                                
                                    public static final java.lang.String __unit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unit");
                                
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _bag =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__bag,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _barrel =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__barrel,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _basketOrHamper =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__basketOrHamper,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _box =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__box,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _bucket =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__bucket,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _bundle =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__bundle,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _cage =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__cage,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _carton =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__carton,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _case =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__case,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _chest =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__chest,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _container =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__container,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _crate =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__crate,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _cylinder =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__cylinder,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _drum =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__drum,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _envelope =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__envelope,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _package =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__package,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _pail =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__pail,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _pallet =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__pallet,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _parcel =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__parcel,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _pieces =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__pieces,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _reel =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__reel,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _roll =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__roll,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _sack =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__sack,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _shrinkWrapped =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__shrinkWrapped,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _skid =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__skid,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _tank =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__tank,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _toteBin =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__toteBin,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _tube =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__tube,true);
                            
                                public static final ItemFulfillmentPackageFedExAdmPackageTypeFedEx _unit =
                                    new ItemFulfillmentPackageFedExAdmPackageTypeFedEx(__unit,true);
                            

                                public java.lang.String getValue() { return localItemFulfillmentPackageFedExAdmPackageTypeFedEx;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localItemFulfillmentPackageFedExAdmPackageTypeFedEx.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.sales_2017_2.transactions.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":ItemFulfillmentPackageFedExAdmPackageTypeFedEx",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "ItemFulfillmentPackageFedExAdmPackageTypeFedEx",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localItemFulfillmentPackageFedExAdmPackageTypeFedEx==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("ItemFulfillmentPackageFedExAdmPackageTypeFedEx cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localItemFulfillmentPackageFedExAdmPackageTypeFedEx);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns20";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemFulfillmentPackageFedExAdmPackageTypeFedEx)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static ItemFulfillmentPackageFedExAdmPackageTypeFedEx fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    ItemFulfillmentPackageFedExAdmPackageTypeFedEx enumeration = (ItemFulfillmentPackageFedExAdmPackageTypeFedEx)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static ItemFulfillmentPackageFedExAdmPackageTypeFedEx fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static ItemFulfillmentPackageFedExAdmPackageTypeFedEx fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return ItemFulfillmentPackageFedExAdmPackageTypeFedEx.Factory.fromString(content,namespaceUri);
                    } else {
                       return ItemFulfillmentPackageFedExAdmPackageTypeFedEx.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemFulfillmentPackageFedExAdmPackageTypeFedEx parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemFulfillmentPackageFedExAdmPackageTypeFedEx object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ItemFulfillmentPackageFedExAdmPackageTypeFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = ItemFulfillmentPackageFedExAdmPackageTypeFedEx.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = ItemFulfillmentPackageFedExAdmPackageTypeFedEx.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    