
/**
 * TransactionLinkType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2.types;
            

            /**
            *  TransactionLinkType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionLinkType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.sales_2017_2.transactions.webservices.netsuite.com",
                "TransactionLinkType",
                "ns20");

            

                        /**
                        * field for TransactionLinkType
                        */

                        
                                    protected java.lang.String localTransactionLinkType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected TransactionLinkType(java.lang.String value, boolean isRegisterValue) {
                                    localTransactionLinkType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localTransactionLinkType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __advancedCostAmortization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_advancedCostAmortization");
                                
                                    public static final java.lang.String __authorizationDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_authorizationDeposit");
                                
                                    public static final java.lang.String __blcgaJeFullfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_blcgaJeFullfillment");
                                
                                    public static final java.lang.String __closedPeriodFxVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_closedPeriodFxVariance");
                                
                                    public static final java.lang.String __closeWorkOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_closeWorkOrder");
                                
                                    public static final java.lang.String __cogsLink =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cogsLink");
                                
                                    public static final java.lang.String __collectTegata =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_collectTegata");
                                
                                    public static final java.lang.String __commission =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commission");
                                
                                    public static final java.lang.String __contractCostDeferral =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_contractCostDeferral");
                                
                                    public static final java.lang.String __contractCostDeferralReversal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_contractCostDeferralReversal");
                                
                                    public static final java.lang.String __deferredRevenueReallocation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deferredRevenueReallocation");
                                
                                    public static final java.lang.String __depositApplication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_depositApplication");
                                
                                    public static final java.lang.String __depositRefundCheck =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_depositRefundCheck");
                                
                                    public static final java.lang.String __discountTegata =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_discountTegata");
                                
                                    public static final java.lang.String __dropShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dropShipment");
                                
                                    public static final java.lang.String __endorseTegata =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_endorseTegata");
                                
                                    public static final java.lang.String __estimateInvoicing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimateInvoicing");
                                
                                    public static final java.lang.String __fulfillmentRequestFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fulfillmentRequestFulfillment");
                                
                                    public static final java.lang.String __glImpactAdjustment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_glImpactAdjustment");
                                
                                    public static final java.lang.String __intercompanyAdjustment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_intercompanyAdjustment");
                                
                                    public static final java.lang.String __inventoryCountAdjustment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryCountAdjustment");
                                
                                    public static final java.lang.String __landedCost =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_landedCost");
                                
                                    public static final java.lang.String __linkedReturnCost =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_linkedReturnCost");
                                
                                    public static final java.lang.String __opportunityClose =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunityClose");
                                
                                    public static final java.lang.String __opportunityEstimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunityEstimate");
                                
                                    public static final java.lang.String __orderBillInvoice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_orderBillInvoice");
                                
                                    public static final java.lang.String __orderFulfillmentRequest =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_orderFulfillmentRequest");
                                
                                    public static final java.lang.String __orderPickingPacking =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_orderPickingPacking");
                                
                                    public static final java.lang.String __ownershipTransferItemReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ownershipTransferItemReceipt");
                                
                                    public static final java.lang.String __payment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payment");
                                
                                    public static final java.lang.String __paymentRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentRefund");
                                
                                    public static final java.lang.String __payTegata =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payTegata");
                                
                                    public static final java.lang.String __poToOwnershipTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_poToOwnershipTransfer");
                                
                                    public static final java.lang.String __purchaseContractOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseContractOrder");
                                
                                    public static final java.lang.String __purchaseOrderRequisition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderRequisition");
                                
                                    public static final java.lang.String __purchaseOrderToBlanket =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderToBlanket");
                                
                                    public static final java.lang.String __purchaseReturn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseReturn");
                                
                                    public static final java.lang.String __receiptBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_receiptBill");
                                
                                    public static final java.lang.String __receiptFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_receiptFulfillment");
                                
                                    public static final java.lang.String __reimbursement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reimbursement");
                                
                                    public static final java.lang.String __revalueWorkOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revalueWorkOrder");
                                
                                    public static final java.lang.String __revenueAmortizatonRecognition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueAmortizatonRecognition");
                                
                                    public static final java.lang.String __revenueArrangement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueArrangement");
                                
                                    public static final java.lang.String __revenueCommitted =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueCommitted");
                                
                                    public static final java.lang.String __rfqToVendorRfq =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rfqToVendorRfq");
                                
                                    public static final java.lang.String __saleReturn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saleReturn");
                                
                                    public static final java.lang.String __salesOrderAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderAuthorization");
                                
                                    public static final java.lang.String __salesOrderDegross =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderDegross");
                                
                                    public static final java.lang.String __salesOrderDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderDeposit");
                                
                                    public static final java.lang.String __salesOrderRevenueRevaluation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderRevenueRevaluation");
                                
                                    public static final java.lang.String __sourceOfRevenueContract =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sourceOfRevenueContract");
                                
                                    public static final java.lang.String __specialOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_specialOrder");
                                
                                    public static final java.lang.String __transferOrderFulfillmentReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderFulfillmentReceipt");
                                
                                    public static final java.lang.String __vendorBillVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorBillVariance");
                                
                                    public static final java.lang.String __vendorRfqToPurchaseContract =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorRfqToPurchaseContract");
                                
                                    public static final java.lang.String __wipBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_wipBuild");
                                
                                    public static final java.lang.String __workOrderBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderBuild");
                                
                                public static final TransactionLinkType _advancedCostAmortization =
                                    new TransactionLinkType(__advancedCostAmortization,true);
                            
                                public static final TransactionLinkType _authorizationDeposit =
                                    new TransactionLinkType(__authorizationDeposit,true);
                            
                                public static final TransactionLinkType _blcgaJeFullfillment =
                                    new TransactionLinkType(__blcgaJeFullfillment,true);
                            
                                public static final TransactionLinkType _closedPeriodFxVariance =
                                    new TransactionLinkType(__closedPeriodFxVariance,true);
                            
                                public static final TransactionLinkType _closeWorkOrder =
                                    new TransactionLinkType(__closeWorkOrder,true);
                            
                                public static final TransactionLinkType _cogsLink =
                                    new TransactionLinkType(__cogsLink,true);
                            
                                public static final TransactionLinkType _collectTegata =
                                    new TransactionLinkType(__collectTegata,true);
                            
                                public static final TransactionLinkType _commission =
                                    new TransactionLinkType(__commission,true);
                            
                                public static final TransactionLinkType _contractCostDeferral =
                                    new TransactionLinkType(__contractCostDeferral,true);
                            
                                public static final TransactionLinkType _contractCostDeferralReversal =
                                    new TransactionLinkType(__contractCostDeferralReversal,true);
                            
                                public static final TransactionLinkType _deferredRevenueReallocation =
                                    new TransactionLinkType(__deferredRevenueReallocation,true);
                            
                                public static final TransactionLinkType _depositApplication =
                                    new TransactionLinkType(__depositApplication,true);
                            
                                public static final TransactionLinkType _depositRefundCheck =
                                    new TransactionLinkType(__depositRefundCheck,true);
                            
                                public static final TransactionLinkType _discountTegata =
                                    new TransactionLinkType(__discountTegata,true);
                            
                                public static final TransactionLinkType _dropShipment =
                                    new TransactionLinkType(__dropShipment,true);
                            
                                public static final TransactionLinkType _endorseTegata =
                                    new TransactionLinkType(__endorseTegata,true);
                            
                                public static final TransactionLinkType _estimateInvoicing =
                                    new TransactionLinkType(__estimateInvoicing,true);
                            
                                public static final TransactionLinkType _fulfillmentRequestFulfillment =
                                    new TransactionLinkType(__fulfillmentRequestFulfillment,true);
                            
                                public static final TransactionLinkType _glImpactAdjustment =
                                    new TransactionLinkType(__glImpactAdjustment,true);
                            
                                public static final TransactionLinkType _intercompanyAdjustment =
                                    new TransactionLinkType(__intercompanyAdjustment,true);
                            
                                public static final TransactionLinkType _inventoryCountAdjustment =
                                    new TransactionLinkType(__inventoryCountAdjustment,true);
                            
                                public static final TransactionLinkType _landedCost =
                                    new TransactionLinkType(__landedCost,true);
                            
                                public static final TransactionLinkType _linkedReturnCost =
                                    new TransactionLinkType(__linkedReturnCost,true);
                            
                                public static final TransactionLinkType _opportunityClose =
                                    new TransactionLinkType(__opportunityClose,true);
                            
                                public static final TransactionLinkType _opportunityEstimate =
                                    new TransactionLinkType(__opportunityEstimate,true);
                            
                                public static final TransactionLinkType _orderBillInvoice =
                                    new TransactionLinkType(__orderBillInvoice,true);
                            
                                public static final TransactionLinkType _orderFulfillmentRequest =
                                    new TransactionLinkType(__orderFulfillmentRequest,true);
                            
                                public static final TransactionLinkType _orderPickingPacking =
                                    new TransactionLinkType(__orderPickingPacking,true);
                            
                                public static final TransactionLinkType _ownershipTransferItemReceipt =
                                    new TransactionLinkType(__ownershipTransferItemReceipt,true);
                            
                                public static final TransactionLinkType _payment =
                                    new TransactionLinkType(__payment,true);
                            
                                public static final TransactionLinkType _paymentRefund =
                                    new TransactionLinkType(__paymentRefund,true);
                            
                                public static final TransactionLinkType _payTegata =
                                    new TransactionLinkType(__payTegata,true);
                            
                                public static final TransactionLinkType _poToOwnershipTransfer =
                                    new TransactionLinkType(__poToOwnershipTransfer,true);
                            
                                public static final TransactionLinkType _purchaseContractOrder =
                                    new TransactionLinkType(__purchaseContractOrder,true);
                            
                                public static final TransactionLinkType _purchaseOrderRequisition =
                                    new TransactionLinkType(__purchaseOrderRequisition,true);
                            
                                public static final TransactionLinkType _purchaseOrderToBlanket =
                                    new TransactionLinkType(__purchaseOrderToBlanket,true);
                            
                                public static final TransactionLinkType _purchaseReturn =
                                    new TransactionLinkType(__purchaseReturn,true);
                            
                                public static final TransactionLinkType _receiptBill =
                                    new TransactionLinkType(__receiptBill,true);
                            
                                public static final TransactionLinkType _receiptFulfillment =
                                    new TransactionLinkType(__receiptFulfillment,true);
                            
                                public static final TransactionLinkType _reimbursement =
                                    new TransactionLinkType(__reimbursement,true);
                            
                                public static final TransactionLinkType _revalueWorkOrder =
                                    new TransactionLinkType(__revalueWorkOrder,true);
                            
                                public static final TransactionLinkType _revenueAmortizatonRecognition =
                                    new TransactionLinkType(__revenueAmortizatonRecognition,true);
                            
                                public static final TransactionLinkType _revenueArrangement =
                                    new TransactionLinkType(__revenueArrangement,true);
                            
                                public static final TransactionLinkType _revenueCommitted =
                                    new TransactionLinkType(__revenueCommitted,true);
                            
                                public static final TransactionLinkType _rfqToVendorRfq =
                                    new TransactionLinkType(__rfqToVendorRfq,true);
                            
                                public static final TransactionLinkType _saleReturn =
                                    new TransactionLinkType(__saleReturn,true);
                            
                                public static final TransactionLinkType _salesOrderAuthorization =
                                    new TransactionLinkType(__salesOrderAuthorization,true);
                            
                                public static final TransactionLinkType _salesOrderDegross =
                                    new TransactionLinkType(__salesOrderDegross,true);
                            
                                public static final TransactionLinkType _salesOrderDeposit =
                                    new TransactionLinkType(__salesOrderDeposit,true);
                            
                                public static final TransactionLinkType _salesOrderRevenueRevaluation =
                                    new TransactionLinkType(__salesOrderRevenueRevaluation,true);
                            
                                public static final TransactionLinkType _sourceOfRevenueContract =
                                    new TransactionLinkType(__sourceOfRevenueContract,true);
                            
                                public static final TransactionLinkType _specialOrder =
                                    new TransactionLinkType(__specialOrder,true);
                            
                                public static final TransactionLinkType _transferOrderFulfillmentReceipt =
                                    new TransactionLinkType(__transferOrderFulfillmentReceipt,true);
                            
                                public static final TransactionLinkType _vendorBillVariance =
                                    new TransactionLinkType(__vendorBillVariance,true);
                            
                                public static final TransactionLinkType _vendorRfqToPurchaseContract =
                                    new TransactionLinkType(__vendorRfqToPurchaseContract,true);
                            
                                public static final TransactionLinkType _wipBuild =
                                    new TransactionLinkType(__wipBuild,true);
                            
                                public static final TransactionLinkType _workOrderBuild =
                                    new TransactionLinkType(__workOrderBuild,true);
                            

                                public java.lang.String getValue() { return localTransactionLinkType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localTransactionLinkType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.sales_2017_2.transactions.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":TransactionLinkType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "TransactionLinkType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localTransactionLinkType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("TransactionLinkType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localTransactionLinkType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns20";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionLinkType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static TransactionLinkType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    TransactionLinkType enumeration = (TransactionLinkType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static TransactionLinkType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static TransactionLinkType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return TransactionLinkType.Factory.fromString(content,namespaceUri);
                    } else {
                       return TransactionLinkType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionLinkType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionLinkType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TransactionLinkType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = TransactionLinkType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = TransactionLinkType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    