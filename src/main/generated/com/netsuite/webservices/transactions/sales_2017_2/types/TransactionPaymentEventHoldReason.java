
/**
 * TransactionPaymentEventHoldReason.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2.types;
            

            /**
            *  TransactionPaymentEventHoldReason bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionPaymentEventHoldReason
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.sales_2017_2.transactions.webservices.netsuite.com",
                "TransactionPaymentEventHoldReason",
                "ns20");

            

                        /**
                        * field for TransactionPaymentEventHoldReason
                        */

                        
                                    protected java.lang.String localTransactionPaymentEventHoldReason ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected TransactionPaymentEventHoldReason(java.lang.String value, boolean isRegisterValue) {
                                    localTransactionPaymentEventHoldReason = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localTransactionPaymentEventHoldReason, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __amountExceedsMaximumAllowedAmount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_amountExceedsMaximumAllowedAmount");
                                
                                    public static final java.lang.String __authorizationDecline =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_authorizationDecline");
                                
                                    public static final java.lang.String __cardExpired =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cardExpired");
                                
                                    public static final java.lang.String __cardInvalid =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cardInvalid");
                                
                                    public static final java.lang.String __confirmationOfTheOperationIsPending =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_confirmationOfTheOperationIsPending");
                                
                                    public static final java.lang.String __externalFraudRejection =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_externalFraudRejection");
                                
                                    public static final java.lang.String __externalFraudReview =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_externalFraudReview");
                                
                                    public static final java.lang.String __fatalError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fatalError");
                                
                                    public static final java.lang.String __forwardedToPayerAuthentication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_forwardedToPayerAuthentication");
                                
                                    public static final java.lang.String __forwardRequested =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_forwardRequested");
                                
                                    public static final java.lang.String __gatewayAsynchronousNotification =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gatewayAsynchronousNotification");
                                
                                    public static final java.lang.String __gatewayError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gatewayError");
                                
                                    public static final java.lang.String __generalHold =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generalHold");
                                
                                    public static final java.lang.String __generalReject =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generalReject");
                                
                                    public static final java.lang.String __operationWasSuccessful =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_operationWasSuccessful");
                                
                                    public static final java.lang.String __operationWasTerminated =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_operationWasTerminated");
                                
                                    public static final java.lang.String __overridenBy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_overridenBy");
                                
                                    public static final java.lang.String __partnerDecline =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerDecline");
                                
                                    public static final java.lang.String __paymentDeviceWasPrimed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentDeviceWasPrimed");
                                
                                    public static final java.lang.String __paymentOperationWasCanceled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentOperationWasCanceled");
                                
                                    public static final java.lang.String __systemError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_systemError");
                                
                                    public static final java.lang.String __verbalAuthorizationRequested =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_verbalAuthorizationRequested");
                                
                                    public static final java.lang.String __verificationRejection =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_verificationRejection");
                                
                                    public static final java.lang.String __verificationRequired =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_verificationRequired");
                                
                                public static final TransactionPaymentEventHoldReason _amountExceedsMaximumAllowedAmount =
                                    new TransactionPaymentEventHoldReason(__amountExceedsMaximumAllowedAmount,true);
                            
                                public static final TransactionPaymentEventHoldReason _authorizationDecline =
                                    new TransactionPaymentEventHoldReason(__authorizationDecline,true);
                            
                                public static final TransactionPaymentEventHoldReason _cardExpired =
                                    new TransactionPaymentEventHoldReason(__cardExpired,true);
                            
                                public static final TransactionPaymentEventHoldReason _cardInvalid =
                                    new TransactionPaymentEventHoldReason(__cardInvalid,true);
                            
                                public static final TransactionPaymentEventHoldReason _confirmationOfTheOperationIsPending =
                                    new TransactionPaymentEventHoldReason(__confirmationOfTheOperationIsPending,true);
                            
                                public static final TransactionPaymentEventHoldReason _externalFraudRejection =
                                    new TransactionPaymentEventHoldReason(__externalFraudRejection,true);
                            
                                public static final TransactionPaymentEventHoldReason _externalFraudReview =
                                    new TransactionPaymentEventHoldReason(__externalFraudReview,true);
                            
                                public static final TransactionPaymentEventHoldReason _fatalError =
                                    new TransactionPaymentEventHoldReason(__fatalError,true);
                            
                                public static final TransactionPaymentEventHoldReason _forwardedToPayerAuthentication =
                                    new TransactionPaymentEventHoldReason(__forwardedToPayerAuthentication,true);
                            
                                public static final TransactionPaymentEventHoldReason _forwardRequested =
                                    new TransactionPaymentEventHoldReason(__forwardRequested,true);
                            
                                public static final TransactionPaymentEventHoldReason _gatewayAsynchronousNotification =
                                    new TransactionPaymentEventHoldReason(__gatewayAsynchronousNotification,true);
                            
                                public static final TransactionPaymentEventHoldReason _gatewayError =
                                    new TransactionPaymentEventHoldReason(__gatewayError,true);
                            
                                public static final TransactionPaymentEventHoldReason _generalHold =
                                    new TransactionPaymentEventHoldReason(__generalHold,true);
                            
                                public static final TransactionPaymentEventHoldReason _generalReject =
                                    new TransactionPaymentEventHoldReason(__generalReject,true);
                            
                                public static final TransactionPaymentEventHoldReason _operationWasSuccessful =
                                    new TransactionPaymentEventHoldReason(__operationWasSuccessful,true);
                            
                                public static final TransactionPaymentEventHoldReason _operationWasTerminated =
                                    new TransactionPaymentEventHoldReason(__operationWasTerminated,true);
                            
                                public static final TransactionPaymentEventHoldReason _overridenBy =
                                    new TransactionPaymentEventHoldReason(__overridenBy,true);
                            
                                public static final TransactionPaymentEventHoldReason _partnerDecline =
                                    new TransactionPaymentEventHoldReason(__partnerDecline,true);
                            
                                public static final TransactionPaymentEventHoldReason _paymentDeviceWasPrimed =
                                    new TransactionPaymentEventHoldReason(__paymentDeviceWasPrimed,true);
                            
                                public static final TransactionPaymentEventHoldReason _paymentOperationWasCanceled =
                                    new TransactionPaymentEventHoldReason(__paymentOperationWasCanceled,true);
                            
                                public static final TransactionPaymentEventHoldReason _systemError =
                                    new TransactionPaymentEventHoldReason(__systemError,true);
                            
                                public static final TransactionPaymentEventHoldReason _verbalAuthorizationRequested =
                                    new TransactionPaymentEventHoldReason(__verbalAuthorizationRequested,true);
                            
                                public static final TransactionPaymentEventHoldReason _verificationRejection =
                                    new TransactionPaymentEventHoldReason(__verificationRejection,true);
                            
                                public static final TransactionPaymentEventHoldReason _verificationRequired =
                                    new TransactionPaymentEventHoldReason(__verificationRequired,true);
                            

                                public java.lang.String getValue() { return localTransactionPaymentEventHoldReason;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localTransactionPaymentEventHoldReason.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.sales_2017_2.transactions.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":TransactionPaymentEventHoldReason",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "TransactionPaymentEventHoldReason",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localTransactionPaymentEventHoldReason==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("TransactionPaymentEventHoldReason cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localTransactionPaymentEventHoldReason);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns20";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionPaymentEventHoldReason)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static TransactionPaymentEventHoldReason fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    TransactionPaymentEventHoldReason enumeration = (TransactionPaymentEventHoldReason)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static TransactionPaymentEventHoldReason fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static TransactionPaymentEventHoldReason fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return TransactionPaymentEventHoldReason.Factory.fromString(content,namespaceUri);
                    } else {
                       return TransactionPaymentEventHoldReason.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionPaymentEventHoldReason parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionPaymentEventHoldReason object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TransactionPaymentEventHoldReason" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = TransactionPaymentEventHoldReason.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = TransactionPaymentEventHoldReason.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    