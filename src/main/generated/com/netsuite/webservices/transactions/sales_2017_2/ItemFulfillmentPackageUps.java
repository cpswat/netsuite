
/**
 * ItemFulfillmentPackageUps.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  ItemFulfillmentPackageUps bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemFulfillmentPackageUps
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemFulfillmentPackageUps
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for PackageWeightUps
                        */

                        
                                    protected double localPackageWeightUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageWeightUpsTracker = false ;

                           public boolean isPackageWeightUpsSpecified(){
                               return localPackageWeightUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPackageWeightUps(){
                               return localPackageWeightUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageWeightUps
                               */
                               public void setPackageWeightUps(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageWeightUpsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPackageWeightUps=param;
                                    

                               }
                            

                        /**
                        * field for PackageDescrUps
                        */

                        
                                    protected java.lang.String localPackageDescrUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageDescrUpsTracker = false ;

                           public boolean isPackageDescrUpsSpecified(){
                               return localPackageDescrUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPackageDescrUps(){
                               return localPackageDescrUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageDescrUps
                               */
                               public void setPackageDescrUps(java.lang.String param){
                            localPackageDescrUpsTracker = param != null;
                                   
                                            this.localPackageDescrUps=param;
                                    

                               }
                            

                        /**
                        * field for PackageTrackingNumberUps
                        */

                        
                                    protected java.lang.String localPackageTrackingNumberUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageTrackingNumberUpsTracker = false ;

                           public boolean isPackageTrackingNumberUpsSpecified(){
                               return localPackageTrackingNumberUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPackageTrackingNumberUps(){
                               return localPackageTrackingNumberUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageTrackingNumberUps
                               */
                               public void setPackageTrackingNumberUps(java.lang.String param){
                            localPackageTrackingNumberUpsTracker = param != null;
                                   
                                            this.localPackageTrackingNumberUps=param;
                                    

                               }
                            

                        /**
                        * field for PackagingUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsPackagingUps localPackagingUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackagingUpsTracker = false ;

                           public boolean isPackagingUpsSpecified(){
                               return localPackagingUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsPackagingUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsPackagingUps getPackagingUps(){
                               return localPackagingUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackagingUps
                               */
                               public void setPackagingUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsPackagingUps param){
                            localPackagingUpsTracker = param != null;
                                   
                                            this.localPackagingUps=param;
                                    

                               }
                            

                        /**
                        * field for UseInsuredValueUps
                        */

                        
                                    protected boolean localUseInsuredValueUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseInsuredValueUpsTracker = false ;

                           public boolean isUseInsuredValueUpsSpecified(){
                               return localUseInsuredValueUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseInsuredValueUps(){
                               return localUseInsuredValueUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseInsuredValueUps
                               */
                               public void setUseInsuredValueUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseInsuredValueUpsTracker =
                                       true;
                                   
                                            this.localUseInsuredValueUps=param;
                                    

                               }
                            

                        /**
                        * field for InsuredValueUps
                        */

                        
                                    protected double localInsuredValueUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsuredValueUpsTracker = false ;

                           public boolean isInsuredValueUpsSpecified(){
                               return localInsuredValueUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getInsuredValueUps(){
                               return localInsuredValueUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsuredValueUps
                               */
                               public void setInsuredValueUps(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localInsuredValueUpsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localInsuredValueUps=param;
                                    

                               }
                            

                        /**
                        * field for Reference1Ups
                        */

                        
                                    protected java.lang.String localReference1Ups ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReference1UpsTracker = false ;

                           public boolean isReference1UpsSpecified(){
                               return localReference1UpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getReference1Ups(){
                               return localReference1Ups;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reference1Ups
                               */
                               public void setReference1Ups(java.lang.String param){
                            localReference1UpsTracker = param != null;
                                   
                                            this.localReference1Ups=param;
                                    

                               }
                            

                        /**
                        * field for Reference2Ups
                        */

                        
                                    protected java.lang.String localReference2Ups ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReference2UpsTracker = false ;

                           public boolean isReference2UpsSpecified(){
                               return localReference2UpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getReference2Ups(){
                               return localReference2Ups;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reference2Ups
                               */
                               public void setReference2Ups(java.lang.String param){
                            localReference2UpsTracker = param != null;
                                   
                                            this.localReference2Ups=param;
                                    

                               }
                            

                        /**
                        * field for PackageLengthUps
                        */

                        
                                    protected long localPackageLengthUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageLengthUpsTracker = false ;

                           public boolean isPackageLengthUpsSpecified(){
                               return localPackageLengthUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageLengthUps(){
                               return localPackageLengthUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageLengthUps
                               */
                               public void setPackageLengthUps(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageLengthUpsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageLengthUps=param;
                                    

                               }
                            

                        /**
                        * field for PackageWidthUps
                        */

                        
                                    protected long localPackageWidthUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageWidthUpsTracker = false ;

                           public boolean isPackageWidthUpsSpecified(){
                               return localPackageWidthUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageWidthUps(){
                               return localPackageWidthUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageWidthUps
                               */
                               public void setPackageWidthUps(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageWidthUpsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageWidthUps=param;
                                    

                               }
                            

                        /**
                        * field for PackageHeightUps
                        */

                        
                                    protected long localPackageHeightUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageHeightUpsTracker = false ;

                           public boolean isPackageHeightUpsSpecified(){
                               return localPackageHeightUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPackageHeightUps(){
                               return localPackageHeightUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageHeightUps
                               */
                               public void setPackageHeightUps(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPackageHeightUpsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPackageHeightUps=param;
                                    

                               }
                            

                        /**
                        * field for AdditionalHandlingUps
                        */

                        
                                    protected boolean localAdditionalHandlingUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAdditionalHandlingUpsTracker = false ;

                           public boolean isAdditionalHandlingUpsSpecified(){
                               return localAdditionalHandlingUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAdditionalHandlingUps(){
                               return localAdditionalHandlingUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AdditionalHandlingUps
                               */
                               public void setAdditionalHandlingUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAdditionalHandlingUpsTracker =
                                       true;
                                   
                                            this.localAdditionalHandlingUps=param;
                                    

                               }
                            

                        /**
                        * field for UseCodUps
                        */

                        
                                    protected boolean localUseCodUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseCodUpsTracker = false ;

                           public boolean isUseCodUpsSpecified(){
                               return localUseCodUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseCodUps(){
                               return localUseCodUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseCodUps
                               */
                               public void setUseCodUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseCodUpsTracker =
                                       true;
                                   
                                            this.localUseCodUps=param;
                                    

                               }
                            

                        /**
                        * field for CodAmountUps
                        */

                        
                                    protected double localCodAmountUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodAmountUpsTracker = false ;

                           public boolean isCodAmountUpsSpecified(){
                               return localCodAmountUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCodAmountUps(){
                               return localCodAmountUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodAmountUps
                               */
                               public void setCodAmountUps(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCodAmountUpsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCodAmountUps=param;
                                    

                               }
                            

                        /**
                        * field for CodMethodUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsCodMethodUps localCodMethodUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodMethodUpsTracker = false ;

                           public boolean isCodMethodUpsSpecified(){
                               return localCodMethodUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsCodMethodUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsCodMethodUps getCodMethodUps(){
                               return localCodMethodUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodMethodUps
                               */
                               public void setCodMethodUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsCodMethodUps param){
                            localCodMethodUpsTracker = param != null;
                                   
                                            this.localCodMethodUps=param;
                                    

                               }
                            

                        /**
                        * field for DeliveryConfUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsDeliveryConfUps localDeliveryConfUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeliveryConfUpsTracker = false ;

                           public boolean isDeliveryConfUpsSpecified(){
                               return localDeliveryConfUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsDeliveryConfUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsDeliveryConfUps getDeliveryConfUps(){
                               return localDeliveryConfUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeliveryConfUps
                               */
                               public void setDeliveryConfUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsDeliveryConfUps param){
                            localDeliveryConfUpsTracker = param != null;
                                   
                                            this.localDeliveryConfUps=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemFulfillmentPackageUps",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemFulfillmentPackageUps",
                           xmlWriter);
                   }

               
                   }
                if (localPackageWeightUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageWeightUps", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPackageWeightUps)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageWeightUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWeightUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageDescrUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageDescrUps", xmlWriter);
                             

                                          if (localPackageDescrUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("packageDescrUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPackageDescrUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageTrackingNumberUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageTrackingNumberUps", xmlWriter);
                             

                                          if (localPackageTrackingNumberUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("packageTrackingNumberUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPackageTrackingNumberUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackagingUpsTracker){
                                            if (localPackagingUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packagingUps cannot be null!!");
                                            }
                                           localPackagingUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packagingUps"),
                                               xmlWriter);
                                        } if (localUseInsuredValueUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useInsuredValueUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useInsuredValueUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseInsuredValueUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsuredValueUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "insuredValueUps", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localInsuredValueUps)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("insuredValueUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsuredValueUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReference1UpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reference1Ups", xmlWriter);
                             

                                          if (localReference1Ups==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("reference1Ups cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReference1Ups);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReference2UpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reference2Ups", xmlWriter);
                             

                                          if (localReference2Ups==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("reference2Ups cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReference2Ups);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageLengthUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageLengthUps", xmlWriter);
                             
                                               if (localPackageLengthUps==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageLengthUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageLengthUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageWidthUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageWidthUps", xmlWriter);
                             
                                               if (localPackageWidthUps==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageWidthUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWidthUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackageHeightUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packageHeightUps", xmlWriter);
                             
                                               if (localPackageHeightUps==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("packageHeightUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageHeightUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAdditionalHandlingUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "additionalHandlingUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("additionalHandlingUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAdditionalHandlingUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUseCodUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useCodUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useCodUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseCodUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCodAmountUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "codAmountUps", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCodAmountUps)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("codAmountUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodAmountUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCodMethodUpsTracker){
                                            if (localCodMethodUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("codMethodUps cannot be null!!");
                                            }
                                           localCodMethodUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codMethodUps"),
                                               xmlWriter);
                                        } if (localDeliveryConfUpsTracker){
                                            if (localDeliveryConfUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deliveryConfUps cannot be null!!");
                                            }
                                           localDeliveryConfUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deliveryConfUps"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localPackageWeightUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageWeightUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWeightUps));
                            } if (localPackageDescrUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageDescrUps"));
                                 
                                        if (localPackageDescrUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageDescrUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("packageDescrUps cannot be null!!");
                                        }
                                    } if (localPackageTrackingNumberUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageTrackingNumberUps"));
                                 
                                        if (localPackageTrackingNumberUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageTrackingNumberUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("packageTrackingNumberUps cannot be null!!");
                                        }
                                    } if (localPackagingUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packagingUps"));
                            
                            
                                    if (localPackagingUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("packagingUps cannot be null!!");
                                    }
                                    elementList.add(localPackagingUps);
                                } if (localUseInsuredValueUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "useInsuredValueUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseInsuredValueUps));
                            } if (localInsuredValueUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "insuredValueUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsuredValueUps));
                            } if (localReference1UpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "reference1Ups"));
                                 
                                        if (localReference1Ups != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReference1Ups));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("reference1Ups cannot be null!!");
                                        }
                                    } if (localReference2UpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "reference2Ups"));
                                 
                                        if (localReference2Ups != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReference2Ups));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("reference2Ups cannot be null!!");
                                        }
                                    } if (localPackageLengthUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageLengthUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageLengthUps));
                            } if (localPackageWidthUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageWidthUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageWidthUps));
                            } if (localPackageHeightUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageHeightUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackageHeightUps));
                            } if (localAdditionalHandlingUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "additionalHandlingUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAdditionalHandlingUps));
                            } if (localUseCodUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "useCodUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseCodUps));
                            } if (localCodAmountUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "codAmountUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCodAmountUps));
                            } if (localCodMethodUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "codMethodUps"));
                            
                            
                                    if (localCodMethodUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("codMethodUps cannot be null!!");
                                    }
                                    elementList.add(localCodMethodUps);
                                } if (localDeliveryConfUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "deliveryConfUps"));
                            
                            
                                    if (localDeliveryConfUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("deliveryConfUps cannot be null!!");
                                    }
                                    elementList.add(localDeliveryConfUps);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemFulfillmentPackageUps parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemFulfillmentPackageUps object =
                new ItemFulfillmentPackageUps();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemFulfillmentPackageUps".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemFulfillmentPackageUps)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageWeightUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageWeightUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageWeightUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageWeightUps(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageDescrUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageDescrUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageDescrUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageTrackingNumberUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageTrackingNumberUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageTrackingNumberUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packagingUps").equals(reader.getName())){
                                
                                                object.setPackagingUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsPackagingUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","useInsuredValueUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useInsuredValueUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseInsuredValueUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","insuredValueUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"insuredValueUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInsuredValueUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setInsuredValueUps(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","reference1Ups").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reference1Ups" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReference1Ups(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","reference2Ups").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reference2Ups" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReference2Ups(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageLengthUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageLengthUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageLengthUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageLengthUps(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageWidthUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageWidthUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageWidthUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageWidthUps(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageHeightUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packageHeightUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackageHeightUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPackageHeightUps(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","additionalHandlingUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"additionalHandlingUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAdditionalHandlingUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","useCodUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useCodUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseCodUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codAmountUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"codAmountUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCodAmountUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCodAmountUps(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","codMethodUps").equals(reader.getName())){
                                
                                                object.setCodMethodUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsCodMethodUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deliveryConfUps").equals(reader.getName())){
                                
                                                object.setDeliveryConfUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsDeliveryConfUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    