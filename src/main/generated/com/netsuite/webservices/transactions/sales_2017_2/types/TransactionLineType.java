
/**
 * TransactionLineType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2.types;
            

            /**
            *  TransactionLineType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionLineType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.sales_2017_2.transactions.webservices.netsuite.com",
                "TransactionLineType",
                "ns20");

            

                        /**
                        * field for TransactionLineType
                        */

                        
                                    protected java.lang.String localTransactionLineType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected TransactionLineType(java.lang.String value, boolean isRegisterValue) {
                                    localTransactionLineType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localTransactionLineType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __adjustedQuantity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_adjustedQuantity");
                                
                                    public static final java.lang.String __advanceToApplyAmount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_advanceToApplyAmount");
                                
                                    public static final java.lang.String __billExchangeRateVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billExchangeRateVariance");
                                
                                    public static final java.lang.String __billPriceVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billPriceVariance");
                                
                                    public static final java.lang.String __billQuantityVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billQuantityVariance");
                                
                                    public static final java.lang.String __countQuantity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_countQuantity");
                                
                                    public static final java.lang.String __dropshipExpense =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dropshipExpense");
                                
                                    public static final java.lang.String __gainLoss =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gainLoss");
                                
                                    public static final java.lang.String __inTransit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inTransit");
                                
                                    public static final java.lang.String __item =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_item");
                                
                                    public static final java.lang.String __materialOverhead =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_materialOverhead");
                                
                                    public static final java.lang.String __nonReimbursuableExpenseOffset =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nonReimbursuableExpenseOffset");
                                
                                    public static final java.lang.String __nonReimbursuableExpenseOriginal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nonReimbursuableExpenseOriginal");
                                
                                    public static final java.lang.String __ownershipTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ownershipTransfer");
                                
                                    public static final java.lang.String __productionPriceVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_productionPriceVariance");
                                
                                    public static final java.lang.String __productionQuantityVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_productionQuantityVariance");
                                
                                    public static final java.lang.String __purchasePriceVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchasePriceVariance");
                                
                                    public static final java.lang.String __receiving =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_receiving");
                                
                                    public static final java.lang.String __routingItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_routingItem");
                                
                                    public static final java.lang.String __routingWorkInProcess =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_routingWorkInProcess");
                                
                                    public static final java.lang.String __scrap =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_scrap");
                                
                                    public static final java.lang.String __shipping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shipping");
                                
                                    public static final java.lang.String __snapshotQuantity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_snapshotQuantity");
                                
                                    public static final java.lang.String __totalBillVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_totalBillVariance");
                                
                                    public static final java.lang.String __unbuildVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unbuildVariance");
                                
                                    public static final java.lang.String __workInProcess =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workInProcess");
                                
                                    public static final java.lang.String __workInProcessVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workInProcessVariance");
                                
                                public static final TransactionLineType _adjustedQuantity =
                                    new TransactionLineType(__adjustedQuantity,true);
                            
                                public static final TransactionLineType _advanceToApplyAmount =
                                    new TransactionLineType(__advanceToApplyAmount,true);
                            
                                public static final TransactionLineType _billExchangeRateVariance =
                                    new TransactionLineType(__billExchangeRateVariance,true);
                            
                                public static final TransactionLineType _billPriceVariance =
                                    new TransactionLineType(__billPriceVariance,true);
                            
                                public static final TransactionLineType _billQuantityVariance =
                                    new TransactionLineType(__billQuantityVariance,true);
                            
                                public static final TransactionLineType _countQuantity =
                                    new TransactionLineType(__countQuantity,true);
                            
                                public static final TransactionLineType _dropshipExpense =
                                    new TransactionLineType(__dropshipExpense,true);
                            
                                public static final TransactionLineType _gainLoss =
                                    new TransactionLineType(__gainLoss,true);
                            
                                public static final TransactionLineType _inTransit =
                                    new TransactionLineType(__inTransit,true);
                            
                                public static final TransactionLineType _item =
                                    new TransactionLineType(__item,true);
                            
                                public static final TransactionLineType _materialOverhead =
                                    new TransactionLineType(__materialOverhead,true);
                            
                                public static final TransactionLineType _nonReimbursuableExpenseOffset =
                                    new TransactionLineType(__nonReimbursuableExpenseOffset,true);
                            
                                public static final TransactionLineType _nonReimbursuableExpenseOriginal =
                                    new TransactionLineType(__nonReimbursuableExpenseOriginal,true);
                            
                                public static final TransactionLineType _ownershipTransfer =
                                    new TransactionLineType(__ownershipTransfer,true);
                            
                                public static final TransactionLineType _productionPriceVariance =
                                    new TransactionLineType(__productionPriceVariance,true);
                            
                                public static final TransactionLineType _productionQuantityVariance =
                                    new TransactionLineType(__productionQuantityVariance,true);
                            
                                public static final TransactionLineType _purchasePriceVariance =
                                    new TransactionLineType(__purchasePriceVariance,true);
                            
                                public static final TransactionLineType _receiving =
                                    new TransactionLineType(__receiving,true);
                            
                                public static final TransactionLineType _routingItem =
                                    new TransactionLineType(__routingItem,true);
                            
                                public static final TransactionLineType _routingWorkInProcess =
                                    new TransactionLineType(__routingWorkInProcess,true);
                            
                                public static final TransactionLineType _scrap =
                                    new TransactionLineType(__scrap,true);
                            
                                public static final TransactionLineType _shipping =
                                    new TransactionLineType(__shipping,true);
                            
                                public static final TransactionLineType _snapshotQuantity =
                                    new TransactionLineType(__snapshotQuantity,true);
                            
                                public static final TransactionLineType _totalBillVariance =
                                    new TransactionLineType(__totalBillVariance,true);
                            
                                public static final TransactionLineType _unbuildVariance =
                                    new TransactionLineType(__unbuildVariance,true);
                            
                                public static final TransactionLineType _workInProcess =
                                    new TransactionLineType(__workInProcess,true);
                            
                                public static final TransactionLineType _workInProcessVariance =
                                    new TransactionLineType(__workInProcessVariance,true);
                            

                                public java.lang.String getValue() { return localTransactionLineType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localTransactionLineType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.sales_2017_2.transactions.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":TransactionLineType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "TransactionLineType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localTransactionLineType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("TransactionLineType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localTransactionLineType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns20";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionLineType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static TransactionLineType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    TransactionLineType enumeration = (TransactionLineType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static TransactionLineType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static TransactionLineType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return TransactionLineType.Factory.fromString(content,namespaceUri);
                    } else {
                       return TransactionLineType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionLineType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionLineType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TransactionLineType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = TransactionLineType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = TransactionLineType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    