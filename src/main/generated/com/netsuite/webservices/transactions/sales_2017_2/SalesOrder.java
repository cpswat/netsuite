
/**
 * SalesOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  SalesOrder bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SalesOrder extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = SalesOrder
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for DrAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDrAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDrAccountTracker = false ;

                           public boolean isDrAccountSpecified(){
                               return localDrAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDrAccount(){
                               return localDrAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DrAccount
                               */
                               public void setDrAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDrAccountTracker = param != null;
                                   
                                            this.localDrAccount=param;
                                    

                               }
                            

                        /**
                        * field for FxAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localFxAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxAccountTracker = false ;

                           public boolean isFxAccountSpecified(){
                               return localFxAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getFxAccount(){
                               return localFxAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxAccount
                               */
                               public void setFxAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localFxAccountTracker = param != null;
                                   
                                            this.localFxAccount=param;
                                    

                               }
                            

                        /**
                        * field for TranDate
                        */

                        
                                    protected java.util.Calendar localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTranDate(){
                               return localTranDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranDate
                               */
                               public void setTranDate(java.util.Calendar param){
                            localTranDateTracker = param != null;
                                   
                                            this.localTranDate=param;
                                    

                               }
                            

                        /**
                        * field for TranId
                        */

                        
                                    protected java.lang.String localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTranId(){
                               return localTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranId
                               */
                               public void setTranId(java.lang.String param){
                            localTranIdTracker = param != null;
                                   
                                            this.localTranId=param;
                                    

                               }
                            

                        /**
                        * field for EntityTaxRegNum
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntityTaxRegNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTaxRegNumTracker = false ;

                           public boolean isEntityTaxRegNumSpecified(){
                               return localEntityTaxRegNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntityTaxRegNum(){
                               return localEntityTaxRegNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityTaxRegNum
                               */
                               public void setEntityTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTaxRegNumTracker = param != null;
                                   
                                            this.localEntityTaxRegNum=param;
                                    

                               }
                            

                        /**
                        * field for Source
                        */

                        
                                    protected java.lang.String localSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceTracker = false ;

                           public boolean isSourceSpecified(){
                               return localSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSource(){
                               return localSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Source
                               */
                               public void setSource(java.lang.String param){
                            localSourceTracker = param != null;
                                   
                                            this.localSource=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromTracker = false ;

                           public boolean isCreatedFromSpecified(){
                               return localCreatedFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedFrom(){
                               return localCreatedFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFrom
                               */
                               public void setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedFromTracker = param != null;
                                   
                                            this.localCreatedFrom=param;
                                    

                               }
                            

                        /**
                        * field for OrderStatus
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderOrderStatus localOrderStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderStatusTracker = false ;

                           public boolean isOrderStatusSpecified(){
                               return localOrderStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderOrderStatus
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderOrderStatus getOrderStatus(){
                               return localOrderStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderStatus
                               */
                               public void setOrderStatus(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderOrderStatus param){
                            localOrderStatusTracker = param != null;
                                   
                                            this.localOrderStatus=param;
                                    

                               }
                            

                        /**
                        * field for NextBill
                        */

                        
                                    protected java.util.Calendar localNextBill ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextBillTracker = false ;

                           public boolean isNextBillSpecified(){
                               return localNextBillTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getNextBill(){
                               return localNextBill;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextBill
                               */
                               public void setNextBill(java.util.Calendar param){
                            localNextBillTracker = param != null;
                                   
                                            this.localNextBill=param;
                                    

                               }
                            

                        /**
                        * field for Opportunity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOpportunity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpportunityTracker = false ;

                           public boolean isOpportunitySpecified(){
                               return localOpportunityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOpportunity(){
                               return localOpportunity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Opportunity
                               */
                               public void setOpportunity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOpportunityTracker = param != null;
                                   
                                            this.localOpportunity=param;
                                    

                               }
                            

                        /**
                        * field for SalesRep
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepTracker = false ;

                           public boolean isSalesRepSpecified(){
                               return localSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesRep(){
                               return localSalesRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRep
                               */
                               public void setSalesRep(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesRepTracker = param != null;
                                   
                                            this.localSalesRep=param;
                                    

                               }
                            

                        /**
                        * field for ContribPct
                        */

                        
                                    protected java.lang.String localContribPct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContribPctTracker = false ;

                           public boolean isContribPctSpecified(){
                               return localContribPctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getContribPct(){
                               return localContribPct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContribPct
                               */
                               public void setContribPct(java.lang.String param){
                            localContribPctTracker = param != null;
                                   
                                            this.localContribPct=param;
                                    

                               }
                            

                        /**
                        * field for Partner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPartner(){
                               return localPartner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Partner
                               */
                               public void setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPartnerTracker = param != null;
                                   
                                            this.localPartner=param;
                                    

                               }
                            

                        /**
                        * field for SalesGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesGroupTracker = false ;

                           public boolean isSalesGroupSpecified(){
                               return localSalesGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesGroup(){
                               return localSalesGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesGroup
                               */
                               public void setSalesGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesGroupTracker = param != null;
                                   
                                            this.localSalesGroup=param;
                                    

                               }
                            

                        /**
                        * field for SyncSalesTeams
                        */

                        
                                    protected boolean localSyncSalesTeams ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSyncSalesTeamsTracker = false ;

                           public boolean isSyncSalesTeamsSpecified(){
                               return localSyncSalesTeamsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSyncSalesTeams(){
                               return localSyncSalesTeams;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SyncSalesTeams
                               */
                               public void setSyncSalesTeams(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSyncSalesTeamsTracker =
                                       true;
                                   
                                            this.localSyncSalesTeams=param;
                                    

                               }
                            

                        /**
                        * field for LeadSource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLeadSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceTracker = false ;

                           public boolean isLeadSourceSpecified(){
                               return localLeadSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLeadSource(){
                               return localLeadSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadSource
                               */
                               public void setLeadSource(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLeadSourceTracker = param != null;
                                   
                                            this.localLeadSource=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for OtherRefNum
                        */

                        
                                    protected java.lang.String localOtherRefNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOtherRefNumTracker = false ;

                           public boolean isOtherRefNumSpecified(){
                               return localOtherRefNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOtherRefNum(){
                               return localOtherRefNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OtherRefNum
                               */
                               public void setOtherRefNum(java.lang.String param){
                            localOtherRefNumTracker = param != null;
                                   
                                            this.localOtherRefNum=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for SalesEffectiveDate
                        */

                        
                                    protected java.util.Calendar localSalesEffectiveDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesEffectiveDateTracker = false ;

                           public boolean isSalesEffectiveDateSpecified(){
                               return localSalesEffectiveDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getSalesEffectiveDate(){
                               return localSalesEffectiveDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesEffectiveDate
                               */
                               public void setSalesEffectiveDate(java.util.Calendar param){
                            localSalesEffectiveDateTracker = param != null;
                                   
                                            this.localSalesEffectiveDate=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeCommission
                        */

                        
                                    protected boolean localExcludeCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeCommissionTracker = false ;

                           public boolean isExcludeCommissionSpecified(){
                               return localExcludeCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExcludeCommission(){
                               return localExcludeCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeCommission
                               */
                               public void setExcludeCommission(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExcludeCommissionTracker =
                                       true;
                                   
                                            this.localExcludeCommission=param;
                                    

                               }
                            

                        /**
                        * field for TotalCostEstimate
                        */

                        
                                    protected double localTotalCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalCostEstimateTracker = false ;

                           public boolean isTotalCostEstimateSpecified(){
                               return localTotalCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTotalCostEstimate(){
                               return localTotalCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TotalCostEstimate
                               */
                               public void setTotalCostEstimate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTotalCostEstimateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTotalCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for EstGrossProfit
                        */

                        
                                    protected double localEstGrossProfit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstGrossProfitTracker = false ;

                           public boolean isEstGrossProfitSpecified(){
                               return localEstGrossProfitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstGrossProfit(){
                               return localEstGrossProfit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstGrossProfit
                               */
                               public void setEstGrossProfit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstGrossProfitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstGrossProfit=param;
                                    

                               }
                            

                        /**
                        * field for EstGrossProfitPercent
                        */

                        
                                    protected double localEstGrossProfitPercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstGrossProfitPercentTracker = false ;

                           public boolean isEstGrossProfitPercentSpecified(){
                               return localEstGrossProfitPercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstGrossProfitPercent(){
                               return localEstGrossProfitPercent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstGrossProfitPercent
                               */
                               public void setEstGrossProfitPercent(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstGrossProfitPercentTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstGrossProfitPercent=param;
                                    

                               }
                            

                        /**
                        * field for ExchangeRate
                        */

                        
                                    protected double localExchangeRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExchangeRateTracker = false ;

                           public boolean isExchangeRateSpecified(){
                               return localExchangeRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getExchangeRate(){
                               return localExchangeRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExchangeRate
                               */
                               public void setExchangeRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localExchangeRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localExchangeRate=param;
                                    

                               }
                            

                        /**
                        * field for PromoCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPromoCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPromoCodeTracker = false ;

                           public boolean isPromoCodeSpecified(){
                               return localPromoCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPromoCode(){
                               return localPromoCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PromoCode
                               */
                               public void setPromoCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPromoCodeTracker = param != null;
                                   
                                            this.localPromoCode=param;
                                    

                               }
                            

                        /**
                        * field for CurrencyName
                        */

                        
                                    protected java.lang.String localCurrencyName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyNameTracker = false ;

                           public boolean isCurrencyNameSpecified(){
                               return localCurrencyNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCurrencyName(){
                               return localCurrencyName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CurrencyName
                               */
                               public void setCurrencyName(java.lang.String param){
                            localCurrencyNameTracker = param != null;
                                   
                                            this.localCurrencyName=param;
                                    

                               }
                            

                        /**
                        * field for DiscountItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDiscountItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDiscountItemTracker = false ;

                           public boolean isDiscountItemSpecified(){
                               return localDiscountItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDiscountItem(){
                               return localDiscountItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DiscountItem
                               */
                               public void setDiscountItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDiscountItemTracker = param != null;
                                   
                                            this.localDiscountItem=param;
                                    

                               }
                            

                        /**
                        * field for DiscountRate
                        */

                        
                                    protected java.lang.String localDiscountRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDiscountRateTracker = false ;

                           public boolean isDiscountRateSpecified(){
                               return localDiscountRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDiscountRate(){
                               return localDiscountRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DiscountRate
                               */
                               public void setDiscountRate(java.lang.String param){
                            localDiscountRateTracker = param != null;
                                   
                                            this.localDiscountRate=param;
                                    

                               }
                            

                        /**
                        * field for IsTaxable
                        */

                        
                                    protected boolean localIsTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTaxableTracker = false ;

                           public boolean isIsTaxableSpecified(){
                               return localIsTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsTaxable(){
                               return localIsTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTaxable
                               */
                               public void setIsTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsTaxableTracker =
                                       true;
                                   
                                            this.localIsTaxable=param;
                                    

                               }
                            

                        /**
                        * field for TaxItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxItemTracker = false ;

                           public boolean isTaxItemSpecified(){
                               return localTaxItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxItem(){
                               return localTaxItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxItem
                               */
                               public void setTaxItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxItemTracker = param != null;
                                   
                                            this.localTaxItem=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate
                        */

                        
                                    protected double localTaxRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRateTracker = false ;

                           public boolean isTaxRateSpecified(){
                               return localTaxRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate(){
                               return localTaxRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate
                               */
                               public void setTaxRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate=param;
                                    

                               }
                            

                        /**
                        * field for ToBePrinted
                        */

                        
                                    protected boolean localToBePrinted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToBePrintedTracker = false ;

                           public boolean isToBePrintedSpecified(){
                               return localToBePrintedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getToBePrinted(){
                               return localToBePrinted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToBePrinted
                               */
                               public void setToBePrinted(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localToBePrintedTracker =
                                       true;
                                   
                                            this.localToBePrinted=param;
                                    

                               }
                            

                        /**
                        * field for ToBeEmailed
                        */

                        
                                    protected boolean localToBeEmailed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToBeEmailedTracker = false ;

                           public boolean isToBeEmailedSpecified(){
                               return localToBeEmailedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getToBeEmailed(){
                               return localToBeEmailed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToBeEmailed
                               */
                               public void setToBeEmailed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localToBeEmailedTracker =
                                       true;
                                   
                                            this.localToBeEmailed=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected java.lang.String localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(java.lang.String param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for ToBeFaxed
                        */

                        
                                    protected boolean localToBeFaxed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToBeFaxedTracker = false ;

                           public boolean isToBeFaxedSpecified(){
                               return localToBeFaxedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getToBeFaxed(){
                               return localToBeFaxed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToBeFaxed
                               */
                               public void setToBeFaxed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localToBeFaxedTracker =
                                       true;
                                   
                                            this.localToBeFaxed=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected java.lang.String localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(java.lang.String param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for MessageSel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localMessageSel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageSelTracker = false ;

                           public boolean isMessageSelSpecified(){
                               return localMessageSelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getMessageSel(){
                               return localMessageSel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessageSel
                               */
                               public void setMessageSel(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localMessageSelTracker = param != null;
                                   
                                            this.localMessageSel=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected java.lang.String localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(java.lang.String param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for BillingAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.Address localBillingAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingAddressTracker = false ;

                           public boolean isBillingAddressSpecified(){
                               return localBillingAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.Address
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.Address getBillingAddress(){
                               return localBillingAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingAddress
                               */
                               public void setBillingAddress(com.netsuite.webservices.platform.common_2017_2.Address param){
                            localBillingAddressTracker = param != null;
                                   
                                            this.localBillingAddress=param;
                                    

                               }
                            

                        /**
                        * field for BillAddressList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillAddressList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillAddressListTracker = false ;

                           public boolean isBillAddressListSpecified(){
                               return localBillAddressListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillAddressList(){
                               return localBillAddressList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillAddressList
                               */
                               public void setBillAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillAddressListTracker = param != null;
                                   
                                            this.localBillAddressList=param;
                                    

                               }
                            

                        /**
                        * field for ShippingAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.Address localShippingAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingAddressTracker = false ;

                           public boolean isShippingAddressSpecified(){
                               return localShippingAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.Address
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.Address getShippingAddress(){
                               return localShippingAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingAddress
                               */
                               public void setShippingAddress(com.netsuite.webservices.platform.common_2017_2.Address param){
                            localShippingAddressTracker = param != null;
                                   
                                            this.localShippingAddress=param;
                                    

                               }
                            

                        /**
                        * field for ShipIsResidential
                        */

                        
                                    protected boolean localShipIsResidential ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipIsResidentialTracker = false ;

                           public boolean isShipIsResidentialSpecified(){
                               return localShipIsResidentialTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShipIsResidential(){
                               return localShipIsResidential;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipIsResidential
                               */
                               public void setShipIsResidential(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipIsResidentialTracker =
                                       true;
                                   
                                            this.localShipIsResidential=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddressList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipAddressList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressListTracker = false ;

                           public boolean isShipAddressListSpecified(){
                               return localShipAddressListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipAddressList(){
                               return localShipAddressList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddressList
                               */
                               public void setShipAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipAddressListTracker = param != null;
                                   
                                            this.localShipAddressList=param;
                                    

                               }
                            

                        /**
                        * field for Fob
                        */

                        
                                    protected java.lang.String localFob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFobTracker = false ;

                           public boolean isFobSpecified(){
                               return localFobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFob(){
                               return localFob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fob
                               */
                               public void setFob(java.lang.String param){
                            localFobTracker = param != null;
                                   
                                            this.localFob=param;
                                    

                               }
                            

                        /**
                        * field for ShipDate
                        */

                        
                                    protected java.util.Calendar localShipDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipDateTracker = false ;

                           public boolean isShipDateSpecified(){
                               return localShipDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getShipDate(){
                               return localShipDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipDate
                               */
                               public void setShipDate(java.util.Calendar param){
                            localShipDateTracker = param != null;
                                   
                                            this.localShipDate=param;
                                    

                               }
                            

                        /**
                        * field for ActualShipDate
                        */

                        
                                    protected java.util.Calendar localActualShipDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualShipDateTracker = false ;

                           public boolean isActualShipDateSpecified(){
                               return localActualShipDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getActualShipDate(){
                               return localActualShipDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualShipDate
                               */
                               public void setActualShipDate(java.util.Calendar param){
                            localActualShipDateTracker = param != null;
                                   
                                            this.localActualShipDate=param;
                                    

                               }
                            

                        /**
                        * field for ShipMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipMethodTracker = false ;

                           public boolean isShipMethodSpecified(){
                               return localShipMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipMethod(){
                               return localShipMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipMethod
                               */
                               public void setShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipMethodTracker = param != null;
                                   
                                            this.localShipMethod=param;
                                    

                               }
                            

                        /**
                        * field for ShippingCost
                        */

                        
                                    protected double localShippingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingCostTracker = false ;

                           public boolean isShippingCostSpecified(){
                               return localShippingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingCost(){
                               return localShippingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingCost
                               */
                               public void setShippingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingCost=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTax1Rate
                        */

                        
                                    protected double localShippingTax1Rate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTax1RateTracker = false ;

                           public boolean isShippingTax1RateSpecified(){
                               return localShippingTax1RateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingTax1Rate(){
                               return localShippingTax1Rate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTax1Rate
                               */
                               public void setShippingTax1Rate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingTax1RateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingTax1Rate=param;
                                    

                               }
                            

                        /**
                        * field for IsMultiShipTo
                        */

                        
                                    protected boolean localIsMultiShipTo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsMultiShipToTracker = false ;

                           public boolean isIsMultiShipToSpecified(){
                               return localIsMultiShipToTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsMultiShipTo(){
                               return localIsMultiShipTo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsMultiShipTo
                               */
                               public void setIsMultiShipTo(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsMultiShipToTracker =
                                       true;
                                   
                                            this.localIsMultiShipTo=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTax2Rate
                        */

                        
                                    protected java.lang.String localShippingTax2Rate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTax2RateTracker = false ;

                           public boolean isShippingTax2RateSpecified(){
                               return localShippingTax2RateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShippingTax2Rate(){
                               return localShippingTax2Rate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTax2Rate
                               */
                               public void setShippingTax2Rate(java.lang.String param){
                            localShippingTax2RateTracker = param != null;
                                   
                                            this.localShippingTax2Rate=param;
                                    

                               }
                            

                        /**
                        * field for ShippingTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShippingTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingTaxCodeTracker = false ;

                           public boolean isShippingTaxCodeSpecified(){
                               return localShippingTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShippingTaxCode(){
                               return localShippingTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingTaxCode
                               */
                               public void setShippingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShippingTaxCodeTracker = param != null;
                                   
                                            this.localShippingTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localHandlingTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTaxCodeTracker = false ;

                           public boolean isHandlingTaxCodeSpecified(){
                               return localHandlingTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getHandlingTaxCode(){
                               return localHandlingTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTaxCode
                               */
                               public void setHandlingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localHandlingTaxCodeTracker = param != null;
                                   
                                            this.localHandlingTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTax1Rate
                        */

                        
                                    protected double localHandlingTax1Rate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTax1RateTracker = false ;

                           public boolean isHandlingTax1RateSpecified(){
                               return localHandlingTax1RateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingTax1Rate(){
                               return localHandlingTax1Rate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTax1Rate
                               */
                               public void setHandlingTax1Rate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingTax1RateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingTax1Rate=param;
                                    

                               }
                            

                        /**
                        * field for HandlingTax2Rate
                        */

                        
                                    protected java.lang.String localHandlingTax2Rate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingTax2RateTracker = false ;

                           public boolean isHandlingTax2RateSpecified(){
                               return localHandlingTax2RateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHandlingTax2Rate(){
                               return localHandlingTax2Rate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingTax2Rate
                               */
                               public void setHandlingTax2Rate(java.lang.String param){
                            localHandlingTax2RateTracker = param != null;
                                   
                                            this.localHandlingTax2Rate=param;
                                    

                               }
                            

                        /**
                        * field for HandlingCost
                        */

                        
                                    protected double localHandlingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingCostTracker = false ;

                           public boolean isHandlingCostSpecified(){
                               return localHandlingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingCost(){
                               return localHandlingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingCost
                               */
                               public void setHandlingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingCost=param;
                                    

                               }
                            

                        /**
                        * field for TrackingNumbers
                        */

                        
                                    protected java.lang.String localTrackingNumbers ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTrackingNumbersTracker = false ;

                           public boolean isTrackingNumbersSpecified(){
                               return localTrackingNumbersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTrackingNumbers(){
                               return localTrackingNumbers;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TrackingNumbers
                               */
                               public void setTrackingNumbers(java.lang.String param){
                            localTrackingNumbersTracker = param != null;
                                   
                                            this.localTrackingNumbers=param;
                                    

                               }
                            

                        /**
                        * field for LinkedTrackingNumbers
                        */

                        
                                    protected java.lang.String localLinkedTrackingNumbers ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLinkedTrackingNumbersTracker = false ;

                           public boolean isLinkedTrackingNumbersSpecified(){
                               return localLinkedTrackingNumbersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLinkedTrackingNumbers(){
                               return localLinkedTrackingNumbers;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LinkedTrackingNumbers
                               */
                               public void setLinkedTrackingNumbers(java.lang.String param){
                            localLinkedTrackingNumbersTracker = param != null;
                                   
                                            this.localLinkedTrackingNumbers=param;
                                    

                               }
                            

                        /**
                        * field for ShipComplete
                        */

                        
                                    protected boolean localShipComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipCompleteTracker = false ;

                           public boolean isShipCompleteSpecified(){
                               return localShipCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShipComplete(){
                               return localShipComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipComplete
                               */
                               public void setShipComplete(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipCompleteTracker =
                                       true;
                                   
                                            this.localShipComplete=param;
                                    

                               }
                            

                        /**
                        * field for PaymentMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPaymentMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentMethodTracker = false ;

                           public boolean isPaymentMethodSpecified(){
                               return localPaymentMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPaymentMethod(){
                               return localPaymentMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentMethod
                               */
                               public void setPaymentMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPaymentMethodTracker = param != null;
                                   
                                            this.localPaymentMethod=param;
                                    

                               }
                            

                        /**
                        * field for ShopperIpAddress
                        */

                        
                                    protected java.lang.String localShopperIpAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShopperIpAddressTracker = false ;

                           public boolean isShopperIpAddressSpecified(){
                               return localShopperIpAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShopperIpAddress(){
                               return localShopperIpAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShopperIpAddress
                               */
                               public void setShopperIpAddress(java.lang.String param){
                            localShopperIpAddressTracker = param != null;
                                   
                                            this.localShopperIpAddress=param;
                                    

                               }
                            

                        /**
                        * field for SaveOnAuthDecline
                        */

                        
                                    protected boolean localSaveOnAuthDecline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaveOnAuthDeclineTracker = false ;

                           public boolean isSaveOnAuthDeclineSpecified(){
                               return localSaveOnAuthDeclineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSaveOnAuthDecline(){
                               return localSaveOnAuthDecline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SaveOnAuthDecline
                               */
                               public void setSaveOnAuthDecline(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSaveOnAuthDeclineTracker =
                                       true;
                                   
                                            this.localSaveOnAuthDecline=param;
                                    

                               }
                            

                        /**
                        * field for CanHaveStackable
                        */

                        
                                    protected boolean localCanHaveStackable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCanHaveStackableTracker = false ;

                           public boolean isCanHaveStackableSpecified(){
                               return localCanHaveStackableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCanHaveStackable(){
                               return localCanHaveStackable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CanHaveStackable
                               */
                               public void setCanHaveStackable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localCanHaveStackableTracker =
                                       true;
                                   
                                            this.localCanHaveStackable=param;
                                    

                               }
                            

                        /**
                        * field for CreditCard
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreditCard ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditCardTracker = false ;

                           public boolean isCreditCardSpecified(){
                               return localCreditCardTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreditCard(){
                               return localCreditCard;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditCard
                               */
                               public void setCreditCard(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreditCardTracker = param != null;
                                   
                                            this.localCreditCard=param;
                                    

                               }
                            

                        /**
                        * field for RevenueStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus localRevenueStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueStatusTracker = false ;

                           public boolean isRevenueStatusSpecified(){
                               return localRevenueStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus getRevenueStatus(){
                               return localRevenueStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueStatus
                               */
                               public void setRevenueStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus param){
                            localRevenueStatusTracker = param != null;
                                   
                                            this.localRevenueStatus=param;
                                    

                               }
                            

                        /**
                        * field for RecognizedRevenue
                        */

                        
                                    protected double localRecognizedRevenue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecognizedRevenueTracker = false ;

                           public boolean isRecognizedRevenueSpecified(){
                               return localRecognizedRevenueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRecognizedRevenue(){
                               return localRecognizedRevenue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecognizedRevenue
                               */
                               public void setRecognizedRevenue(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRecognizedRevenueTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRecognizedRevenue=param;
                                    

                               }
                            

                        /**
                        * field for DeferredRevenue
                        */

                        
                                    protected double localDeferredRevenue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferredRevenueTracker = false ;

                           public boolean isDeferredRevenueSpecified(){
                               return localDeferredRevenueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDeferredRevenue(){
                               return localDeferredRevenue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferredRevenue
                               */
                               public void setDeferredRevenue(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDeferredRevenueTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDeferredRevenue=param;
                                    

                               }
                            

                        /**
                        * field for RevRecOnRevCommitment
                        */

                        
                                    protected boolean localRevRecOnRevCommitment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecOnRevCommitmentTracker = false ;

                           public boolean isRevRecOnRevCommitmentSpecified(){
                               return localRevRecOnRevCommitmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRevRecOnRevCommitment(){
                               return localRevRecOnRevCommitment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecOnRevCommitment
                               */
                               public void setRevRecOnRevCommitment(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRevRecOnRevCommitmentTracker =
                                       true;
                                   
                                            this.localRevRecOnRevCommitment=param;
                                    

                               }
                            

                        /**
                        * field for RevCommitStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus localRevCommitStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevCommitStatusTracker = false ;

                           public boolean isRevCommitStatusSpecified(){
                               return localRevCommitStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus getRevCommitStatus(){
                               return localRevCommitStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevCommitStatus
                               */
                               public void setRevCommitStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus param){
                            localRevCommitStatusTracker = param != null;
                                   
                                            this.localRevCommitStatus=param;
                                    

                               }
                            

                        /**
                        * field for CcNumber
                        */

                        
                                    protected java.lang.String localCcNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcNumberTracker = false ;

                           public boolean isCcNumberSpecified(){
                               return localCcNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcNumber(){
                               return localCcNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcNumber
                               */
                               public void setCcNumber(java.lang.String param){
                            localCcNumberTracker = param != null;
                                   
                                            this.localCcNumber=param;
                                    

                               }
                            

                        /**
                        * field for CcExpireDate
                        */

                        
                                    protected java.util.Calendar localCcExpireDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcExpireDateTracker = false ;

                           public boolean isCcExpireDateSpecified(){
                               return localCcExpireDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCcExpireDate(){
                               return localCcExpireDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcExpireDate
                               */
                               public void setCcExpireDate(java.util.Calendar param){
                            localCcExpireDateTracker = param != null;
                                   
                                            this.localCcExpireDate=param;
                                    

                               }
                            

                        /**
                        * field for CcName
                        */

                        
                                    protected java.lang.String localCcName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcNameTracker = false ;

                           public boolean isCcNameSpecified(){
                               return localCcNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcName(){
                               return localCcName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcName
                               */
                               public void setCcName(java.lang.String param){
                            localCcNameTracker = param != null;
                                   
                                            this.localCcName=param;
                                    

                               }
                            

                        /**
                        * field for CcStreet
                        */

                        
                                    protected java.lang.String localCcStreet ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcStreetTracker = false ;

                           public boolean isCcStreetSpecified(){
                               return localCcStreetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcStreet(){
                               return localCcStreet;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcStreet
                               */
                               public void setCcStreet(java.lang.String param){
                            localCcStreetTracker = param != null;
                                   
                                            this.localCcStreet=param;
                                    

                               }
                            

                        /**
                        * field for CcZipCode
                        */

                        
                                    protected java.lang.String localCcZipCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcZipCodeTracker = false ;

                           public boolean isCcZipCodeSpecified(){
                               return localCcZipCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcZipCode(){
                               return localCcZipCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcZipCode
                               */
                               public void setCcZipCode(java.lang.String param){
                            localCcZipCodeTracker = param != null;
                                   
                                            this.localCcZipCode=param;
                                    

                               }
                            

                        /**
                        * field for PayPalStatus
                        */

                        
                                    protected java.lang.String localPayPalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayPalStatusTracker = false ;

                           public boolean isPayPalStatusSpecified(){
                               return localPayPalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPayPalStatus(){
                               return localPayPalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayPalStatus
                               */
                               public void setPayPalStatus(java.lang.String param){
                            localPayPalStatusTracker = param != null;
                                   
                                            this.localPayPalStatus=param;
                                    

                               }
                            

                        /**
                        * field for CreditCardProcessor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreditCardProcessor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditCardProcessorTracker = false ;

                           public boolean isCreditCardProcessorSpecified(){
                               return localCreditCardProcessorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreditCardProcessor(){
                               return localCreditCardProcessor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditCardProcessor
                               */
                               public void setCreditCardProcessor(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreditCardProcessorTracker = param != null;
                                   
                                            this.localCreditCardProcessor=param;
                                    

                               }
                            

                        /**
                        * field for PayPalTranId
                        */

                        
                                    protected java.lang.String localPayPalTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayPalTranIdTracker = false ;

                           public boolean isPayPalTranIdSpecified(){
                               return localPayPalTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPayPalTranId(){
                               return localPayPalTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayPalTranId
                               */
                               public void setPayPalTranId(java.lang.String param){
                            localPayPalTranIdTracker = param != null;
                                   
                                            this.localPayPalTranId=param;
                                    

                               }
                            

                        /**
                        * field for CcApproved
                        */

                        
                                    protected boolean localCcApproved ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcApprovedTracker = false ;

                           public boolean isCcApprovedSpecified(){
                               return localCcApprovedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCcApproved(){
                               return localCcApproved;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcApproved
                               */
                               public void setCcApproved(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localCcApprovedTracker =
                                       true;
                                   
                                            this.localCcApproved=param;
                                    

                               }
                            

                        /**
                        * field for GetAuth
                        */

                        
                                    protected boolean localGetAuth ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGetAuthTracker = false ;

                           public boolean isGetAuthSpecified(){
                               return localGetAuthTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getGetAuth(){
                               return localGetAuth;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GetAuth
                               */
                               public void setGetAuth(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localGetAuthTracker =
                                       true;
                                   
                                            this.localGetAuth=param;
                                    

                               }
                            

                        /**
                        * field for AuthCode
                        */

                        
                                    protected java.lang.String localAuthCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAuthCodeTracker = false ;

                           public boolean isAuthCodeSpecified(){
                               return localAuthCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAuthCode(){
                               return localAuthCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AuthCode
                               */
                               public void setAuthCode(java.lang.String param){
                            localAuthCodeTracker = param != null;
                                   
                                            this.localAuthCode=param;
                                    

                               }
                            

                        /**
                        * field for CcAvsStreetMatch
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode localCcAvsStreetMatch ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcAvsStreetMatchTracker = false ;

                           public boolean isCcAvsStreetMatchSpecified(){
                               return localCcAvsStreetMatchTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode getCcAvsStreetMatch(){
                               return localCcAvsStreetMatch;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcAvsStreetMatch
                               */
                               public void setCcAvsStreetMatch(com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode param){
                            localCcAvsStreetMatchTracker = param != null;
                                   
                                            this.localCcAvsStreetMatch=param;
                                    

                               }
                            

                        /**
                        * field for CcAvsZipMatch
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode localCcAvsZipMatch ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcAvsZipMatchTracker = false ;

                           public boolean isCcAvsZipMatchSpecified(){
                               return localCcAvsZipMatchTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode getCcAvsZipMatch(){
                               return localCcAvsZipMatch;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcAvsZipMatch
                               */
                               public void setCcAvsZipMatch(com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode param){
                            localCcAvsZipMatchTracker = param != null;
                                   
                                            this.localCcAvsZipMatch=param;
                                    

                               }
                            

                        /**
                        * field for IsRecurringPayment
                        */

                        
                                    protected boolean localIsRecurringPayment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsRecurringPaymentTracker = false ;

                           public boolean isIsRecurringPaymentSpecified(){
                               return localIsRecurringPaymentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsRecurringPayment(){
                               return localIsRecurringPayment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsRecurringPayment
                               */
                               public void setIsRecurringPayment(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsRecurringPaymentTracker =
                                       true;
                                   
                                            this.localIsRecurringPayment=param;
                                    

                               }
                            

                        /**
                        * field for CcSecurityCodeMatch
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode localCcSecurityCodeMatch ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcSecurityCodeMatchTracker = false ;

                           public boolean isCcSecurityCodeMatchSpecified(){
                               return localCcSecurityCodeMatchTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode getCcSecurityCodeMatch(){
                               return localCcSecurityCodeMatch;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcSecurityCodeMatch
                               */
                               public void setCcSecurityCodeMatch(com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode param){
                            localCcSecurityCodeMatchTracker = param != null;
                                   
                                            this.localCcSecurityCodeMatch=param;
                                    

                               }
                            

                        /**
                        * field for AltSalesTotal
                        */

                        
                                    protected double localAltSalesTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltSalesTotalTracker = false ;

                           public boolean isAltSalesTotalSpecified(){
                               return localAltSalesTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAltSalesTotal(){
                               return localAltSalesTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltSalesTotal
                               */
                               public void setAltSalesTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAltSalesTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAltSalesTotal=param;
                                    

                               }
                            

                        /**
                        * field for IgnoreAvs
                        */

                        
                                    protected boolean localIgnoreAvs ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIgnoreAvsTracker = false ;

                           public boolean isIgnoreAvsSpecified(){
                               return localIgnoreAvsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIgnoreAvs(){
                               return localIgnoreAvs;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IgnoreAvs
                               */
                               public void setIgnoreAvs(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIgnoreAvsTracker =
                                       true;
                                   
                                            this.localIgnoreAvs=param;
                                    

                               }
                            

                        /**
                        * field for PaymentEventResult
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventResult localPaymentEventResult ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentEventResultTracker = false ;

                           public boolean isPaymentEventResultSpecified(){
                               return localPaymentEventResultTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventResult
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventResult getPaymentEventResult(){
                               return localPaymentEventResult;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentEventResult
                               */
                               public void setPaymentEventResult(com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventResult param){
                            localPaymentEventResultTracker = param != null;
                                   
                                            this.localPaymentEventResult=param;
                                    

                               }
                            

                        /**
                        * field for PaymentEventHoldReason
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventHoldReason localPaymentEventHoldReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentEventHoldReasonTracker = false ;

                           public boolean isPaymentEventHoldReasonSpecified(){
                               return localPaymentEventHoldReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventHoldReason
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventHoldReason getPaymentEventHoldReason(){
                               return localPaymentEventHoldReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentEventHoldReason
                               */
                               public void setPaymentEventHoldReason(com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventHoldReason param){
                            localPaymentEventHoldReasonTracker = param != null;
                                   
                                            this.localPaymentEventHoldReason=param;
                                    

                               }
                            

                        /**
                        * field for PaymentEventType
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventType localPaymentEventType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentEventTypeTracker = false ;

                           public boolean isPaymentEventTypeSpecified(){
                               return localPaymentEventTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventType
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventType getPaymentEventType(){
                               return localPaymentEventType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentEventType
                               */
                               public void setPaymentEventType(com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventType param){
                            localPaymentEventTypeTracker = param != null;
                                   
                                            this.localPaymentEventType=param;
                                    

                               }
                            

                        /**
                        * field for PaymentEventDate
                        */

                        
                                    protected java.util.Calendar localPaymentEventDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentEventDateTracker = false ;

                           public boolean isPaymentEventDateSpecified(){
                               return localPaymentEventDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getPaymentEventDate(){
                               return localPaymentEventDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentEventDate
                               */
                               public void setPaymentEventDate(java.util.Calendar param){
                            localPaymentEventDateTracker = param != null;
                                   
                                            this.localPaymentEventDate=param;
                                    

                               }
                            

                        /**
                        * field for PaymentEventUpdatedBy
                        */

                        
                                    protected java.lang.String localPaymentEventUpdatedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaymentEventUpdatedByTracker = false ;

                           public boolean isPaymentEventUpdatedBySpecified(){
                               return localPaymentEventUpdatedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPaymentEventUpdatedBy(){
                               return localPaymentEventUpdatedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaymentEventUpdatedBy
                               */
                               public void setPaymentEventUpdatedBy(java.lang.String param){
                            localPaymentEventUpdatedByTracker = param != null;
                                   
                                            this.localPaymentEventUpdatedBy=param;
                                    

                               }
                            

                        /**
                        * field for SubTotal
                        */

                        
                                    protected double localSubTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubTotalTracker = false ;

                           public boolean isSubTotalSpecified(){
                               return localSubTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSubTotal(){
                               return localSubTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubTotal
                               */
                               public void setSubTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSubTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSubTotal=param;
                                    

                               }
                            

                        /**
                        * field for DiscountTotal
                        */

                        
                                    protected double localDiscountTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDiscountTotalTracker = false ;

                           public boolean isDiscountTotalSpecified(){
                               return localDiscountTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDiscountTotal(){
                               return localDiscountTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DiscountTotal
                               */
                               public void setDiscountTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDiscountTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDiscountTotal=param;
                                    

                               }
                            

                        /**
                        * field for TaxTotal
                        */

                        
                                    protected double localTaxTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxTotalTracker = false ;

                           public boolean isTaxTotalSpecified(){
                               return localTaxTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxTotal(){
                               return localTaxTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxTotal
                               */
                               public void setTaxTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxTotal=param;
                                    

                               }
                            

                        /**
                        * field for AltShippingCost
                        */

                        
                                    protected double localAltShippingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltShippingCostTracker = false ;

                           public boolean isAltShippingCostSpecified(){
                               return localAltShippingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAltShippingCost(){
                               return localAltShippingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltShippingCost
                               */
                               public void setAltShippingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAltShippingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAltShippingCost=param;
                                    

                               }
                            

                        /**
                        * field for AltHandlingCost
                        */

                        
                                    protected double localAltHandlingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltHandlingCostTracker = false ;

                           public boolean isAltHandlingCostSpecified(){
                               return localAltHandlingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAltHandlingCost(){
                               return localAltHandlingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltHandlingCost
                               */
                               public void setAltHandlingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAltHandlingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAltHandlingCost=param;
                                    

                               }
                            

                        /**
                        * field for Total
                        */

                        
                                    protected double localTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalTracker = false ;

                           public boolean isTotalSpecified(){
                               return localTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTotal(){
                               return localTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Total
                               */
                               public void setTotal(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTotal=param;
                                    

                               }
                            

                        /**
                        * field for RevRecSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleTracker = false ;

                           public boolean isRevRecScheduleSpecified(){
                               return localRevRecScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecSchedule(){
                               return localRevRecSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecSchedule
                               */
                               public void setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecScheduleTracker = param != null;
                                   
                                            this.localRevRecSchedule=param;
                                    

                               }
                            

                        /**
                        * field for RevRecStartDate
                        */

                        
                                    protected java.util.Calendar localRevRecStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecStartDateTracker = false ;

                           public boolean isRevRecStartDateSpecified(){
                               return localRevRecStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecStartDate(){
                               return localRevRecStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecStartDate
                               */
                               public void setRevRecStartDate(java.util.Calendar param){
                            localRevRecStartDateTracker = param != null;
                                   
                                            this.localRevRecStartDate=param;
                                    

                               }
                            

                        /**
                        * field for RevRecEndDate
                        */

                        
                                    protected java.util.Calendar localRevRecEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecEndDateTracker = false ;

                           public boolean isRevRecEndDateSpecified(){
                               return localRevRecEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecEndDate(){
                               return localRevRecEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecEndDate
                               */
                               public void setRevRecEndDate(java.util.Calendar param){
                            localRevRecEndDateTracker = param != null;
                                   
                                            this.localRevRecEndDate=param;
                                    

                               }
                            

                        /**
                        * field for PaypalAuthId
                        */

                        
                                    protected java.lang.String localPaypalAuthId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaypalAuthIdTracker = false ;

                           public boolean isPaypalAuthIdSpecified(){
                               return localPaypalAuthIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPaypalAuthId(){
                               return localPaypalAuthId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaypalAuthId
                               */
                               public void setPaypalAuthId(java.lang.String param){
                            localPaypalAuthIdTracker = param != null;
                                   
                                            this.localPaypalAuthId=param;
                                    

                               }
                            

                        /**
                        * field for Balance
                        */

                        
                                    protected double localBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBalanceTracker = false ;

                           public boolean isBalanceSpecified(){
                               return localBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getBalance(){
                               return localBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Balance
                               */
                               public void setBalance(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localBalanceTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localBalance=param;
                                    

                               }
                            

                        /**
                        * field for PaypalProcess
                        */

                        
                                    protected boolean localPaypalProcess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaypalProcessTracker = false ;

                           public boolean isPaypalProcessSpecified(){
                               return localPaypalProcessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPaypalProcess(){
                               return localPaypalProcess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaypalProcess
                               */
                               public void setPaypalProcess(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPaypalProcessTracker =
                                       true;
                                   
                                            this.localPaypalProcess=param;
                                    

                               }
                            

                        /**
                        * field for BillingSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingScheduleTracker = false ;

                           public boolean isBillingScheduleSpecified(){
                               return localBillingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingSchedule(){
                               return localBillingSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingSchedule
                               */
                               public void setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingScheduleTracker = param != null;
                                   
                                            this.localBillingSchedule=param;
                                    

                               }
                            

                        /**
                        * field for CcSecurityCode
                        */

                        
                                    protected java.lang.String localCcSecurityCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcSecurityCodeTracker = false ;

                           public boolean isCcSecurityCodeSpecified(){
                               return localCcSecurityCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCcSecurityCode(){
                               return localCcSecurityCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcSecurityCode
                               */
                               public void setCcSecurityCode(java.lang.String param){
                            localCcSecurityCodeTracker = param != null;
                                   
                                            this.localCcSecurityCode=param;
                                    

                               }
                            

                        /**
                        * field for ThreeDStatusCode
                        */

                        
                                    protected java.lang.String localThreeDStatusCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThreeDStatusCodeTracker = false ;

                           public boolean isThreeDStatusCodeSpecified(){
                               return localThreeDStatusCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getThreeDStatusCode(){
                               return localThreeDStatusCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThreeDStatusCode
                               */
                               public void setThreeDStatusCode(java.lang.String param){
                            localThreeDStatusCodeTracker = param != null;
                                   
                                            this.localThreeDStatusCode=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for IntercoTransaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIntercoTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoTransactionTracker = false ;

                           public boolean isIntercoTransactionSpecified(){
                               return localIntercoTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIntercoTransaction(){
                               return localIntercoTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoTransaction
                               */
                               public void setIntercoTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIntercoTransactionTracker = param != null;
                                   
                                            this.localIntercoTransaction=param;
                                    

                               }
                            

                        /**
                        * field for IntercoStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus localIntercoStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoStatusTracker = false ;

                           public boolean isIntercoStatusSpecified(){
                               return localIntercoStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus getIntercoStatus(){
                               return localIntercoStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoStatus
                               */
                               public void setIntercoStatus(com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus param){
                            localIntercoStatusTracker = param != null;
                                   
                                            this.localIntercoStatus=param;
                                    

                               }
                            

                        /**
                        * field for DebitCardIssueNo
                        */

                        
                                    protected java.lang.String localDebitCardIssueNo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDebitCardIssueNoTracker = false ;

                           public boolean isDebitCardIssueNoSpecified(){
                               return localDebitCardIssueNoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDebitCardIssueNo(){
                               return localDebitCardIssueNo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DebitCardIssueNo
                               */
                               public void setDebitCardIssueNo(java.lang.String param){
                            localDebitCardIssueNoTracker = param != null;
                                   
                                            this.localDebitCardIssueNo=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for Nexus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localNexus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNexusTracker = false ;

                           public boolean isNexusSpecified(){
                               return localNexusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getNexus(){
                               return localNexus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Nexus
                               */
                               public void setNexus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localNexusTracker = param != null;
                                   
                                            this.localNexus=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryTaxRegNum
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiaryTaxRegNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTaxRegNumTracker = false ;

                           public boolean isSubsidiaryTaxRegNumSpecified(){
                               return localSubsidiaryTaxRegNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiaryTaxRegNum(){
                               return localSubsidiaryTaxRegNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryTaxRegNum
                               */
                               public void setSubsidiaryTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTaxRegNumTracker = param != null;
                                   
                                            this.localSubsidiaryTaxRegNum=param;
                                    

                               }
                            

                        /**
                        * field for TaxRegOverride
                        */

                        
                                    protected boolean localTaxRegOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRegOverrideTracker = false ;

                           public boolean isTaxRegOverrideSpecified(){
                               return localTaxRegOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTaxRegOverride(){
                               return localTaxRegOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRegOverride
                               */
                               public void setTaxRegOverride(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRegOverrideTracker =
                                       true;
                                   
                                            this.localTaxRegOverride=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsOverride
                        */

                        
                                    protected boolean localTaxDetailsOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsOverrideTracker = false ;

                           public boolean isTaxDetailsOverrideSpecified(){
                               return localTaxDetailsOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTaxDetailsOverride(){
                               return localTaxDetailsOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsOverride
                               */
                               public void setTaxDetailsOverride(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxDetailsOverrideTracker =
                                       true;
                                   
                                            this.localTaxDetailsOverride=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for PnRefNum
                        */

                        
                                    protected java.lang.String localPnRefNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPnRefNumTracker = false ;

                           public boolean isPnRefNumSpecified(){
                               return localPnRefNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPnRefNum(){
                               return localPnRefNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PnRefNum
                               */
                               public void setPnRefNum(java.lang.String param){
                            localPnRefNumTracker = param != null;
                                   
                                            this.localPnRefNum=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected java.lang.String localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(java.lang.String param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for Tax2Total
                        */

                        
                                    protected double localTax2Total ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax2TotalTracker = false ;

                           public boolean isTax2TotalSpecified(){
                               return localTax2TotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax2Total(){
                               return localTax2Total;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax2Total
                               */
                               public void setTax2Total(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax2TotalTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax2Total=param;
                                    

                               }
                            

                        /**
                        * field for Terms
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTerms ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTermsTracker = false ;

                           public boolean isTermsSpecified(){
                               return localTermsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTerms(){
                               return localTerms;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Terms
                               */
                               public void setTerms(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTermsTracker = param != null;
                                   
                                            this.localTerms=param;
                                    

                               }
                            

                        /**
                        * field for ValidFrom
                        */

                        
                                    protected java.util.Calendar localValidFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localValidFromTracker = false ;

                           public boolean isValidFromSpecified(){
                               return localValidFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getValidFrom(){
                               return localValidFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ValidFrom
                               */
                               public void setValidFrom(java.util.Calendar param){
                            localValidFromTracker = param != null;
                                   
                                            this.localValidFrom=param;
                                    

                               }
                            

                        /**
                        * field for VatRegNum
                        */

                        
                                    protected java.lang.String localVatRegNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVatRegNumTracker = false ;

                           public boolean isVatRegNumSpecified(){
                               return localVatRegNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getVatRegNum(){
                               return localVatRegNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VatRegNum
                               */
                               public void setVatRegNum(java.lang.String param){
                            localVatRegNumTracker = param != null;
                                   
                                            this.localVatRegNum=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertApplied
                        */

                        
                                    protected double localGiftCertApplied ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertAppliedTracker = false ;

                           public boolean isGiftCertAppliedSpecified(){
                               return localGiftCertAppliedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGiftCertApplied(){
                               return localGiftCertApplied;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertApplied
                               */
                               public void setGiftCertApplied(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGiftCertAppliedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGiftCertApplied=param;
                                    

                               }
                            

                        /**
                        * field for OneTime
                        */

                        
                                    protected double localOneTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOneTimeTracker = false ;

                           public boolean isOneTimeSpecified(){
                               return localOneTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getOneTime(){
                               return localOneTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OneTime
                               */
                               public void setOneTime(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localOneTimeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localOneTime=param;
                                    

                               }
                            

                        /**
                        * field for RecurWeekly
                        */

                        
                                    protected double localRecurWeekly ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurWeeklyTracker = false ;

                           public boolean isRecurWeeklySpecified(){
                               return localRecurWeeklyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRecurWeekly(){
                               return localRecurWeekly;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurWeekly
                               */
                               public void setRecurWeekly(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRecurWeeklyTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRecurWeekly=param;
                                    

                               }
                            

                        /**
                        * field for RecurMonthly
                        */

                        
                                    protected double localRecurMonthly ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurMonthlyTracker = false ;

                           public boolean isRecurMonthlySpecified(){
                               return localRecurMonthlyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRecurMonthly(){
                               return localRecurMonthly;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurMonthly
                               */
                               public void setRecurMonthly(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRecurMonthlyTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRecurMonthly=param;
                                    

                               }
                            

                        /**
                        * field for RecurQuarterly
                        */

                        
                                    protected double localRecurQuarterly ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurQuarterlyTracker = false ;

                           public boolean isRecurQuarterlySpecified(){
                               return localRecurQuarterlyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRecurQuarterly(){
                               return localRecurQuarterly;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurQuarterly
                               */
                               public void setRecurQuarterly(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRecurQuarterlyTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRecurQuarterly=param;
                                    

                               }
                            

                        /**
                        * field for RecurAnnually
                        */

                        
                                    protected double localRecurAnnually ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurAnnuallyTracker = false ;

                           public boolean isRecurAnnuallySpecified(){
                               return localRecurAnnuallyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRecurAnnually(){
                               return localRecurAnnually;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurAnnually
                               */
                               public void setRecurAnnually(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRecurAnnuallyTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRecurAnnually=param;
                                    

                               }
                            

                        /**
                        * field for TranIsVsoeBundle
                        */

                        
                                    protected boolean localTranIsVsoeBundle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIsVsoeBundleTracker = false ;

                           public boolean isTranIsVsoeBundleSpecified(){
                               return localTranIsVsoeBundleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTranIsVsoeBundle(){
                               return localTranIsVsoeBundle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranIsVsoeBundle
                               */
                               public void setTranIsVsoeBundle(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTranIsVsoeBundleTracker =
                                       true;
                                   
                                            this.localTranIsVsoeBundle=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAutoCalc
                        */

                        
                                    protected boolean localVsoeAutoCalc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAutoCalcTracker = false ;

                           public boolean isVsoeAutoCalcSpecified(){
                               return localVsoeAutoCalcTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeAutoCalc(){
                               return localVsoeAutoCalc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAutoCalc
                               */
                               public void setVsoeAutoCalc(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeAutoCalcTracker =
                                       true;
                                   
                                            this.localVsoeAutoCalc=param;
                                    

                               }
                            

                        /**
                        * field for SyncPartnerTeams
                        */

                        
                                    protected boolean localSyncPartnerTeams ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSyncPartnerTeamsTracker = false ;

                           public boolean isSyncPartnerTeamsSpecified(){
                               return localSyncPartnerTeamsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSyncPartnerTeams(){
                               return localSyncPartnerTeams;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SyncPartnerTeams
                               */
                               public void setSyncPartnerTeams(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSyncPartnerTeamsTracker =
                                       true;
                                   
                                            this.localSyncPartnerTeams=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeamList localSalesTeamList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamListTracker = false ;

                           public boolean isSalesTeamListSpecified(){
                               return localSalesTeamListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeamList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeamList getSalesTeamList(){
                               return localSalesTeamList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamList
                               */
                               public void setSalesTeamList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeamList param){
                            localSalesTeamListTracker = param != null;
                                   
                                            this.localSalesTeamList=param;
                                    

                               }
                            

                        /**
                        * field for PartnersList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.SalesOrderPartnersList localPartnersList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnersListTracker = false ;

                           public boolean isPartnersListSpecified(){
                               return localPartnersListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.SalesOrderPartnersList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderPartnersList getPartnersList(){
                               return localPartnersList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnersList
                               */
                               public void setPartnersList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderPartnersList param){
                            localPartnersListTracker = param != null;
                                   
                                            this.localPartnersList=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertRedemptionList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemptionList localGiftCertRedemptionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertRedemptionListTracker = false ;

                           public boolean isGiftCertRedemptionListSpecified(){
                               return localGiftCertRedemptionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemptionList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemptionList getGiftCertRedemptionList(){
                               return localGiftCertRedemptionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertRedemptionList
                               */
                               public void setGiftCertRedemptionList(com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemptionList param){
                            localGiftCertRedemptionListTracker = param != null;
                                   
                                            this.localGiftCertRedemptionList=param;
                                    

                               }
                            

                        /**
                        * field for PromotionsList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.PromotionsList localPromotionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPromotionsListTracker = false ;

                           public boolean isPromotionsListSpecified(){
                               return localPromotionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.PromotionsList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.PromotionsList getPromotionsList(){
                               return localPromotionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PromotionsList
                               */
                               public void setPromotionsList(com.netsuite.webservices.transactions.sales_2017_2.PromotionsList param){
                            localPromotionsListTracker = param != null;
                                   
                                            this.localPromotionsList=param;
                                    

                               }
                            

                        /**
                        * field for ItemList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItemList localItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemListTracker = false ;

                           public boolean isItemListSpecified(){
                               return localItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItemList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItemList getItemList(){
                               return localItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemList
                               */
                               public void setItemList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItemList param){
                            localItemListTracker = param != null;
                                   
                                            this.localItemList=param;
                                    

                               }
                            

                        /**
                        * field for ShipGroupList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.SalesOrderShipGroupList localShipGroupList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipGroupListTracker = false ;

                           public boolean isShipGroupListSpecified(){
                               return localShipGroupListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.SalesOrderShipGroupList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderShipGroupList getShipGroupList(){
                               return localShipGroupList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipGroupList
                               */
                               public void setShipGroupList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderShipGroupList param){
                            localShipGroupListTracker = param != null;
                                   
                                            this.localShipGroupList=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaxDetailsList localTaxDetailsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsListTracker = false ;

                           public boolean isTaxDetailsListSpecified(){
                               return localTaxDetailsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaxDetailsList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaxDetailsList getTaxDetailsList(){
                               return localTaxDetailsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsList
                               */
                               public void setTaxDetailsList(com.netsuite.webservices.platform.common_2017_2.TaxDetailsList param){
                            localTaxDetailsListTracker = param != null;
                                   
                                            this.localTaxDetailsList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":SalesOrder",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "SalesOrder",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localDrAccountTracker){
                                            if (localDrAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("drAccount cannot be null!!");
                                            }
                                           localDrAccount.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","drAccount"),
                                               xmlWriter);
                                        } if (localFxAccountTracker){
                                            if (localFxAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxAccount cannot be null!!");
                                            }
                                           localFxAccount.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fxAccount"),
                                               xmlWriter);
                                        } if (localTranDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranDate", xmlWriter);
                             

                                          if (localTranDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranIdTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranId", xmlWriter);
                             

                                          if (localTranId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTranId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEntityTaxRegNumTracker){
                                            if (localEntityTaxRegNum==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityTaxRegNum cannot be null!!");
                                            }
                                           localEntityTaxRegNum.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entityTaxRegNum"),
                                               xmlWriter);
                                        } if (localSourceTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "source", xmlWriter);
                             

                                          if (localSource==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSource);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedFromTracker){
                                            if (localCreatedFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                            }
                                           localCreatedFrom.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFrom"),
                                               xmlWriter);
                                        } if (localOrderStatusTracker){
                                            if (localOrderStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("orderStatus cannot be null!!");
                                            }
                                           localOrderStatus.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","orderStatus"),
                                               xmlWriter);
                                        } if (localNextBillTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "nextBill", xmlWriter);
                             

                                          if (localNextBill==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("nextBill cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextBill));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOpportunityTracker){
                                            if (localOpportunity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("opportunity cannot be null!!");
                                            }
                                           localOpportunity.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","opportunity"),
                                               xmlWriter);
                                        } if (localSalesRepTracker){
                                            if (localSalesRep==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                            }
                                           localSalesRep.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesRep"),
                                               xmlWriter);
                                        } if (localContribPctTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "contribPct", xmlWriter);
                             

                                          if (localContribPct==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("contribPct cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localContribPct);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPartnerTracker){
                                            if (localPartner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                            }
                                           localPartner.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partner"),
                                               xmlWriter);
                                        } if (localSalesGroupTracker){
                                            if (localSalesGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesGroup cannot be null!!");
                                            }
                                           localSalesGroup.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesGroup"),
                                               xmlWriter);
                                        } if (localSyncSalesTeamsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "syncSalesTeams", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("syncSalesTeams cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncSalesTeams));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLeadSourceTracker){
                                            if (localLeadSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                            }
                                           localLeadSource.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","leadSource"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOtherRefNumTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "otherRefNum", xmlWriter);
                             

                                          if (localOtherRefNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("otherRefNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOtherRefNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMemoTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesEffectiveDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "salesEffectiveDate", xmlWriter);
                             

                                          if (localSalesEffectiveDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("salesEffectiveDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalesEffectiveDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExcludeCommissionTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "excludeCommission", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("excludeCommission cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeCommission));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTotalCostEstimateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "totalCostEstimate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTotalCostEstimate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("totalCostEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotalCostEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstGrossProfitTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estGrossProfit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstGrossProfit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estGrossProfit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstGrossProfitPercentTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estGrossProfitPercent", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstGrossProfitPercent)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estGrossProfitPercent cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfitPercent));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExchangeRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "exchangeRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localExchangeRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("exchangeRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExchangeRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPromoCodeTracker){
                                            if (localPromoCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("promoCode cannot be null!!");
                                            }
                                           localPromoCode.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","promoCode"),
                                               xmlWriter);
                                        } if (localCurrencyNameTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "currencyName", xmlWriter);
                             

                                          if (localCurrencyName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("currencyName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCurrencyName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDiscountItemTracker){
                                            if (localDiscountItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("discountItem cannot be null!!");
                                            }
                                           localDiscountItem.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","discountItem"),
                                               xmlWriter);
                                        } if (localDiscountRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "discountRate", xmlWriter);
                             

                                          if (localDiscountRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("discountRate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDiscountRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsTaxableTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxItemTracker){
                                            if (localTaxItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxItem cannot be null!!");
                                            }
                                           localTaxItem.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxItem"),
                                               xmlWriter);
                                        } if (localTaxRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localToBePrintedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "toBePrinted", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("toBePrinted cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBePrinted));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localToBeEmailedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "toBeEmailed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("toBeEmailed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeEmailed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "email", xmlWriter);
                             

                                          if (localEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localToBeFaxedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "toBeFaxed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("toBeFaxed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeFaxed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFaxTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fax", xmlWriter);
                             

                                          if (localFax==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFax);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMessageSelTracker){
                                            if (localMessageSel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messageSel cannot be null!!");
                                            }
                                           localMessageSel.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","messageSel"),
                                               xmlWriter);
                                        } if (localMessageTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "message", xmlWriter);
                             

                                          if (localMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillingAddressTracker){
                                            if (localBillingAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingAddress cannot be null!!");
                                            }
                                           localBillingAddress.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingAddress"),
                                               xmlWriter);
                                        } if (localBillAddressListTracker){
                                            if (localBillAddressList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billAddressList cannot be null!!");
                                            }
                                           localBillAddressList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billAddressList"),
                                               xmlWriter);
                                        } if (localShippingAddressTracker){
                                            if (localShippingAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingAddress cannot be null!!");
                                            }
                                           localShippingAddress.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingAddress"),
                                               xmlWriter);
                                        } if (localShipIsResidentialTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipIsResidential", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipIsResidential cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipIsResidential));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipAddressListTracker){
                                            if (localShipAddressList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddressList cannot be null!!");
                                            }
                                           localShipAddressList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipAddressList"),
                                               xmlWriter);
                                        } if (localFobTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fob", xmlWriter);
                             

                                          if (localFob==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("fob cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFob);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipDate", xmlWriter);
                             

                                          if (localShipDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localActualShipDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "actualShipDate", xmlWriter);
                             

                                          if (localActualShipDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("actualShipDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualShipDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipMethodTracker){
                                            if (localShipMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipMethod cannot be null!!");
                                            }
                                           localShipMethod.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipMethod"),
                                               xmlWriter);
                                        } if (localShippingCostTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTax1RateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingTax1Rate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingTax1Rate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingTax1Rate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTax1Rate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsMultiShipToTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isMultiShipTo", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isMultiShipTo cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMultiShipTo));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTax2RateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingTax2Rate", xmlWriter);
                             

                                          if (localShippingTax2Rate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shippingTax2Rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShippingTax2Rate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingTaxCodeTracker){
                                            if (localShippingTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingTaxCode cannot be null!!");
                                            }
                                           localShippingTaxCode.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTaxCode"),
                                               xmlWriter);
                                        } if (localHandlingTaxCodeTracker){
                                            if (localHandlingTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("handlingTaxCode cannot be null!!");
                                            }
                                           localHandlingTaxCode.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTaxCode"),
                                               xmlWriter);
                                        } if (localHandlingTax1RateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingTax1Rate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingTax1Rate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingTax1Rate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTax1Rate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingTax2RateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingTax2Rate", xmlWriter);
                             

                                          if (localHandlingTax2Rate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("handlingTax2Rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHandlingTax2Rate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingCostTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTrackingNumbersTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "trackingNumbers", xmlWriter);
                             

                                          if (localTrackingNumbers==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("trackingNumbers cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTrackingNumbers);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLinkedTrackingNumbersTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "linkedTrackingNumbers", xmlWriter);
                             

                                          if (localLinkedTrackingNumbers==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("linkedTrackingNumbers cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLinkedTrackingNumbers);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipCompleteTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipComplete", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipComplete cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipComplete));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPaymentMethodTracker){
                                            if (localPaymentMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("paymentMethod cannot be null!!");
                                            }
                                           localPaymentMethod.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentMethod"),
                                               xmlWriter);
                                        } if (localShopperIpAddressTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shopperIpAddress", xmlWriter);
                             

                                          if (localShopperIpAddress==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shopperIpAddress cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShopperIpAddress);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSaveOnAuthDeclineTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "saveOnAuthDecline", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("saveOnAuthDecline cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaveOnAuthDecline));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCanHaveStackableTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "canHaveStackable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("canHaveStackable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCanHaveStackable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreditCardTracker){
                                            if (localCreditCard==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditCard cannot be null!!");
                                            }
                                           localCreditCard.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","creditCard"),
                                               xmlWriter);
                                        } if (localRevenueStatusTracker){
                                            if (localRevenueStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueStatus cannot be null!!");
                                            }
                                           localRevenueStatus.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revenueStatus"),
                                               xmlWriter);
                                        } if (localRecognizedRevenueTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recognizedRevenue", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRecognizedRevenue)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("recognizedRevenue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecognizedRevenue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeferredRevenueTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "deferredRevenue", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDeferredRevenue)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("deferredRevenue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferredRevenue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecOnRevCommitmentTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecOnRevCommitment", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("revRecOnRevCommitment cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecOnRevCommitment));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevCommitStatusTracker){
                                            if (localRevCommitStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revCommitStatus cannot be null!!");
                                            }
                                           localRevCommitStatus.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revCommitStatus"),
                                               xmlWriter);
                                        } if (localCcNumberTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccNumber", xmlWriter);
                             

                                          if (localCcNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcExpireDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccExpireDate", xmlWriter);
                             

                                          if (localCcExpireDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccExpireDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcExpireDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcNameTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccName", xmlWriter);
                             

                                          if (localCcName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcStreetTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccStreet", xmlWriter);
                             

                                          if (localCcStreet==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccStreet cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcStreet);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcZipCodeTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccZipCode", xmlWriter);
                             

                                          if (localCcZipCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccZipCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcZipCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPayPalStatusTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "payPalStatus", xmlWriter);
                             

                                          if (localPayPalStatus==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("payPalStatus cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPayPalStatus);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreditCardProcessorTracker){
                                            if (localCreditCardProcessor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditCardProcessor cannot be null!!");
                                            }
                                           localCreditCardProcessor.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","creditCardProcessor"),
                                               xmlWriter);
                                        } if (localPayPalTranIdTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "payPalTranId", xmlWriter);
                             

                                          if (localPayPalTranId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("payPalTranId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPayPalTranId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcApprovedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccApproved", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("ccApproved cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcApproved));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGetAuthTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "getAuth", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("getAuth cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGetAuth));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAuthCodeTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "authCode", xmlWriter);
                             

                                          if (localAuthCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("authCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAuthCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcAvsStreetMatchTracker){
                                            if (localCcAvsStreetMatch==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccAvsStreetMatch cannot be null!!");
                                            }
                                           localCcAvsStreetMatch.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccAvsStreetMatch"),
                                               xmlWriter);
                                        } if (localCcAvsZipMatchTracker){
                                            if (localCcAvsZipMatch==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccAvsZipMatch cannot be null!!");
                                            }
                                           localCcAvsZipMatch.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccAvsZipMatch"),
                                               xmlWriter);
                                        } if (localIsRecurringPaymentTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isRecurringPayment", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isRecurringPayment cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsRecurringPayment));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcSecurityCodeMatchTracker){
                                            if (localCcSecurityCodeMatch==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccSecurityCodeMatch cannot be null!!");
                                            }
                                           localCcSecurityCodeMatch.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccSecurityCodeMatch"),
                                               xmlWriter);
                                        } if (localAltSalesTotalTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altSalesTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAltSalesTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("altSalesTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIgnoreAvsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ignoreAvs", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("ignoreAvs cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIgnoreAvs));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPaymentEventResultTracker){
                                            if (localPaymentEventResult==null){
                                                 throw new org.apache.axis2.databinding.ADBException("paymentEventResult cannot be null!!");
                                            }
                                           localPaymentEventResult.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventResult"),
                                               xmlWriter);
                                        } if (localPaymentEventHoldReasonTracker){
                                            if (localPaymentEventHoldReason==null){
                                                 throw new org.apache.axis2.databinding.ADBException("paymentEventHoldReason cannot be null!!");
                                            }
                                           localPaymentEventHoldReason.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventHoldReason"),
                                               xmlWriter);
                                        } if (localPaymentEventTypeTracker){
                                            if (localPaymentEventType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("paymentEventType cannot be null!!");
                                            }
                                           localPaymentEventType.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventType"),
                                               xmlWriter);
                                        } if (localPaymentEventDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "paymentEventDate", xmlWriter);
                             

                                          if (localPaymentEventDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("paymentEventDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaymentEventDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPaymentEventUpdatedByTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "paymentEventUpdatedBy", xmlWriter);
                             

                                          if (localPaymentEventUpdatedBy==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("paymentEventUpdatedBy cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPaymentEventUpdatedBy);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubTotalTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "subTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSubTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("subTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDiscountTotalTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "discountTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDiscountTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("discountTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscountTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxTotalTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxTotal", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxTotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltShippingCostTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altShippingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAltShippingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("altShippingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltShippingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltHandlingCostTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altHandlingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAltHandlingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("altHandlingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltHandlingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTotalTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "total", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTotal)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("total cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecScheduleTracker){
                                            if (localRevRecSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                            }
                                           localRevRecSchedule.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecSchedule"),
                                               xmlWriter);
                                        } if (localRevRecStartDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecStartDate", xmlWriter);
                             

                                          if (localRevRecStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecEndDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecEndDate", xmlWriter);
                             

                                          if (localRevRecEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPaypalAuthIdTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "paypalAuthId", xmlWriter);
                             

                                          if (localPaypalAuthId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("paypalAuthId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPaypalAuthId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBalanceTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "balance", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localBalance)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("balance cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBalance));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPaypalProcessTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "paypalProcess", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("paypalProcess cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaypalProcess));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillingScheduleTracker){
                                            if (localBillingSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                            }
                                           localBillingSchedule.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingSchedule"),
                                               xmlWriter);
                                        } if (localCcSecurityCodeTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "ccSecurityCode", xmlWriter);
                             

                                          if (localCcSecurityCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ccSecurityCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCcSecurityCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localThreeDStatusCodeTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "threeDStatusCode", xmlWriter);
                             

                                          if (localThreeDStatusCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("threeDStatusCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localThreeDStatusCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localIntercoTransactionTracker){
                                            if (localIntercoTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoTransaction cannot be null!!");
                                            }
                                           localIntercoTransaction.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","intercoTransaction"),
                                               xmlWriter);
                                        } if (localIntercoStatusTracker){
                                            if (localIntercoStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoStatus cannot be null!!");
                                            }
                                           localIntercoStatus.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","intercoStatus"),
                                               xmlWriter);
                                        } if (localDebitCardIssueNoTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "debitCardIssueNo", xmlWriter);
                             

                                          if (localDebitCardIssueNo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("debitCardIssueNo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDebitCardIssueNo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNexusTracker){
                                            if (localNexus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nexus cannot be null!!");
                                            }
                                           localNexus.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","nexus"),
                                               xmlWriter);
                                        } if (localSubsidiaryTaxRegNumTracker){
                                            if (localSubsidiaryTaxRegNum==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryTaxRegNum cannot be null!!");
                                            }
                                           localSubsidiaryTaxRegNum.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subsidiaryTaxRegNum"),
                                               xmlWriter);
                                        } if (localTaxRegOverrideTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRegOverride", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRegOverride cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRegOverride));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxDetailsOverrideTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxDetailsOverride", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxDetailsOverride cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsOverride));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localPnRefNumTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pnRefNum", xmlWriter);
                             

                                          if (localPnRefNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("pnRefNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPnRefNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStatusTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "status", xmlWriter);
                             

                                          if (localStatus==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStatus);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax2TotalTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax2Total", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax2Total)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax2Total cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax2Total));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTermsTracker){
                                            if (localTerms==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terms cannot be null!!");
                                            }
                                           localTerms.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","terms"),
                                               xmlWriter);
                                        } if (localValidFromTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "validFrom", xmlWriter);
                             

                                          if (localValidFrom==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("validFrom cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localValidFrom));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVatRegNumTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vatRegNum", xmlWriter);
                             

                                          if (localVatRegNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("vatRegNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localVatRegNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertAppliedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertApplied", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGiftCertApplied)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("giftCertApplied cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertApplied));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOneTimeTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "oneTime", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localOneTime)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("oneTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOneTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurWeeklyTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recurWeekly", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRecurWeekly)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("recurWeekly cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurWeekly));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurMonthlyTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recurMonthly", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRecurMonthly)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("recurMonthly cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurMonthly));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurQuarterlyTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recurQuarterly", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRecurQuarterly)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("recurQuarterly cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurQuarterly));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurAnnuallyTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recurAnnually", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRecurAnnually)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("recurAnnually cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurAnnually));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranIsVsoeBundleTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranIsVsoeBundle", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tranIsVsoeBundle cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranIsVsoeBundle));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeAutoCalcTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeAutoCalc", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeAutoCalc cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAutoCalc));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSyncPartnerTeamsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "syncPartnerTeams", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("syncPartnerTeams cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncPartnerTeams));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesTeamListTracker){
                                            if (localSalesTeamList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamList cannot be null!!");
                                            }
                                           localSalesTeamList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesTeamList"),
                                               xmlWriter);
                                        } if (localPartnersListTracker){
                                            if (localPartnersList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnersList cannot be null!!");
                                            }
                                           localPartnersList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partnersList"),
                                               xmlWriter);
                                        } if (localGiftCertRedemptionListTracker){
                                            if (localGiftCertRedemptionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giftCertRedemptionList cannot be null!!");
                                            }
                                           localGiftCertRedemptionList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertRedemptionList"),
                                               xmlWriter);
                                        } if (localPromotionsListTracker){
                                            if (localPromotionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("promotionsList cannot be null!!");
                                            }
                                           localPromotionsList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","promotionsList"),
                                               xmlWriter);
                                        } if (localItemListTracker){
                                            if (localItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                            }
                                           localItemList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemList"),
                                               xmlWriter);
                                        } if (localShipGroupListTracker){
                                            if (localShipGroupList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipGroupList cannot be null!!");
                                            }
                                           localShipGroupList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipGroupList"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localTaxDetailsListTracker){
                                            if (localTaxDetailsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxDetailsList cannot be null!!");
                                            }
                                           localTaxDetailsList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxDetailsList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","SalesOrder"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localDrAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "drAccount"));
                            
                            
                                    if (localDrAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("drAccount cannot be null!!");
                                    }
                                    elementList.add(localDrAccount);
                                } if (localFxAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fxAccount"));
                            
                            
                                    if (localFxAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxAccount cannot be null!!");
                                    }
                                    elementList.add(localFxAccount);
                                } if (localTranDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranDate"));
                                 
                                        if (localTranDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                        }
                                    } if (localTranIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranId"));
                                 
                                        if (localTranId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                        }
                                    } if (localEntityTaxRegNumTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "entityTaxRegNum"));
                            
                            
                                    if (localEntityTaxRegNum==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityTaxRegNum cannot be null!!");
                                    }
                                    elementList.add(localEntityTaxRegNum);
                                } if (localSourceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "source"));
                                 
                                        if (localSource != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSource));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                        }
                                    } if (localCreatedFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFrom"));
                            
                            
                                    if (localCreatedFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                    }
                                    elementList.add(localCreatedFrom);
                                } if (localOrderStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderStatus"));
                            
                            
                                    if (localOrderStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("orderStatus cannot be null!!");
                                    }
                                    elementList.add(localOrderStatus);
                                } if (localNextBillTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "nextBill"));
                                 
                                        if (localNextBill != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextBill));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("nextBill cannot be null!!");
                                        }
                                    } if (localOpportunityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "opportunity"));
                            
                            
                                    if (localOpportunity==null){
                                         throw new org.apache.axis2.databinding.ADBException("opportunity cannot be null!!");
                                    }
                                    elementList.add(localOpportunity);
                                } if (localSalesRepTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesRep"));
                            
                            
                                    if (localSalesRep==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                    }
                                    elementList.add(localSalesRep);
                                } if (localContribPctTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "contribPct"));
                                 
                                        if (localContribPct != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContribPct));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("contribPct cannot be null!!");
                                        }
                                    } if (localPartnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "partner"));
                            
                            
                                    if (localPartner==null){
                                         throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    }
                                    elementList.add(localPartner);
                                } if (localSalesGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesGroup"));
                            
                            
                                    if (localSalesGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesGroup cannot be null!!");
                                    }
                                    elementList.add(localSalesGroup);
                                } if (localSyncSalesTeamsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "syncSalesTeams"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncSalesTeams));
                            } if (localLeadSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "leadSource"));
                            
                            
                                    if (localLeadSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                    }
                                    elementList.add(localLeadSource);
                                } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localOtherRefNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "otherRefNum"));
                                 
                                        if (localOtherRefNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOtherRefNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("otherRefNum cannot be null!!");
                                        }
                                    } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localSalesEffectiveDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesEffectiveDate"));
                                 
                                        if (localSalesEffectiveDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalesEffectiveDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("salesEffectiveDate cannot be null!!");
                                        }
                                    } if (localExcludeCommissionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "excludeCommission"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeCommission));
                            } if (localTotalCostEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "totalCostEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotalCostEstimate));
                            } if (localEstGrossProfitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "estGrossProfit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfit));
                            } if (localEstGrossProfitPercentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "estGrossProfitPercent"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstGrossProfitPercent));
                            } if (localExchangeRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "exchangeRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExchangeRate));
                            } if (localPromoCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "promoCode"));
                            
                            
                                    if (localPromoCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("promoCode cannot be null!!");
                                    }
                                    elementList.add(localPromoCode);
                                } if (localCurrencyNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "currencyName"));
                                 
                                        if (localCurrencyName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCurrencyName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("currencyName cannot be null!!");
                                        }
                                    } if (localDiscountItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "discountItem"));
                            
                            
                                    if (localDiscountItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("discountItem cannot be null!!");
                                    }
                                    elementList.add(localDiscountItem);
                                } if (localDiscountRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "discountRate"));
                                 
                                        if (localDiscountRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscountRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("discountRate cannot be null!!");
                                        }
                                    } if (localIsTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                            } if (localTaxItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxItem"));
                            
                            
                                    if (localTaxItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxItem cannot be null!!");
                                    }
                                    elementList.add(localTaxItem);
                                } if (localTaxRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate));
                            } if (localToBePrintedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "toBePrinted"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBePrinted));
                            } if (localToBeEmailedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "toBeEmailed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeEmailed));
                            } if (localEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "email"));
                                 
                                        if (localEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                        }
                                    } if (localToBeFaxedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "toBeFaxed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localToBeFaxed));
                            } if (localFaxTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fax"));
                                 
                                        if (localFax != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFax));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                        }
                                    } if (localMessageSelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "messageSel"));
                            
                            
                                    if (localMessageSel==null){
                                         throw new org.apache.axis2.databinding.ADBException("messageSel cannot be null!!");
                                    }
                                    elementList.add(localMessageSel);
                                } if (localMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "message"));
                                 
                                        if (localMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                        }
                                    } if (localBillingAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingAddress"));
                            
                            
                                    if (localBillingAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingAddress cannot be null!!");
                                    }
                                    elementList.add(localBillingAddress);
                                } if (localBillAddressListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "billAddressList"));
                            
                            
                                    if (localBillAddressList==null){
                                         throw new org.apache.axis2.databinding.ADBException("billAddressList cannot be null!!");
                                    }
                                    elementList.add(localBillAddressList);
                                } if (localShippingAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingAddress"));
                            
                            
                                    if (localShippingAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingAddress cannot be null!!");
                                    }
                                    elementList.add(localShippingAddress);
                                } if (localShipIsResidentialTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipIsResidential"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipIsResidential));
                            } if (localShipAddressListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipAddressList"));
                            
                            
                                    if (localShipAddressList==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddressList cannot be null!!");
                                    }
                                    elementList.add(localShipAddressList);
                                } if (localFobTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fob"));
                                 
                                        if (localFob != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFob));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("fob cannot be null!!");
                                        }
                                    } if (localShipDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipDate"));
                                 
                                        if (localShipDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipDate cannot be null!!");
                                        }
                                    } if (localActualShipDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "actualShipDate"));
                                 
                                        if (localActualShipDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualShipDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("actualShipDate cannot be null!!");
                                        }
                                    } if (localShipMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipMethod"));
                            
                            
                                    if (localShipMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipMethod cannot be null!!");
                                    }
                                    elementList.add(localShipMethod);
                                } if (localShippingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCost));
                            } if (localShippingTax1RateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTax1Rate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTax1Rate));
                            } if (localIsMultiShipToTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isMultiShipTo"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMultiShipTo));
                            } if (localShippingTax2RateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTax2Rate"));
                                 
                                        if (localShippingTax2Rate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingTax2Rate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shippingTax2Rate cannot be null!!");
                                        }
                                    } if (localShippingTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingTaxCode"));
                            
                            
                                    if (localShippingTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingTaxCode cannot be null!!");
                                    }
                                    elementList.add(localShippingTaxCode);
                                } if (localHandlingTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTaxCode"));
                            
                            
                                    if (localHandlingTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("handlingTaxCode cannot be null!!");
                                    }
                                    elementList.add(localHandlingTaxCode);
                                } if (localHandlingTax1RateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTax1Rate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTax1Rate));
                            } if (localHandlingTax2RateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingTax2Rate"));
                                 
                                        if (localHandlingTax2Rate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingTax2Rate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("handlingTax2Rate cannot be null!!");
                                        }
                                    } if (localHandlingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCost));
                            } if (localTrackingNumbersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "trackingNumbers"));
                                 
                                        if (localTrackingNumbers != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTrackingNumbers));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("trackingNumbers cannot be null!!");
                                        }
                                    } if (localLinkedTrackingNumbersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "linkedTrackingNumbers"));
                                 
                                        if (localLinkedTrackingNumbers != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLinkedTrackingNumbers));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("linkedTrackingNumbers cannot be null!!");
                                        }
                                    } if (localShipCompleteTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipComplete"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipComplete));
                            } if (localPaymentMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentMethod"));
                            
                            
                                    if (localPaymentMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("paymentMethod cannot be null!!");
                                    }
                                    elementList.add(localPaymentMethod);
                                } if (localShopperIpAddressTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shopperIpAddress"));
                                 
                                        if (localShopperIpAddress != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShopperIpAddress));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shopperIpAddress cannot be null!!");
                                        }
                                    } if (localSaveOnAuthDeclineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "saveOnAuthDecline"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaveOnAuthDecline));
                            } if (localCanHaveStackableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "canHaveStackable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCanHaveStackable));
                            } if (localCreditCardTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "creditCard"));
                            
                            
                                    if (localCreditCard==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditCard cannot be null!!");
                                    }
                                    elementList.add(localCreditCard);
                                } if (localRevenueStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revenueStatus"));
                            
                            
                                    if (localRevenueStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueStatus cannot be null!!");
                                    }
                                    elementList.add(localRevenueStatus);
                                } if (localRecognizedRevenueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "recognizedRevenue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecognizedRevenue));
                            } if (localDeferredRevenueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "deferredRevenue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferredRevenue));
                            } if (localRevRecOnRevCommitmentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecOnRevCommitment"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecOnRevCommitment));
                            } if (localRevCommitStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revCommitStatus"));
                            
                            
                                    if (localRevCommitStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("revCommitStatus cannot be null!!");
                                    }
                                    elementList.add(localRevCommitStatus);
                                } if (localCcNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccNumber"));
                                 
                                        if (localCcNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccNumber cannot be null!!");
                                        }
                                    } if (localCcExpireDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccExpireDate"));
                                 
                                        if (localCcExpireDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcExpireDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccExpireDate cannot be null!!");
                                        }
                                    } if (localCcNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccName"));
                                 
                                        if (localCcName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccName cannot be null!!");
                                        }
                                    } if (localCcStreetTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccStreet"));
                                 
                                        if (localCcStreet != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcStreet));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccStreet cannot be null!!");
                                        }
                                    } if (localCcZipCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccZipCode"));
                                 
                                        if (localCcZipCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcZipCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccZipCode cannot be null!!");
                                        }
                                    } if (localPayPalStatusTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "payPalStatus"));
                                 
                                        if (localPayPalStatus != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPayPalStatus));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("payPalStatus cannot be null!!");
                                        }
                                    } if (localCreditCardProcessorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "creditCardProcessor"));
                            
                            
                                    if (localCreditCardProcessor==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditCardProcessor cannot be null!!");
                                    }
                                    elementList.add(localCreditCardProcessor);
                                } if (localPayPalTranIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "payPalTranId"));
                                 
                                        if (localPayPalTranId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPayPalTranId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("payPalTranId cannot be null!!");
                                        }
                                    } if (localCcApprovedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccApproved"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcApproved));
                            } if (localGetAuthTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "getAuth"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGetAuth));
                            } if (localAuthCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "authCode"));
                                 
                                        if (localAuthCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAuthCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("authCode cannot be null!!");
                                        }
                                    } if (localCcAvsStreetMatchTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccAvsStreetMatch"));
                            
                            
                                    if (localCcAvsStreetMatch==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccAvsStreetMatch cannot be null!!");
                                    }
                                    elementList.add(localCcAvsStreetMatch);
                                } if (localCcAvsZipMatchTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccAvsZipMatch"));
                            
                            
                                    if (localCcAvsZipMatch==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccAvsZipMatch cannot be null!!");
                                    }
                                    elementList.add(localCcAvsZipMatch);
                                } if (localIsRecurringPaymentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isRecurringPayment"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsRecurringPayment));
                            } if (localCcSecurityCodeMatchTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccSecurityCodeMatch"));
                            
                            
                                    if (localCcSecurityCodeMatch==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccSecurityCodeMatch cannot be null!!");
                                    }
                                    elementList.add(localCcSecurityCodeMatch);
                                } if (localAltSalesTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "altSalesTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesTotal));
                            } if (localIgnoreAvsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ignoreAvs"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIgnoreAvs));
                            } if (localPaymentEventResultTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentEventResult"));
                            
                            
                                    if (localPaymentEventResult==null){
                                         throw new org.apache.axis2.databinding.ADBException("paymentEventResult cannot be null!!");
                                    }
                                    elementList.add(localPaymentEventResult);
                                } if (localPaymentEventHoldReasonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentEventHoldReason"));
                            
                            
                                    if (localPaymentEventHoldReason==null){
                                         throw new org.apache.axis2.databinding.ADBException("paymentEventHoldReason cannot be null!!");
                                    }
                                    elementList.add(localPaymentEventHoldReason);
                                } if (localPaymentEventTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentEventType"));
                            
                            
                                    if (localPaymentEventType==null){
                                         throw new org.apache.axis2.databinding.ADBException("paymentEventType cannot be null!!");
                                    }
                                    elementList.add(localPaymentEventType);
                                } if (localPaymentEventDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentEventDate"));
                                 
                                        if (localPaymentEventDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaymentEventDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("paymentEventDate cannot be null!!");
                                        }
                                    } if (localPaymentEventUpdatedByTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paymentEventUpdatedBy"));
                                 
                                        if (localPaymentEventUpdatedBy != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaymentEventUpdatedBy));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("paymentEventUpdatedBy cannot be null!!");
                                        }
                                    } if (localSubTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "subTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubTotal));
                            } if (localDiscountTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "discountTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDiscountTotal));
                            } if (localTaxTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxTotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxTotal));
                            } if (localAltShippingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "altShippingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltShippingCost));
                            } if (localAltHandlingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "altHandlingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltHandlingCost));
                            } if (localTotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "total"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotal));
                            } if (localRevRecScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecSchedule"));
                            
                            
                                    if (localRevRecSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                    }
                                    elementList.add(localRevRecSchedule);
                                } if (localRevRecStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecStartDate"));
                                 
                                        if (localRevRecStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                        }
                                    } if (localRevRecEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecEndDate"));
                                 
                                        if (localRevRecEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                        }
                                    } if (localPaypalAuthIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paypalAuthId"));
                                 
                                        if (localPaypalAuthId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaypalAuthId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("paypalAuthId cannot be null!!");
                                        }
                                    } if (localBalanceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "balance"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBalance));
                            } if (localPaypalProcessTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paypalProcess"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPaypalProcess));
                            } if (localBillingScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingSchedule"));
                            
                            
                                    if (localBillingSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                    }
                                    elementList.add(localBillingSchedule);
                                } if (localCcSecurityCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ccSecurityCode"));
                                 
                                        if (localCcSecurityCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCcSecurityCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ccSecurityCode cannot be null!!");
                                        }
                                    } if (localThreeDStatusCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "threeDStatusCode"));
                                 
                                        if (localThreeDStatusCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localThreeDStatusCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("threeDStatusCode cannot be null!!");
                                        }
                                    } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localIntercoTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "intercoTransaction"));
                            
                            
                                    if (localIntercoTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoTransaction cannot be null!!");
                                    }
                                    elementList.add(localIntercoTransaction);
                                } if (localIntercoStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "intercoStatus"));
                            
                            
                                    if (localIntercoStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoStatus cannot be null!!");
                                    }
                                    elementList.add(localIntercoStatus);
                                } if (localDebitCardIssueNoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "debitCardIssueNo"));
                                 
                                        if (localDebitCardIssueNo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDebitCardIssueNo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("debitCardIssueNo cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localNexusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "nexus"));
                            
                            
                                    if (localNexus==null){
                                         throw new org.apache.axis2.databinding.ADBException("nexus cannot be null!!");
                                    }
                                    elementList.add(localNexus);
                                } if (localSubsidiaryTaxRegNumTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiaryTaxRegNum"));
                            
                            
                                    if (localSubsidiaryTaxRegNum==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryTaxRegNum cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryTaxRegNum);
                                } if (localTaxRegOverrideTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRegOverride"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRegOverride));
                            } if (localTaxDetailsOverrideTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsOverride"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsOverride));
                            } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localPnRefNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "pnRefNum"));
                                 
                                        if (localPnRefNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPnRefNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("pnRefNum cannot be null!!");
                                        }
                                    } if (localStatusTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "status"));
                                 
                                        if (localStatus != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStatus));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                        }
                                    } if (localTax2TotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax2Total"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax2Total));
                            } if (localTermsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "terms"));
                            
                            
                                    if (localTerms==null){
                                         throw new org.apache.axis2.databinding.ADBException("terms cannot be null!!");
                                    }
                                    elementList.add(localTerms);
                                } if (localValidFromTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "validFrom"));
                                 
                                        if (localValidFrom != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localValidFrom));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("validFrom cannot be null!!");
                                        }
                                    } if (localVatRegNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vatRegNum"));
                                 
                                        if (localVatRegNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVatRegNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("vatRegNum cannot be null!!");
                                        }
                                    } if (localGiftCertAppliedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertApplied"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertApplied));
                            } if (localOneTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "oneTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOneTime));
                            } if (localRecurWeeklyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "recurWeekly"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurWeekly));
                            } if (localRecurMonthlyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "recurMonthly"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurMonthly));
                            } if (localRecurQuarterlyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "recurQuarterly"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurQuarterly));
                            } if (localRecurAnnuallyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "recurAnnually"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurAnnually));
                            } if (localTranIsVsoeBundleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranIsVsoeBundle"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranIsVsoeBundle));
                            } if (localVsoeAutoCalcTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeAutoCalc"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAutoCalc));
                            } if (localSyncPartnerTeamsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "syncPartnerTeams"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSyncPartnerTeams));
                            } if (localSalesTeamListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesTeamList"));
                            
                            
                                    if (localSalesTeamList==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamList cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamList);
                                } if (localPartnersListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "partnersList"));
                            
                            
                                    if (localPartnersList==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnersList cannot be null!!");
                                    }
                                    elementList.add(localPartnersList);
                                } if (localGiftCertRedemptionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertRedemptionList"));
                            
                            
                                    if (localGiftCertRedemptionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("giftCertRedemptionList cannot be null!!");
                                    }
                                    elementList.add(localGiftCertRedemptionList);
                                } if (localPromotionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "promotionsList"));
                            
                            
                                    if (localPromotionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("promotionsList cannot be null!!");
                                    }
                                    elementList.add(localPromotionsList);
                                } if (localItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemList"));
                            
                            
                                    if (localItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                    }
                                    elementList.add(localItemList);
                                } if (localShipGroupListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipGroupList"));
                            
                            
                                    if (localShipGroupList==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipGroupList cannot be null!!");
                                    }
                                    elementList.add(localShipGroupList);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localTaxDetailsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsList"));
                            
                            
                                    if (localTaxDetailsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxDetailsList cannot be null!!");
                                    }
                                    elementList.add(localTaxDetailsList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SalesOrder parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SalesOrder object =
                new SalesOrder();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"SalesOrder".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SalesOrder)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","drAccount").equals(reader.getName())){
                                
                                                object.setDrAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fxAccount").equals(reader.getName())){
                                
                                                object.setFxAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entityTaxRegNum").equals(reader.getName())){
                                
                                                object.setEntityTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","source").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"source" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSource(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFrom").equals(reader.getName())){
                                
                                                object.setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","orderStatus").equals(reader.getName())){
                                
                                                object.setOrderStatus(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderOrderStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","nextBill").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"nextBill" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNextBill(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","opportunity").equals(reader.getName())){
                                
                                                object.setOpportunity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                
                                                object.setSalesRep(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","contribPct").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"contribPct" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setContribPct(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                                object.setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesGroup").equals(reader.getName())){
                                
                                                object.setSalesGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","syncSalesTeams").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"syncSalesTeams" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSyncSalesTeams(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","leadSource").equals(reader.getName())){
                                
                                                object.setLeadSource(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","otherRefNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"otherRefNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOtherRefNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesEffectiveDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"salesEffectiveDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSalesEffectiveDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","excludeCommission").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"excludeCommission" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExcludeCommission(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","totalCostEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"totalCostEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTotalCostEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTotalCostEstimate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","estGrossProfit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estGrossProfit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstGrossProfit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstGrossProfit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","estGrossProfitPercent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estGrossProfitPercent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstGrossProfitPercent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstGrossProfitPercent(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","exchangeRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"exchangeRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExchangeRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setExchangeRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","promoCode").equals(reader.getName())){
                                
                                                object.setPromoCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","currencyName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"currencyName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCurrencyName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","discountItem").equals(reader.getName())){
                                
                                                object.setDiscountItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","discountRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"discountRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDiscountRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxItem").equals(reader.getName())){
                                
                                                object.setTaxItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","toBePrinted").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"toBePrinted" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setToBePrinted(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","toBeEmailed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"toBeEmailed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setToBeEmailed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"email" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","toBeFaxed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"toBeFaxed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setToBeFaxed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fax" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFax(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","messageSel").equals(reader.getName())){
                                
                                                object.setMessageSel(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"message" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingAddress").equals(reader.getName())){
                                
                                                object.setBillingAddress(com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billAddressList").equals(reader.getName())){
                                
                                                object.setBillAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingAddress").equals(reader.getName())){
                                
                                                object.setShippingAddress(com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipIsResidential").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipIsResidential" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipIsResidential(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipAddressList").equals(reader.getName())){
                                
                                                object.setShipAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fob").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fob" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFob(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","actualShipDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"actualShipDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setActualShipDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipMethod").equals(reader.getName())){
                                
                                                object.setShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTax1Rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingTax1Rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingTax1Rate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingTax1Rate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isMultiShipTo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isMultiShipTo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsMultiShipTo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTax2Rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingTax2Rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingTax2Rate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingTaxCode").equals(reader.getName())){
                                
                                                object.setShippingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTaxCode").equals(reader.getName())){
                                
                                                object.setHandlingTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTax1Rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingTax1Rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingTax1Rate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingTax1Rate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingTax2Rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingTax2Rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingTax2Rate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","trackingNumbers").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"trackingNumbers" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTrackingNumbers(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","linkedTrackingNumbers").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"linkedTrackingNumbers" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLinkedTrackingNumbers(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipComplete").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipComplete" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipComplete(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentMethod").equals(reader.getName())){
                                
                                                object.setPaymentMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shopperIpAddress").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shopperIpAddress" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShopperIpAddress(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","saveOnAuthDecline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"saveOnAuthDecline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSaveOnAuthDecline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","canHaveStackable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"canHaveStackable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCanHaveStackable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","creditCard").equals(reader.getName())){
                                
                                                object.setCreditCard(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revenueStatus").equals(reader.getName())){
                                
                                                object.setRevenueStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","recognizedRevenue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recognizedRevenue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecognizedRevenue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRecognizedRevenue(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deferredRevenue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"deferredRevenue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeferredRevenue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDeferredRevenue(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecOnRevCommitment").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecOnRevCommitment" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecOnRevCommitment(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revCommitStatus").equals(reader.getName())){
                                
                                                object.setRevCommitStatus(com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccExpireDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccExpireDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcExpireDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccStreet").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccStreet" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcStreet(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccZipCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccZipCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcZipCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","payPalStatus").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"payPalStatus" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPayPalStatus(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","creditCardProcessor").equals(reader.getName())){
                                
                                                object.setCreditCardProcessor(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","payPalTranId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"payPalTranId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPayPalTranId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccApproved").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccApproved" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcApproved(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","getAuth").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"getAuth" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGetAuth(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","authCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"authCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAuthCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccAvsStreetMatch").equals(reader.getName())){
                                
                                                object.setCcAvsStreetMatch(com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccAvsZipMatch").equals(reader.getName())){
                                
                                                object.setCcAvsZipMatch(com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isRecurringPayment").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isRecurringPayment" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsRecurringPayment(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccSecurityCodeMatch").equals(reader.getName())){
                                
                                                object.setCcSecurityCodeMatch(com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","altSalesTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altSalesTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltSalesTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAltSalesTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ignoreAvs").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ignoreAvs" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIgnoreAvs(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventResult").equals(reader.getName())){
                                
                                                object.setPaymentEventResult(com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventResult.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventHoldReason").equals(reader.getName())){
                                
                                                object.setPaymentEventHoldReason(com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventHoldReason.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventType").equals(reader.getName())){
                                
                                                object.setPaymentEventType(com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"paymentEventDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPaymentEventDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paymentEventUpdatedBy").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"paymentEventUpdatedBy" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPaymentEventUpdatedBy(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"subTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSubTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSubTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","discountTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"discountTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDiscountTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDiscountTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxTotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxTotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","altShippingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altShippingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltShippingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAltShippingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","altHandlingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altHandlingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltHandlingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAltHandlingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","total").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"total" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTotal(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecSchedule").equals(reader.getName())){
                                
                                                object.setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paypalAuthId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"paypalAuthId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPaypalAuthId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","balance").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"balance" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBalance(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBalance(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paypalProcess").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"paypalProcess" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPaypalProcess(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingSchedule").equals(reader.getName())){
                                
                                                object.setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ccSecurityCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ccSecurityCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCcSecurityCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","threeDStatusCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"threeDStatusCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setThreeDStatusCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","intercoTransaction").equals(reader.getName())){
                                
                                                object.setIntercoTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","intercoStatus").equals(reader.getName())){
                                
                                                object.setIntercoStatus(com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","debitCardIssueNo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"debitCardIssueNo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDebitCardIssueNo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","nexus").equals(reader.getName())){
                                
                                                object.setNexus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subsidiaryTaxRegNum").equals(reader.getName())){
                                
                                                object.setSubsidiaryTaxRegNum(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxRegOverride").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRegOverride" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRegOverride(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxDetailsOverride").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxDetailsOverride" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxDetailsOverride(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","pnRefNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pnRefNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPnRefNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"status" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStatus(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tax2Total").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax2Total" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax2Total(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax2Total(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","terms").equals(reader.getName())){
                                
                                                object.setTerms(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","validFrom").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"validFrom" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setValidFrom(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vatRegNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vatRegNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVatRegNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertApplied").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertApplied" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertApplied(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGiftCertApplied(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","oneTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"oneTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOneTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOneTime(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","recurWeekly").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recurWeekly" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecurWeekly(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRecurWeekly(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","recurMonthly").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recurMonthly" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecurMonthly(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRecurMonthly(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","recurQuarterly").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recurQuarterly" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecurQuarterly(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRecurQuarterly(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","recurAnnually").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recurAnnually" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecurAnnually(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRecurAnnually(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tranIsVsoeBundle").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranIsVsoeBundle" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranIsVsoeBundle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeAutoCalc").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeAutoCalc" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeAutoCalc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","syncPartnerTeams").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"syncPartnerTeams" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSyncPartnerTeams(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesTeamList").equals(reader.getName())){
                                
                                                object.setSalesTeamList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeamList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partnersList").equals(reader.getName())){
                                
                                                object.setPartnersList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderPartnersList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertRedemptionList").equals(reader.getName())){
                                
                                                object.setGiftCertRedemptionList(com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemptionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","promotionsList").equals(reader.getName())){
                                
                                                object.setPromotionsList(com.netsuite.webservices.transactions.sales_2017_2.PromotionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemList").equals(reader.getName())){
                                
                                                object.setItemList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipGroupList").equals(reader.getName())){
                                
                                                object.setShipGroupList(com.netsuite.webservices.transactions.sales_2017_2.SalesOrderShipGroupList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxDetailsList").equals(reader.getName())){
                                
                                                object.setTaxDetailsList(com.netsuite.webservices.platform.common_2017_2.TaxDetailsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    