
/**
 * ItemFulfillment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  ItemFulfillment bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemFulfillment extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemFulfillment
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for PostingPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPostingPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPostingPeriodTracker = false ;

                           public boolean isPostingPeriodSpecified(){
                               return localPostingPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPostingPeriod(){
                               return localPostingPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PostingPeriod
                               */
                               public void setPostingPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPostingPeriodTracker = param != null;
                                   
                                            this.localPostingPeriod=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromTracker = false ;

                           public boolean isCreatedFromSpecified(){
                               return localCreatedFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedFrom(){
                               return localCreatedFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFrom
                               */
                               public void setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedFromTracker = param != null;
                                   
                                            this.localCreatedFrom=param;
                                    

                               }
                            

                        /**
                        * field for RequestedBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRequestedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRequestedByTracker = false ;

                           public boolean isRequestedBySpecified(){
                               return localRequestedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRequestedBy(){
                               return localRequestedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RequestedBy
                               */
                               public void setRequestedBy(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRequestedByTracker = param != null;
                                   
                                            this.localRequestedBy=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFromShipGroup
                        */

                        
                                    protected long localCreatedFromShipGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromShipGroupTracker = false ;

                           public boolean isCreatedFromShipGroupSpecified(){
                               return localCreatedFromShipGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getCreatedFromShipGroup(){
                               return localCreatedFromShipGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFromShipGroup
                               */
                               public void setCreatedFromShipGroup(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localCreatedFromShipGroupTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localCreatedFromShipGroup=param;
                                    

                               }
                            

                        /**
                        * field for Partner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPartner(){
                               return localPartner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Partner
                               */
                               public void setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPartnerTracker = param != null;
                                   
                                            this.localPartner=param;
                                    

                               }
                            

                        /**
                        * field for ShippingAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.Address localShippingAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingAddressTracker = false ;

                           public boolean isShippingAddressSpecified(){
                               return localShippingAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.Address
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.Address getShippingAddress(){
                               return localShippingAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingAddress
                               */
                               public void setShippingAddress(com.netsuite.webservices.platform.common_2017_2.Address param){
                            localShippingAddressTracker = param != null;
                                   
                                            this.localShippingAddress=param;
                                    

                               }
                            

                        /**
                        * field for PickedDate
                        */

                        
                                    protected java.util.Calendar localPickedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPickedDateTracker = false ;

                           public boolean isPickedDateSpecified(){
                               return localPickedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getPickedDate(){
                               return localPickedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PickedDate
                               */
                               public void setPickedDate(java.util.Calendar param){
                            localPickedDateTracker = param != null;
                                   
                                            this.localPickedDate=param;
                                    

                               }
                            

                        /**
                        * field for PackedDate
                        */

                        
                                    protected java.util.Calendar localPackedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackedDateTracker = false ;

                           public boolean isPackedDateSpecified(){
                               return localPackedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getPackedDate(){
                               return localPackedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackedDate
                               */
                               public void setPackedDate(java.util.Calendar param){
                            localPackedDateTracker = param != null;
                                   
                                            this.localPackedDate=param;
                                    

                               }
                            

                        /**
                        * field for ShippedDate
                        */

                        
                                    protected java.util.Calendar localShippedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippedDateTracker = false ;

                           public boolean isShippedDateSpecified(){
                               return localShippedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getShippedDate(){
                               return localShippedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippedDate
                               */
                               public void setShippedDate(java.util.Calendar param){
                            localShippedDateTracker = param != null;
                                   
                                            this.localShippedDate=param;
                                    

                               }
                            

                        /**
                        * field for ShipIsResidential
                        */

                        
                                    protected boolean localShipIsResidential ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipIsResidentialTracker = false ;

                           public boolean isShipIsResidentialSpecified(){
                               return localShipIsResidentialTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShipIsResidential(){
                               return localShipIsResidential;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipIsResidential
                               */
                               public void setShipIsResidential(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipIsResidentialTracker =
                                       true;
                                   
                                            this.localShipIsResidential=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddressList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipAddressList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressListTracker = false ;

                           public boolean isShipAddressListSpecified(){
                               return localShipAddressListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipAddressList(){
                               return localShipAddressList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddressList
                               */
                               public void setShipAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipAddressListTracker = param != null;
                                   
                                            this.localShipAddressList=param;
                                    

                               }
                            

                        /**
                        * field for ShipStatus
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentShipStatus localShipStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipStatusTracker = false ;

                           public boolean isShipStatusSpecified(){
                               return localShipStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentShipStatus
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentShipStatus getShipStatus(){
                               return localShipStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipStatus
                               */
                               public void setShipStatus(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentShipStatus param){
                            localShipStatusTracker = param != null;
                                   
                                            this.localShipStatus=param;
                                    

                               }
                            

                        /**
                        * field for SaturdayDeliveryUps
                        */

                        
                                    protected boolean localSaturdayDeliveryUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaturdayDeliveryUpsTracker = false ;

                           public boolean isSaturdayDeliveryUpsSpecified(){
                               return localSaturdayDeliveryUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSaturdayDeliveryUps(){
                               return localSaturdayDeliveryUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SaturdayDeliveryUps
                               */
                               public void setSaturdayDeliveryUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSaturdayDeliveryUpsTracker =
                                       true;
                                   
                                            this.localSaturdayDeliveryUps=param;
                                    

                               }
                            

                        /**
                        * field for SendShipNotifyEmailUps
                        */

                        
                                    protected boolean localSendShipNotifyEmailUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendShipNotifyEmailUpsTracker = false ;

                           public boolean isSendShipNotifyEmailUpsSpecified(){
                               return localSendShipNotifyEmailUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendShipNotifyEmailUps(){
                               return localSendShipNotifyEmailUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendShipNotifyEmailUps
                               */
                               public void setSendShipNotifyEmailUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendShipNotifyEmailUpsTracker =
                                       true;
                                   
                                            this.localSendShipNotifyEmailUps=param;
                                    

                               }
                            

                        /**
                        * field for SendBackupEmailUps
                        */

                        
                                    protected boolean localSendBackupEmailUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendBackupEmailUpsTracker = false ;

                           public boolean isSendBackupEmailUpsSpecified(){
                               return localSendBackupEmailUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendBackupEmailUps(){
                               return localSendBackupEmailUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendBackupEmailUps
                               */
                               public void setSendBackupEmailUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendBackupEmailUpsTracker =
                                       true;
                                   
                                            this.localSendBackupEmailUps=param;
                                    

                               }
                            

                        /**
                        * field for ShipNotifyEmailAddressUps
                        */

                        
                                    protected java.lang.String localShipNotifyEmailAddressUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipNotifyEmailAddressUpsTracker = false ;

                           public boolean isShipNotifyEmailAddressUpsSpecified(){
                               return localShipNotifyEmailAddressUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShipNotifyEmailAddressUps(){
                               return localShipNotifyEmailAddressUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipNotifyEmailAddressUps
                               */
                               public void setShipNotifyEmailAddressUps(java.lang.String param){
                            localShipNotifyEmailAddressUpsTracker = param != null;
                                   
                                            this.localShipNotifyEmailAddressUps=param;
                                    

                               }
                            

                        /**
                        * field for ShipNotifyEmailAddress2Ups
                        */

                        
                                    protected java.lang.String localShipNotifyEmailAddress2Ups ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipNotifyEmailAddress2UpsTracker = false ;

                           public boolean isShipNotifyEmailAddress2UpsSpecified(){
                               return localShipNotifyEmailAddress2UpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShipNotifyEmailAddress2Ups(){
                               return localShipNotifyEmailAddress2Ups;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipNotifyEmailAddress2Ups
                               */
                               public void setShipNotifyEmailAddress2Ups(java.lang.String param){
                            localShipNotifyEmailAddress2UpsTracker = param != null;
                                   
                                            this.localShipNotifyEmailAddress2Ups=param;
                                    

                               }
                            

                        /**
                        * field for BackupEmailAddressUps
                        */

                        
                                    protected java.lang.String localBackupEmailAddressUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBackupEmailAddressUpsTracker = false ;

                           public boolean isBackupEmailAddressUpsSpecified(){
                               return localBackupEmailAddressUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBackupEmailAddressUps(){
                               return localBackupEmailAddressUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BackupEmailAddressUps
                               */
                               public void setBackupEmailAddressUps(java.lang.String param){
                            localBackupEmailAddressUpsTracker = param != null;
                                   
                                            this.localBackupEmailAddressUps=param;
                                    

                               }
                            

                        /**
                        * field for ShipNotifyEmailMessageUps
                        */

                        
                                    protected java.lang.String localShipNotifyEmailMessageUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipNotifyEmailMessageUpsTracker = false ;

                           public boolean isShipNotifyEmailMessageUpsSpecified(){
                               return localShipNotifyEmailMessageUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShipNotifyEmailMessageUps(){
                               return localShipNotifyEmailMessageUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipNotifyEmailMessageUps
                               */
                               public void setShipNotifyEmailMessageUps(java.lang.String param){
                            localShipNotifyEmailMessageUpsTracker = param != null;
                                   
                                            this.localShipNotifyEmailMessageUps=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyAcctUps
                        */

                        
                                    protected java.lang.String localThirdPartyAcctUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyAcctUpsTracker = false ;

                           public boolean isThirdPartyAcctUpsSpecified(){
                               return localThirdPartyAcctUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getThirdPartyAcctUps(){
                               return localThirdPartyAcctUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyAcctUps
                               */
                               public void setThirdPartyAcctUps(java.lang.String param){
                            localThirdPartyAcctUpsTracker = param != null;
                                   
                                            this.localThirdPartyAcctUps=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyZipcodeUps
                        */

                        
                                    protected java.lang.String localThirdPartyZipcodeUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyZipcodeUpsTracker = false ;

                           public boolean isThirdPartyZipcodeUpsSpecified(){
                               return localThirdPartyZipcodeUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getThirdPartyZipcodeUps(){
                               return localThirdPartyZipcodeUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyZipcodeUps
                               */
                               public void setThirdPartyZipcodeUps(java.lang.String param){
                            localThirdPartyZipcodeUpsTracker = param != null;
                                   
                                            this.localThirdPartyZipcodeUps=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyCountryUps
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.Country localThirdPartyCountryUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyCountryUpsTracker = false ;

                           public boolean isThirdPartyCountryUpsSpecified(){
                               return localThirdPartyCountryUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.Country
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.Country getThirdPartyCountryUps(){
                               return localThirdPartyCountryUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyCountryUps
                               */
                               public void setThirdPartyCountryUps(com.netsuite.webservices.platform.common_2017_2.types.Country param){
                            localThirdPartyCountryUpsTracker = param != null;
                                   
                                            this.localThirdPartyCountryUps=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyTypeUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeUps localThirdPartyTypeUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyTypeUpsTracker = false ;

                           public boolean isThirdPartyTypeUpsSpecified(){
                               return localThirdPartyTypeUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeUps getThirdPartyTypeUps(){
                               return localThirdPartyTypeUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyTypeUps
                               */
                               public void setThirdPartyTypeUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeUps param){
                            localThirdPartyTypeUpsTracker = param != null;
                                   
                                            this.localThirdPartyTypeUps=param;
                                    

                               }
                            

                        /**
                        * field for PartiesToTransactionUps
                        */

                        
                                    protected boolean localPartiesToTransactionUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartiesToTransactionUpsTracker = false ;

                           public boolean isPartiesToTransactionUpsSpecified(){
                               return localPartiesToTransactionUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPartiesToTransactionUps(){
                               return localPartiesToTransactionUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartiesToTransactionUps
                               */
                               public void setPartiesToTransactionUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPartiesToTransactionUpsTracker =
                                       true;
                                   
                                            this.localPartiesToTransactionUps=param;
                                    

                               }
                            

                        /**
                        * field for ExportTypeUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentExportTypeUps localExportTypeUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExportTypeUpsTracker = false ;

                           public boolean isExportTypeUpsSpecified(){
                               return localExportTypeUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentExportTypeUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentExportTypeUps getExportTypeUps(){
                               return localExportTypeUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExportTypeUps
                               */
                               public void setExportTypeUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentExportTypeUps param){
                            localExportTypeUpsTracker = param != null;
                                   
                                            this.localExportTypeUps=param;
                                    

                               }
                            

                        /**
                        * field for MethodOfTransportUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentMethodOfTransportUps localMethodOfTransportUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMethodOfTransportUpsTracker = false ;

                           public boolean isMethodOfTransportUpsSpecified(){
                               return localMethodOfTransportUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentMethodOfTransportUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentMethodOfTransportUps getMethodOfTransportUps(){
                               return localMethodOfTransportUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MethodOfTransportUps
                               */
                               public void setMethodOfTransportUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentMethodOfTransportUps param){
                            localMethodOfTransportUpsTracker = param != null;
                                   
                                            this.localMethodOfTransportUps=param;
                                    

                               }
                            

                        /**
                        * field for CarrierIdUps
                        */

                        
                                    protected java.lang.String localCarrierIdUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCarrierIdUpsTracker = false ;

                           public boolean isCarrierIdUpsSpecified(){
                               return localCarrierIdUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCarrierIdUps(){
                               return localCarrierIdUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CarrierIdUps
                               */
                               public void setCarrierIdUps(java.lang.String param){
                            localCarrierIdUpsTracker = param != null;
                                   
                                            this.localCarrierIdUps=param;
                                    

                               }
                            

                        /**
                        * field for EntryNumberUps
                        */

                        
                                    protected java.lang.String localEntryNumberUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntryNumberUpsTracker = false ;

                           public boolean isEntryNumberUpsSpecified(){
                               return localEntryNumberUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEntryNumberUps(){
                               return localEntryNumberUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntryNumberUps
                               */
                               public void setEntryNumberUps(java.lang.String param){
                            localEntryNumberUpsTracker = param != null;
                                   
                                            this.localEntryNumberUps=param;
                                    

                               }
                            

                        /**
                        * field for InbondCodeUps
                        */

                        
                                    protected java.lang.String localInbondCodeUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInbondCodeUpsTracker = false ;

                           public boolean isInbondCodeUpsSpecified(){
                               return localInbondCodeUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInbondCodeUps(){
                               return localInbondCodeUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InbondCodeUps
                               */
                               public void setInbondCodeUps(java.lang.String param){
                            localInbondCodeUpsTracker = param != null;
                                   
                                            this.localInbondCodeUps=param;
                                    

                               }
                            

                        /**
                        * field for IsRoutedExportTransactionUps
                        */

                        
                                    protected boolean localIsRoutedExportTransactionUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsRoutedExportTransactionUpsTracker = false ;

                           public boolean isIsRoutedExportTransactionUpsSpecified(){
                               return localIsRoutedExportTransactionUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsRoutedExportTransactionUps(){
                               return localIsRoutedExportTransactionUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsRoutedExportTransactionUps
                               */
                               public void setIsRoutedExportTransactionUps(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsRoutedExportTransactionUpsTracker =
                                       true;
                                   
                                            this.localIsRoutedExportTransactionUps=param;
                                    

                               }
                            

                        /**
                        * field for LicenseNumberUps
                        */

                        
                                    protected java.lang.String localLicenseNumberUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLicenseNumberUpsTracker = false ;

                           public boolean isLicenseNumberUpsSpecified(){
                               return localLicenseNumberUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLicenseNumberUps(){
                               return localLicenseNumberUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LicenseNumberUps
                               */
                               public void setLicenseNumberUps(java.lang.String param){
                            localLicenseNumberUpsTracker = param != null;
                                   
                                            this.localLicenseNumberUps=param;
                                    

                               }
                            

                        /**
                        * field for LicenseDateUps
                        */

                        
                                    protected java.util.Calendar localLicenseDateUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLicenseDateUpsTracker = false ;

                           public boolean isLicenseDateUpsSpecified(){
                               return localLicenseDateUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLicenseDateUps(){
                               return localLicenseDateUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LicenseDateUps
                               */
                               public void setLicenseDateUps(java.util.Calendar param){
                            localLicenseDateUpsTracker = param != null;
                                   
                                            this.localLicenseDateUps=param;
                                    

                               }
                            

                        /**
                        * field for LicenseExceptionUps
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentLicenseExceptionUps localLicenseExceptionUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLicenseExceptionUpsTracker = false ;

                           public boolean isLicenseExceptionUpsSpecified(){
                               return localLicenseExceptionUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentLicenseExceptionUps
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentLicenseExceptionUps getLicenseExceptionUps(){
                               return localLicenseExceptionUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LicenseExceptionUps
                               */
                               public void setLicenseExceptionUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentLicenseExceptionUps param){
                            localLicenseExceptionUpsTracker = param != null;
                                   
                                            this.localLicenseExceptionUps=param;
                                    

                               }
                            

                        /**
                        * field for EccNumberUps
                        */

                        
                                    protected java.lang.String localEccNumberUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEccNumberUpsTracker = false ;

                           public boolean isEccNumberUpsSpecified(){
                               return localEccNumberUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEccNumberUps(){
                               return localEccNumberUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EccNumberUps
                               */
                               public void setEccNumberUps(java.lang.String param){
                            localEccNumberUpsTracker = param != null;
                                   
                                            this.localEccNumberUps=param;
                                    

                               }
                            

                        /**
                        * field for RecipientTaxIdUps
                        */

                        
                                    protected java.lang.String localRecipientTaxIdUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecipientTaxIdUpsTracker = false ;

                           public boolean isRecipientTaxIdUpsSpecified(){
                               return localRecipientTaxIdUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRecipientTaxIdUps(){
                               return localRecipientTaxIdUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecipientTaxIdUps
                               */
                               public void setRecipientTaxIdUps(java.lang.String param){
                            localRecipientTaxIdUpsTracker = param != null;
                                   
                                            this.localRecipientTaxIdUps=param;
                                    

                               }
                            

                        /**
                        * field for BlanketStartDateUps
                        */

                        
                                    protected java.util.Calendar localBlanketStartDateUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBlanketStartDateUpsTracker = false ;

                           public boolean isBlanketStartDateUpsSpecified(){
                               return localBlanketStartDateUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getBlanketStartDateUps(){
                               return localBlanketStartDateUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BlanketStartDateUps
                               */
                               public void setBlanketStartDateUps(java.util.Calendar param){
                            localBlanketStartDateUpsTracker = param != null;
                                   
                                            this.localBlanketStartDateUps=param;
                                    

                               }
                            

                        /**
                        * field for BlanketEndDateUps
                        */

                        
                                    protected java.util.Calendar localBlanketEndDateUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBlanketEndDateUpsTracker = false ;

                           public boolean isBlanketEndDateUpsSpecified(){
                               return localBlanketEndDateUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getBlanketEndDateUps(){
                               return localBlanketEndDateUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BlanketEndDateUps
                               */
                               public void setBlanketEndDateUps(java.util.Calendar param){
                            localBlanketEndDateUpsTracker = param != null;
                                   
                                            this.localBlanketEndDateUps=param;
                                    

                               }
                            

                        /**
                        * field for ShipmentWeightUps
                        */

                        
                                    protected double localShipmentWeightUps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipmentWeightUpsTracker = false ;

                           public boolean isShipmentWeightUpsSpecified(){
                               return localShipmentWeightUpsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShipmentWeightUps(){
                               return localShipmentWeightUps;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipmentWeightUps
                               */
                               public void setShipmentWeightUps(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipmentWeightUpsTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShipmentWeightUps=param;
                                    

                               }
                            

                        /**
                        * field for SaturdayDeliveryFedEx
                        */

                        
                                    protected boolean localSaturdayDeliveryFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaturdayDeliveryFedExTracker = false ;

                           public boolean isSaturdayDeliveryFedExSpecified(){
                               return localSaturdayDeliveryFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSaturdayDeliveryFedEx(){
                               return localSaturdayDeliveryFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SaturdayDeliveryFedEx
                               */
                               public void setSaturdayDeliveryFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSaturdayDeliveryFedExTracker =
                                       true;
                                   
                                            this.localSaturdayDeliveryFedEx=param;
                                    

                               }
                            

                        /**
                        * field for SaturdayPickupFedex
                        */

                        
                                    protected boolean localSaturdayPickupFedex ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaturdayPickupFedexTracker = false ;

                           public boolean isSaturdayPickupFedexSpecified(){
                               return localSaturdayPickupFedexTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSaturdayPickupFedex(){
                               return localSaturdayPickupFedex;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SaturdayPickupFedex
                               */
                               public void setSaturdayPickupFedex(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSaturdayPickupFedexTracker =
                                       true;
                                   
                                            this.localSaturdayPickupFedex=param;
                                    

                               }
                            

                        /**
                        * field for SendShipNotifyEmailFedEx
                        */

                        
                                    protected boolean localSendShipNotifyEmailFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendShipNotifyEmailFedExTracker = false ;

                           public boolean isSendShipNotifyEmailFedExSpecified(){
                               return localSendShipNotifyEmailFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendShipNotifyEmailFedEx(){
                               return localSendShipNotifyEmailFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendShipNotifyEmailFedEx
                               */
                               public void setSendShipNotifyEmailFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendShipNotifyEmailFedExTracker =
                                       true;
                                   
                                            this.localSendShipNotifyEmailFedEx=param;
                                    

                               }
                            

                        /**
                        * field for SendBackupEmailFedEx
                        */

                        
                                    protected boolean localSendBackupEmailFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendBackupEmailFedExTracker = false ;

                           public boolean isSendBackupEmailFedExSpecified(){
                               return localSendBackupEmailFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendBackupEmailFedEx(){
                               return localSendBackupEmailFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendBackupEmailFedEx
                               */
                               public void setSendBackupEmailFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendBackupEmailFedExTracker =
                                       true;
                                   
                                            this.localSendBackupEmailFedEx=param;
                                    

                               }
                            

                        /**
                        * field for SignatureHomeDeliveryFedEx
                        */

                        
                                    protected boolean localSignatureHomeDeliveryFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSignatureHomeDeliveryFedExTracker = false ;

                           public boolean isSignatureHomeDeliveryFedExSpecified(){
                               return localSignatureHomeDeliveryFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSignatureHomeDeliveryFedEx(){
                               return localSignatureHomeDeliveryFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SignatureHomeDeliveryFedEx
                               */
                               public void setSignatureHomeDeliveryFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSignatureHomeDeliveryFedExTracker =
                                       true;
                                   
                                            this.localSignatureHomeDeliveryFedEx=param;
                                    

                               }
                            

                        /**
                        * field for ShipNotifyEmailAddressFedEx
                        */

                        
                                    protected java.lang.String localShipNotifyEmailAddressFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipNotifyEmailAddressFedExTracker = false ;

                           public boolean isShipNotifyEmailAddressFedExSpecified(){
                               return localShipNotifyEmailAddressFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShipNotifyEmailAddressFedEx(){
                               return localShipNotifyEmailAddressFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipNotifyEmailAddressFedEx
                               */
                               public void setShipNotifyEmailAddressFedEx(java.lang.String param){
                            localShipNotifyEmailAddressFedExTracker = param != null;
                                   
                                            this.localShipNotifyEmailAddressFedEx=param;
                                    

                               }
                            

                        /**
                        * field for BackupEmailAddressFedEx
                        */

                        
                                    protected java.lang.String localBackupEmailAddressFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBackupEmailAddressFedExTracker = false ;

                           public boolean isBackupEmailAddressFedExSpecified(){
                               return localBackupEmailAddressFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBackupEmailAddressFedEx(){
                               return localBackupEmailAddressFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BackupEmailAddressFedEx
                               */
                               public void setBackupEmailAddressFedEx(java.lang.String param){
                            localBackupEmailAddressFedExTracker = param != null;
                                   
                                            this.localBackupEmailAddressFedEx=param;
                                    

                               }
                            

                        /**
                        * field for ShipDateFedEx
                        */

                        
                                    protected java.util.Calendar localShipDateFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipDateFedExTracker = false ;

                           public boolean isShipDateFedExSpecified(){
                               return localShipDateFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getShipDateFedEx(){
                               return localShipDateFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipDateFedEx
                               */
                               public void setShipDateFedEx(java.util.Calendar param){
                            localShipDateFedExTracker = param != null;
                                   
                                            this.localShipDateFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HomeDeliveryTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHomeDeliveryTypeFedEx localHomeDeliveryTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHomeDeliveryTypeFedExTracker = false ;

                           public boolean isHomeDeliveryTypeFedExSpecified(){
                               return localHomeDeliveryTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHomeDeliveryTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHomeDeliveryTypeFedEx getHomeDeliveryTypeFedEx(){
                               return localHomeDeliveryTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HomeDeliveryTypeFedEx
                               */
                               public void setHomeDeliveryTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHomeDeliveryTypeFedEx param){
                            localHomeDeliveryTypeFedExTracker = param != null;
                                   
                                            this.localHomeDeliveryTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HomeDeliveryDateFedEx
                        */

                        
                                    protected java.util.Calendar localHomeDeliveryDateFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHomeDeliveryDateFedExTracker = false ;

                           public boolean isHomeDeliveryDateFedExSpecified(){
                               return localHomeDeliveryDateFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getHomeDeliveryDateFedEx(){
                               return localHomeDeliveryDateFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HomeDeliveryDateFedEx
                               */
                               public void setHomeDeliveryDateFedEx(java.util.Calendar param){
                            localHomeDeliveryDateFedExTracker = param != null;
                                   
                                            this.localHomeDeliveryDateFedEx=param;
                                    

                               }
                            

                        /**
                        * field for BookingConfirmationNumFedEx
                        */

                        
                                    protected java.lang.String localBookingConfirmationNumFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBookingConfirmationNumFedExTracker = false ;

                           public boolean isBookingConfirmationNumFedExSpecified(){
                               return localBookingConfirmationNumFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBookingConfirmationNumFedEx(){
                               return localBookingConfirmationNumFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BookingConfirmationNumFedEx
                               */
                               public void setBookingConfirmationNumFedEx(java.lang.String param){
                            localBookingConfirmationNumFedExTracker = param != null;
                                   
                                            this.localBookingConfirmationNumFedEx=param;
                                    

                               }
                            

                        /**
                        * field for IntlExemptionNumFedEx
                        */

                        
                                    protected java.lang.String localIntlExemptionNumFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntlExemptionNumFedExTracker = false ;

                           public boolean isIntlExemptionNumFedExSpecified(){
                               return localIntlExemptionNumFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIntlExemptionNumFedEx(){
                               return localIntlExemptionNumFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntlExemptionNumFedEx
                               */
                               public void setIntlExemptionNumFedEx(java.lang.String param){
                            localIntlExemptionNumFedExTracker = param != null;
                                   
                                            this.localIntlExemptionNumFedEx=param;
                                    

                               }
                            

                        /**
                        * field for B13AFilingOptionFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentB13AFilingOptionFedEx localB13AFilingOptionFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localB13AFilingOptionFedExTracker = false ;

                           public boolean isB13AFilingOptionFedExSpecified(){
                               return localB13AFilingOptionFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentB13AFilingOptionFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentB13AFilingOptionFedEx getB13AFilingOptionFedEx(){
                               return localB13AFilingOptionFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param B13AFilingOptionFedEx
                               */
                               public void setB13AFilingOptionFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentB13AFilingOptionFedEx param){
                            localB13AFilingOptionFedExTracker = param != null;
                                   
                                            this.localB13AFilingOptionFedEx=param;
                                    

                               }
                            

                        /**
                        * field for B13AStatementDataFedEx
                        */

                        
                                    protected java.lang.String localB13AStatementDataFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localB13AStatementDataFedExTracker = false ;

                           public boolean isB13AStatementDataFedExSpecified(){
                               return localB13AStatementDataFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getB13AStatementDataFedEx(){
                               return localB13AStatementDataFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param B13AStatementDataFedEx
                               */
                               public void setB13AStatementDataFedEx(java.lang.String param){
                            localB13AStatementDataFedExTracker = param != null;
                                   
                                            this.localB13AStatementDataFedEx=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyAcctFedEx
                        */

                        
                                    protected java.lang.String localThirdPartyAcctFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyAcctFedExTracker = false ;

                           public boolean isThirdPartyAcctFedExSpecified(){
                               return localThirdPartyAcctFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getThirdPartyAcctFedEx(){
                               return localThirdPartyAcctFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyAcctFedEx
                               */
                               public void setThirdPartyAcctFedEx(java.lang.String param){
                            localThirdPartyAcctFedExTracker = param != null;
                                   
                                            this.localThirdPartyAcctFedEx=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyCountryFedEx
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.Country localThirdPartyCountryFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyCountryFedExTracker = false ;

                           public boolean isThirdPartyCountryFedExSpecified(){
                               return localThirdPartyCountryFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.Country
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.Country getThirdPartyCountryFedEx(){
                               return localThirdPartyCountryFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyCountryFedEx
                               */
                               public void setThirdPartyCountryFedEx(com.netsuite.webservices.platform.common_2017_2.types.Country param){
                            localThirdPartyCountryFedExTracker = param != null;
                                   
                                            this.localThirdPartyCountryFedEx=param;
                                    

                               }
                            

                        /**
                        * field for ThirdPartyTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeFedEx localThirdPartyTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThirdPartyTypeFedExTracker = false ;

                           public boolean isThirdPartyTypeFedExSpecified(){
                               return localThirdPartyTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeFedEx getThirdPartyTypeFedEx(){
                               return localThirdPartyTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ThirdPartyTypeFedEx
                               */
                               public void setThirdPartyTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeFedEx param){
                            localThirdPartyTypeFedExTracker = param != null;
                                   
                                            this.localThirdPartyTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for ShipmentWeightFedEx
                        */

                        
                                    protected double localShipmentWeightFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipmentWeightFedExTracker = false ;

                           public boolean isShipmentWeightFedExSpecified(){
                               return localShipmentWeightFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShipmentWeightFedEx(){
                               return localShipmentWeightFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipmentWeightFedEx
                               */
                               public void setShipmentWeightFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipmentWeightFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShipmentWeightFedEx=param;
                                    

                               }
                            

                        /**
                        * field for TermsOfSaleFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentTermsOfSaleFedEx localTermsOfSaleFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTermsOfSaleFedExTracker = false ;

                           public boolean isTermsOfSaleFedExSpecified(){
                               return localTermsOfSaleFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentTermsOfSaleFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentTermsOfSaleFedEx getTermsOfSaleFedEx(){
                               return localTermsOfSaleFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TermsOfSaleFedEx
                               */
                               public void setTermsOfSaleFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentTermsOfSaleFedEx param){
                            localTermsOfSaleFedExTracker = param != null;
                                   
                                            this.localTermsOfSaleFedEx=param;
                                    

                               }
                            

                        /**
                        * field for TermsFreightChargeFedEx
                        */

                        
                                    protected double localTermsFreightChargeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTermsFreightChargeFedExTracker = false ;

                           public boolean isTermsFreightChargeFedExSpecified(){
                               return localTermsFreightChargeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTermsFreightChargeFedEx(){
                               return localTermsFreightChargeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TermsFreightChargeFedEx
                               */
                               public void setTermsFreightChargeFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTermsFreightChargeFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTermsFreightChargeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for TermsInsuranceChargeFedEx
                        */

                        
                                    protected double localTermsInsuranceChargeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTermsInsuranceChargeFedExTracker = false ;

                           public boolean isTermsInsuranceChargeFedExSpecified(){
                               return localTermsInsuranceChargeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTermsInsuranceChargeFedEx(){
                               return localTermsInsuranceChargeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TermsInsuranceChargeFedEx
                               */
                               public void setTermsInsuranceChargeFedEx(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTermsInsuranceChargeFedExTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTermsInsuranceChargeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for InsideDeliveryFedEx
                        */

                        
                                    protected boolean localInsideDeliveryFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsideDeliveryFedExTracker = false ;

                           public boolean isInsideDeliveryFedExSpecified(){
                               return localInsideDeliveryFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getInsideDeliveryFedEx(){
                               return localInsideDeliveryFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsideDeliveryFedEx
                               */
                               public void setInsideDeliveryFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localInsideDeliveryFedExTracker =
                                       true;
                                   
                                            this.localInsideDeliveryFedEx=param;
                                    

                               }
                            

                        /**
                        * field for InsidePickupFedEx
                        */

                        
                                    protected boolean localInsidePickupFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsidePickupFedExTracker = false ;

                           public boolean isInsidePickupFedExSpecified(){
                               return localInsidePickupFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getInsidePickupFedEx(){
                               return localInsidePickupFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsidePickupFedEx
                               */
                               public void setInsidePickupFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localInsidePickupFedExTracker =
                                       true;
                                   
                                            this.localInsidePickupFedEx=param;
                                    

                               }
                            

                        /**
                        * field for AncillaryEndorsementFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAncillaryEndorsementFedEx localAncillaryEndorsementFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAncillaryEndorsementFedExTracker = false ;

                           public boolean isAncillaryEndorsementFedExSpecified(){
                               return localAncillaryEndorsementFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAncillaryEndorsementFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAncillaryEndorsementFedEx getAncillaryEndorsementFedEx(){
                               return localAncillaryEndorsementFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AncillaryEndorsementFedEx
                               */
                               public void setAncillaryEndorsementFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAncillaryEndorsementFedEx param){
                            localAncillaryEndorsementFedExTracker = param != null;
                                   
                                            this.localAncillaryEndorsementFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HoldAtLocationFedEx
                        */

                        
                                    protected boolean localHoldAtLocationFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHoldAtLocationFedExTracker = false ;

                           public boolean isHoldAtLocationFedExSpecified(){
                               return localHoldAtLocationFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getHoldAtLocationFedEx(){
                               return localHoldAtLocationFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HoldAtLocationFedEx
                               */
                               public void setHoldAtLocationFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localHoldAtLocationFedExTracker =
                                       true;
                                   
                                            this.localHoldAtLocationFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalPhoneFedEx
                        */

                        
                                    protected java.lang.String localHalPhoneFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalPhoneFedExTracker = false ;

                           public boolean isHalPhoneFedExSpecified(){
                               return localHalPhoneFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalPhoneFedEx(){
                               return localHalPhoneFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalPhoneFedEx
                               */
                               public void setHalPhoneFedEx(java.lang.String param){
                            localHalPhoneFedExTracker = param != null;
                                   
                                            this.localHalPhoneFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalAddr1FedEx
                        */

                        
                                    protected java.lang.String localHalAddr1FedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalAddr1FedExTracker = false ;

                           public boolean isHalAddr1FedExSpecified(){
                               return localHalAddr1FedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalAddr1FedEx(){
                               return localHalAddr1FedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalAddr1FedEx
                               */
                               public void setHalAddr1FedEx(java.lang.String param){
                            localHalAddr1FedExTracker = param != null;
                                   
                                            this.localHalAddr1FedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalAddr2FedEx
                        */

                        
                                    protected java.lang.String localHalAddr2FedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalAddr2FedExTracker = false ;

                           public boolean isHalAddr2FedExSpecified(){
                               return localHalAddr2FedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalAddr2FedEx(){
                               return localHalAddr2FedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalAddr2FedEx
                               */
                               public void setHalAddr2FedEx(java.lang.String param){
                            localHalAddr2FedExTracker = param != null;
                                   
                                            this.localHalAddr2FedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalAddr3FedEx
                        */

                        
                                    protected java.lang.String localHalAddr3FedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalAddr3FedExTracker = false ;

                           public boolean isHalAddr3FedExSpecified(){
                               return localHalAddr3FedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalAddr3FedEx(){
                               return localHalAddr3FedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalAddr3FedEx
                               */
                               public void setHalAddr3FedEx(java.lang.String param){
                            localHalAddr3FedExTracker = param != null;
                                   
                                            this.localHalAddr3FedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalCityFedEx
                        */

                        
                                    protected java.lang.String localHalCityFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalCityFedExTracker = false ;

                           public boolean isHalCityFedExSpecified(){
                               return localHalCityFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalCityFedEx(){
                               return localHalCityFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalCityFedEx
                               */
                               public void setHalCityFedEx(java.lang.String param){
                            localHalCityFedExTracker = param != null;
                                   
                                            this.localHalCityFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalZipFedEx
                        */

                        
                                    protected java.lang.String localHalZipFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalZipFedExTracker = false ;

                           public boolean isHalZipFedExSpecified(){
                               return localHalZipFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalZipFedEx(){
                               return localHalZipFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalZipFedEx
                               */
                               public void setHalZipFedEx(java.lang.String param){
                            localHalZipFedExTracker = param != null;
                                   
                                            this.localHalZipFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalStateFedEx
                        */

                        
                                    protected java.lang.String localHalStateFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalStateFedExTracker = false ;

                           public boolean isHalStateFedExSpecified(){
                               return localHalStateFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalStateFedEx(){
                               return localHalStateFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalStateFedEx
                               */
                               public void setHalStateFedEx(java.lang.String param){
                            localHalStateFedExTracker = param != null;
                                   
                                            this.localHalStateFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HalCountryFedEx
                        */

                        
                                    protected java.lang.String localHalCountryFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHalCountryFedExTracker = false ;

                           public boolean isHalCountryFedExSpecified(){
                               return localHalCountryFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHalCountryFedEx(){
                               return localHalCountryFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HalCountryFedEx
                               */
                               public void setHalCountryFedEx(java.lang.String param){
                            localHalCountryFedExTracker = param != null;
                                   
                                            this.localHalCountryFedEx=param;
                                    

                               }
                            

                        /**
                        * field for HazmatTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHazmatTypeFedEx localHazmatTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatTypeFedExTracker = false ;

                           public boolean isHazmatTypeFedExSpecified(){
                               return localHazmatTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHazmatTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHazmatTypeFedEx getHazmatTypeFedEx(){
                               return localHazmatTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatTypeFedEx
                               */
                               public void setHazmatTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHazmatTypeFedEx param){
                            localHazmatTypeFedExTracker = param != null;
                                   
                                            this.localHazmatTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for AccessibilityTypeFedEx
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAccessibilityTypeFedEx localAccessibilityTypeFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccessibilityTypeFedExTracker = false ;

                           public boolean isAccessibilityTypeFedExSpecified(){
                               return localAccessibilityTypeFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAccessibilityTypeFedEx
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAccessibilityTypeFedEx getAccessibilityTypeFedEx(){
                               return localAccessibilityTypeFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccessibilityTypeFedEx
                               */
                               public void setAccessibilityTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAccessibilityTypeFedEx param){
                            localAccessibilityTypeFedExTracker = param != null;
                                   
                                            this.localAccessibilityTypeFedEx=param;
                                    

                               }
                            

                        /**
                        * field for IsCargoAircraftOnlyFedEx
                        */

                        
                                    protected boolean localIsCargoAircraftOnlyFedEx ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsCargoAircraftOnlyFedExTracker = false ;

                           public boolean isIsCargoAircraftOnlyFedExSpecified(){
                               return localIsCargoAircraftOnlyFedExTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsCargoAircraftOnlyFedEx(){
                               return localIsCargoAircraftOnlyFedEx;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsCargoAircraftOnlyFedEx
                               */
                               public void setIsCargoAircraftOnlyFedEx(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsCargoAircraftOnlyFedExTracker =
                                       true;
                                   
                                            this.localIsCargoAircraftOnlyFedEx=param;
                                    

                               }
                            

                        /**
                        * field for TranDate
                        */

                        
                                    protected java.util.Calendar localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTranDate(){
                               return localTranDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranDate
                               */
                               public void setTranDate(java.util.Calendar param){
                            localTranDateTracker = param != null;
                                   
                                            this.localTranDate=param;
                                    

                               }
                            

                        /**
                        * field for TranId
                        */

                        
                                    protected java.lang.String localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTranId(){
                               return localTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranId
                               */
                               public void setTranId(java.lang.String param){
                            localTranIdTracker = param != null;
                                   
                                            this.localTranId=param;
                                    

                               }
                            

                        /**
                        * field for ShipMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipMethodTracker = false ;

                           public boolean isShipMethodSpecified(){
                               return localShipMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipMethod(){
                               return localShipMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipMethod
                               */
                               public void setShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipMethodTracker = param != null;
                                   
                                            this.localShipMethod=param;
                                    

                               }
                            

                        /**
                        * field for GenerateIntegratedShipperLabel
                        */

                        
                                    protected boolean localGenerateIntegratedShipperLabel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGenerateIntegratedShipperLabelTracker = false ;

                           public boolean isGenerateIntegratedShipperLabelSpecified(){
                               return localGenerateIntegratedShipperLabelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getGenerateIntegratedShipperLabel(){
                               return localGenerateIntegratedShipperLabel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GenerateIntegratedShipperLabel
                               */
                               public void setGenerateIntegratedShipperLabel(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localGenerateIntegratedShipperLabelTracker =
                                       true;
                                   
                                            this.localGenerateIntegratedShipperLabel=param;
                                    

                               }
                            

                        /**
                        * field for ShippingCost
                        */

                        
                                    protected double localShippingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingCostTracker = false ;

                           public boolean isShippingCostSpecified(){
                               return localShippingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingCost(){
                               return localShippingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingCost
                               */
                               public void setShippingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingCost=param;
                                    

                               }
                            

                        /**
                        * field for HandlingCost
                        */

                        
                                    protected double localHandlingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingCostTracker = false ;

                           public boolean isHandlingCostSpecified(){
                               return localHandlingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingCost(){
                               return localHandlingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingCost
                               */
                               public void setHandlingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingCost=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for TransferLocation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTransferLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransferLocationTracker = false ;

                           public boolean isTransferLocationSpecified(){
                               return localTransferLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTransferLocation(){
                               return localTransferLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransferLocation
                               */
                               public void setTransferLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTransferLocationTracker = param != null;
                                   
                                            this.localTransferLocation=param;
                                    

                               }
                            

                        /**
                        * field for PackageList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageList localPackageList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageListTracker = false ;

                           public boolean isPackageListSpecified(){
                               return localPackageListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageList getPackageList(){
                               return localPackageList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageList
                               */
                               public void setPackageList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageList param){
                            localPackageListTracker = param != null;
                                   
                                            this.localPackageList=param;
                                    

                               }
                            

                        /**
                        * field for PackageUpsList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUpsList localPackageUpsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageUpsListTracker = false ;

                           public boolean isPackageUpsListSpecified(){
                               return localPackageUpsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUpsList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUpsList getPackageUpsList(){
                               return localPackageUpsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageUpsList
                               */
                               public void setPackageUpsList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUpsList param){
                            localPackageUpsListTracker = param != null;
                                   
                                            this.localPackageUpsList=param;
                                    

                               }
                            

                        /**
                        * field for PackageUspsList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUspsList localPackageUspsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageUspsListTracker = false ;

                           public boolean isPackageUspsListSpecified(){
                               return localPackageUspsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUspsList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUspsList getPackageUspsList(){
                               return localPackageUspsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageUspsList
                               */
                               public void setPackageUspsList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUspsList param){
                            localPackageUspsListTracker = param != null;
                                   
                                            this.localPackageUspsList=param;
                                    

                               }
                            

                        /**
                        * field for PackageFedExList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedExList localPackageFedExList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPackageFedExListTracker = false ;

                           public boolean isPackageFedExListSpecified(){
                               return localPackageFedExListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedExList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedExList getPackageFedExList(){
                               return localPackageFedExList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PackageFedExList
                               */
                               public void setPackageFedExList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedExList param){
                            localPackageFedExListTracker = param != null;
                                   
                                            this.localPackageFedExList=param;
                                    

                               }
                            

                        /**
                        * field for ItemList
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItemList localItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemListTracker = false ;

                           public boolean isItemListSpecified(){
                               return localItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItemList
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItemList getItemList(){
                               return localItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemList
                               */
                               public void setItemList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItemList param){
                            localItemListTracker = param != null;
                                   
                                            this.localItemList=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemFulfillment",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemFulfillment",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localPostingPeriodTracker){
                                            if (localPostingPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("postingPeriod cannot be null!!");
                                            }
                                           localPostingPeriod.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","postingPeriod"),
                                               xmlWriter);
                                        } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localCreatedFromTracker){
                                            if (localCreatedFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                            }
                                           localCreatedFrom.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFrom"),
                                               xmlWriter);
                                        } if (localRequestedByTracker){
                                            if (localRequestedBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("requestedBy cannot be null!!");
                                            }
                                           localRequestedBy.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","requestedBy"),
                                               xmlWriter);
                                        } if (localCreatedFromShipGroupTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdFromShipGroup", xmlWriter);
                             
                                               if (localCreatedFromShipGroup==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("createdFromShipGroup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedFromShipGroup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPartnerTracker){
                                            if (localPartner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                            }
                                           localPartner.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partner"),
                                               xmlWriter);
                                        } if (localShippingAddressTracker){
                                            if (localShippingAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingAddress cannot be null!!");
                                            }
                                           localShippingAddress.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingAddress"),
                                               xmlWriter);
                                        } if (localPickedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pickedDate", xmlWriter);
                             

                                          if (localPickedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("pickedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPickedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPackedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "packedDate", xmlWriter);
                             

                                          if (localPackedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("packedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippedDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippedDate", xmlWriter);
                             

                                          if (localShippedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shippedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipIsResidentialTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipIsResidential", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipIsResidential cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipIsResidential));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipAddressListTracker){
                                            if (localShipAddressList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddressList cannot be null!!");
                                            }
                                           localShipAddressList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipAddressList"),
                                               xmlWriter);
                                        } if (localShipStatusTracker){
                                            if (localShipStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipStatus cannot be null!!");
                                            }
                                           localShipStatus.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipStatus"),
                                               xmlWriter);
                                        } if (localSaturdayDeliveryUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "saturdayDeliveryUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("saturdayDeliveryUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaturdayDeliveryUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSendShipNotifyEmailUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendShipNotifyEmailUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendShipNotifyEmailUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendShipNotifyEmailUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSendBackupEmailUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendBackupEmailUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendBackupEmailUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendBackupEmailUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipNotifyEmailAddressUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipNotifyEmailAddressUps", xmlWriter);
                             

                                          if (localShipNotifyEmailAddressUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailAddressUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShipNotifyEmailAddressUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipNotifyEmailAddress2UpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipNotifyEmailAddress2Ups", xmlWriter);
                             

                                          if (localShipNotifyEmailAddress2Ups==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailAddress2Ups cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShipNotifyEmailAddress2Ups);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBackupEmailAddressUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "backupEmailAddressUps", xmlWriter);
                             

                                          if (localBackupEmailAddressUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("backupEmailAddressUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBackupEmailAddressUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipNotifyEmailMessageUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipNotifyEmailMessageUps", xmlWriter);
                             

                                          if (localShipNotifyEmailMessageUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailMessageUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShipNotifyEmailMessageUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localThirdPartyAcctUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "thirdPartyAcctUps", xmlWriter);
                             

                                          if (localThirdPartyAcctUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("thirdPartyAcctUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localThirdPartyAcctUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localThirdPartyZipcodeUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "thirdPartyZipcodeUps", xmlWriter);
                             

                                          if (localThirdPartyZipcodeUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("thirdPartyZipcodeUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localThirdPartyZipcodeUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localThirdPartyCountryUpsTracker){
                                            if (localThirdPartyCountryUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("thirdPartyCountryUps cannot be null!!");
                                            }
                                           localThirdPartyCountryUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyCountryUps"),
                                               xmlWriter);
                                        } if (localThirdPartyTypeUpsTracker){
                                            if (localThirdPartyTypeUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("thirdPartyTypeUps cannot be null!!");
                                            }
                                           localThirdPartyTypeUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyTypeUps"),
                                               xmlWriter);
                                        } if (localPartiesToTransactionUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "partiesToTransactionUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("partiesToTransactionUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPartiesToTransactionUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExportTypeUpsTracker){
                                            if (localExportTypeUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("exportTypeUps cannot be null!!");
                                            }
                                           localExportTypeUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","exportTypeUps"),
                                               xmlWriter);
                                        } if (localMethodOfTransportUpsTracker){
                                            if (localMethodOfTransportUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("methodOfTransportUps cannot be null!!");
                                            }
                                           localMethodOfTransportUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","methodOfTransportUps"),
                                               xmlWriter);
                                        } if (localCarrierIdUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "carrierIdUps", xmlWriter);
                             

                                          if (localCarrierIdUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("carrierIdUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCarrierIdUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEntryNumberUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "entryNumberUps", xmlWriter);
                             

                                          if (localEntryNumberUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("entryNumberUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEntryNumberUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInbondCodeUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "inbondCodeUps", xmlWriter);
                             

                                          if (localInbondCodeUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("inbondCodeUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localInbondCodeUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsRoutedExportTransactionUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isRoutedExportTransactionUps", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isRoutedExportTransactionUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsRoutedExportTransactionUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLicenseNumberUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "licenseNumberUps", xmlWriter);
                             

                                          if (localLicenseNumberUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("licenseNumberUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLicenseNumberUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLicenseDateUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "licenseDateUps", xmlWriter);
                             

                                          if (localLicenseDateUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("licenseDateUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLicenseDateUps));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLicenseExceptionUpsTracker){
                                            if (localLicenseExceptionUps==null){
                                                 throw new org.apache.axis2.databinding.ADBException("licenseExceptionUps cannot be null!!");
                                            }
                                           localLicenseExceptionUps.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","licenseExceptionUps"),
                                               xmlWriter);
                                        } if (localEccNumberUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "eccNumberUps", xmlWriter);
                             

                                          if (localEccNumberUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("eccNumberUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEccNumberUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecipientTaxIdUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recipientTaxIdUps", xmlWriter);
                             

                                          if (localRecipientTaxIdUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("recipientTaxIdUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRecipientTaxIdUps);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBlanketStartDateUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "blanketStartDateUps", xmlWriter);
                             

                                          if (localBlanketStartDateUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("blanketStartDateUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBlanketStartDateUps));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBlanketEndDateUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "blanketEndDateUps", xmlWriter);
                             

                                          if (localBlanketEndDateUps==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("blanketEndDateUps cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBlanketEndDateUps));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipmentWeightUpsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipmentWeightUps", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShipmentWeightUps)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipmentWeightUps cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentWeightUps));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSaturdayDeliveryFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "saturdayDeliveryFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("saturdayDeliveryFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaturdayDeliveryFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSaturdayPickupFedexTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "saturdayPickupFedex", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("saturdayPickupFedex cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaturdayPickupFedex));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSendShipNotifyEmailFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendShipNotifyEmailFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendShipNotifyEmailFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendShipNotifyEmailFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSendBackupEmailFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendBackupEmailFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendBackupEmailFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendBackupEmailFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSignatureHomeDeliveryFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "signatureHomeDeliveryFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("signatureHomeDeliveryFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSignatureHomeDeliveryFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipNotifyEmailAddressFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipNotifyEmailAddressFedEx", xmlWriter);
                             

                                          if (localShipNotifyEmailAddressFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailAddressFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShipNotifyEmailAddressFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBackupEmailAddressFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "backupEmailAddressFedEx", xmlWriter);
                             

                                          if (localBackupEmailAddressFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("backupEmailAddressFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBackupEmailAddressFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipDateFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipDateFedEx", xmlWriter);
                             

                                          if (localShipDateFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shipDateFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipDateFedEx));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHomeDeliveryTypeFedExTracker){
                                            if (localHomeDeliveryTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("homeDeliveryTypeFedEx cannot be null!!");
                                            }
                                           localHomeDeliveryTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","homeDeliveryTypeFedEx"),
                                               xmlWriter);
                                        } if (localHomeDeliveryDateFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "homeDeliveryDateFedEx", xmlWriter);
                             

                                          if (localHomeDeliveryDateFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("homeDeliveryDateFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHomeDeliveryDateFedEx));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBookingConfirmationNumFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "bookingConfirmationNumFedEx", xmlWriter);
                             

                                          if (localBookingConfirmationNumFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("bookingConfirmationNumFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBookingConfirmationNumFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIntlExemptionNumFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "intlExemptionNumFedEx", xmlWriter);
                             

                                          if (localIntlExemptionNumFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("intlExemptionNumFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIntlExemptionNumFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localB13AFilingOptionFedExTracker){
                                            if (localB13AFilingOptionFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("b13aFilingOptionFedEx cannot be null!!");
                                            }
                                           localB13AFilingOptionFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","b13aFilingOptionFedEx"),
                                               xmlWriter);
                                        } if (localB13AStatementDataFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "b13aStatementDataFedEx", xmlWriter);
                             

                                          if (localB13AStatementDataFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("b13aStatementDataFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localB13AStatementDataFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localThirdPartyAcctFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "thirdPartyAcctFedEx", xmlWriter);
                             

                                          if (localThirdPartyAcctFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("thirdPartyAcctFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localThirdPartyAcctFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localThirdPartyCountryFedExTracker){
                                            if (localThirdPartyCountryFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("thirdPartyCountryFedEx cannot be null!!");
                                            }
                                           localThirdPartyCountryFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyCountryFedEx"),
                                               xmlWriter);
                                        } if (localThirdPartyTypeFedExTracker){
                                            if (localThirdPartyTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("thirdPartyTypeFedEx cannot be null!!");
                                            }
                                           localThirdPartyTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyTypeFedEx"),
                                               xmlWriter);
                                        } if (localShipmentWeightFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipmentWeightFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShipmentWeightFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipmentWeightFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentWeightFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTermsOfSaleFedExTracker){
                                            if (localTermsOfSaleFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("termsOfSaleFedEx cannot be null!!");
                                            }
                                           localTermsOfSaleFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","termsOfSaleFedEx"),
                                               xmlWriter);
                                        } if (localTermsFreightChargeFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "termsFreightChargeFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTermsFreightChargeFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("termsFreightChargeFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTermsFreightChargeFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTermsInsuranceChargeFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "termsInsuranceChargeFedEx", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTermsInsuranceChargeFedEx)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("termsInsuranceChargeFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTermsInsuranceChargeFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsideDeliveryFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "insideDeliveryFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("insideDeliveryFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsideDeliveryFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsidePickupFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "insidePickupFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("insidePickupFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsidePickupFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAncillaryEndorsementFedExTracker){
                                            if (localAncillaryEndorsementFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ancillaryEndorsementFedEx cannot be null!!");
                                            }
                                           localAncillaryEndorsementFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ancillaryEndorsementFedEx"),
                                               xmlWriter);
                                        } if (localHoldAtLocationFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "holdAtLocationFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("holdAtLocationFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHoldAtLocationFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalPhoneFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halPhoneFedEx", xmlWriter);
                             

                                          if (localHalPhoneFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halPhoneFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalPhoneFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalAddr1FedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halAddr1FedEx", xmlWriter);
                             

                                          if (localHalAddr1FedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halAddr1FedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalAddr1FedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalAddr2FedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halAddr2FedEx", xmlWriter);
                             

                                          if (localHalAddr2FedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halAddr2FedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalAddr2FedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalAddr3FedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halAddr3FedEx", xmlWriter);
                             

                                          if (localHalAddr3FedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halAddr3FedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalAddr3FedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalCityFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halCityFedEx", xmlWriter);
                             

                                          if (localHalCityFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halCityFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalCityFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalZipFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halZipFedEx", xmlWriter);
                             

                                          if (localHalZipFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halZipFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalZipFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalStateFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halStateFedEx", xmlWriter);
                             

                                          if (localHalStateFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halStateFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalStateFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHalCountryFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "halCountryFedEx", xmlWriter);
                             

                                          if (localHalCountryFedEx==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("halCountryFedEx cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHalCountryFedEx);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHazmatTypeFedExTracker){
                                            if (localHazmatTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hazmatTypeFedEx cannot be null!!");
                                            }
                                           localHazmatTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","hazmatTypeFedEx"),
                                               xmlWriter);
                                        } if (localAccessibilityTypeFedExTracker){
                                            if (localAccessibilityTypeFedEx==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accessibilityTypeFedEx cannot be null!!");
                                            }
                                           localAccessibilityTypeFedEx.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accessibilityTypeFedEx"),
                                               xmlWriter);
                                        } if (localIsCargoAircraftOnlyFedExTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isCargoAircraftOnlyFedEx", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isCargoAircraftOnlyFedEx cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsCargoAircraftOnlyFedEx));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranDate", xmlWriter);
                             

                                          if (localTranDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranIdTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranId", xmlWriter);
                             

                                          if (localTranId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTranId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipMethodTracker){
                                            if (localShipMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipMethod cannot be null!!");
                                            }
                                           localShipMethod.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipMethod"),
                                               xmlWriter);
                                        } if (localGenerateIntegratedShipperLabelTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "generateIntegratedShipperLabel", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("generateIntegratedShipperLabel cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenerateIntegratedShipperLabel));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingCostTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingCostTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMemoTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTransferLocationTracker){
                                            if (localTransferLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transferLocation cannot be null!!");
                                            }
                                           localTransferLocation.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","transferLocation"),
                                               xmlWriter);
                                        } if (localPackageListTracker){
                                            if (localPackageList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packageList cannot be null!!");
                                            }
                                           localPackageList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageList"),
                                               xmlWriter);
                                        } if (localPackageUpsListTracker){
                                            if (localPackageUpsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packageUpsList cannot be null!!");
                                            }
                                           localPackageUpsList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageUpsList"),
                                               xmlWriter);
                                        } if (localPackageUspsListTracker){
                                            if (localPackageUspsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packageUspsList cannot be null!!");
                                            }
                                           localPackageUspsList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageUspsList"),
                                               xmlWriter);
                                        } if (localPackageFedExListTracker){
                                            if (localPackageFedExList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("packageFedExList cannot be null!!");
                                            }
                                           localPackageFedExList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageFedExList"),
                                               xmlWriter);
                                        } if (localItemListTracker){
                                            if (localItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                            }
                                           localItemList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemList"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ItemFulfillment"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localPostingPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "postingPeriod"));
                            
                            
                                    if (localPostingPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("postingPeriod cannot be null!!");
                                    }
                                    elementList.add(localPostingPeriod);
                                } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localCreatedFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFrom"));
                            
                            
                                    if (localCreatedFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFrom cannot be null!!");
                                    }
                                    elementList.add(localCreatedFrom);
                                } if (localRequestedByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "requestedBy"));
                            
                            
                                    if (localRequestedBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("requestedBy cannot be null!!");
                                    }
                                    elementList.add(localRequestedBy);
                                } if (localCreatedFromShipGroupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFromShipGroup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedFromShipGroup));
                            } if (localPartnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "partner"));
                            
                            
                                    if (localPartner==null){
                                         throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    }
                                    elementList.add(localPartner);
                                } if (localShippingAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingAddress"));
                            
                            
                                    if (localShippingAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingAddress cannot be null!!");
                                    }
                                    elementList.add(localShippingAddress);
                                } if (localPickedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "pickedDate"));
                                 
                                        if (localPickedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPickedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("pickedDate cannot be null!!");
                                        }
                                    } if (localPackedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packedDate"));
                                 
                                        if (localPackedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPackedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("packedDate cannot be null!!");
                                        }
                                    } if (localShippedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippedDate"));
                                 
                                        if (localShippedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shippedDate cannot be null!!");
                                        }
                                    } if (localShipIsResidentialTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipIsResidential"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipIsResidential));
                            } if (localShipAddressListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipAddressList"));
                            
                            
                                    if (localShipAddressList==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddressList cannot be null!!");
                                    }
                                    elementList.add(localShipAddressList);
                                } if (localShipStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipStatus"));
                            
                            
                                    if (localShipStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipStatus cannot be null!!");
                                    }
                                    elementList.add(localShipStatus);
                                } if (localSaturdayDeliveryUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "saturdayDeliveryUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaturdayDeliveryUps));
                            } if (localSendShipNotifyEmailUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "sendShipNotifyEmailUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendShipNotifyEmailUps));
                            } if (localSendBackupEmailUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "sendBackupEmailUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendBackupEmailUps));
                            } if (localShipNotifyEmailAddressUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipNotifyEmailAddressUps"));
                                 
                                        if (localShipNotifyEmailAddressUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipNotifyEmailAddressUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailAddressUps cannot be null!!");
                                        }
                                    } if (localShipNotifyEmailAddress2UpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipNotifyEmailAddress2Ups"));
                                 
                                        if (localShipNotifyEmailAddress2Ups != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipNotifyEmailAddress2Ups));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailAddress2Ups cannot be null!!");
                                        }
                                    } if (localBackupEmailAddressUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "backupEmailAddressUps"));
                                 
                                        if (localBackupEmailAddressUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBackupEmailAddressUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("backupEmailAddressUps cannot be null!!");
                                        }
                                    } if (localShipNotifyEmailMessageUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipNotifyEmailMessageUps"));
                                 
                                        if (localShipNotifyEmailMessageUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipNotifyEmailMessageUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailMessageUps cannot be null!!");
                                        }
                                    } if (localThirdPartyAcctUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyAcctUps"));
                                 
                                        if (localThirdPartyAcctUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localThirdPartyAcctUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("thirdPartyAcctUps cannot be null!!");
                                        }
                                    } if (localThirdPartyZipcodeUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyZipcodeUps"));
                                 
                                        if (localThirdPartyZipcodeUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localThirdPartyZipcodeUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("thirdPartyZipcodeUps cannot be null!!");
                                        }
                                    } if (localThirdPartyCountryUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyCountryUps"));
                            
                            
                                    if (localThirdPartyCountryUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("thirdPartyCountryUps cannot be null!!");
                                    }
                                    elementList.add(localThirdPartyCountryUps);
                                } if (localThirdPartyTypeUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyTypeUps"));
                            
                            
                                    if (localThirdPartyTypeUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("thirdPartyTypeUps cannot be null!!");
                                    }
                                    elementList.add(localThirdPartyTypeUps);
                                } if (localPartiesToTransactionUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "partiesToTransactionUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPartiesToTransactionUps));
                            } if (localExportTypeUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "exportTypeUps"));
                            
                            
                                    if (localExportTypeUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("exportTypeUps cannot be null!!");
                                    }
                                    elementList.add(localExportTypeUps);
                                } if (localMethodOfTransportUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "methodOfTransportUps"));
                            
                            
                                    if (localMethodOfTransportUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("methodOfTransportUps cannot be null!!");
                                    }
                                    elementList.add(localMethodOfTransportUps);
                                } if (localCarrierIdUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "carrierIdUps"));
                                 
                                        if (localCarrierIdUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCarrierIdUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("carrierIdUps cannot be null!!");
                                        }
                                    } if (localEntryNumberUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "entryNumberUps"));
                                 
                                        if (localEntryNumberUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEntryNumberUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("entryNumberUps cannot be null!!");
                                        }
                                    } if (localInbondCodeUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "inbondCodeUps"));
                                 
                                        if (localInbondCodeUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInbondCodeUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("inbondCodeUps cannot be null!!");
                                        }
                                    } if (localIsRoutedExportTransactionUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isRoutedExportTransactionUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsRoutedExportTransactionUps));
                            } if (localLicenseNumberUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "licenseNumberUps"));
                                 
                                        if (localLicenseNumberUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLicenseNumberUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("licenseNumberUps cannot be null!!");
                                        }
                                    } if (localLicenseDateUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "licenseDateUps"));
                                 
                                        if (localLicenseDateUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLicenseDateUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("licenseDateUps cannot be null!!");
                                        }
                                    } if (localLicenseExceptionUpsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "licenseExceptionUps"));
                            
                            
                                    if (localLicenseExceptionUps==null){
                                         throw new org.apache.axis2.databinding.ADBException("licenseExceptionUps cannot be null!!");
                                    }
                                    elementList.add(localLicenseExceptionUps);
                                } if (localEccNumberUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "eccNumberUps"));
                                 
                                        if (localEccNumberUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEccNumberUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("eccNumberUps cannot be null!!");
                                        }
                                    } if (localRecipientTaxIdUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "recipientTaxIdUps"));
                                 
                                        if (localRecipientTaxIdUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecipientTaxIdUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("recipientTaxIdUps cannot be null!!");
                                        }
                                    } if (localBlanketStartDateUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "blanketStartDateUps"));
                                 
                                        if (localBlanketStartDateUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBlanketStartDateUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("blanketStartDateUps cannot be null!!");
                                        }
                                    } if (localBlanketEndDateUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "blanketEndDateUps"));
                                 
                                        if (localBlanketEndDateUps != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBlanketEndDateUps));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("blanketEndDateUps cannot be null!!");
                                        }
                                    } if (localShipmentWeightUpsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipmentWeightUps"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentWeightUps));
                            } if (localSaturdayDeliveryFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "saturdayDeliveryFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaturdayDeliveryFedEx));
                            } if (localSaturdayPickupFedexTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "saturdayPickupFedex"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSaturdayPickupFedex));
                            } if (localSendShipNotifyEmailFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "sendShipNotifyEmailFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendShipNotifyEmailFedEx));
                            } if (localSendBackupEmailFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "sendBackupEmailFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendBackupEmailFedEx));
                            } if (localSignatureHomeDeliveryFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "signatureHomeDeliveryFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSignatureHomeDeliveryFedEx));
                            } if (localShipNotifyEmailAddressFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipNotifyEmailAddressFedEx"));
                                 
                                        if (localShipNotifyEmailAddressFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipNotifyEmailAddressFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipNotifyEmailAddressFedEx cannot be null!!");
                                        }
                                    } if (localBackupEmailAddressFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "backupEmailAddressFedEx"));
                                 
                                        if (localBackupEmailAddressFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBackupEmailAddressFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("backupEmailAddressFedEx cannot be null!!");
                                        }
                                    } if (localShipDateFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipDateFedEx"));
                                 
                                        if (localShipDateFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipDateFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shipDateFedEx cannot be null!!");
                                        }
                                    } if (localHomeDeliveryTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "homeDeliveryTypeFedEx"));
                            
                            
                                    if (localHomeDeliveryTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("homeDeliveryTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localHomeDeliveryTypeFedEx);
                                } if (localHomeDeliveryDateFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "homeDeliveryDateFedEx"));
                                 
                                        if (localHomeDeliveryDateFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHomeDeliveryDateFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("homeDeliveryDateFedEx cannot be null!!");
                                        }
                                    } if (localBookingConfirmationNumFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "bookingConfirmationNumFedEx"));
                                 
                                        if (localBookingConfirmationNumFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBookingConfirmationNumFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("bookingConfirmationNumFedEx cannot be null!!");
                                        }
                                    } if (localIntlExemptionNumFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "intlExemptionNumFedEx"));
                                 
                                        if (localIntlExemptionNumFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIntlExemptionNumFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("intlExemptionNumFedEx cannot be null!!");
                                        }
                                    } if (localB13AFilingOptionFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "b13aFilingOptionFedEx"));
                            
                            
                                    if (localB13AFilingOptionFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("b13aFilingOptionFedEx cannot be null!!");
                                    }
                                    elementList.add(localB13AFilingOptionFedEx);
                                } if (localB13AStatementDataFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "b13aStatementDataFedEx"));
                                 
                                        if (localB13AStatementDataFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localB13AStatementDataFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("b13aStatementDataFedEx cannot be null!!");
                                        }
                                    } if (localThirdPartyAcctFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyAcctFedEx"));
                                 
                                        if (localThirdPartyAcctFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localThirdPartyAcctFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("thirdPartyAcctFedEx cannot be null!!");
                                        }
                                    } if (localThirdPartyCountryFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyCountryFedEx"));
                            
                            
                                    if (localThirdPartyCountryFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("thirdPartyCountryFedEx cannot be null!!");
                                    }
                                    elementList.add(localThirdPartyCountryFedEx);
                                } if (localThirdPartyTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "thirdPartyTypeFedEx"));
                            
                            
                                    if (localThirdPartyTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("thirdPartyTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localThirdPartyTypeFedEx);
                                } if (localShipmentWeightFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipmentWeightFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipmentWeightFedEx));
                            } if (localTermsOfSaleFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "termsOfSaleFedEx"));
                            
                            
                                    if (localTermsOfSaleFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("termsOfSaleFedEx cannot be null!!");
                                    }
                                    elementList.add(localTermsOfSaleFedEx);
                                } if (localTermsFreightChargeFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "termsFreightChargeFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTermsFreightChargeFedEx));
                            } if (localTermsInsuranceChargeFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "termsInsuranceChargeFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTermsInsuranceChargeFedEx));
                            } if (localInsideDeliveryFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "insideDeliveryFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsideDeliveryFedEx));
                            } if (localInsidePickupFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "insidePickupFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInsidePickupFedEx));
                            } if (localAncillaryEndorsementFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "ancillaryEndorsementFedEx"));
                            
                            
                                    if (localAncillaryEndorsementFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("ancillaryEndorsementFedEx cannot be null!!");
                                    }
                                    elementList.add(localAncillaryEndorsementFedEx);
                                } if (localHoldAtLocationFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "holdAtLocationFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHoldAtLocationFedEx));
                            } if (localHalPhoneFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halPhoneFedEx"));
                                 
                                        if (localHalPhoneFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalPhoneFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halPhoneFedEx cannot be null!!");
                                        }
                                    } if (localHalAddr1FedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halAddr1FedEx"));
                                 
                                        if (localHalAddr1FedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalAddr1FedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halAddr1FedEx cannot be null!!");
                                        }
                                    } if (localHalAddr2FedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halAddr2FedEx"));
                                 
                                        if (localHalAddr2FedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalAddr2FedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halAddr2FedEx cannot be null!!");
                                        }
                                    } if (localHalAddr3FedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halAddr3FedEx"));
                                 
                                        if (localHalAddr3FedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalAddr3FedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halAddr3FedEx cannot be null!!");
                                        }
                                    } if (localHalCityFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halCityFedEx"));
                                 
                                        if (localHalCityFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalCityFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halCityFedEx cannot be null!!");
                                        }
                                    } if (localHalZipFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halZipFedEx"));
                                 
                                        if (localHalZipFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalZipFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halZipFedEx cannot be null!!");
                                        }
                                    } if (localHalStateFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halStateFedEx"));
                                 
                                        if (localHalStateFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalStateFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halStateFedEx cannot be null!!");
                                        }
                                    } if (localHalCountryFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "halCountryFedEx"));
                                 
                                        if (localHalCountryFedEx != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHalCountryFedEx));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("halCountryFedEx cannot be null!!");
                                        }
                                    } if (localHazmatTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "hazmatTypeFedEx"));
                            
                            
                                    if (localHazmatTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("hazmatTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localHazmatTypeFedEx);
                                } if (localAccessibilityTypeFedExTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "accessibilityTypeFedEx"));
                            
                            
                                    if (localAccessibilityTypeFedEx==null){
                                         throw new org.apache.axis2.databinding.ADBException("accessibilityTypeFedEx cannot be null!!");
                                    }
                                    elementList.add(localAccessibilityTypeFedEx);
                                } if (localIsCargoAircraftOnlyFedExTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isCargoAircraftOnlyFedEx"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsCargoAircraftOnlyFedEx));
                            } if (localTranDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranDate"));
                                 
                                        if (localTranDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                        }
                                    } if (localTranIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tranId"));
                                 
                                        if (localTranId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                        }
                                    } if (localShipMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipMethod"));
                            
                            
                                    if (localShipMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipMethod cannot be null!!");
                                    }
                                    elementList.add(localShipMethod);
                                } if (localGenerateIntegratedShipperLabelTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "generateIntegratedShipperLabel"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenerateIntegratedShipperLabel));
                            } if (localShippingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCost));
                            } if (localHandlingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "handlingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCost));
                            } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localTransferLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "transferLocation"));
                            
                            
                                    if (localTransferLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("transferLocation cannot be null!!");
                                    }
                                    elementList.add(localTransferLocation);
                                } if (localPackageListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageList"));
                            
                            
                                    if (localPackageList==null){
                                         throw new org.apache.axis2.databinding.ADBException("packageList cannot be null!!");
                                    }
                                    elementList.add(localPackageList);
                                } if (localPackageUpsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageUpsList"));
                            
                            
                                    if (localPackageUpsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("packageUpsList cannot be null!!");
                                    }
                                    elementList.add(localPackageUpsList);
                                } if (localPackageUspsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageUspsList"));
                            
                            
                                    if (localPackageUspsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("packageUspsList cannot be null!!");
                                    }
                                    elementList.add(localPackageUspsList);
                                } if (localPackageFedExListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "packageFedExList"));
                            
                            
                                    if (localPackageFedExList==null){
                                         throw new org.apache.axis2.databinding.ADBException("packageFedExList cannot be null!!");
                                    }
                                    elementList.add(localPackageFedExList);
                                } if (localItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemList"));
                            
                            
                                    if (localItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemList cannot be null!!");
                                    }
                                    elementList.add(localItemList);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemFulfillment parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemFulfillment object =
                new ItemFulfillment();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemFulfillment".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemFulfillment)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","postingPeriod").equals(reader.getName())){
                                
                                                object.setPostingPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFrom").equals(reader.getName())){
                                
                                                object.setCreatedFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","requestedBy").equals(reader.getName())){
                                
                                                object.setRequestedBy(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFromShipGroup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdFromShipGroup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedFromShipGroup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCreatedFromShipGroup(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                                object.setPartner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingAddress").equals(reader.getName())){
                                
                                                object.setShippingAddress(com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","pickedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pickedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPickedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"packedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPackedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipIsResidential").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipIsResidential" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipIsResidential(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipAddressList").equals(reader.getName())){
                                
                                                object.setShipAddressList(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipStatus").equals(reader.getName())){
                                
                                                object.setShipStatus(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentShipStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","saturdayDeliveryUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"saturdayDeliveryUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSaturdayDeliveryUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sendShipNotifyEmailUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendShipNotifyEmailUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendShipNotifyEmailUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sendBackupEmailUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendBackupEmailUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendBackupEmailUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipNotifyEmailAddressUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipNotifyEmailAddressUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipNotifyEmailAddressUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipNotifyEmailAddress2Ups").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipNotifyEmailAddress2Ups" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipNotifyEmailAddress2Ups(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","backupEmailAddressUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"backupEmailAddressUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBackupEmailAddressUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipNotifyEmailMessageUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipNotifyEmailMessageUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipNotifyEmailMessageUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyAcctUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"thirdPartyAcctUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setThirdPartyAcctUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyZipcodeUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"thirdPartyZipcodeUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setThirdPartyZipcodeUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyCountryUps").equals(reader.getName())){
                                
                                                object.setThirdPartyCountryUps(com.netsuite.webservices.platform.common_2017_2.types.Country.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyTypeUps").equals(reader.getName())){
                                
                                                object.setThirdPartyTypeUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partiesToTransactionUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"partiesToTransactionUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPartiesToTransactionUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","exportTypeUps").equals(reader.getName())){
                                
                                                object.setExportTypeUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentExportTypeUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","methodOfTransportUps").equals(reader.getName())){
                                
                                                object.setMethodOfTransportUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentMethodOfTransportUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","carrierIdUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"carrierIdUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCarrierIdUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","entryNumberUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"entryNumberUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEntryNumberUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","inbondCodeUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"inbondCodeUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInbondCodeUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isRoutedExportTransactionUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isRoutedExportTransactionUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsRoutedExportTransactionUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","licenseNumberUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"licenseNumberUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLicenseNumberUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","licenseDateUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"licenseDateUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLicenseDateUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","licenseExceptionUps").equals(reader.getName())){
                                
                                                object.setLicenseExceptionUps(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentLicenseExceptionUps.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","eccNumberUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"eccNumberUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEccNumberUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","recipientTaxIdUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recipientTaxIdUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecipientTaxIdUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","blanketStartDateUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"blanketStartDateUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBlanketStartDateUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","blanketEndDateUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"blanketEndDateUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBlanketEndDateUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipmentWeightUps").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipmentWeightUps" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipmentWeightUps(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShipmentWeightUps(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","saturdayDeliveryFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"saturdayDeliveryFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSaturdayDeliveryFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","saturdayPickupFedex").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"saturdayPickupFedex" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSaturdayPickupFedex(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sendShipNotifyEmailFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendShipNotifyEmailFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendShipNotifyEmailFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","sendBackupEmailFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendBackupEmailFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendBackupEmailFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","signatureHomeDeliveryFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"signatureHomeDeliveryFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSignatureHomeDeliveryFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipNotifyEmailAddressFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipNotifyEmailAddressFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipNotifyEmailAddressFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","backupEmailAddressFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"backupEmailAddressFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBackupEmailAddressFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipDateFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipDateFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipDateFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","homeDeliveryTypeFedEx").equals(reader.getName())){
                                
                                                object.setHomeDeliveryTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHomeDeliveryTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","homeDeliveryDateFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"homeDeliveryDateFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHomeDeliveryDateFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","bookingConfirmationNumFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"bookingConfirmationNumFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBookingConfirmationNumFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","intlExemptionNumFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"intlExemptionNumFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIntlExemptionNumFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","b13aFilingOptionFedEx").equals(reader.getName())){
                                
                                                object.setB13AFilingOptionFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentB13AFilingOptionFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","b13aStatementDataFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"b13aStatementDataFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setB13AStatementDataFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyAcctFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"thirdPartyAcctFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setThirdPartyAcctFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyCountryFedEx").equals(reader.getName())){
                                
                                                object.setThirdPartyCountryFedEx(com.netsuite.webservices.platform.common_2017_2.types.Country.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","thirdPartyTypeFedEx").equals(reader.getName())){
                                
                                                object.setThirdPartyTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipmentWeightFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipmentWeightFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipmentWeightFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShipmentWeightFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","termsOfSaleFedEx").equals(reader.getName())){
                                
                                                object.setTermsOfSaleFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentTermsOfSaleFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","termsFreightChargeFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"termsFreightChargeFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTermsFreightChargeFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTermsFreightChargeFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","termsInsuranceChargeFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"termsInsuranceChargeFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTermsInsuranceChargeFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTermsInsuranceChargeFedEx(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","insideDeliveryFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"insideDeliveryFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInsideDeliveryFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","insidePickupFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"insidePickupFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInsidePickupFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","ancillaryEndorsementFedEx").equals(reader.getName())){
                                
                                                object.setAncillaryEndorsementFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAncillaryEndorsementFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","holdAtLocationFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"holdAtLocationFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHoldAtLocationFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halPhoneFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halPhoneFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalPhoneFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halAddr1FedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halAddr1FedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalAddr1FedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halAddr2FedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halAddr2FedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalAddr2FedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halAddr3FedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halAddr3FedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalAddr3FedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halCityFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halCityFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalCityFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halZipFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halZipFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalZipFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halStateFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halStateFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalStateFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","halCountryFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"halCountryFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHalCountryFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","hazmatTypeFedEx").equals(reader.getName())){
                                
                                                object.setHazmatTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHazmatTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accessibilityTypeFedEx").equals(reader.getName())){
                                
                                                object.setAccessibilityTypeFedEx(com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAccessibilityTypeFedEx.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isCargoAircraftOnlyFedEx").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isCargoAircraftOnlyFedEx" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsCargoAircraftOnlyFedEx(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipMethod").equals(reader.getName())){
                                
                                                object.setShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","generateIntegratedShipperLabel").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"generateIntegratedShipperLabel" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGenerateIntegratedShipperLabel(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","handlingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","transferLocation").equals(reader.getName())){
                                
                                                object.setTransferLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageList").equals(reader.getName())){
                                
                                                object.setPackageList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageUpsList").equals(reader.getName())){
                                
                                                object.setPackageUpsList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUpsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageUspsList").equals(reader.getName())){
                                
                                                object.setPackageUspsList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUspsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","packageFedExList").equals(reader.getName())){
                                
                                                object.setPackageFedExList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedExList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemList").equals(reader.getName())){
                                
                                                object.setItemList(com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    