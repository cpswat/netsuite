
/**
 * TransactionSearch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  TransactionSearch bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionSearch extends com.netsuite.webservices.platform.core_2017_2.SearchRecord
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TransactionSearch
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for AccountJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic localAccountJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountJoinTracker = false ;

                           public boolean isAccountJoinSpecified(){
                               return localAccountJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic getAccountJoin(){
                               return localAccountJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountJoin
                               */
                               public void setAccountJoin(com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic param){
                            localAccountJoinTracker = param != null;
                                   
                                            this.localAccountJoin=param;
                                    

                               }
                            

                        /**
                        * field for AccountingPeriodJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchBasic localAccountingPeriodJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingPeriodJoinTracker = false ;

                           public boolean isAccountingPeriodJoinSpecified(){
                               return localAccountingPeriodJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchBasic getAccountingPeriodJoin(){
                               return localAccountingPeriodJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingPeriodJoin
                               */
                               public void setAccountingPeriodJoin(com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchBasic param){
                            localAccountingPeriodJoinTracker = param != null;
                                   
                                            this.localAccountingPeriodJoin=param;
                                    

                               }
                            

                        /**
                        * field for AccountingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchBasic localAccountingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingTransactionJoinTracker = false ;

                           public boolean isAccountingTransactionJoinSpecified(){
                               return localAccountingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchBasic getAccountingTransactionJoin(){
                               return localAccountingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingTransactionJoin
                               */
                               public void setAccountingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchBasic param){
                            localAccountingTransactionJoinTracker = param != null;
                                   
                                            this.localAccountingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for AdvanceToApplyAccountJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic localAdvanceToApplyAccountJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAdvanceToApplyAccountJoinTracker = false ;

                           public boolean isAdvanceToApplyAccountJoinSpecified(){
                               return localAdvanceToApplyAccountJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic getAdvanceToApplyAccountJoin(){
                               return localAdvanceToApplyAccountJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AdvanceToApplyAccountJoin
                               */
                               public void setAdvanceToApplyAccountJoin(com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic param){
                            localAdvanceToApplyAccountJoinTracker = param != null;
                                   
                                            this.localAdvanceToApplyAccountJoin=param;
                                    

                               }
                            

                        /**
                        * field for AppliedToTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localAppliedToTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAppliedToTransactionJoinTracker = false ;

                           public boolean isAppliedToTransactionJoinSpecified(){
                               return localAppliedToTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getAppliedToTransactionJoin(){
                               return localAppliedToTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AppliedToTransactionJoin
                               */
                               public void setAppliedToTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localAppliedToTransactionJoinTracker = param != null;
                                   
                                            this.localAppliedToTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for ApplyingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localApplyingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApplyingTransactionJoinTracker = false ;

                           public boolean isApplyingTransactionJoinSpecified(){
                               return localApplyingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getApplyingTransactionJoin(){
                               return localApplyingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApplyingTransactionJoin
                               */
                               public void setApplyingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localApplyingTransactionJoinTracker = param != null;
                                   
                                            this.localApplyingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for BillingAddressJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic localBillingAddressJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingAddressJoinTracker = false ;

                           public boolean isBillingAddressJoinSpecified(){
                               return localBillingAddressJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic getBillingAddressJoin(){
                               return localBillingAddressJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingAddressJoin
                               */
                               public void setBillingAddressJoin(com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic param){
                            localBillingAddressJoinTracker = param != null;
                                   
                                            this.localBillingAddressJoin=param;
                                    

                               }
                            

                        /**
                        * field for BillingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localBillingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingTransactionJoinTracker = false ;

                           public boolean isBillingTransactionJoinSpecified(){
                               return localBillingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getBillingTransactionJoin(){
                               return localBillingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingTransactionJoin
                               */
                               public void setBillingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localBillingTransactionJoinTracker = param != null;
                                   
                                            this.localBillingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for BinNumberJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.BinSearchBasic localBinNumberJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBinNumberJoinTracker = false ;

                           public boolean isBinNumberJoinSpecified(){
                               return localBinNumberJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.BinSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.BinSearchBasic getBinNumberJoin(){
                               return localBinNumberJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BinNumberJoin
                               */
                               public void setBinNumberJoin(com.netsuite.webservices.platform.common_2017_2.BinSearchBasic param){
                            localBinNumberJoinTracker = param != null;
                                   
                                            this.localBinNumberJoin=param;
                                    

                               }
                            

                        /**
                        * field for CallJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic localCallJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCallJoinTracker = false ;

                           public boolean isCallJoinSpecified(){
                               return localCallJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic getCallJoin(){
                               return localCallJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CallJoin
                               */
                               public void setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic param){
                            localCallJoinTracker = param != null;
                                   
                                            this.localCallJoin=param;
                                    

                               }
                            

                        /**
                        * field for CaseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic localCaseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseJoinTracker = false ;

                           public boolean isCaseJoinSpecified(){
                               return localCaseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic getCaseJoin(){
                               return localCaseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseJoin
                               */
                               public void setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic param){
                            localCaseJoinTracker = param != null;
                                   
                                            this.localCaseJoin=param;
                                    

                               }
                            

                        /**
                        * field for ClassJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic localClassJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClassJoinTracker = false ;

                           public boolean isClassJoinSpecified(){
                               return localClassJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic getClassJoin(){
                               return localClassJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClassJoin
                               */
                               public void setClassJoin(com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic param){
                            localClassJoinTracker = param != null;
                                   
                                            this.localClassJoin=param;
                                    

                               }
                            

                        /**
                        * field for CogsPurchaseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localCogsPurchaseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCogsPurchaseJoinTracker = false ;

                           public boolean isCogsPurchaseJoinSpecified(){
                               return localCogsPurchaseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getCogsPurchaseJoin(){
                               return localCogsPurchaseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CogsPurchaseJoin
                               */
                               public void setCogsPurchaseJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localCogsPurchaseJoinTracker = param != null;
                                   
                                            this.localCogsPurchaseJoin=param;
                                    

                               }
                            

                        /**
                        * field for CogsSaleJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localCogsSaleJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCogsSaleJoinTracker = false ;

                           public boolean isCogsSaleJoinSpecified(){
                               return localCogsSaleJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getCogsSaleJoin(){
                               return localCogsSaleJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CogsSaleJoin
                               */
                               public void setCogsSaleJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localCogsSaleJoinTracker = param != null;
                                   
                                            this.localCogsSaleJoin=param;
                                    

                               }
                            

                        /**
                        * field for ContactPrimaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic localContactPrimaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactPrimaryJoinTracker = false ;

                           public boolean isContactPrimaryJoinSpecified(){
                               return localContactPrimaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic getContactPrimaryJoin(){
                               return localContactPrimaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContactPrimaryJoin
                               */
                               public void setContactPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic param){
                            localContactPrimaryJoinTracker = param != null;
                                   
                                            this.localContactPrimaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for CreatedFromJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localCreatedFromJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedFromJoinTracker = false ;

                           public boolean isCreatedFromJoinSpecified(){
                               return localCreatedFromJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getCreatedFromJoin(){
                               return localCreatedFromJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedFromJoin
                               */
                               public void setCreatedFromJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localCreatedFromJoinTracker = param != null;
                                   
                                            this.localCreatedFromJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic localCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerJoinTracker = false ;

                           public boolean isCustomerJoinSpecified(){
                               return localCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic getCustomerJoin(){
                               return localCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerJoin
                               */
                               public void setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic param){
                            localCustomerJoinTracker = param != null;
                                   
                                            this.localCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerMainJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic localCustomerMainJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerMainJoinTracker = false ;

                           public boolean isCustomerMainJoinSpecified(){
                               return localCustomerMainJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic getCustomerMainJoin(){
                               return localCustomerMainJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerMainJoin
                               */
                               public void setCustomerMainJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic param){
                            localCustomerMainJoinTracker = param != null;
                                   
                                            this.localCustomerMainJoin=param;
                                    

                               }
                            

                        /**
                        * field for DepartmentJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic localDepartmentJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentJoinTracker = false ;

                           public boolean isDepartmentJoinSpecified(){
                               return localDepartmentJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic getDepartmentJoin(){
                               return localDepartmentJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DepartmentJoin
                               */
                               public void setDepartmentJoin(com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic param){
                            localDepartmentJoinTracker = param != null;
                                   
                                            this.localDepartmentJoin=param;
                                    

                               }
                            

                        /**
                        * field for DepositTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localDepositTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepositTransactionJoinTracker = false ;

                           public boolean isDepositTransactionJoinSpecified(){
                               return localDepositTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getDepositTransactionJoin(){
                               return localDepositTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DepositTransactionJoin
                               */
                               public void setDepositTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localDepositTransactionJoinTracker = param != null;
                                   
                                            this.localDepositTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localEmployeeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeJoinTracker = false ;

                           public boolean isEmployeeJoinSpecified(){
                               return localEmployeeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getEmployeeJoin(){
                               return localEmployeeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeJoin
                               */
                               public void setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localEmployeeJoinTracker = param != null;
                                   
                                            this.localEmployeeJoin=param;
                                    

                               }
                            

                        /**
                        * field for EventJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic localEventJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventJoinTracker = false ;

                           public boolean isEventJoinSpecified(){
                               return localEventJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic getEventJoin(){
                               return localEventJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventJoin
                               */
                               public void setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic param){
                            localEventJoinTracker = param != null;
                                   
                                            this.localEventJoin=param;
                                    

                               }
                            

                        /**
                        * field for ExpenseCategoryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchBasic localExpenseCategoryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpenseCategoryJoinTracker = false ;

                           public boolean isExpenseCategoryJoinSpecified(){
                               return localExpenseCategoryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchBasic getExpenseCategoryJoin(){
                               return localExpenseCategoryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpenseCategoryJoin
                               */
                               public void setExpenseCategoryJoin(com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchBasic param){
                            localExpenseCategoryJoinTracker = param != null;
                                   
                                            this.localExpenseCategoryJoin=param;
                                    

                               }
                            

                        /**
                        * field for FileJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.FileSearchBasic localFileJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFileJoinTracker = false ;

                           public boolean isFileJoinSpecified(){
                               return localFileJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.FileSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.FileSearchBasic getFileJoin(){
                               return localFileJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FileJoin
                               */
                               public void setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchBasic param){
                            localFileJoinTracker = param != null;
                                   
                                            this.localFileJoin=param;
                                    

                               }
                            

                        /**
                        * field for FromLocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic localFromLocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFromLocationJoinTracker = false ;

                           public boolean isFromLocationJoinSpecified(){
                               return localFromLocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic getFromLocationJoin(){
                               return localFromLocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FromLocationJoin
                               */
                               public void setFromLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic param){
                            localFromLocationJoinTracker = param != null;
                                   
                                            this.localFromLocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for FulfillingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localFulfillingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFulfillingTransactionJoinTracker = false ;

                           public boolean isFulfillingTransactionJoinSpecified(){
                               return localFulfillingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getFulfillingTransactionJoin(){
                               return localFulfillingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FulfillingTransactionJoin
                               */
                               public void setFulfillingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localFulfillingTransactionJoinTracker = param != null;
                                   
                                            this.localFulfillingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for HeaderBillingAccountJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic localHeaderBillingAccountJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHeaderBillingAccountJoinTracker = false ;

                           public boolean isHeaderBillingAccountJoinSpecified(){
                               return localHeaderBillingAccountJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic getHeaderBillingAccountJoin(){
                               return localHeaderBillingAccountJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HeaderBillingAccountJoin
                               */
                               public void setHeaderBillingAccountJoin(com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic param){
                            localHeaderBillingAccountJoinTracker = param != null;
                                   
                                            this.localHeaderBillingAccountJoin=param;
                                    

                               }
                            

                        /**
                        * field for InventoryDetailJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchBasic localInventoryDetailJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryDetailJoinTracker = false ;

                           public boolean isInventoryDetailJoinSpecified(){
                               return localInventoryDetailJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchBasic getInventoryDetailJoin(){
                               return localInventoryDetailJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryDetailJoin
                               */
                               public void setInventoryDetailJoin(com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchBasic param){
                            localInventoryDetailJoinTracker = param != null;
                                   
                                            this.localInventoryDetailJoin=param;
                                    

                               }
                            

                        /**
                        * field for ItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic localItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemJoinTracker = false ;

                           public boolean isItemJoinSpecified(){
                               return localItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic getItemJoin(){
                               return localItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemJoin
                               */
                               public void setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic param){
                            localItemJoinTracker = param != null;
                                   
                                            this.localItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for ItemNumberJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchBasic localItemNumberJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemNumberJoinTracker = false ;

                           public boolean isItemNumberJoinSpecified(){
                               return localItemNumberJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchBasic getItemNumberJoin(){
                               return localItemNumberJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemNumberJoin
                               */
                               public void setItemNumberJoin(com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchBasic param){
                            localItemNumberJoinTracker = param != null;
                                   
                                            this.localItemNumberJoin=param;
                                    

                               }
                            

                        /**
                        * field for JobJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchBasic localJobJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobJoinTracker = false ;

                           public boolean isJobJoinSpecified(){
                               return localJobJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchBasic getJobJoin(){
                               return localJobJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobJoin
                               */
                               public void setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchBasic param){
                            localJobJoinTracker = param != null;
                                   
                                            this.localJobJoin=param;
                                    

                               }
                            

                        /**
                        * field for JobMainJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchBasic localJobMainJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobMainJoinTracker = false ;

                           public boolean isJobMainJoinSpecified(){
                               return localJobMainJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchBasic getJobMainJoin(){
                               return localJobMainJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobMainJoin
                               */
                               public void setJobMainJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchBasic param){
                            localJobMainJoinTracker = param != null;
                                   
                                            this.localJobMainJoin=param;
                                    

                               }
                            

                        /**
                        * field for LeadSourceJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CampaignSearchBasic localLeadSourceJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceJoinTracker = false ;

                           public boolean isLeadSourceJoinSpecified(){
                               return localLeadSourceJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CampaignSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CampaignSearchBasic getLeadSourceJoin(){
                               return localLeadSourceJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadSourceJoin
                               */
                               public void setLeadSourceJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchBasic param){
                            localLeadSourceJoinTracker = param != null;
                                   
                                            this.localLeadSourceJoin=param;
                                    

                               }
                            

                        /**
                        * field for LineBillingAccountJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic localLineBillingAccountJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineBillingAccountJoinTracker = false ;

                           public boolean isLineBillingAccountJoinSpecified(){
                               return localLineBillingAccountJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic getLineBillingAccountJoin(){
                               return localLineBillingAccountJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LineBillingAccountJoin
                               */
                               public void setLineBillingAccountJoin(com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic param){
                            localLineBillingAccountJoinTracker = param != null;
                                   
                                            this.localLineBillingAccountJoin=param;
                                    

                               }
                            

                        /**
                        * field for LocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic localLocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationJoinTracker = false ;

                           public boolean isLocationJoinSpecified(){
                               return localLocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic getLocationJoin(){
                               return localLocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationJoin
                               */
                               public void setLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic param){
                            localLocationJoinTracker = param != null;
                                   
                                            this.localLocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingOperationTaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchBasic localManufacturingOperationTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingOperationTaskJoinTracker = false ;

                           public boolean isManufacturingOperationTaskJoinSpecified(){
                               return localManufacturingOperationTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchBasic getManufacturingOperationTaskJoin(){
                               return localManufacturingOperationTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingOperationTaskJoin
                               */
                               public void setManufacturingOperationTaskJoin(com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchBasic param){
                            localManufacturingOperationTaskJoinTracker = param != null;
                                   
                                            this.localManufacturingOperationTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic localMessagesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesJoinTracker = false ;

                           public boolean isMessagesJoinSpecified(){
                               return localMessagesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic getMessagesJoin(){
                               return localMessagesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesJoin
                               */
                               public void setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic param){
                            localMessagesJoinTracker = param != null;
                                   
                                            this.localMessagesJoin=param;
                                    

                               }
                            

                        /**
                        * field for NextApproverJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic localNextApproverJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextApproverJoinTracker = false ;

                           public boolean isNextApproverJoinSpecified(){
                               return localNextApproverJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic getNextApproverJoin(){
                               return localNextApproverJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextApproverJoin
                               */
                               public void setNextApproverJoin(com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic param){
                            localNextApproverJoinTracker = param != null;
                                   
                                            this.localNextApproverJoin=param;
                                    

                               }
                            

                        /**
                        * field for OpportunityJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.OpportunitySearchBasic localOpportunityJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpportunityJoinTracker = false ;

                           public boolean isOpportunityJoinSpecified(){
                               return localOpportunityJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.OpportunitySearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.OpportunitySearchBasic getOpportunityJoin(){
                               return localOpportunityJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OpportunityJoin
                               */
                               public void setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchBasic param){
                            localOpportunityJoinTracker = param != null;
                                   
                                            this.localOpportunityJoin=param;
                                    

                               }
                            

                        /**
                        * field for PaidTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localPaidTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaidTransactionJoinTracker = false ;

                           public boolean isPaidTransactionJoinSpecified(){
                               return localPaidTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getPaidTransactionJoin(){
                               return localPaidTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PaidTransactionJoin
                               */
                               public void setPaidTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localPaidTransactionJoinTracker = param != null;
                                   
                                            this.localPaidTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for PartnerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PartnerSearchBasic localPartnerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerJoinTracker = false ;

                           public boolean isPartnerJoinSpecified(){
                               return localPartnerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PartnerSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PartnerSearchBasic getPartnerJoin(){
                               return localPartnerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerJoin
                               */
                               public void setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchBasic param){
                            localPartnerJoinTracker = param != null;
                                   
                                            this.localPartnerJoin=param;
                                    

                               }
                            

                        /**
                        * field for PayingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localPayingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayingTransactionJoinTracker = false ;

                           public boolean isPayingTransactionJoinSpecified(){
                               return localPayingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getPayingTransactionJoin(){
                               return localPayingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayingTransactionJoin
                               */
                               public void setPayingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localPayingTransactionJoinTracker = param != null;
                                   
                                            this.localPayingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for PayrollItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchBasic localPayrollItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayrollItemJoinTracker = false ;

                           public boolean isPayrollItemJoinSpecified(){
                               return localPayrollItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchBasic getPayrollItemJoin(){
                               return localPayrollItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayrollItemJoin
                               */
                               public void setPayrollItemJoin(com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchBasic param){
                            localPayrollItemJoinTracker = param != null;
                                   
                                            this.localPayrollItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for ProjectTaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic localProjectTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTaskJoinTracker = false ;

                           public boolean isProjectTaskJoinSpecified(){
                               return localProjectTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic getProjectTaskJoin(){
                               return localProjectTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectTaskJoin
                               */
                               public void setProjectTaskJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic param){
                            localProjectTaskJoinTracker = param != null;
                                   
                                            this.localProjectTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localPurchaseOrderJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderJoinTracker = false ;

                           public boolean isPurchaseOrderJoinSpecified(){
                               return localPurchaseOrderJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getPurchaseOrderJoin(){
                               return localPurchaseOrderJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderJoin
                               */
                               public void setPurchaseOrderJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localPurchaseOrderJoinTracker = param != null;
                                   
                                            this.localPurchaseOrderJoin=param;
                                    

                               }
                            

                        /**
                        * field for RequestorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localRequestorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRequestorJoinTracker = false ;

                           public boolean isRequestorJoinSpecified(){
                               return localRequestorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getRequestorJoin(){
                               return localRequestorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RequestorJoin
                               */
                               public void setRequestorJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localRequestorJoinTracker = param != null;
                                   
                                            this.localRequestorJoin=param;
                                    

                               }
                            

                        /**
                        * field for RevCommittingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localRevCommittingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevCommittingTransactionJoinTracker = false ;

                           public boolean isRevCommittingTransactionJoinSpecified(){
                               return localRevCommittingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getRevCommittingTransactionJoin(){
                               return localRevCommittingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevCommittingTransactionJoin
                               */
                               public void setRevCommittingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localRevCommittingTransactionJoinTracker = param != null;
                                   
                                            this.localRevCommittingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for RevisionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchBasic localRevisionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevisionJoinTracker = false ;

                           public boolean isRevisionJoinSpecified(){
                               return localRevisionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchBasic getRevisionJoin(){
                               return localRevisionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevisionJoin
                               */
                               public void setRevisionJoin(com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchBasic param){
                            localRevisionJoinTracker = param != null;
                                   
                                            this.localRevisionJoin=param;
                                    

                               }
                            

                        /**
                        * field for RevRecScheduleJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchBasic localRevRecScheduleJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleJoinTracker = false ;

                           public boolean isRevRecScheduleJoinSpecified(){
                               return localRevRecScheduleJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchBasic getRevRecScheduleJoin(){
                               return localRevRecScheduleJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecScheduleJoin
                               */
                               public void setRevRecScheduleJoin(com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchBasic param){
                            localRevRecScheduleJoinTracker = param != null;
                                   
                                            this.localRevRecScheduleJoin=param;
                                    

                               }
                            

                        /**
                        * field for RgPostingTransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localRgPostingTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRgPostingTransactionJoinTracker = false ;

                           public boolean isRgPostingTransactionJoinSpecified(){
                               return localRgPostingTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getRgPostingTransactionJoin(){
                               return localRgPostingTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RgPostingTransactionJoin
                               */
                               public void setRgPostingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localRgPostingTransactionJoinTracker = param != null;
                                   
                                            this.localRgPostingTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for SalesOrderJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localSalesOrderJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesOrderJoinTracker = false ;

                           public boolean isSalesOrderJoinSpecified(){
                               return localSalesOrderJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getSalesOrderJoin(){
                               return localSalesOrderJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesOrderJoin
                               */
                               public void setSalesOrderJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localSalesOrderJoinTracker = param != null;
                                   
                                            this.localSalesOrderJoin=param;
                                    

                               }
                            

                        /**
                        * field for SalesRepJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localSalesRepJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepJoinTracker = false ;

                           public boolean isSalesRepJoinSpecified(){
                               return localSalesRepJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getSalesRepJoin(){
                               return localSalesRepJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRepJoin
                               */
                               public void setSalesRepJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localSalesRepJoinTracker = param != null;
                                   
                                            this.localSalesRepJoin=param;
                                    

                               }
                            

                        /**
                        * field for ShippingAddressJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic localShippingAddressJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingAddressJoinTracker = false ;

                           public boolean isShippingAddressJoinSpecified(){
                               return localShippingAddressJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic getShippingAddressJoin(){
                               return localShippingAddressJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingAddressJoin
                               */
                               public void setShippingAddressJoin(com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic param){
                            localShippingAddressJoinTracker = param != null;
                                   
                                            this.localShippingAddressJoin=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchBasic localSubsidiaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryJoinTracker = false ;

                           public boolean isSubsidiaryJoinSpecified(){
                               return localSubsidiaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchBasic getSubsidiaryJoin(){
                               return localSubsidiaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryJoin
                               */
                               public void setSubsidiaryJoin(com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchBasic param){
                            localSubsidiaryJoinTracker = param != null;
                                   
                                            this.localSubsidiaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic localTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaskJoinTracker = false ;

                           public boolean isTaskJoinSpecified(){
                               return localTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic getTaskJoin(){
                               return localTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaskJoin
                               */
                               public void setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic param){
                            localTaskJoinTracker = param != null;
                                   
                                            this.localTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaxCodeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic localTaxCodeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxCodeJoinTracker = false ;

                           public boolean isTaxCodeJoinSpecified(){
                               return localTaxCodeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic getTaxCodeJoin(){
                               return localTaxCodeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxCodeJoin
                               */
                               public void setTaxCodeJoin(com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic param){
                            localTaxCodeJoinTracker = param != null;
                                   
                                            this.localTaxCodeJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchBasic localTaxDetailJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailJoinTracker = false ;

                           public boolean isTaxDetailJoinSpecified(){
                               return localTaxDetailJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchBasic getTaxDetailJoin(){
                               return localTaxDetailJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailJoin
                               */
                               public void setTaxDetailJoin(com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchBasic param){
                            localTaxDetailJoinTracker = param != null;
                                   
                                            this.localTaxDetailJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaxItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic localTaxItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxItemJoinTracker = false ;

                           public boolean isTaxItemJoinSpecified(){
                               return localTaxItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic getTaxItemJoin(){
                               return localTaxItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxItemJoin
                               */
                               public void setTaxItemJoin(com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic param){
                            localTaxItemJoinTracker = param != null;
                                   
                                            this.localTaxItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for TimeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic localTimeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeJoinTracker = false ;

                           public boolean isTimeJoinSpecified(){
                               return localTimeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic getTimeJoin(){
                               return localTimeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeJoin
                               */
                               public void setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic param){
                            localTimeJoinTracker = param != null;
                                   
                                            this.localTimeJoin=param;
                                    

                               }
                            

                        /**
                        * field for ToLocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic localToLocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localToLocationJoinTracker = false ;

                           public boolean isToLocationJoinSpecified(){
                               return localToLocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic getToLocationJoin(){
                               return localToLocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ToLocationJoin
                               */
                               public void setToLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic param){
                            localToLocationJoinTracker = param != null;
                                   
                                            this.localToLocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserNotesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic localUserNotesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserNotesJoinTracker = false ;

                           public boolean isUserNotesJoinSpecified(){
                               return localUserNotesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic getUserNotesJoin(){
                               return localUserNotesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserNotesJoin
                               */
                               public void setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic param){
                            localUserNotesJoinTracker = param != null;
                                   
                                            this.localUserNotesJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic localVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorJoinTracker = false ;

                           public boolean isVendorJoinSpecified(){
                               return localVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic getVendorJoin(){
                               return localVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorJoin
                               */
                               public void setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic param){
                            localVendorJoinTracker = param != null;
                                   
                                            this.localVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorLineJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic localVendorLineJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorLineJoinTracker = false ;

                           public boolean isVendorLineJoinSpecified(){
                               return localVendorLineJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic getVendorLineJoin(){
                               return localVendorLineJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorLineJoin
                               */
                               public void setVendorLineJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic param){
                            localVendorLineJoinTracker = param != null;
                                   
                                            this.localVendorLineJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TransactionSearch",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TransactionSearch",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localAccountJoinTracker){
                                            if (localAccountJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountJoin cannot be null!!");
                                            }
                                           localAccountJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountJoin"),
                                               xmlWriter);
                                        } if (localAccountingPeriodJoinTracker){
                                            if (localAccountingPeriodJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingPeriodJoin cannot be null!!");
                                            }
                                           localAccountingPeriodJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingPeriodJoin"),
                                               xmlWriter);
                                        } if (localAccountingTransactionJoinTracker){
                                            if (localAccountingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingTransactionJoin cannot be null!!");
                                            }
                                           localAccountingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingTransactionJoin"),
                                               xmlWriter);
                                        } if (localAdvanceToApplyAccountJoinTracker){
                                            if (localAdvanceToApplyAccountJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("advanceToApplyAccountJoin cannot be null!!");
                                            }
                                           localAdvanceToApplyAccountJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","advanceToApplyAccountJoin"),
                                               xmlWriter);
                                        } if (localAppliedToTransactionJoinTracker){
                                            if (localAppliedToTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("appliedToTransactionJoin cannot be null!!");
                                            }
                                           localAppliedToTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","appliedToTransactionJoin"),
                                               xmlWriter);
                                        } if (localApplyingTransactionJoinTracker){
                                            if (localApplyingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("applyingTransactionJoin cannot be null!!");
                                            }
                                           localApplyingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","applyingTransactionJoin"),
                                               xmlWriter);
                                        } if (localBillingAddressJoinTracker){
                                            if (localBillingAddressJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingAddressJoin cannot be null!!");
                                            }
                                           localBillingAddressJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingAddressJoin"),
                                               xmlWriter);
                                        } if (localBillingTransactionJoinTracker){
                                            if (localBillingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingTransactionJoin cannot be null!!");
                                            }
                                           localBillingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingTransactionJoin"),
                                               xmlWriter);
                                        } if (localBinNumberJoinTracker){
                                            if (localBinNumberJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("binNumberJoin cannot be null!!");
                                            }
                                           localBinNumberJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","binNumberJoin"),
                                               xmlWriter);
                                        } if (localCallJoinTracker){
                                            if (localCallJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                            }
                                           localCallJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","callJoin"),
                                               xmlWriter);
                                        } if (localCaseJoinTracker){
                                            if (localCaseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                            }
                                           localCaseJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","caseJoin"),
                                               xmlWriter);
                                        } if (localClassJoinTracker){
                                            if (localClassJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("classJoin cannot be null!!");
                                            }
                                           localClassJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","classJoin"),
                                               xmlWriter);
                                        } if (localCogsPurchaseJoinTracker){
                                            if (localCogsPurchaseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("cogsPurchaseJoin cannot be null!!");
                                            }
                                           localCogsPurchaseJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","cogsPurchaseJoin"),
                                               xmlWriter);
                                        } if (localCogsSaleJoinTracker){
                                            if (localCogsSaleJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("cogsSaleJoin cannot be null!!");
                                            }
                                           localCogsSaleJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","cogsSaleJoin"),
                                               xmlWriter);
                                        } if (localContactPrimaryJoinTracker){
                                            if (localContactPrimaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contactPrimaryJoin cannot be null!!");
                                            }
                                           localContactPrimaryJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","contactPrimaryJoin"),
                                               xmlWriter);
                                        } if (localCreatedFromJoinTracker){
                                            if (localCreatedFromJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdFromJoin cannot be null!!");
                                            }
                                           localCreatedFromJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFromJoin"),
                                               xmlWriter);
                                        } if (localCustomerJoinTracker){
                                            if (localCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                            }
                                           localCustomerJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customerJoin"),
                                               xmlWriter);
                                        } if (localCustomerMainJoinTracker){
                                            if (localCustomerMainJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerMainJoin cannot be null!!");
                                            }
                                           localCustomerMainJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customerMainJoin"),
                                               xmlWriter);
                                        } if (localDepartmentJoinTracker){
                                            if (localDepartmentJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("departmentJoin cannot be null!!");
                                            }
                                           localDepartmentJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","departmentJoin"),
                                               xmlWriter);
                                        } if (localDepositTransactionJoinTracker){
                                            if (localDepositTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("depositTransactionJoin cannot be null!!");
                                            }
                                           localDepositTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","depositTransactionJoin"),
                                               xmlWriter);
                                        } if (localEmployeeJoinTracker){
                                            if (localEmployeeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                            }
                                           localEmployeeJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","employeeJoin"),
                                               xmlWriter);
                                        } if (localEventJoinTracker){
                                            if (localEventJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                            }
                                           localEventJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","eventJoin"),
                                               xmlWriter);
                                        } if (localExpenseCategoryJoinTracker){
                                            if (localExpenseCategoryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expenseCategoryJoin cannot be null!!");
                                            }
                                           localExpenseCategoryJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","expenseCategoryJoin"),
                                               xmlWriter);
                                        } if (localFileJoinTracker){
                                            if (localFileJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                            }
                                           localFileJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fileJoin"),
                                               xmlWriter);
                                        } if (localFromLocationJoinTracker){
                                            if (localFromLocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fromLocationJoin cannot be null!!");
                                            }
                                           localFromLocationJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fromLocationJoin"),
                                               xmlWriter);
                                        } if (localFulfillingTransactionJoinTracker){
                                            if (localFulfillingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fulfillingTransactionJoin cannot be null!!");
                                            }
                                           localFulfillingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fulfillingTransactionJoin"),
                                               xmlWriter);
                                        } if (localHeaderBillingAccountJoinTracker){
                                            if (localHeaderBillingAccountJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("headerBillingAccountJoin cannot be null!!");
                                            }
                                           localHeaderBillingAccountJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","headerBillingAccountJoin"),
                                               xmlWriter);
                                        } if (localInventoryDetailJoinTracker){
                                            if (localInventoryDetailJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryDetailJoin cannot be null!!");
                                            }
                                           localInventoryDetailJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","inventoryDetailJoin"),
                                               xmlWriter);
                                        } if (localItemJoinTracker){
                                            if (localItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                            }
                                           localItemJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemJoin"),
                                               xmlWriter);
                                        } if (localItemNumberJoinTracker){
                                            if (localItemNumberJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemNumberJoin cannot be null!!");
                                            }
                                           localItemNumberJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemNumberJoin"),
                                               xmlWriter);
                                        } if (localJobJoinTracker){
                                            if (localJobJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                            }
                                           localJobJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","jobJoin"),
                                               xmlWriter);
                                        } if (localJobMainJoinTracker){
                                            if (localJobMainJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobMainJoin cannot be null!!");
                                            }
                                           localJobMainJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","jobMainJoin"),
                                               xmlWriter);
                                        } if (localLeadSourceJoinTracker){
                                            if (localLeadSourceJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadSourceJoin cannot be null!!");
                                            }
                                           localLeadSourceJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","leadSourceJoin"),
                                               xmlWriter);
                                        } if (localLineBillingAccountJoinTracker){
                                            if (localLineBillingAccountJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lineBillingAccountJoin cannot be null!!");
                                            }
                                           localLineBillingAccountJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","lineBillingAccountJoin"),
                                               xmlWriter);
                                        } if (localLocationJoinTracker){
                                            if (localLocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationJoin cannot be null!!");
                                            }
                                           localLocationJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","locationJoin"),
                                               xmlWriter);
                                        } if (localManufacturingOperationTaskJoinTracker){
                                            if (localManufacturingOperationTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingOperationTaskJoin cannot be null!!");
                                            }
                                           localManufacturingOperationTaskJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","manufacturingOperationTaskJoin"),
                                               xmlWriter);
                                        } if (localMessagesJoinTracker){
                                            if (localMessagesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                            }
                                           localMessagesJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","messagesJoin"),
                                               xmlWriter);
                                        } if (localNextApproverJoinTracker){
                                            if (localNextApproverJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nextApproverJoin cannot be null!!");
                                            }
                                           localNextApproverJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","nextApproverJoin"),
                                               xmlWriter);
                                        } if (localOpportunityJoinTracker){
                                            if (localOpportunityJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                            }
                                           localOpportunityJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","opportunityJoin"),
                                               xmlWriter);
                                        } if (localPaidTransactionJoinTracker){
                                            if (localPaidTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("paidTransactionJoin cannot be null!!");
                                            }
                                           localPaidTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paidTransactionJoin"),
                                               xmlWriter);
                                        } if (localPartnerJoinTracker){
                                            if (localPartnerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                            }
                                           localPartnerJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partnerJoin"),
                                               xmlWriter);
                                        } if (localPayingTransactionJoinTracker){
                                            if (localPayingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("payingTransactionJoin cannot be null!!");
                                            }
                                           localPayingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","payingTransactionJoin"),
                                               xmlWriter);
                                        } if (localPayrollItemJoinTracker){
                                            if (localPayrollItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("payrollItemJoin cannot be null!!");
                                            }
                                           localPayrollItemJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","payrollItemJoin"),
                                               xmlWriter);
                                        } if (localProjectTaskJoinTracker){
                                            if (localProjectTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectTaskJoin cannot be null!!");
                                            }
                                           localProjectTaskJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","projectTaskJoin"),
                                               xmlWriter);
                                        } if (localPurchaseOrderJoinTracker){
                                            if (localPurchaseOrderJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderJoin cannot be null!!");
                                            }
                                           localPurchaseOrderJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","purchaseOrderJoin"),
                                               xmlWriter);
                                        } if (localRequestorJoinTracker){
                                            if (localRequestorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("requestorJoin cannot be null!!");
                                            }
                                           localRequestorJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","requestorJoin"),
                                               xmlWriter);
                                        } if (localRevCommittingTransactionJoinTracker){
                                            if (localRevCommittingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revCommittingTransactionJoin cannot be null!!");
                                            }
                                           localRevCommittingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revCommittingTransactionJoin"),
                                               xmlWriter);
                                        } if (localRevisionJoinTracker){
                                            if (localRevisionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revisionJoin cannot be null!!");
                                            }
                                           localRevisionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revisionJoin"),
                                               xmlWriter);
                                        } if (localRevRecScheduleJoinTracker){
                                            if (localRevRecScheduleJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecScheduleJoin cannot be null!!");
                                            }
                                           localRevRecScheduleJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecScheduleJoin"),
                                               xmlWriter);
                                        } if (localRgPostingTransactionJoinTracker){
                                            if (localRgPostingTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rgPostingTransactionJoin cannot be null!!");
                                            }
                                           localRgPostingTransactionJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","rgPostingTransactionJoin"),
                                               xmlWriter);
                                        } if (localSalesOrderJoinTracker){
                                            if (localSalesOrderJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesOrderJoin cannot be null!!");
                                            }
                                           localSalesOrderJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesOrderJoin"),
                                               xmlWriter);
                                        } if (localSalesRepJoinTracker){
                                            if (localSalesRepJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRepJoin cannot be null!!");
                                            }
                                           localSalesRepJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesRepJoin"),
                                               xmlWriter);
                                        } if (localShippingAddressJoinTracker){
                                            if (localShippingAddressJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingAddressJoin cannot be null!!");
                                            }
                                           localShippingAddressJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingAddressJoin"),
                                               xmlWriter);
                                        } if (localSubsidiaryJoinTracker){
                                            if (localSubsidiaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryJoin cannot be null!!");
                                            }
                                           localSubsidiaryJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subsidiaryJoin"),
                                               xmlWriter);
                                        } if (localTaskJoinTracker){
                                            if (localTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                            }
                                           localTaskJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taskJoin"),
                                               xmlWriter);
                                        } if (localTaxCodeJoinTracker){
                                            if (localTaxCodeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxCodeJoin cannot be null!!");
                                            }
                                           localTaxCodeJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxCodeJoin"),
                                               xmlWriter);
                                        } if (localTaxDetailJoinTracker){
                                            if (localTaxDetailJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxDetailJoin cannot be null!!");
                                            }
                                           localTaxDetailJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxDetailJoin"),
                                               xmlWriter);
                                        } if (localTaxItemJoinTracker){
                                            if (localTaxItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxItemJoin cannot be null!!");
                                            }
                                           localTaxItemJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxItemJoin"),
                                               xmlWriter);
                                        } if (localTimeJoinTracker){
                                            if (localTimeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                            }
                                           localTimeJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","timeJoin"),
                                               xmlWriter);
                                        } if (localToLocationJoinTracker){
                                            if (localToLocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("toLocationJoin cannot be null!!");
                                            }
                                           localToLocationJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","toLocationJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localUserNotesJoinTracker){
                                            if (localUserNotesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                            }
                                           localUserNotesJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","userNotesJoin"),
                                               xmlWriter);
                                        } if (localVendorJoinTracker){
                                            if (localVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                            }
                                           localVendorJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vendorJoin"),
                                               xmlWriter);
                                        } if (localVendorLineJoinTracker){
                                            if (localVendorLineJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorLineJoin cannot be null!!");
                                            }
                                           localVendorLineJoin.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vendorLineJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","TransactionSearch"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localAccountJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountJoin"));
                            
                            
                                    if (localAccountJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountJoin cannot be null!!");
                                    }
                                    elementList.add(localAccountJoin);
                                } if (localAccountingPeriodJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingPeriodJoin"));
                            
                            
                                    if (localAccountingPeriodJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingPeriodJoin cannot be null!!");
                                    }
                                    elementList.add(localAccountingPeriodJoin);
                                } if (localAccountingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "accountingTransactionJoin"));
                            
                            
                                    if (localAccountingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localAccountingTransactionJoin);
                                } if (localAdvanceToApplyAccountJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "advanceToApplyAccountJoin"));
                            
                            
                                    if (localAdvanceToApplyAccountJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("advanceToApplyAccountJoin cannot be null!!");
                                    }
                                    elementList.add(localAdvanceToApplyAccountJoin);
                                } if (localAppliedToTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "appliedToTransactionJoin"));
                            
                            
                                    if (localAppliedToTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("appliedToTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localAppliedToTransactionJoin);
                                } if (localApplyingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "applyingTransactionJoin"));
                            
                            
                                    if (localApplyingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("applyingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localApplyingTransactionJoin);
                                } if (localBillingAddressJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingAddressJoin"));
                            
                            
                                    if (localBillingAddressJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingAddressJoin cannot be null!!");
                                    }
                                    elementList.add(localBillingAddressJoin);
                                } if (localBillingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingTransactionJoin"));
                            
                            
                                    if (localBillingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localBillingTransactionJoin);
                                } if (localBinNumberJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "binNumberJoin"));
                            
                            
                                    if (localBinNumberJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("binNumberJoin cannot be null!!");
                                    }
                                    elementList.add(localBinNumberJoin);
                                } if (localCallJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "callJoin"));
                            
                            
                                    if (localCallJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                    }
                                    elementList.add(localCallJoin);
                                } if (localCaseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "caseJoin"));
                            
                            
                                    if (localCaseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                    }
                                    elementList.add(localCaseJoin);
                                } if (localClassJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "classJoin"));
                            
                            
                                    if (localClassJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("classJoin cannot be null!!");
                                    }
                                    elementList.add(localClassJoin);
                                } if (localCogsPurchaseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "cogsPurchaseJoin"));
                            
                            
                                    if (localCogsPurchaseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("cogsPurchaseJoin cannot be null!!");
                                    }
                                    elementList.add(localCogsPurchaseJoin);
                                } if (localCogsSaleJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "cogsSaleJoin"));
                            
                            
                                    if (localCogsSaleJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("cogsSaleJoin cannot be null!!");
                                    }
                                    elementList.add(localCogsSaleJoin);
                                } if (localContactPrimaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "contactPrimaryJoin"));
                            
                            
                                    if (localContactPrimaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("contactPrimaryJoin cannot be null!!");
                                    }
                                    elementList.add(localContactPrimaryJoin);
                                } if (localCreatedFromJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdFromJoin"));
                            
                            
                                    if (localCreatedFromJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdFromJoin cannot be null!!");
                                    }
                                    elementList.add(localCreatedFromJoin);
                                } if (localCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customerJoin"));
                            
                            
                                    if (localCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerJoin);
                                } if (localCustomerMainJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customerMainJoin"));
                            
                            
                                    if (localCustomerMainJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerMainJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerMainJoin);
                                } if (localDepartmentJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "departmentJoin"));
                            
                            
                                    if (localDepartmentJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("departmentJoin cannot be null!!");
                                    }
                                    elementList.add(localDepartmentJoin);
                                } if (localDepositTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "depositTransactionJoin"));
                            
                            
                                    if (localDepositTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("depositTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localDepositTransactionJoin);
                                } if (localEmployeeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "employeeJoin"));
                            
                            
                                    if (localEmployeeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                    }
                                    elementList.add(localEmployeeJoin);
                                } if (localEventJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "eventJoin"));
                            
                            
                                    if (localEventJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                    }
                                    elementList.add(localEventJoin);
                                } if (localExpenseCategoryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "expenseCategoryJoin"));
                            
                            
                                    if (localExpenseCategoryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("expenseCategoryJoin cannot be null!!");
                                    }
                                    elementList.add(localExpenseCategoryJoin);
                                } if (localFileJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fileJoin"));
                            
                            
                                    if (localFileJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                    }
                                    elementList.add(localFileJoin);
                                } if (localFromLocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fromLocationJoin"));
                            
                            
                                    if (localFromLocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fromLocationJoin cannot be null!!");
                                    }
                                    elementList.add(localFromLocationJoin);
                                } if (localFulfillingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fulfillingTransactionJoin"));
                            
                            
                                    if (localFulfillingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fulfillingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localFulfillingTransactionJoin);
                                } if (localHeaderBillingAccountJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "headerBillingAccountJoin"));
                            
                            
                                    if (localHeaderBillingAccountJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("headerBillingAccountJoin cannot be null!!");
                                    }
                                    elementList.add(localHeaderBillingAccountJoin);
                                } if (localInventoryDetailJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "inventoryDetailJoin"));
                            
                            
                                    if (localInventoryDetailJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryDetailJoin cannot be null!!");
                                    }
                                    elementList.add(localInventoryDetailJoin);
                                } if (localItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemJoin"));
                            
                            
                                    if (localItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                    }
                                    elementList.add(localItemJoin);
                                } if (localItemNumberJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemNumberJoin"));
                            
                            
                                    if (localItemNumberJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemNumberJoin cannot be null!!");
                                    }
                                    elementList.add(localItemNumberJoin);
                                } if (localJobJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "jobJoin"));
                            
                            
                                    if (localJobJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                    }
                                    elementList.add(localJobJoin);
                                } if (localJobMainJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "jobMainJoin"));
                            
                            
                                    if (localJobMainJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobMainJoin cannot be null!!");
                                    }
                                    elementList.add(localJobMainJoin);
                                } if (localLeadSourceJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "leadSourceJoin"));
                            
                            
                                    if (localLeadSourceJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadSourceJoin cannot be null!!");
                                    }
                                    elementList.add(localLeadSourceJoin);
                                } if (localLineBillingAccountJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "lineBillingAccountJoin"));
                            
                            
                                    if (localLineBillingAccountJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("lineBillingAccountJoin cannot be null!!");
                                    }
                                    elementList.add(localLineBillingAccountJoin);
                                } if (localLocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "locationJoin"));
                            
                            
                                    if (localLocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationJoin cannot be null!!");
                                    }
                                    elementList.add(localLocationJoin);
                                } if (localManufacturingOperationTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "manufacturingOperationTaskJoin"));
                            
                            
                                    if (localManufacturingOperationTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingOperationTaskJoin cannot be null!!");
                                    }
                                    elementList.add(localManufacturingOperationTaskJoin);
                                } if (localMessagesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "messagesJoin"));
                            
                            
                                    if (localMessagesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesJoin);
                                } if (localNextApproverJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "nextApproverJoin"));
                            
                            
                                    if (localNextApproverJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("nextApproverJoin cannot be null!!");
                                    }
                                    elementList.add(localNextApproverJoin);
                                } if (localOpportunityJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "opportunityJoin"));
                            
                            
                                    if (localOpportunityJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                    }
                                    elementList.add(localOpportunityJoin);
                                } if (localPaidTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "paidTransactionJoin"));
                            
                            
                                    if (localPaidTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("paidTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localPaidTransactionJoin);
                                } if (localPartnerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "partnerJoin"));
                            
                            
                                    if (localPartnerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                    }
                                    elementList.add(localPartnerJoin);
                                } if (localPayingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "payingTransactionJoin"));
                            
                            
                                    if (localPayingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("payingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localPayingTransactionJoin);
                                } if (localPayrollItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "payrollItemJoin"));
                            
                            
                                    if (localPayrollItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("payrollItemJoin cannot be null!!");
                                    }
                                    elementList.add(localPayrollItemJoin);
                                } if (localProjectTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "projectTaskJoin"));
                            
                            
                                    if (localProjectTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectTaskJoin cannot be null!!");
                                    }
                                    elementList.add(localProjectTaskJoin);
                                } if (localPurchaseOrderJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "purchaseOrderJoin"));
                            
                            
                                    if (localPurchaseOrderJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderJoin cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderJoin);
                                } if (localRequestorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "requestorJoin"));
                            
                            
                                    if (localRequestorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("requestorJoin cannot be null!!");
                                    }
                                    elementList.add(localRequestorJoin);
                                } if (localRevCommittingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revCommittingTransactionJoin"));
                            
                            
                                    if (localRevCommittingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("revCommittingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localRevCommittingTransactionJoin);
                                } if (localRevisionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revisionJoin"));
                            
                            
                                    if (localRevisionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("revisionJoin cannot be null!!");
                                    }
                                    elementList.add(localRevisionJoin);
                                } if (localRevRecScheduleJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecScheduleJoin"));
                            
                            
                                    if (localRevRecScheduleJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecScheduleJoin cannot be null!!");
                                    }
                                    elementList.add(localRevRecScheduleJoin);
                                } if (localRgPostingTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "rgPostingTransactionJoin"));
                            
                            
                                    if (localRgPostingTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("rgPostingTransactionJoin cannot be null!!");
                                    }
                                    elementList.add(localRgPostingTransactionJoin);
                                } if (localSalesOrderJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesOrderJoin"));
                            
                            
                                    if (localSalesOrderJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesOrderJoin cannot be null!!");
                                    }
                                    elementList.add(localSalesOrderJoin);
                                } if (localSalesRepJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "salesRepJoin"));
                            
                            
                                    if (localSalesRepJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRepJoin cannot be null!!");
                                    }
                                    elementList.add(localSalesRepJoin);
                                } if (localShippingAddressJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shippingAddressJoin"));
                            
                            
                                    if (localShippingAddressJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingAddressJoin cannot be null!!");
                                    }
                                    elementList.add(localShippingAddressJoin);
                                } if (localSubsidiaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiaryJoin"));
                            
                            
                                    if (localSubsidiaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryJoin cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryJoin);
                                } if (localTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taskJoin"));
                            
                            
                                    if (localTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                    }
                                    elementList.add(localTaskJoin);
                                } if (localTaxCodeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxCodeJoin"));
                            
                            
                                    if (localTaxCodeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxCodeJoin cannot be null!!");
                                    }
                                    elementList.add(localTaxCodeJoin);
                                } if (localTaxDetailJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailJoin"));
                            
                            
                                    if (localTaxDetailJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxDetailJoin cannot be null!!");
                                    }
                                    elementList.add(localTaxDetailJoin);
                                } if (localTaxItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxItemJoin"));
                            
                            
                                    if (localTaxItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxItemJoin cannot be null!!");
                                    }
                                    elementList.add(localTaxItemJoin);
                                } if (localTimeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "timeJoin"));
                            
                            
                                    if (localTimeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                    }
                                    elementList.add(localTimeJoin);
                                } if (localToLocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "toLocationJoin"));
                            
                            
                                    if (localToLocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("toLocationJoin cannot be null!!");
                                    }
                                    elementList.add(localToLocationJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localUserNotesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "userNotesJoin"));
                            
                            
                                    if (localUserNotesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                    }
                                    elementList.add(localUserNotesJoin);
                                } if (localVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vendorJoin"));
                            
                            
                                    if (localVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorJoin);
                                } if (localVendorLineJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vendorLineJoin"));
                            
                            
                                    if (localVendorLineJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorLineJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorLineJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionSearch parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionSearch object =
                new TransactionSearch();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TransactionSearch".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TransactionSearch)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list66 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountJoin").equals(reader.getName())){
                                
                                                object.setAccountJoin(com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingPeriodJoin").equals(reader.getName())){
                                
                                                object.setAccountingPeriodJoin(com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","accountingTransactionJoin").equals(reader.getName())){
                                
                                                object.setAccountingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","advanceToApplyAccountJoin").equals(reader.getName())){
                                
                                                object.setAdvanceToApplyAccountJoin(com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","appliedToTransactionJoin").equals(reader.getName())){
                                
                                                object.setAppliedToTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","applyingTransactionJoin").equals(reader.getName())){
                                
                                                object.setApplyingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingAddressJoin").equals(reader.getName())){
                                
                                                object.setBillingAddressJoin(com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingTransactionJoin").equals(reader.getName())){
                                
                                                object.setBillingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","binNumberJoin").equals(reader.getName())){
                                
                                                object.setBinNumberJoin(com.netsuite.webservices.platform.common_2017_2.BinSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","callJoin").equals(reader.getName())){
                                
                                                object.setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","caseJoin").equals(reader.getName())){
                                
                                                object.setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","classJoin").equals(reader.getName())){
                                
                                                object.setClassJoin(com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","cogsPurchaseJoin").equals(reader.getName())){
                                
                                                object.setCogsPurchaseJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","cogsSaleJoin").equals(reader.getName())){
                                
                                                object.setCogsSaleJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","contactPrimaryJoin").equals(reader.getName())){
                                
                                                object.setContactPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdFromJoin").equals(reader.getName())){
                                
                                                object.setCreatedFromJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customerJoin").equals(reader.getName())){
                                
                                                object.setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customerMainJoin").equals(reader.getName())){
                                
                                                object.setCustomerMainJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","departmentJoin").equals(reader.getName())){
                                
                                                object.setDepartmentJoin(com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","depositTransactionJoin").equals(reader.getName())){
                                
                                                object.setDepositTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","employeeJoin").equals(reader.getName())){
                                
                                                object.setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","eventJoin").equals(reader.getName())){
                                
                                                object.setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","expenseCategoryJoin").equals(reader.getName())){
                                
                                                object.setExpenseCategoryJoin(com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fileJoin").equals(reader.getName())){
                                
                                                object.setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fromLocationJoin").equals(reader.getName())){
                                
                                                object.setFromLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fulfillingTransactionJoin").equals(reader.getName())){
                                
                                                object.setFulfillingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","headerBillingAccountJoin").equals(reader.getName())){
                                
                                                object.setHeaderBillingAccountJoin(com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","inventoryDetailJoin").equals(reader.getName())){
                                
                                                object.setInventoryDetailJoin(com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemJoin").equals(reader.getName())){
                                
                                                object.setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemNumberJoin").equals(reader.getName())){
                                
                                                object.setItemNumberJoin(com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","jobJoin").equals(reader.getName())){
                                
                                                object.setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","jobMainJoin").equals(reader.getName())){
                                
                                                object.setJobMainJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","leadSourceJoin").equals(reader.getName())){
                                
                                                object.setLeadSourceJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","lineBillingAccountJoin").equals(reader.getName())){
                                
                                                object.setLineBillingAccountJoin(com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","locationJoin").equals(reader.getName())){
                                
                                                object.setLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","manufacturingOperationTaskJoin").equals(reader.getName())){
                                
                                                object.setManufacturingOperationTaskJoin(com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","messagesJoin").equals(reader.getName())){
                                
                                                object.setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","nextApproverJoin").equals(reader.getName())){
                                
                                                object.setNextApproverJoin(com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","opportunityJoin").equals(reader.getName())){
                                
                                                object.setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","paidTransactionJoin").equals(reader.getName())){
                                
                                                object.setPaidTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","partnerJoin").equals(reader.getName())){
                                
                                                object.setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","payingTransactionJoin").equals(reader.getName())){
                                
                                                object.setPayingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","payrollItemJoin").equals(reader.getName())){
                                
                                                object.setPayrollItemJoin(com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","projectTaskJoin").equals(reader.getName())){
                                
                                                object.setProjectTaskJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","purchaseOrderJoin").equals(reader.getName())){
                                
                                                object.setPurchaseOrderJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","requestorJoin").equals(reader.getName())){
                                
                                                object.setRequestorJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revCommittingTransactionJoin").equals(reader.getName())){
                                
                                                object.setRevCommittingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revisionJoin").equals(reader.getName())){
                                
                                                object.setRevisionJoin(com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecScheduleJoin").equals(reader.getName())){
                                
                                                object.setRevRecScheduleJoin(com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","rgPostingTransactionJoin").equals(reader.getName())){
                                
                                                object.setRgPostingTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesOrderJoin").equals(reader.getName())){
                                
                                                object.setSalesOrderJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","salesRepJoin").equals(reader.getName())){
                                
                                                object.setSalesRepJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shippingAddressJoin").equals(reader.getName())){
                                
                                                object.setShippingAddressJoin(com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subsidiaryJoin").equals(reader.getName())){
                                
                                                object.setSubsidiaryJoin(com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taskJoin").equals(reader.getName())){
                                
                                                object.setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxCodeJoin").equals(reader.getName())){
                                
                                                object.setTaxCodeJoin(com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxDetailJoin").equals(reader.getName())){
                                
                                                object.setTaxDetailJoin(com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxItemJoin").equals(reader.getName())){
                                
                                                object.setTaxItemJoin(com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","timeJoin").equals(reader.getName())){
                                
                                                object.setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","toLocationJoin").equals(reader.getName())){
                                
                                                object.setToLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","userNotesJoin").equals(reader.getName())){
                                
                                                object.setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vendorJoin").equals(reader.getName())){
                                
                                                object.setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vendorLineJoin").equals(reader.getName())){
                                
                                                object.setVendorLineJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list66.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone66 = false;
                                                        while(!loopDone66){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone66 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list66.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone66 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.class,
                                                                list66));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    