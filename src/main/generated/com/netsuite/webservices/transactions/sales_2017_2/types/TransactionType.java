
/**
 * TransactionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2.types;
            

            /**
            *  TransactionType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.sales_2017_2.transactions.webservices.netsuite.com",
                "TransactionType",
                "ns20");

            

                        /**
                        * field for TransactionType
                        */

                        
                                    protected java.lang.String localTransactionType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected TransactionType(java.lang.String value, boolean isRegisterValue) {
                                    localTransactionType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localTransactionType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __assemblyBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_assemblyBuild");
                                
                                    public static final java.lang.String __assemblyUnbuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_assemblyUnbuild");
                                
                                    public static final java.lang.String __binTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_binTransfer");
                                
                                    public static final java.lang.String __binWorksheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_binWorksheet");
                                
                                    public static final java.lang.String __cashRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashRefund");
                                
                                    public static final java.lang.String __cashSale =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashSale");
                                
                                    public static final java.lang.String __check =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_check");
                                
                                    public static final java.lang.String __creditMemo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditMemo");
                                
                                    public static final java.lang.String __custom =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_custom");
                                
                                    public static final java.lang.String __customerDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerDeposit");
                                
                                    public static final java.lang.String __customerPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerPayment");
                                
                                    public static final java.lang.String __customerRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerRefund");
                                
                                    public static final java.lang.String __deposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deposit");
                                
                                    public static final java.lang.String __depositApplication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_depositApplication");
                                
                                    public static final java.lang.String __estimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimate");
                                
                                    public static final java.lang.String __expenseReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReport");
                                
                                    public static final java.lang.String __inboundShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inboundShipment");
                                
                                    public static final java.lang.String __inventoryAdjustment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryAdjustment");
                                
                                    public static final java.lang.String __inventoryCostRevaluation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryCostRevaluation");
                                
                                    public static final java.lang.String __inventoryTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryTransfer");
                                
                                    public static final java.lang.String __invoice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoice");
                                
                                    public static final java.lang.String __itemFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemFulfillment");
                                
                                    public static final java.lang.String __itemReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemReceipt");
                                
                                    public static final java.lang.String __journal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_journal");
                                
                                    public static final java.lang.String __opportunity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunity");
                                
                                    public static final java.lang.String __paycheck =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheck");
                                
                                    public static final java.lang.String __paycheckJournal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckJournal");
                                
                                    public static final java.lang.String __purchaseOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrder");
                                
                                    public static final java.lang.String __requisition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisition");
                                
                                    public static final java.lang.String __returnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorization");
                                
                                    public static final java.lang.String __salesOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrder");
                                
                                    public static final java.lang.String __transferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrder");
                                
                                    public static final java.lang.String __vendorBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorBill");
                                
                                    public static final java.lang.String __vendorCredit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorCredit");
                                
                                    public static final java.lang.String __vendorPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorPayment");
                                
                                    public static final java.lang.String __vendorReturnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorization");
                                
                                    public static final java.lang.String __workOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrder");
                                
                                    public static final java.lang.String __workOrderClose =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderClose");
                                
                                    public static final java.lang.String __workOrderCompletion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderCompletion");
                                
                                    public static final java.lang.String __workOrderIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderIssue");
                                
                                public static final TransactionType _assemblyBuild =
                                    new TransactionType(__assemblyBuild,true);
                            
                                public static final TransactionType _assemblyUnbuild =
                                    new TransactionType(__assemblyUnbuild,true);
                            
                                public static final TransactionType _binTransfer =
                                    new TransactionType(__binTransfer,true);
                            
                                public static final TransactionType _binWorksheet =
                                    new TransactionType(__binWorksheet,true);
                            
                                public static final TransactionType _cashRefund =
                                    new TransactionType(__cashRefund,true);
                            
                                public static final TransactionType _cashSale =
                                    new TransactionType(__cashSale,true);
                            
                                public static final TransactionType _check =
                                    new TransactionType(__check,true);
                            
                                public static final TransactionType _creditMemo =
                                    new TransactionType(__creditMemo,true);
                            
                                public static final TransactionType _custom =
                                    new TransactionType(__custom,true);
                            
                                public static final TransactionType _customerDeposit =
                                    new TransactionType(__customerDeposit,true);
                            
                                public static final TransactionType _customerPayment =
                                    new TransactionType(__customerPayment,true);
                            
                                public static final TransactionType _customerRefund =
                                    new TransactionType(__customerRefund,true);
                            
                                public static final TransactionType _deposit =
                                    new TransactionType(__deposit,true);
                            
                                public static final TransactionType _depositApplication =
                                    new TransactionType(__depositApplication,true);
                            
                                public static final TransactionType _estimate =
                                    new TransactionType(__estimate,true);
                            
                                public static final TransactionType _expenseReport =
                                    new TransactionType(__expenseReport,true);
                            
                                public static final TransactionType _inboundShipment =
                                    new TransactionType(__inboundShipment,true);
                            
                                public static final TransactionType _inventoryAdjustment =
                                    new TransactionType(__inventoryAdjustment,true);
                            
                                public static final TransactionType _inventoryCostRevaluation =
                                    new TransactionType(__inventoryCostRevaluation,true);
                            
                                public static final TransactionType _inventoryTransfer =
                                    new TransactionType(__inventoryTransfer,true);
                            
                                public static final TransactionType _invoice =
                                    new TransactionType(__invoice,true);
                            
                                public static final TransactionType _itemFulfillment =
                                    new TransactionType(__itemFulfillment,true);
                            
                                public static final TransactionType _itemReceipt =
                                    new TransactionType(__itemReceipt,true);
                            
                                public static final TransactionType _journal =
                                    new TransactionType(__journal,true);
                            
                                public static final TransactionType _opportunity =
                                    new TransactionType(__opportunity,true);
                            
                                public static final TransactionType _paycheck =
                                    new TransactionType(__paycheck,true);
                            
                                public static final TransactionType _paycheckJournal =
                                    new TransactionType(__paycheckJournal,true);
                            
                                public static final TransactionType _purchaseOrder =
                                    new TransactionType(__purchaseOrder,true);
                            
                                public static final TransactionType _requisition =
                                    new TransactionType(__requisition,true);
                            
                                public static final TransactionType _returnAuthorization =
                                    new TransactionType(__returnAuthorization,true);
                            
                                public static final TransactionType _salesOrder =
                                    new TransactionType(__salesOrder,true);
                            
                                public static final TransactionType _transferOrder =
                                    new TransactionType(__transferOrder,true);
                            
                                public static final TransactionType _vendorBill =
                                    new TransactionType(__vendorBill,true);
                            
                                public static final TransactionType _vendorCredit =
                                    new TransactionType(__vendorCredit,true);
                            
                                public static final TransactionType _vendorPayment =
                                    new TransactionType(__vendorPayment,true);
                            
                                public static final TransactionType _vendorReturnAuthorization =
                                    new TransactionType(__vendorReturnAuthorization,true);
                            
                                public static final TransactionType _workOrder =
                                    new TransactionType(__workOrder,true);
                            
                                public static final TransactionType _workOrderClose =
                                    new TransactionType(__workOrderClose,true);
                            
                                public static final TransactionType _workOrderCompletion =
                                    new TransactionType(__workOrderCompletion,true);
                            
                                public static final TransactionType _workOrderIssue =
                                    new TransactionType(__workOrderIssue,true);
                            

                                public java.lang.String getValue() { return localTransactionType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localTransactionType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.sales_2017_2.transactions.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":TransactionType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "TransactionType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localTransactionType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("TransactionType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localTransactionType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns20";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static TransactionType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    TransactionType enumeration = (TransactionType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static TransactionType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static TransactionType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return TransactionType.Factory.fromString(content,namespaceUri);
                    } else {
                       return TransactionType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TransactionType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = TransactionType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = TransactionType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    