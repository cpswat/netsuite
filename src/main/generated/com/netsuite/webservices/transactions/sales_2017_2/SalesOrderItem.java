
/**
 * SalesOrderItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.sales_2017_2;
            

            /**
            *  SalesOrderItem bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SalesOrderItem
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = SalesOrderItem
                Namespace URI = urn:sales_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns21
                */
            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for Subscription
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubscription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubscriptionTracker = false ;

                           public boolean isSubscriptionSpecified(){
                               return localSubscriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubscription(){
                               return localSubscription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subscription
                               */
                               public void setSubscription(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubscriptionTracker = param != null;
                                   
                                            this.localSubscription=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for QuantityAvailable
                        */

                        
                                    protected double localQuantityAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityAvailableTracker = false ;

                           public boolean isQuantityAvailableSpecified(){
                               return localQuantityAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityAvailable(){
                               return localQuantityAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityAvailable
                               */
                               public void setQuantityAvailable(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityAvailableTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityAvailable=param;
                                    

                               }
                            

                        /**
                        * field for ExpandItemGroup
                        */

                        
                                    protected boolean localExpandItemGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpandItemGroupTracker = false ;

                           public boolean isExpandItemGroupSpecified(){
                               return localExpandItemGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExpandItemGroup(){
                               return localExpandItemGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpandItemGroup
                               */
                               public void setExpandItemGroup(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExpandItemGroupTracker =
                                       true;
                                   
                                            this.localExpandItemGroup=param;
                                    

                               }
                            

                        /**
                        * field for LineUniqueKey
                        */

                        
                                    protected long localLineUniqueKey ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineUniqueKeyTracker = false ;

                           public boolean isLineUniqueKeySpecified(){
                               return localLineUniqueKeyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLineUniqueKey(){
                               return localLineUniqueKey;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LineUniqueKey
                               */
                               public void setLineUniqueKey(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineUniqueKeyTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLineUniqueKey=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnHand
                        */

                        
                                    protected double localQuantityOnHand ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnHandTracker = false ;

                           public boolean isQuantityOnHandSpecified(){
                               return localQuantityOnHandTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnHand(){
                               return localQuantityOnHand;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnHand
                               */
                               public void setQuantityOnHand(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnHandTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnHand=param;
                                    

                               }
                            

                        /**
                        * field for Quantity
                        */

                        
                                    protected double localQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityTracker = false ;

                           public boolean isQuantitySpecified(){
                               return localQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantity(){
                               return localQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Quantity
                               */
                               public void setQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantity=param;
                                    

                               }
                            

                        /**
                        * field for Units
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTracker = false ;

                           public boolean isUnitsSpecified(){
                               return localUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnits(){
                               return localUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Units
                               */
                               public void setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitsTracker = param != null;
                                   
                                            this.localUnits=param;
                                    

                               }
                            

                        /**
                        * field for InventoryDetail
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryDetail localInventoryDetail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryDetailTracker = false ;

                           public boolean isInventoryDetailSpecified(){
                               return localInventoryDetailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryDetail
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryDetail getInventoryDetail(){
                               return localInventoryDetail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryDetail
                               */
                               public void setInventoryDetail(com.netsuite.webservices.platform.common_2017_2.InventoryDetail param){
                            localInventoryDetailTracker = param != null;
                                   
                                            this.localInventoryDetail=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for Price
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriceTracker = false ;

                           public boolean isPriceSpecified(){
                               return localPriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPrice(){
                               return localPrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Price
                               */
                               public void setPrice(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPriceTracker = param != null;
                                   
                                            this.localPrice=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected java.lang.String localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(java.lang.String param){
                            localRateTracker = param != null;
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for SerialNumbers
                        */

                        
                                    protected java.lang.String localSerialNumbers ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSerialNumbersTracker = false ;

                           public boolean isSerialNumbersSpecified(){
                               return localSerialNumbersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSerialNumbers(){
                               return localSerialNumbers;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SerialNumbers
                               */
                               public void setSerialNumbers(java.lang.String param){
                            localSerialNumbersTracker = param != null;
                                   
                                            this.localSerialNumbers=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected double localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for IsTaxable
                        */

                        
                                    protected boolean localIsTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTaxableTracker = false ;

                           public boolean isIsTaxableSpecified(){
                               return localIsTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsTaxable(){
                               return localIsTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTaxable
                               */
                               public void setIsTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsTaxableTracker =
                                       true;
                                   
                                            this.localIsTaxable=param;
                                    

                               }
                            

                        /**
                        * field for CommitInventory
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCommitInventory localCommitInventory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommitInventoryTracker = false ;

                           public boolean isCommitInventorySpecified(){
                               return localCommitInventoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCommitInventory
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCommitInventory getCommitInventory(){
                               return localCommitInventory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CommitInventory
                               */
                               public void setCommitInventory(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCommitInventory param){
                            localCommitInventoryTracker = param != null;
                                   
                                            this.localCommitInventory=param;
                                    

                               }
                            

                        /**
                        * field for OrderPriority
                        */

                        
                                    protected double localOrderPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderPriorityTracker = false ;

                           public boolean isOrderPrioritySpecified(){
                               return localOrderPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getOrderPriority(){
                               return localOrderPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderPriority
                               */
                               public void setOrderPriority(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localOrderPriorityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localOrderPriority=param;
                                    

                               }
                            

                        /**
                        * field for LicenseCode
                        */

                        
                                    protected java.lang.String localLicenseCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLicenseCodeTracker = false ;

                           public boolean isLicenseCodeSpecified(){
                               return localLicenseCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLicenseCode(){
                               return localLicenseCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LicenseCode
                               */
                               public void setLicenseCode(java.lang.String param){
                            localLicenseCodeTracker = param != null;
                                   
                                            this.localLicenseCode=param;
                                    

                               }
                            

                        /**
                        * field for Options
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localOptions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOptionsTracker = false ;

                           public boolean isOptionsSpecified(){
                               return localOptionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getOptions(){
                               return localOptions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Options
                               */
                               public void setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localOptionsTracker = param != null;
                                   
                                            this.localOptions=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for CreatePo
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCreatePo localCreatePo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatePoTracker = false ;

                           public boolean isCreatePoSpecified(){
                               return localCreatePoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCreatePo
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCreatePo getCreatePo(){
                               return localCreatePo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatePo
                               */
                               public void setCreatePo(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCreatePo param){
                            localCreatePoTracker = param != null;
                                   
                                            this.localCreatePo=param;
                                    

                               }
                            

                        /**
                        * field for CreatedPo
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreatedPo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedPoTracker = false ;

                           public boolean isCreatedPoSpecified(){
                               return localCreatedPoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreatedPo(){
                               return localCreatedPo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedPo
                               */
                               public void setCreatedPo(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreatedPoTracker = param != null;
                                   
                                            this.localCreatedPo=param;
                                    

                               }
                            

                        /**
                        * field for AltSalesAmt
                        */

                        
                                    protected double localAltSalesAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltSalesAmtTracker = false ;

                           public boolean isAltSalesAmtSpecified(){
                               return localAltSalesAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAltSalesAmt(){
                               return localAltSalesAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltSalesAmt
                               */
                               public void setAltSalesAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAltSalesAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAltSalesAmt=param;
                                    

                               }
                            

                        /**
                        * field for CreateWo
                        */

                        
                                    protected boolean localCreateWo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreateWoTracker = false ;

                           public boolean isCreateWoSpecified(){
                               return localCreateWoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCreateWo(){
                               return localCreateWo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreateWo
                               */
                               public void setCreateWo(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localCreateWoTracker =
                                       true;
                                   
                                            this.localCreateWo=param;
                                    

                               }
                            

                        /**
                        * field for PoVendor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPoVendor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoVendorTracker = false ;

                           public boolean isPoVendorSpecified(){
                               return localPoVendorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPoVendor(){
                               return localPoVendor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoVendor
                               */
                               public void setPoVendor(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPoVendorTracker = param != null;
                                   
                                            this.localPoVendor=param;
                                    

                               }
                            

                        /**
                        * field for PoCurrency
                        */

                        
                                    protected java.lang.String localPoCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoCurrencyTracker = false ;

                           public boolean isPoCurrencySpecified(){
                               return localPoCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPoCurrency(){
                               return localPoCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoCurrency
                               */
                               public void setPoCurrency(java.lang.String param){
                            localPoCurrencyTracker = param != null;
                                   
                                            this.localPoCurrency=param;
                                    

                               }
                            

                        /**
                        * field for PoRate
                        */

                        
                                    protected double localPoRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoRateTracker = false ;

                           public boolean isPoRateSpecified(){
                               return localPoRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPoRate(){
                               return localPoRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoRate
                               */
                               public void setPoRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPoRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPoRate=param;
                                    

                               }
                            

                        /**
                        * field for RevRecSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleTracker = false ;

                           public boolean isRevRecScheduleSpecified(){
                               return localRevRecScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecSchedule(){
                               return localRevRecSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecSchedule
                               */
                               public void setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecScheduleTracker = param != null;
                                   
                                            this.localRevRecSchedule=param;
                                    

                               }
                            

                        /**
                        * field for RevRecStartDate
                        */

                        
                                    protected java.util.Calendar localRevRecStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecStartDateTracker = false ;

                           public boolean isRevRecStartDateSpecified(){
                               return localRevRecStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecStartDate(){
                               return localRevRecStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecStartDate
                               */
                               public void setRevRecStartDate(java.util.Calendar param){
                            localRevRecStartDateTracker = param != null;
                                   
                                            this.localRevRecStartDate=param;
                                    

                               }
                            

                        /**
                        * field for RevRecTermInMonths
                        */

                        
                                    protected long localRevRecTermInMonths ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecTermInMonthsTracker = false ;

                           public boolean isRevRecTermInMonthsSpecified(){
                               return localRevRecTermInMonthsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getRevRecTermInMonths(){
                               return localRevRecTermInMonths;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecTermInMonths
                               */
                               public void setRevRecTermInMonths(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localRevRecTermInMonthsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localRevRecTermInMonths=param;
                                    

                               }
                            

                        /**
                        * field for RevRecEndDate
                        */

                        
                                    protected java.util.Calendar localRevRecEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecEndDateTracker = false ;

                           public boolean isRevRecEndDateSpecified(){
                               return localRevRecEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getRevRecEndDate(){
                               return localRevRecEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecEndDate
                               */
                               public void setRevRecEndDate(java.util.Calendar param){
                            localRevRecEndDateTracker = param != null;
                                   
                                            this.localRevRecEndDate=param;
                                    

                               }
                            

                        /**
                        * field for DeferRevRec
                        */

                        
                                    protected boolean localDeferRevRec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferRevRecTracker = false ;

                           public boolean isDeferRevRecSpecified(){
                               return localDeferRevRecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDeferRevRec(){
                               return localDeferRevRec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferRevRec
                               */
                               public void setDeferRevRec(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDeferRevRecTracker =
                                       true;
                                   
                                            this.localDeferRevRec=param;
                                    

                               }
                            

                        /**
                        * field for IsClosed
                        */

                        
                                    protected boolean localIsClosed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsClosedTracker = false ;

                           public boolean isIsClosedSpecified(){
                               return localIsClosedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsClosed(){
                               return localIsClosed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsClosed
                               */
                               public void setIsClosed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsClosedTracker =
                                       true;
                                   
                                            this.localIsClosed=param;
                                    

                               }
                            

                        /**
                        * field for ItemFulfillmentChoice
                        */

                        
                                    protected com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemFulfillmentChoice localItemFulfillmentChoice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemFulfillmentChoiceTracker = false ;

                           public boolean isItemFulfillmentChoiceSpecified(){
                               return localItemFulfillmentChoiceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemFulfillmentChoice
                           */
                           public  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemFulfillmentChoice getItemFulfillmentChoice(){
                               return localItemFulfillmentChoice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemFulfillmentChoice
                               */
                               public void setItemFulfillmentChoice(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemFulfillmentChoice param){
                            localItemFulfillmentChoiceTracker = param != null;
                                   
                                            this.localItemFulfillmentChoice=param;
                                    

                               }
                            

                        /**
                        * field for CatchUpPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCatchUpPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCatchUpPeriodTracker = false ;

                           public boolean isCatchUpPeriodSpecified(){
                               return localCatchUpPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCatchUpPeriod(){
                               return localCatchUpPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CatchUpPeriod
                               */
                               public void setCatchUpPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCatchUpPeriodTracker = param != null;
                                   
                                            this.localCatchUpPeriod=param;
                                    

                               }
                            

                        /**
                        * field for BillingSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingScheduleTracker = false ;

                           public boolean isBillingScheduleSpecified(){
                               return localBillingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingSchedule(){
                               return localBillingSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingSchedule
                               */
                               public void setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingScheduleTracker = param != null;
                                   
                                            this.localBillingSchedule=param;
                                    

                               }
                            

                        /**
                        * field for FromJob
                        */

                        
                                    protected boolean localFromJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFromJobTracker = false ;

                           public boolean isFromJobSpecified(){
                               return localFromJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getFromJob(){
                               return localFromJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FromJob
                               */
                               public void setFromJob(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localFromJobTracker =
                                       true;
                                   
                                            this.localFromJob=param;
                                    

                               }
                            

                        /**
                        * field for GrossAmt
                        */

                        
                                    protected double localGrossAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGrossAmtTracker = false ;

                           public boolean isGrossAmtSpecified(){
                               return localGrossAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGrossAmt(){
                               return localGrossAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GrossAmt
                               */
                               public void setGrossAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGrossAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGrossAmt=param;
                                    

                               }
                            

                        /**
                        * field for TaxAmount
                        */

                        
                                    protected double localTaxAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxAmountTracker = false ;

                           public boolean isTaxAmountSpecified(){
                               return localTaxAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxAmount(){
                               return localTaxAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxAmount
                               */
                               public void setTaxAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxAmount=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeFromRateRequest
                        */

                        
                                    protected boolean localExcludeFromRateRequest ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeFromRateRequestTracker = false ;

                           public boolean isExcludeFromRateRequestSpecified(){
                               return localExcludeFromRateRequestTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExcludeFromRateRequest(){
                               return localExcludeFromRateRequest;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeFromRateRequest
                               */
                               public void setExcludeFromRateRequest(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExcludeFromRateRequestTracker =
                                       true;
                                   
                                            this.localExcludeFromRateRequest=param;
                                    

                               }
                            

                        /**
                        * field for IsEstimate
                        */

                        
                                    protected boolean localIsEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsEstimateTracker = false ;

                           public boolean isIsEstimateSpecified(){
                               return localIsEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsEstimate(){
                               return localIsEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsEstimate
                               */
                               public void setIsEstimate(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsEstimateTracker =
                                       true;
                                   
                                            this.localIsEstimate=param;
                                    

                               }
                            

                        /**
                        * field for Line
                        */

                        
                                    protected long localLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineTracker = false ;

                           public boolean isLineSpecified(){
                               return localLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLine(){
                               return localLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Line
                               */
                               public void setLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLine=param;
                                    

                               }
                            

                        /**
                        * field for PercentComplete
                        */

                        
                                    protected double localPercentComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentCompleteTracker = false ;

                           public boolean isPercentCompleteSpecified(){
                               return localPercentCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPercentComplete(){
                               return localPercentComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentComplete
                               */
                               public void setPercentComplete(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPercentCompleteTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPercentComplete=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimateType
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType localCostEstimateType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTypeTracker = false ;

                           public boolean isCostEstimateTypeSpecified(){
                               return localCostEstimateTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType getCostEstimateType(){
                               return localCostEstimateType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimateType
                               */
                               public void setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType param){
                            localCostEstimateTypeTracker = param != null;
                                   
                                            this.localCostEstimateType=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimate
                        */

                        
                                    protected double localCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTracker = false ;

                           public boolean isCostEstimateSpecified(){
                               return localCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCostEstimate(){
                               return localCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimate
                               */
                               public void setCostEstimate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostEstimateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBackOrdered
                        */

                        
                                    protected double localQuantityBackOrdered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBackOrderedTracker = false ;

                           public boolean isQuantityBackOrderedSpecified(){
                               return localQuantityBackOrderedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityBackOrdered(){
                               return localQuantityBackOrdered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBackOrdered
                               */
                               public void setQuantityBackOrdered(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityBackOrderedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityBackOrdered=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBilled
                        */

                        
                                    protected double localQuantityBilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBilledTracker = false ;

                           public boolean isQuantityBilledSpecified(){
                               return localQuantityBilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityBilled(){
                               return localQuantityBilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBilled
                               */
                               public void setQuantityBilled(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityBilledTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityBilled=param;
                                    

                               }
                            

                        /**
                        * field for QuantityCommitted
                        */

                        
                                    protected double localQuantityCommitted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityCommittedTracker = false ;

                           public boolean isQuantityCommittedSpecified(){
                               return localQuantityCommittedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityCommitted(){
                               return localQuantityCommitted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityCommitted
                               */
                               public void setQuantityCommitted(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityCommittedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityCommitted=param;
                                    

                               }
                            

                        /**
                        * field for QuantityFulfilled
                        */

                        
                                    protected double localQuantityFulfilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityFulfilledTracker = false ;

                           public boolean isQuantityFulfilledSpecified(){
                               return localQuantityFulfilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityFulfilled(){
                               return localQuantityFulfilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityFulfilled
                               */
                               public void setQuantityFulfilled(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityFulfilledTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityFulfilled=param;
                                    

                               }
                            

                        /**
                        * field for QuantityPacked
                        */

                        
                                    protected double localQuantityPacked ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityPackedTracker = false ;

                           public boolean isQuantityPackedSpecified(){
                               return localQuantityPackedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityPacked(){
                               return localQuantityPacked;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityPacked
                               */
                               public void setQuantityPacked(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityPackedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityPacked=param;
                                    

                               }
                            

                        /**
                        * field for QuantityPicked
                        */

                        
                                    protected double localQuantityPicked ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityPickedTracker = false ;

                           public boolean isQuantityPickedSpecified(){
                               return localQuantityPickedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityPicked(){
                               return localQuantityPicked;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityPicked
                               */
                               public void setQuantityPicked(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityPickedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityPicked=param;
                                    

                               }
                            

                        /**
                        * field for Tax1Amt
                        */

                        
                                    protected double localTax1Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax1AmtTracker = false ;

                           public boolean isTax1AmtSpecified(){
                               return localTax1AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax1Amt(){
                               return localTax1Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax1Amt
                               */
                               public void setTax1Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax1AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax1Amt=param;
                                    

                               }
                            

                        /**
                        * field for TaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxCodeTracker = false ;

                           public boolean isTaxCodeSpecified(){
                               return localTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxCode(){
                               return localTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxCode
                               */
                               public void setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxCodeTracker = param != null;
                                   
                                            this.localTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate1
                        */

                        
                                    protected double localTaxRate1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate1Tracker = false ;

                           public boolean isTaxRate1Specified(){
                               return localTaxRate1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate1(){
                               return localTaxRate1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate1
                               */
                               public void setTaxRate1(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate1Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate1=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate2
                        */

                        
                                    protected double localTaxRate2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate2Tracker = false ;

                           public boolean isTaxRate2Specified(){
                               return localTaxRate2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate2(){
                               return localTaxRate2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate2
                               */
                               public void setTaxRate2(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate2Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate2=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertFrom
                        */

                        
                                    protected java.lang.String localGiftCertFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertFromTracker = false ;

                           public boolean isGiftCertFromSpecified(){
                               return localGiftCertFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertFrom(){
                               return localGiftCertFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertFrom
                               */
                               public void setGiftCertFrom(java.lang.String param){
                            localGiftCertFromTracker = param != null;
                                   
                                            this.localGiftCertFrom=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertRecipientName
                        */

                        
                                    protected java.lang.String localGiftCertRecipientName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertRecipientNameTracker = false ;

                           public boolean isGiftCertRecipientNameSpecified(){
                               return localGiftCertRecipientNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertRecipientName(){
                               return localGiftCertRecipientName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertRecipientName
                               */
                               public void setGiftCertRecipientName(java.lang.String param){
                            localGiftCertRecipientNameTracker = param != null;
                                   
                                            this.localGiftCertRecipientName=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertRecipientEmail
                        */

                        
                                    protected java.lang.String localGiftCertRecipientEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertRecipientEmailTracker = false ;

                           public boolean isGiftCertRecipientEmailSpecified(){
                               return localGiftCertRecipientEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertRecipientEmail(){
                               return localGiftCertRecipientEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertRecipientEmail
                               */
                               public void setGiftCertRecipientEmail(java.lang.String param){
                            localGiftCertRecipientEmailTracker = param != null;
                                   
                                            this.localGiftCertRecipientEmail=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertMessage
                        */

                        
                                    protected java.lang.String localGiftCertMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertMessageTracker = false ;

                           public boolean isGiftCertMessageSpecified(){
                               return localGiftCertMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertMessage(){
                               return localGiftCertMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertMessage
                               */
                               public void setGiftCertMessage(java.lang.String param){
                            localGiftCertMessageTracker = param != null;
                                   
                                            this.localGiftCertMessage=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertNumber
                        */

                        
                                    protected java.lang.String localGiftCertNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertNumberTracker = false ;

                           public boolean isGiftCertNumberSpecified(){
                               return localGiftCertNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getGiftCertNumber(){
                               return localGiftCertNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertNumber
                               */
                               public void setGiftCertNumber(java.lang.String param){
                            localGiftCertNumberTracker = param != null;
                                   
                                            this.localGiftCertNumber=param;
                                    

                               }
                            

                        /**
                        * field for ShipGroup
                        */

                        
                                    protected long localShipGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipGroupTracker = false ;

                           public boolean isShipGroupSpecified(){
                               return localShipGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getShipGroup(){
                               return localShipGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipGroup
                               */
                               public void setShipGroup(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipGroupTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localShipGroup=param;
                                    

                               }
                            

                        /**
                        * field for ItemIsFulfilled
                        */

                        
                                    protected boolean localItemIsFulfilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemIsFulfilledTracker = false ;

                           public boolean isItemIsFulfilledSpecified(){
                               return localItemIsFulfilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getItemIsFulfilled(){
                               return localItemIsFulfilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemIsFulfilled
                               */
                               public void setItemIsFulfilled(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localItemIsFulfilledTracker =
                                       true;
                                   
                                            this.localItemIsFulfilled=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressTracker = false ;

                           public boolean isShipAddressSpecified(){
                               return localShipAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipAddress(){
                               return localShipAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddress
                               */
                               public void setShipAddress(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipAddressTracker = param != null;
                                   
                                            this.localShipAddress=param;
                                    

                               }
                            

                        /**
                        * field for ShipMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipMethodTracker = false ;

                           public boolean isShipMethodSpecified(){
                               return localShipMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipMethod(){
                               return localShipMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipMethod
                               */
                               public void setShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipMethodTracker = param != null;
                                   
                                            this.localShipMethod=param;
                                    

                               }
                            

                        /**
                        * field for VsoeSopGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup localVsoeSopGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeSopGroupTracker = false ;

                           public boolean isVsoeSopGroupSpecified(){
                               return localVsoeSopGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup getVsoeSopGroup(){
                               return localVsoeSopGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeSopGroup
                               */
                               public void setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup param){
                            localVsoeSopGroupTracker = param != null;
                                   
                                            this.localVsoeSopGroup=param;
                                    

                               }
                            

                        /**
                        * field for VsoeIsEstimate
                        */

                        
                                    protected boolean localVsoeIsEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeIsEstimateTracker = false ;

                           public boolean isVsoeIsEstimateSpecified(){
                               return localVsoeIsEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeIsEstimate(){
                               return localVsoeIsEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeIsEstimate
                               */
                               public void setVsoeIsEstimate(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeIsEstimateTracker =
                                       true;
                                   
                                            this.localVsoeIsEstimate=param;
                                    

                               }
                            

                        /**
                        * field for VsoePrice
                        */

                        
                                    protected double localVsoePrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePriceTracker = false ;

                           public boolean isVsoePriceSpecified(){
                               return localVsoePriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoePrice(){
                               return localVsoePrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePrice
                               */
                               public void setVsoePrice(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoePriceTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoePrice=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAmount
                        */

                        
                                    protected double localVsoeAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAmountTracker = false ;

                           public boolean isVsoeAmountSpecified(){
                               return localVsoeAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoeAmount(){
                               return localVsoeAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAmount
                               */
                               public void setVsoeAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoeAmount=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAllocation
                        */

                        
                                    protected double localVsoeAllocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAllocationTracker = false ;

                           public boolean isVsoeAllocationSpecified(){
                               return localVsoeAllocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoeAllocation(){
                               return localVsoeAllocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAllocation
                               */
                               public void setVsoeAllocation(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeAllocationTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoeAllocation=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDeferral
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral localVsoeDeferral ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeferralTracker = false ;

                           public boolean isVsoeDeferralSpecified(){
                               return localVsoeDeferralTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral getVsoeDeferral(){
                               return localVsoeDeferral;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDeferral
                               */
                               public void setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral param){
                            localVsoeDeferralTracker = param != null;
                                   
                                            this.localVsoeDeferral=param;
                                    

                               }
                            

                        /**
                        * field for VsoePermitDiscount
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount localVsoePermitDiscount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePermitDiscountTracker = false ;

                           public boolean isVsoePermitDiscountSpecified(){
                               return localVsoePermitDiscountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount getVsoePermitDiscount(){
                               return localVsoePermitDiscount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePermitDiscount
                               */
                               public void setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount param){
                            localVsoePermitDiscountTracker = param != null;
                                   
                                            this.localVsoePermitDiscount=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDelivered
                        */

                        
                                    protected boolean localVsoeDelivered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeliveredTracker = false ;

                           public boolean isVsoeDeliveredSpecified(){
                               return localVsoeDeliveredTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeDelivered(){
                               return localVsoeDelivered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDelivered
                               */
                               public void setVsoeDelivered(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeDeliveredTracker =
                                       true;
                                   
                                            this.localVsoeDelivered=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedShipDate
                        */

                        
                                    protected java.util.Calendar localExpectedShipDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedShipDateTracker = false ;

                           public boolean isExpectedShipDateSpecified(){
                               return localExpectedShipDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getExpectedShipDate(){
                               return localExpectedShipDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedShipDate
                               */
                               public void setExpectedShipDate(java.util.Calendar param){
                            localExpectedShipDateTracker = param != null;
                                   
                                            this.localExpectedShipDate=param;
                                    

                               }
                            

                        /**
                        * field for NoAutoAssignLocation
                        */

                        
                                    protected boolean localNoAutoAssignLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNoAutoAssignLocationTracker = false ;

                           public boolean isNoAutoAssignLocationSpecified(){
                               return localNoAutoAssignLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getNoAutoAssignLocation(){
                               return localNoAutoAssignLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NoAutoAssignLocation
                               */
                               public void setNoAutoAssignLocation(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localNoAutoAssignLocationTracker =
                                       true;
                                   
                                            this.localNoAutoAssignLocation=param;
                                    

                               }
                            

                        /**
                        * field for LocationAutoAssigned
                        */

                        
                                    protected boolean localLocationAutoAssigned ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationAutoAssignedTracker = false ;

                           public boolean isLocationAutoAssignedSpecified(){
                               return localLocationAutoAssignedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getLocationAutoAssigned(){
                               return localLocationAutoAssigned;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationAutoAssigned
                               */
                               public void setLocationAutoAssigned(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationAutoAssignedTracker =
                                       true;
                                   
                                            this.localLocationAutoAssigned=param;
                                    

                               }
                            

                        /**
                        * field for TaxDetailsReference
                        */

                        
                                    protected java.lang.String localTaxDetailsReference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxDetailsReferenceTracker = false ;

                           public boolean isTaxDetailsReferenceSpecified(){
                               return localTaxDetailsReferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTaxDetailsReference(){
                               return localTaxDetailsReference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxDetailsReference
                               */
                               public void setTaxDetailsReference(java.lang.String param){
                            localTaxDetailsReferenceTracker = param != null;
                                   
                                            this.localTaxDetailsReference=param;
                                    

                               }
                            

                        /**
                        * field for ChargeType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localChargeType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localChargeTypeTracker = false ;

                           public boolean isChargeTypeSpecified(){
                               return localChargeTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getChargeType(){
                               return localChargeType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ChargeType
                               */
                               public void setChargeType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localChargeTypeTracker = param != null;
                                   
                                            this.localChargeType=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:sales_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":SalesOrderItem",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "SalesOrderItem",
                           xmlWriter);
                   }

               
                   }
                if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localSubscriptionTracker){
                                            if (localSubscription==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subscription cannot be null!!");
                                            }
                                           localSubscription.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subscription"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localQuantityAvailableTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityAvailable", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityAvailable)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityAvailable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExpandItemGroupTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "expandItemGroup", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("expandItemGroup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpandItemGroup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLineUniqueKeyTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lineUniqueKey", xmlWriter);
                             
                                               if (localLineUniqueKey==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("lineUniqueKey cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLineUniqueKey));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityOnHandTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnHand", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnHand)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnHand cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnitsTracker){
                                            if (localUnits==null){
                                                 throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                            }
                                           localUnits.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","units"),
                                               xmlWriter);
                                        } if (localInventoryDetailTracker){
                                            if (localInventoryDetail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryDetail cannot be null!!");
                                            }
                                           localInventoryDetail.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","inventoryDetail"),
                                               xmlWriter);
                                        } if (localDescriptionTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPriceTracker){
                                            if (localPrice==null){
                                                 throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");
                                            }
                                           localPrice.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","price"),
                                               xmlWriter);
                                        } if (localRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             

                                          if (localRate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSerialNumbersTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "serialNumbers", xmlWriter);
                             

                                          if (localSerialNumbers==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSerialNumbers);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmountTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsTaxableTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCommitInventoryTracker){
                                            if (localCommitInventory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("commitInventory cannot be null!!");
                                            }
                                           localCommitInventory.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","commitInventory"),
                                               xmlWriter);
                                        } if (localOrderPriorityTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "orderPriority", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localOrderPriority)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("orderPriority cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderPriority));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLicenseCodeTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "licenseCode", xmlWriter);
                             

                                          if (localLicenseCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("licenseCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLicenseCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOptionsTracker){
                                            if (localOptions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                            }
                                           localOptions.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","options"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localCreatePoTracker){
                                            if (localCreatePo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createPo cannot be null!!");
                                            }
                                           localCreatePo.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createPo"),
                                               xmlWriter);
                                        } if (localCreatedPoTracker){
                                            if (localCreatedPo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdPo cannot be null!!");
                                            }
                                           localCreatedPo.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdPo"),
                                               xmlWriter);
                                        } if (localAltSalesAmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altSalesAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAltSalesAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("altSalesAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreateWoTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createWo", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("createWo cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreateWo));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPoVendorTracker){
                                            if (localPoVendor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("poVendor cannot be null!!");
                                            }
                                           localPoVendor.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","poVendor"),
                                               xmlWriter);
                                        } if (localPoCurrencyTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "poCurrency", xmlWriter);
                             

                                          if (localPoCurrency==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("poCurrency cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPoCurrency);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPoRateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "poRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPoRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("poRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPoRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecScheduleTracker){
                                            if (localRevRecSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                            }
                                           localRevRecSchedule.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecSchedule"),
                                               xmlWriter);
                                        } if (localRevRecStartDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecStartDate", xmlWriter);
                             

                                          if (localRevRecStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecTermInMonthsTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecTermInMonths", xmlWriter);
                             
                                               if (localRevRecTermInMonths==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("revRecTermInMonths cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecTermInMonths));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevRecEndDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "revRecEndDate", xmlWriter);
                             

                                          if (localRevRecEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeferRevRecTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "deferRevRec", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("deferRevRec cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsClosedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isClosed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isClosed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsClosed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemFulfillmentChoiceTracker){
                                            if (localItemFulfillmentChoice==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemFulfillmentChoice cannot be null!!");
                                            }
                                           localItemFulfillmentChoice.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemFulfillmentChoice"),
                                               xmlWriter);
                                        } if (localCatchUpPeriodTracker){
                                            if (localCatchUpPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("catchUpPeriod cannot be null!!");
                                            }
                                           localCatchUpPeriod.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","catchUpPeriod"),
                                               xmlWriter);
                                        } if (localBillingScheduleTracker){
                                            if (localBillingSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                            }
                                           localBillingSchedule.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingSchedule"),
                                               xmlWriter);
                                        } if (localFromJobTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fromJob", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("fromJob cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFromJob));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGrossAmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "grossAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGrossAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("grossAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxAmountTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExcludeFromRateRequestTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "excludeFromRateRequest", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("excludeFromRateRequest cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromRateRequest));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsEstimateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isEstimate", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLineTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "line", xmlWriter);
                             
                                               if (localLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("line cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPercentCompleteTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "percentComplete", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPercentComplete)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("percentComplete cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPercentComplete));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostEstimateTypeTracker){
                                            if (localCostEstimateType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                            }
                                           localCostEstimateType.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","costEstimateType"),
                                               xmlWriter);
                                        } if (localCostEstimateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costEstimate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCostEstimate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("costEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityBackOrderedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityBackOrdered", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityBackOrdered)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityBackOrdered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBackOrdered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityBilledTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityBilled", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityBilled)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityBilled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBilled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityCommittedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityCommitted", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityCommitted)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityCommitted cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityCommitted));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityFulfilledTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityFulfilled", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityFulfilled)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityFulfilled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityFulfilled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityPackedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityPacked", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityPacked)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityPacked cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityPacked));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityPickedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityPicked", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityPicked)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityPicked cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityPicked));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax1AmtTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax1Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax1Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax1Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxCodeTracker){
                                            if (localTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                            }
                                           localTaxCode.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxCode"),
                                               xmlWriter);
                                        } if (localTaxRate1Tracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate1", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate1)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate1 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxRate2Tracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate2", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate2)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate2 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertFromTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertFrom", xmlWriter);
                             

                                          if (localGiftCertFrom==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertFrom cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertFrom);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertRecipientNameTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertRecipientName", xmlWriter);
                             

                                          if (localGiftCertRecipientName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertRecipientName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertRecipientName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertRecipientEmailTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertRecipientEmail", xmlWriter);
                             

                                          if (localGiftCertRecipientEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertRecipientEmail cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertRecipientEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertMessageTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertMessage", xmlWriter);
                             

                                          if (localGiftCertMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiftCertNumberTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giftCertNumber", xmlWriter);
                             

                                          if (localGiftCertNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("giftCertNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localGiftCertNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipGroupTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipGroup", xmlWriter);
                             
                                               if (localShipGroup==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipGroup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipGroup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemIsFulfilledTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "itemIsFulfilled", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("itemIsFulfilled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemIsFulfilled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipAddressTracker){
                                            if (localShipAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                            }
                                           localShipAddress.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipAddress"),
                                               xmlWriter);
                                        } if (localShipMethodTracker){
                                            if (localShipMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipMethod cannot be null!!");
                                            }
                                           localShipMethod.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipMethod"),
                                               xmlWriter);
                                        } if (localVsoeSopGroupTracker){
                                            if (localVsoeSopGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                            }
                                           localVsoeSopGroup.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeSopGroup"),
                                               xmlWriter);
                                        } if (localVsoeIsEstimateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeIsEstimate", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeIsEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeIsEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoePriceTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoePrice", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoePrice)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoePrice cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeAmountTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoeAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeAllocationTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeAllocation", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoeAllocation)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeAllocation cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAllocation));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeDeferralTracker){
                                            if (localVsoeDeferral==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                            }
                                           localVsoeDeferral.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeDeferral"),
                                               xmlWriter);
                                        } if (localVsoePermitDiscountTracker){
                                            if (localVsoePermitDiscount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                            }
                                           localVsoePermitDiscount.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoePermitDiscount"),
                                               xmlWriter);
                                        } if (localVsoeDeliveredTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeDelivered", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeDelivered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExpectedShipDateTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "expectedShipDate", xmlWriter);
                             

                                          if (localExpectedShipDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("expectedShipDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedShipDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNoAutoAssignLocationTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "noAutoAssignLocation", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("noAutoAssignLocation cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoAutoAssignLocation));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationAutoAssignedTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "locationAutoAssigned", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationAutoAssigned cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationAutoAssigned));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxDetailsReferenceTracker){
                                    namespace = "urn:sales_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxDetailsReference", xmlWriter);
                             

                                          if (localTaxDetailsReference==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTaxDetailsReference);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localChargeTypeTracker){
                                            if (localChargeType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("chargeType cannot be null!!");
                                            }
                                           localChargeType.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","chargeType"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:sales_2017_2.transactions.webservices.netsuite.com")){
                return "ns21";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localSubscriptionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "subscription"));
                            
                            
                                    if (localSubscription==null){
                                         throw new org.apache.axis2.databinding.ADBException("subscription cannot be null!!");
                                    }
                                    elementList.add(localSubscription);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localQuantityAvailableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityAvailable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                            } if (localExpandItemGroupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "expandItemGroup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpandItemGroup));
                            } if (localLineUniqueKeyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "lineUniqueKey"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLineUniqueKey));
                            } if (localQuantityOnHandTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityOnHand"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                            } if (localQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantity));
                            } if (localUnitsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "units"));
                            
                            
                                    if (localUnits==null){
                                         throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                    }
                                    elementList.add(localUnits);
                                } if (localInventoryDetailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "inventoryDetail"));
                            
                            
                                    if (localInventoryDetail==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryDetail cannot be null!!");
                                    }
                                    elementList.add(localInventoryDetail);
                                } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localPriceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "price"));
                            
                            
                                    if (localPrice==null){
                                         throw new org.apache.axis2.databinding.ADBException("price cannot be null!!");
                                    }
                                    elementList.add(localPrice);
                                } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                        if (localRate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                        }
                                    } if (localSerialNumbersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "serialNumbers"));
                                 
                                        if (localSerialNumbers != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSerialNumbers));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                        }
                                    } if (localAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "amount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmount));
                            } if (localIsTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                            } if (localCommitInventoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "commitInventory"));
                            
                            
                                    if (localCommitInventory==null){
                                         throw new org.apache.axis2.databinding.ADBException("commitInventory cannot be null!!");
                                    }
                                    elementList.add(localCommitInventory);
                                } if (localOrderPriorityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "orderPriority"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOrderPriority));
                            } if (localLicenseCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "licenseCode"));
                                 
                                        if (localLicenseCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLicenseCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("licenseCode cannot be null!!");
                                        }
                                    } if (localOptionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "options"));
                            
                            
                                    if (localOptions==null){
                                         throw new org.apache.axis2.databinding.ADBException("options cannot be null!!");
                                    }
                                    elementList.add(localOptions);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localCreatePoTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createPo"));
                            
                            
                                    if (localCreatePo==null){
                                         throw new org.apache.axis2.databinding.ADBException("createPo cannot be null!!");
                                    }
                                    elementList.add(localCreatePo);
                                } if (localCreatedPoTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createdPo"));
                            
                            
                                    if (localCreatedPo==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdPo cannot be null!!");
                                    }
                                    elementList.add(localCreatedPo);
                                } if (localAltSalesAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "altSalesAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltSalesAmt));
                            } if (localCreateWoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "createWo"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreateWo));
                            } if (localPoVendorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "poVendor"));
                            
                            
                                    if (localPoVendor==null){
                                         throw new org.apache.axis2.databinding.ADBException("poVendor cannot be null!!");
                                    }
                                    elementList.add(localPoVendor);
                                } if (localPoCurrencyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "poCurrency"));
                                 
                                        if (localPoCurrency != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPoCurrency));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("poCurrency cannot be null!!");
                                        }
                                    } if (localPoRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "poRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPoRate));
                            } if (localRevRecScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecSchedule"));
                            
                            
                                    if (localRevRecSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                    }
                                    elementList.add(localRevRecSchedule);
                                } if (localRevRecStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecStartDate"));
                                 
                                        if (localRevRecStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                        }
                                    } if (localRevRecTermInMonthsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecTermInMonths"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecTermInMonths));
                            } if (localRevRecEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "revRecEndDate"));
                                 
                                        if (localRevRecEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRevRecEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                        }
                                    } if (localDeferRevRecTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "deferRevRec"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                            } if (localIsClosedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isClosed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsClosed));
                            } if (localItemFulfillmentChoiceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemFulfillmentChoice"));
                            
                            
                                    if (localItemFulfillmentChoice==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemFulfillmentChoice cannot be null!!");
                                    }
                                    elementList.add(localItemFulfillmentChoice);
                                } if (localCatchUpPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "catchUpPeriod"));
                            
                            
                                    if (localCatchUpPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("catchUpPeriod cannot be null!!");
                                    }
                                    elementList.add(localCatchUpPeriod);
                                } if (localBillingScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "billingSchedule"));
                            
                            
                                    if (localBillingSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                    }
                                    elementList.add(localBillingSchedule);
                                } if (localFromJobTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "fromJob"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFromJob));
                            } if (localGrossAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "grossAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                            } if (localTaxAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxAmount));
                            } if (localExcludeFromRateRequestTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "excludeFromRateRequest"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromRateRequest));
                            } if (localIsEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "isEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsEstimate));
                            } if (localLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "line"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                            } if (localPercentCompleteTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "percentComplete"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPercentComplete));
                            } if (localCostEstimateTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "costEstimateType"));
                            
                            
                                    if (localCostEstimateType==null){
                                         throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                    }
                                    elementList.add(localCostEstimateType);
                                } if (localCostEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "costEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                            } if (localQuantityBackOrderedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityBackOrdered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBackOrdered));
                            } if (localQuantityBilledTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityBilled"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBilled));
                            } if (localQuantityCommittedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityCommitted"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityCommitted));
                            } if (localQuantityFulfilledTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityFulfilled"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityFulfilled));
                            } if (localQuantityPackedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityPacked"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityPacked));
                            } if (localQuantityPickedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "quantityPicked"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityPicked));
                            } if (localTax1AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax1Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                            } if (localTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxCode"));
                            
                            
                                    if (localTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                    }
                                    elementList.add(localTaxCode);
                                } if (localTaxRate1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate1"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                            } if (localTaxRate2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate2"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate2));
                            } if (localGiftCertFromTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertFrom"));
                                 
                                        if (localGiftCertFrom != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertFrom));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertFrom cannot be null!!");
                                        }
                                    } if (localGiftCertRecipientNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertRecipientName"));
                                 
                                        if (localGiftCertRecipientName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertRecipientName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertRecipientName cannot be null!!");
                                        }
                                    } if (localGiftCertRecipientEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertRecipientEmail"));
                                 
                                        if (localGiftCertRecipientEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertRecipientEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertRecipientEmail cannot be null!!");
                                        }
                                    } if (localGiftCertMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertMessage"));
                                 
                                        if (localGiftCertMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertMessage cannot be null!!");
                                        }
                                    } if (localGiftCertNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "giftCertNumber"));
                                 
                                        if (localGiftCertNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiftCertNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("giftCertNumber cannot be null!!");
                                        }
                                    } if (localShipGroupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipGroup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipGroup));
                            } if (localItemIsFulfilledTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemIsFulfilled"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemIsFulfilled));
                            } if (localShipAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipAddress"));
                            
                            
                                    if (localShipAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                    }
                                    elementList.add(localShipAddress);
                                } if (localShipMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "shipMethod"));
                            
                            
                                    if (localShipMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipMethod cannot be null!!");
                                    }
                                    elementList.add(localShipMethod);
                                } if (localVsoeSopGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeSopGroup"));
                            
                            
                                    if (localVsoeSopGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                    }
                                    elementList.add(localVsoeSopGroup);
                                } if (localVsoeIsEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeIsEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeIsEstimate));
                            } if (localVsoePriceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoePrice"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                            } if (localVsoeAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAmount));
                            } if (localVsoeAllocationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeAllocation"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeAllocation));
                            } if (localVsoeDeferralTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeDeferral"));
                            
                            
                                    if (localVsoeDeferral==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                    }
                                    elementList.add(localVsoeDeferral);
                                } if (localVsoePermitDiscountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoePermitDiscount"));
                            
                            
                                    if (localVsoePermitDiscount==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                    }
                                    elementList.add(localVsoePermitDiscount);
                                } if (localVsoeDeliveredTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "vsoeDelivered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                            } if (localExpectedShipDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "expectedShipDate"));
                                 
                                        if (localExpectedShipDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpectedShipDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("expectedShipDate cannot be null!!");
                                        }
                                    } if (localNoAutoAssignLocationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "noAutoAssignLocation"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoAutoAssignLocation));
                            } if (localLocationAutoAssignedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "locationAutoAssigned"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationAutoAssigned));
                            } if (localTaxDetailsReferenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxDetailsReference"));
                                 
                                        if (localTaxDetailsReference != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxDetailsReference));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("taxDetailsReference cannot be null!!");
                                        }
                                    } if (localChargeTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "chargeType"));
                            
                            
                                    if (localChargeType==null){
                                         throw new org.apache.axis2.databinding.ADBException("chargeType cannot be null!!");
                                    }
                                    elementList.add(localChargeType);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SalesOrderItem parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SalesOrderItem object =
                new SalesOrderItem();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"SalesOrderItem".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SalesOrderItem)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","subscription").equals(reader.getName())){
                                
                                                object.setSubscription(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityAvailable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityAvailable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityAvailable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityAvailable(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","expandItemGroup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"expandItemGroup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpandItemGroup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","lineUniqueKey").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lineUniqueKey" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLineUniqueKey(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLineUniqueKey(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityOnHand").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnHand" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnHand(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnHand(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","units").equals(reader.getName())){
                                
                                                object.setUnits(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","inventoryDetail").equals(reader.getName())){
                                
                                                object.setInventoryDetail(com.netsuite.webservices.platform.common_2017_2.InventoryDetail.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","price").equals(reader.getName())){
                                
                                                object.setPrice(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","serialNumbers").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"serialNumbers" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSerialNumbers(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","commitInventory").equals(reader.getName())){
                                
                                                object.setCommitInventory(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCommitInventory.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","orderPriority").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"orderPriority" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOrderPriority(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOrderPriority(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","licenseCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"licenseCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLicenseCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","options").equals(reader.getName())){
                                
                                                object.setOptions(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createPo").equals(reader.getName())){
                                
                                                object.setCreatePo(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCreatePo.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createdPo").equals(reader.getName())){
                                
                                                object.setCreatedPo(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","altSalesAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altSalesAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltSalesAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAltSalesAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","createWo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createWo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreateWo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","poVendor").equals(reader.getName())){
                                
                                                object.setPoVendor(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","poCurrency").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"poCurrency" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPoCurrency(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","poRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"poRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPoRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPoRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecSchedule").equals(reader.getName())){
                                
                                                object.setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecTermInMonths").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecTermInMonths" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecTermInMonths(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRevRecTermInMonths(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","revRecEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"revRecEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRevRecEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","deferRevRec").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"deferRevRec" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeferRevRec(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isClosed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isClosed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsClosed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemFulfillmentChoice").equals(reader.getName())){
                                
                                                object.setItemFulfillmentChoice(com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemFulfillmentChoice.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","catchUpPeriod").equals(reader.getName())){
                                
                                                object.setCatchUpPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","billingSchedule").equals(reader.getName())){
                                
                                                object.setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","fromJob").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fromJob" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFromJob(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","grossAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"grossAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGrossAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGrossAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","excludeFromRateRequest").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"excludeFromRateRequest" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExcludeFromRateRequest(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","isEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","line").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"line" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","percentComplete").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"percentComplete" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPercentComplete(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPercentComplete(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","costEstimateType").equals(reader.getName())){
                                
                                                object.setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","costEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCostEstimate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityBackOrdered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityBackOrdered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityBackOrdered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityBackOrdered(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityBilled").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityBilled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityBilled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityBilled(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityCommitted").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityCommitted" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityCommitted(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityCommitted(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityFulfilled").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityFulfilled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityFulfilled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityFulfilled(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityPacked").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityPacked" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityPacked(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityPacked(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","quantityPicked").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityPicked" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityPicked(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityPicked(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","tax1Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax1Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax1Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax1Amt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxCode").equals(reader.getName())){
                                
                                                object.setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxRate1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate1(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxRate2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate2(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertFrom").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertFrom" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertFrom(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertRecipientName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertRecipientName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertRecipientName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertRecipientEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertRecipientEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertRecipientEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","giftCertNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giftCertNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiftCertNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipGroup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipGroup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipGroup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShipGroup(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","itemIsFulfilled").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"itemIsFulfilled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setItemIsFulfilled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipAddress").equals(reader.getName())){
                                
                                                object.setShipAddress(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","shipMethod").equals(reader.getName())){
                                
                                                object.setShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeSopGroup").equals(reader.getName())){
                                
                                                object.setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeIsEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeIsEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeIsEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoePrice").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoePrice" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoePrice(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoePrice(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoeAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeAllocation").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeAllocation" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeAllocation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoeAllocation(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeDeferral").equals(reader.getName())){
                                
                                                object.setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoePermitDiscount").equals(reader.getName())){
                                
                                                object.setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","vsoeDelivered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeDelivered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeDelivered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","expectedShipDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"expectedShipDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpectedShipDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","noAutoAssignLocation").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"noAutoAssignLocation" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNoAutoAssignLocation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","locationAutoAssigned").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationAutoAssigned" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationAutoAssigned(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","taxDetailsReference").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxDetailsReference" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxDetailsReference(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","chargeType").equals(reader.getName())){
                                
                                                object.setChargeType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:sales_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    