
/**
 * TimeBillSearch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.employees_2017_2;
            

            /**
            *  TimeBillSearch bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TimeBillSearch extends com.netsuite.webservices.platform.core_2017_2.SearchRecord
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TimeBillSearch
                Namespace URI = urn:employees_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns38
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for CallJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic localCallJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCallJoinTracker = false ;

                           public boolean isCallJoinSpecified(){
                               return localCallJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic getCallJoin(){
                               return localCallJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CallJoin
                               */
                               public void setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic param){
                            localCallJoinTracker = param != null;
                                   
                                            this.localCallJoin=param;
                                    

                               }
                            

                        /**
                        * field for CaseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic localCaseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseJoinTracker = false ;

                           public boolean isCaseJoinSpecified(){
                               return localCaseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic getCaseJoin(){
                               return localCaseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseJoin
                               */
                               public void setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic param){
                            localCaseJoinTracker = param != null;
                                   
                                            this.localCaseJoin=param;
                                    

                               }
                            

                        /**
                        * field for ChargeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ChargeSearchBasic localChargeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localChargeJoinTracker = false ;

                           public boolean isChargeJoinSpecified(){
                               return localChargeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ChargeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ChargeSearchBasic getChargeJoin(){
                               return localChargeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ChargeJoin
                               */
                               public void setChargeJoin(com.netsuite.webservices.platform.common_2017_2.ChargeSearchBasic param){
                            localChargeJoinTracker = param != null;
                                   
                                            this.localChargeJoin=param;
                                    

                               }
                            

                        /**
                        * field for ClassJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic localClassJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClassJoinTracker = false ;

                           public boolean isClassJoinSpecified(){
                               return localClassJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic getClassJoin(){
                               return localClassJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClassJoin
                               */
                               public void setClassJoin(com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic param){
                            localClassJoinTracker = param != null;
                                   
                                            this.localClassJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic localCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerJoinTracker = false ;

                           public boolean isCustomerJoinSpecified(){
                               return localCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic getCustomerJoin(){
                               return localCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerJoin
                               */
                               public void setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic param){
                            localCustomerJoinTracker = param != null;
                                   
                                            this.localCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for DepartmentJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic localDepartmentJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentJoinTracker = false ;

                           public boolean isDepartmentJoinSpecified(){
                               return localDepartmentJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic getDepartmentJoin(){
                               return localDepartmentJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DepartmentJoin
                               */
                               public void setDepartmentJoin(com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic param){
                            localDepartmentJoinTracker = param != null;
                                   
                                            this.localDepartmentJoin=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localEmployeeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeJoinTracker = false ;

                           public boolean isEmployeeJoinSpecified(){
                               return localEmployeeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getEmployeeJoin(){
                               return localEmployeeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeJoin
                               */
                               public void setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localEmployeeJoinTracker = param != null;
                                   
                                            this.localEmployeeJoin=param;
                                    

                               }
                            

                        /**
                        * field for EventJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic localEventJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventJoinTracker = false ;

                           public boolean isEventJoinSpecified(){
                               return localEventJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic getEventJoin(){
                               return localEventJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventJoin
                               */
                               public void setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic param){
                            localEventJoinTracker = param != null;
                                   
                                            this.localEventJoin=param;
                                    

                               }
                            

                        /**
                        * field for ItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic localItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemJoinTracker = false ;

                           public boolean isItemJoinSpecified(){
                               return localItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic getItemJoin(){
                               return localItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemJoin
                               */
                               public void setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic param){
                            localItemJoinTracker = param != null;
                                   
                                            this.localItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for JobJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchBasic localJobJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobJoinTracker = false ;

                           public boolean isJobJoinSpecified(){
                               return localJobJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchBasic getJobJoin(){
                               return localJobJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobJoin
                               */
                               public void setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchBasic param){
                            localJobJoinTracker = param != null;
                                   
                                            this.localJobJoin=param;
                                    

                               }
                            

                        /**
                        * field for LocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic localLocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationJoinTracker = false ;

                           public boolean isLocationJoinSpecified(){
                               return localLocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic getLocationJoin(){
                               return localLocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationJoin
                               */
                               public void setLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic param){
                            localLocationJoinTracker = param != null;
                                   
                                            this.localLocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for ProjectTaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic localProjectTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTaskJoinTracker = false ;

                           public boolean isProjectTaskJoinSpecified(){
                               return localProjectTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic getProjectTaskJoin(){
                               return localProjectTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectTaskJoin
                               */
                               public void setProjectTaskJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic param){
                            localProjectTaskJoinTracker = param != null;
                                   
                                            this.localProjectTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for ProjectTaskAssignmentJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchBasic localProjectTaskAssignmentJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTaskAssignmentJoinTracker = false ;

                           public boolean isProjectTaskAssignmentJoinSpecified(){
                               return localProjectTaskAssignmentJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchBasic getProjectTaskAssignmentJoin(){
                               return localProjectTaskAssignmentJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectTaskAssignmentJoin
                               */
                               public void setProjectTaskAssignmentJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchBasic param){
                            localProjectTaskAssignmentJoinTracker = param != null;
                                   
                                            this.localProjectTaskAssignmentJoin=param;
                                    

                               }
                            

                        /**
                        * field for ResourceAllocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchBasic localResourceAllocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResourceAllocationJoinTracker = false ;

                           public boolean isResourceAllocationJoinSpecified(){
                               return localResourceAllocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchBasic getResourceAllocationJoin(){
                               return localResourceAllocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResourceAllocationJoin
                               */
                               public void setResourceAllocationJoin(com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchBasic param){
                            localResourceAllocationJoinTracker = param != null;
                                   
                                            this.localResourceAllocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic localTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaskJoinTracker = false ;

                           public boolean isTaskJoinSpecified(){
                               return localTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic getTaskJoin(){
                               return localTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaskJoin
                               */
                               public void setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic param){
                            localTaskJoinTracker = param != null;
                                   
                                            this.localTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic localVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorJoinTracker = false ;

                           public boolean isVendorJoinSpecified(){
                               return localVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic getVendorJoin(){
                               return localVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorJoin
                               */
                               public void setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic param){
                            localVendorJoinTracker = param != null;
                                   
                                            this.localVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for TimeSheetJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchBasic localTimeSheetJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeSheetJoinTracker = false ;

                           public boolean isTimeSheetJoinSpecified(){
                               return localTimeSheetJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchBasic getTimeSheetJoin(){
                               return localTimeSheetJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeSheetJoin
                               */
                               public void setTimeSheetJoin(com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchBasic param){
                            localTimeSheetJoinTracker = param != null;
                                   
                                            this.localTimeSheetJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:employees_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TimeBillSearch",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TimeBillSearch",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localCallJoinTracker){
                                            if (localCallJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                            }
                                           localCallJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","callJoin"),
                                               xmlWriter);
                                        } if (localCaseJoinTracker){
                                            if (localCaseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                            }
                                           localCaseJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","caseJoin"),
                                               xmlWriter);
                                        } if (localChargeJoinTracker){
                                            if (localChargeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("chargeJoin cannot be null!!");
                                            }
                                           localChargeJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","chargeJoin"),
                                               xmlWriter);
                                        } if (localClassJoinTracker){
                                            if (localClassJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("classJoin cannot be null!!");
                                            }
                                           localClassJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","classJoin"),
                                               xmlWriter);
                                        } if (localCustomerJoinTracker){
                                            if (localCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                            }
                                           localCustomerJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customerJoin"),
                                               xmlWriter);
                                        } if (localDepartmentJoinTracker){
                                            if (localDepartmentJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("departmentJoin cannot be null!!");
                                            }
                                           localDepartmentJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","departmentJoin"),
                                               xmlWriter);
                                        } if (localEmployeeJoinTracker){
                                            if (localEmployeeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                            }
                                           localEmployeeJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","employeeJoin"),
                                               xmlWriter);
                                        } if (localEventJoinTracker){
                                            if (localEventJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                            }
                                           localEventJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","eventJoin"),
                                               xmlWriter);
                                        } if (localItemJoinTracker){
                                            if (localItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                            }
                                           localItemJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","itemJoin"),
                                               xmlWriter);
                                        } if (localJobJoinTracker){
                                            if (localJobJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                            }
                                           localJobJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","jobJoin"),
                                               xmlWriter);
                                        } if (localLocationJoinTracker){
                                            if (localLocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationJoin cannot be null!!");
                                            }
                                           localLocationJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","locationJoin"),
                                               xmlWriter);
                                        } if (localProjectTaskJoinTracker){
                                            if (localProjectTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectTaskJoin cannot be null!!");
                                            }
                                           localProjectTaskJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","projectTaskJoin"),
                                               xmlWriter);
                                        } if (localProjectTaskAssignmentJoinTracker){
                                            if (localProjectTaskAssignmentJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectTaskAssignmentJoin cannot be null!!");
                                            }
                                           localProjectTaskAssignmentJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","projectTaskAssignmentJoin"),
                                               xmlWriter);
                                        } if (localResourceAllocationJoinTracker){
                                            if (localResourceAllocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resourceAllocationJoin cannot be null!!");
                                            }
                                           localResourceAllocationJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","resourceAllocationJoin"),
                                               xmlWriter);
                                        } if (localTaskJoinTracker){
                                            if (localTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                            }
                                           localTaskJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","taskJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localVendorJoinTracker){
                                            if (localVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                            }
                                           localVendorJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","vendorJoin"),
                                               xmlWriter);
                                        } if (localTimeSheetJoinTracker){
                                            if (localTimeSheetJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeSheetJoin cannot be null!!");
                                            }
                                           localTimeSheetJoin.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","timeSheetJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:employees_2017_2.transactions.webservices.netsuite.com")){
                return "ns38";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","TimeBillSearch"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localCallJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "callJoin"));
                            
                            
                                    if (localCallJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                    }
                                    elementList.add(localCallJoin);
                                } if (localCaseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "caseJoin"));
                            
                            
                                    if (localCaseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                    }
                                    elementList.add(localCaseJoin);
                                } if (localChargeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "chargeJoin"));
                            
                            
                                    if (localChargeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("chargeJoin cannot be null!!");
                                    }
                                    elementList.add(localChargeJoin);
                                } if (localClassJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "classJoin"));
                            
                            
                                    if (localClassJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("classJoin cannot be null!!");
                                    }
                                    elementList.add(localClassJoin);
                                } if (localCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "customerJoin"));
                            
                            
                                    if (localCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerJoin);
                                } if (localDepartmentJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "departmentJoin"));
                            
                            
                                    if (localDepartmentJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("departmentJoin cannot be null!!");
                                    }
                                    elementList.add(localDepartmentJoin);
                                } if (localEmployeeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "employeeJoin"));
                            
                            
                                    if (localEmployeeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                    }
                                    elementList.add(localEmployeeJoin);
                                } if (localEventJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "eventJoin"));
                            
                            
                                    if (localEventJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                    }
                                    elementList.add(localEventJoin);
                                } if (localItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "itemJoin"));
                            
                            
                                    if (localItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                    }
                                    elementList.add(localItemJoin);
                                } if (localJobJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "jobJoin"));
                            
                            
                                    if (localJobJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                    }
                                    elementList.add(localJobJoin);
                                } if (localLocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "locationJoin"));
                            
                            
                                    if (localLocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationJoin cannot be null!!");
                                    }
                                    elementList.add(localLocationJoin);
                                } if (localProjectTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "projectTaskJoin"));
                            
                            
                                    if (localProjectTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectTaskJoin cannot be null!!");
                                    }
                                    elementList.add(localProjectTaskJoin);
                                } if (localProjectTaskAssignmentJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "projectTaskAssignmentJoin"));
                            
                            
                                    if (localProjectTaskAssignmentJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectTaskAssignmentJoin cannot be null!!");
                                    }
                                    elementList.add(localProjectTaskAssignmentJoin);
                                } if (localResourceAllocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "resourceAllocationJoin"));
                            
                            
                                    if (localResourceAllocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("resourceAllocationJoin cannot be null!!");
                                    }
                                    elementList.add(localResourceAllocationJoin);
                                } if (localTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "taskJoin"));
                            
                            
                                    if (localTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                    }
                                    elementList.add(localTaskJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "vendorJoin"));
                            
                            
                                    if (localVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorJoin);
                                } if (localTimeSheetJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "timeSheetJoin"));
                            
                            
                                    if (localTimeSheetJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeSheetJoin cannot be null!!");
                                    }
                                    elementList.add(localTimeSheetJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TimeBillSearch parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TimeBillSearch object =
                new TimeBillSearch();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TimeBillSearch".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TimeBillSearch)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list20 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","callJoin").equals(reader.getName())){
                                
                                                object.setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","caseJoin").equals(reader.getName())){
                                
                                                object.setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","chargeJoin").equals(reader.getName())){
                                
                                                object.setChargeJoin(com.netsuite.webservices.platform.common_2017_2.ChargeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","classJoin").equals(reader.getName())){
                                
                                                object.setClassJoin(com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customerJoin").equals(reader.getName())){
                                
                                                object.setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","departmentJoin").equals(reader.getName())){
                                
                                                object.setDepartmentJoin(com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","employeeJoin").equals(reader.getName())){
                                
                                                object.setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","eventJoin").equals(reader.getName())){
                                
                                                object.setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","itemJoin").equals(reader.getName())){
                                
                                                object.setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","jobJoin").equals(reader.getName())){
                                
                                                object.setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","locationJoin").equals(reader.getName())){
                                
                                                object.setLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","projectTaskJoin").equals(reader.getName())){
                                
                                                object.setProjectTaskJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","projectTaskAssignmentJoin").equals(reader.getName())){
                                
                                                object.setProjectTaskAssignmentJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","resourceAllocationJoin").equals(reader.getName())){
                                
                                                object.setResourceAllocationJoin(com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","taskJoin").equals(reader.getName())){
                                
                                                object.setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","vendorJoin").equals(reader.getName())){
                                
                                                object.setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","timeSheetJoin").equals(reader.getName())){
                                
                                                object.setTimeSheetJoin(com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list20.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone20 = false;
                                                        while(!loopDone20){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone20 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list20.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone20 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.class,
                                                                list20));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    