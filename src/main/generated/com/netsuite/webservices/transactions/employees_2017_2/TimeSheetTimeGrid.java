
/**
 * TimeSheetTimeGrid.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.employees_2017_2;
            

            /**
            *  TimeSheetTimeGrid bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TimeSheetTimeGrid
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TimeSheetTimeGrid
                Namespace URI = urn:employees_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns38
                */
            

                        /**
                        * field for Sunday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localSunday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSundayTracker = false ;

                           public boolean isSundaySpecified(){
                               return localSundayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getSunday(){
                               return localSunday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sunday
                               */
                               public void setSunday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localSundayTracker = param != null;
                                   
                                            this.localSunday=param;
                                    

                               }
                            

                        /**
                        * field for Monday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localMonday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMondayTracker = false ;

                           public boolean isMondaySpecified(){
                               return localMondayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getMonday(){
                               return localMonday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Monday
                               */
                               public void setMonday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localMondayTracker = param != null;
                                   
                                            this.localMonday=param;
                                    

                               }
                            

                        /**
                        * field for Tuesday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localTuesday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTuesdayTracker = false ;

                           public boolean isTuesdaySpecified(){
                               return localTuesdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getTuesday(){
                               return localTuesday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tuesday
                               */
                               public void setTuesday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localTuesdayTracker = param != null;
                                   
                                            this.localTuesday=param;
                                    

                               }
                            

                        /**
                        * field for Wednesday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localWednesday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWednesdayTracker = false ;

                           public boolean isWednesdaySpecified(){
                               return localWednesdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getWednesday(){
                               return localWednesday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Wednesday
                               */
                               public void setWednesday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localWednesdayTracker = param != null;
                                   
                                            this.localWednesday=param;
                                    

                               }
                            

                        /**
                        * field for Thursday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localThursday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localThursdayTracker = false ;

                           public boolean isThursdaySpecified(){
                               return localThursdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getThursday(){
                               return localThursday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Thursday
                               */
                               public void setThursday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localThursdayTracker = param != null;
                                   
                                            this.localThursday=param;
                                    

                               }
                            

                        /**
                        * field for Friday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localFriday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFridayTracker = false ;

                           public boolean isFridaySpecified(){
                               return localFridayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getFriday(){
                               return localFriday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Friday
                               */
                               public void setFriday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localFridayTracker = param != null;
                                   
                                            this.localFriday=param;
                                    

                               }
                            

                        /**
                        * field for Saturday
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeEntry localSaturday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaturdayTracker = false ;

                           public boolean isSaturdaySpecified(){
                               return localSaturdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeEntry
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry getSaturday(){
                               return localSaturday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Saturday
                               */
                               public void setSaturday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry param){
                            localSaturdayTracker = param != null;
                                   
                                            this.localSaturday=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:employees_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TimeSheetTimeGrid",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TimeSheetTimeGrid",
                           xmlWriter);
                   }

               
                   }
                if (localSundayTracker){
                                            if (localSunday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sunday cannot be null!!");
                                            }
                                           localSunday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","sunday"),
                                               xmlWriter);
                                        } if (localMondayTracker){
                                            if (localMonday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("monday cannot be null!!");
                                            }
                                           localMonday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","monday"),
                                               xmlWriter);
                                        } if (localTuesdayTracker){
                                            if (localTuesday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tuesday cannot be null!!");
                                            }
                                           localTuesday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","tuesday"),
                                               xmlWriter);
                                        } if (localWednesdayTracker){
                                            if (localWednesday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("wednesday cannot be null!!");
                                            }
                                           localWednesday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","wednesday"),
                                               xmlWriter);
                                        } if (localThursdayTracker){
                                            if (localThursday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("thursday cannot be null!!");
                                            }
                                           localThursday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","thursday"),
                                               xmlWriter);
                                        } if (localFridayTracker){
                                            if (localFriday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("friday cannot be null!!");
                                            }
                                           localFriday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","friday"),
                                               xmlWriter);
                                        } if (localSaturdayTracker){
                                            if (localSaturday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("saturday cannot be null!!");
                                            }
                                           localSaturday.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","saturday"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:employees_2017_2.transactions.webservices.netsuite.com")){
                return "ns38";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localSundayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "sunday"));
                            
                            
                                    if (localSunday==null){
                                         throw new org.apache.axis2.databinding.ADBException("sunday cannot be null!!");
                                    }
                                    elementList.add(localSunday);
                                } if (localMondayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "monday"));
                            
                            
                                    if (localMonday==null){
                                         throw new org.apache.axis2.databinding.ADBException("monday cannot be null!!");
                                    }
                                    elementList.add(localMonday);
                                } if (localTuesdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "tuesday"));
                            
                            
                                    if (localTuesday==null){
                                         throw new org.apache.axis2.databinding.ADBException("tuesday cannot be null!!");
                                    }
                                    elementList.add(localTuesday);
                                } if (localWednesdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "wednesday"));
                            
                            
                                    if (localWednesday==null){
                                         throw new org.apache.axis2.databinding.ADBException("wednesday cannot be null!!");
                                    }
                                    elementList.add(localWednesday);
                                } if (localThursdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "thursday"));
                            
                            
                                    if (localThursday==null){
                                         throw new org.apache.axis2.databinding.ADBException("thursday cannot be null!!");
                                    }
                                    elementList.add(localThursday);
                                } if (localFridayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "friday"));
                            
                            
                                    if (localFriday==null){
                                         throw new org.apache.axis2.databinding.ADBException("friday cannot be null!!");
                                    }
                                    elementList.add(localFriday);
                                } if (localSaturdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "saturday"));
                            
                            
                                    if (localSaturday==null){
                                         throw new org.apache.axis2.databinding.ADBException("saturday cannot be null!!");
                                    }
                                    elementList.add(localSaturday);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TimeSheetTimeGrid parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TimeSheetTimeGrid object =
                new TimeSheetTimeGrid();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TimeSheetTimeGrid".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TimeSheetTimeGrid)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","sunday").equals(reader.getName())){
                                
                                                object.setSunday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","monday").equals(reader.getName())){
                                
                                                object.setMonday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","tuesday").equals(reader.getName())){
                                
                                                object.setTuesday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","wednesday").equals(reader.getName())){
                                
                                                object.setWednesday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","thursday").equals(reader.getName())){
                                
                                                object.setThursday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","friday").equals(reader.getName())){
                                
                                                object.setFriday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","saturday").equals(reader.getName())){
                                
                                                object.setSaturday(com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    