
/**
 * TimeSheet.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.employees_2017_2;
            

            /**
            *  TimeSheet bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TimeSheet extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TimeSheet
                Namespace URI = urn:employees_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns38
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for Employee
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEmployee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeTracker = false ;

                           public boolean isEmployeeSpecified(){
                               return localEmployeeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEmployee(){
                               return localEmployee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Employee
                               */
                               public void setEmployee(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEmployeeTracker = param != null;
                                   
                                            this.localEmployee=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for TotalHours
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.Duration localTotalHours ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalHoursTracker = false ;

                           public boolean isTotalHoursSpecified(){
                               return localTotalHoursTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.Duration
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.Duration getTotalHours(){
                               return localTotalHours;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TotalHours
                               */
                               public void setTotalHours(com.netsuite.webservices.platform.core_2017_2.Duration param){
                            localTotalHoursTracker = param != null;
                                   
                                            this.localTotalHours=param;
                                    

                               }
                            

                        /**
                        * field for ApprovalStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localApprovalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApprovalStatusTracker = false ;

                           public boolean isApprovalStatusSpecified(){
                               return localApprovalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getApprovalStatus(){
                               return localApprovalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApprovalStatus
                               */
                               public void setApprovalStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localApprovalStatusTracker = param != null;
                                   
                                            this.localApprovalStatus=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for TimeGridList
                        */

                        
                                    protected com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGridList localTimeGridList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeGridListTracker = false ;

                           public boolean isTimeGridListSpecified(){
                               return localTimeGridListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGridList
                           */
                           public  com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGridList getTimeGridList(){
                               return localTimeGridList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeGridList
                               */
                               public void setTimeGridList(com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGridList param){
                            localTimeGridListTracker = param != null;
                                   
                                            this.localTimeGridList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:employees_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TimeSheet",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TimeSheet",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localEmployeeTracker){
                                            if (localEmployee==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employee cannot be null!!");
                                            }
                                           localEmployee.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","employee"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                    namespace = "urn:employees_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:employees_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTotalHoursTracker){
                                            if (localTotalHours==null){
                                                 throw new org.apache.axis2.databinding.ADBException("totalHours cannot be null!!");
                                            }
                                           localTotalHours.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","totalHours"),
                                               xmlWriter);
                                        } if (localApprovalStatusTracker){
                                            if (localApprovalStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                            }
                                           localApprovalStatus.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","approvalStatus"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTimeGridListTracker){
                                            if (localTimeGridList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeGridList cannot be null!!");
                                            }
                                           localTimeGridList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","timeGridList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:employees_2017_2.transactions.webservices.netsuite.com")){
                return "ns38";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","TimeSheet"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localEmployeeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "employee"));
                            
                            
                                    if (localEmployee==null){
                                         throw new org.apache.axis2.databinding.ADBException("employee cannot be null!!");
                                    }
                                    elementList.add(localEmployee);
                                } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localTotalHoursTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "totalHours"));
                            
                            
                                    if (localTotalHours==null){
                                         throw new org.apache.axis2.databinding.ADBException("totalHours cannot be null!!");
                                    }
                                    elementList.add(localTotalHours);
                                } if (localApprovalStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "approvalStatus"));
                            
                            
                                    if (localApprovalStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                    }
                                    elementList.add(localApprovalStatus);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTimeGridListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "timeGridList"));
                            
                            
                                    if (localTimeGridList==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeGridList cannot be null!!");
                                    }
                                    elementList.add(localTimeGridList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TimeSheet parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TimeSheet object =
                new TimeSheet();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TimeSheet".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TimeSheet)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","employee").equals(reader.getName())){
                                
                                                object.setEmployee(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","totalHours").equals(reader.getName())){
                                
                                                object.setTotalHours(com.netsuite.webservices.platform.core_2017_2.Duration.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","approvalStatus").equals(reader.getName())){
                                
                                                object.setApprovalStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","timeGridList").equals(reader.getName())){
                                
                                                object.setTimeGridList(com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGridList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    