
/**
 * AdvInterCompanyJournalEntryLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.transactions.general_2017_2;
            

            /**
            *  AdvInterCompanyJournalEntryLine bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class AdvInterCompanyJournalEntryLine
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = AdvInterCompanyJournalEntryLine
                Namespace URI = urn:general_2017_2.transactions.webservices.netsuite.com
                Namespace Prefix = ns31
                */
            

                        /**
                        * field for LineSubsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLineSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineSubsidiaryTracker = false ;

                           public boolean isLineSubsidiarySpecified(){
                               return localLineSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLineSubsidiary(){
                               return localLineSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LineSubsidiary
                               */
                               public void setLineSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLineSubsidiaryTracker = param != null;
                                   
                                            this.localLineSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for Account
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountTracker = false ;

                           public boolean isAccountSpecified(){
                               return localAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAccount(){
                               return localAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Account
                               */
                               public void setAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAccountTracker = param != null;
                                   
                                            this.localAccount=param;
                                    

                               }
                            

                        /**
                        * field for Line
                        */

                        
                                    protected long localLine ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineTracker = false ;

                           public boolean isLineSpecified(){
                               return localLineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLine(){
                               return localLine;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Line
                               */
                               public void setLine(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLine=param;
                                    

                               }
                            

                        /**
                        * field for Debit
                        */

                        
                                    protected double localDebit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDebitTracker = false ;

                           public boolean isDebitSpecified(){
                               return localDebitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDebit(){
                               return localDebit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Debit
                               */
                               public void setDebit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDebitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDebit=param;
                                    

                               }
                            

                        /**
                        * field for Credit
                        */

                        
                                    protected double localCredit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditTracker = false ;

                           public boolean isCreditSpecified(){
                               return localCreditTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCredit(){
                               return localCredit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Credit
                               */
                               public void setCredit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCreditTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCredit=param;
                                    

                               }
                            

                        /**
                        * field for TaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxCodeTracker = false ;

                           public boolean isTaxCodeSpecified(){
                               return localTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxCode(){
                               return localTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxCode
                               */
                               public void setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxCodeTracker = param != null;
                                   
                                            this.localTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected java.lang.String localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(java.lang.String param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for TaxRate1
                        */

                        
                                    protected double localTaxRate1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRate1Tracker = false ;

                           public boolean isTaxRate1Specified(){
                               return localTaxRate1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTaxRate1(){
                               return localTaxRate1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRate1
                               */
                               public void setTaxRate1(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTaxRate1Tracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTaxRate1=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for DueToFromSubsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDueToFromSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDueToFromSubsidiaryTracker = false ;

                           public boolean isDueToFromSubsidiarySpecified(){
                               return localDueToFromSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDueToFromSubsidiary(){
                               return localDueToFromSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DueToFromSubsidiary
                               */
                               public void setDueToFromSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDueToFromSubsidiaryTracker = param != null;
                                   
                                            this.localDueToFromSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for GrossAmt
                        */

                        
                                    protected double localGrossAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGrossAmtTracker = false ;

                           public boolean isGrossAmtSpecified(){
                               return localGrossAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getGrossAmt(){
                               return localGrossAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GrossAmt
                               */
                               public void setGrossAmt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localGrossAmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localGrossAmt=param;
                                    

                               }
                            

                        /**
                        * field for Schedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleTracker = false ;

                           public boolean isScheduleSpecified(){
                               return localScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSchedule(){
                               return localSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Schedule
                               */
                               public void setSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localScheduleTracker = param != null;
                                   
                                            this.localSchedule=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for RevenueRecognitionRule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevenueRecognitionRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueRecognitionRuleTracker = false ;

                           public boolean isRevenueRecognitionRuleSpecified(){
                               return localRevenueRecognitionRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevenueRecognitionRule(){
                               return localRevenueRecognitionRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueRecognitionRule
                               */
                               public void setRevenueRecognitionRule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevenueRecognitionRuleTracker = param != null;
                                   
                                            this.localRevenueRecognitionRule=param;
                                    

                               }
                            

                        /**
                        * field for Eliminate
                        */

                        
                                    protected boolean localEliminate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEliminateTracker = false ;

                           public boolean isEliminateSpecified(){
                               return localEliminateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEliminate(){
                               return localEliminate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Eliminate
                               */
                               public void setEliminate(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEliminateTracker =
                                       true;
                                   
                                            this.localEliminate=param;
                                    

                               }
                            

                        /**
                        * field for Residual
                        */

                        
                                    protected java.lang.String localResidual ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResidualTracker = false ;

                           public boolean isResidualSpecified(){
                               return localResidualTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getResidual(){
                               return localResidual;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Residual
                               */
                               public void setResidual(java.lang.String param){
                            localResidualTracker = param != null;
                                   
                                            this.localResidual=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationSched
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAmortizationSched ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationSchedTracker = false ;

                           public boolean isAmortizationSchedSpecified(){
                               return localAmortizationSchedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAmortizationSched(){
                               return localAmortizationSched;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationSched
                               */
                               public void setAmortizationSched(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAmortizationSchedTracker = param != null;
                                   
                                            this.localAmortizationSched=param;
                                    

                               }
                            

                        /**
                        * field for ScheduleNum
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localScheduleNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleNumTracker = false ;

                           public boolean isScheduleNumSpecified(){
                               return localScheduleNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getScheduleNum(){
                               return localScheduleNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleNum
                               */
                               public void setScheduleNum(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localScheduleNumTracker = param != null;
                                   
                                            this.localScheduleNum=param;
                                    

                               }
                            

                        /**
                        * field for AmortizStartDate
                        */

                        
                                    protected java.util.Calendar localAmortizStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizStartDateTracker = false ;

                           public boolean isAmortizStartDateSpecified(){
                               return localAmortizStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getAmortizStartDate(){
                               return localAmortizStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizStartDate
                               */
                               public void setAmortizStartDate(java.util.Calendar param){
                            localAmortizStartDateTracker = param != null;
                                   
                                            this.localAmortizStartDate=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationEndDate
                        */

                        
                                    protected java.util.Calendar localAmortizationEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationEndDateTracker = false ;

                           public boolean isAmortizationEndDateSpecified(){
                               return localAmortizationEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getAmortizationEndDate(){
                               return localAmortizationEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationEndDate
                               */
                               public void setAmortizationEndDate(java.util.Calendar param){
                            localAmortizationEndDateTracker = param != null;
                                   
                                            this.localAmortizationEndDate=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationResidual
                        */

                        
                                    protected java.lang.String localAmortizationResidual ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationResidualTracker = false ;

                           public boolean isAmortizationResidualSpecified(){
                               return localAmortizationResidualTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAmortizationResidual(){
                               return localAmortizationResidual;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationResidual
                               */
                               public void setAmortizationResidual(java.lang.String param){
                            localAmortizationResidualTracker = param != null;
                                   
                                            this.localAmortizationResidual=param;
                                    

                               }
                            

                        /**
                        * field for Tax1Amt
                        */

                        
                                    protected double localTax1Amt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax1AmtTracker = false ;

                           public boolean isTax1AmtSpecified(){
                               return localTax1AmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getTax1Amt(){
                               return localTax1Amt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax1Amt
                               */
                               public void setTax1Amt(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localTax1AmtTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localTax1Amt=param;
                                    

                               }
                            

                        /**
                        * field for Tax1Acct
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTax1Acct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTax1AcctTracker = false ;

                           public boolean isTax1AcctSpecified(){
                               return localTax1AcctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTax1Acct(){
                               return localTax1Acct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tax1Acct
                               */
                               public void setTax1Acct(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTax1AcctTracker = param != null;
                                   
                                            this.localTax1Acct=param;
                                    

                               }
                            

                        /**
                        * field for LineFxRate
                        */

                        
                                    protected double localLineFxRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLineFxRateTracker = false ;

                           public boolean isLineFxRateSpecified(){
                               return localLineFxRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLineFxRate(){
                               return localLineFxRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LineFxRate
                               */
                               public void setLineFxRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLineFxRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLineFxRate=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:general_2017_2.transactions.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":AdvInterCompanyJournalEntryLine",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "AdvInterCompanyJournalEntryLine",
                           xmlWriter);
                   }

               
                   }
                if (localLineSubsidiaryTracker){
                                            if (localLineSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lineSubsidiary cannot be null!!");
                                            }
                                           localLineSubsidiary.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","lineSubsidiary"),
                                               xmlWriter);
                                        } if (localAccountTracker){
                                            if (localAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                            }
                                           localAccount.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","account"),
                                               xmlWriter);
                                        } if (localLineTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "line", xmlWriter);
                             
                                               if (localLine==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("line cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDebitTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "debit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDebit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("debit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDebit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreditTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "credit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCredit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("credit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCredit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxCodeTracker){
                                            if (localTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                            }
                                           localTaxCode.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","taxCode"),
                                               xmlWriter);
                                        } if (localMemoTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "memo", xmlWriter);
                             

                                          if (localMemo==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMemo);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxRate1Tracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxRate1", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTaxRate1)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("taxRate1 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localDueToFromSubsidiaryTracker){
                                            if (localDueToFromSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dueToFromSubsidiary cannot be null!!");
                                            }
                                           localDueToFromSubsidiary.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","dueToFromSubsidiary"),
                                               xmlWriter);
                                        } if (localGrossAmtTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "grossAmt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localGrossAmt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("grossAmt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localScheduleTracker){
                                            if (localSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("schedule cannot be null!!");
                                            }
                                           localSchedule.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","schedule"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localEndDateTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localRevenueRecognitionRuleTracker){
                                            if (localRevenueRecognitionRule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueRecognitionRule cannot be null!!");
                                            }
                                           localRevenueRecognitionRule.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","revenueRecognitionRule"),
                                               xmlWriter);
                                        } if (localEliminateTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "eliminate", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("eliminate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEliminate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localResidualTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "residual", xmlWriter);
                             

                                          if (localResidual==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("residual cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localResidual);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmortizationSchedTracker){
                                            if (localAmortizationSched==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amortizationSched cannot be null!!");
                                            }
                                           localAmortizationSched.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","amortizationSched"),
                                               xmlWriter);
                                        } if (localScheduleNumTracker){
                                            if (localScheduleNum==null){
                                                 throw new org.apache.axis2.databinding.ADBException("scheduleNum cannot be null!!");
                                            }
                                           localScheduleNum.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","scheduleNum"),
                                               xmlWriter);
                                        } if (localAmortizStartDateTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizStartDate", xmlWriter);
                             

                                          if (localAmortizStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("amortizStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmortizationEndDateTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizationEndDate", xmlWriter);
                             

                                          if (localAmortizationEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("amortizationEndDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAmortizationResidualTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizationResidual", xmlWriter);
                             

                                          if (localAmortizationResidual==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("amortizationResidual cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAmortizationResidual);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax1AmtTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tax1Amt", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localTax1Amt)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("tax1Amt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTax1AcctTracker){
                                            if (localTax1Acct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tax1Acct cannot be null!!");
                                            }
                                           localTax1Acct.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","tax1Acct"),
                                               xmlWriter);
                                        } if (localLineFxRateTracker){
                                    namespace = "urn:general_2017_2.transactions.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lineFxRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLineFxRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("lineFxRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLineFxRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:general_2017_2.transactions.webservices.netsuite.com")){
                return "ns31";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localLineSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "lineSubsidiary"));
                            
                            
                                    if (localLineSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("lineSubsidiary cannot be null!!");
                                    }
                                    elementList.add(localLineSubsidiary);
                                } if (localAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "account"));
                            
                            
                                    if (localAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                    }
                                    elementList.add(localAccount);
                                } if (localLineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "line"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLine));
                            } if (localDebitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "debit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDebit));
                            } if (localCreditTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "credit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCredit));
                            } if (localTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxCode"));
                            
                            
                                    if (localTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxCode cannot be null!!");
                                    }
                                    elementList.add(localTaxCode);
                                } if (localMemoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "memo"));
                                 
                                        if (localMemo != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMemo));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        }
                                    } if (localTaxRate1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "taxRate1"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxRate1));
                            } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localDueToFromSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "dueToFromSubsidiary"));
                            
                            
                                    if (localDueToFromSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("dueToFromSubsidiary cannot be null!!");
                                    }
                                    elementList.add(localDueToFromSubsidiary);
                                } if (localGrossAmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "grossAmt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGrossAmt));
                            } if (localScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "schedule"));
                            
                            
                                    if (localSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("schedule cannot be null!!");
                                    }
                                    elementList.add(localSchedule);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localRevenueRecognitionRuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "revenueRecognitionRule"));
                            
                            
                                    if (localRevenueRecognitionRule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueRecognitionRule cannot be null!!");
                                    }
                                    elementList.add(localRevenueRecognitionRule);
                                } if (localEliminateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "eliminate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEliminate));
                            } if (localResidualTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "residual"));
                                 
                                        if (localResidual != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResidual));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("residual cannot be null!!");
                                        }
                                    } if (localAmortizationSchedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizationSched"));
                            
                            
                                    if (localAmortizationSched==null){
                                         throw new org.apache.axis2.databinding.ADBException("amortizationSched cannot be null!!");
                                    }
                                    elementList.add(localAmortizationSched);
                                } if (localScheduleNumTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "scheduleNum"));
                            
                            
                                    if (localScheduleNum==null){
                                         throw new org.apache.axis2.databinding.ADBException("scheduleNum cannot be null!!");
                                    }
                                    elementList.add(localScheduleNum);
                                } if (localAmortizStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizStartDate"));
                                 
                                        if (localAmortizStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("amortizStartDate cannot be null!!");
                                        }
                                    } if (localAmortizationEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizationEndDate"));
                                 
                                        if (localAmortizationEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("amortizationEndDate cannot be null!!");
                                        }
                                    } if (localAmortizationResidualTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "amortizationResidual"));
                                 
                                        if (localAmortizationResidual != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationResidual));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("amortizationResidual cannot be null!!");
                                        }
                                    } if (localTax1AmtTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax1Amt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTax1Amt));
                            } if (localTax1AcctTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "tax1Acct"));
                            
                            
                                    if (localTax1Acct==null){
                                         throw new org.apache.axis2.databinding.ADBException("tax1Acct cannot be null!!");
                                    }
                                    elementList.add(localTax1Acct);
                                } if (localLineFxRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "lineFxRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLineFxRate));
                            } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static AdvInterCompanyJournalEntryLine parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            AdvInterCompanyJournalEntryLine object =
                new AdvInterCompanyJournalEntryLine();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"AdvInterCompanyJournalEntryLine".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AdvInterCompanyJournalEntryLine)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","lineSubsidiary").equals(reader.getName())){
                                
                                                object.setLineSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","account").equals(reader.getName())){
                                
                                                object.setAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","line").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"line" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLine(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLine(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","debit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"debit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDebit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDebit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","credit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"credit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCredit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCredit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","taxCode").equals(reader.getName())){
                                
                                                object.setTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"memo" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMemo(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","taxRate1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxRate1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxRate1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTaxRate1(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","dueToFromSubsidiary").equals(reader.getName())){
                                
                                                object.setDueToFromSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","grossAmt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"grossAmt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGrossAmt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setGrossAmt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","schedule").equals(reader.getName())){
                                
                                                object.setSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","revenueRecognitionRule").equals(reader.getName())){
                                
                                                object.setRevenueRecognitionRule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","eliminate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"eliminate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEliminate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","residual").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"residual" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setResidual(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","amortizationSched").equals(reader.getName())){
                                
                                                object.setAmortizationSched(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","scheduleNum").equals(reader.getName())){
                                
                                                object.setScheduleNum(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","amortizStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","amortizationEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizationEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizationEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","amortizationResidual").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizationResidual" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizationResidual(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","tax1Amt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tax1Amt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTax1Amt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTax1Amt(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","tax1Acct").equals(reader.getName())){
                                
                                                object.setTax1Acct(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","lineFxRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lineFxRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLineFxRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLineFxRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:general_2017_2.transactions.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    