
/**
 * SupportCaseSearch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.support_2017_2;
            

            /**
            *  SupportCaseSearch bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SupportCaseSearch extends com.netsuite.webservices.platform.core_2017_2.SearchRecord
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = SupportCaseSearch
                Namespace URI = urn:support_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns17
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for CompanyJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic localCompanyJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyJoinTracker = false ;

                           public boolean isCompanyJoinSpecified(){
                               return localCompanyJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic getCompanyJoin(){
                               return localCompanyJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompanyJoin
                               */
                               public void setCompanyJoin(com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic param){
                            localCompanyJoinTracker = param != null;
                                   
                                            this.localCompanyJoin=param;
                                    

                               }
                            

                        /**
                        * field for ContactJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic localContactJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactJoinTracker = false ;

                           public boolean isContactJoinSpecified(){
                               return localContactJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic getContactJoin(){
                               return localContactJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContactJoin
                               */
                               public void setContactJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic param){
                            localContactJoinTracker = param != null;
                                   
                                            this.localContactJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic localCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerJoinTracker = false ;

                           public boolean isCustomerJoinSpecified(){
                               return localCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic getCustomerJoin(){
                               return localCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerJoin
                               */
                               public void setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic param){
                            localCustomerJoinTracker = param != null;
                                   
                                            this.localCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localEmployeeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeJoinTracker = false ;

                           public boolean isEmployeeJoinSpecified(){
                               return localEmployeeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getEmployeeJoin(){
                               return localEmployeeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeJoin
                               */
                               public void setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localEmployeeJoinTracker = param != null;
                                   
                                            this.localEmployeeJoin=param;
                                    

                               }
                            

                        /**
                        * field for FileJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.FileSearchBasic localFileJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFileJoinTracker = false ;

                           public boolean isFileJoinSpecified(){
                               return localFileJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.FileSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.FileSearchBasic getFileJoin(){
                               return localFileJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FileJoin
                               */
                               public void setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchBasic param){
                            localFileJoinTracker = param != null;
                                   
                                            this.localFileJoin=param;
                                    

                               }
                            

                        /**
                        * field for IssueJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.IssueSearchBasic localIssueJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueJoinTracker = false ;

                           public boolean isIssueJoinSpecified(){
                               return localIssueJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.IssueSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.IssueSearchBasic getIssueJoin(){
                               return localIssueJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueJoin
                               */
                               public void setIssueJoin(com.netsuite.webservices.platform.common_2017_2.IssueSearchBasic param){
                            localIssueJoinTracker = param != null;
                                   
                                            this.localIssueJoin=param;
                                    

                               }
                            

                        /**
                        * field for ItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic localItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemJoinTracker = false ;

                           public boolean isItemJoinSpecified(){
                               return localItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic getItemJoin(){
                               return localItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemJoin
                               */
                               public void setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic param){
                            localItemJoinTracker = param != null;
                                   
                                            this.localItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic localMessagesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesJoinTracker = false ;

                           public boolean isMessagesJoinSpecified(){
                               return localMessagesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic getMessagesJoin(){
                               return localMessagesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesJoin
                               */
                               public void setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic param){
                            localMessagesJoinTracker = param != null;
                                   
                                            this.localMessagesJoin=param;
                                    

                               }
                            

                        /**
                        * field for TimeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic localTimeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeJoinTracker = false ;

                           public boolean isTimeJoinSpecified(){
                               return localTimeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic getTimeJoin(){
                               return localTimeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeJoin
                               */
                               public void setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic param){
                            localTimeJoinTracker = param != null;
                                   
                                            this.localTimeJoin=param;
                                    

                               }
                            

                        /**
                        * field for TransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic localTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionJoinTracker = false ;

                           public boolean isTransactionJoinSpecified(){
                               return localTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic getTransactionJoin(){
                               return localTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionJoin
                               */
                               public void setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic param){
                            localTransactionJoinTracker = param != null;
                                   
                                            this.localTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserNotesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic localUserNotesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserNotesJoinTracker = false ;

                           public boolean isUserNotesJoinSpecified(){
                               return localUserNotesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic getUserNotesJoin(){
                               return localUserNotesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserNotesJoin
                               */
                               public void setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic param){
                            localUserNotesJoinTracker = param != null;
                                   
                                            this.localUserNotesJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:support_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":SupportCaseSearch",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "SupportCaseSearch",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localCompanyJoinTracker){
                                            if (localCompanyJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("companyJoin cannot be null!!");
                                            }
                                           localCompanyJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","companyJoin"),
                                               xmlWriter);
                                        } if (localContactJoinTracker){
                                            if (localContactJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contactJoin cannot be null!!");
                                            }
                                           localContactJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","contactJoin"),
                                               xmlWriter);
                                        } if (localCustomerJoinTracker){
                                            if (localCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                            }
                                           localCustomerJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customerJoin"),
                                               xmlWriter);
                                        } if (localEmployeeJoinTracker){
                                            if (localEmployeeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                            }
                                           localEmployeeJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","employeeJoin"),
                                               xmlWriter);
                                        } if (localFileJoinTracker){
                                            if (localFileJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                            }
                                           localFileJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","fileJoin"),
                                               xmlWriter);
                                        } if (localIssueJoinTracker){
                                            if (localIssueJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueJoin cannot be null!!");
                                            }
                                           localIssueJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueJoin"),
                                               xmlWriter);
                                        } if (localItemJoinTracker){
                                            if (localItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                            }
                                           localItemJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","itemJoin"),
                                               xmlWriter);
                                        } if (localMessagesJoinTracker){
                                            if (localMessagesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                            }
                                           localMessagesJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","messagesJoin"),
                                               xmlWriter);
                                        } if (localTimeJoinTracker){
                                            if (localTimeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                            }
                                           localTimeJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","timeJoin"),
                                               xmlWriter);
                                        } if (localTransactionJoinTracker){
                                            if (localTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                            }
                                           localTransactionJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","transactionJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localUserNotesJoinTracker){
                                            if (localUserNotesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                            }
                                           localUserNotesJoin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","userNotesJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:support_2017_2.lists.webservices.netsuite.com")){
                return "ns17";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","SupportCaseSearch"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localCompanyJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "companyJoin"));
                            
                            
                                    if (localCompanyJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("companyJoin cannot be null!!");
                                    }
                                    elementList.add(localCompanyJoin);
                                } if (localContactJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "contactJoin"));
                            
                            
                                    if (localContactJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("contactJoin cannot be null!!");
                                    }
                                    elementList.add(localContactJoin);
                                } if (localCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "customerJoin"));
                            
                            
                                    if (localCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerJoin);
                                } if (localEmployeeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "employeeJoin"));
                            
                            
                                    if (localEmployeeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                    }
                                    elementList.add(localEmployeeJoin);
                                } if (localFileJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "fileJoin"));
                            
                            
                                    if (localFileJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                    }
                                    elementList.add(localFileJoin);
                                } if (localIssueJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issueJoin"));
                            
                            
                                    if (localIssueJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueJoin cannot be null!!");
                                    }
                                    elementList.add(localIssueJoin);
                                } if (localItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "itemJoin"));
                            
                            
                                    if (localItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                    }
                                    elementList.add(localItemJoin);
                                } if (localMessagesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesJoin"));
                            
                            
                                    if (localMessagesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesJoin);
                                } if (localTimeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "timeJoin"));
                            
                            
                                    if (localTimeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                    }
                                    elementList.add(localTimeJoin);
                                } if (localTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "transactionJoin"));
                            
                            
                                    if (localTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                    }
                                    elementList.add(localTransactionJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localUserNotesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "userNotesJoin"));
                            
                            
                                    if (localUserNotesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                    }
                                    elementList.add(localUserNotesJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SupportCaseSearch parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SupportCaseSearch object =
                new SupportCaseSearch();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"SupportCaseSearch".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SupportCaseSearch)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list14 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","companyJoin").equals(reader.getName())){
                                
                                                object.setCompanyJoin(com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","contactJoin").equals(reader.getName())){
                                
                                                object.setContactJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customerJoin").equals(reader.getName())){
                                
                                                object.setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","employeeJoin").equals(reader.getName())){
                                
                                                object.setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","fileJoin").equals(reader.getName())){
                                
                                                object.setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueJoin").equals(reader.getName())){
                                
                                                object.setIssueJoin(com.netsuite.webservices.platform.common_2017_2.IssueSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","itemJoin").equals(reader.getName())){
                                
                                                object.setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","messagesJoin").equals(reader.getName())){
                                
                                                object.setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","timeJoin").equals(reader.getName())){
                                
                                                object.setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","transactionJoin").equals(reader.getName())){
                                
                                                object.setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","userNotesJoin").equals(reader.getName())){
                                
                                                object.setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list14.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone14 = false;
                                                        while(!loopDone14){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone14 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list14.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone14 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.class,
                                                                list14));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    