
/**
 * Issue.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.support_2017_2;
            

            /**
            *  Issue bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Issue extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Issue
                Namespace URI = urn:support_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns17
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for IssueNumber
                        */

                        
                                    protected java.lang.String localIssueNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueNumberTracker = false ;

                           public boolean isIssueNumberSpecified(){
                               return localIssueNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIssueNumber(){
                               return localIssueNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueNumber
                               */
                               public void setIssueNumber(java.lang.String param){
                            localIssueNumberTracker = param != null;
                                   
                                            this.localIssueNumber=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for IssueType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIssueType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueTypeTracker = false ;

                           public boolean isIssueTypeSpecified(){
                               return localIssueTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIssueType(){
                               return localIssueType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueType
                               */
                               public void setIssueType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIssueTypeTracker = param != null;
                                   
                                            this.localIssueType=param;
                                    

                               }
                            

                        /**
                        * field for Product
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProduct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProductTracker = false ;

                           public boolean isProductSpecified(){
                               return localProductTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProduct(){
                               return localProduct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Product
                               */
                               public void setProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProductTracker = param != null;
                                   
                                            this.localProduct=param;
                                    

                               }
                            

                        /**
                        * field for Module
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localModule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localModuleTracker = false ;

                           public boolean isModuleSpecified(){
                               return localModuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getModule(){
                               return localModule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Module
                               */
                               public void setModule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localModuleTracker = param != null;
                                   
                                            this.localModule=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for ProductTeam
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProductTeam ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProductTeamTracker = false ;

                           public boolean isProductTeamSpecified(){
                               return localProductTeamTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProductTeam(){
                               return localProductTeam;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProductTeam
                               */
                               public void setProductTeam(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProductTeamTracker = param != null;
                                   
                                            this.localProductTeam=param;
                                    

                               }
                            

                        /**
                        * field for Source
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceTracker = false ;

                           public boolean isSourceSpecified(){
                               return localSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSource(){
                               return localSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Source
                               */
                               public void setSource(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSourceTracker = param != null;
                                   
                                            this.localSource=param;
                                    

                               }
                            

                        /**
                        * field for ReportedBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localReportedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReportedByTracker = false ;

                           public boolean isReportedBySpecified(){
                               return localReportedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getReportedBy(){
                               return localReportedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReportedBy
                               */
                               public void setReportedBy(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localReportedByTracker = param != null;
                                   
                                            this.localReportedBy=param;
                                    

                               }
                            

                        /**
                        * field for Reproduce
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localReproduce ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReproduceTracker = false ;

                           public boolean isReproduceSpecified(){
                               return localReproduceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getReproduce(){
                               return localReproduce;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reproduce
                               */
                               public void setReproduce(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localReproduceTracker = param != null;
                                   
                                            this.localReproduce=param;
                                    

                               }
                            

                        /**
                        * field for VersionBroken
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localVersionBroken ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVersionBrokenTracker = false ;

                           public boolean isVersionBrokenSpecified(){
                               return localVersionBrokenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getVersionBroken(){
                               return localVersionBroken;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VersionBroken
                               */
                               public void setVersionBroken(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localVersionBrokenTracker = param != null;
                                   
                                            this.localVersionBroken=param;
                                    

                               }
                            

                        /**
                        * field for BuildBroken
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBuildBroken ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildBrokenTracker = false ;

                           public boolean isBuildBrokenSpecified(){
                               return localBuildBrokenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBuildBroken(){
                               return localBuildBroken;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildBroken
                               */
                               public void setBuildBroken(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBuildBrokenTracker = param != null;
                                   
                                            this.localBuildBroken=param;
                                    

                               }
                            

                        /**
                        * field for VersionTarget
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localVersionTarget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVersionTargetTracker = false ;

                           public boolean isVersionTargetSpecified(){
                               return localVersionTargetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getVersionTarget(){
                               return localVersionTarget;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VersionTarget
                               */
                               public void setVersionTarget(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localVersionTargetTracker = param != null;
                                   
                                            this.localVersionTarget=param;
                                    

                               }
                            

                        /**
                        * field for BuildTarget
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBuildTarget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildTargetTracker = false ;

                           public boolean isBuildTargetSpecified(){
                               return localBuildTargetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBuildTarget(){
                               return localBuildTarget;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildTarget
                               */
                               public void setBuildTarget(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBuildTargetTracker = param != null;
                                   
                                            this.localBuildTarget=param;
                                    

                               }
                            

                        /**
                        * field for VersionFixed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localVersionFixed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVersionFixedTracker = false ;

                           public boolean isVersionFixedSpecified(){
                               return localVersionFixedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getVersionFixed(){
                               return localVersionFixed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VersionFixed
                               */
                               public void setVersionFixed(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localVersionFixedTracker = param != null;
                                   
                                            this.localVersionFixed=param;
                                    

                               }
                            

                        /**
                        * field for BuildFixed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBuildFixed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildFixedTracker = false ;

                           public boolean isBuildFixedSpecified(){
                               return localBuildFixedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBuildFixed(){
                               return localBuildFixed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildFixed
                               */
                               public void setBuildFixed(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBuildFixedTracker = param != null;
                                   
                                            this.localBuildFixed=param;
                                    

                               }
                            

                        /**
                        * field for Severity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSeverity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSeverityTracker = false ;

                           public boolean isSeveritySpecified(){
                               return localSeverityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSeverity(){
                               return localSeverity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Severity
                               */
                               public void setSeverity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSeverityTracker = param != null;
                                   
                                            this.localSeverity=param;
                                    

                               }
                            

                        /**
                        * field for Priority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityTracker = false ;

                           public boolean isPrioritySpecified(){
                               return localPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPriority(){
                               return localPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Priority
                               */
                               public void setPriority(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPriorityTracker = param != null;
                                   
                                            this.localPriority=param;
                                    

                               }
                            

                        /**
                        * field for IsShowStopper
                        */

                        
                                    protected boolean localIsShowStopper ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsShowStopperTracker = false ;

                           public boolean isIsShowStopperSpecified(){
                               return localIsShowStopperTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsShowStopper(){
                               return localIsShowStopper;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsShowStopper
                               */
                               public void setIsShowStopper(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsShowStopperTracker =
                                       true;
                                   
                                            this.localIsShowStopper=param;
                                    

                               }
                            

                        /**
                        * field for Assigned
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAssigned ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssignedTracker = false ;

                           public boolean isAssignedSpecified(){
                               return localAssignedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAssigned(){
                               return localAssigned;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Assigned
                               */
                               public void setAssigned(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAssignedTracker = param != null;
                                   
                                            this.localAssigned=param;
                                    

                               }
                            

                        /**
                        * field for Reviewer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localReviewer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReviewerTracker = false ;

                           public boolean isReviewerSpecified(){
                               return localReviewerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getReviewer(){
                               return localReviewer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reviewer
                               */
                               public void setReviewer(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localReviewerTracker = param != null;
                                   
                                            this.localReviewer=param;
                                    

                               }
                            

                        /**
                        * field for IsReviewed
                        */

                        
                                    protected boolean localIsReviewed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsReviewedTracker = false ;

                           public boolean isIsReviewedSpecified(){
                               return localIsReviewedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsReviewed(){
                               return localIsReviewed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsReviewed
                               */
                               public void setIsReviewed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsReviewedTracker =
                                       true;
                                   
                                            this.localIsReviewed=param;
                                    

                               }
                            

                        /**
                        * field for IssueStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIssueStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueStatusTracker = false ;

                           public boolean isIssueStatusSpecified(){
                               return localIssueStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIssueStatus(){
                               return localIssueStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueStatus
                               */
                               public void setIssueStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIssueStatusTracker = param != null;
                                   
                                            this.localIssueStatus=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for IssueTagsList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localIssueTagsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueTagsListTracker = false ;

                           public boolean isIssueTagsListSpecified(){
                               return localIssueTagsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getIssueTagsList(){
                               return localIssueTagsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueTagsList
                               */
                               public void setIssueTagsList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localIssueTagsListTracker = param != null;
                                   
                                            this.localIssueTagsList=param;
                                    

                               }
                            

                        /**
                        * field for IssueAbstract
                        */

                        
                                    protected java.lang.String localIssueAbstract ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueAbstractTracker = false ;

                           public boolean isIssueAbstractSpecified(){
                               return localIssueAbstractTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIssueAbstract(){
                               return localIssueAbstract;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueAbstract
                               */
                               public void setIssueAbstract(java.lang.String param){
                            localIssueAbstractTracker = param != null;
                                   
                                            this.localIssueAbstract=param;
                                    

                               }
                            

                        /**
                        * field for NewDetails
                        */

                        
                                    protected java.lang.String localNewDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNewDetailsTracker = false ;

                           public boolean isNewDetailsSpecified(){
                               return localNewDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNewDetails(){
                               return localNewDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NewDetails
                               */
                               public void setNewDetails(java.lang.String param){
                            localNewDetailsTracker = param != null;
                                   
                                            this.localNewDetails=param;
                                    

                               }
                            

                        /**
                        * field for IsOwner
                        */

                        
                                    protected boolean localIsOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOwnerTracker = false ;

                           public boolean isIsOwnerSpecified(){
                               return localIsOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsOwner(){
                               return localIsOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOwner
                               */
                               public void setIsOwner(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsOwnerTracker =
                                       true;
                                   
                                            this.localIsOwner=param;
                                    

                               }
                            

                        /**
                        * field for TrackCode
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.types.IssueTrackCode localTrackCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTrackCodeTracker = false ;

                           public boolean isTrackCodeSpecified(){
                               return localTrackCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.types.IssueTrackCode
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.types.IssueTrackCode getTrackCode(){
                               return localTrackCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TrackCode
                               */
                               public void setTrackCode(com.netsuite.webservices.lists.support_2017_2.types.IssueTrackCode param){
                            localTrackCodeTracker = param != null;
                                   
                                            this.localTrackCode=param;
                                    

                               }
                            

                        /**
                        * field for EmailAssignee
                        */

                        
                                    protected boolean localEmailAssignee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailAssigneeTracker = false ;

                           public boolean isEmailAssigneeSpecified(){
                               return localEmailAssigneeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEmailAssignee(){
                               return localEmailAssignee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailAssignee
                               */
                               public void setEmailAssignee(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEmailAssigneeTracker =
                                       true;
                                   
                                            this.localEmailAssignee=param;
                                    

                               }
                            

                        /**
                        * field for EmailEmployeesList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList localEmailEmployeesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailEmployeesListTracker = false ;

                           public boolean isEmailEmployeesListSpecified(){
                               return localEmailEmployeesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList getEmailEmployeesList(){
                               return localEmailEmployeesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailEmployeesList
                               */
                               public void setEmailEmployeesList(com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList param){
                            localEmailEmployeesListTracker = param != null;
                                   
                                            this.localEmailEmployeesList=param;
                                    

                               }
                            

                        /**
                        * field for EmailCellsList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localEmailCellsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailCellsListTracker = false ;

                           public boolean isEmailCellsListSpecified(){
                               return localEmailCellsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getEmailCellsList(){
                               return localEmailCellsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailCellsList
                               */
                               public void setEmailCellsList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localEmailCellsListTracker = param != null;
                                   
                                            this.localEmailCellsList=param;
                                    

                               }
                            

                        /**
                        * field for ExternalAbstract
                        */

                        
                                    protected java.lang.String localExternalAbstract ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalAbstractTracker = false ;

                           public boolean isExternalAbstractSpecified(){
                               return localExternalAbstractTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalAbstract(){
                               return localExternalAbstract;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalAbstract
                               */
                               public void setExternalAbstract(java.lang.String param){
                            localExternalAbstractTracker = param != null;
                                   
                                            this.localExternalAbstract=param;
                                    

                               }
                            

                        /**
                        * field for ExternalDetails
                        */

                        
                                    protected java.lang.String localExternalDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalDetailsTracker = false ;

                           public boolean isExternalDetailsSpecified(){
                               return localExternalDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalDetails(){
                               return localExternalDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalDetails
                               */
                               public void setExternalDetails(java.lang.String param){
                            localExternalDetailsTracker = param != null;
                                   
                                            this.localExternalDetails=param;
                                    

                               }
                            

                        /**
                        * field for BrokenInVersionList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.IssueVersionList localBrokenInVersionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBrokenInVersionListTracker = false ;

                           public boolean isBrokenInVersionListSpecified(){
                               return localBrokenInVersionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.IssueVersionList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.IssueVersionList getBrokenInVersionList(){
                               return localBrokenInVersionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BrokenInVersionList
                               */
                               public void setBrokenInVersionList(com.netsuite.webservices.lists.support_2017_2.IssueVersionList param){
                            localBrokenInVersionListTracker = param != null;
                                   
                                            this.localBrokenInVersionList=param;
                                    

                               }
                            

                        /**
                        * field for TargetVersionList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.IssueVersionList localTargetVersionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTargetVersionListTracker = false ;

                           public boolean isTargetVersionListSpecified(){
                               return localTargetVersionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.IssueVersionList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.IssueVersionList getTargetVersionList(){
                               return localTargetVersionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TargetVersionList
                               */
                               public void setTargetVersionList(com.netsuite.webservices.lists.support_2017_2.IssueVersionList param){
                            localTargetVersionListTracker = param != null;
                                   
                                            this.localTargetVersionList=param;
                                    

                               }
                            

                        /**
                        * field for FixedInVersionList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.IssueVersionList localFixedInVersionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFixedInVersionListTracker = false ;

                           public boolean isFixedInVersionListSpecified(){
                               return localFixedInVersionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.IssueVersionList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.IssueVersionList getFixedInVersionList(){
                               return localFixedInVersionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FixedInVersionList
                               */
                               public void setFixedInVersionList(com.netsuite.webservices.lists.support_2017_2.IssueVersionList param){
                            localFixedInVersionListTracker = param != null;
                                   
                                            this.localFixedInVersionList=param;
                                    

                               }
                            

                        /**
                        * field for RelatedIssuesList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssuesList localRelatedIssuesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelatedIssuesListTracker = false ;

                           public boolean isRelatedIssuesListSpecified(){
                               return localRelatedIssuesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssuesList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssuesList getRelatedIssuesList(){
                               return localRelatedIssuesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RelatedIssuesList
                               */
                               public void setRelatedIssuesList(com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssuesList param){
                            localRelatedIssuesListTracker = param != null;
                                   
                                            this.localRelatedIssuesList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:support_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Issue",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Issue",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localIssueNumberTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "issueNumber", xmlWriter);
                             

                                          if (localIssueNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("issueNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIssueNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIssueTypeTracker){
                                            if (localIssueType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueType cannot be null!!");
                                            }
                                           localIssueType.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueType"),
                                               xmlWriter);
                                        } if (localProductTracker){
                                            if (localProduct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("product cannot be null!!");
                                            }
                                           localProduct.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","product"),
                                               xmlWriter);
                                        } if (localModuleTracker){
                                            if (localModule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("module cannot be null!!");
                                            }
                                           localModule.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","module"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localProductTeamTracker){
                                            if (localProductTeam==null){
                                                 throw new org.apache.axis2.databinding.ADBException("productTeam cannot be null!!");
                                            }
                                           localProductTeam.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","productTeam"),
                                               xmlWriter);
                                        } if (localSourceTracker){
                                            if (localSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                            }
                                           localSource.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","source"),
                                               xmlWriter);
                                        } if (localReportedByTracker){
                                            if (localReportedBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reportedBy cannot be null!!");
                                            }
                                           localReportedBy.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","reportedBy"),
                                               xmlWriter);
                                        } if (localReproduceTracker){
                                            if (localReproduce==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reproduce cannot be null!!");
                                            }
                                           localReproduce.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","reproduce"),
                                               xmlWriter);
                                        } if (localVersionBrokenTracker){
                                            if (localVersionBroken==null){
                                                 throw new org.apache.axis2.databinding.ADBException("versionBroken cannot be null!!");
                                            }
                                           localVersionBroken.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","versionBroken"),
                                               xmlWriter);
                                        } if (localBuildBrokenTracker){
                                            if (localBuildBroken==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildBroken cannot be null!!");
                                            }
                                           localBuildBroken.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","buildBroken"),
                                               xmlWriter);
                                        } if (localVersionTargetTracker){
                                            if (localVersionTarget==null){
                                                 throw new org.apache.axis2.databinding.ADBException("versionTarget cannot be null!!");
                                            }
                                           localVersionTarget.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","versionTarget"),
                                               xmlWriter);
                                        } if (localBuildTargetTracker){
                                            if (localBuildTarget==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildTarget cannot be null!!");
                                            }
                                           localBuildTarget.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","buildTarget"),
                                               xmlWriter);
                                        } if (localVersionFixedTracker){
                                            if (localVersionFixed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("versionFixed cannot be null!!");
                                            }
                                           localVersionFixed.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","versionFixed"),
                                               xmlWriter);
                                        } if (localBuildFixedTracker){
                                            if (localBuildFixed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildFixed cannot be null!!");
                                            }
                                           localBuildFixed.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","buildFixed"),
                                               xmlWriter);
                                        } if (localSeverityTracker){
                                            if (localSeverity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("severity cannot be null!!");
                                            }
                                           localSeverity.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","severity"),
                                               xmlWriter);
                                        } if (localPriorityTracker){
                                            if (localPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                            }
                                           localPriority.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","priority"),
                                               xmlWriter);
                                        } if (localIsShowStopperTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isShowStopper", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isShowStopper cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsShowStopper));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAssignedTracker){
                                            if (localAssigned==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                            }
                                           localAssigned.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","assigned"),
                                               xmlWriter);
                                        } if (localReviewerTracker){
                                            if (localReviewer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reviewer cannot be null!!");
                                            }
                                           localReviewer.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","reviewer"),
                                               xmlWriter);
                                        } if (localIsReviewedTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isReviewed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isReviewed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsReviewed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIssueStatusTracker){
                                            if (localIssueStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueStatus cannot be null!!");
                                            }
                                           localIssueStatus.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueStatus"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIssueTagsListTracker){
                                            if (localIssueTagsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueTagsList cannot be null!!");
                                            }
                                           localIssueTagsList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueTagsList"),
                                               xmlWriter);
                                        } if (localIssueAbstractTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "issueAbstract", xmlWriter);
                             

                                          if (localIssueAbstract==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("issueAbstract cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIssueAbstract);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNewDetailsTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "newDetails", xmlWriter);
                             

                                          if (localNewDetails==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("newDetails cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNewDetails);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsOwnerTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isOwner", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isOwner cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOwner));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTrackCodeTracker){
                                            if (localTrackCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("trackCode cannot be null!!");
                                            }
                                           localTrackCode.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","trackCode"),
                                               xmlWriter);
                                        } if (localEmailAssigneeTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "emailAssignee", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("emailAssignee cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailAssignee));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailEmployeesListTracker){
                                            if (localEmailEmployeesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailEmployeesList cannot be null!!");
                                            }
                                           localEmailEmployeesList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailEmployeesList"),
                                               xmlWriter);
                                        } if (localEmailCellsListTracker){
                                            if (localEmailCellsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailCellsList cannot be null!!");
                                            }
                                           localEmailCellsList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailCellsList"),
                                               xmlWriter);
                                        } if (localExternalAbstractTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "externalAbstract", xmlWriter);
                             

                                          if (localExternalAbstract==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("externalAbstract cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localExternalAbstract);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExternalDetailsTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "externalDetails", xmlWriter);
                             

                                          if (localExternalDetails==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("externalDetails cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localExternalDetails);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBrokenInVersionListTracker){
                                            if (localBrokenInVersionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("brokenInVersionList cannot be null!!");
                                            }
                                           localBrokenInVersionList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","brokenInVersionList"),
                                               xmlWriter);
                                        } if (localTargetVersionListTracker){
                                            if (localTargetVersionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("targetVersionList cannot be null!!");
                                            }
                                           localTargetVersionList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","targetVersionList"),
                                               xmlWriter);
                                        } if (localFixedInVersionListTracker){
                                            if (localFixedInVersionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fixedInVersionList cannot be null!!");
                                            }
                                           localFixedInVersionList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","fixedInVersionList"),
                                               xmlWriter);
                                        } if (localRelatedIssuesListTracker){
                                            if (localRelatedIssuesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("relatedIssuesList cannot be null!!");
                                            }
                                           localRelatedIssuesList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","relatedIssuesList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:support_2017_2.lists.webservices.netsuite.com")){
                return "ns17";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","Issue"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localIssueNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issueNumber"));
                                 
                                        if (localIssueNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIssueNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("issueNumber cannot be null!!");
                                        }
                                    } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localIssueTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issueType"));
                            
                            
                                    if (localIssueType==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueType cannot be null!!");
                                    }
                                    elementList.add(localIssueType);
                                } if (localProductTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "product"));
                            
                            
                                    if (localProduct==null){
                                         throw new org.apache.axis2.databinding.ADBException("product cannot be null!!");
                                    }
                                    elementList.add(localProduct);
                                } if (localModuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "module"));
                            
                            
                                    if (localModule==null){
                                         throw new org.apache.axis2.databinding.ADBException("module cannot be null!!");
                                    }
                                    elementList.add(localModule);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localProductTeamTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "productTeam"));
                            
                            
                                    if (localProductTeam==null){
                                         throw new org.apache.axis2.databinding.ADBException("productTeam cannot be null!!");
                                    }
                                    elementList.add(localProductTeam);
                                } if (localSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "source"));
                            
                            
                                    if (localSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                    }
                                    elementList.add(localSource);
                                } if (localReportedByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "reportedBy"));
                            
                            
                                    if (localReportedBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("reportedBy cannot be null!!");
                                    }
                                    elementList.add(localReportedBy);
                                } if (localReproduceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "reproduce"));
                            
                            
                                    if (localReproduce==null){
                                         throw new org.apache.axis2.databinding.ADBException("reproduce cannot be null!!");
                                    }
                                    elementList.add(localReproduce);
                                } if (localVersionBrokenTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "versionBroken"));
                            
                            
                                    if (localVersionBroken==null){
                                         throw new org.apache.axis2.databinding.ADBException("versionBroken cannot be null!!");
                                    }
                                    elementList.add(localVersionBroken);
                                } if (localBuildBrokenTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "buildBroken"));
                            
                            
                                    if (localBuildBroken==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildBroken cannot be null!!");
                                    }
                                    elementList.add(localBuildBroken);
                                } if (localVersionTargetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "versionTarget"));
                            
                            
                                    if (localVersionTarget==null){
                                         throw new org.apache.axis2.databinding.ADBException("versionTarget cannot be null!!");
                                    }
                                    elementList.add(localVersionTarget);
                                } if (localBuildTargetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "buildTarget"));
                            
                            
                                    if (localBuildTarget==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildTarget cannot be null!!");
                                    }
                                    elementList.add(localBuildTarget);
                                } if (localVersionFixedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "versionFixed"));
                            
                            
                                    if (localVersionFixed==null){
                                         throw new org.apache.axis2.databinding.ADBException("versionFixed cannot be null!!");
                                    }
                                    elementList.add(localVersionFixed);
                                } if (localBuildFixedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "buildFixed"));
                            
                            
                                    if (localBuildFixed==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildFixed cannot be null!!");
                                    }
                                    elementList.add(localBuildFixed);
                                } if (localSeverityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "severity"));
                            
                            
                                    if (localSeverity==null){
                                         throw new org.apache.axis2.databinding.ADBException("severity cannot be null!!");
                                    }
                                    elementList.add(localSeverity);
                                } if (localPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "priority"));
                            
                            
                                    if (localPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                    }
                                    elementList.add(localPriority);
                                } if (localIsShowStopperTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "isShowStopper"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsShowStopper));
                            } if (localAssignedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "assigned"));
                            
                            
                                    if (localAssigned==null){
                                         throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                    }
                                    elementList.add(localAssigned);
                                } if (localReviewerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "reviewer"));
                            
                            
                                    if (localReviewer==null){
                                         throw new org.apache.axis2.databinding.ADBException("reviewer cannot be null!!");
                                    }
                                    elementList.add(localReviewer);
                                } if (localIsReviewedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "isReviewed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsReviewed));
                            } if (localIssueStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issueStatus"));
                            
                            
                                    if (localIssueStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueStatus cannot be null!!");
                                    }
                                    elementList.add(localIssueStatus);
                                } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localIssueTagsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issueTagsList"));
                            
                            
                                    if (localIssueTagsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueTagsList cannot be null!!");
                                    }
                                    elementList.add(localIssueTagsList);
                                } if (localIssueAbstractTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issueAbstract"));
                                 
                                        if (localIssueAbstract != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIssueAbstract));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("issueAbstract cannot be null!!");
                                        }
                                    } if (localNewDetailsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "newDetails"));
                                 
                                        if (localNewDetails != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNewDetails));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("newDetails cannot be null!!");
                                        }
                                    } if (localIsOwnerTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "isOwner"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOwner));
                            } if (localTrackCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "trackCode"));
                            
                            
                                    if (localTrackCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("trackCode cannot be null!!");
                                    }
                                    elementList.add(localTrackCode);
                                } if (localEmailAssigneeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "emailAssignee"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailAssignee));
                            } if (localEmailEmployeesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "emailEmployeesList"));
                            
                            
                                    if (localEmailEmployeesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailEmployeesList cannot be null!!");
                                    }
                                    elementList.add(localEmailEmployeesList);
                                } if (localEmailCellsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "emailCellsList"));
                            
                            
                                    if (localEmailCellsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailCellsList cannot be null!!");
                                    }
                                    elementList.add(localEmailCellsList);
                                } if (localExternalAbstractTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "externalAbstract"));
                                 
                                        if (localExternalAbstract != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalAbstract));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("externalAbstract cannot be null!!");
                                        }
                                    } if (localExternalDetailsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "externalDetails"));
                                 
                                        if (localExternalDetails != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalDetails));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("externalDetails cannot be null!!");
                                        }
                                    } if (localBrokenInVersionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "brokenInVersionList"));
                            
                            
                                    if (localBrokenInVersionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("brokenInVersionList cannot be null!!");
                                    }
                                    elementList.add(localBrokenInVersionList);
                                } if (localTargetVersionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "targetVersionList"));
                            
                            
                                    if (localTargetVersionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("targetVersionList cannot be null!!");
                                    }
                                    elementList.add(localTargetVersionList);
                                } if (localFixedInVersionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "fixedInVersionList"));
                            
                            
                                    if (localFixedInVersionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("fixedInVersionList cannot be null!!");
                                    }
                                    elementList.add(localFixedInVersionList);
                                } if (localRelatedIssuesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "relatedIssuesList"));
                            
                            
                                    if (localRelatedIssuesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("relatedIssuesList cannot be null!!");
                                    }
                                    elementList.add(localRelatedIssuesList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Issue parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Issue object =
                new Issue();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Issue".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Issue)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"issueNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIssueNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueType").equals(reader.getName())){
                                
                                                object.setIssueType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","product").equals(reader.getName())){
                                
                                                object.setProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","module").equals(reader.getName())){
                                
                                                object.setModule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","productTeam").equals(reader.getName())){
                                
                                                object.setProductTeam(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","source").equals(reader.getName())){
                                
                                                object.setSource(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","reportedBy").equals(reader.getName())){
                                
                                                object.setReportedBy(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","reproduce").equals(reader.getName())){
                                
                                                object.setReproduce(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","versionBroken").equals(reader.getName())){
                                
                                                object.setVersionBroken(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","buildBroken").equals(reader.getName())){
                                
                                                object.setBuildBroken(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","versionTarget").equals(reader.getName())){
                                
                                                object.setVersionTarget(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","buildTarget").equals(reader.getName())){
                                
                                                object.setBuildTarget(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","versionFixed").equals(reader.getName())){
                                
                                                object.setVersionFixed(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","buildFixed").equals(reader.getName())){
                                
                                                object.setBuildFixed(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","severity").equals(reader.getName())){
                                
                                                object.setSeverity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","priority").equals(reader.getName())){
                                
                                                object.setPriority(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","isShowStopper").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isShowStopper" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsShowStopper(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","assigned").equals(reader.getName())){
                                
                                                object.setAssigned(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","reviewer").equals(reader.getName())){
                                
                                                object.setReviewer(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","isReviewed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isReviewed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsReviewed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueStatus").equals(reader.getName())){
                                
                                                object.setIssueStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueTagsList").equals(reader.getName())){
                                
                                                object.setIssueTagsList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issueAbstract").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"issueAbstract" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIssueAbstract(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","newDetails").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"newDetails" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNewDetails(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","isOwner").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isOwner" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsOwner(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","trackCode").equals(reader.getName())){
                                
                                                object.setTrackCode(com.netsuite.webservices.lists.support_2017_2.types.IssueTrackCode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailAssignee").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"emailAssignee" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmailAssignee(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailEmployeesList").equals(reader.getName())){
                                
                                                object.setEmailEmployeesList(com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailCellsList").equals(reader.getName())){
                                
                                                object.setEmailCellsList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","externalAbstract").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"externalAbstract" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExternalAbstract(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","externalDetails").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"externalDetails" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExternalDetails(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","brokenInVersionList").equals(reader.getName())){
                                
                                                object.setBrokenInVersionList(com.netsuite.webservices.lists.support_2017_2.IssueVersionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","targetVersionList").equals(reader.getName())){
                                
                                                object.setTargetVersionList(com.netsuite.webservices.lists.support_2017_2.IssueVersionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","fixedInVersionList").equals(reader.getName())){
                                
                                                object.setFixedInVersionList(com.netsuite.webservices.lists.support_2017_2.IssueVersionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","relatedIssuesList").equals(reader.getName())){
                                
                                                object.setRelatedIssuesList(com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssuesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    