
/**
 * SupportCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.support_2017_2;
            

            /**
            *  SupportCase bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SupportCase extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = SupportCase
                Namespace URI = urn:support_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns17
                */
            

                        /**
                        * field for EscalationMessage
                        */

                        
                                    protected java.lang.String localEscalationMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEscalationMessageTracker = false ;

                           public boolean isEscalationMessageSpecified(){
                               return localEscalationMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEscalationMessage(){
                               return localEscalationMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EscalationMessage
                               */
                               public void setEscalationMessage(java.lang.String param){
                            localEscalationMessageTracker = param != null;
                                   
                                            this.localEscalationMessage=param;
                                    

                               }
                            

                        /**
                        * field for LastReopenedDate
                        */

                        
                                    protected java.util.Calendar localLastReopenedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastReopenedDateTracker = false ;

                           public boolean isLastReopenedDateSpecified(){
                               return localLastReopenedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastReopenedDate(){
                               return localLastReopenedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastReopenedDate
                               */
                               public void setLastReopenedDate(java.util.Calendar param){
                            localLastReopenedDateTracker = param != null;
                                   
                                            this.localLastReopenedDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for IncomingMessage
                        */

                        
                                    protected java.lang.String localIncomingMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncomingMessageTracker = false ;

                           public boolean isIncomingMessageSpecified(){
                               return localIncomingMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIncomingMessage(){
                               return localIncomingMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncomingMessage
                               */
                               public void setIncomingMessage(java.lang.String param){
                            localIncomingMessageTracker = param != null;
                                   
                                            this.localIncomingMessage=param;
                                    

                               }
                            

                        /**
                        * field for InsertSolution
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInsertSolution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsertSolutionTracker = false ;

                           public boolean isInsertSolutionSpecified(){
                               return localInsertSolutionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInsertSolution(){
                               return localInsertSolution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsertSolution
                               */
                               public void setInsertSolution(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInsertSolutionTracker = param != null;
                                   
                                            this.localInsertSolution=param;
                                    

                               }
                            

                        /**
                        * field for OutgoingMessage
                        */

                        
                                    protected java.lang.String localOutgoingMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOutgoingMessageTracker = false ;

                           public boolean isOutgoingMessageSpecified(){
                               return localOutgoingMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOutgoingMessage(){
                               return localOutgoingMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OutgoingMessage
                               */
                               public void setOutgoingMessage(java.lang.String param){
                            localOutgoingMessageTracker = param != null;
                                   
                                            this.localOutgoingMessage=param;
                                    

                               }
                            

                        /**
                        * field for SearchSolution
                        */

                        
                                    protected java.lang.String localSearchSolution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchSolutionTracker = false ;

                           public boolean isSearchSolutionSpecified(){
                               return localSearchSolutionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSearchSolution(){
                               return localSearchSolution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchSolution
                               */
                               public void setSearchSolution(java.lang.String param){
                            localSearchSolutionTracker = param != null;
                                   
                                            this.localSearchSolution=param;
                                    

                               }
                            

                        /**
                        * field for EmailForm
                        */

                        
                                    protected boolean localEmailForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailFormTracker = false ;

                           public boolean isEmailFormSpecified(){
                               return localEmailFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEmailForm(){
                               return localEmailForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailForm
                               */
                               public void setEmailForm(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEmailFormTracker =
                                       true;
                                   
                                            this.localEmailForm=param;
                                    

                               }
                            

                        /**
                        * field for NewSolutionFromMsg
                        */

                        
                                    protected java.lang.String localNewSolutionFromMsg ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNewSolutionFromMsgTracker = false ;

                           public boolean isNewSolutionFromMsgSpecified(){
                               return localNewSolutionFromMsgTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNewSolutionFromMsg(){
                               return localNewSolutionFromMsg;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NewSolutionFromMsg
                               */
                               public void setNewSolutionFromMsg(java.lang.String param){
                            localNewSolutionFromMsgTracker = param != null;
                                   
                                            this.localNewSolutionFromMsg=param;
                                    

                               }
                            

                        /**
                        * field for InternalOnly
                        */

                        
                                    protected boolean localInternalOnly ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalOnlyTracker = false ;

                           public boolean isInternalOnlySpecified(){
                               return localInternalOnlyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getInternalOnly(){
                               return localInternalOnly;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalOnly
                               */
                               public void setInternalOnly(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localInternalOnlyTracker =
                                       true;
                                   
                                            this.localInternalOnly=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for CaseNumber
                        */

                        
                                    protected java.lang.String localCaseNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseNumberTracker = false ;

                           public boolean isCaseNumberSpecified(){
                               return localCaseNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCaseNumber(){
                               return localCaseNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseNumber
                               */
                               public void setCaseNumber(java.lang.String param){
                            localCaseNumberTracker = param != null;
                                   
                                            this.localCaseNumber=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastMessageDate
                        */

                        
                                    protected java.util.Calendar localLastMessageDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastMessageDateTracker = false ;

                           public boolean isLastMessageDateSpecified(){
                               return localLastMessageDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastMessageDate(){
                               return localLastMessageDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastMessageDate
                               */
                               public void setLastMessageDate(java.util.Calendar param){
                            localLastMessageDateTracker = param != null;
                                   
                                            this.localLastMessageDate=param;
                                    

                               }
                            

                        /**
                        * field for Company
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCompany ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyTracker = false ;

                           public boolean isCompanySpecified(){
                               return localCompanyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCompany(){
                               return localCompany;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Company
                               */
                               public void setCompany(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCompanyTracker = param != null;
                                   
                                            this.localCompany=param;
                                    

                               }
                            

                        /**
                        * field for Profile
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProfile ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProfileTracker = false ;

                           public boolean isProfileSpecified(){
                               return localProfileTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProfile(){
                               return localProfile;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Profile
                               */
                               public void setProfile(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProfileTracker = param != null;
                                   
                                            this.localProfile=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected java.lang.String localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(java.lang.String param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected java.lang.String localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(java.lang.String param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for Product
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProduct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProductTracker = false ;

                           public boolean isProductSpecified(){
                               return localProductTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProduct(){
                               return localProduct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Product
                               */
                               public void setProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProductTracker = param != null;
                                   
                                            this.localProduct=param;
                                    

                               }
                            

                        /**
                        * field for Module
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localModule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localModuleTracker = false ;

                           public boolean isModuleSpecified(){
                               return localModuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getModule(){
                               return localModule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Module
                               */
                               public void setModule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localModuleTracker = param != null;
                                   
                                            this.localModule=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for SerialNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSerialNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSerialNumberTracker = false ;

                           public boolean isSerialNumberSpecified(){
                               return localSerialNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSerialNumber(){
                               return localSerialNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SerialNumber
                               */
                               public void setSerialNumber(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSerialNumberTracker = param != null;
                                   
                                            this.localSerialNumber=param;
                                    

                               }
                            

                        /**
                        * field for InboundEmail
                        */

                        
                                    protected java.lang.String localInboundEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInboundEmailTracker = false ;

                           public boolean isInboundEmailSpecified(){
                               return localInboundEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInboundEmail(){
                               return localInboundEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InboundEmail
                               */
                               public void setInboundEmail(java.lang.String param){
                            localInboundEmailTracker = param != null;
                                   
                                            this.localInboundEmail=param;
                                    

                               }
                            

                        /**
                        * field for Issue
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIssue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueTracker = false ;

                           public boolean isIssueSpecified(){
                               return localIssueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIssue(){
                               return localIssue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Issue
                               */
                               public void setIssue(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIssueTracker = param != null;
                                   
                                            this.localIssue=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for Priority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityTracker = false ;

                           public boolean isPrioritySpecified(){
                               return localPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPriority(){
                               return localPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Priority
                               */
                               public void setPriority(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPriorityTracker = param != null;
                                   
                                            this.localPriority=param;
                                    

                               }
                            

                        /**
                        * field for Origin
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOrigin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOriginTracker = false ;

                           public boolean isOriginSpecified(){
                               return localOriginTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOrigin(){
                               return localOrigin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Origin
                               */
                               public void setOrigin(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOriginTracker = param != null;
                                   
                                            this.localOrigin=param;
                                    

                               }
                            

                        /**
                        * field for Category
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryTracker = false ;

                           public boolean isCategorySpecified(){
                               return localCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCategory(){
                               return localCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Category
                               */
                               public void setCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCategoryTracker = param != null;
                                   
                                            this.localCategory=param;
                                    

                               }
                            

                        /**
                        * field for Assigned
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAssigned ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssignedTracker = false ;

                           public boolean isAssignedSpecified(){
                               return localAssignedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAssigned(){
                               return localAssigned;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Assigned
                               */
                               public void setAssigned(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAssignedTracker = param != null;
                                   
                                            this.localAssigned=param;
                                    

                               }
                            

                        /**
                        * field for HelpDesk
                        */

                        
                                    protected boolean localHelpDesk ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHelpDeskTracker = false ;

                           public boolean isHelpDeskSpecified(){
                               return localHelpDeskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getHelpDesk(){
                               return localHelpDesk;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HelpDesk
                               */
                               public void setHelpDesk(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localHelpDeskTracker =
                                       true;
                                   
                                            this.localHelpDesk=param;
                                    

                               }
                            

                        /**
                        * field for EmailEmployeesList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList localEmailEmployeesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailEmployeesListTracker = false ;

                           public boolean isEmailEmployeesListSpecified(){
                               return localEmailEmployeesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList getEmailEmployeesList(){
                               return localEmailEmployeesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailEmployeesList
                               */
                               public void setEmailEmployeesList(com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList param){
                            localEmailEmployeesListTracker = param != null;
                                   
                                            this.localEmailEmployeesList=param;
                                    

                               }
                            

                        /**
                        * field for EscalateToList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateToList localEscalateToList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEscalateToListTracker = false ;

                           public boolean isEscalateToListSpecified(){
                               return localEscalateToListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateToList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateToList getEscalateToList(){
                               return localEscalateToList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EscalateToList
                               */
                               public void setEscalateToList(com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateToList param){
                            localEscalateToListTracker = param != null;
                                   
                                            this.localEscalateToList=param;
                                    

                               }
                            

                        /**
                        * field for TimeItemList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.SupportCaseTimeItemList localTimeItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeItemListTracker = false ;

                           public boolean isTimeItemListSpecified(){
                               return localTimeItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.SupportCaseTimeItemList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.SupportCaseTimeItemList getTimeItemList(){
                               return localTimeItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeItemList
                               */
                               public void setTimeItemList(com.netsuite.webservices.lists.support_2017_2.SupportCaseTimeItemList param){
                            localTimeItemListTracker = param != null;
                                   
                                            this.localTimeItemList=param;
                                    

                               }
                            

                        /**
                        * field for SolutionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutionsList localSolutionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSolutionsListTracker = false ;

                           public boolean isSolutionsListSpecified(){
                               return localSolutionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutionsList
                           */
                           public  com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutionsList getSolutionsList(){
                               return localSolutionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SolutionsList
                               */
                               public void setSolutionsList(com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutionsList param){
                            localSolutionsListTracker = param != null;
                                   
                                            this.localSolutionsList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:support_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":SupportCase",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "SupportCase",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localEscalationMessageTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "escalationMessage", xmlWriter);
                             

                                          if (localEscalationMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("escalationMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEscalationMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastReopenedDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastReopenedDate", xmlWriter);
                             

                                          if (localLastReopenedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastReopenedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastReopenedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncomingMessageTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "incomingMessage", xmlWriter);
                             

                                          if (localIncomingMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("incomingMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIncomingMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsertSolutionTracker){
                                            if (localInsertSolution==null){
                                                 throw new org.apache.axis2.databinding.ADBException("insertSolution cannot be null!!");
                                            }
                                           localInsertSolution.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","insertSolution"),
                                               xmlWriter);
                                        } if (localOutgoingMessageTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "outgoingMessage", xmlWriter);
                             

                                          if (localOutgoingMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("outgoingMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOutgoingMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSearchSolutionTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "searchSolution", xmlWriter);
                             

                                          if (localSearchSolution==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("searchSolution cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSearchSolution);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailFormTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "emailForm", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("emailForm cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailForm));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNewSolutionFromMsgTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "newSolutionFromMsg", xmlWriter);
                             

                                          if (localNewSolutionFromMsg==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("newSolutionFromMsg cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNewSolutionFromMsg);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInternalOnlyTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "internalOnly", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("internalOnly cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalOnly));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCaseNumberTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "caseNumber", xmlWriter);
                             

                                          if (localCaseNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("caseNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCaseNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastMessageDateTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastMessageDate", xmlWriter);
                             

                                          if (localLastMessageDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastMessageDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastMessageDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCompanyTracker){
                                            if (localCompany==null){
                                                 throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                            }
                                           localCompany.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","company"),
                                               xmlWriter);
                                        } if (localProfileTracker){
                                            if (localProfile==null){
                                                 throw new org.apache.axis2.databinding.ADBException("profile cannot be null!!");
                                            }
                                           localProfile.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","profile"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localEmailTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "email", xmlWriter);
                             

                                          if (localEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPhoneTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "phone", xmlWriter);
                             

                                          if (localPhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localProductTracker){
                                            if (localProduct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("product cannot be null!!");
                                            }
                                           localProduct.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","product"),
                                               xmlWriter);
                                        } if (localModuleTracker){
                                            if (localModule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("module cannot be null!!");
                                            }
                                           localModule.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","module"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localSerialNumberTracker){
                                            if (localSerialNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("serialNumber cannot be null!!");
                                            }
                                           localSerialNumber.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","serialNumber"),
                                               xmlWriter);
                                        } if (localInboundEmailTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "inboundEmail", xmlWriter);
                             

                                          if (localInboundEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("inboundEmail cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localInboundEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIssueTracker){
                                            if (localIssue==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issue cannot be null!!");
                                            }
                                           localIssue.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issue"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPriorityTracker){
                                            if (localPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                            }
                                           localPriority.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","priority"),
                                               xmlWriter);
                                        } if (localOriginTracker){
                                            if (localOrigin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("origin cannot be null!!");
                                            }
                                           localOrigin.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","origin"),
                                               xmlWriter);
                                        } if (localCategoryTracker){
                                            if (localCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                            }
                                           localCategory.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","category"),
                                               xmlWriter);
                                        } if (localAssignedTracker){
                                            if (localAssigned==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                            }
                                           localAssigned.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","assigned"),
                                               xmlWriter);
                                        } if (localHelpDeskTracker){
                                    namespace = "urn:support_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "helpDesk", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("helpDesk cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHelpDesk));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailEmployeesListTracker){
                                            if (localEmailEmployeesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailEmployeesList cannot be null!!");
                                            }
                                           localEmailEmployeesList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailEmployeesList"),
                                               xmlWriter);
                                        } if (localEscalateToListTracker){
                                            if (localEscalateToList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("escalateToList cannot be null!!");
                                            }
                                           localEscalateToList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","escalateToList"),
                                               xmlWriter);
                                        } if (localTimeItemListTracker){
                                            if (localTimeItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeItemList cannot be null!!");
                                            }
                                           localTimeItemList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","timeItemList"),
                                               xmlWriter);
                                        } if (localSolutionsListTracker){
                                            if (localSolutionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("solutionsList cannot be null!!");
                                            }
                                           localSolutionsList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","solutionsList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:support_2017_2.lists.webservices.netsuite.com")){
                return "ns17";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","SupportCase"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localEscalationMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "escalationMessage"));
                                 
                                        if (localEscalationMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEscalationMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("escalationMessage cannot be null!!");
                                        }
                                    } if (localLastReopenedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "lastReopenedDate"));
                                 
                                        if (localLastReopenedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastReopenedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastReopenedDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localIncomingMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "incomingMessage"));
                                 
                                        if (localIncomingMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncomingMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("incomingMessage cannot be null!!");
                                        }
                                    } if (localInsertSolutionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "insertSolution"));
                            
                            
                                    if (localInsertSolution==null){
                                         throw new org.apache.axis2.databinding.ADBException("insertSolution cannot be null!!");
                                    }
                                    elementList.add(localInsertSolution);
                                } if (localOutgoingMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "outgoingMessage"));
                                 
                                        if (localOutgoingMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOutgoingMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("outgoingMessage cannot be null!!");
                                        }
                                    } if (localSearchSolutionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "searchSolution"));
                                 
                                        if (localSearchSolution != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSearchSolution));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("searchSolution cannot be null!!");
                                        }
                                    } if (localEmailFormTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "emailForm"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailForm));
                            } if (localNewSolutionFromMsgTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "newSolutionFromMsg"));
                                 
                                        if (localNewSolutionFromMsg != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNewSolutionFromMsg));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("newSolutionFromMsg cannot be null!!");
                                        }
                                    } if (localInternalOnlyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "internalOnly"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalOnly));
                            } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localCaseNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "caseNumber"));
                                 
                                        if (localCaseNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCaseNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("caseNumber cannot be null!!");
                                        }
                                    } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localLastMessageDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "lastMessageDate"));
                                 
                                        if (localLastMessageDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastMessageDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastMessageDate cannot be null!!");
                                        }
                                    } if (localCompanyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "company"));
                            
                            
                                    if (localCompany==null){
                                         throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                    }
                                    elementList.add(localCompany);
                                } if (localProfileTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "profile"));
                            
                            
                                    if (localProfile==null){
                                         throw new org.apache.axis2.databinding.ADBException("profile cannot be null!!");
                                    }
                                    elementList.add(localProfile);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "email"));
                                 
                                        if (localEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                        }
                                    } if (localPhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "phone"));
                                 
                                        if (localPhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                        }
                                    } if (localProductTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "product"));
                            
                            
                                    if (localProduct==null){
                                         throw new org.apache.axis2.databinding.ADBException("product cannot be null!!");
                                    }
                                    elementList.add(localProduct);
                                } if (localModuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "module"));
                            
                            
                                    if (localModule==null){
                                         throw new org.apache.axis2.databinding.ADBException("module cannot be null!!");
                                    }
                                    elementList.add(localModule);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localSerialNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "serialNumber"));
                            
                            
                                    if (localSerialNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("serialNumber cannot be null!!");
                                    }
                                    elementList.add(localSerialNumber);
                                } if (localInboundEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "inboundEmail"));
                                 
                                        if (localInboundEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInboundEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("inboundEmail cannot be null!!");
                                        }
                                    } if (localIssueTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "issue"));
                            
                            
                                    if (localIssue==null){
                                         throw new org.apache.axis2.databinding.ADBException("issue cannot be null!!");
                                    }
                                    elementList.add(localIssue);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "priority"));
                            
                            
                                    if (localPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                    }
                                    elementList.add(localPriority);
                                } if (localOriginTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "origin"));
                            
                            
                                    if (localOrigin==null){
                                         throw new org.apache.axis2.databinding.ADBException("origin cannot be null!!");
                                    }
                                    elementList.add(localOrigin);
                                } if (localCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "category"));
                            
                            
                                    if (localCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                    }
                                    elementList.add(localCategory);
                                } if (localAssignedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "assigned"));
                            
                            
                                    if (localAssigned==null){
                                         throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                    }
                                    elementList.add(localAssigned);
                                } if (localHelpDeskTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "helpDesk"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHelpDesk));
                            } if (localEmailEmployeesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "emailEmployeesList"));
                            
                            
                                    if (localEmailEmployeesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailEmployeesList cannot be null!!");
                                    }
                                    elementList.add(localEmailEmployeesList);
                                } if (localEscalateToListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "escalateToList"));
                            
                            
                                    if (localEscalateToList==null){
                                         throw new org.apache.axis2.databinding.ADBException("escalateToList cannot be null!!");
                                    }
                                    elementList.add(localEscalateToList);
                                } if (localTimeItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "timeItemList"));
                            
                            
                                    if (localTimeItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeItemList cannot be null!!");
                                    }
                                    elementList.add(localTimeItemList);
                                } if (localSolutionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "solutionsList"));
                            
                            
                                    if (localSolutionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("solutionsList cannot be null!!");
                                    }
                                    elementList.add(localSolutionsList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SupportCase parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SupportCase object =
                new SupportCase();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"SupportCase".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SupportCase)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","escalationMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"escalationMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEscalationMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","lastReopenedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastReopenedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastReopenedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","incomingMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"incomingMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncomingMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","insertSolution").equals(reader.getName())){
                                
                                                object.setInsertSolution(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","outgoingMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"outgoingMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOutgoingMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","searchSolution").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"searchSolution" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSearchSolution(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailForm").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"emailForm" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmailForm(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","newSolutionFromMsg").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"newSolutionFromMsg" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNewSolutionFromMsg(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","internalOnly").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"internalOnly" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInternalOnly(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"title" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","caseNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"caseNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCaseNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","lastMessageDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastMessageDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastMessageDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","company").equals(reader.getName())){
                                
                                                object.setCompany(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","profile").equals(reader.getName())){
                                
                                                object.setProfile(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"email" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"phone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","product").equals(reader.getName())){
                                
                                                object.setProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","module").equals(reader.getName())){
                                
                                                object.setModule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","serialNumber").equals(reader.getName())){
                                
                                                object.setSerialNumber(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","inboundEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"inboundEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInboundEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","issue").equals(reader.getName())){
                                
                                                object.setIssue(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","priority").equals(reader.getName())){
                                
                                                object.setPriority(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","origin").equals(reader.getName())){
                                
                                                object.setOrigin(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","category").equals(reader.getName())){
                                
                                                object.setCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","assigned").equals(reader.getName())){
                                
                                                object.setAssigned(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","helpDesk").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"helpDesk" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHelpDesk(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","emailEmployeesList").equals(reader.getName())){
                                
                                                object.setEmailEmployeesList(com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","escalateToList").equals(reader.getName())){
                                
                                                object.setEscalateToList(com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateToList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","timeItemList").equals(reader.getName())){
                                
                                                object.setTimeItemList(com.netsuite.webservices.lists.support_2017_2.SupportCaseTimeItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","solutionsList").equals(reader.getName())){
                                
                                                object.setSolutionsList(com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:support_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    