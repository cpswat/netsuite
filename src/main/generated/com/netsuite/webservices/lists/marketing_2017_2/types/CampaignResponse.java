
/**
 * CampaignResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.marketing_2017_2.types;
            

            /**
            *  CampaignResponse bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CampaignResponse
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.marketing_2017_2.lists.webservices.netsuite.com",
                "CampaignResponse",
                "ns39");

            

                        /**
                        * field for CampaignResponse
                        */

                        
                                    protected java.lang.String localCampaignResponse ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected CampaignResponse(java.lang.String value, boolean isRegisterValue) {
                                    localCampaignResponse = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localCampaignResponse, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __clickedThrough =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_clickedThrough");
                                
                                    public static final java.lang.String __failedDeliveryFailure =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_failedDeliveryFailure");
                                
                                    public static final java.lang.String __failedInvalidAddress =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_failedInvalidAddress");
                                
                                    public static final java.lang.String __failedOther =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_failedOther");
                                
                                    public static final java.lang.String __failedSpam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_failedSpam");
                                
                                    public static final java.lang.String __failedTemplateError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_failedTemplateError");
                                
                                    public static final java.lang.String __failedUnexpectedError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_failedUnexpectedError");
                                
                                    public static final java.lang.String __invalidSenderAddress =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invalidSenderAddress");
                                
                                    public static final java.lang.String __mailboxDisabled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mailboxDisabled");
                                
                                    public static final java.lang.String __mailboxIsFull =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mailboxIsFull");
                                
                                    public static final java.lang.String __mailboxNotAcceptingMessages =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mailboxNotAcceptingMessages");
                                
                                    public static final java.lang.String __mailProtocolIssues =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mailProtocolIssues");
                                
                                    public static final java.lang.String __mediaError =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mediaError");
                                
                                    public static final java.lang.String __messageExceedsSizeLengthLimits =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_messageExceedsSizeLengthLimits");
                                
                                    public static final java.lang.String __networkServerIssues =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_networkServerIssues");
                                
                                    public static final java.lang.String __opened =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opened");
                                
                                    public static final java.lang.String __purchased =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchased");
                                
                                    public static final java.lang.String __queued =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_queued");
                                
                                    public static final java.lang.String __received =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_received");
                                
                                    public static final java.lang.String __responded =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_responded");
                                
                                    public static final java.lang.String __securityIssues =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_securityIssues");
                                
                                    public static final java.lang.String __sent =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sent");
                                
                                    public static final java.lang.String __skippedDueToPreviousHardBounce =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_skippedDueToPreviousHardBounce");
                                
                                    public static final java.lang.String __subscribed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_subscribed");
                                
                                    public static final java.lang.String __tooManyRecipients =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tooManyRecipients");
                                
                                    public static final java.lang.String __unsubscribed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unsubscribed");
                                
                                    public static final java.lang.String __unsubscribedByFeedbackLoop =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unsubscribedByFeedbackLoop");
                                
                                public static final CampaignResponse _clickedThrough =
                                    new CampaignResponse(__clickedThrough,true);
                            
                                public static final CampaignResponse _failedDeliveryFailure =
                                    new CampaignResponse(__failedDeliveryFailure,true);
                            
                                public static final CampaignResponse _failedInvalidAddress =
                                    new CampaignResponse(__failedInvalidAddress,true);
                            
                                public static final CampaignResponse _failedOther =
                                    new CampaignResponse(__failedOther,true);
                            
                                public static final CampaignResponse _failedSpam =
                                    new CampaignResponse(__failedSpam,true);
                            
                                public static final CampaignResponse _failedTemplateError =
                                    new CampaignResponse(__failedTemplateError,true);
                            
                                public static final CampaignResponse _failedUnexpectedError =
                                    new CampaignResponse(__failedUnexpectedError,true);
                            
                                public static final CampaignResponse _invalidSenderAddress =
                                    new CampaignResponse(__invalidSenderAddress,true);
                            
                                public static final CampaignResponse _mailboxDisabled =
                                    new CampaignResponse(__mailboxDisabled,true);
                            
                                public static final CampaignResponse _mailboxIsFull =
                                    new CampaignResponse(__mailboxIsFull,true);
                            
                                public static final CampaignResponse _mailboxNotAcceptingMessages =
                                    new CampaignResponse(__mailboxNotAcceptingMessages,true);
                            
                                public static final CampaignResponse _mailProtocolIssues =
                                    new CampaignResponse(__mailProtocolIssues,true);
                            
                                public static final CampaignResponse _mediaError =
                                    new CampaignResponse(__mediaError,true);
                            
                                public static final CampaignResponse _messageExceedsSizeLengthLimits =
                                    new CampaignResponse(__messageExceedsSizeLengthLimits,true);
                            
                                public static final CampaignResponse _networkServerIssues =
                                    new CampaignResponse(__networkServerIssues,true);
                            
                                public static final CampaignResponse _opened =
                                    new CampaignResponse(__opened,true);
                            
                                public static final CampaignResponse _purchased =
                                    new CampaignResponse(__purchased,true);
                            
                                public static final CampaignResponse _queued =
                                    new CampaignResponse(__queued,true);
                            
                                public static final CampaignResponse _received =
                                    new CampaignResponse(__received,true);
                            
                                public static final CampaignResponse _responded =
                                    new CampaignResponse(__responded,true);
                            
                                public static final CampaignResponse _securityIssues =
                                    new CampaignResponse(__securityIssues,true);
                            
                                public static final CampaignResponse _sent =
                                    new CampaignResponse(__sent,true);
                            
                                public static final CampaignResponse _skippedDueToPreviousHardBounce =
                                    new CampaignResponse(__skippedDueToPreviousHardBounce,true);
                            
                                public static final CampaignResponse _subscribed =
                                    new CampaignResponse(__subscribed,true);
                            
                                public static final CampaignResponse _tooManyRecipients =
                                    new CampaignResponse(__tooManyRecipients,true);
                            
                                public static final CampaignResponse _unsubscribed =
                                    new CampaignResponse(__unsubscribed,true);
                            
                                public static final CampaignResponse _unsubscribedByFeedbackLoop =
                                    new CampaignResponse(__unsubscribedByFeedbackLoop,true);
                            

                                public java.lang.String getValue() { return localCampaignResponse;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localCampaignResponse.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.marketing_2017_2.lists.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":CampaignResponse",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "CampaignResponse",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localCampaignResponse==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("CampaignResponse cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localCampaignResponse);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.marketing_2017_2.lists.webservices.netsuite.com")){
                return "ns39";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCampaignResponse)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static CampaignResponse fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    CampaignResponse enumeration = (CampaignResponse)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static CampaignResponse fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static CampaignResponse fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return CampaignResponse.Factory.fromString(content,namespaceUri);
                    } else {
                       return CampaignResponse.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CampaignResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CampaignResponse object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"CampaignResponse" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = CampaignResponse.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = CampaignResponse.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    