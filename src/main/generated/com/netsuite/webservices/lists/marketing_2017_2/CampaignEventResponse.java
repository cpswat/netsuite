
/**
 * CampaignEventResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.marketing_2017_2;
            

            /**
            *  CampaignEventResponse bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CampaignEventResponse
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = CampaignEventResponse
                Namespace URI = urn:marketing_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns40
                */
            

                        /**
                        * field for Name
                        */

                        
                                    protected java.lang.String localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(java.lang.String param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for Type
                        */

                        
                                    protected java.lang.String localType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTypeTracker = false ;

                           public boolean isTypeSpecified(){
                               return localTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getType(){
                               return localType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Type
                               */
                               public void setType(java.lang.String param){
                            localTypeTracker = param != null;
                                   
                                            this.localType=param;
                                    

                               }
                            

                        /**
                        * field for DateSent
                        */

                        
                                    protected java.util.Calendar localDateSent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateSentTracker = false ;

                           public boolean isDateSentSpecified(){
                               return localDateSentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getDateSent(){
                               return localDateSent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateSent
                               */
                               public void setDateSent(java.util.Calendar param){
                            localDateSentTracker = param != null;
                                   
                                            this.localDateSent=param;
                                    

                               }
                            

                        /**
                        * field for Sent
                        */

                        
                                    protected double localSent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSentTracker = false ;

                           public boolean isSentSpecified(){
                               return localSentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSent(){
                               return localSent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sent
                               */
                               public void setSent(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSentTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSent=param;
                                    

                               }
                            

                        /**
                        * field for Opened
                        */

                        
                                    protected double localOpened ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpenedTracker = false ;

                           public boolean isOpenedSpecified(){
                               return localOpenedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getOpened(){
                               return localOpened;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Opened
                               */
                               public void setOpened(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localOpenedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localOpened=param;
                                    

                               }
                            

                        /**
                        * field for OpenedRatio
                        */

                        
                                    protected double localOpenedRatio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpenedRatioTracker = false ;

                           public boolean isOpenedRatioSpecified(){
                               return localOpenedRatioTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getOpenedRatio(){
                               return localOpenedRatio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OpenedRatio
                               */
                               public void setOpenedRatio(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localOpenedRatioTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localOpenedRatio=param;
                                    

                               }
                            

                        /**
                        * field for ClickedThru
                        */

                        
                                    protected double localClickedThru ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClickedThruTracker = false ;

                           public boolean isClickedThruSpecified(){
                               return localClickedThruTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getClickedThru(){
                               return localClickedThru;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClickedThru
                               */
                               public void setClickedThru(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localClickedThruTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localClickedThru=param;
                                    

                               }
                            

                        /**
                        * field for ClickedThruRatio
                        */

                        
                                    protected double localClickedThruRatio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClickedThruRatioTracker = false ;

                           public boolean isClickedThruRatioSpecified(){
                               return localClickedThruRatioTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getClickedThruRatio(){
                               return localClickedThruRatio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClickedThruRatio
                               */
                               public void setClickedThruRatio(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localClickedThruRatioTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localClickedThruRatio=param;
                                    

                               }
                            

                        /**
                        * field for Responded
                        */

                        
                                    protected long localResponded ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRespondedTracker = false ;

                           public boolean isRespondedSpecified(){
                               return localRespondedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getResponded(){
                               return localResponded;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Responded
                               */
                               public void setResponded(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localRespondedTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localResponded=param;
                                    

                               }
                            

                        /**
                        * field for RespondedRatio
                        */

                        
                                    protected double localRespondedRatio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRespondedRatioTracker = false ;

                           public boolean isRespondedRatioSpecified(){
                               return localRespondedRatioTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRespondedRatio(){
                               return localRespondedRatio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RespondedRatio
                               */
                               public void setRespondedRatio(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRespondedRatioTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRespondedRatio=param;
                                    

                               }
                            

                        /**
                        * field for Unsubscribed
                        */

                        
                                    protected long localUnsubscribed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnsubscribedTracker = false ;

                           public boolean isUnsubscribedSpecified(){
                               return localUnsubscribedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getUnsubscribed(){
                               return localUnsubscribed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Unsubscribed
                               */
                               public void setUnsubscribed(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localUnsubscribedTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localUnsubscribed=param;
                                    

                               }
                            

                        /**
                        * field for UnsubscribedRatio
                        */

                        
                                    protected double localUnsubscribedRatio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnsubscribedRatioTracker = false ;

                           public boolean isUnsubscribedRatioSpecified(){
                               return localUnsubscribedRatioTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getUnsubscribedRatio(){
                               return localUnsubscribedRatio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UnsubscribedRatio
                               */
                               public void setUnsubscribedRatio(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localUnsubscribedRatioTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localUnsubscribedRatio=param;
                                    

                               }
                            

                        /**
                        * field for Bounced
                        */

                        
                                    protected long localBounced ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBouncedTracker = false ;

                           public boolean isBouncedSpecified(){
                               return localBouncedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBounced(){
                               return localBounced;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bounced
                               */
                               public void setBounced(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localBouncedTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localBounced=param;
                                    

                               }
                            

                        /**
                        * field for BouncedRatio
                        */

                        
                                    protected double localBouncedRatio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBouncedRatioTracker = false ;

                           public boolean isBouncedRatioSpecified(){
                               return localBouncedRatioTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getBouncedRatio(){
                               return localBouncedRatio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BouncedRatio
                               */
                               public void setBouncedRatio(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localBouncedRatioTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localBouncedRatio=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:marketing_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":CampaignEventResponse",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "CampaignEventResponse",
                           xmlWriter);
                   }

               
                   }
                if (localNameTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "name", xmlWriter);
                             

                                          if (localName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTypeTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "type", xmlWriter);
                             

                                          if (localType==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localType);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDateSentTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dateSent", xmlWriter);
                             

                                          if (localDateSent==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("dateSent cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateSent));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSentTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sent", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSent)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sent cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSent));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOpenedTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "opened", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localOpened)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("opened cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOpened));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOpenedRatioTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "openedRatio", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localOpenedRatio)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("openedRatio cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOpenedRatio));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localClickedThruTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "clickedThru", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localClickedThru)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("clickedThru cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClickedThru));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localClickedThruRatioTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "clickedThruRatio", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localClickedThruRatio)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("clickedThruRatio cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClickedThruRatio));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRespondedTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "responded", xmlWriter);
                             
                                               if (localResponded==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("responded cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResponded));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRespondedRatioTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "respondedRatio", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRespondedRatio)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("respondedRatio cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRespondedRatio));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnsubscribedTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "unsubscribed", xmlWriter);
                             
                                               if (localUnsubscribed==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("unsubscribed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnsubscribed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnsubscribedRatioTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "unsubscribedRatio", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localUnsubscribedRatio)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("unsubscribedRatio cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnsubscribedRatio));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBouncedTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "bounced", xmlWriter);
                             
                                               if (localBounced==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("bounced cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBounced));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBouncedRatioTracker){
                                    namespace = "urn:marketing_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "bouncedRatio", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localBouncedRatio)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("bouncedRatio cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBouncedRatio));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:marketing_2017_2.lists.webservices.netsuite.com")){
                return "ns40";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "name"));
                                 
                                        if (localName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        }
                                    } if (localTypeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "type"));
                                 
                                        if (localType != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localType));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                        }
                                    } if (localDateSentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "dateSent"));
                                 
                                        if (localDateSent != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateSent));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("dateSent cannot be null!!");
                                        }
                                    } if (localSentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "sent"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSent));
                            } if (localOpenedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "opened"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOpened));
                            } if (localOpenedRatioTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "openedRatio"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOpenedRatio));
                            } if (localClickedThruTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "clickedThru"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClickedThru));
                            } if (localClickedThruRatioTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "clickedThruRatio"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localClickedThruRatio));
                            } if (localRespondedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "responded"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResponded));
                            } if (localRespondedRatioTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "respondedRatio"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRespondedRatio));
                            } if (localUnsubscribedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "unsubscribed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnsubscribed));
                            } if (localUnsubscribedRatioTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "unsubscribedRatio"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnsubscribedRatio));
                            } if (localBouncedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "bounced"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBounced));
                            } if (localBouncedRatioTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com",
                                                                      "bouncedRatio"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBouncedRatio));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CampaignEventResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CampaignEventResponse object =
                new CampaignEventResponse();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"CampaignEventResponse".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CampaignEventResponse)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"name" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","type").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"type" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","dateSent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dateSent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDateSent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","sent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSent(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","opened").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"opened" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOpened(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOpened(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","openedRatio").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"openedRatio" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOpenedRatio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOpenedRatio(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","clickedThru").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"clickedThru" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setClickedThru(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setClickedThru(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","clickedThruRatio").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"clickedThruRatio" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setClickedThruRatio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setClickedThruRatio(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","responded").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"responded" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setResponded(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setResponded(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","respondedRatio").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"respondedRatio" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRespondedRatio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRespondedRatio(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","unsubscribed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"unsubscribed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUnsubscribed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setUnsubscribed(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","unsubscribedRatio").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"unsubscribedRatio" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUnsubscribedRatio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setUnsubscribedRatio(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","bounced").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"bounced" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBounced(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBounced(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:marketing_2017_2.lists.webservices.netsuite.com","bouncedRatio").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"bouncedRatio" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBouncedRatio(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBouncedRatio(java.lang.Double.NaN);
                                           
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    