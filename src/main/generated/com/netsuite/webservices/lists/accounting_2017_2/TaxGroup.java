
/**
 * TaxGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  TaxGroup bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TaxGroup extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TaxGroup
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for ItemId
                        */

                        
                                    protected java.lang.String localItemId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemIdTracker = false ;

                           public boolean isItemIdSpecified(){
                               return localItemIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getItemId(){
                               return localItemId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemId
                               */
                               public void setItemId(java.lang.String param){
                            localItemIdTracker = param != null;
                                   
                                            this.localItemId=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for State
                        */

                        
                                    protected java.lang.String localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getState(){
                               return localState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param State
                               */
                               public void setState(java.lang.String param){
                            localStateTracker = param != null;
                                   
                                            this.localState=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localSubsidiaryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryListTracker = false ;

                           public boolean isSubsidiaryListSpecified(){
                               return localSubsidiaryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getSubsidiaryList(){
                               return localSubsidiaryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryList
                               */
                               public void setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localSubsidiaryListTracker = param != null;
                                   
                                            this.localSubsidiaryList=param;
                                    

                               }
                            

                        /**
                        * field for Taxitem1
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxitem1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxitem1Tracker = false ;

                           public boolean isTaxitem1Specified(){
                               return localTaxitem1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxitem1(){
                               return localTaxitem1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Taxitem1
                               */
                               public void setTaxitem1(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxitem1Tracker = param != null;
                                   
                                            this.localTaxitem1=param;
                                    

                               }
                            

                        /**
                        * field for Unitprice1
                        */

                        
                                    protected java.lang.String localUnitprice1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitprice1Tracker = false ;

                           public boolean isUnitprice1Specified(){
                               return localUnitprice1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUnitprice1(){
                               return localUnitprice1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Unitprice1
                               */
                               public void setUnitprice1(java.lang.String param){
                            localUnitprice1Tracker = param != null;
                                   
                                            this.localUnitprice1=param;
                                    

                               }
                            

                        /**
                        * field for Taxitem2
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxitem2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxitem2Tracker = false ;

                           public boolean isTaxitem2Specified(){
                               return localTaxitem2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxitem2(){
                               return localTaxitem2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Taxitem2
                               */
                               public void setTaxitem2(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxitem2Tracker = param != null;
                                   
                                            this.localTaxitem2=param;
                                    

                               }
                            

                        /**
                        * field for Unitprice2
                        */

                        
                                    protected java.lang.String localUnitprice2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitprice2Tracker = false ;

                           public boolean isUnitprice2Specified(){
                               return localUnitprice2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUnitprice2(){
                               return localUnitprice2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Unitprice2
                               */
                               public void setUnitprice2(java.lang.String param){
                            localUnitprice2Tracker = param != null;
                                   
                                            this.localUnitprice2=param;
                                    

                               }
                            

                        /**
                        * field for Piggyback
                        */

                        
                                    protected boolean localPiggyback ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPiggybackTracker = false ;

                           public boolean isPiggybackSpecified(){
                               return localPiggybackTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPiggyback(){
                               return localPiggyback;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Piggyback
                               */
                               public void setPiggyback(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPiggybackTracker =
                                       true;
                                   
                                            this.localPiggyback=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected double localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for TaxType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxTypeTracker = false ;

                           public boolean isTaxTypeSpecified(){
                               return localTaxTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxType(){
                               return localTaxType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxType
                               */
                               public void setTaxType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxTypeTracker = param != null;
                                   
                                            this.localTaxType=param;
                                    

                               }
                            

                        /**
                        * field for IncludeChildren
                        */

                        
                                    protected boolean localIncludeChildren ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncludeChildrenTracker = false ;

                           public boolean isIncludeChildrenSpecified(){
                               return localIncludeChildrenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIncludeChildren(){
                               return localIncludeChildren;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncludeChildren
                               */
                               public void setIncludeChildren(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIncludeChildrenTracker =
                                       true;
                                   
                                            this.localIncludeChildren=param;
                                    

                               }
                            

                        /**
                        * field for County
                        */

                        
                                    protected java.lang.String localCounty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountyTracker = false ;

                           public boolean isCountySpecified(){
                               return localCountyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCounty(){
                               return localCounty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param County
                               */
                               public void setCounty(java.lang.String param){
                            localCountyTracker = param != null;
                                   
                                            this.localCounty=param;
                                    

                               }
                            

                        /**
                        * field for City
                        */

                        
                                    protected java.lang.String localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCity(){
                               return localCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param City
                               */
                               public void setCity(java.lang.String param){
                            localCityTracker = param != null;
                                   
                                            this.localCity=param;
                                    

                               }
                            

                        /**
                        * field for Zip
                        */

                        
                                    protected java.lang.String localZip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipTracker = false ;

                           public boolean isZipSpecified(){
                               return localZipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getZip(){
                               return localZip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zip
                               */
                               public void setZip(java.lang.String param){
                            localZipTracker = param != null;
                                   
                                            this.localZip=param;
                                    

                               }
                            

                        /**
                        * field for NexusCountry
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localNexusCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNexusCountryTracker = false ;

                           public boolean isNexusCountrySpecified(){
                               return localNexusCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getNexusCountry(){
                               return localNexusCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NexusCountry
                               */
                               public void setNexusCountry(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localNexusCountryTracker = param != null;
                                   
                                            this.localNexusCountry=param;
                                    

                               }
                            

                        /**
                        * field for IsDefault
                        */

                        
                                    protected boolean localIsDefault ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultTracker = false ;

                           public boolean isIsDefaultSpecified(){
                               return localIsDefaultTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsDefault(){
                               return localIsDefault;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefault
                               */
                               public void setIsDefault(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsDefaultTracker =
                                       true;
                                   
                                            this.localIsDefault=param;
                                    

                               }
                            

                        /**
                        * field for TaxItemList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItemList localTaxItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxItemListTracker = false ;

                           public boolean isTaxItemListSpecified(){
                               return localTaxItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItemList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItemList getTaxItemList(){
                               return localTaxItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxItemList
                               */
                               public void setTaxItemList(com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItemList param){
                            localTaxItemListTracker = param != null;
                                   
                                            this.localTaxItemList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TaxGroup",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TaxGroup",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localItemIdTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "itemId", xmlWriter);
                             

                                          if (localItemId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localItemId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "state", xmlWriter);
                             

                                          if (localState==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localState);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubsidiaryListTracker){
                                            if (localSubsidiaryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                            }
                                           localSubsidiaryList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList"),
                                               xmlWriter);
                                        } if (localTaxitem1Tracker){
                                            if (localTaxitem1==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxitem1 cannot be null!!");
                                            }
                                           localTaxitem1.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxitem1"),
                                               xmlWriter);
                                        } if (localUnitprice1Tracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "unitprice1", xmlWriter);
                             

                                          if (localUnitprice1==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("unitprice1 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUnitprice1);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxitem2Tracker){
                                            if (localTaxitem2==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxitem2 cannot be null!!");
                                            }
                                           localTaxitem2.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxitem2"),
                                               xmlWriter);
                                        } if (localUnitprice2Tracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "unitprice2", xmlWriter);
                             

                                          if (localUnitprice2==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("unitprice2 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUnitprice2);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPiggybackTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "piggyback", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("piggyback cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPiggyback));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsInactiveTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxTypeTracker){
                                            if (localTaxType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxType cannot be null!!");
                                            }
                                           localTaxType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxType"),
                                               xmlWriter);
                                        } if (localIncludeChildrenTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "includeChildren", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("includeChildren cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCountyTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "county", xmlWriter);
                             

                                          if (localCounty==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCounty);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "city", xmlWriter);
                             

                                          if (localCity==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCity);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZipTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "zip", xmlWriter);
                             

                                          if (localZip==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("zip cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localZip);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNexusCountryTracker){
                                            if (localNexusCountry==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nexusCountry cannot be null!!");
                                            }
                                           localNexusCountry.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","nexusCountry"),
                                               xmlWriter);
                                        } if (localIsDefaultTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isDefault", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isDefault cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDefault));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxItemListTracker){
                                            if (localTaxItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxItemList cannot be null!!");
                                            }
                                           localTaxItemList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxItemList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","TaxGroup"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localItemIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemId"));
                                 
                                        if (localItemId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                        }
                                    } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localStateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "state"));
                                 
                                        if (localState != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localState));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                        }
                                    } if (localSubsidiaryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiaryList"));
                            
                            
                                    if (localSubsidiaryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryList);
                                } if (localTaxitem1Tracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "taxitem1"));
                            
                            
                                    if (localTaxitem1==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxitem1 cannot be null!!");
                                    }
                                    elementList.add(localTaxitem1);
                                } if (localUnitprice1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "unitprice1"));
                                 
                                        if (localUnitprice1 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnitprice1));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("unitprice1 cannot be null!!");
                                        }
                                    } if (localTaxitem2Tracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "taxitem2"));
                            
                            
                                    if (localTaxitem2==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxitem2 cannot be null!!");
                                    }
                                    elementList.add(localTaxitem2);
                                } if (localUnitprice2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "unitprice2"));
                                 
                                        if (localUnitprice2 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnitprice2));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("unitprice2 cannot be null!!");
                                        }
                                    } if (localPiggybackTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "piggyback"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPiggyback));
                            } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                            } if (localTaxTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "taxType"));
                            
                            
                                    if (localTaxType==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxType cannot be null!!");
                                    }
                                    elementList.add(localTaxType);
                                } if (localIncludeChildrenTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "includeChildren"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                            } if (localCountyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "county"));
                                 
                                        if (localCounty != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCounty));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                        }
                                    } if (localCityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "city"));
                                 
                                        if (localCity != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCity));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                        }
                                    } if (localZipTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "zip"));
                                 
                                        if (localZip != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZip));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("zip cannot be null!!");
                                        }
                                    } if (localNexusCountryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "nexusCountry"));
                            
                            
                                    if (localNexusCountry==null){
                                         throw new org.apache.axis2.databinding.ADBException("nexusCountry cannot be null!!");
                                    }
                                    elementList.add(localNexusCountry);
                                } if (localIsDefaultTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isDefault"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDefault));
                            } if (localTaxItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "taxItemList"));
                            
                            
                                    if (localTaxItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxItemList cannot be null!!");
                                    }
                                    elementList.add(localTaxItemList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TaxGroup parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TaxGroup object =
                new TaxGroup();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TaxGroup".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TaxGroup)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"itemId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setItemId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"state" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setState(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList").equals(reader.getName())){
                                
                                                object.setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxitem1").equals(reader.getName())){
                                
                                                object.setTaxitem1(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","unitprice1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"unitprice1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUnitprice1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxitem2").equals(reader.getName())){
                                
                                                object.setTaxitem2(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","unitprice2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"unitprice2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUnitprice2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","piggyback").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"piggyback" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPiggyback(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxType").equals(reader.getName())){
                                
                                                object.setTaxType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","includeChildren").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"includeChildren" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncludeChildren(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","county").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"county" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCounty(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"city" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","zip").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"zip" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZip(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","nexusCountry").equals(reader.getName())){
                                
                                                object.setNexusCountry(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isDefault").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isDefault" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsDefault(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxItemList").equals(reader.getName())){
                                
                                                object.setTaxItemList(com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    