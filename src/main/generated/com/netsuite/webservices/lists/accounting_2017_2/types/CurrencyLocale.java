
/**
 * CurrencyLocale.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2.types;
            

            /**
            *  CurrencyLocale bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CurrencyLocale
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.accounting_2017_2.lists.webservices.netsuite.com",
                "CurrencyLocale",
                "ns18");

            

                        /**
                        * field for CurrencyLocale
                        */

                        
                                    protected java.lang.String localCurrencyLocale ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected CurrencyLocale(java.lang.String value, boolean isRegisterValue) {
                                    localCurrencyLocale = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localCurrencyLocale, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __afghanistanPashto =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_afghanistanPashto");
                                
                                    public static final java.lang.String __afghanistanPersian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_afghanistanPersian");
                                
                                    public static final java.lang.String __alandIslandsSwedish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_alandIslandsSwedish");
                                
                                    public static final java.lang.String __albaniaAlbanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_albaniaAlbanian");
                                
                                    public static final java.lang.String __algeriaArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_algeriaArabic");
                                
                                    public static final java.lang.String __angolaPortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_angolaPortuguese");
                                
                                    public static final java.lang.String __anguillaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_anguillaEnglish");
                                
                                    public static final java.lang.String __antiguaAndBarbudaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_antiguaAndBarbudaEnglish");
                                
                                    public static final java.lang.String __argentinaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_argentinaSpanish");
                                
                                    public static final java.lang.String __armeniaArmenian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_armeniaArmenian");
                                
                                    public static final java.lang.String __arubaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_arubaEnglish");
                                
                                    public static final java.lang.String __arubaPortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_arubaPortuguese");
                                
                                    public static final java.lang.String __australiaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaEnglish");
                                
                                    public static final java.lang.String __austriaGerman =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_austriaGerman");
                                
                                    public static final java.lang.String __azerbaijanAzerbaijani =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_azerbaijanAzerbaijani");
                                
                                    public static final java.lang.String __bahamasEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bahamasEnglish");
                                
                                    public static final java.lang.String __bahrainArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bahrainArabic");
                                
                                    public static final java.lang.String __barbadosEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_barbadosEnglish");
                                
                                    public static final java.lang.String __belarusByelorussian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belarusByelorussian");
                                
                                    public static final java.lang.String __belgiumDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belgiumDutch");
                                
                                    public static final java.lang.String __belgiumFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belgiumFrench");
                                
                                    public static final java.lang.String __belizeEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belizeEnglish");
                                
                                    public static final java.lang.String __bengali =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bengali");
                                
                                    public static final java.lang.String __beninFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_beninFrench");
                                
                                    public static final java.lang.String __bermudaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bermudaEnglish");
                                
                                    public static final java.lang.String __bhutanDzongkha =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bhutanDzongkha");
                                
                                    public static final java.lang.String __boliviaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_boliviaSpanish");
                                
                                    public static final java.lang.String __bonaireSaintEustatiusAndSabaDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bonaireSaintEustatiusAndSabaDutch");
                                
                                    public static final java.lang.String __bosniaAndHerzegovinaBosnian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bosniaAndHerzegovinaBosnian");
                                
                                    public static final java.lang.String __botswanaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_botswanaEnglish");
                                
                                    public static final java.lang.String __brazilPortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_brazilPortuguese");
                                
                                    public static final java.lang.String __bruneiMalay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bruneiMalay");
                                
                                    public static final java.lang.String __bulgariaBulgarian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bulgariaBulgarian");
                                
                                    public static final java.lang.String __burkinaFasoFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_burkinaFasoFrench");
                                
                                    public static final java.lang.String __burundiFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_burundiFrench");
                                
                                    public static final java.lang.String __cambodiaKhmer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cambodiaKhmer");
                                
                                    public static final java.lang.String __cameroonFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cameroonFrench");
                                
                                    public static final java.lang.String __canadaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_canadaEnglish");
                                
                                    public static final java.lang.String __canadaFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_canadaFrench");
                                
                                    public static final java.lang.String __canaryIslandsSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_canaryIslandsSpanish");
                                
                                    public static final java.lang.String __capeVerdePortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_capeVerdePortuguese");
                                
                                    public static final java.lang.String __caymanIslandsEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_caymanIslandsEnglish");
                                
                                    public static final java.lang.String __centralAfricanRepublicFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_centralAfricanRepublicFrench");
                                
                                    public static final java.lang.String __ceutaAndMelillaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ceutaAndMelillaSpanish");
                                
                                    public static final java.lang.String __chadFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chadFrench");
                                
                                    public static final java.lang.String __chileSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chileSpanish");
                                
                                    public static final java.lang.String __chinaChinese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chinaChinese");
                                
                                    public static final java.lang.String __colombiaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_colombiaSpanish");
                                
                                    public static final java.lang.String __comorosFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_comorosFrench");
                                
                                    public static final java.lang.String __congoDemocraticRepublicEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_congoDemocraticRepublicEnglish");
                                
                                    public static final java.lang.String __congoDemocraticRepublicFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_congoDemocraticRepublicFrench");
                                
                                    public static final java.lang.String __congoRepublicOfFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_congoRepublicOfFrench");
                                
                                    public static final java.lang.String __costaRicaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_costaRicaSpanish");
                                
                                    public static final java.lang.String __coteDivoireFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_coteDivoireFrench");
                                
                                    public static final java.lang.String __croatiaCroatian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_croatiaCroatian");
                                
                                    public static final java.lang.String __cubaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cubaSpanish");
                                
                                    public static final java.lang.String __curacaoDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_curacaoDutch");
                                
                                    public static final java.lang.String __cyprusEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cyprusEnglish");
                                
                                    public static final java.lang.String __cyprusEnglishEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cyprusEnglishEuro");
                                
                                    public static final java.lang.String __czechRepublicCzech =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_czechRepublicCzech");
                                
                                    public static final java.lang.String __denmarkDanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_denmarkDanish");
                                
                                    public static final java.lang.String __djiboutiArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_djiboutiArabic");
                                
                                    public static final java.lang.String __djiboutiFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_djiboutiFrench");
                                
                                    public static final java.lang.String __dominicaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dominicaEnglish");
                                
                                    public static final java.lang.String __dominicanRepublicSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dominicanRepublicSpanish");
                                
                                    public static final java.lang.String __ecuadorSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ecuadorSpanish");
                                
                                    public static final java.lang.String __egyptArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_egyptArabic");
                                
                                    public static final java.lang.String __elSalvadorSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_elSalvadorSpanish");
                                
                                    public static final java.lang.String __equatorialGuineaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_equatorialGuineaSpanish");
                                
                                    public static final java.lang.String __eritreaAfar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_eritreaAfar");
                                
                                    public static final java.lang.String __estoniaEstonian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estoniaEstonian");
                                
                                    public static final java.lang.String __ethiopiaAmharic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ethiopiaAmharic");
                                
                                    public static final java.lang.String __falklandIslandsEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_falklandIslandsEnglish");
                                
                                    public static final java.lang.String __fijiFijian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fijiFijian");
                                
                                    public static final java.lang.String __finlandFinnish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_finlandFinnish");
                                
                                    public static final java.lang.String __finlandFinnishEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_finlandFinnishEuro");
                                
                                    public static final java.lang.String __franceFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_franceFrench");
                                
                                    public static final java.lang.String __franceFrenchEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_franceFrenchEuro");
                                
                                    public static final java.lang.String __frenchPolynesiaFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_frenchPolynesiaFrench");
                                
                                    public static final java.lang.String __gabonFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gabonFrench");
                                
                                    public static final java.lang.String __gambiaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gambiaEnglish");
                                
                                    public static final java.lang.String __georgiaGeorgian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_georgiaGeorgian");
                                
                                    public static final java.lang.String __germanyGerman =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_germanyGerman");
                                
                                    public static final java.lang.String __germanyGermanEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_germanyGermanEuro");
                                
                                    public static final java.lang.String __ghanaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ghanaEnglish");
                                
                                    public static final java.lang.String __gibraltarEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gibraltarEnglish");
                                
                                    public static final java.lang.String __goldOunce =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_goldOunce");
                                
                                    public static final java.lang.String __greeceGreek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_greeceGreek");
                                
                                    public static final java.lang.String __grenadaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_grenadaEnglish");
                                
                                    public static final java.lang.String __guatemalaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guatemalaSpanish");
                                
                                    public static final java.lang.String __guineaBissauPortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guineaBissauPortuguese");
                                
                                    public static final java.lang.String __guineaFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guineaFrench");
                                
                                    public static final java.lang.String __guyanaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guyanaEnglish");
                                
                                    public static final java.lang.String __haitian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_haitian");
                                
                                    public static final java.lang.String __hondurasSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hondurasSpanish");
                                
                                    public static final java.lang.String __hongKongChinese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hongKongChinese");
                                
                                    public static final java.lang.String __hungaryHungarian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hungaryHungarian");
                                
                                    public static final java.lang.String __icelandIcelandic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_icelandIcelandic");
                                
                                    public static final java.lang.String __indiaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaEnglish");
                                
                                    public static final java.lang.String __indiaGujarati =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaGujarati");
                                
                                    public static final java.lang.String __indiaHindi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaHindi");
                                
                                    public static final java.lang.String __indiaKannada =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaKannada");
                                
                                    public static final java.lang.String __indiaMarathi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaMarathi");
                                
                                    public static final java.lang.String __indiaPanjabi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaPanjabi");
                                
                                    public static final java.lang.String __indiaTamil =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaTamil");
                                
                                    public static final java.lang.String __indiaTelugu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indiaTelugu");
                                
                                    public static final java.lang.String __indonesiaIndonesian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indonesiaIndonesian");
                                
                                    public static final java.lang.String __iranPersian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_iranPersian");
                                
                                    public static final java.lang.String __iraqArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_iraqArabic");
                                
                                    public static final java.lang.String __irelandEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_irelandEnglish");
                                
                                    public static final java.lang.String __israelHebrew =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_israelHebrew");
                                
                                    public static final java.lang.String __italyItalian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_italyItalian");
                                
                                    public static final java.lang.String __italyItalianEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_italyItalianEuro");
                                
                                    public static final java.lang.String __jamaicaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jamaicaEnglish");
                                
                                    public static final java.lang.String __japanJapanese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_japanJapanese");
                                
                                    public static final java.lang.String __jordanArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jordanArabic");
                                
                                    public static final java.lang.String __jordanEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jordanEnglish");
                                
                                    public static final java.lang.String __kazakhstanRussian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kazakhstanRussian");
                                
                                    public static final java.lang.String __kenyaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kenyaEnglish");
                                
                                    public static final java.lang.String __kuwaitArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kuwaitArabic");
                                
                                    public static final java.lang.String __kuwaitEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kuwaitEnglish");
                                
                                    public static final java.lang.String __kyrgyzstanRussian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kyrgyzstanRussian");
                                
                                    public static final java.lang.String __laosLao =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_laosLao");
                                
                                    public static final java.lang.String __latviaLatvianLettish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_latviaLatvianLettish");
                                
                                    public static final java.lang.String __lebanonArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lebanonArabic");
                                
                                    public static final java.lang.String __lesothoEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lesothoEnglish");
                                
                                    public static final java.lang.String __liberiaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_liberiaEnglish");
                                
                                    public static final java.lang.String __libyaArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_libyaArabic");
                                
                                    public static final java.lang.String __lithuaniaLithuanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lithuaniaLithuanian");
                                
                                    public static final java.lang.String __luxembourgFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_luxembourgFrench");
                                
                                    public static final java.lang.String __luxembourgGerman =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_luxembourgGerman");
                                
                                    public static final java.lang.String __luxembourgLuxembourgish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_luxembourgLuxembourgish");
                                
                                    public static final java.lang.String __macauChinese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_macauChinese");
                                
                                    public static final java.lang.String __macedoniaMacedonian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_macedoniaMacedonian");
                                
                                    public static final java.lang.String __malawiEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_malawiEnglish");
                                
                                    public static final java.lang.String __malaysiaMalay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_malaysiaMalay");
                                
                                    public static final java.lang.String __maldivesDhivehi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_maldivesDhivehi");
                                
                                    public static final java.lang.String __maliFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_maliFrench");
                                
                                    public static final java.lang.String __mauritiusEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mauritiusEnglish");
                                
                                    public static final java.lang.String __mexicoSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mexicoSpanish");
                                
                                    public static final java.lang.String __moldovaRomanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_moldovaRomanian");
                                
                                    public static final java.lang.String __moldovaRussian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_moldovaRussian");
                                
                                    public static final java.lang.String __mongoliaMongolian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mongoliaMongolian");
                                
                                    public static final java.lang.String __moroccoArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_moroccoArabic");
                                
                                    public static final java.lang.String __mozambiquePortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mozambiquePortuguese");
                                
                                    public static final java.lang.String __myanmarBurmese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_myanmarBurmese");
                                
                                    public static final java.lang.String __namibiaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_namibiaEnglish");
                                
                                    public static final java.lang.String __nepalNepali =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nepalNepali");
                                
                                    public static final java.lang.String __netherlandsAntillesDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_netherlandsAntillesDutch");
                                
                                    public static final java.lang.String __netherlandsDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_netherlandsDutch");
                                
                                    public static final java.lang.String __netherlandsDutchEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_netherlandsDutchEuro");
                                
                                    public static final java.lang.String __newCaledoniaFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_newCaledoniaFrench");
                                
                                    public static final java.lang.String __newZealandEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_newZealandEnglish");
                                
                                    public static final java.lang.String __nicaraguaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nicaraguaSpanish");
                                
                                    public static final java.lang.String __nigerFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nigerFrench");
                                
                                    public static final java.lang.String __nigeriaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nigeriaEnglish");
                                
                                    public static final java.lang.String __northKoreaKorean =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_northKoreaKorean");
                                
                                    public static final java.lang.String __norwayNorwegian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_norwayNorwegian");
                                
                                    public static final java.lang.String __omanArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_omanArabic");
                                
                                    public static final java.lang.String __pakistanUrdu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pakistanUrdu");
                                
                                    public static final java.lang.String __palladiumOunce =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_palladiumOunce");
                                
                                    public static final java.lang.String __panamaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_panamaSpanish");
                                
                                    public static final java.lang.String __papuaNewGuineaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_papuaNewGuineaEnglish");
                                
                                    public static final java.lang.String __paraguaySpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paraguaySpanish");
                                
                                    public static final java.lang.String __peruSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_peruSpanish");
                                
                                    public static final java.lang.String __philippinesEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_philippinesEnglish");
                                
                                    public static final java.lang.String __philippinesTagalog =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_philippinesTagalog");
                                
                                    public static final java.lang.String __platinumOunce =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_platinumOunce");
                                
                                    public static final java.lang.String __polandPolish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_polandPolish");
                                
                                    public static final java.lang.String __portugalPortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_portugalPortuguese");
                                
                                    public static final java.lang.String __portugalPortugueseEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_portugalPortugueseEuro");
                                
                                    public static final java.lang.String __puertoRicoSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_puertoRicoSpanish");
                                
                                    public static final java.lang.String __qatarArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_qatarArabic");
                                
                                    public static final java.lang.String __qatarEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_qatarEnglish");
                                
                                    public static final java.lang.String __romaniaRomanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_romaniaRomanian");
                                
                                    public static final java.lang.String __russiaRussian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_russiaRussian");
                                
                                    public static final java.lang.String __rwandaFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rwandaFrench");
                                
                                    public static final java.lang.String __saintBarthelemyFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintBarthelemyFrench");
                                
                                    public static final java.lang.String __saintHelenaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintHelenaEnglish");
                                
                                    public static final java.lang.String __saintKittsAndNevisEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintKittsAndNevisEnglish");
                                
                                    public static final java.lang.String __saintLuciaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintLuciaEnglish");
                                
                                    public static final java.lang.String __saintMartinEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintMartinEnglish");
                                
                                    public static final java.lang.String __saintVincentAndTheGrenadinesEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintVincentAndTheGrenadinesEnglish");
                                
                                    public static final java.lang.String __samoaSamoan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_samoaSamoan");
                                
                                    public static final java.lang.String __saoTomeAndPrincipePortuguese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saoTomeAndPrincipePortuguese");
                                
                                    public static final java.lang.String __saudiArabiaArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saudiArabiaArabic");
                                
                                    public static final java.lang.String __senegalFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_senegalFrench");
                                
                                    public static final java.lang.String __serbiaAndMontenegroSerbian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_serbiaAndMontenegroSerbian");
                                
                                    public static final java.lang.String __serbiaSerbian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_serbiaSerbian");
                                
                                    public static final java.lang.String __serbiaSerboCroatian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_serbiaSerboCroatian");
                                
                                    public static final java.lang.String __seychellesEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_seychellesEnglish");
                                
                                    public static final java.lang.String __seychellesFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_seychellesFrench");
                                
                                    public static final java.lang.String __sierraLeoneEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sierraLeoneEnglish");
                                
                                    public static final java.lang.String __silverOunce =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_silverOunce");
                                
                                    public static final java.lang.String __singaporeEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_singaporeEnglish");
                                
                                    public static final java.lang.String __sintMaartenDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sintMaartenDutch");
                                
                                    public static final java.lang.String __slovakiaSlovak =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_slovakiaSlovak");
                                
                                    public static final java.lang.String __slovakiaSlovakEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_slovakiaSlovakEuro");
                                
                                    public static final java.lang.String __sloveniaSlovenian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sloveniaSlovenian");
                                
                                    public static final java.lang.String __sloveniaSlovenianEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sloveniaSlovenianEuro");
                                
                                    public static final java.lang.String __solomonIslandsEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_solomonIslandsEnglish");
                                
                                    public static final java.lang.String __somaliaSomali =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_somaliaSomali");
                                
                                    public static final java.lang.String __southAfricaAfrikaans =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southAfricaAfrikaans");
                                
                                    public static final java.lang.String __southAfricaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southAfricaEnglish");
                                
                                    public static final java.lang.String __southKoreaKorean =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southKoreaKorean");
                                
                                    public static final java.lang.String __southSudanEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southSudanEnglish");
                                
                                    public static final java.lang.String __spainCatalan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_spainCatalan");
                                
                                    public static final java.lang.String __spainSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_spainSpanish");
                                
                                    public static final java.lang.String __spainSpanishEuro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_spainSpanishEuro");
                                
                                    public static final java.lang.String __sriLankaSinhalese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sriLankaSinhalese");
                                
                                    public static final java.lang.String __sudanArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sudanArabic");
                                
                                    public static final java.lang.String __surinameDutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_surinameDutch");
                                
                                    public static final java.lang.String __swazilandSwati =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_swazilandSwati");
                                
                                    public static final java.lang.String __swedenSwedish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_swedenSwedish");
                                
                                    public static final java.lang.String __switzerlandFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_switzerlandFrench");
                                
                                    public static final java.lang.String __switzerlandGerman =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_switzerlandGerman");
                                
                                    public static final java.lang.String __switzerlandItalian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_switzerlandItalian");
                                
                                    public static final java.lang.String __syriaArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_syriaArabic");
                                
                                    public static final java.lang.String __taiwanChinese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taiwanChinese");
                                
                                    public static final java.lang.String __tajikistanTajik =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tajikistanTajik");
                                
                                    public static final java.lang.String __tanzaniaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tanzaniaEnglish");
                                
                                    public static final java.lang.String __thailandThai =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thailandThai");
                                
                                    public static final java.lang.String __togoFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_togoFrench");
                                
                                    public static final java.lang.String __tongaTonga =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tongaTonga");
                                
                                    public static final java.lang.String __trinidadAndTobagoEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_trinidadAndTobagoEnglish");
                                
                                    public static final java.lang.String __tunisiaArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tunisiaArabic");
                                
                                    public static final java.lang.String __turkeyTurkish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turkeyTurkish");
                                
                                    public static final java.lang.String __turkmenistanTurkmen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turkmenistanTurkmen");
                                
                                    public static final java.lang.String __turksAndCaicosIslandsEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turksAndCaicosIslandsEnglish");
                                
                                    public static final java.lang.String __ugandaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ugandaEnglish");
                                
                                    public static final java.lang.String __ukraineUkrainian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ukraineUkrainian");
                                
                                    public static final java.lang.String __unitedArabEmiratesArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedArabEmiratesArabic");
                                
                                    public static final java.lang.String __unitedArabEmiratesEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedArabEmiratesEnglish");
                                
                                    public static final java.lang.String __unitedKingdomEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedKingdomEnglish");
                                
                                    public static final java.lang.String __unitedStatesEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedStatesEnglish");
                                
                                    public static final java.lang.String __uruguaySpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uruguaySpanish");
                                
                                    public static final java.lang.String __uzbekistanUzbek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uzbekistanUzbek");
                                
                                    public static final java.lang.String __vanuatuEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vanuatuEnglish");
                                
                                    public static final java.lang.String __vanuatuFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vanuatuFrench");
                                
                                    public static final java.lang.String __venezuelaSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_venezuelaSpanish");
                                
                                    public static final java.lang.String __vietnamVietnamese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vietnamVietnamese");
                                
                                    public static final java.lang.String __wallisAndFutunaFrench =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_wallisAndFutunaFrench");
                                
                                    public static final java.lang.String __yemenArabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_yemenArabic");
                                
                                    public static final java.lang.String __zambiaEnglish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zambiaEnglish");
                                
                                public static final CurrencyLocale _afghanistanPashto =
                                    new CurrencyLocale(__afghanistanPashto,true);
                            
                                public static final CurrencyLocale _afghanistanPersian =
                                    new CurrencyLocale(__afghanistanPersian,true);
                            
                                public static final CurrencyLocale _alandIslandsSwedish =
                                    new CurrencyLocale(__alandIslandsSwedish,true);
                            
                                public static final CurrencyLocale _albaniaAlbanian =
                                    new CurrencyLocale(__albaniaAlbanian,true);
                            
                                public static final CurrencyLocale _algeriaArabic =
                                    new CurrencyLocale(__algeriaArabic,true);
                            
                                public static final CurrencyLocale _angolaPortuguese =
                                    new CurrencyLocale(__angolaPortuguese,true);
                            
                                public static final CurrencyLocale _anguillaEnglish =
                                    new CurrencyLocale(__anguillaEnglish,true);
                            
                                public static final CurrencyLocale _antiguaAndBarbudaEnglish =
                                    new CurrencyLocale(__antiguaAndBarbudaEnglish,true);
                            
                                public static final CurrencyLocale _argentinaSpanish =
                                    new CurrencyLocale(__argentinaSpanish,true);
                            
                                public static final CurrencyLocale _armeniaArmenian =
                                    new CurrencyLocale(__armeniaArmenian,true);
                            
                                public static final CurrencyLocale _arubaEnglish =
                                    new CurrencyLocale(__arubaEnglish,true);
                            
                                public static final CurrencyLocale _arubaPortuguese =
                                    new CurrencyLocale(__arubaPortuguese,true);
                            
                                public static final CurrencyLocale _australiaEnglish =
                                    new CurrencyLocale(__australiaEnglish,true);
                            
                                public static final CurrencyLocale _austriaGerman =
                                    new CurrencyLocale(__austriaGerman,true);
                            
                                public static final CurrencyLocale _azerbaijanAzerbaijani =
                                    new CurrencyLocale(__azerbaijanAzerbaijani,true);
                            
                                public static final CurrencyLocale _bahamasEnglish =
                                    new CurrencyLocale(__bahamasEnglish,true);
                            
                                public static final CurrencyLocale _bahrainArabic =
                                    new CurrencyLocale(__bahrainArabic,true);
                            
                                public static final CurrencyLocale _barbadosEnglish =
                                    new CurrencyLocale(__barbadosEnglish,true);
                            
                                public static final CurrencyLocale _belarusByelorussian =
                                    new CurrencyLocale(__belarusByelorussian,true);
                            
                                public static final CurrencyLocale _belgiumDutch =
                                    new CurrencyLocale(__belgiumDutch,true);
                            
                                public static final CurrencyLocale _belgiumFrench =
                                    new CurrencyLocale(__belgiumFrench,true);
                            
                                public static final CurrencyLocale _belizeEnglish =
                                    new CurrencyLocale(__belizeEnglish,true);
                            
                                public static final CurrencyLocale _bengali =
                                    new CurrencyLocale(__bengali,true);
                            
                                public static final CurrencyLocale _beninFrench =
                                    new CurrencyLocale(__beninFrench,true);
                            
                                public static final CurrencyLocale _bermudaEnglish =
                                    new CurrencyLocale(__bermudaEnglish,true);
                            
                                public static final CurrencyLocale _bhutanDzongkha =
                                    new CurrencyLocale(__bhutanDzongkha,true);
                            
                                public static final CurrencyLocale _boliviaSpanish =
                                    new CurrencyLocale(__boliviaSpanish,true);
                            
                                public static final CurrencyLocale _bonaireSaintEustatiusAndSabaDutch =
                                    new CurrencyLocale(__bonaireSaintEustatiusAndSabaDutch,true);
                            
                                public static final CurrencyLocale _bosniaAndHerzegovinaBosnian =
                                    new CurrencyLocale(__bosniaAndHerzegovinaBosnian,true);
                            
                                public static final CurrencyLocale _botswanaEnglish =
                                    new CurrencyLocale(__botswanaEnglish,true);
                            
                                public static final CurrencyLocale _brazilPortuguese =
                                    new CurrencyLocale(__brazilPortuguese,true);
                            
                                public static final CurrencyLocale _bruneiMalay =
                                    new CurrencyLocale(__bruneiMalay,true);
                            
                                public static final CurrencyLocale _bulgariaBulgarian =
                                    new CurrencyLocale(__bulgariaBulgarian,true);
                            
                                public static final CurrencyLocale _burkinaFasoFrench =
                                    new CurrencyLocale(__burkinaFasoFrench,true);
                            
                                public static final CurrencyLocale _burundiFrench =
                                    new CurrencyLocale(__burundiFrench,true);
                            
                                public static final CurrencyLocale _cambodiaKhmer =
                                    new CurrencyLocale(__cambodiaKhmer,true);
                            
                                public static final CurrencyLocale _cameroonFrench =
                                    new CurrencyLocale(__cameroonFrench,true);
                            
                                public static final CurrencyLocale _canadaEnglish =
                                    new CurrencyLocale(__canadaEnglish,true);
                            
                                public static final CurrencyLocale _canadaFrench =
                                    new CurrencyLocale(__canadaFrench,true);
                            
                                public static final CurrencyLocale _canaryIslandsSpanish =
                                    new CurrencyLocale(__canaryIslandsSpanish,true);
                            
                                public static final CurrencyLocale _capeVerdePortuguese =
                                    new CurrencyLocale(__capeVerdePortuguese,true);
                            
                                public static final CurrencyLocale _caymanIslandsEnglish =
                                    new CurrencyLocale(__caymanIslandsEnglish,true);
                            
                                public static final CurrencyLocale _centralAfricanRepublicFrench =
                                    new CurrencyLocale(__centralAfricanRepublicFrench,true);
                            
                                public static final CurrencyLocale _ceutaAndMelillaSpanish =
                                    new CurrencyLocale(__ceutaAndMelillaSpanish,true);
                            
                                public static final CurrencyLocale _chadFrench =
                                    new CurrencyLocale(__chadFrench,true);
                            
                                public static final CurrencyLocale _chileSpanish =
                                    new CurrencyLocale(__chileSpanish,true);
                            
                                public static final CurrencyLocale _chinaChinese =
                                    new CurrencyLocale(__chinaChinese,true);
                            
                                public static final CurrencyLocale _colombiaSpanish =
                                    new CurrencyLocale(__colombiaSpanish,true);
                            
                                public static final CurrencyLocale _comorosFrench =
                                    new CurrencyLocale(__comorosFrench,true);
                            
                                public static final CurrencyLocale _congoDemocraticRepublicEnglish =
                                    new CurrencyLocale(__congoDemocraticRepublicEnglish,true);
                            
                                public static final CurrencyLocale _congoDemocraticRepublicFrench =
                                    new CurrencyLocale(__congoDemocraticRepublicFrench,true);
                            
                                public static final CurrencyLocale _congoRepublicOfFrench =
                                    new CurrencyLocale(__congoRepublicOfFrench,true);
                            
                                public static final CurrencyLocale _costaRicaSpanish =
                                    new CurrencyLocale(__costaRicaSpanish,true);
                            
                                public static final CurrencyLocale _coteDivoireFrench =
                                    new CurrencyLocale(__coteDivoireFrench,true);
                            
                                public static final CurrencyLocale _croatiaCroatian =
                                    new CurrencyLocale(__croatiaCroatian,true);
                            
                                public static final CurrencyLocale _cubaSpanish =
                                    new CurrencyLocale(__cubaSpanish,true);
                            
                                public static final CurrencyLocale _curacaoDutch =
                                    new CurrencyLocale(__curacaoDutch,true);
                            
                                public static final CurrencyLocale _cyprusEnglish =
                                    new CurrencyLocale(__cyprusEnglish,true);
                            
                                public static final CurrencyLocale _cyprusEnglishEuro =
                                    new CurrencyLocale(__cyprusEnglishEuro,true);
                            
                                public static final CurrencyLocale _czechRepublicCzech =
                                    new CurrencyLocale(__czechRepublicCzech,true);
                            
                                public static final CurrencyLocale _denmarkDanish =
                                    new CurrencyLocale(__denmarkDanish,true);
                            
                                public static final CurrencyLocale _djiboutiArabic =
                                    new CurrencyLocale(__djiboutiArabic,true);
                            
                                public static final CurrencyLocale _djiboutiFrench =
                                    new CurrencyLocale(__djiboutiFrench,true);
                            
                                public static final CurrencyLocale _dominicaEnglish =
                                    new CurrencyLocale(__dominicaEnglish,true);
                            
                                public static final CurrencyLocale _dominicanRepublicSpanish =
                                    new CurrencyLocale(__dominicanRepublicSpanish,true);
                            
                                public static final CurrencyLocale _ecuadorSpanish =
                                    new CurrencyLocale(__ecuadorSpanish,true);
                            
                                public static final CurrencyLocale _egyptArabic =
                                    new CurrencyLocale(__egyptArabic,true);
                            
                                public static final CurrencyLocale _elSalvadorSpanish =
                                    new CurrencyLocale(__elSalvadorSpanish,true);
                            
                                public static final CurrencyLocale _equatorialGuineaSpanish =
                                    new CurrencyLocale(__equatorialGuineaSpanish,true);
                            
                                public static final CurrencyLocale _eritreaAfar =
                                    new CurrencyLocale(__eritreaAfar,true);
                            
                                public static final CurrencyLocale _estoniaEstonian =
                                    new CurrencyLocale(__estoniaEstonian,true);
                            
                                public static final CurrencyLocale _ethiopiaAmharic =
                                    new CurrencyLocale(__ethiopiaAmharic,true);
                            
                                public static final CurrencyLocale _falklandIslandsEnglish =
                                    new CurrencyLocale(__falklandIslandsEnglish,true);
                            
                                public static final CurrencyLocale _fijiFijian =
                                    new CurrencyLocale(__fijiFijian,true);
                            
                                public static final CurrencyLocale _finlandFinnish =
                                    new CurrencyLocale(__finlandFinnish,true);
                            
                                public static final CurrencyLocale _finlandFinnishEuro =
                                    new CurrencyLocale(__finlandFinnishEuro,true);
                            
                                public static final CurrencyLocale _franceFrench =
                                    new CurrencyLocale(__franceFrench,true);
                            
                                public static final CurrencyLocale _franceFrenchEuro =
                                    new CurrencyLocale(__franceFrenchEuro,true);
                            
                                public static final CurrencyLocale _frenchPolynesiaFrench =
                                    new CurrencyLocale(__frenchPolynesiaFrench,true);
                            
                                public static final CurrencyLocale _gabonFrench =
                                    new CurrencyLocale(__gabonFrench,true);
                            
                                public static final CurrencyLocale _gambiaEnglish =
                                    new CurrencyLocale(__gambiaEnglish,true);
                            
                                public static final CurrencyLocale _georgiaGeorgian =
                                    new CurrencyLocale(__georgiaGeorgian,true);
                            
                                public static final CurrencyLocale _germanyGerman =
                                    new CurrencyLocale(__germanyGerman,true);
                            
                                public static final CurrencyLocale _germanyGermanEuro =
                                    new CurrencyLocale(__germanyGermanEuro,true);
                            
                                public static final CurrencyLocale _ghanaEnglish =
                                    new CurrencyLocale(__ghanaEnglish,true);
                            
                                public static final CurrencyLocale _gibraltarEnglish =
                                    new CurrencyLocale(__gibraltarEnglish,true);
                            
                                public static final CurrencyLocale _goldOunce =
                                    new CurrencyLocale(__goldOunce,true);
                            
                                public static final CurrencyLocale _greeceGreek =
                                    new CurrencyLocale(__greeceGreek,true);
                            
                                public static final CurrencyLocale _grenadaEnglish =
                                    new CurrencyLocale(__grenadaEnglish,true);
                            
                                public static final CurrencyLocale _guatemalaSpanish =
                                    new CurrencyLocale(__guatemalaSpanish,true);
                            
                                public static final CurrencyLocale _guineaBissauPortuguese =
                                    new CurrencyLocale(__guineaBissauPortuguese,true);
                            
                                public static final CurrencyLocale _guineaFrench =
                                    new CurrencyLocale(__guineaFrench,true);
                            
                                public static final CurrencyLocale _guyanaEnglish =
                                    new CurrencyLocale(__guyanaEnglish,true);
                            
                                public static final CurrencyLocale _haitian =
                                    new CurrencyLocale(__haitian,true);
                            
                                public static final CurrencyLocale _hondurasSpanish =
                                    new CurrencyLocale(__hondurasSpanish,true);
                            
                                public static final CurrencyLocale _hongKongChinese =
                                    new CurrencyLocale(__hongKongChinese,true);
                            
                                public static final CurrencyLocale _hungaryHungarian =
                                    new CurrencyLocale(__hungaryHungarian,true);
                            
                                public static final CurrencyLocale _icelandIcelandic =
                                    new CurrencyLocale(__icelandIcelandic,true);
                            
                                public static final CurrencyLocale _indiaEnglish =
                                    new CurrencyLocale(__indiaEnglish,true);
                            
                                public static final CurrencyLocale _indiaGujarati =
                                    new CurrencyLocale(__indiaGujarati,true);
                            
                                public static final CurrencyLocale _indiaHindi =
                                    new CurrencyLocale(__indiaHindi,true);
                            
                                public static final CurrencyLocale _indiaKannada =
                                    new CurrencyLocale(__indiaKannada,true);
                            
                                public static final CurrencyLocale _indiaMarathi =
                                    new CurrencyLocale(__indiaMarathi,true);
                            
                                public static final CurrencyLocale _indiaPanjabi =
                                    new CurrencyLocale(__indiaPanjabi,true);
                            
                                public static final CurrencyLocale _indiaTamil =
                                    new CurrencyLocale(__indiaTamil,true);
                            
                                public static final CurrencyLocale _indiaTelugu =
                                    new CurrencyLocale(__indiaTelugu,true);
                            
                                public static final CurrencyLocale _indonesiaIndonesian =
                                    new CurrencyLocale(__indonesiaIndonesian,true);
                            
                                public static final CurrencyLocale _iranPersian =
                                    new CurrencyLocale(__iranPersian,true);
                            
                                public static final CurrencyLocale _iraqArabic =
                                    new CurrencyLocale(__iraqArabic,true);
                            
                                public static final CurrencyLocale _irelandEnglish =
                                    new CurrencyLocale(__irelandEnglish,true);
                            
                                public static final CurrencyLocale _israelHebrew =
                                    new CurrencyLocale(__israelHebrew,true);
                            
                                public static final CurrencyLocale _italyItalian =
                                    new CurrencyLocale(__italyItalian,true);
                            
                                public static final CurrencyLocale _italyItalianEuro =
                                    new CurrencyLocale(__italyItalianEuro,true);
                            
                                public static final CurrencyLocale _jamaicaEnglish =
                                    new CurrencyLocale(__jamaicaEnglish,true);
                            
                                public static final CurrencyLocale _japanJapanese =
                                    new CurrencyLocale(__japanJapanese,true);
                            
                                public static final CurrencyLocale _jordanArabic =
                                    new CurrencyLocale(__jordanArabic,true);
                            
                                public static final CurrencyLocale _jordanEnglish =
                                    new CurrencyLocale(__jordanEnglish,true);
                            
                                public static final CurrencyLocale _kazakhstanRussian =
                                    new CurrencyLocale(__kazakhstanRussian,true);
                            
                                public static final CurrencyLocale _kenyaEnglish =
                                    new CurrencyLocale(__kenyaEnglish,true);
                            
                                public static final CurrencyLocale _kuwaitArabic =
                                    new CurrencyLocale(__kuwaitArabic,true);
                            
                                public static final CurrencyLocale _kuwaitEnglish =
                                    new CurrencyLocale(__kuwaitEnglish,true);
                            
                                public static final CurrencyLocale _kyrgyzstanRussian =
                                    new CurrencyLocale(__kyrgyzstanRussian,true);
                            
                                public static final CurrencyLocale _laosLao =
                                    new CurrencyLocale(__laosLao,true);
                            
                                public static final CurrencyLocale _latviaLatvianLettish =
                                    new CurrencyLocale(__latviaLatvianLettish,true);
                            
                                public static final CurrencyLocale _lebanonArabic =
                                    new CurrencyLocale(__lebanonArabic,true);
                            
                                public static final CurrencyLocale _lesothoEnglish =
                                    new CurrencyLocale(__lesothoEnglish,true);
                            
                                public static final CurrencyLocale _liberiaEnglish =
                                    new CurrencyLocale(__liberiaEnglish,true);
                            
                                public static final CurrencyLocale _libyaArabic =
                                    new CurrencyLocale(__libyaArabic,true);
                            
                                public static final CurrencyLocale _lithuaniaLithuanian =
                                    new CurrencyLocale(__lithuaniaLithuanian,true);
                            
                                public static final CurrencyLocale _luxembourgFrench =
                                    new CurrencyLocale(__luxembourgFrench,true);
                            
                                public static final CurrencyLocale _luxembourgGerman =
                                    new CurrencyLocale(__luxembourgGerman,true);
                            
                                public static final CurrencyLocale _luxembourgLuxembourgish =
                                    new CurrencyLocale(__luxembourgLuxembourgish,true);
                            
                                public static final CurrencyLocale _macauChinese =
                                    new CurrencyLocale(__macauChinese,true);
                            
                                public static final CurrencyLocale _macedoniaMacedonian =
                                    new CurrencyLocale(__macedoniaMacedonian,true);
                            
                                public static final CurrencyLocale _malawiEnglish =
                                    new CurrencyLocale(__malawiEnglish,true);
                            
                                public static final CurrencyLocale _malaysiaMalay =
                                    new CurrencyLocale(__malaysiaMalay,true);
                            
                                public static final CurrencyLocale _maldivesDhivehi =
                                    new CurrencyLocale(__maldivesDhivehi,true);
                            
                                public static final CurrencyLocale _maliFrench =
                                    new CurrencyLocale(__maliFrench,true);
                            
                                public static final CurrencyLocale _mauritiusEnglish =
                                    new CurrencyLocale(__mauritiusEnglish,true);
                            
                                public static final CurrencyLocale _mexicoSpanish =
                                    new CurrencyLocale(__mexicoSpanish,true);
                            
                                public static final CurrencyLocale _moldovaRomanian =
                                    new CurrencyLocale(__moldovaRomanian,true);
                            
                                public static final CurrencyLocale _moldovaRussian =
                                    new CurrencyLocale(__moldovaRussian,true);
                            
                                public static final CurrencyLocale _mongoliaMongolian =
                                    new CurrencyLocale(__mongoliaMongolian,true);
                            
                                public static final CurrencyLocale _moroccoArabic =
                                    new CurrencyLocale(__moroccoArabic,true);
                            
                                public static final CurrencyLocale _mozambiquePortuguese =
                                    new CurrencyLocale(__mozambiquePortuguese,true);
                            
                                public static final CurrencyLocale _myanmarBurmese =
                                    new CurrencyLocale(__myanmarBurmese,true);
                            
                                public static final CurrencyLocale _namibiaEnglish =
                                    new CurrencyLocale(__namibiaEnglish,true);
                            
                                public static final CurrencyLocale _nepalNepali =
                                    new CurrencyLocale(__nepalNepali,true);
                            
                                public static final CurrencyLocale _netherlandsAntillesDutch =
                                    new CurrencyLocale(__netherlandsAntillesDutch,true);
                            
                                public static final CurrencyLocale _netherlandsDutch =
                                    new CurrencyLocale(__netherlandsDutch,true);
                            
                                public static final CurrencyLocale _netherlandsDutchEuro =
                                    new CurrencyLocale(__netherlandsDutchEuro,true);
                            
                                public static final CurrencyLocale _newCaledoniaFrench =
                                    new CurrencyLocale(__newCaledoniaFrench,true);
                            
                                public static final CurrencyLocale _newZealandEnglish =
                                    new CurrencyLocale(__newZealandEnglish,true);
                            
                                public static final CurrencyLocale _nicaraguaSpanish =
                                    new CurrencyLocale(__nicaraguaSpanish,true);
                            
                                public static final CurrencyLocale _nigerFrench =
                                    new CurrencyLocale(__nigerFrench,true);
                            
                                public static final CurrencyLocale _nigeriaEnglish =
                                    new CurrencyLocale(__nigeriaEnglish,true);
                            
                                public static final CurrencyLocale _northKoreaKorean =
                                    new CurrencyLocale(__northKoreaKorean,true);
                            
                                public static final CurrencyLocale _norwayNorwegian =
                                    new CurrencyLocale(__norwayNorwegian,true);
                            
                                public static final CurrencyLocale _omanArabic =
                                    new CurrencyLocale(__omanArabic,true);
                            
                                public static final CurrencyLocale _pakistanUrdu =
                                    new CurrencyLocale(__pakistanUrdu,true);
                            
                                public static final CurrencyLocale _palladiumOunce =
                                    new CurrencyLocale(__palladiumOunce,true);
                            
                                public static final CurrencyLocale _panamaSpanish =
                                    new CurrencyLocale(__panamaSpanish,true);
                            
                                public static final CurrencyLocale _papuaNewGuineaEnglish =
                                    new CurrencyLocale(__papuaNewGuineaEnglish,true);
                            
                                public static final CurrencyLocale _paraguaySpanish =
                                    new CurrencyLocale(__paraguaySpanish,true);
                            
                                public static final CurrencyLocale _peruSpanish =
                                    new CurrencyLocale(__peruSpanish,true);
                            
                                public static final CurrencyLocale _philippinesEnglish =
                                    new CurrencyLocale(__philippinesEnglish,true);
                            
                                public static final CurrencyLocale _philippinesTagalog =
                                    new CurrencyLocale(__philippinesTagalog,true);
                            
                                public static final CurrencyLocale _platinumOunce =
                                    new CurrencyLocale(__platinumOunce,true);
                            
                                public static final CurrencyLocale _polandPolish =
                                    new CurrencyLocale(__polandPolish,true);
                            
                                public static final CurrencyLocale _portugalPortuguese =
                                    new CurrencyLocale(__portugalPortuguese,true);
                            
                                public static final CurrencyLocale _portugalPortugueseEuro =
                                    new CurrencyLocale(__portugalPortugueseEuro,true);
                            
                                public static final CurrencyLocale _puertoRicoSpanish =
                                    new CurrencyLocale(__puertoRicoSpanish,true);
                            
                                public static final CurrencyLocale _qatarArabic =
                                    new CurrencyLocale(__qatarArabic,true);
                            
                                public static final CurrencyLocale _qatarEnglish =
                                    new CurrencyLocale(__qatarEnglish,true);
                            
                                public static final CurrencyLocale _romaniaRomanian =
                                    new CurrencyLocale(__romaniaRomanian,true);
                            
                                public static final CurrencyLocale _russiaRussian =
                                    new CurrencyLocale(__russiaRussian,true);
                            
                                public static final CurrencyLocale _rwandaFrench =
                                    new CurrencyLocale(__rwandaFrench,true);
                            
                                public static final CurrencyLocale _saintBarthelemyFrench =
                                    new CurrencyLocale(__saintBarthelemyFrench,true);
                            
                                public static final CurrencyLocale _saintHelenaEnglish =
                                    new CurrencyLocale(__saintHelenaEnglish,true);
                            
                                public static final CurrencyLocale _saintKittsAndNevisEnglish =
                                    new CurrencyLocale(__saintKittsAndNevisEnglish,true);
                            
                                public static final CurrencyLocale _saintLuciaEnglish =
                                    new CurrencyLocale(__saintLuciaEnglish,true);
                            
                                public static final CurrencyLocale _saintMartinEnglish =
                                    new CurrencyLocale(__saintMartinEnglish,true);
                            
                                public static final CurrencyLocale _saintVincentAndTheGrenadinesEnglish =
                                    new CurrencyLocale(__saintVincentAndTheGrenadinesEnglish,true);
                            
                                public static final CurrencyLocale _samoaSamoan =
                                    new CurrencyLocale(__samoaSamoan,true);
                            
                                public static final CurrencyLocale _saoTomeAndPrincipePortuguese =
                                    new CurrencyLocale(__saoTomeAndPrincipePortuguese,true);
                            
                                public static final CurrencyLocale _saudiArabiaArabic =
                                    new CurrencyLocale(__saudiArabiaArabic,true);
                            
                                public static final CurrencyLocale _senegalFrench =
                                    new CurrencyLocale(__senegalFrench,true);
                            
                                public static final CurrencyLocale _serbiaAndMontenegroSerbian =
                                    new CurrencyLocale(__serbiaAndMontenegroSerbian,true);
                            
                                public static final CurrencyLocale _serbiaSerbian =
                                    new CurrencyLocale(__serbiaSerbian,true);
                            
                                public static final CurrencyLocale _serbiaSerboCroatian =
                                    new CurrencyLocale(__serbiaSerboCroatian,true);
                            
                                public static final CurrencyLocale _seychellesEnglish =
                                    new CurrencyLocale(__seychellesEnglish,true);
                            
                                public static final CurrencyLocale _seychellesFrench =
                                    new CurrencyLocale(__seychellesFrench,true);
                            
                                public static final CurrencyLocale _sierraLeoneEnglish =
                                    new CurrencyLocale(__sierraLeoneEnglish,true);
                            
                                public static final CurrencyLocale _silverOunce =
                                    new CurrencyLocale(__silverOunce,true);
                            
                                public static final CurrencyLocale _singaporeEnglish =
                                    new CurrencyLocale(__singaporeEnglish,true);
                            
                                public static final CurrencyLocale _sintMaartenDutch =
                                    new CurrencyLocale(__sintMaartenDutch,true);
                            
                                public static final CurrencyLocale _slovakiaSlovak =
                                    new CurrencyLocale(__slovakiaSlovak,true);
                            
                                public static final CurrencyLocale _slovakiaSlovakEuro =
                                    new CurrencyLocale(__slovakiaSlovakEuro,true);
                            
                                public static final CurrencyLocale _sloveniaSlovenian =
                                    new CurrencyLocale(__sloveniaSlovenian,true);
                            
                                public static final CurrencyLocale _sloveniaSlovenianEuro =
                                    new CurrencyLocale(__sloveniaSlovenianEuro,true);
                            
                                public static final CurrencyLocale _solomonIslandsEnglish =
                                    new CurrencyLocale(__solomonIslandsEnglish,true);
                            
                                public static final CurrencyLocale _somaliaSomali =
                                    new CurrencyLocale(__somaliaSomali,true);
                            
                                public static final CurrencyLocale _southAfricaAfrikaans =
                                    new CurrencyLocale(__southAfricaAfrikaans,true);
                            
                                public static final CurrencyLocale _southAfricaEnglish =
                                    new CurrencyLocale(__southAfricaEnglish,true);
                            
                                public static final CurrencyLocale _southKoreaKorean =
                                    new CurrencyLocale(__southKoreaKorean,true);
                            
                                public static final CurrencyLocale _southSudanEnglish =
                                    new CurrencyLocale(__southSudanEnglish,true);
                            
                                public static final CurrencyLocale _spainCatalan =
                                    new CurrencyLocale(__spainCatalan,true);
                            
                                public static final CurrencyLocale _spainSpanish =
                                    new CurrencyLocale(__spainSpanish,true);
                            
                                public static final CurrencyLocale _spainSpanishEuro =
                                    new CurrencyLocale(__spainSpanishEuro,true);
                            
                                public static final CurrencyLocale _sriLankaSinhalese =
                                    new CurrencyLocale(__sriLankaSinhalese,true);
                            
                                public static final CurrencyLocale _sudanArabic =
                                    new CurrencyLocale(__sudanArabic,true);
                            
                                public static final CurrencyLocale _surinameDutch =
                                    new CurrencyLocale(__surinameDutch,true);
                            
                                public static final CurrencyLocale _swazilandSwati =
                                    new CurrencyLocale(__swazilandSwati,true);
                            
                                public static final CurrencyLocale _swedenSwedish =
                                    new CurrencyLocale(__swedenSwedish,true);
                            
                                public static final CurrencyLocale _switzerlandFrench =
                                    new CurrencyLocale(__switzerlandFrench,true);
                            
                                public static final CurrencyLocale _switzerlandGerman =
                                    new CurrencyLocale(__switzerlandGerman,true);
                            
                                public static final CurrencyLocale _switzerlandItalian =
                                    new CurrencyLocale(__switzerlandItalian,true);
                            
                                public static final CurrencyLocale _syriaArabic =
                                    new CurrencyLocale(__syriaArabic,true);
                            
                                public static final CurrencyLocale _taiwanChinese =
                                    new CurrencyLocale(__taiwanChinese,true);
                            
                                public static final CurrencyLocale _tajikistanTajik =
                                    new CurrencyLocale(__tajikistanTajik,true);
                            
                                public static final CurrencyLocale _tanzaniaEnglish =
                                    new CurrencyLocale(__tanzaniaEnglish,true);
                            
                                public static final CurrencyLocale _thailandThai =
                                    new CurrencyLocale(__thailandThai,true);
                            
                                public static final CurrencyLocale _togoFrench =
                                    new CurrencyLocale(__togoFrench,true);
                            
                                public static final CurrencyLocale _tongaTonga =
                                    new CurrencyLocale(__tongaTonga,true);
                            
                                public static final CurrencyLocale _trinidadAndTobagoEnglish =
                                    new CurrencyLocale(__trinidadAndTobagoEnglish,true);
                            
                                public static final CurrencyLocale _tunisiaArabic =
                                    new CurrencyLocale(__tunisiaArabic,true);
                            
                                public static final CurrencyLocale _turkeyTurkish =
                                    new CurrencyLocale(__turkeyTurkish,true);
                            
                                public static final CurrencyLocale _turkmenistanTurkmen =
                                    new CurrencyLocale(__turkmenistanTurkmen,true);
                            
                                public static final CurrencyLocale _turksAndCaicosIslandsEnglish =
                                    new CurrencyLocale(__turksAndCaicosIslandsEnglish,true);
                            
                                public static final CurrencyLocale _ugandaEnglish =
                                    new CurrencyLocale(__ugandaEnglish,true);
                            
                                public static final CurrencyLocale _ukraineUkrainian =
                                    new CurrencyLocale(__ukraineUkrainian,true);
                            
                                public static final CurrencyLocale _unitedArabEmiratesArabic =
                                    new CurrencyLocale(__unitedArabEmiratesArabic,true);
                            
                                public static final CurrencyLocale _unitedArabEmiratesEnglish =
                                    new CurrencyLocale(__unitedArabEmiratesEnglish,true);
                            
                                public static final CurrencyLocale _unitedKingdomEnglish =
                                    new CurrencyLocale(__unitedKingdomEnglish,true);
                            
                                public static final CurrencyLocale _unitedStatesEnglish =
                                    new CurrencyLocale(__unitedStatesEnglish,true);
                            
                                public static final CurrencyLocale _uruguaySpanish =
                                    new CurrencyLocale(__uruguaySpanish,true);
                            
                                public static final CurrencyLocale _uzbekistanUzbek =
                                    new CurrencyLocale(__uzbekistanUzbek,true);
                            
                                public static final CurrencyLocale _vanuatuEnglish =
                                    new CurrencyLocale(__vanuatuEnglish,true);
                            
                                public static final CurrencyLocale _vanuatuFrench =
                                    new CurrencyLocale(__vanuatuFrench,true);
                            
                                public static final CurrencyLocale _venezuelaSpanish =
                                    new CurrencyLocale(__venezuelaSpanish,true);
                            
                                public static final CurrencyLocale _vietnamVietnamese =
                                    new CurrencyLocale(__vietnamVietnamese,true);
                            
                                public static final CurrencyLocale _wallisAndFutunaFrench =
                                    new CurrencyLocale(__wallisAndFutunaFrench,true);
                            
                                public static final CurrencyLocale _yemenArabic =
                                    new CurrencyLocale(__yemenArabic,true);
                            
                                public static final CurrencyLocale _zambiaEnglish =
                                    new CurrencyLocale(__zambiaEnglish,true);
                            

                                public java.lang.String getValue() { return localCurrencyLocale;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localCurrencyLocale.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.accounting_2017_2.lists.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":CurrencyLocale",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "CurrencyLocale",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localCurrencyLocale==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("CurrencyLocale cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localCurrencyLocale);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns18";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCurrencyLocale)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static CurrencyLocale fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    CurrencyLocale enumeration = (CurrencyLocale)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static CurrencyLocale fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static CurrencyLocale fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return CurrencyLocale.Factory.fromString(content,namespaceUri);
                    } else {
                       return CurrencyLocale.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CurrencyLocale parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CurrencyLocale object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"CurrencyLocale" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = CurrencyLocale.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = CurrencyLocale.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    