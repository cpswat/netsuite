
/**
 * ItemAccountMappingSearchAdvanced.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  ItemAccountMappingSearchAdvanced bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemAccountMappingSearchAdvanced extends com.netsuite.webservices.platform.core_2017_2.SearchRecord
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemAccountMappingSearchAdvanced
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for Criteria
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearch localCriteria ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCriteriaTracker = false ;

                           public boolean isCriteriaSpecified(){
                               return localCriteriaTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearch
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearch getCriteria(){
                               return localCriteria;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Criteria
                               */
                               public void setCriteria(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearch param){
                            localCriteriaTracker = param != null;
                                   
                                            this.localCriteria=param;
                                    

                               }
                            

                        /**
                        * field for Columns
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchRow localColumns ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColumnsTracker = false ;

                           public boolean isColumnsSpecified(){
                               return localColumnsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchRow
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchRow getColumns(){
                               return localColumns;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Columns
                               */
                               public void setColumns(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchRow param){
                            localColumnsTracker = param != null;
                                   
                                            this.localColumns=param;
                                    

                               }
                            

                        /**
                        * field for SavedSearchId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localSavedSearchId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSavedSearchId(){
                               return localSavedSearchId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SavedSearchId
                               */
                               public void setSavedSearchId(java.lang.String param){
                            
                                            this.localSavedSearchId=param;
                                    

                               }
                            

                        /**
                        * field for SavedSearchScriptId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localSavedSearchScriptId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSavedSearchScriptId(){
                               return localSavedSearchScriptId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SavedSearchScriptId
                               */
                               public void setSavedSearchScriptId(java.lang.String param){
                            
                                            this.localSavedSearchScriptId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemAccountMappingSearchAdvanced",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemAccountMappingSearchAdvanced",
                           xmlWriter);
                   }

               
                                            if (localSavedSearchId != null){
                                        
                                                writeAttribute("",
                                                         "savedSearchId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSavedSearchId), xmlWriter);

                                            
                                      }
                                    
                                            if (localSavedSearchScriptId != null){
                                        
                                                writeAttribute("",
                                                         "savedSearchScriptId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSavedSearchScriptId), xmlWriter);

                                            
                                      }
                                     if (localCriteriaTracker){
                                            if (localCriteria==null){
                                                 throw new org.apache.axis2.databinding.ADBException("criteria cannot be null!!");
                                            }
                                           localCriteria.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","criteria"),
                                               xmlWriter);
                                        } if (localColumnsTracker){
                                            if (localColumns==null){
                                                 throw new org.apache.axis2.databinding.ADBException("columns cannot be null!!");
                                            }
                                           localColumns.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","columns"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","ItemAccountMappingSearchAdvanced"));
                 if (localCriteriaTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "criteria"));
                            
                            
                                    if (localCriteria==null){
                                         throw new org.apache.axis2.databinding.ADBException("criteria cannot be null!!");
                                    }
                                    elementList.add(localCriteria);
                                } if (localColumnsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "columns"));
                            
                            
                                    if (localColumns==null){
                                         throw new org.apache.axis2.databinding.ADBException("columns cannot be null!!");
                                    }
                                    elementList.add(localColumns);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","savedSearchId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSavedSearchId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","savedSearchScriptId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSavedSearchScriptId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemAccountMappingSearchAdvanced parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemAccountMappingSearchAdvanced object =
                new ItemAccountMappingSearchAdvanced();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemAccountMappingSearchAdvanced".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemAccountMappingSearchAdvanced)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "savedSearchId"
                    java.lang.String tempAttribSavedSearchId =
                        
                                reader.getAttributeValue(null,"savedSearchId");
                            
                   if (tempAttribSavedSearchId!=null){
                         java.lang.String content = tempAttribSavedSearchId;
                        
                                                 object.setSavedSearchId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribSavedSearchId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("savedSearchId");
                    
                    // handle attribute "savedSearchScriptId"
                    java.lang.String tempAttribSavedSearchScriptId =
                        
                                reader.getAttributeValue(null,"savedSearchScriptId");
                            
                   if (tempAttribSavedSearchScriptId!=null){
                         java.lang.String content = tempAttribSavedSearchScriptId;
                        
                                                 object.setSavedSearchScriptId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribSavedSearchScriptId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("savedSearchScriptId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","criteria").equals(reader.getName())){
                                
                                                object.setCriteria(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearch.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","columns").equals(reader.getName())){
                                
                                                object.setColumns(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchRow.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    