
/**
 * SerializedInventoryItemLocations.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  SerializedInventoryItemLocations bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SerializedInventoryItemLocations
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = SerializedInventoryItemLocations
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for Location
                        */

                        
                                    protected java.lang.String localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(java.lang.String param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnHand
                        */

                        
                                    protected double localQuantityOnHand ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnHandTracker = false ;

                           public boolean isQuantityOnHandSpecified(){
                               return localQuantityOnHandTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnHand(){
                               return localQuantityOnHand;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnHand
                               */
                               public void setQuantityOnHand(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnHandTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnHand=param;
                                    

                               }
                            

                        /**
                        * field for OnHandValueMli
                        */

                        
                                    protected double localOnHandValueMli ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOnHandValueMliTracker = false ;

                           public boolean isOnHandValueMliSpecified(){
                               return localOnHandValueMliTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getOnHandValueMli(){
                               return localOnHandValueMli;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OnHandValueMli
                               */
                               public void setOnHandValueMli(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localOnHandValueMliTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localOnHandValueMli=param;
                                    

                               }
                            

                        /**
                        * field for SerialNumbers
                        */

                        
                                    protected java.lang.String localSerialNumbers ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSerialNumbersTracker = false ;

                           public boolean isSerialNumbersSpecified(){
                               return localSerialNumbersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSerialNumbers(){
                               return localSerialNumbers;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SerialNumbers
                               */
                               public void setSerialNumbers(java.lang.String param){
                            localSerialNumbersTracker = param != null;
                                   
                                            this.localSerialNumbers=param;
                                    

                               }
                            

                        /**
                        * field for AverageCostMli
                        */

                        
                                    protected double localAverageCostMli ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAverageCostMliTracker = false ;

                           public boolean isAverageCostMliSpecified(){
                               return localAverageCostMliTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAverageCostMli(){
                               return localAverageCostMli;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AverageCostMli
                               */
                               public void setAverageCostMli(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAverageCostMliTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAverageCostMli=param;
                                    

                               }
                            

                        /**
                        * field for LastPurchasePriceMli
                        */

                        
                                    protected double localLastPurchasePriceMli ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastPurchasePriceMliTracker = false ;

                           public boolean isLastPurchasePriceMliSpecified(){
                               return localLastPurchasePriceMliTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLastPurchasePriceMli(){
                               return localLastPurchasePriceMli;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastPurchasePriceMli
                               */
                               public void setLastPurchasePriceMli(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLastPurchasePriceMliTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLastPurchasePriceMli=param;
                                    

                               }
                            

                        /**
                        * field for ReorderPoint
                        */

                        
                                    protected double localReorderPoint ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReorderPointTracker = false ;

                           public boolean isReorderPointSpecified(){
                               return localReorderPointTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getReorderPoint(){
                               return localReorderPoint;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReorderPoint
                               */
                               public void setReorderPoint(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localReorderPointTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localReorderPoint=param;
                                    

                               }
                            

                        /**
                        * field for LocationAllowStorePickup
                        */

                        
                                    protected boolean localLocationAllowStorePickup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationAllowStorePickupTracker = false ;

                           public boolean isLocationAllowStorePickupSpecified(){
                               return localLocationAllowStorePickupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getLocationAllowStorePickup(){
                               return localLocationAllowStorePickup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationAllowStorePickup
                               */
                               public void setLocationAllowStorePickup(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationAllowStorePickupTracker =
                                       true;
                                   
                                            this.localLocationAllowStorePickup=param;
                                    

                               }
                            

                        /**
                        * field for LocationStorePickupBufferStock
                        */

                        
                                    protected double localLocationStorePickupBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationStorePickupBufferStockTracker = false ;

                           public boolean isLocationStorePickupBufferStockSpecified(){
                               return localLocationStorePickupBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLocationStorePickupBufferStock(){
                               return localLocationStorePickupBufferStock;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationStorePickupBufferStock
                               */
                               public void setLocationStorePickupBufferStock(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationStorePickupBufferStockTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLocationStorePickupBufferStock=param;
                                    

                               }
                            

                        /**
                        * field for LocationQtyAvailForStorePickup
                        */

                        
                                    protected double localLocationQtyAvailForStorePickup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationQtyAvailForStorePickupTracker = false ;

                           public boolean isLocationQtyAvailForStorePickupSpecified(){
                               return localLocationQtyAvailForStorePickupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLocationQtyAvailForStorePickup(){
                               return localLocationQtyAvailForStorePickup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationQtyAvailForStorePickup
                               */
                               public void setLocationQtyAvailForStorePickup(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLocationQtyAvailForStorePickupTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLocationQtyAvailForStorePickup=param;
                                    

                               }
                            

                        /**
                        * field for PreferredStockLevel
                        */

                        
                                    protected double localPreferredStockLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPreferredStockLevelTracker = false ;

                           public boolean isPreferredStockLevelSpecified(){
                               return localPreferredStockLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPreferredStockLevel(){
                               return localPreferredStockLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PreferredStockLevel
                               */
                               public void setPreferredStockLevel(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPreferredStockLevelTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPreferredStockLevel=param;
                                    

                               }
                            

                        /**
                        * field for LeadTime
                        */

                        
                                    protected long localLeadTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadTimeTracker = false ;

                           public boolean isLeadTimeSpecified(){
                               return localLeadTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLeadTime(){
                               return localLeadTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadTime
                               */
                               public void setLeadTime(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLeadTimeTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLeadTime=param;
                                    

                               }
                            

                        /**
                        * field for DefaultReturnCost
                        */

                        
                                    protected double localDefaultReturnCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultReturnCostTracker = false ;

                           public boolean isDefaultReturnCostSpecified(){
                               return localDefaultReturnCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getDefaultReturnCost(){
                               return localDefaultReturnCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultReturnCost
                               */
                               public void setDefaultReturnCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localDefaultReturnCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localDefaultReturnCost=param;
                                    

                               }
                            

                        /**
                        * field for IsWip
                        */

                        
                                    protected boolean localIsWip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsWipTracker = false ;

                           public boolean isIsWipSpecified(){
                               return localIsWipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsWip(){
                               return localIsWip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsWip
                               */
                               public void setIsWip(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsWipTracker =
                                       true;
                                   
                                            this.localIsWip=param;
                                    

                               }
                            

                        /**
                        * field for SafetyStockLevel
                        */

                        
                                    protected double localSafetyStockLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSafetyStockLevelTracker = false ;

                           public boolean isSafetyStockLevelSpecified(){
                               return localSafetyStockLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSafetyStockLevel(){
                               return localSafetyStockLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SafetyStockLevel
                               */
                               public void setSafetyStockLevel(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSafetyStockLevelTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSafetyStockLevel=param;
                                    

                               }
                            

                        /**
                        * field for Cost
                        */

                        
                                    protected double localCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostTracker = false ;

                           public boolean isCostSpecified(){
                               return localCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCost(){
                               return localCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cost
                               */
                               public void setCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCost=param;
                                    

                               }
                            

                        /**
                        * field for InventoryCostTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInventoryCostTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryCostTemplateTracker = false ;

                           public boolean isInventoryCostTemplateSpecified(){
                               return localInventoryCostTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInventoryCostTemplate(){
                               return localInventoryCostTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryCostTemplate
                               */
                               public void setInventoryCostTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInventoryCostTemplateTracker = param != null;
                                   
                                            this.localInventoryCostTemplate=param;
                                    

                               }
                            

                        /**
                        * field for BuildTime
                        */

                        
                                    protected double localBuildTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildTimeTracker = false ;

                           public boolean isBuildTimeSpecified(){
                               return localBuildTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getBuildTime(){
                               return localBuildTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildTime
                               */
                               public void setBuildTime(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localBuildTimeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localBuildTime=param;
                                    

                               }
                            

                        /**
                        * field for LastInvtCountDate
                        */

                        
                                    protected java.util.Calendar localLastInvtCountDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastInvtCountDateTracker = false ;

                           public boolean isLastInvtCountDateSpecified(){
                               return localLastInvtCountDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastInvtCountDate(){
                               return localLastInvtCountDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastInvtCountDate
                               */
                               public void setLastInvtCountDate(java.util.Calendar param){
                            localLastInvtCountDateTracker = param != null;
                                   
                                            this.localLastInvtCountDate=param;
                                    

                               }
                            

                        /**
                        * field for NextInvtCountDate
                        */

                        
                                    protected java.util.Calendar localNextInvtCountDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextInvtCountDateTracker = false ;

                           public boolean isNextInvtCountDateSpecified(){
                               return localNextInvtCountDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getNextInvtCountDate(){
                               return localNextInvtCountDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextInvtCountDate
                               */
                               public void setNextInvtCountDate(java.util.Calendar param){
                            localNextInvtCountDateTracker = param != null;
                                   
                                            this.localNextInvtCountDate=param;
                                    

                               }
                            

                        /**
                        * field for InvtCountInterval
                        */

                        
                                    protected long localInvtCountInterval ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInvtCountIntervalTracker = false ;

                           public boolean isInvtCountIntervalSpecified(){
                               return localInvtCountIntervalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getInvtCountInterval(){
                               return localInvtCountInterval;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InvtCountInterval
                               */
                               public void setInvtCountInterval(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localInvtCountIntervalTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localInvtCountInterval=param;
                                    

                               }
                            

                        /**
                        * field for InvtClassification
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemInvtClassification localInvtClassification ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInvtClassificationTracker = false ;

                           public boolean isInvtClassificationSpecified(){
                               return localInvtClassificationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemInvtClassification
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemInvtClassification getInvtClassification(){
                               return localInvtClassification;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InvtClassification
                               */
                               public void setInvtClassification(com.netsuite.webservices.lists.accounting_2017_2.types.ItemInvtClassification param){
                            localInvtClassificationTracker = param != null;
                                   
                                            this.localInvtClassification=param;
                                    

                               }
                            

                        /**
                        * field for CostingLotSize
                        */

                        
                                    protected double localCostingLotSize ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostingLotSizeTracker = false ;

                           public boolean isCostingLotSizeSpecified(){
                               return localCostingLotSizeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCostingLotSize(){
                               return localCostingLotSize;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostingLotSize
                               */
                               public void setCostingLotSize(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostingLotSizeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCostingLotSize=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnOrder
                        */

                        
                                    protected double localQuantityOnOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnOrderTracker = false ;

                           public boolean isQuantityOnOrderSpecified(){
                               return localQuantityOnOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnOrder(){
                               return localQuantityOnOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnOrder
                               */
                               public void setQuantityOnOrder(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnOrderTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnOrder=param;
                                    

                               }
                            

                        /**
                        * field for QuantityCommitted
                        */

                        
                                    protected double localQuantityCommitted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityCommittedTracker = false ;

                           public boolean isQuantityCommittedSpecified(){
                               return localQuantityCommittedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityCommitted(){
                               return localQuantityCommitted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityCommitted
                               */
                               public void setQuantityCommitted(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityCommittedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityCommitted=param;
                                    

                               }
                            

                        /**
                        * field for QuantityAvailable
                        */

                        
                                    protected double localQuantityAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityAvailableTracker = false ;

                           public boolean isQuantityAvailableSpecified(){
                               return localQuantityAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityAvailable(){
                               return localQuantityAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityAvailable
                               */
                               public void setQuantityAvailable(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityAvailableTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityAvailable=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBackOrdered
                        */

                        
                                    protected double localQuantityBackOrdered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBackOrderedTracker = false ;

                           public boolean isQuantityBackOrderedSpecified(){
                               return localQuantityBackOrderedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityBackOrdered(){
                               return localQuantityBackOrdered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBackOrdered
                               */
                               public void setQuantityBackOrdered(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityBackOrderedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityBackOrdered=param;
                                    

                               }
                            

                        /**
                        * field for LocationId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocationId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationIdTracker = false ;

                           public boolean isLocationIdSpecified(){
                               return localLocationIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocationId(){
                               return localLocationId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationId
                               */
                               public void setLocationId(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationIdTracker = param != null;
                                   
                                            this.localLocationId=param;
                                    

                               }
                            

                        /**
                        * field for SupplyReplenishmentMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSupplyReplenishmentMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupplyReplenishmentMethodTracker = false ;

                           public boolean isSupplyReplenishmentMethodSpecified(){
                               return localSupplyReplenishmentMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSupplyReplenishmentMethod(){
                               return localSupplyReplenishmentMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SupplyReplenishmentMethod
                               */
                               public void setSupplyReplenishmentMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSupplyReplenishmentMethodTracker = param != null;
                                   
                                            this.localSupplyReplenishmentMethod=param;
                                    

                               }
                            

                        /**
                        * field for AlternateDemandSourceItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAlternateDemandSourceItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAlternateDemandSourceItemTracker = false ;

                           public boolean isAlternateDemandSourceItemSpecified(){
                               return localAlternateDemandSourceItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAlternateDemandSourceItem(){
                               return localAlternateDemandSourceItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AlternateDemandSourceItem
                               */
                               public void setAlternateDemandSourceItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAlternateDemandSourceItemTracker = param != null;
                                   
                                            this.localAlternateDemandSourceItem=param;
                                    

                               }
                            

                        /**
                        * field for FixedLotSize
                        */

                        
                                    protected double localFixedLotSize ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFixedLotSizeTracker = false ;

                           public boolean isFixedLotSizeSpecified(){
                               return localFixedLotSizeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getFixedLotSize(){
                               return localFixedLotSize;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FixedLotSize
                               */
                               public void setFixedLotSize(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localFixedLotSizeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localFixedLotSize=param;
                                    

                               }
                            

                        /**
                        * field for PeriodicLotSizeType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.PeriodicLotSizeType localPeriodicLotSizeType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodicLotSizeTypeTracker = false ;

                           public boolean isPeriodicLotSizeTypeSpecified(){
                               return localPeriodicLotSizeTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.PeriodicLotSizeType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.PeriodicLotSizeType getPeriodicLotSizeType(){
                               return localPeriodicLotSizeType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodicLotSizeType
                               */
                               public void setPeriodicLotSizeType(com.netsuite.webservices.lists.accounting_2017_2.types.PeriodicLotSizeType param){
                            localPeriodicLotSizeTypeTracker = param != null;
                                   
                                            this.localPeriodicLotSizeType=param;
                                    

                               }
                            

                        /**
                        * field for PeriodicLotSizeDays
                        */

                        
                                    protected long localPeriodicLotSizeDays ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodicLotSizeDaysTracker = false ;

                           public boolean isPeriodicLotSizeDaysSpecified(){
                               return localPeriodicLotSizeDaysTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPeriodicLotSizeDays(){
                               return localPeriodicLotSizeDays;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodicLotSizeDays
                               */
                               public void setPeriodicLotSizeDays(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodicLotSizeDaysTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPeriodicLotSizeDays=param;
                                    

                               }
                            

                        /**
                        * field for SupplyType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSupplyType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupplyTypeTracker = false ;

                           public boolean isSupplyTypeSpecified(){
                               return localSupplyTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSupplyType(){
                               return localSupplyType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SupplyType
                               */
                               public void setSupplyType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSupplyTypeTracker = param != null;
                                   
                                            this.localSupplyType=param;
                                    

                               }
                            

                        /**
                        * field for SupplyLotSizingMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSupplyLotSizingMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupplyLotSizingMethodTracker = false ;

                           public boolean isSupplyLotSizingMethodSpecified(){
                               return localSupplyLotSizingMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSupplyLotSizingMethod(){
                               return localSupplyLotSizingMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SupplyLotSizingMethod
                               */
                               public void setSupplyLotSizingMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSupplyLotSizingMethodTracker = param != null;
                                   
                                            this.localSupplyLotSizingMethod=param;
                                    

                               }
                            

                        /**
                        * field for DemandSource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDemandSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDemandSourceTracker = false ;

                           public boolean isDemandSourceSpecified(){
                               return localDemandSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDemandSource(){
                               return localDemandSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DemandSource
                               */
                               public void setDemandSource(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDemandSourceTracker = param != null;
                                   
                                            this.localDemandSource=param;
                                    

                               }
                            

                        /**
                        * field for BackwardConsumptionDays
                        */

                        
                                    protected long localBackwardConsumptionDays ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBackwardConsumptionDaysTracker = false ;

                           public boolean isBackwardConsumptionDaysSpecified(){
                               return localBackwardConsumptionDaysTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBackwardConsumptionDays(){
                               return localBackwardConsumptionDays;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BackwardConsumptionDays
                               */
                               public void setBackwardConsumptionDays(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localBackwardConsumptionDaysTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localBackwardConsumptionDays=param;
                                    

                               }
                            

                        /**
                        * field for ForwardConsumptionDays
                        */

                        
                                    protected long localForwardConsumptionDays ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForwardConsumptionDaysTracker = false ;

                           public boolean isForwardConsumptionDaysSpecified(){
                               return localForwardConsumptionDaysTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getForwardConsumptionDays(){
                               return localForwardConsumptionDays;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ForwardConsumptionDays
                               */
                               public void setForwardConsumptionDays(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localForwardConsumptionDaysTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localForwardConsumptionDays=param;
                                    

                               }
                            

                        /**
                        * field for DemandTimeFence
                        */

                        
                                    protected long localDemandTimeFence ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDemandTimeFenceTracker = false ;

                           public boolean isDemandTimeFenceSpecified(){
                               return localDemandTimeFenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getDemandTimeFence(){
                               return localDemandTimeFence;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DemandTimeFence
                               */
                               public void setDemandTimeFence(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localDemandTimeFenceTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localDemandTimeFence=param;
                                    

                               }
                            

                        /**
                        * field for SupplyTimeFence
                        */

                        
                                    protected long localSupplyTimeFence ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupplyTimeFenceTracker = false ;

                           public boolean isSupplyTimeFenceSpecified(){
                               return localSupplyTimeFenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getSupplyTimeFence(){
                               return localSupplyTimeFence;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SupplyTimeFence
                               */
                               public void setSupplyTimeFence(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localSupplyTimeFenceTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localSupplyTimeFence=param;
                                    

                               }
                            

                        /**
                        * field for RescheduleInDays
                        */

                        
                                    protected long localRescheduleInDays ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRescheduleInDaysTracker = false ;

                           public boolean isRescheduleInDaysSpecified(){
                               return localRescheduleInDaysTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getRescheduleInDays(){
                               return localRescheduleInDays;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RescheduleInDays
                               */
                               public void setRescheduleInDays(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localRescheduleInDaysTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localRescheduleInDays=param;
                                    

                               }
                            

                        /**
                        * field for RescheduleOutDays
                        */

                        
                                    protected long localRescheduleOutDays ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRescheduleOutDaysTracker = false ;

                           public boolean isRescheduleOutDaysSpecified(){
                               return localRescheduleOutDaysTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getRescheduleOutDays(){
                               return localRescheduleOutDays;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RescheduleOutDays
                               */
                               public void setRescheduleOutDays(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localRescheduleOutDaysTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localRescheduleOutDays=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":SerializedInventoryItemLocations",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "SerializedInventoryItemLocations",
                           xmlWriter);
                   }

               
                   }
                if (localLocationTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "location", xmlWriter);
                             

                                          if (localLocation==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocation);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityOnHandTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnHand", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnHand)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnHand cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOnHandValueMliTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "onHandValueMli", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localOnHandValueMli)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("onHandValueMli cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnHandValueMli));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSerialNumbersTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "serialNumbers", xmlWriter);
                             

                                          if (localSerialNumbers==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSerialNumbers);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAverageCostMliTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "averageCostMli", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAverageCostMli)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("averageCostMli cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAverageCostMli));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastPurchasePriceMliTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastPurchasePriceMli", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLastPurchasePriceMli)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("lastPurchasePriceMli cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastPurchasePriceMli));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReorderPointTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reorderPoint", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localReorderPoint)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("reorderPoint cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReorderPoint));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationAllowStorePickupTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "locationAllowStorePickup", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationAllowStorePickup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationAllowStorePickup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationStorePickupBufferStockTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "locationStorePickupBufferStock", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLocationStorePickupBufferStock)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationStorePickupBufferStock cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationStorePickupBufferStock));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationQtyAvailForStorePickupTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "locationQtyAvailForStorePickup", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLocationQtyAvailForStorePickup)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("locationQtyAvailForStorePickup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationQtyAvailForStorePickup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPreferredStockLevelTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "preferredStockLevel", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPreferredStockLevel)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("preferredStockLevel cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPreferredStockLevel));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLeadTimeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "leadTime", xmlWriter);
                             
                                               if (localLeadTime==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("leadTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLeadTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDefaultReturnCostTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "defaultReturnCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localDefaultReturnCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("defaultReturnCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultReturnCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsWipTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isWip", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isWip cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsWip));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSafetyStockLevelTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "safetyStockLevel", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSafetyStockLevel)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("safetyStockLevel cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSafetyStockLevel));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "cost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("cost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInventoryCostTemplateTracker){
                                            if (localInventoryCostTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryCostTemplate cannot be null!!");
                                            }
                                           localInventoryCostTemplate.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryCostTemplate"),
                                               xmlWriter);
                                        } if (localBuildTimeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "buildTime", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localBuildTime)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("buildTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBuildTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastInvtCountDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastInvtCountDate", xmlWriter);
                             

                                          if (localLastInvtCountDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastInvtCountDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastInvtCountDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNextInvtCountDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "nextInvtCountDate", xmlWriter);
                             

                                          if (localNextInvtCountDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("nextInvtCountDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextInvtCountDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInvtCountIntervalTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "invtCountInterval", xmlWriter);
                             
                                               if (localInvtCountInterval==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("invtCountInterval cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInvtCountInterval));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInvtClassificationTracker){
                                            if (localInvtClassification==null){
                                                 throw new org.apache.axis2.databinding.ADBException("invtClassification cannot be null!!");
                                            }
                                           localInvtClassification.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","invtClassification"),
                                               xmlWriter);
                                        } if (localCostingLotSizeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costingLotSize", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCostingLotSize)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("costingLotSize cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostingLotSize));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityOnOrderTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnOrder", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnOrder)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnOrder cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnOrder));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityCommittedTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityCommitted", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityCommitted)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityCommitted cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityCommitted));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityAvailableTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityAvailable", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityAvailable)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityAvailable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityBackOrderedTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityBackOrdered", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityBackOrdered)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityBackOrdered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBackOrdered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationIdTracker){
                                            if (localLocationId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationId cannot be null!!");
                                            }
                                           localLocationId.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationId"),
                                               xmlWriter);
                                        } if (localSupplyReplenishmentMethodTracker){
                                            if (localSupplyReplenishmentMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supplyReplenishmentMethod cannot be null!!");
                                            }
                                           localSupplyReplenishmentMethod.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyReplenishmentMethod"),
                                               xmlWriter);
                                        } if (localAlternateDemandSourceItemTracker){
                                            if (localAlternateDemandSourceItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("alternateDemandSourceItem cannot be null!!");
                                            }
                                           localAlternateDemandSourceItem.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","alternateDemandSourceItem"),
                                               xmlWriter);
                                        } if (localFixedLotSizeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fixedLotSize", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localFixedLotSize)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("fixedLotSize cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFixedLotSize));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPeriodicLotSizeTypeTracker){
                                            if (localPeriodicLotSizeType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("periodicLotSizeType cannot be null!!");
                                            }
                                           localPeriodicLotSizeType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","periodicLotSizeType"),
                                               xmlWriter);
                                        } if (localPeriodicLotSizeDaysTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "periodicLotSizeDays", xmlWriter);
                             
                                               if (localPeriodicLotSizeDays==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("periodicLotSizeDays cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodicLotSizeDays));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSupplyTypeTracker){
                                            if (localSupplyType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supplyType cannot be null!!");
                                            }
                                           localSupplyType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyType"),
                                               xmlWriter);
                                        } if (localSupplyLotSizingMethodTracker){
                                            if (localSupplyLotSizingMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supplyLotSizingMethod cannot be null!!");
                                            }
                                           localSupplyLotSizingMethod.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyLotSizingMethod"),
                                               xmlWriter);
                                        } if (localDemandSourceTracker){
                                            if (localDemandSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("demandSource cannot be null!!");
                                            }
                                           localDemandSource.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","demandSource"),
                                               xmlWriter);
                                        } if (localBackwardConsumptionDaysTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "backwardConsumptionDays", xmlWriter);
                             
                                               if (localBackwardConsumptionDays==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("backwardConsumptionDays cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBackwardConsumptionDays));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localForwardConsumptionDaysTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "forwardConsumptionDays", xmlWriter);
                             
                                               if (localForwardConsumptionDays==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("forwardConsumptionDays cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localForwardConsumptionDays));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDemandTimeFenceTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "demandTimeFence", xmlWriter);
                             
                                               if (localDemandTimeFence==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("demandTimeFence cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDemandTimeFence));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSupplyTimeFenceTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "supplyTimeFence", xmlWriter);
                             
                                               if (localSupplyTimeFence==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("supplyTimeFence cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSupplyTimeFence));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRescheduleInDaysTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rescheduleInDays", xmlWriter);
                             
                                               if (localRescheduleInDays==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("rescheduleInDays cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRescheduleInDays));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRescheduleOutDaysTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rescheduleOutDays", xmlWriter);
                             
                                               if (localRescheduleOutDays==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("rescheduleOutDays cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRescheduleOutDays));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localLocationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "location"));
                                 
                                        if (localLocation != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocation));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                        }
                                    } if (localQuantityOnHandTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityOnHand"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                            } if (localOnHandValueMliTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "onHandValueMli"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnHandValueMli));
                            } if (localSerialNumbersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "serialNumbers"));
                                 
                                        if (localSerialNumbers != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSerialNumbers));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("serialNumbers cannot be null!!");
                                        }
                                    } if (localAverageCostMliTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "averageCostMli"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAverageCostMli));
                            } if (localLastPurchasePriceMliTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "lastPurchasePriceMli"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastPurchasePriceMli));
                            } if (localReorderPointTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "reorderPoint"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReorderPoint));
                            } if (localLocationAllowStorePickupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "locationAllowStorePickup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationAllowStorePickup));
                            } if (localLocationStorePickupBufferStockTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "locationStorePickupBufferStock"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationStorePickupBufferStock));
                            } if (localLocationQtyAvailForStorePickupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "locationQtyAvailForStorePickup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationQtyAvailForStorePickup));
                            } if (localPreferredStockLevelTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "preferredStockLevel"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPreferredStockLevel));
                            } if (localLeadTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "leadTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLeadTime));
                            } if (localDefaultReturnCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "defaultReturnCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultReturnCost));
                            } if (localIsWipTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isWip"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsWip));
                            } if (localSafetyStockLevelTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "safetyStockLevel"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSafetyStockLevel));
                            } if (localCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "cost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCost));
                            } if (localInventoryCostTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "inventoryCostTemplate"));
                            
                            
                                    if (localInventoryCostTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryCostTemplate cannot be null!!");
                                    }
                                    elementList.add(localInventoryCostTemplate);
                                } if (localBuildTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "buildTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBuildTime));
                            } if (localLastInvtCountDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "lastInvtCountDate"));
                                 
                                        if (localLastInvtCountDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastInvtCountDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastInvtCountDate cannot be null!!");
                                        }
                                    } if (localNextInvtCountDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "nextInvtCountDate"));
                                 
                                        if (localNextInvtCountDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextInvtCountDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("nextInvtCountDate cannot be null!!");
                                        }
                                    } if (localInvtCountIntervalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "invtCountInterval"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInvtCountInterval));
                            } if (localInvtClassificationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "invtClassification"));
                            
                            
                                    if (localInvtClassification==null){
                                         throw new org.apache.axis2.databinding.ADBException("invtClassification cannot be null!!");
                                    }
                                    elementList.add(localInvtClassification);
                                } if (localCostingLotSizeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costingLotSize"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostingLotSize));
                            } if (localQuantityOnOrderTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityOnOrder"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnOrder));
                            } if (localQuantityCommittedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityCommitted"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityCommitted));
                            } if (localQuantityAvailableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityAvailable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                            } if (localQuantityBackOrderedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityBackOrdered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBackOrdered));
                            } if (localLocationIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "locationId"));
                            
                            
                                    if (localLocationId==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationId cannot be null!!");
                                    }
                                    elementList.add(localLocationId);
                                } if (localSupplyReplenishmentMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "supplyReplenishmentMethod"));
                            
                            
                                    if (localSupplyReplenishmentMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("supplyReplenishmentMethod cannot be null!!");
                                    }
                                    elementList.add(localSupplyReplenishmentMethod);
                                } if (localAlternateDemandSourceItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "alternateDemandSourceItem"));
                            
                            
                                    if (localAlternateDemandSourceItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("alternateDemandSourceItem cannot be null!!");
                                    }
                                    elementList.add(localAlternateDemandSourceItem);
                                } if (localFixedLotSizeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "fixedLotSize"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFixedLotSize));
                            } if (localPeriodicLotSizeTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "periodicLotSizeType"));
                            
                            
                                    if (localPeriodicLotSizeType==null){
                                         throw new org.apache.axis2.databinding.ADBException("periodicLotSizeType cannot be null!!");
                                    }
                                    elementList.add(localPeriodicLotSizeType);
                                } if (localPeriodicLotSizeDaysTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "periodicLotSizeDays"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriodicLotSizeDays));
                            } if (localSupplyTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "supplyType"));
                            
                            
                                    if (localSupplyType==null){
                                         throw new org.apache.axis2.databinding.ADBException("supplyType cannot be null!!");
                                    }
                                    elementList.add(localSupplyType);
                                } if (localSupplyLotSizingMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "supplyLotSizingMethod"));
                            
                            
                                    if (localSupplyLotSizingMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("supplyLotSizingMethod cannot be null!!");
                                    }
                                    elementList.add(localSupplyLotSizingMethod);
                                } if (localDemandSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "demandSource"));
                            
                            
                                    if (localDemandSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("demandSource cannot be null!!");
                                    }
                                    elementList.add(localDemandSource);
                                } if (localBackwardConsumptionDaysTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "backwardConsumptionDays"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBackwardConsumptionDays));
                            } if (localForwardConsumptionDaysTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "forwardConsumptionDays"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localForwardConsumptionDays));
                            } if (localDemandTimeFenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "demandTimeFence"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDemandTimeFence));
                            } if (localSupplyTimeFenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "supplyTimeFence"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSupplyTimeFence));
                            } if (localRescheduleInDaysTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "rescheduleInDays"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRescheduleInDays));
                            } if (localRescheduleOutDaysTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "rescheduleOutDays"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRescheduleOutDays));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SerializedInventoryItemLocations parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SerializedInventoryItemLocations object =
                new SerializedInventoryItemLocations();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"SerializedInventoryItemLocations".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SerializedInventoryItemLocations)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"location" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityOnHand").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnHand" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnHand(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnHand(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","onHandValueMli").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"onHandValueMli" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOnHandValueMli(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOnHandValueMli(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","serialNumbers").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"serialNumbers" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSerialNumbers(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","averageCostMli").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"averageCostMli" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAverageCostMli(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAverageCostMli(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","lastPurchasePriceMli").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastPurchasePriceMli" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastPurchasePriceMli(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLastPurchasePriceMli(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","reorderPoint").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reorderPoint" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReorderPoint(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setReorderPoint(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationAllowStorePickup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationAllowStorePickup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationAllowStorePickup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationStorePickupBufferStock").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationStorePickupBufferStock" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationStorePickupBufferStock(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLocationStorePickupBufferStock(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationQtyAvailForStorePickup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"locationQtyAvailForStorePickup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocationQtyAvailForStorePickup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLocationQtyAvailForStorePickup(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferredStockLevel").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"preferredStockLevel" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPreferredStockLevel(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPreferredStockLevel(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","leadTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"leadTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLeadTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLeadTime(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","defaultReturnCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"defaultReturnCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDefaultReturnCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDefaultReturnCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isWip").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isWip" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsWip(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","safetyStockLevel").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"safetyStockLevel" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSafetyStockLevel(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSafetyStockLevel(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","cost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"cost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryCostTemplate").equals(reader.getName())){
                                
                                                object.setInventoryCostTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","buildTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"buildTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBuildTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBuildTime(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","lastInvtCountDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastInvtCountDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastInvtCountDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","nextInvtCountDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"nextInvtCountDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNextInvtCountDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","invtCountInterval").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"invtCountInterval" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInvtCountInterval(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setInvtCountInterval(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","invtClassification").equals(reader.getName())){
                                
                                                object.setInvtClassification(com.netsuite.webservices.lists.accounting_2017_2.types.ItemInvtClassification.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costingLotSize").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costingLotSize" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostingLotSize(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCostingLotSize(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityOnOrder").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnOrder" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnOrder(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnOrder(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityCommitted").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityCommitted" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityCommitted(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityCommitted(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityAvailable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityAvailable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityAvailable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityAvailable(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityBackOrdered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityBackOrdered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityBackOrdered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityBackOrdered(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationId").equals(reader.getName())){
                                
                                                object.setLocationId(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyReplenishmentMethod").equals(reader.getName())){
                                
                                                object.setSupplyReplenishmentMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","alternateDemandSourceItem").equals(reader.getName())){
                                
                                                object.setAlternateDemandSourceItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","fixedLotSize").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fixedLotSize" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFixedLotSize(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setFixedLotSize(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","periodicLotSizeType").equals(reader.getName())){
                                
                                                object.setPeriodicLotSizeType(com.netsuite.webservices.lists.accounting_2017_2.types.PeriodicLotSizeType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","periodicLotSizeDays").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"periodicLotSizeDays" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriodicLotSizeDays(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriodicLotSizeDays(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyType").equals(reader.getName())){
                                
                                                object.setSupplyType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyLotSizingMethod").equals(reader.getName())){
                                
                                                object.setSupplyLotSizingMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","demandSource").equals(reader.getName())){
                                
                                                object.setDemandSource(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","backwardConsumptionDays").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"backwardConsumptionDays" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBackwardConsumptionDays(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBackwardConsumptionDays(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","forwardConsumptionDays").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"forwardConsumptionDays" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setForwardConsumptionDays(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setForwardConsumptionDays(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","demandTimeFence").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"demandTimeFence" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDemandTimeFence(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDemandTimeFence(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","supplyTimeFence").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"supplyTimeFence" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSupplyTimeFence(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSupplyTimeFence(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","rescheduleInDays").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rescheduleInDays" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRescheduleInDays(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRescheduleInDays(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","rescheduleOutDays").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rescheduleOutDays" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRescheduleOutDays(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRescheduleOutDays(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    