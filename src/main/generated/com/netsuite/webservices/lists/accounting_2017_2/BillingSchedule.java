
/**
 * BillingSchedule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  BillingSchedule bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class BillingSchedule extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = BillingSchedule
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for ScheduleType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleType localScheduleType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleTypeTracker = false ;

                           public boolean isScheduleTypeSpecified(){
                               return localScheduleTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleType getScheduleType(){
                               return localScheduleType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleType
                               */
                               public void setScheduleType(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleType param){
                            localScheduleTypeTracker = param != null;
                                   
                                            this.localScheduleType=param;
                                    

                               }
                            

                        /**
                        * field for Name
                        */

                        
                                    protected java.lang.String localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(java.lang.String param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for RecurrencePattern
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrencePattern localRecurrencePattern ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrencePatternTracker = false ;

                           public boolean isRecurrencePatternSpecified(){
                               return localRecurrencePatternTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrencePattern
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrencePattern getRecurrencePattern(){
                               return localRecurrencePattern;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrencePattern
                               */
                               public void setRecurrencePattern(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrencePattern param){
                            localRecurrencePatternTracker = param != null;
                                   
                                            this.localRecurrencePattern=param;
                                    

                               }
                            

                        /**
                        * field for Project
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProject ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTracker = false ;

                           public boolean isProjectSpecified(){
                               return localProjectTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProject(){
                               return localProject;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Project
                               */
                               public void setProject(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProjectTracker = param != null;
                                   
                                            this.localProject=param;
                                    

                               }
                            

                        /**
                        * field for InitialAmount
                        */

                        
                                    protected java.lang.String localInitialAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInitialAmountTracker = false ;

                           public boolean isInitialAmountSpecified(){
                               return localInitialAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInitialAmount(){
                               return localInitialAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InitialAmount
                               */
                               public void setInitialAmount(java.lang.String param){
                            localInitialAmountTracker = param != null;
                                   
                                            this.localInitialAmount=param;
                                    

                               }
                            

                        /**
                        * field for InitialTerms
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInitialTerms ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInitialTermsTracker = false ;

                           public boolean isInitialTermsSpecified(){
                               return localInitialTermsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInitialTerms(){
                               return localInitialTerms;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InitialTerms
                               */
                               public void setInitialTerms(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInitialTermsTracker = param != null;
                                   
                                            this.localInitialTerms=param;
                                    

                               }
                            

                        /**
                        * field for Frequency
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleFrequency localFrequency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFrequencyTracker = false ;

                           public boolean isFrequencySpecified(){
                               return localFrequencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleFrequency
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleFrequency getFrequency(){
                               return localFrequency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Frequency
                               */
                               public void setFrequency(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleFrequency param){
                            localFrequencyTracker = param != null;
                                   
                                            this.localFrequency=param;
                                    

                               }
                            

                        /**
                        * field for RecurrenceDowMaskList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList localRecurrenceDowMaskList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceDowMaskListTracker = false ;

                           public boolean isRecurrenceDowMaskListSpecified(){
                               return localRecurrenceDowMaskListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList getRecurrenceDowMaskList(){
                               return localRecurrenceDowMaskList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrenceDowMaskList
                               */
                               public void setRecurrenceDowMaskList(com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList param){
                            localRecurrenceDowMaskListTracker = param != null;
                                   
                                            this.localRecurrenceDowMaskList=param;
                                    

                               }
                            

                        /**
                        * field for YearMode
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode localYearMode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearModeTracker = false ;

                           public boolean isYearModeSpecified(){
                               return localYearModeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode getYearMode(){
                               return localYearMode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param YearMode
                               */
                               public void setYearMode(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode param){
                            localYearModeTracker = param != null;
                                   
                                            this.localYearMode=param;
                                    

                               }
                            

                        /**
                        * field for YearDowim
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowim localYearDowim ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearDowimTracker = false ;

                           public boolean isYearDowimSpecified(){
                               return localYearDowimTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowim
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowim getYearDowim(){
                               return localYearDowim;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param YearDowim
                               */
                               public void setYearDowim(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowim param){
                            localYearDowimTracker = param != null;
                                   
                                            this.localYearDowim=param;
                                    

                               }
                            

                        /**
                        * field for YearDow
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDow localYearDow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearDowTracker = false ;

                           public boolean isYearDowSpecified(){
                               return localYearDowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDow
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDow getYearDow(){
                               return localYearDow;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param YearDow
                               */
                               public void setYearDow(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDow param){
                            localYearDowTracker = param != null;
                                   
                                            this.localYearDow=param;
                                    

                               }
                            

                        /**
                        * field for YearDowimMonth
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowimMonth localYearDowimMonth ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearDowimMonthTracker = false ;

                           public boolean isYearDowimMonthSpecified(){
                               return localYearDowimMonthTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowimMonth
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowimMonth getYearDowimMonth(){
                               return localYearDowimMonth;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param YearDowimMonth
                               */
                               public void setYearDowimMonth(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowimMonth param){
                            localYearDowimMonthTracker = param != null;
                                   
                                            this.localYearDowimMonth=param;
                                    

                               }
                            

                        /**
                        * field for YearMonth
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearMonth localYearMonth ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearMonthTracker = false ;

                           public boolean isYearMonthSpecified(){
                               return localYearMonthTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearMonth
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearMonth getYearMonth(){
                               return localYearMonth;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param YearMonth
                               */
                               public void setYearMonth(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearMonth param){
                            localYearMonthTracker = param != null;
                                   
                                            this.localYearMonth=param;
                                    

                               }
                            

                        /**
                        * field for YearDom
                        */

                        
                                    protected long localYearDom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localYearDomTracker = false ;

                           public boolean isYearDomSpecified(){
                               return localYearDomTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getYearDom(){
                               return localYearDom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param YearDom
                               */
                               public void setYearDom(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localYearDomTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localYearDom=param;
                                    

                               }
                            

                        /**
                        * field for MonthMode
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode localMonthMode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMonthModeTracker = false ;

                           public boolean isMonthModeSpecified(){
                               return localMonthModeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode getMonthMode(){
                               return localMonthMode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MonthMode
                               */
                               public void setMonthMode(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode param){
                            localMonthModeTracker = param != null;
                                   
                                            this.localMonthMode=param;
                                    

                               }
                            

                        /**
                        * field for MonthDowim
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDowim localMonthDowim ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMonthDowimTracker = false ;

                           public boolean isMonthDowimSpecified(){
                               return localMonthDowimTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDowim
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDowim getMonthDowim(){
                               return localMonthDowim;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MonthDowim
                               */
                               public void setMonthDowim(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDowim param){
                            localMonthDowimTracker = param != null;
                                   
                                            this.localMonthDowim=param;
                                    

                               }
                            

                        /**
                        * field for MonthDow
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDow localMonthDow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMonthDowTracker = false ;

                           public boolean isMonthDowSpecified(){
                               return localMonthDowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDow
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDow getMonthDow(){
                               return localMonthDow;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MonthDow
                               */
                               public void setMonthDow(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDow param){
                            localMonthDowTracker = param != null;
                                   
                                            this.localMonthDow=param;
                                    

                               }
                            

                        /**
                        * field for MonthDom
                        */

                        
                                    protected long localMonthDom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMonthDomTracker = false ;

                           public boolean isMonthDomSpecified(){
                               return localMonthDomTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getMonthDom(){
                               return localMonthDom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MonthDom
                               */
                               public void setMonthDom(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localMonthDomTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localMonthDom=param;
                                    

                               }
                            

                        /**
                        * field for DayPeriod
                        */

                        
                                    protected long localDayPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDayPeriodTracker = false ;

                           public boolean isDayPeriodSpecified(){
                               return localDayPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getDayPeriod(){
                               return localDayPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DayPeriod
                               */
                               public void setDayPeriod(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localDayPeriodTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localDayPeriod=param;
                                    

                               }
                            

                        /**
                        * field for RepeatEvery
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRepeatEvery localRepeatEvery ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRepeatEveryTracker = false ;

                           public boolean isRepeatEverySpecified(){
                               return localRepeatEveryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRepeatEvery
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRepeatEvery getRepeatEvery(){
                               return localRepeatEvery;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RepeatEvery
                               */
                               public void setRepeatEvery(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRepeatEvery param){
                            localRepeatEveryTracker = param != null;
                                   
                                            this.localRepeatEvery=param;
                                    

                               }
                            

                        /**
                        * field for BillForActuals
                        */

                        
                                    protected boolean localBillForActuals ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillForActualsTracker = false ;

                           public boolean isBillForActualsSpecified(){
                               return localBillForActualsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getBillForActuals(){
                               return localBillForActuals;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillForActuals
                               */
                               public void setBillForActuals(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localBillForActualsTracker =
                                       true;
                                   
                                            this.localBillForActuals=param;
                                    

                               }
                            

                        /**
                        * field for NumberRemaining
                        */

                        
                                    protected long localNumberRemaining ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberRemainingTracker = false ;

                           public boolean isNumberRemainingSpecified(){
                               return localNumberRemainingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getNumberRemaining(){
                               return localNumberRemaining;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberRemaining
                               */
                               public void setNumberRemaining(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localNumberRemainingTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localNumberRemaining=param;
                                    

                               }
                            

                        /**
                        * field for InArrears
                        */

                        
                                    protected boolean localInArrears ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInArrearsTracker = false ;

                           public boolean isInArrearsSpecified(){
                               return localInArrearsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getInArrears(){
                               return localInArrears;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InArrears
                               */
                               public void setInArrears(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localInArrearsTracker =
                                       true;
                                   
                                            this.localInArrears=param;
                                    

                               }
                            

                        /**
                        * field for RecurrenceTerms
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRecurrenceTerms ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceTermsTracker = false ;

                           public boolean isRecurrenceTermsSpecified(){
                               return localRecurrenceTermsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRecurrenceTerms(){
                               return localRecurrenceTerms;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrenceTerms
                               */
                               public void setRecurrenceTerms(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRecurrenceTermsTracker = param != null;
                                   
                                            this.localRecurrenceTerms=param;
                                    

                               }
                            

                        /**
                        * field for IsPublic
                        */

                        
                                    protected boolean localIsPublic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsPublicTracker = false ;

                           public boolean isIsPublicSpecified(){
                               return localIsPublicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsPublic(){
                               return localIsPublic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsPublic
                               */
                               public void setIsPublic(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsPublicTracker =
                                       true;
                                   
                                            this.localIsPublic=param;
                                    

                               }
                            

                        /**
                        * field for ApplyToSubtotal
                        */

                        
                                    protected boolean localApplyToSubtotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApplyToSubtotalTracker = false ;

                           public boolean isApplyToSubtotalSpecified(){
                               return localApplyToSubtotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getApplyToSubtotal(){
                               return localApplyToSubtotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApplyToSubtotal
                               */
                               public void setApplyToSubtotal(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localApplyToSubtotalTracker =
                                       true;
                                   
                                            this.localApplyToSubtotal=param;
                                    

                               }
                            

                        /**
                        * field for Transaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionTracker = false ;

                           public boolean isTransactionSpecified(){
                               return localTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTransaction(){
                               return localTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Transaction
                               */
                               public void setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTransactionTracker = param != null;
                                   
                                            this.localTransaction=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for SeriesStartDate
                        */

                        
                                    protected java.util.Calendar localSeriesStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSeriesStartDateTracker = false ;

                           public boolean isSeriesStartDateSpecified(){
                               return localSeriesStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getSeriesStartDate(){
                               return localSeriesStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SeriesStartDate
                               */
                               public void setSeriesStartDate(java.util.Calendar param){
                            localSeriesStartDateTracker = param != null;
                                   
                                            this.localSeriesStartDate=param;
                                    

                               }
                            

                        /**
                        * field for RecurrenceList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrenceList localRecurrenceList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceListTracker = false ;

                           public boolean isRecurrenceListSpecified(){
                               return localRecurrenceListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrenceList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrenceList getRecurrenceList(){
                               return localRecurrenceList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrenceList
                               */
                               public void setRecurrenceList(com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrenceList param){
                            localRecurrenceListTracker = param != null;
                                   
                                            this.localRecurrenceList=param;
                                    

                               }
                            

                        /**
                        * field for MilestoneList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestoneList localMilestoneList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMilestoneListTracker = false ;

                           public boolean isMilestoneListSpecified(){
                               return localMilestoneListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestoneList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestoneList getMilestoneList(){
                               return localMilestoneList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MilestoneList
                               */
                               public void setMilestoneList(com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestoneList param){
                            localMilestoneListTracker = param != null;
                                   
                                            this.localMilestoneList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":BillingSchedule",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "BillingSchedule",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localScheduleTypeTracker){
                                            if (localScheduleType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("scheduleType cannot be null!!");
                                            }
                                           localScheduleType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","scheduleType"),
                                               xmlWriter);
                                        } if (localNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "name", xmlWriter);
                             

                                          if (localName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurrencePatternTracker){
                                            if (localRecurrencePattern==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrencePattern cannot be null!!");
                                            }
                                           localRecurrencePattern.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrencePattern"),
                                               xmlWriter);
                                        } if (localProjectTracker){
                                            if (localProject==null){
                                                 throw new org.apache.axis2.databinding.ADBException("project cannot be null!!");
                                            }
                                           localProject.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","project"),
                                               xmlWriter);
                                        } if (localInitialAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "initialAmount", xmlWriter);
                             

                                          if (localInitialAmount==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("initialAmount cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localInitialAmount);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInitialTermsTracker){
                                            if (localInitialTerms==null){
                                                 throw new org.apache.axis2.databinding.ADBException("initialTerms cannot be null!!");
                                            }
                                           localInitialTerms.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","initialTerms"),
                                               xmlWriter);
                                        } if (localFrequencyTracker){
                                            if (localFrequency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("frequency cannot be null!!");
                                            }
                                           localFrequency.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","frequency"),
                                               xmlWriter);
                                        } if (localRecurrenceDowMaskListTracker){
                                            if (localRecurrenceDowMaskList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrenceDowMaskList cannot be null!!");
                                            }
                                           localRecurrenceDowMaskList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrenceDowMaskList"),
                                               xmlWriter);
                                        } if (localYearModeTracker){
                                            if (localYearMode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("yearMode cannot be null!!");
                                            }
                                           localYearMode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearMode"),
                                               xmlWriter);
                                        } if (localYearDowimTracker){
                                            if (localYearDowim==null){
                                                 throw new org.apache.axis2.databinding.ADBException("yearDowim cannot be null!!");
                                            }
                                           localYearDowim.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDowim"),
                                               xmlWriter);
                                        } if (localYearDowTracker){
                                            if (localYearDow==null){
                                                 throw new org.apache.axis2.databinding.ADBException("yearDow cannot be null!!");
                                            }
                                           localYearDow.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDow"),
                                               xmlWriter);
                                        } if (localYearDowimMonthTracker){
                                            if (localYearDowimMonth==null){
                                                 throw new org.apache.axis2.databinding.ADBException("yearDowimMonth cannot be null!!");
                                            }
                                           localYearDowimMonth.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDowimMonth"),
                                               xmlWriter);
                                        } if (localYearMonthTracker){
                                            if (localYearMonth==null){
                                                 throw new org.apache.axis2.databinding.ADBException("yearMonth cannot be null!!");
                                            }
                                           localYearMonth.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearMonth"),
                                               xmlWriter);
                                        } if (localYearDomTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "yearDom", xmlWriter);
                             
                                               if (localYearDom==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("yearDom cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localYearDom));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMonthModeTracker){
                                            if (localMonthMode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("monthMode cannot be null!!");
                                            }
                                           localMonthMode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthMode"),
                                               xmlWriter);
                                        } if (localMonthDowimTracker){
                                            if (localMonthDowim==null){
                                                 throw new org.apache.axis2.databinding.ADBException("monthDowim cannot be null!!");
                                            }
                                           localMonthDowim.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthDowim"),
                                               xmlWriter);
                                        } if (localMonthDowTracker){
                                            if (localMonthDow==null){
                                                 throw new org.apache.axis2.databinding.ADBException("monthDow cannot be null!!");
                                            }
                                           localMonthDow.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthDow"),
                                               xmlWriter);
                                        } if (localMonthDomTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "monthDom", xmlWriter);
                             
                                               if (localMonthDom==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("monthDom cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMonthDom));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDayPeriodTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dayPeriod", xmlWriter);
                             
                                               if (localDayPeriod==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("dayPeriod cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDayPeriod));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRepeatEveryTracker){
                                            if (localRepeatEvery==null){
                                                 throw new org.apache.axis2.databinding.ADBException("repeatEvery cannot be null!!");
                                            }
                                           localRepeatEvery.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","repeatEvery"),
                                               xmlWriter);
                                        } if (localBillForActualsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "billForActuals", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("billForActuals cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBillForActuals));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNumberRemainingTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberRemaining", xmlWriter);
                             
                                               if (localNumberRemaining==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("numberRemaining cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberRemaining));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInArrearsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "inArrears", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("inArrears cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInArrears));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurrenceTermsTracker){
                                            if (localRecurrenceTerms==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrenceTerms cannot be null!!");
                                            }
                                           localRecurrenceTerms.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrenceTerms"),
                                               xmlWriter);
                                        } if (localIsPublicTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isPublic", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isPublic cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsPublic));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localApplyToSubtotalTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "applyToSubtotal", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("applyToSubtotal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApplyToSubtotal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTransactionTracker){
                                            if (localTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                            }
                                           localTransaction.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","transaction"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSeriesStartDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "seriesStartDate", xmlWriter);
                             

                                          if (localSeriesStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("seriesStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeriesStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurrenceListTracker){
                                            if (localRecurrenceList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrenceList cannot be null!!");
                                            }
                                           localRecurrenceList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrenceList"),
                                               xmlWriter);
                                        } if (localMilestoneListTracker){
                                            if (localMilestoneList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("milestoneList cannot be null!!");
                                            }
                                           localMilestoneList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","milestoneList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","BillingSchedule"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localScheduleTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "scheduleType"));
                            
                            
                                    if (localScheduleType==null){
                                         throw new org.apache.axis2.databinding.ADBException("scheduleType cannot be null!!");
                                    }
                                    elementList.add(localScheduleType);
                                } if (localNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "name"));
                                 
                                        if (localName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        }
                                    } if (localRecurrencePatternTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "recurrencePattern"));
                            
                            
                                    if (localRecurrencePattern==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrencePattern cannot be null!!");
                                    }
                                    elementList.add(localRecurrencePattern);
                                } if (localProjectTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "project"));
                            
                            
                                    if (localProject==null){
                                         throw new org.apache.axis2.databinding.ADBException("project cannot be null!!");
                                    }
                                    elementList.add(localProject);
                                } if (localInitialAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "initialAmount"));
                                 
                                        if (localInitialAmount != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInitialAmount));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("initialAmount cannot be null!!");
                                        }
                                    } if (localInitialTermsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "initialTerms"));
                            
                            
                                    if (localInitialTerms==null){
                                         throw new org.apache.axis2.databinding.ADBException("initialTerms cannot be null!!");
                                    }
                                    elementList.add(localInitialTerms);
                                } if (localFrequencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "frequency"));
                            
                            
                                    if (localFrequency==null){
                                         throw new org.apache.axis2.databinding.ADBException("frequency cannot be null!!");
                                    }
                                    elementList.add(localFrequency);
                                } if (localRecurrenceDowMaskListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "recurrenceDowMaskList"));
                            
                            
                                    if (localRecurrenceDowMaskList==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrenceDowMaskList cannot be null!!");
                                    }
                                    elementList.add(localRecurrenceDowMaskList);
                                } if (localYearModeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "yearMode"));
                            
                            
                                    if (localYearMode==null){
                                         throw new org.apache.axis2.databinding.ADBException("yearMode cannot be null!!");
                                    }
                                    elementList.add(localYearMode);
                                } if (localYearDowimTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "yearDowim"));
                            
                            
                                    if (localYearDowim==null){
                                         throw new org.apache.axis2.databinding.ADBException("yearDowim cannot be null!!");
                                    }
                                    elementList.add(localYearDowim);
                                } if (localYearDowTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "yearDow"));
                            
                            
                                    if (localYearDow==null){
                                         throw new org.apache.axis2.databinding.ADBException("yearDow cannot be null!!");
                                    }
                                    elementList.add(localYearDow);
                                } if (localYearDowimMonthTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "yearDowimMonth"));
                            
                            
                                    if (localYearDowimMonth==null){
                                         throw new org.apache.axis2.databinding.ADBException("yearDowimMonth cannot be null!!");
                                    }
                                    elementList.add(localYearDowimMonth);
                                } if (localYearMonthTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "yearMonth"));
                            
                            
                                    if (localYearMonth==null){
                                         throw new org.apache.axis2.databinding.ADBException("yearMonth cannot be null!!");
                                    }
                                    elementList.add(localYearMonth);
                                } if (localYearDomTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "yearDom"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localYearDom));
                            } if (localMonthModeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "monthMode"));
                            
                            
                                    if (localMonthMode==null){
                                         throw new org.apache.axis2.databinding.ADBException("monthMode cannot be null!!");
                                    }
                                    elementList.add(localMonthMode);
                                } if (localMonthDowimTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "monthDowim"));
                            
                            
                                    if (localMonthDowim==null){
                                         throw new org.apache.axis2.databinding.ADBException("monthDowim cannot be null!!");
                                    }
                                    elementList.add(localMonthDowim);
                                } if (localMonthDowTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "monthDow"));
                            
                            
                                    if (localMonthDow==null){
                                         throw new org.apache.axis2.databinding.ADBException("monthDow cannot be null!!");
                                    }
                                    elementList.add(localMonthDow);
                                } if (localMonthDomTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "monthDom"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMonthDom));
                            } if (localDayPeriodTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "dayPeriod"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDayPeriod));
                            } if (localRepeatEveryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "repeatEvery"));
                            
                            
                                    if (localRepeatEvery==null){
                                         throw new org.apache.axis2.databinding.ADBException("repeatEvery cannot be null!!");
                                    }
                                    elementList.add(localRepeatEvery);
                                } if (localBillForActualsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "billForActuals"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBillForActuals));
                            } if (localNumberRemainingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "numberRemaining"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberRemaining));
                            } if (localInArrearsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "inArrears"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInArrears));
                            } if (localRecurrenceTermsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "recurrenceTerms"));
                            
                            
                                    if (localRecurrenceTerms==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrenceTerms cannot be null!!");
                                    }
                                    elementList.add(localRecurrenceTerms);
                                } if (localIsPublicTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isPublic"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsPublic));
                            } if (localApplyToSubtotalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "applyToSubtotal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApplyToSubtotal));
                            } if (localTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "transaction"));
                            
                            
                                    if (localTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                    }
                                    elementList.add(localTransaction);
                                } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localSeriesStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "seriesStartDate"));
                                 
                                        if (localSeriesStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeriesStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("seriesStartDate cannot be null!!");
                                        }
                                    } if (localRecurrenceListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "recurrenceList"));
                            
                            
                                    if (localRecurrenceList==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrenceList cannot be null!!");
                                    }
                                    elementList.add(localRecurrenceList);
                                } if (localMilestoneListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "milestoneList"));
                            
                            
                                    if (localMilestoneList==null){
                                         throw new org.apache.axis2.databinding.ADBException("milestoneList cannot be null!!");
                                    }
                                    elementList.add(localMilestoneList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static BillingSchedule parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            BillingSchedule object =
                new BillingSchedule();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"BillingSchedule".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (BillingSchedule)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","scheduleType").equals(reader.getName())){
                                
                                                object.setScheduleType(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"name" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrencePattern").equals(reader.getName())){
                                
                                                object.setRecurrencePattern(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrencePattern.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","project").equals(reader.getName())){
                                
                                                object.setProject(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","initialAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"initialAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInitialAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","initialTerms").equals(reader.getName())){
                                
                                                object.setInitialTerms(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","frequency").equals(reader.getName())){
                                
                                                object.setFrequency(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleFrequency.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrenceDowMaskList").equals(reader.getName())){
                                
                                                object.setRecurrenceDowMaskList(com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearMode").equals(reader.getName())){
                                
                                                object.setYearMode(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDowim").equals(reader.getName())){
                                
                                                object.setYearDowim(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowim.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDow").equals(reader.getName())){
                                
                                                object.setYearDow(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDow.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDowimMonth").equals(reader.getName())){
                                
                                                object.setYearDowimMonth(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowimMonth.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearMonth").equals(reader.getName())){
                                
                                                object.setYearMonth(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearMonth.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","yearDom").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"yearDom" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setYearDom(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setYearDom(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthMode").equals(reader.getName())){
                                
                                                object.setMonthMode(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthDowim").equals(reader.getName())){
                                
                                                object.setMonthDowim(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDowim.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthDow").equals(reader.getName())){
                                
                                                object.setMonthDow(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDow.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","monthDom").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"monthDom" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMonthDom(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMonthDom(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","dayPeriod").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dayPeriod" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDayPeriod(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDayPeriod(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","repeatEvery").equals(reader.getName())){
                                
                                                object.setRepeatEvery(com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRepeatEvery.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billForActuals").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"billForActuals" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBillForActuals(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","numberRemaining").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberRemaining" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberRemaining(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setNumberRemaining(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inArrears").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"inArrears" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInArrears(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrenceTerms").equals(reader.getName())){
                                
                                                object.setRecurrenceTerms(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isPublic").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isPublic" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsPublic(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","applyToSubtotal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"applyToSubtotal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setApplyToSubtotal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","transaction").equals(reader.getName())){
                                
                                                object.setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","seriesStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"seriesStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSeriesStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","recurrenceList").equals(reader.getName())){
                                
                                                object.setRecurrenceList(com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrenceList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","milestoneList").equals(reader.getName())){
                                
                                                object.setMilestoneList(com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestoneList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    