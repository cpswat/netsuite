
/**
 * Location.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  Location bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Location extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Location
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for Name
                        */

                        
                                    protected java.lang.String localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(java.lang.String param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for IncludeChildren
                        */

                        
                                    protected boolean localIncludeChildren ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncludeChildrenTracker = false ;

                           public boolean isIncludeChildrenSpecified(){
                               return localIncludeChildrenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIncludeChildren(){
                               return localIncludeChildren;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncludeChildren
                               */
                               public void setIncludeChildren(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIncludeChildrenTracker =
                                       true;
                                   
                                            this.localIncludeChildren=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localSubsidiaryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryListTracker = false ;

                           public boolean isSubsidiaryListSpecified(){
                               return localSubsidiaryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getSubsidiaryList(){
                               return localSubsidiaryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryList
                               */
                               public void setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localSubsidiaryListTracker = param != null;
                                   
                                            this.localSubsidiaryList=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for TranPrefix
                        */

                        
                                    protected java.lang.String localTranPrefix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranPrefixTracker = false ;

                           public boolean isTranPrefixSpecified(){
                               return localTranPrefixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTranPrefix(){
                               return localTranPrefix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranPrefix
                               */
                               public void setTranPrefix(java.lang.String param){
                            localTranPrefixTracker = param != null;
                                   
                                            this.localTranPrefix=param;
                                    

                               }
                            

                        /**
                        * field for MainAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.Address localMainAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMainAddressTracker = false ;

                           public boolean isMainAddressSpecified(){
                               return localMainAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.Address
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.Address getMainAddress(){
                               return localMainAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MainAddress
                               */
                               public void setMainAddress(com.netsuite.webservices.platform.common_2017_2.Address param){
                            localMainAddressTracker = param != null;
                                   
                                            this.localMainAddress=param;
                                    

                               }
                            

                        /**
                        * field for ReturnAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.Address localReturnAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReturnAddressTracker = false ;

                           public boolean isReturnAddressSpecified(){
                               return localReturnAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.Address
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.Address getReturnAddress(){
                               return localReturnAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReturnAddress
                               */
                               public void setReturnAddress(com.netsuite.webservices.platform.common_2017_2.Address param){
                            localReturnAddressTracker = param != null;
                                   
                                            this.localReturnAddress=param;
                                    

                               }
                            

                        /**
                        * field for LocationType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.LocationType localLocationType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTypeTracker = false ;

                           public boolean isLocationTypeSpecified(){
                               return localLocationTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.LocationType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.LocationType getLocationType(){
                               return localLocationType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationType
                               */
                               public void setLocationType(com.netsuite.webservices.lists.accounting_2017_2.types.LocationType param){
                            localLocationTypeTracker = param != null;
                                   
                                            this.localLocationType=param;
                                    

                               }
                            

                        /**
                        * field for TimeZone
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.LocationTimeZone localTimeZone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeZoneTracker = false ;

                           public boolean isTimeZoneSpecified(){
                               return localTimeZoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.LocationTimeZone
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.LocationTimeZone getTimeZone(){
                               return localTimeZone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeZone
                               */
                               public void setTimeZone(com.netsuite.webservices.lists.accounting_2017_2.types.LocationTimeZone param){
                            localTimeZoneTracker = param != null;
                                   
                                            this.localTimeZone=param;
                                    

                               }
                            

                        /**
                        * field for Latitude
                        */

                        
                                    protected double localLatitude ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLatitudeTracker = false ;

                           public boolean isLatitudeSpecified(){
                               return localLatitudeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLatitude(){
                               return localLatitude;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Latitude
                               */
                               public void setLatitude(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLatitudeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLatitude=param;
                                    

                               }
                            

                        /**
                        * field for Longitude
                        */

                        
                                    protected double localLongitude ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLongitudeTracker = false ;

                           public boolean isLongitudeSpecified(){
                               return localLongitudeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLongitude(){
                               return localLongitude;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Longitude
                               */
                               public void setLongitude(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLongitudeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLongitude=param;
                                    

                               }
                            

                        /**
                        * field for Logo
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLogo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLogoTracker = false ;

                           public boolean isLogoSpecified(){
                               return localLogoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLogo(){
                               return localLogo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Logo
                               */
                               public void setLogo(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLogoTracker = param != null;
                                   
                                            this.localLogo=param;
                                    

                               }
                            

                        /**
                        * field for UseBins
                        */

                        
                                    protected boolean localUseBins ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseBinsTracker = false ;

                           public boolean isUseBinsSpecified(){
                               return localUseBinsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseBins(){
                               return localUseBins;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseBins
                               */
                               public void setUseBins(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseBinsTracker =
                                       true;
                                   
                                            this.localUseBins=param;
                                    

                               }
                            

                        /**
                        * field for MakeInventoryAvailable
                        */

                        
                                    protected boolean localMakeInventoryAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMakeInventoryAvailableTracker = false ;

                           public boolean isMakeInventoryAvailableSpecified(){
                               return localMakeInventoryAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getMakeInventoryAvailable(){
                               return localMakeInventoryAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MakeInventoryAvailable
                               */
                               public void setMakeInventoryAvailable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localMakeInventoryAvailableTracker =
                                       true;
                                   
                                            this.localMakeInventoryAvailable=param;
                                    

                               }
                            

                        /**
                        * field for MakeInventoryAvailableStore
                        */

                        
                                    protected boolean localMakeInventoryAvailableStore ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMakeInventoryAvailableStoreTracker = false ;

                           public boolean isMakeInventoryAvailableStoreSpecified(){
                               return localMakeInventoryAvailableStoreTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getMakeInventoryAvailableStore(){
                               return localMakeInventoryAvailableStore;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MakeInventoryAvailableStore
                               */
                               public void setMakeInventoryAvailableStore(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localMakeInventoryAvailableStoreTracker =
                                       true;
                                   
                                            this.localMakeInventoryAvailableStore=param;
                                    

                               }
                            

                        /**
                        * field for GeolocationMethod
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.LocationGeolocationMethod localGeolocationMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGeolocationMethodTracker = false ;

                           public boolean isGeolocationMethodSpecified(){
                               return localGeolocationMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.LocationGeolocationMethod
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.LocationGeolocationMethod getGeolocationMethod(){
                               return localGeolocationMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GeolocationMethod
                               */
                               public void setGeolocationMethod(com.netsuite.webservices.lists.accounting_2017_2.types.LocationGeolocationMethod param){
                            localGeolocationMethodTracker = param != null;
                                   
                                            this.localGeolocationMethod=param;
                                    

                               }
                            

                        /**
                        * field for AutoAssignmentRegionSetting
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.LocationAutoAssignmentRegionSetting localAutoAssignmentRegionSetting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAutoAssignmentRegionSettingTracker = false ;

                           public boolean isAutoAssignmentRegionSettingSpecified(){
                               return localAutoAssignmentRegionSettingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.LocationAutoAssignmentRegionSetting
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.LocationAutoAssignmentRegionSetting getAutoAssignmentRegionSetting(){
                               return localAutoAssignmentRegionSetting;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AutoAssignmentRegionSetting
                               */
                               public void setAutoAssignmentRegionSetting(com.netsuite.webservices.lists.accounting_2017_2.types.LocationAutoAssignmentRegionSetting param){
                            localAutoAssignmentRegionSettingTracker = param != null;
                                   
                                            this.localAutoAssignmentRegionSetting=param;
                                    

                               }
                            

                        /**
                        * field for NextPickupCutOffTime
                        */

                        
                                    protected java.util.Calendar localNextPickupCutOffTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextPickupCutOffTimeTracker = false ;

                           public boolean isNextPickupCutOffTimeSpecified(){
                               return localNextPickupCutOffTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getNextPickupCutOffTime(){
                               return localNextPickupCutOffTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextPickupCutOffTime
                               */
                               public void setNextPickupCutOffTime(java.util.Calendar param){
                            localNextPickupCutOffTimeTracker = param != null;
                                   
                                            this.localNextPickupCutOffTime=param;
                                    

                               }
                            

                        /**
                        * field for BufferStock
                        */

                        
                                    protected long localBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBufferStockTracker = false ;

                           public boolean isBufferStockSpecified(){
                               return localBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBufferStock(){
                               return localBufferStock;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BufferStock
                               */
                               public void setBufferStock(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localBufferStockTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localBufferStock=param;
                                    

                               }
                            

                        /**
                        * field for AllowStorePickup
                        */

                        
                                    protected boolean localAllowStorePickup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowStorePickupTracker = false ;

                           public boolean isAllowStorePickupSpecified(){
                               return localAllowStorePickupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAllowStorePickup(){
                               return localAllowStorePickup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllowStorePickup
                               */
                               public void setAllowStorePickup(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllowStorePickupTracker =
                                       true;
                                   
                                            this.localAllowStorePickup=param;
                                    

                               }
                            

                        /**
                        * field for StorePickupBufferStock
                        */

                        
                                    protected double localStorePickupBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStorePickupBufferStockTracker = false ;

                           public boolean isStorePickupBufferStockSpecified(){
                               return localStorePickupBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getStorePickupBufferStock(){
                               return localStorePickupBufferStock;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StorePickupBufferStock
                               */
                               public void setStorePickupBufferStock(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localStorePickupBufferStockTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localStorePickupBufferStock=param;
                                    

                               }
                            

                        /**
                        * field for DailyShippingCapacity
                        */

                        
                                    protected long localDailyShippingCapacity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDailyShippingCapacityTracker = false ;

                           public boolean isDailyShippingCapacitySpecified(){
                               return localDailyShippingCapacityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getDailyShippingCapacity(){
                               return localDailyShippingCapacity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DailyShippingCapacity
                               */
                               public void setDailyShippingCapacity(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localDailyShippingCapacityTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localDailyShippingCapacity=param;
                                    

                               }
                            

                        /**
                        * field for TotalShippingCapacity
                        */

                        
                                    protected long localTotalShippingCapacity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalShippingCapacityTracker = false ;

                           public boolean isTotalShippingCapacitySpecified(){
                               return localTotalShippingCapacityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getTotalShippingCapacity(){
                               return localTotalShippingCapacity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TotalShippingCapacity
                               */
                               public void setTotalShippingCapacity(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localTotalShippingCapacityTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localTotalShippingCapacity=param;
                                    

                               }
                            

                        /**
                        * field for IncludeLocationRegionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList localIncludeLocationRegionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncludeLocationRegionsListTracker = false ;

                           public boolean isIncludeLocationRegionsListSpecified(){
                               return localIncludeLocationRegionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList getIncludeLocationRegionsList(){
                               return localIncludeLocationRegionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncludeLocationRegionsList
                               */
                               public void setIncludeLocationRegionsList(com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList param){
                            localIncludeLocationRegionsListTracker = param != null;
                                   
                                            this.localIncludeLocationRegionsList=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeLocationRegionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList localExcludeLocationRegionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeLocationRegionsListTracker = false ;

                           public boolean isExcludeLocationRegionsListSpecified(){
                               return localExcludeLocationRegionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList getExcludeLocationRegionsList(){
                               return localExcludeLocationRegionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeLocationRegionsList
                               */
                               public void setExcludeLocationRegionsList(com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList param){
                            localExcludeLocationRegionsListTracker = param != null;
                                   
                                            this.localExcludeLocationRegionsList=param;
                                    

                               }
                            

                        /**
                        * field for BusinessHoursList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHoursList localBusinessHoursList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBusinessHoursListTracker = false ;

                           public boolean isBusinessHoursListSpecified(){
                               return localBusinessHoursListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHoursList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHoursList getBusinessHoursList(){
                               return localBusinessHoursList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BusinessHoursList
                               */
                               public void setBusinessHoursList(com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHoursList param){
                            localBusinessHoursListTracker = param != null;
                                   
                                            this.localBusinessHoursList=param;
                                    

                               }
                            

                        /**
                        * field for ClassTranslationList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ClassTranslationList localClassTranslationList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClassTranslationListTracker = false ;

                           public boolean isClassTranslationListSpecified(){
                               return localClassTranslationListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ClassTranslationList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ClassTranslationList getClassTranslationList(){
                               return localClassTranslationList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClassTranslationList
                               */
                               public void setClassTranslationList(com.netsuite.webservices.lists.accounting_2017_2.ClassTranslationList param){
                            localClassTranslationListTracker = param != null;
                                   
                                            this.localClassTranslationList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Location",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Location",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "name", xmlWriter);
                             

                                          if (localName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localIncludeChildrenTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "includeChildren", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("includeChildren cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubsidiaryListTracker){
                                            if (localSubsidiaryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                            }
                                           localSubsidiaryList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranPrefixTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "tranPrefix", xmlWriter);
                             

                                          if (localTranPrefix==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tranPrefix cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTranPrefix);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMainAddressTracker){
                                            if (localMainAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("mainAddress cannot be null!!");
                                            }
                                           localMainAddress.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","mainAddress"),
                                               xmlWriter);
                                        } if (localReturnAddressTracker){
                                            if (localReturnAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("returnAddress cannot be null!!");
                                            }
                                           localReturnAddress.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","returnAddress"),
                                               xmlWriter);
                                        } if (localLocationTypeTracker){
                                            if (localLocationType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationType cannot be null!!");
                                            }
                                           localLocationType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationType"),
                                               xmlWriter);
                                        } if (localTimeZoneTracker){
                                            if (localTimeZone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeZone cannot be null!!");
                                            }
                                           localTimeZone.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","timeZone"),
                                               xmlWriter);
                                        } if (localLatitudeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "latitude", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLatitude)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("latitude cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitude));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLongitudeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "longitude", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLongitude)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("longitude cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitude));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLogoTracker){
                                            if (localLogo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("logo cannot be null!!");
                                            }
                                           localLogo.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","logo"),
                                               xmlWriter);
                                        } if (localUseBinsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useBins", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useBins cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseBins));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMakeInventoryAvailableTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "makeInventoryAvailable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMakeInventoryAvailable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMakeInventoryAvailableStoreTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "makeInventoryAvailableStore", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailableStore cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMakeInventoryAvailableStore));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGeolocationMethodTracker){
                                            if (localGeolocationMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("geolocationMethod cannot be null!!");
                                            }
                                           localGeolocationMethod.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","geolocationMethod"),
                                               xmlWriter);
                                        } if (localAutoAssignmentRegionSettingTracker){
                                            if (localAutoAssignmentRegionSetting==null){
                                                 throw new org.apache.axis2.databinding.ADBException("autoAssignmentRegionSetting cannot be null!!");
                                            }
                                           localAutoAssignmentRegionSetting.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","autoAssignmentRegionSetting"),
                                               xmlWriter);
                                        } if (localNextPickupCutOffTimeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "nextPickupCutOffTime", xmlWriter);
                             

                                          if (localNextPickupCutOffTime==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("nextPickupCutOffTime cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextPickupCutOffTime));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBufferStockTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "bufferStock", xmlWriter);
                             
                                               if (localBufferStock==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("bufferStock cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBufferStock));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllowStorePickupTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allowStorePickup", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allowStorePickup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowStorePickup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStorePickupBufferStockTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storePickupBufferStock", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localStorePickupBufferStock)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("storePickupBufferStock cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStorePickupBufferStock));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDailyShippingCapacityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dailyShippingCapacity", xmlWriter);
                             
                                               if (localDailyShippingCapacity==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("dailyShippingCapacity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDailyShippingCapacity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTotalShippingCapacityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "totalShippingCapacity", xmlWriter);
                             
                                               if (localTotalShippingCapacity==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("totalShippingCapacity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotalShippingCapacity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncludeLocationRegionsListTracker){
                                            if (localIncludeLocationRegionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("includeLocationRegionsList cannot be null!!");
                                            }
                                           localIncludeLocationRegionsList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","includeLocationRegionsList"),
                                               xmlWriter);
                                        } if (localExcludeLocationRegionsListTracker){
                                            if (localExcludeLocationRegionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("excludeLocationRegionsList cannot be null!!");
                                            }
                                           localExcludeLocationRegionsList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","excludeLocationRegionsList"),
                                               xmlWriter);
                                        } if (localBusinessHoursListTracker){
                                            if (localBusinessHoursList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("businessHoursList cannot be null!!");
                                            }
                                           localBusinessHoursList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","businessHoursList"),
                                               xmlWriter);
                                        } if (localClassTranslationListTracker){
                                            if (localClassTranslationList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("classTranslationList cannot be null!!");
                                            }
                                           localClassTranslationList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","classTranslationList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","Location"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "name"));
                                 
                                        if (localName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        }
                                    } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localIncludeChildrenTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "includeChildren"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                            } if (localSubsidiaryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiaryList"));
                            
                            
                                    if (localSubsidiaryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryList);
                                } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localTranPrefixTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "tranPrefix"));
                                 
                                        if (localTranPrefix != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTranPrefix));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tranPrefix cannot be null!!");
                                        }
                                    } if (localMainAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "mainAddress"));
                            
                            
                                    if (localMainAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("mainAddress cannot be null!!");
                                    }
                                    elementList.add(localMainAddress);
                                } if (localReturnAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "returnAddress"));
                            
                            
                                    if (localReturnAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("returnAddress cannot be null!!");
                                    }
                                    elementList.add(localReturnAddress);
                                } if (localLocationTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "locationType"));
                            
                            
                                    if (localLocationType==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationType cannot be null!!");
                                    }
                                    elementList.add(localLocationType);
                                } if (localTimeZoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "timeZone"));
                            
                            
                                    if (localTimeZone==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeZone cannot be null!!");
                                    }
                                    elementList.add(localTimeZone);
                                } if (localLatitudeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "latitude"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLatitude));
                            } if (localLongitudeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "longitude"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLongitude));
                            } if (localLogoTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "logo"));
                            
                            
                                    if (localLogo==null){
                                         throw new org.apache.axis2.databinding.ADBException("logo cannot be null!!");
                                    }
                                    elementList.add(localLogo);
                                } if (localUseBinsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "useBins"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseBins));
                            } if (localMakeInventoryAvailableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "makeInventoryAvailable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMakeInventoryAvailable));
                            } if (localMakeInventoryAvailableStoreTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "makeInventoryAvailableStore"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMakeInventoryAvailableStore));
                            } if (localGeolocationMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "geolocationMethod"));
                            
                            
                                    if (localGeolocationMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("geolocationMethod cannot be null!!");
                                    }
                                    elementList.add(localGeolocationMethod);
                                } if (localAutoAssignmentRegionSettingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "autoAssignmentRegionSetting"));
                            
                            
                                    if (localAutoAssignmentRegionSetting==null){
                                         throw new org.apache.axis2.databinding.ADBException("autoAssignmentRegionSetting cannot be null!!");
                                    }
                                    elementList.add(localAutoAssignmentRegionSetting);
                                } if (localNextPickupCutOffTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "nextPickupCutOffTime"));
                                 
                                        if (localNextPickupCutOffTime != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextPickupCutOffTime));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("nextPickupCutOffTime cannot be null!!");
                                        }
                                    } if (localBufferStockTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "bufferStock"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBufferStock));
                            } if (localAllowStorePickupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "allowStorePickup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowStorePickup));
                            } if (localStorePickupBufferStockTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storePickupBufferStock"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStorePickupBufferStock));
                            } if (localDailyShippingCapacityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "dailyShippingCapacity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDailyShippingCapacity));
                            } if (localTotalShippingCapacityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "totalShippingCapacity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTotalShippingCapacity));
                            } if (localIncludeLocationRegionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "includeLocationRegionsList"));
                            
                            
                                    if (localIncludeLocationRegionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("includeLocationRegionsList cannot be null!!");
                                    }
                                    elementList.add(localIncludeLocationRegionsList);
                                } if (localExcludeLocationRegionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "excludeLocationRegionsList"));
                            
                            
                                    if (localExcludeLocationRegionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("excludeLocationRegionsList cannot be null!!");
                                    }
                                    elementList.add(localExcludeLocationRegionsList);
                                } if (localBusinessHoursListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "businessHoursList"));
                            
                            
                                    if (localBusinessHoursList==null){
                                         throw new org.apache.axis2.databinding.ADBException("businessHoursList cannot be null!!");
                                    }
                                    elementList.add(localBusinessHoursList);
                                } if (localClassTranslationListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "classTranslationList"));
                            
                            
                                    if (localClassTranslationList==null){
                                         throw new org.apache.axis2.databinding.ADBException("classTranslationList cannot be null!!");
                                    }
                                    elementList.add(localClassTranslationList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Location parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Location object =
                new Location();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Location".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Location)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"name" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","includeChildren").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"includeChildren" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncludeChildren(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList").equals(reader.getName())){
                                
                                                object.setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","tranPrefix").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"tranPrefix" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTranPrefix(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","mainAddress").equals(reader.getName())){
                                
                                                object.setMainAddress(com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","returnAddress").equals(reader.getName())){
                                
                                                object.setReturnAddress(com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","locationType").equals(reader.getName())){
                                
                                                object.setLocationType(com.netsuite.webservices.lists.accounting_2017_2.types.LocationType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","timeZone").equals(reader.getName())){
                                
                                                object.setTimeZone(com.netsuite.webservices.lists.accounting_2017_2.types.LocationTimeZone.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","latitude").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"latitude" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLatitude(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLatitude(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","longitude").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"longitude" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLongitude(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLongitude(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","logo").equals(reader.getName())){
                                
                                                object.setLogo(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","useBins").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useBins" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseBins(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","makeInventoryAvailable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"makeInventoryAvailable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMakeInventoryAvailable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","makeInventoryAvailableStore").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"makeInventoryAvailableStore" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMakeInventoryAvailableStore(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","geolocationMethod").equals(reader.getName())){
                                
                                                object.setGeolocationMethod(com.netsuite.webservices.lists.accounting_2017_2.types.LocationGeolocationMethod.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","autoAssignmentRegionSetting").equals(reader.getName())){
                                
                                                object.setAutoAssignmentRegionSetting(com.netsuite.webservices.lists.accounting_2017_2.types.LocationAutoAssignmentRegionSetting.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","nextPickupCutOffTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"nextPickupCutOffTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNextPickupCutOffTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","bufferStock").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"bufferStock" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBufferStock(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBufferStock(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","allowStorePickup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allowStorePickup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllowStorePickup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storePickupBufferStock").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storePickupBufferStock" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStorePickupBufferStock(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setStorePickupBufferStock(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","dailyShippingCapacity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dailyShippingCapacity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDailyShippingCapacity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDailyShippingCapacity(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","totalShippingCapacity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"totalShippingCapacity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTotalShippingCapacity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setTotalShippingCapacity(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","includeLocationRegionsList").equals(reader.getName())){
                                
                                                object.setIncludeLocationRegionsList(com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","excludeLocationRegionsList").equals(reader.getName())){
                                
                                                object.setExcludeLocationRegionsList(com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","businessHoursList").equals(reader.getName())){
                                
                                                object.setBusinessHoursList(com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHoursList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","classTranslationList").equals(reader.getName())){
                                
                                                object.setClassTranslationList(com.netsuite.webservices.lists.accounting_2017_2.ClassTranslationList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    