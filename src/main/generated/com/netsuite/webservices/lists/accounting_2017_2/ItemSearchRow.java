
/**
 * ItemSearchRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  ItemSearchRow bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemSearchRow extends com.netsuite.webservices.platform.core_2017_2.SearchRow
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemSearchRow
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for AssemblyItemBillOfMaterialsJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchRowBasic localAssemblyItemBillOfMaterialsJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssemblyItemBillOfMaterialsJoinTracker = false ;

                           public boolean isAssemblyItemBillOfMaterialsJoinSpecified(){
                               return localAssemblyItemBillOfMaterialsJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchRowBasic getAssemblyItemBillOfMaterialsJoin(){
                               return localAssemblyItemBillOfMaterialsJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AssemblyItemBillOfMaterialsJoin
                               */
                               public void setAssemblyItemBillOfMaterialsJoin(com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchRowBasic param){
                            localAssemblyItemBillOfMaterialsJoinTracker = param != null;
                                   
                                            this.localAssemblyItemBillOfMaterialsJoin=param;
                                    

                               }
                            

                        /**
                        * field for BinNumberJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.BinSearchRowBasic localBinNumberJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBinNumberJoinTracker = false ;

                           public boolean isBinNumberJoinSpecified(){
                               return localBinNumberJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.BinSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.BinSearchRowBasic getBinNumberJoin(){
                               return localBinNumberJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BinNumberJoin
                               */
                               public void setBinNumberJoin(com.netsuite.webservices.platform.common_2017_2.BinSearchRowBasic param){
                            localBinNumberJoinTracker = param != null;
                                   
                                            this.localBinNumberJoin=param;
                                    

                               }
                            

                        /**
                        * field for BinOnHandJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchRowBasic localBinOnHandJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBinOnHandJoinTracker = false ;

                           public boolean isBinOnHandJoinSpecified(){
                               return localBinOnHandJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchRowBasic getBinOnHandJoin(){
                               return localBinOnHandJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BinOnHandJoin
                               */
                               public void setBinOnHandJoin(com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchRowBasic param){
                            localBinOnHandJoinTracker = param != null;
                                   
                                            this.localBinOnHandJoin=param;
                                    

                               }
                            

                        /**
                        * field for CorrelatedItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localCorrelatedItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCorrelatedItemJoinTracker = false ;

                           public boolean isCorrelatedItemJoinSpecified(){
                               return localCorrelatedItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getCorrelatedItemJoin(){
                               return localCorrelatedItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CorrelatedItemJoin
                               */
                               public void setCorrelatedItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localCorrelatedItemJoinTracker = param != null;
                                   
                                            this.localCorrelatedItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for EffectiveRevisionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic localEffectiveRevisionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEffectiveRevisionJoinTracker = false ;

                           public boolean isEffectiveRevisionJoinSpecified(){
                               return localEffectiveRevisionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic getEffectiveRevisionJoin(){
                               return localEffectiveRevisionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EffectiveRevisionJoin
                               */
                               public void setEffectiveRevisionJoin(com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic param){
                            localEffectiveRevisionJoinTracker = param != null;
                                   
                                            this.localEffectiveRevisionJoin=param;
                                    

                               }
                            

                        /**
                        * field for FileJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic localFileJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFileJoinTracker = false ;

                           public boolean isFileJoinSpecified(){
                               return localFileJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic getFileJoin(){
                               return localFileJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FileJoin
                               */
                               public void setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic param){
                            localFileJoinTracker = param != null;
                                   
                                            this.localFileJoin=param;
                                    

                               }
                            

                        /**
                        * field for InventoryDetailJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchRowBasic localInventoryDetailJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryDetailJoinTracker = false ;

                           public boolean isInventoryDetailJoinSpecified(){
                               return localInventoryDetailJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchRowBasic getInventoryDetailJoin(){
                               return localInventoryDetailJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryDetailJoin
                               */
                               public void setInventoryDetailJoin(com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchRowBasic param){
                            localInventoryDetailJoinTracker = param != null;
                                   
                                            this.localInventoryDetailJoin=param;
                                    

                               }
                            

                        /**
                        * field for InventoryLocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic localInventoryLocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryLocationJoinTracker = false ;

                           public boolean isInventoryLocationJoinSpecified(){
                               return localInventoryLocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic getInventoryLocationJoin(){
                               return localInventoryLocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryLocationJoin
                               */
                               public void setInventoryLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic param){
                            localInventoryLocationJoinTracker = param != null;
                                   
                                            this.localInventoryLocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for InventoryNumberJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchRowBasic localInventoryNumberJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryNumberJoinTracker = false ;

                           public boolean isInventoryNumberJoinSpecified(){
                               return localInventoryNumberJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchRowBasic getInventoryNumberJoin(){
                               return localInventoryNumberJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryNumberJoin
                               */
                               public void setInventoryNumberJoin(com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchRowBasic param){
                            localInventoryNumberJoinTracker = param != null;
                                   
                                            this.localInventoryNumberJoin=param;
                                    

                               }
                            

                        /**
                        * field for InventoryNumberBinOnHandJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchRowBasic localInventoryNumberBinOnHandJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInventoryNumberBinOnHandJoinTracker = false ;

                           public boolean isInventoryNumberBinOnHandJoinSpecified(){
                               return localInventoryNumberBinOnHandJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchRowBasic getInventoryNumberBinOnHandJoin(){
                               return localInventoryNumberBinOnHandJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InventoryNumberBinOnHandJoin
                               */
                               public void setInventoryNumberBinOnHandJoin(com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchRowBasic param){
                            localInventoryNumberBinOnHandJoinTracker = param != null;
                                   
                                            this.localInventoryNumberBinOnHandJoin=param;
                                    

                               }
                            

                        /**
                        * field for MemberItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localMemberItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemberItemJoinTracker = false ;

                           public boolean isMemberItemJoinSpecified(){
                               return localMemberItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getMemberItemJoin(){
                               return localMemberItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MemberItemJoin
                               */
                               public void setMemberItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localMemberItemJoinTracker = param != null;
                                   
                                            this.localMemberItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for ObsoleteRevisionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic localObsoleteRevisionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localObsoleteRevisionJoinTracker = false ;

                           public boolean isObsoleteRevisionJoinSpecified(){
                               return localObsoleteRevisionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic getObsoleteRevisionJoin(){
                               return localObsoleteRevisionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ObsoleteRevisionJoin
                               */
                               public void setObsoleteRevisionJoin(com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic param){
                            localObsoleteRevisionJoinTracker = param != null;
                                   
                                            this.localObsoleteRevisionJoin=param;
                                    

                               }
                            

                        /**
                        * field for ParentJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localParentJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentJoinTracker = false ;

                           public boolean isParentJoinSpecified(){
                               return localParentJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getParentJoin(){
                               return localParentJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentJoin
                               */
                               public void setParentJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localParentJoinTracker = param != null;
                                   
                                            this.localParentJoin=param;
                                    

                               }
                            

                        /**
                        * field for PreferredLocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic localPreferredLocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPreferredLocationJoinTracker = false ;

                           public boolean isPreferredLocationJoinSpecified(){
                               return localPreferredLocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic getPreferredLocationJoin(){
                               return localPreferredLocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PreferredLocationJoin
                               */
                               public void setPreferredLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic param){
                            localPreferredLocationJoinTracker = param != null;
                                   
                                            this.localPreferredLocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for PreferredVendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic localPreferredVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPreferredVendorJoinTracker = false ;

                           public boolean isPreferredVendorJoinSpecified(){
                               return localPreferredVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic getPreferredVendorJoin(){
                               return localPreferredVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PreferredVendorJoin
                               */
                               public void setPreferredVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic param){
                            localPreferredVendorJoinTracker = param != null;
                                   
                                            this.localPreferredVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for PricingJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic localPricingJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingJoinTracker = false ;

                           public boolean isPricingJoinSpecified(){
                               return localPricingJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic getPricingJoin(){
                               return localPricingJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingJoin
                               */
                               public void setPricingJoin(com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic param){
                            localPricingJoinTracker = param != null;
                                   
                                            this.localPricingJoin=param;
                                    

                               }
                            

                        /**
                        * field for ShopperJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localShopperJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShopperJoinTracker = false ;

                           public boolean isShopperJoinSpecified(){
                               return localShopperJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getShopperJoin(){
                               return localShopperJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShopperJoin
                               */
                               public void setShopperJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localShopperJoinTracker = param != null;
                                   
                                            this.localShopperJoin=param;
                                    

                               }
                            

                        /**
                        * field for TransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic localTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionJoinTracker = false ;

                           public boolean isTransactionJoinSpecified(){
                               return localTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic getTransactionJoin(){
                               return localTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionJoin
                               */
                               public void setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic param){
                            localTransactionJoinTracker = param != null;
                                   
                                            this.localTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserNotesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic localUserNotesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserNotesJoinTracker = false ;

                           public boolean isUserNotesJoinSpecified(){
                               return localUserNotesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic getUserNotesJoin(){
                               return localUserNotesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserNotesJoin
                               */
                               public void setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic param){
                            localUserNotesJoinTracker = param != null;
                                   
                                            this.localUserNotesJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic localVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorJoinTracker = false ;

                           public boolean isVendorJoinSpecified(){
                               return localVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic getVendorJoin(){
                               return localVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorJoin
                               */
                               public void setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic param){
                            localVendorJoinTracker = param != null;
                                   
                                            this.localVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemSearchRow",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemSearchRow",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localAssemblyItemBillOfMaterialsJoinTracker){
                                            if (localAssemblyItemBillOfMaterialsJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assemblyItemBillOfMaterialsJoin cannot be null!!");
                                            }
                                           localAssemblyItemBillOfMaterialsJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","assemblyItemBillOfMaterialsJoin"),
                                               xmlWriter);
                                        } if (localBinNumberJoinTracker){
                                            if (localBinNumberJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("binNumberJoin cannot be null!!");
                                            }
                                           localBinNumberJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","binNumberJoin"),
                                               xmlWriter);
                                        } if (localBinOnHandJoinTracker){
                                            if (localBinOnHandJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("binOnHandJoin cannot be null!!");
                                            }
                                           localBinOnHandJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","binOnHandJoin"),
                                               xmlWriter);
                                        } if (localCorrelatedItemJoinTracker){
                                            if (localCorrelatedItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("correlatedItemJoin cannot be null!!");
                                            }
                                           localCorrelatedItemJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","correlatedItemJoin"),
                                               xmlWriter);
                                        } if (localEffectiveRevisionJoinTracker){
                                            if (localEffectiveRevisionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("effectiveRevisionJoin cannot be null!!");
                                            }
                                           localEffectiveRevisionJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","effectiveRevisionJoin"),
                                               xmlWriter);
                                        } if (localFileJoinTracker){
                                            if (localFileJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                            }
                                           localFileJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","fileJoin"),
                                               xmlWriter);
                                        } if (localInventoryDetailJoinTracker){
                                            if (localInventoryDetailJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryDetailJoin cannot be null!!");
                                            }
                                           localInventoryDetailJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryDetailJoin"),
                                               xmlWriter);
                                        } if (localInventoryLocationJoinTracker){
                                            if (localInventoryLocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryLocationJoin cannot be null!!");
                                            }
                                           localInventoryLocationJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryLocationJoin"),
                                               xmlWriter);
                                        } if (localInventoryNumberJoinTracker){
                                            if (localInventoryNumberJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryNumberJoin cannot be null!!");
                                            }
                                           localInventoryNumberJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryNumberJoin"),
                                               xmlWriter);
                                        } if (localInventoryNumberBinOnHandJoinTracker){
                                            if (localInventoryNumberBinOnHandJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inventoryNumberBinOnHandJoin cannot be null!!");
                                            }
                                           localInventoryNumberBinOnHandJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryNumberBinOnHandJoin"),
                                               xmlWriter);
                                        } if (localMemberItemJoinTracker){
                                            if (localMemberItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("memberItemJoin cannot be null!!");
                                            }
                                           localMemberItemJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","memberItemJoin"),
                                               xmlWriter);
                                        } if (localObsoleteRevisionJoinTracker){
                                            if (localObsoleteRevisionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("obsoleteRevisionJoin cannot be null!!");
                                            }
                                           localObsoleteRevisionJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","obsoleteRevisionJoin"),
                                               xmlWriter);
                                        } if (localParentJoinTracker){
                                            if (localParentJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentJoin cannot be null!!");
                                            }
                                           localParentJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parentJoin"),
                                               xmlWriter);
                                        } if (localPreferredLocationJoinTracker){
                                            if (localPreferredLocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("preferredLocationJoin cannot be null!!");
                                            }
                                           localPreferredLocationJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferredLocationJoin"),
                                               xmlWriter);
                                        } if (localPreferredVendorJoinTracker){
                                            if (localPreferredVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("preferredVendorJoin cannot be null!!");
                                            }
                                           localPreferredVendorJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferredVendorJoin"),
                                               xmlWriter);
                                        } if (localPricingJoinTracker){
                                            if (localPricingJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingJoin cannot be null!!");
                                            }
                                           localPricingJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingJoin"),
                                               xmlWriter);
                                        } if (localShopperJoinTracker){
                                            if (localShopperJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shopperJoin cannot be null!!");
                                            }
                                           localShopperJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shopperJoin"),
                                               xmlWriter);
                                        } if (localTransactionJoinTracker){
                                            if (localTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                            }
                                           localTransactionJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","transactionJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localUserNotesJoinTracker){
                                            if (localUserNotesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                            }
                                           localUserNotesJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","userNotesJoin"),
                                               xmlWriter);
                                        } if (localVendorJoinTracker){
                                            if (localVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                            }
                                           localVendorJoin.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vendorJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","ItemSearchRow"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localAssemblyItemBillOfMaterialsJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "assemblyItemBillOfMaterialsJoin"));
                            
                            
                                    if (localAssemblyItemBillOfMaterialsJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("assemblyItemBillOfMaterialsJoin cannot be null!!");
                                    }
                                    elementList.add(localAssemblyItemBillOfMaterialsJoin);
                                } if (localBinNumberJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "binNumberJoin"));
                            
                            
                                    if (localBinNumberJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("binNumberJoin cannot be null!!");
                                    }
                                    elementList.add(localBinNumberJoin);
                                } if (localBinOnHandJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "binOnHandJoin"));
                            
                            
                                    if (localBinOnHandJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("binOnHandJoin cannot be null!!");
                                    }
                                    elementList.add(localBinOnHandJoin);
                                } if (localCorrelatedItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "correlatedItemJoin"));
                            
                            
                                    if (localCorrelatedItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("correlatedItemJoin cannot be null!!");
                                    }
                                    elementList.add(localCorrelatedItemJoin);
                                } if (localEffectiveRevisionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "effectiveRevisionJoin"));
                            
                            
                                    if (localEffectiveRevisionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("effectiveRevisionJoin cannot be null!!");
                                    }
                                    elementList.add(localEffectiveRevisionJoin);
                                } if (localFileJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "fileJoin"));
                            
                            
                                    if (localFileJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                    }
                                    elementList.add(localFileJoin);
                                } if (localInventoryDetailJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "inventoryDetailJoin"));
                            
                            
                                    if (localInventoryDetailJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryDetailJoin cannot be null!!");
                                    }
                                    elementList.add(localInventoryDetailJoin);
                                } if (localInventoryLocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "inventoryLocationJoin"));
                            
                            
                                    if (localInventoryLocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryLocationJoin cannot be null!!");
                                    }
                                    elementList.add(localInventoryLocationJoin);
                                } if (localInventoryNumberJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "inventoryNumberJoin"));
                            
                            
                                    if (localInventoryNumberJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryNumberJoin cannot be null!!");
                                    }
                                    elementList.add(localInventoryNumberJoin);
                                } if (localInventoryNumberBinOnHandJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "inventoryNumberBinOnHandJoin"));
                            
                            
                                    if (localInventoryNumberBinOnHandJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("inventoryNumberBinOnHandJoin cannot be null!!");
                                    }
                                    elementList.add(localInventoryNumberBinOnHandJoin);
                                } if (localMemberItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "memberItemJoin"));
                            
                            
                                    if (localMemberItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("memberItemJoin cannot be null!!");
                                    }
                                    elementList.add(localMemberItemJoin);
                                } if (localObsoleteRevisionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "obsoleteRevisionJoin"));
                            
                            
                                    if (localObsoleteRevisionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("obsoleteRevisionJoin cannot be null!!");
                                    }
                                    elementList.add(localObsoleteRevisionJoin);
                                } if (localParentJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "parentJoin"));
                            
                            
                                    if (localParentJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentJoin cannot be null!!");
                                    }
                                    elementList.add(localParentJoin);
                                } if (localPreferredLocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "preferredLocationJoin"));
                            
                            
                                    if (localPreferredLocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("preferredLocationJoin cannot be null!!");
                                    }
                                    elementList.add(localPreferredLocationJoin);
                                } if (localPreferredVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "preferredVendorJoin"));
                            
                            
                                    if (localPreferredVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("preferredVendorJoin cannot be null!!");
                                    }
                                    elementList.add(localPreferredVendorJoin);
                                } if (localPricingJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricingJoin"));
                            
                            
                                    if (localPricingJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingJoin cannot be null!!");
                                    }
                                    elementList.add(localPricingJoin);
                                } if (localShopperJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shopperJoin"));
                            
                            
                                    if (localShopperJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("shopperJoin cannot be null!!");
                                    }
                                    elementList.add(localShopperJoin);
                                } if (localTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "transactionJoin"));
                            
                            
                                    if (localTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                    }
                                    elementList.add(localTransactionJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localUserNotesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "userNotesJoin"));
                            
                            
                                    if (localUserNotesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                    }
                                    elementList.add(localUserNotesJoin);
                                } if (localVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vendorJoin"));
                            
                            
                                    if (localVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemSearchRow parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemSearchRow object =
                new ItemSearchRow();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemSearchRow".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemSearchRow)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list23 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","assemblyItemBillOfMaterialsJoin").equals(reader.getName())){
                                
                                                object.setAssemblyItemBillOfMaterialsJoin(com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","binNumberJoin").equals(reader.getName())){
                                
                                                object.setBinNumberJoin(com.netsuite.webservices.platform.common_2017_2.BinSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","binOnHandJoin").equals(reader.getName())){
                                
                                                object.setBinOnHandJoin(com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","correlatedItemJoin").equals(reader.getName())){
                                
                                                object.setCorrelatedItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","effectiveRevisionJoin").equals(reader.getName())){
                                
                                                object.setEffectiveRevisionJoin(com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","fileJoin").equals(reader.getName())){
                                
                                                object.setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryDetailJoin").equals(reader.getName())){
                                
                                                object.setInventoryDetailJoin(com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryLocationJoin").equals(reader.getName())){
                                
                                                object.setInventoryLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryNumberJoin").equals(reader.getName())){
                                
                                                object.setInventoryNumberJoin(com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","inventoryNumberBinOnHandJoin").equals(reader.getName())){
                                
                                                object.setInventoryNumberBinOnHandJoin(com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","memberItemJoin").equals(reader.getName())){
                                
                                                object.setMemberItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","obsoleteRevisionJoin").equals(reader.getName())){
                                
                                                object.setObsoleteRevisionJoin(com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parentJoin").equals(reader.getName())){
                                
                                                object.setParentJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferredLocationJoin").equals(reader.getName())){
                                
                                                object.setPreferredLocationJoin(com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferredVendorJoin").equals(reader.getName())){
                                
                                                object.setPreferredVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingJoin").equals(reader.getName())){
                                
                                                object.setPricingJoin(com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shopperJoin").equals(reader.getName())){
                                
                                                object.setShopperJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","transactionJoin").equals(reader.getName())){
                                
                                                object.setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","userNotesJoin").equals(reader.getName())){
                                
                                                object.setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vendorJoin").equals(reader.getName())){
                                
                                                object.setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list23.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone23 = false;
                                                        while(!loopDone23){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone23 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list23.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone23 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.class,
                                                                list23));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    