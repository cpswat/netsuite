
/**
 * LocationTimeZone.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2.types;
            

            /**
            *  LocationTimeZone bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class LocationTimeZone
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.accounting_2017_2.lists.webservices.netsuite.com",
                "LocationTimeZone",
                "ns18");

            

                        /**
                        * field for LocationTimeZone
                        */

                        
                                    protected java.lang.String localLocationTimeZone ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected LocationTimeZone(java.lang.String value, boolean isRegisterValue) {
                                    localLocationTimeZone = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localLocationTimeZone, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __africaCairo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_africaCairo");
                                
                                    public static final java.lang.String __africaCasablanca =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_africaCasablanca");
                                
                                    public static final java.lang.String __africaJohannesburg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_africaJohannesburg");
                                
                                    public static final java.lang.String __africaNairobi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_africaNairobi");
                                
                                    public static final java.lang.String __africaTunis =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_africaTunis");
                                
                                    public static final java.lang.String __africaWindhoek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_africaWindhoek");
                                
                                    public static final java.lang.String __americaAnchorage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaAnchorage");
                                
                                    public static final java.lang.String __americaArgentinaBuenosAires =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaArgentinaBuenosAires");
                                
                                    public static final java.lang.String __americaBogota =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaBogota");
                                
                                    public static final java.lang.String __americaCaracas =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaCaracas");
                                
                                    public static final java.lang.String __americaCayenne =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaCayenne");
                                
                                    public static final java.lang.String __americaChicago =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaChicago");
                                
                                    public static final java.lang.String __americaChihuahua =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaChihuahua");
                                
                                    public static final java.lang.String __americaDenver =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaDenver");
                                
                                    public static final java.lang.String __americaGodthab =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaGodthab");
                                
                                    public static final java.lang.String __americaGuatemala =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaGuatemala");
                                
                                    public static final java.lang.String __americaHalifax =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaHalifax");
                                
                                    public static final java.lang.String __americaIndianaIndianapolis =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaIndianaIndianapolis");
                                
                                    public static final java.lang.String __americaLaPaz =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaLaPaz");
                                
                                    public static final java.lang.String __americaLosAngeles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaLosAngeles");
                                
                                    public static final java.lang.String __americaManaus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaManaus");
                                
                                    public static final java.lang.String __americaMexicoCity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaMexicoCity");
                                
                                    public static final java.lang.String __americaMontevideo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaMontevideo");
                                
                                    public static final java.lang.String __americaNewYork =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaNewYork");
                                
                                    public static final java.lang.String __americaNoronha =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaNoronha");
                                
                                    public static final java.lang.String __americaPhoenix =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaPhoenix");
                                
                                    public static final java.lang.String __americaRegina =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaRegina");
                                
                                    public static final java.lang.String __americaSantiago =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaSantiago");
                                
                                    public static final java.lang.String __americaSaoPaulo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaSaoPaulo");
                                
                                    public static final java.lang.String __americaStJohns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaStJohns");
                                
                                    public static final java.lang.String __americaTijuana =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americaTijuana");
                                
                                    public static final java.lang.String __asiaAlmaty =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaAlmaty");
                                
                                    public static final java.lang.String __asiaAmman =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaAmman");
                                
                                    public static final java.lang.String __asiaBaghdad =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaBaghdad");
                                
                                    public static final java.lang.String __asiaBaku =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaBaku");
                                
                                    public static final java.lang.String __asiaBangkok =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaBangkok");
                                
                                    public static final java.lang.String __asiaBeirut =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaBeirut");
                                
                                    public static final java.lang.String __asiaDhaka =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaDhaka");
                                
                                    public static final java.lang.String __asiaHongKong =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaHongKong");
                                
                                    public static final java.lang.String __asiaIrkutsk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaIrkutsk");
                                
                                    public static final java.lang.String __asiaJerusalem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaJerusalem");
                                
                                    public static final java.lang.String __asiaKabul =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaKabul");
                                
                                    public static final java.lang.String __asiaKarachi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaKarachi");
                                
                                    public static final java.lang.String __asiaKathmandu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaKathmandu");
                                
                                    public static final java.lang.String __asiaKolkata =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaKolkata");
                                
                                    public static final java.lang.String __asiaKrasnoyarsk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaKrasnoyarsk");
                                
                                    public static final java.lang.String __asiaKualaLumpur =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaKualaLumpur");
                                
                                    public static final java.lang.String __asiaMagadan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaMagadan");
                                
                                    public static final java.lang.String __asiaManila =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaManila");
                                
                                    public static final java.lang.String __asiaMuscat =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaMuscat");
                                
                                    public static final java.lang.String __asiaRangoon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaRangoon");
                                
                                    public static final java.lang.String __asiaRiyadh =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaRiyadh");
                                
                                    public static final java.lang.String __asiaSeoul =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaSeoul");
                                
                                    public static final java.lang.String __asiaTaipei =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaTaipei");
                                
                                    public static final java.lang.String __asiaTashkent =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaTashkent");
                                
                                    public static final java.lang.String __asiaTbilisi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaTbilisi");
                                
                                    public static final java.lang.String __asiaTehran =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaTehran");
                                
                                    public static final java.lang.String __asiaTokyo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaTokyo");
                                
                                    public static final java.lang.String __asiaVladivostok =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaVladivostok");
                                
                                    public static final java.lang.String __asiaYakutsk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaYakutsk");
                                
                                    public static final java.lang.String __asiaYekaterinburg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaYekaterinburg");
                                
                                    public static final java.lang.String __asiaYerevan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asiaYerevan");
                                
                                    public static final java.lang.String __atlanticAzores =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_atlanticAzores");
                                
                                    public static final java.lang.String __atlanticCapeVerde =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_atlanticCapeVerde");
                                
                                    public static final java.lang.String __atlanticReykjavik =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_atlanticReykjavik");
                                
                                    public static final java.lang.String __australiaAdelaide =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaAdelaide");
                                
                                    public static final java.lang.String __australiaBrisbane =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaBrisbane");
                                
                                    public static final java.lang.String __australiaDarwin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaDarwin");
                                
                                    public static final java.lang.String __australiaHobart =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaHobart");
                                
                                    public static final java.lang.String __australiaPerth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaPerth");
                                
                                    public static final java.lang.String __australiaSydney =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australiaSydney");
                                
                                    public static final java.lang.String __etcGmtPlus12 =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_etcGmtPlus12");
                                
                                    public static final java.lang.String __europeAmsterdam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeAmsterdam");
                                
                                    public static final java.lang.String __europeBudapest =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeBudapest");
                                
                                    public static final java.lang.String __europeIstanbul =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeIstanbul");
                                
                                    public static final java.lang.String __europeKiev =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeKiev");
                                
                                    public static final java.lang.String __europeLondon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeLondon");
                                
                                    public static final java.lang.String __europeMinsk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeMinsk");
                                
                                    public static final java.lang.String __europeMoscow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeMoscow");
                                
                                    public static final java.lang.String __europeParis =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeParis");
                                
                                    public static final java.lang.String __europeWarsaw =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_europeWarsaw");
                                
                                    public static final java.lang.String __pacificAuckland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pacificAuckland");
                                
                                    public static final java.lang.String __pacificGuam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pacificGuam");
                                
                                    public static final java.lang.String __pacificHonolulu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pacificHonolulu");
                                
                                    public static final java.lang.String __pacificKwajalein =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pacificKwajalein");
                                
                                    public static final java.lang.String __pacificPagoPago =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pacificPagoPago");
                                
                                    public static final java.lang.String __pacificTongatapu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pacificTongatapu");
                                
                                public static final LocationTimeZone _africaCairo =
                                    new LocationTimeZone(__africaCairo,true);
                            
                                public static final LocationTimeZone _africaCasablanca =
                                    new LocationTimeZone(__africaCasablanca,true);
                            
                                public static final LocationTimeZone _africaJohannesburg =
                                    new LocationTimeZone(__africaJohannesburg,true);
                            
                                public static final LocationTimeZone _africaNairobi =
                                    new LocationTimeZone(__africaNairobi,true);
                            
                                public static final LocationTimeZone _africaTunis =
                                    new LocationTimeZone(__africaTunis,true);
                            
                                public static final LocationTimeZone _africaWindhoek =
                                    new LocationTimeZone(__africaWindhoek,true);
                            
                                public static final LocationTimeZone _americaAnchorage =
                                    new LocationTimeZone(__americaAnchorage,true);
                            
                                public static final LocationTimeZone _americaArgentinaBuenosAires =
                                    new LocationTimeZone(__americaArgentinaBuenosAires,true);
                            
                                public static final LocationTimeZone _americaBogota =
                                    new LocationTimeZone(__americaBogota,true);
                            
                                public static final LocationTimeZone _americaCaracas =
                                    new LocationTimeZone(__americaCaracas,true);
                            
                                public static final LocationTimeZone _americaCayenne =
                                    new LocationTimeZone(__americaCayenne,true);
                            
                                public static final LocationTimeZone _americaChicago =
                                    new LocationTimeZone(__americaChicago,true);
                            
                                public static final LocationTimeZone _americaChihuahua =
                                    new LocationTimeZone(__americaChihuahua,true);
                            
                                public static final LocationTimeZone _americaDenver =
                                    new LocationTimeZone(__americaDenver,true);
                            
                                public static final LocationTimeZone _americaGodthab =
                                    new LocationTimeZone(__americaGodthab,true);
                            
                                public static final LocationTimeZone _americaGuatemala =
                                    new LocationTimeZone(__americaGuatemala,true);
                            
                                public static final LocationTimeZone _americaHalifax =
                                    new LocationTimeZone(__americaHalifax,true);
                            
                                public static final LocationTimeZone _americaIndianaIndianapolis =
                                    new LocationTimeZone(__americaIndianaIndianapolis,true);
                            
                                public static final LocationTimeZone _americaLaPaz =
                                    new LocationTimeZone(__americaLaPaz,true);
                            
                                public static final LocationTimeZone _americaLosAngeles =
                                    new LocationTimeZone(__americaLosAngeles,true);
                            
                                public static final LocationTimeZone _americaManaus =
                                    new LocationTimeZone(__americaManaus,true);
                            
                                public static final LocationTimeZone _americaMexicoCity =
                                    new LocationTimeZone(__americaMexicoCity,true);
                            
                                public static final LocationTimeZone _americaMontevideo =
                                    new LocationTimeZone(__americaMontevideo,true);
                            
                                public static final LocationTimeZone _americaNewYork =
                                    new LocationTimeZone(__americaNewYork,true);
                            
                                public static final LocationTimeZone _americaNoronha =
                                    new LocationTimeZone(__americaNoronha,true);
                            
                                public static final LocationTimeZone _americaPhoenix =
                                    new LocationTimeZone(__americaPhoenix,true);
                            
                                public static final LocationTimeZone _americaRegina =
                                    new LocationTimeZone(__americaRegina,true);
                            
                                public static final LocationTimeZone _americaSantiago =
                                    new LocationTimeZone(__americaSantiago,true);
                            
                                public static final LocationTimeZone _americaSaoPaulo =
                                    new LocationTimeZone(__americaSaoPaulo,true);
                            
                                public static final LocationTimeZone _americaStJohns =
                                    new LocationTimeZone(__americaStJohns,true);
                            
                                public static final LocationTimeZone _americaTijuana =
                                    new LocationTimeZone(__americaTijuana,true);
                            
                                public static final LocationTimeZone _asiaAlmaty =
                                    new LocationTimeZone(__asiaAlmaty,true);
                            
                                public static final LocationTimeZone _asiaAmman =
                                    new LocationTimeZone(__asiaAmman,true);
                            
                                public static final LocationTimeZone _asiaBaghdad =
                                    new LocationTimeZone(__asiaBaghdad,true);
                            
                                public static final LocationTimeZone _asiaBaku =
                                    new LocationTimeZone(__asiaBaku,true);
                            
                                public static final LocationTimeZone _asiaBangkok =
                                    new LocationTimeZone(__asiaBangkok,true);
                            
                                public static final LocationTimeZone _asiaBeirut =
                                    new LocationTimeZone(__asiaBeirut,true);
                            
                                public static final LocationTimeZone _asiaDhaka =
                                    new LocationTimeZone(__asiaDhaka,true);
                            
                                public static final LocationTimeZone _asiaHongKong =
                                    new LocationTimeZone(__asiaHongKong,true);
                            
                                public static final LocationTimeZone _asiaIrkutsk =
                                    new LocationTimeZone(__asiaIrkutsk,true);
                            
                                public static final LocationTimeZone _asiaJerusalem =
                                    new LocationTimeZone(__asiaJerusalem,true);
                            
                                public static final LocationTimeZone _asiaKabul =
                                    new LocationTimeZone(__asiaKabul,true);
                            
                                public static final LocationTimeZone _asiaKarachi =
                                    new LocationTimeZone(__asiaKarachi,true);
                            
                                public static final LocationTimeZone _asiaKathmandu =
                                    new LocationTimeZone(__asiaKathmandu,true);
                            
                                public static final LocationTimeZone _asiaKolkata =
                                    new LocationTimeZone(__asiaKolkata,true);
                            
                                public static final LocationTimeZone _asiaKrasnoyarsk =
                                    new LocationTimeZone(__asiaKrasnoyarsk,true);
                            
                                public static final LocationTimeZone _asiaKualaLumpur =
                                    new LocationTimeZone(__asiaKualaLumpur,true);
                            
                                public static final LocationTimeZone _asiaMagadan =
                                    new LocationTimeZone(__asiaMagadan,true);
                            
                                public static final LocationTimeZone _asiaManila =
                                    new LocationTimeZone(__asiaManila,true);
                            
                                public static final LocationTimeZone _asiaMuscat =
                                    new LocationTimeZone(__asiaMuscat,true);
                            
                                public static final LocationTimeZone _asiaRangoon =
                                    new LocationTimeZone(__asiaRangoon,true);
                            
                                public static final LocationTimeZone _asiaRiyadh =
                                    new LocationTimeZone(__asiaRiyadh,true);
                            
                                public static final LocationTimeZone _asiaSeoul =
                                    new LocationTimeZone(__asiaSeoul,true);
                            
                                public static final LocationTimeZone _asiaTaipei =
                                    new LocationTimeZone(__asiaTaipei,true);
                            
                                public static final LocationTimeZone _asiaTashkent =
                                    new LocationTimeZone(__asiaTashkent,true);
                            
                                public static final LocationTimeZone _asiaTbilisi =
                                    new LocationTimeZone(__asiaTbilisi,true);
                            
                                public static final LocationTimeZone _asiaTehran =
                                    new LocationTimeZone(__asiaTehran,true);
                            
                                public static final LocationTimeZone _asiaTokyo =
                                    new LocationTimeZone(__asiaTokyo,true);
                            
                                public static final LocationTimeZone _asiaVladivostok =
                                    new LocationTimeZone(__asiaVladivostok,true);
                            
                                public static final LocationTimeZone _asiaYakutsk =
                                    new LocationTimeZone(__asiaYakutsk,true);
                            
                                public static final LocationTimeZone _asiaYekaterinburg =
                                    new LocationTimeZone(__asiaYekaterinburg,true);
                            
                                public static final LocationTimeZone _asiaYerevan =
                                    new LocationTimeZone(__asiaYerevan,true);
                            
                                public static final LocationTimeZone _atlanticAzores =
                                    new LocationTimeZone(__atlanticAzores,true);
                            
                                public static final LocationTimeZone _atlanticCapeVerde =
                                    new LocationTimeZone(__atlanticCapeVerde,true);
                            
                                public static final LocationTimeZone _atlanticReykjavik =
                                    new LocationTimeZone(__atlanticReykjavik,true);
                            
                                public static final LocationTimeZone _australiaAdelaide =
                                    new LocationTimeZone(__australiaAdelaide,true);
                            
                                public static final LocationTimeZone _australiaBrisbane =
                                    new LocationTimeZone(__australiaBrisbane,true);
                            
                                public static final LocationTimeZone _australiaDarwin =
                                    new LocationTimeZone(__australiaDarwin,true);
                            
                                public static final LocationTimeZone _australiaHobart =
                                    new LocationTimeZone(__australiaHobart,true);
                            
                                public static final LocationTimeZone _australiaPerth =
                                    new LocationTimeZone(__australiaPerth,true);
                            
                                public static final LocationTimeZone _australiaSydney =
                                    new LocationTimeZone(__australiaSydney,true);
                            
                                public static final LocationTimeZone _etcGmtPlus12 =
                                    new LocationTimeZone(__etcGmtPlus12,true);
                            
                                public static final LocationTimeZone _europeAmsterdam =
                                    new LocationTimeZone(__europeAmsterdam,true);
                            
                                public static final LocationTimeZone _europeBudapest =
                                    new LocationTimeZone(__europeBudapest,true);
                            
                                public static final LocationTimeZone _europeIstanbul =
                                    new LocationTimeZone(__europeIstanbul,true);
                            
                                public static final LocationTimeZone _europeKiev =
                                    new LocationTimeZone(__europeKiev,true);
                            
                                public static final LocationTimeZone _europeLondon =
                                    new LocationTimeZone(__europeLondon,true);
                            
                                public static final LocationTimeZone _europeMinsk =
                                    new LocationTimeZone(__europeMinsk,true);
                            
                                public static final LocationTimeZone _europeMoscow =
                                    new LocationTimeZone(__europeMoscow,true);
                            
                                public static final LocationTimeZone _europeParis =
                                    new LocationTimeZone(__europeParis,true);
                            
                                public static final LocationTimeZone _europeWarsaw =
                                    new LocationTimeZone(__europeWarsaw,true);
                            
                                public static final LocationTimeZone _pacificAuckland =
                                    new LocationTimeZone(__pacificAuckland,true);
                            
                                public static final LocationTimeZone _pacificGuam =
                                    new LocationTimeZone(__pacificGuam,true);
                            
                                public static final LocationTimeZone _pacificHonolulu =
                                    new LocationTimeZone(__pacificHonolulu,true);
                            
                                public static final LocationTimeZone _pacificKwajalein =
                                    new LocationTimeZone(__pacificKwajalein,true);
                            
                                public static final LocationTimeZone _pacificPagoPago =
                                    new LocationTimeZone(__pacificPagoPago,true);
                            
                                public static final LocationTimeZone _pacificTongatapu =
                                    new LocationTimeZone(__pacificTongatapu,true);
                            

                                public java.lang.String getValue() { return localLocationTimeZone;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localLocationTimeZone.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.accounting_2017_2.lists.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":LocationTimeZone",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "LocationTimeZone",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localLocationTimeZone==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("LocationTimeZone cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localLocationTimeZone);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns18";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocationTimeZone)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static LocationTimeZone fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    LocationTimeZone enumeration = (LocationTimeZone)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static LocationTimeZone fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static LocationTimeZone fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return LocationTimeZone.Factory.fromString(content,namespaceUri);
                    } else {
                       return LocationTimeZone.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static LocationTimeZone parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            LocationTimeZone object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"LocationTimeZone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = LocationTimeZone.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = LocationTimeZone.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    