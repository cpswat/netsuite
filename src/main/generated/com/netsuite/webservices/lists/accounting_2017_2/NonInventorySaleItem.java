
/**
 * NonInventorySaleItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  NonInventorySaleItem bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class NonInventorySaleItem extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = NonInventorySaleItem
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for SalesDescription
                        */

                        
                                    protected java.lang.String localSalesDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesDescriptionTracker = false ;

                           public boolean isSalesDescriptionSpecified(){
                               return localSalesDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSalesDescription(){
                               return localSalesDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesDescription
                               */
                               public void setSalesDescription(java.lang.String param){
                            localSalesDescriptionTracker = param != null;
                                   
                                            this.localSalesDescription=param;
                                    

                               }
                            

                        /**
                        * field for IncludeChildren
                        */

                        
                                    protected boolean localIncludeChildren ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncludeChildrenTracker = false ;

                           public boolean isIncludeChildrenSpecified(){
                               return localIncludeChildrenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIncludeChildren(){
                               return localIncludeChildren;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncludeChildren
                               */
                               public void setIncludeChildren(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIncludeChildrenTracker =
                                       true;
                                   
                                            this.localIncludeChildren=param;
                                    

                               }
                            

                        /**
                        * field for IncomeAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIncomeAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncomeAccountTracker = false ;

                           public boolean isIncomeAccountSpecified(){
                               return localIncomeAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIncomeAccount(){
                               return localIncomeAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncomeAccount
                               */
                               public void setIncomeAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIncomeAccountTracker = param != null;
                                   
                                            this.localIncomeAccount=param;
                                    

                               }
                            

                        /**
                        * field for IsTaxable
                        */

                        
                                    protected boolean localIsTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTaxableTracker = false ;

                           public boolean isIsTaxableSpecified(){
                               return localIsTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsTaxable(){
                               return localIsTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTaxable
                               */
                               public void setIsTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsTaxableTracker =
                                       true;
                                   
                                            this.localIsTaxable=param;
                                    

                               }
                            

                        /**
                        * field for MatrixType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType localMatrixType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatrixTypeTracker = false ;

                           public boolean isMatrixTypeSpecified(){
                               return localMatrixTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType getMatrixType(){
                               return localMatrixType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatrixType
                               */
                               public void setMatrixType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType param){
                            localMatrixTypeTracker = param != null;
                                   
                                            this.localMatrixType=param;
                                    

                               }
                            

                        /**
                        * field for TaxSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxScheduleTracker = false ;

                           public boolean isTaxScheduleSpecified(){
                               return localTaxScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxSchedule(){
                               return localTaxSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxSchedule
                               */
                               public void setTaxSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxScheduleTracker = param != null;
                                   
                                            this.localTaxSchedule=param;
                                    

                               }
                            

                        /**
                        * field for ShippingCost
                        */

                        
                                    protected double localShippingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingCostTracker = false ;

                           public boolean isShippingCostSpecified(){
                               return localShippingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getShippingCost(){
                               return localShippingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingCost
                               */
                               public void setShippingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localShippingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localShippingCost=param;
                                    

                               }
                            

                        /**
                        * field for ShippingCostUnits
                        */

                        
                                    protected java.lang.String localShippingCostUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingCostUnitsTracker = false ;

                           public boolean isShippingCostUnitsSpecified(){
                               return localShippingCostUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShippingCostUnits(){
                               return localShippingCostUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingCostUnits
                               */
                               public void setShippingCostUnits(java.lang.String param){
                            localShippingCostUnitsTracker = param != null;
                                   
                                            this.localShippingCostUnits=param;
                                    

                               }
                            

                        /**
                        * field for HandlingCost
                        */

                        
                                    protected double localHandlingCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingCostTracker = false ;

                           public boolean isHandlingCostSpecified(){
                               return localHandlingCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHandlingCost(){
                               return localHandlingCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingCost
                               */
                               public void setHandlingCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHandlingCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHandlingCost=param;
                                    

                               }
                            

                        /**
                        * field for HandlingCostUnits
                        */

                        
                                    protected java.lang.String localHandlingCostUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHandlingCostUnitsTracker = false ;

                           public boolean isHandlingCostUnitsSpecified(){
                               return localHandlingCostUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHandlingCostUnits(){
                               return localHandlingCostUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HandlingCostUnits
                               */
                               public void setHandlingCostUnits(java.lang.String param){
                            localHandlingCostUnitsTracker = param != null;
                                   
                                            this.localHandlingCostUnits=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimateType
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType localCostEstimateType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTypeTracker = false ;

                           public boolean isCostEstimateTypeSpecified(){
                               return localCostEstimateTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType getCostEstimateType(){
                               return localCostEstimateType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimateType
                               */
                               public void setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType param){
                            localCostEstimateTypeTracker = param != null;
                                   
                                            this.localCostEstimateType=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimate
                        */

                        
                                    protected double localCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTracker = false ;

                           public boolean isCostEstimateSpecified(){
                               return localCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCostEstimate(){
                               return localCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimate
                               */
                               public void setCostEstimate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostEstimateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for Weight
                        */

                        
                                    protected double localWeight ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWeightTracker = false ;

                           public boolean isWeightSpecified(){
                               return localWeightTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getWeight(){
                               return localWeight;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Weight
                               */
                               public void setWeight(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localWeightTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localWeight=param;
                                    

                               }
                            

                        /**
                        * field for WeightUnit
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemWeightUnit localWeightUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWeightUnitTracker = false ;

                           public boolean isWeightUnitSpecified(){
                               return localWeightUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemWeightUnit
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemWeightUnit getWeightUnit(){
                               return localWeightUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WeightUnit
                               */
                               public void setWeightUnit(com.netsuite.webservices.lists.accounting_2017_2.types.ItemWeightUnit param){
                            localWeightUnitTracker = param != null;
                                   
                                            this.localWeightUnit=param;
                                    

                               }
                            

                        /**
                        * field for WeightUnits
                        */

                        
                                    protected java.lang.String localWeightUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWeightUnitsTracker = false ;

                           public boolean isWeightUnitsSpecified(){
                               return localWeightUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getWeightUnits(){
                               return localWeightUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WeightUnits
                               */
                               public void setWeightUnits(java.lang.String param){
                            localWeightUnitsTracker = param != null;
                                   
                                            this.localWeightUnits=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimateUnits
                        */

                        
                                    protected java.lang.String localCostEstimateUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateUnitsTracker = false ;

                           public boolean isCostEstimateUnitsSpecified(){
                               return localCostEstimateUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCostEstimateUnits(){
                               return localCostEstimateUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimateUnits
                               */
                               public void setCostEstimateUnits(java.lang.String param){
                            localCostEstimateUnitsTracker = param != null;
                                   
                                            this.localCostEstimateUnits=param;
                                    

                               }
                            

                        /**
                        * field for UnitsType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnitsType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTypeTracker = false ;

                           public boolean isUnitsTypeSpecified(){
                               return localUnitsTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnitsType(){
                               return localUnitsType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UnitsType
                               */
                               public void setUnitsType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitsTypeTracker = param != null;
                                   
                                            this.localUnitsType=param;
                                    

                               }
                            

                        /**
                        * field for SaleUnit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSaleUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaleUnitTracker = false ;

                           public boolean isSaleUnitSpecified(){
                               return localSaleUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSaleUnit(){
                               return localSaleUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SaleUnit
                               */
                               public void setSaleUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSaleUnitTracker = param != null;
                                   
                                            this.localSaleUnit=param;
                                    

                               }
                            

                        /**
                        * field for IssueProduct
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIssueProduct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueProductTracker = false ;

                           public boolean isIssueProductSpecified(){
                               return localIssueProductTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIssueProduct(){
                               return localIssueProduct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueProduct
                               */
                               public void setIssueProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIssueProductTracker = param != null;
                                   
                                            this.localIssueProduct=param;
                                    

                               }
                            

                        /**
                        * field for BillingSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingScheduleTracker = false ;

                           public boolean isBillingScheduleSpecified(){
                               return localBillingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingSchedule(){
                               return localBillingSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingSchedule
                               */
                               public void setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingScheduleTracker = param != null;
                                   
                                            this.localBillingSchedule=param;
                                    

                               }
                            

                        /**
                        * field for DeferredRevenueAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDeferredRevenueAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferredRevenueAccountTracker = false ;

                           public boolean isDeferredRevenueAccountSpecified(){
                               return localDeferredRevenueAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDeferredRevenueAccount(){
                               return localDeferredRevenueAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferredRevenueAccount
                               */
                               public void setDeferredRevenueAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDeferredRevenueAccountTracker = param != null;
                                   
                                            this.localDeferredRevenueAccount=param;
                                    

                               }
                            

                        /**
                        * field for RevRecSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleTracker = false ;

                           public boolean isRevRecScheduleSpecified(){
                               return localRevRecScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecSchedule(){
                               return localRevRecSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecSchedule
                               */
                               public void setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecScheduleTracker = param != null;
                                   
                                            this.localRevRecSchedule=param;
                                    

                               }
                            

                        /**
                        * field for StockDescription
                        */

                        
                                    protected java.lang.String localStockDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStockDescriptionTracker = false ;

                           public boolean isStockDescriptionSpecified(){
                               return localStockDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStockDescription(){
                               return localStockDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StockDescription
                               */
                               public void setStockDescription(java.lang.String param){
                            localStockDescriptionTracker = param != null;
                                   
                                            this.localStockDescription=param;
                                    

                               }
                            

                        /**
                        * field for IsHazmatItem
                        */

                        
                                    protected boolean localIsHazmatItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsHazmatItemTracker = false ;

                           public boolean isIsHazmatItemSpecified(){
                               return localIsHazmatItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsHazmatItem(){
                               return localIsHazmatItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsHazmatItem
                               */
                               public void setIsHazmatItem(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsHazmatItemTracker =
                                       true;
                                   
                                            this.localIsHazmatItem=param;
                                    

                               }
                            

                        /**
                        * field for HazmatId
                        */

                        
                                    protected java.lang.String localHazmatId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatIdTracker = false ;

                           public boolean isHazmatIdSpecified(){
                               return localHazmatIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHazmatId(){
                               return localHazmatId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatId
                               */
                               public void setHazmatId(java.lang.String param){
                            localHazmatIdTracker = param != null;
                                   
                                            this.localHazmatId=param;
                                    

                               }
                            

                        /**
                        * field for HazmatShippingName
                        */

                        
                                    protected java.lang.String localHazmatShippingName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatShippingNameTracker = false ;

                           public boolean isHazmatShippingNameSpecified(){
                               return localHazmatShippingNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHazmatShippingName(){
                               return localHazmatShippingName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatShippingName
                               */
                               public void setHazmatShippingName(java.lang.String param){
                            localHazmatShippingNameTracker = param != null;
                                   
                                            this.localHazmatShippingName=param;
                                    

                               }
                            

                        /**
                        * field for HazmatHazardClass
                        */

                        
                                    protected java.lang.String localHazmatHazardClass ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatHazardClassTracker = false ;

                           public boolean isHazmatHazardClassSpecified(){
                               return localHazmatHazardClassTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHazmatHazardClass(){
                               return localHazmatHazardClass;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatHazardClass
                               */
                               public void setHazmatHazardClass(java.lang.String param){
                            localHazmatHazardClassTracker = param != null;
                                   
                                            this.localHazmatHazardClass=param;
                                    

                               }
                            

                        /**
                        * field for HazmatPackingGroup
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.HazmatPackingGroup localHazmatPackingGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatPackingGroupTracker = false ;

                           public boolean isHazmatPackingGroupSpecified(){
                               return localHazmatPackingGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.HazmatPackingGroup
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.HazmatPackingGroup getHazmatPackingGroup(){
                               return localHazmatPackingGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatPackingGroup
                               */
                               public void setHazmatPackingGroup(com.netsuite.webservices.lists.accounting_2017_2.types.HazmatPackingGroup param){
                            localHazmatPackingGroupTracker = param != null;
                                   
                                            this.localHazmatPackingGroup=param;
                                    

                               }
                            

                        /**
                        * field for HazmatItemUnits
                        */

                        
                                    protected java.lang.String localHazmatItemUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatItemUnitsTracker = false ;

                           public boolean isHazmatItemUnitsSpecified(){
                               return localHazmatItemUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHazmatItemUnits(){
                               return localHazmatItemUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatItemUnits
                               */
                               public void setHazmatItemUnits(java.lang.String param){
                            localHazmatItemUnitsTracker = param != null;
                                   
                                            this.localHazmatItemUnits=param;
                                    

                               }
                            

                        /**
                        * field for HazmatItemUnitsQty
                        */

                        
                                    protected double localHazmatItemUnitsQty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHazmatItemUnitsQtyTracker = false ;

                           public boolean isHazmatItemUnitsQtySpecified(){
                               return localHazmatItemUnitsQtyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getHazmatItemUnitsQty(){
                               return localHazmatItemUnitsQty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HazmatItemUnitsQty
                               */
                               public void setHazmatItemUnitsQty(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localHazmatItemUnitsQtyTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localHazmatItemUnitsQty=param;
                                    

                               }
                            

                        /**
                        * field for Producer
                        */

                        
                                    protected boolean localProducer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProducerTracker = false ;

                           public boolean isProducerSpecified(){
                               return localProducerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getProducer(){
                               return localProducer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Producer
                               */
                               public void setProducer(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localProducerTracker =
                                       true;
                                   
                                            this.localProducer=param;
                                    

                               }
                            

                        /**
                        * field for Manufacturer
                        */

                        
                                    protected java.lang.String localManufacturer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerTracker = false ;

                           public boolean isManufacturerSpecified(){
                               return localManufacturerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturer(){
                               return localManufacturer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Manufacturer
                               */
                               public void setManufacturer(java.lang.String param){
                            localManufacturerTracker = param != null;
                                   
                                            this.localManufacturer=param;
                                    

                               }
                            

                        /**
                        * field for Mpn
                        */

                        
                                    protected java.lang.String localMpn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMpnTracker = false ;

                           public boolean isMpnSpecified(){
                               return localMpnTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMpn(){
                               return localMpn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Mpn
                               */
                               public void setMpn(java.lang.String param){
                            localMpnTracker = param != null;
                                   
                                            this.localMpn=param;
                                    

                               }
                            

                        /**
                        * field for MultManufactureAddr
                        */

                        
                                    protected boolean localMultManufactureAddr ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMultManufactureAddrTracker = false ;

                           public boolean isMultManufactureAddrSpecified(){
                               return localMultManufactureAddrTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getMultManufactureAddr(){
                               return localMultManufactureAddr;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MultManufactureAddr
                               */
                               public void setMultManufactureAddr(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localMultManufactureAddrTracker =
                                       true;
                                   
                                            this.localMultManufactureAddr=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturerAddr1
                        */

                        
                                    protected java.lang.String localManufacturerAddr1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerAddr1Tracker = false ;

                           public boolean isManufacturerAddr1Specified(){
                               return localManufacturerAddr1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturerAddr1(){
                               return localManufacturerAddr1;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturerAddr1
                               */
                               public void setManufacturerAddr1(java.lang.String param){
                            localManufacturerAddr1Tracker = param != null;
                                   
                                            this.localManufacturerAddr1=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturerCity
                        */

                        
                                    protected java.lang.String localManufacturerCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerCityTracker = false ;

                           public boolean isManufacturerCitySpecified(){
                               return localManufacturerCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturerCity(){
                               return localManufacturerCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturerCity
                               */
                               public void setManufacturerCity(java.lang.String param){
                            localManufacturerCityTracker = param != null;
                                   
                                            this.localManufacturerCity=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturerState
                        */

                        
                                    protected java.lang.String localManufacturerState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerStateTracker = false ;

                           public boolean isManufacturerStateSpecified(){
                               return localManufacturerStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturerState(){
                               return localManufacturerState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturerState
                               */
                               public void setManufacturerState(java.lang.String param){
                            localManufacturerStateTracker = param != null;
                                   
                                            this.localManufacturerState=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturerZip
                        */

                        
                                    protected java.lang.String localManufacturerZip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerZipTracker = false ;

                           public boolean isManufacturerZipSpecified(){
                               return localManufacturerZipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturerZip(){
                               return localManufacturerZip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturerZip
                               */
                               public void setManufacturerZip(java.lang.String param){
                            localManufacturerZipTracker = param != null;
                                   
                                            this.localManufacturerZip=param;
                                    

                               }
                            

                        /**
                        * field for CountryOfManufacture
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.Country localCountryOfManufacture ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryOfManufactureTracker = false ;

                           public boolean isCountryOfManufactureSpecified(){
                               return localCountryOfManufactureTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.Country
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.Country getCountryOfManufacture(){
                               return localCountryOfManufacture;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CountryOfManufacture
                               */
                               public void setCountryOfManufacture(com.netsuite.webservices.platform.common_2017_2.types.Country param){
                            localCountryOfManufactureTracker = param != null;
                                   
                                            this.localCountryOfManufacture=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturerTaxId
                        */

                        
                                    protected java.lang.String localManufacturerTaxId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerTaxIdTracker = false ;

                           public boolean isManufacturerTaxIdSpecified(){
                               return localManufacturerTaxIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturerTaxId(){
                               return localManufacturerTaxId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturerTaxId
                               */
                               public void setManufacturerTaxId(java.lang.String param){
                            localManufacturerTaxIdTracker = param != null;
                                   
                                            this.localManufacturerTaxId=param;
                                    

                               }
                            

                        /**
                        * field for ScheduleBNumber
                        */

                        
                                    protected java.lang.String localScheduleBNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleBNumberTracker = false ;

                           public boolean isScheduleBNumberSpecified(){
                               return localScheduleBNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getScheduleBNumber(){
                               return localScheduleBNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleBNumber
                               */
                               public void setScheduleBNumber(java.lang.String param){
                            localScheduleBNumberTracker = param != null;
                                   
                                            this.localScheduleBNumber=param;
                                    

                               }
                            

                        /**
                        * field for ScheduleBQuantity
                        */

                        
                                    protected long localScheduleBQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleBQuantityTracker = false ;

                           public boolean isScheduleBQuantitySpecified(){
                               return localScheduleBQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getScheduleBQuantity(){
                               return localScheduleBQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleBQuantity
                               */
                               public void setScheduleBQuantity(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localScheduleBQuantityTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localScheduleBQuantity=param;
                                    

                               }
                            

                        /**
                        * field for ScheduleBCode
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ScheduleBCode localScheduleBCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleBCodeTracker = false ;

                           public boolean isScheduleBCodeSpecified(){
                               return localScheduleBCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ScheduleBCode
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ScheduleBCode getScheduleBCode(){
                               return localScheduleBCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleBCode
                               */
                               public void setScheduleBCode(com.netsuite.webservices.lists.accounting_2017_2.types.ScheduleBCode param){
                            localScheduleBCodeTracker = param != null;
                                   
                                            this.localScheduleBCode=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturerTariff
                        */

                        
                                    protected java.lang.String localManufacturerTariff ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturerTariffTracker = false ;

                           public boolean isManufacturerTariffSpecified(){
                               return localManufacturerTariffTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getManufacturerTariff(){
                               return localManufacturerTariff;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturerTariff
                               */
                               public void setManufacturerTariff(java.lang.String param){
                            localManufacturerTariffTracker = param != null;
                                   
                                            this.localManufacturerTariff=param;
                                    

                               }
                            

                        /**
                        * field for PreferenceCriterion
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemPreferenceCriterion localPreferenceCriterion ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPreferenceCriterionTracker = false ;

                           public boolean isPreferenceCriterionSpecified(){
                               return localPreferenceCriterionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemPreferenceCriterion
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemPreferenceCriterion getPreferenceCriterion(){
                               return localPreferenceCriterion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PreferenceCriterion
                               */
                               public void setPreferenceCriterion(com.netsuite.webservices.lists.accounting_2017_2.types.ItemPreferenceCriterion param){
                            localPreferenceCriterionTracker = param != null;
                                   
                                            this.localPreferenceCriterion=param;
                                    

                               }
                            

                        /**
                        * field for MinimumQuantity
                        */

                        
                                    protected long localMinimumQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMinimumQuantityTracker = false ;

                           public boolean isMinimumQuantitySpecified(){
                               return localMinimumQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getMinimumQuantity(){
                               return localMinimumQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MinimumQuantity
                               */
                               public void setMinimumQuantity(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localMinimumQuantityTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localMinimumQuantity=param;
                                    

                               }
                            

                        /**
                        * field for EnforceMinQtyInternally
                        */

                        
                                    protected boolean localEnforceMinQtyInternally ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEnforceMinQtyInternallyTracker = false ;

                           public boolean isEnforceMinQtyInternallySpecified(){
                               return localEnforceMinQtyInternallyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEnforceMinQtyInternally(){
                               return localEnforceMinQtyInternally;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EnforceMinQtyInternally
                               */
                               public void setEnforceMinQtyInternally(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEnforceMinQtyInternallyTracker =
                                       true;
                                   
                                            this.localEnforceMinQtyInternally=param;
                                    

                               }
                            

                        /**
                        * field for SoftDescriptor
                        */

                        
                                    protected java.lang.String localSoftDescriptor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSoftDescriptorTracker = false ;

                           public boolean isSoftDescriptorSpecified(){
                               return localSoftDescriptorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSoftDescriptor(){
                               return localSoftDescriptor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SoftDescriptor
                               */
                               public void setSoftDescriptor(java.lang.String param){
                            localSoftDescriptorTracker = param != null;
                                   
                                            this.localSoftDescriptor=param;
                                    

                               }
                            

                        /**
                        * field for ShipPackage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localShipPackage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipPackageTracker = false ;

                           public boolean isShipPackageSpecified(){
                               return localShipPackageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getShipPackage(){
                               return localShipPackage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipPackage
                               */
                               public void setShipPackage(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localShipPackageTracker = param != null;
                                   
                                            this.localShipPackage=param;
                                    

                               }
                            

                        /**
                        * field for ShipIndividually
                        */

                        
                                    protected boolean localShipIndividually ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipIndividuallyTracker = false ;

                           public boolean isShipIndividuallySpecified(){
                               return localShipIndividuallyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShipIndividually(){
                               return localShipIndividually;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipIndividually
                               */
                               public void setShipIndividually(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShipIndividuallyTracker =
                                       true;
                                   
                                            this.localShipIndividually=param;
                                    

                               }
                            

                        /**
                        * field for IsFulfillable
                        */

                        
                                    protected boolean localIsFulfillable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsFulfillableTracker = false ;

                           public boolean isIsFulfillableSpecified(){
                               return localIsFulfillableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsFulfillable(){
                               return localIsFulfillable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsFulfillable
                               */
                               public void setIsFulfillable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsFulfillableTracker =
                                       true;
                                   
                                            this.localIsFulfillable=param;
                                    

                               }
                            

                        /**
                        * field for CostCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCostCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostCategoryTracker = false ;

                           public boolean isCostCategorySpecified(){
                               return localCostCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCostCategory(){
                               return localCostCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostCategory
                               */
                               public void setCostCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCostCategoryTracker = param != null;
                                   
                                            this.localCostCategory=param;
                                    

                               }
                            

                        /**
                        * field for PricesIncludeTax
                        */

                        
                                    protected boolean localPricesIncludeTax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricesIncludeTaxTracker = false ;

                           public boolean isPricesIncludeTaxSpecified(){
                               return localPricesIncludeTaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPricesIncludeTax(){
                               return localPricesIncludeTax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricesIncludeTax
                               */
                               public void setPricesIncludeTax(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPricesIncludeTaxTracker =
                                       true;
                                   
                                            this.localPricesIncludeTax=param;
                                    

                               }
                            

                        /**
                        * field for QuantityPricingSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localQuantityPricingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityPricingScheduleTracker = false ;

                           public boolean isQuantityPricingScheduleSpecified(){
                               return localQuantityPricingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getQuantityPricingSchedule(){
                               return localQuantityPricingSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityPricingSchedule
                               */
                               public void setQuantityPricingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localQuantityPricingScheduleTracker = param != null;
                                   
                                            this.localQuantityPricingSchedule=param;
                                    

                               }
                            

                        /**
                        * field for UseMarginalRates
                        */

                        
                                    protected boolean localUseMarginalRates ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseMarginalRatesTracker = false ;

                           public boolean isUseMarginalRatesSpecified(){
                               return localUseMarginalRatesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseMarginalRates(){
                               return localUseMarginalRates;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseMarginalRates
                               */
                               public void setUseMarginalRates(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseMarginalRatesTracker =
                                       true;
                                   
                                            this.localUseMarginalRates=param;
                                    

                               }
                            

                        /**
                        * field for OverallQuantityPricingType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType localOverallQuantityPricingType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOverallQuantityPricingTypeTracker = false ;

                           public boolean isOverallQuantityPricingTypeSpecified(){
                               return localOverallQuantityPricingTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType getOverallQuantityPricingType(){
                               return localOverallQuantityPricingType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OverallQuantityPricingType
                               */
                               public void setOverallQuantityPricingType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType param){
                            localOverallQuantityPricingTypeTracker = param != null;
                                   
                                            this.localOverallQuantityPricingType=param;
                                    

                               }
                            

                        /**
                        * field for PricingGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPricingGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingGroupTracker = false ;

                           public boolean isPricingGroupSpecified(){
                               return localPricingGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPricingGroup(){
                               return localPricingGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingGroup
                               */
                               public void setPricingGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPricingGroupTracker = param != null;
                                   
                                            this.localPricingGroup=param;
                                    

                               }
                            

                        /**
                        * field for MinimumQuantityUnits
                        */

                        
                                    protected java.lang.String localMinimumQuantityUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMinimumQuantityUnitsTracker = false ;

                           public boolean isMinimumQuantityUnitsSpecified(){
                               return localMinimumQuantityUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMinimumQuantityUnits(){
                               return localMinimumQuantityUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MinimumQuantityUnits
                               */
                               public void setMinimumQuantityUnits(java.lang.String param){
                            localMinimumQuantityUnitsTracker = param != null;
                                   
                                            this.localMinimumQuantityUnits=param;
                                    

                               }
                            

                        /**
                        * field for VsoePrice
                        */

                        
                                    protected double localVsoePrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePriceTracker = false ;

                           public boolean isVsoePriceSpecified(){
                               return localVsoePriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoePrice(){
                               return localVsoePrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePrice
                               */
                               public void setVsoePrice(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoePriceTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoePrice=param;
                                    

                               }
                            

                        /**
                        * field for VsoeSopGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup localVsoeSopGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeSopGroupTracker = false ;

                           public boolean isVsoeSopGroupSpecified(){
                               return localVsoeSopGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup getVsoeSopGroup(){
                               return localVsoeSopGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeSopGroup
                               */
                               public void setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup param){
                            localVsoeSopGroupTracker = param != null;
                                   
                                            this.localVsoeSopGroup=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDeferral
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral localVsoeDeferral ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeferralTracker = false ;

                           public boolean isVsoeDeferralSpecified(){
                               return localVsoeDeferralTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral getVsoeDeferral(){
                               return localVsoeDeferral;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDeferral
                               */
                               public void setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral param){
                            localVsoeDeferralTracker = param != null;
                                   
                                            this.localVsoeDeferral=param;
                                    

                               }
                            

                        /**
                        * field for VsoePermitDiscount
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount localVsoePermitDiscount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePermitDiscountTracker = false ;

                           public boolean isVsoePermitDiscountSpecified(){
                               return localVsoePermitDiscountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount getVsoePermitDiscount(){
                               return localVsoePermitDiscount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePermitDiscount
                               */
                               public void setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount param){
                            localVsoePermitDiscountTracker = param != null;
                                   
                                            this.localVsoePermitDiscount=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDelivered
                        */

                        
                                    protected boolean localVsoeDelivered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeliveredTracker = false ;

                           public boolean isVsoeDeliveredSpecified(){
                               return localVsoeDeliveredTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeDelivered(){
                               return localVsoeDelivered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDelivered
                               */
                               public void setVsoeDelivered(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeDeliveredTracker =
                                       true;
                                   
                                            this.localVsoeDelivered=param;
                                    

                               }
                            

                        /**
                        * field for ItemRevenueCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItemRevenueCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemRevenueCategoryTracker = false ;

                           public boolean isItemRevenueCategorySpecified(){
                               return localItemRevenueCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItemRevenueCategory(){
                               return localItemRevenueCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemRevenueCategory
                               */
                               public void setItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemRevenueCategoryTracker = param != null;
                                   
                                            this.localItemRevenueCategory=param;
                                    

                               }
                            

                        /**
                        * field for DeferRevRec
                        */

                        
                                    protected boolean localDeferRevRec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferRevRecTracker = false ;

                           public boolean isDeferRevRecSpecified(){
                               return localDeferRevRecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDeferRevRec(){
                               return localDeferRevRec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferRevRec
                               */
                               public void setDeferRevRec(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDeferRevRecTracker =
                                       true;
                                   
                                            this.localDeferRevRec=param;
                                    

                               }
                            

                        /**
                        * field for RevenueRecognitionRule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevenueRecognitionRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueRecognitionRuleTracker = false ;

                           public boolean isRevenueRecognitionRuleSpecified(){
                               return localRevenueRecognitionRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevenueRecognitionRule(){
                               return localRevenueRecognitionRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueRecognitionRule
                               */
                               public void setRevenueRecognitionRule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevenueRecognitionRuleTracker = param != null;
                                   
                                            this.localRevenueRecognitionRule=param;
                                    

                               }
                            

                        /**
                        * field for RevRecForecastRule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecForecastRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecForecastRuleTracker = false ;

                           public boolean isRevRecForecastRuleSpecified(){
                               return localRevRecForecastRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecForecastRule(){
                               return localRevRecForecastRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecForecastRule
                               */
                               public void setRevRecForecastRule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecForecastRuleTracker = param != null;
                                   
                                            this.localRevRecForecastRule=param;
                                    

                               }
                            

                        /**
                        * field for RevenueAllocationGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevenueAllocationGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueAllocationGroupTracker = false ;

                           public boolean isRevenueAllocationGroupSpecified(){
                               return localRevenueAllocationGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevenueAllocationGroup(){
                               return localRevenueAllocationGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueAllocationGroup
                               */
                               public void setRevenueAllocationGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevenueAllocationGroupTracker = param != null;
                                   
                                            this.localRevenueAllocationGroup=param;
                                    

                               }
                            

                        /**
                        * field for CreateRevenuePlansOn
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreateRevenuePlansOn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreateRevenuePlansOnTracker = false ;

                           public boolean isCreateRevenuePlansOnSpecified(){
                               return localCreateRevenuePlansOnTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreateRevenuePlansOn(){
                               return localCreateRevenuePlansOn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreateRevenuePlansOn
                               */
                               public void setCreateRevenuePlansOn(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreateRevenuePlansOnTracker = param != null;
                                   
                                            this.localCreateRevenuePlansOn=param;
                                    

                               }
                            

                        /**
                        * field for DirectRevenuePosting
                        */

                        
                                    protected boolean localDirectRevenuePosting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDirectRevenuePostingTracker = false ;

                           public boolean isDirectRevenuePostingSpecified(){
                               return localDirectRevenuePostingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDirectRevenuePosting(){
                               return localDirectRevenuePosting;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DirectRevenuePosting
                               */
                               public void setDirectRevenuePosting(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDirectRevenuePostingTracker =
                                       true;
                                   
                                            this.localDirectRevenuePosting=param;
                                    

                               }
                            

                        /**
                        * field for ContingentRevenueHandling
                        */

                        
                                    protected boolean localContingentRevenueHandling ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContingentRevenueHandlingTracker = false ;

                           public boolean isContingentRevenueHandlingSpecified(){
                               return localContingentRevenueHandlingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getContingentRevenueHandling(){
                               return localContingentRevenueHandling;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContingentRevenueHandling
                               */
                               public void setContingentRevenueHandling(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localContingentRevenueHandlingTracker =
                                       true;
                                   
                                            this.localContingentRevenueHandling=param;
                                    

                               }
                            

                        /**
                        * field for RevReclassFXAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevReclassFXAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevReclassFXAccountTracker = false ;

                           public boolean isRevReclassFXAccountSpecified(){
                               return localRevReclassFXAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevReclassFXAccount(){
                               return localRevReclassFXAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevReclassFXAccount
                               */
                               public void setRevReclassFXAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevReclassFXAccountTracker = param != null;
                                   
                                            this.localRevReclassFXAccount=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayName
                        */

                        
                                    protected java.lang.String localStoreDisplayName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayNameTracker = false ;

                           public boolean isStoreDisplayNameSpecified(){
                               return localStoreDisplayNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDisplayName(){
                               return localStoreDisplayName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayName
                               */
                               public void setStoreDisplayName(java.lang.String param){
                            localStoreDisplayNameTracker = param != null;
                                   
                                            this.localStoreDisplayName=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayThumbnail
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreDisplayThumbnail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayThumbnailTracker = false ;

                           public boolean isStoreDisplayThumbnailSpecified(){
                               return localStoreDisplayThumbnailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreDisplayThumbnail(){
                               return localStoreDisplayThumbnail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayThumbnail
                               */
                               public void setStoreDisplayThumbnail(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreDisplayThumbnailTracker = param != null;
                                   
                                            this.localStoreDisplayThumbnail=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayImage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreDisplayImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayImageTracker = false ;

                           public boolean isStoreDisplayImageSpecified(){
                               return localStoreDisplayImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreDisplayImage(){
                               return localStoreDisplayImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayImage
                               */
                               public void setStoreDisplayImage(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreDisplayImageTracker = param != null;
                                   
                                            this.localStoreDisplayImage=param;
                                    

                               }
                            

                        /**
                        * field for StoreDescription
                        */

                        
                                    protected java.lang.String localStoreDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDescriptionTracker = false ;

                           public boolean isStoreDescriptionSpecified(){
                               return localStoreDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDescription(){
                               return localStoreDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDescription
                               */
                               public void setStoreDescription(java.lang.String param){
                            localStoreDescriptionTracker = param != null;
                                   
                                            this.localStoreDescription=param;
                                    

                               }
                            

                        /**
                        * field for StoreDetailedDescription
                        */

                        
                                    protected java.lang.String localStoreDetailedDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDetailedDescriptionTracker = false ;

                           public boolean isStoreDetailedDescriptionSpecified(){
                               return localStoreDetailedDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDetailedDescription(){
                               return localStoreDetailedDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDetailedDescription
                               */
                               public void setStoreDetailedDescription(java.lang.String param){
                            localStoreDetailedDescriptionTracker = param != null;
                                   
                                            this.localStoreDetailedDescription=param;
                                    

                               }
                            

                        /**
                        * field for StoreItemTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreItemTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreItemTemplateTracker = false ;

                           public boolean isStoreItemTemplateSpecified(){
                               return localStoreItemTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreItemTemplate(){
                               return localStoreItemTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreItemTemplate
                               */
                               public void setStoreItemTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreItemTemplateTracker = param != null;
                                   
                                            this.localStoreItemTemplate=param;
                                    

                               }
                            

                        /**
                        * field for PageTitle
                        */

                        
                                    protected java.lang.String localPageTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPageTitleTracker = false ;

                           public boolean isPageTitleSpecified(){
                               return localPageTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPageTitle(){
                               return localPageTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PageTitle
                               */
                               public void setPageTitle(java.lang.String param){
                            localPageTitleTracker = param != null;
                                   
                                            this.localPageTitle=param;
                                    

                               }
                            

                        /**
                        * field for MetaTagHtml
                        */

                        
                                    protected java.lang.String localMetaTagHtml ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMetaTagHtmlTracker = false ;

                           public boolean isMetaTagHtmlSpecified(){
                               return localMetaTagHtmlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMetaTagHtml(){
                               return localMetaTagHtml;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MetaTagHtml
                               */
                               public void setMetaTagHtml(java.lang.String param){
                            localMetaTagHtmlTracker = param != null;
                                   
                                            this.localMetaTagHtml=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeFromSitemap
                        */

                        
                                    protected boolean localExcludeFromSitemap ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeFromSitemapTracker = false ;

                           public boolean isExcludeFromSitemapSpecified(){
                               return localExcludeFromSitemapTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExcludeFromSitemap(){
                               return localExcludeFromSitemap;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeFromSitemap
                               */
                               public void setExcludeFromSitemap(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExcludeFromSitemapTracker =
                                       true;
                                   
                                            this.localExcludeFromSitemap=param;
                                    

                               }
                            

                        /**
                        * field for SitemapPriority
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority localSitemapPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSitemapPriorityTracker = false ;

                           public boolean isSitemapPrioritySpecified(){
                               return localSitemapPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority getSitemapPriority(){
                               return localSitemapPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SitemapPriority
                               */
                               public void setSitemapPriority(com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority param){
                            localSitemapPriorityTracker = param != null;
                                   
                                            this.localSitemapPriority=param;
                                    

                               }
                            

                        /**
                        * field for SearchKeywords
                        */

                        
                                    protected java.lang.String localSearchKeywords ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchKeywordsTracker = false ;

                           public boolean isSearchKeywordsSpecified(){
                               return localSearchKeywordsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSearchKeywords(){
                               return localSearchKeywords;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchKeywords
                               */
                               public void setSearchKeywords(java.lang.String param){
                            localSearchKeywordsTracker = param != null;
                                   
                                            this.localSearchKeywords=param;
                                    

                               }
                            

                        /**
                        * field for IsDonationItem
                        */

                        
                                    protected boolean localIsDonationItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDonationItemTracker = false ;

                           public boolean isIsDonationItemSpecified(){
                               return localIsDonationItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsDonationItem(){
                               return localIsDonationItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDonationItem
                               */
                               public void setIsDonationItem(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsDonationItemTracker =
                                       true;
                                   
                                            this.localIsDonationItem=param;
                                    

                               }
                            

                        /**
                        * field for ShowDefaultDonationAmount
                        */

                        
                                    protected boolean localShowDefaultDonationAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowDefaultDonationAmountTracker = false ;

                           public boolean isShowDefaultDonationAmountSpecified(){
                               return localShowDefaultDonationAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowDefaultDonationAmount(){
                               return localShowDefaultDonationAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowDefaultDonationAmount
                               */
                               public void setShowDefaultDonationAmount(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowDefaultDonationAmountTracker =
                                       true;
                                   
                                            this.localShowDefaultDonationAmount=param;
                                    

                               }
                            

                        /**
                        * field for MaxDonationAmount
                        */

                        
                                    protected double localMaxDonationAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMaxDonationAmountTracker = false ;

                           public boolean isMaxDonationAmountSpecified(){
                               return localMaxDonationAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getMaxDonationAmount(){
                               return localMaxDonationAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MaxDonationAmount
                               */
                               public void setMaxDonationAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localMaxDonationAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localMaxDonationAmount=param;
                                    

                               }
                            

                        /**
                        * field for DontShowPrice
                        */

                        
                                    protected boolean localDontShowPrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDontShowPriceTracker = false ;

                           public boolean isDontShowPriceSpecified(){
                               return localDontShowPriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDontShowPrice(){
                               return localDontShowPrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DontShowPrice
                               */
                               public void setDontShowPrice(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDontShowPriceTracker =
                                       true;
                                   
                                            this.localDontShowPrice=param;
                                    

                               }
                            

                        /**
                        * field for NoPriceMessage
                        */

                        
                                    protected java.lang.String localNoPriceMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNoPriceMessageTracker = false ;

                           public boolean isNoPriceMessageSpecified(){
                               return localNoPriceMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNoPriceMessage(){
                               return localNoPriceMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NoPriceMessage
                               */
                               public void setNoPriceMessage(java.lang.String param){
                            localNoPriceMessageTracker = param != null;
                                   
                                            this.localNoPriceMessage=param;
                                    

                               }
                            

                        /**
                        * field for OutOfStockMessage
                        */

                        
                                    protected java.lang.String localOutOfStockMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOutOfStockMessageTracker = false ;

                           public boolean isOutOfStockMessageSpecified(){
                               return localOutOfStockMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOutOfStockMessage(){
                               return localOutOfStockMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OutOfStockMessage
                               */
                               public void setOutOfStockMessage(java.lang.String param){
                            localOutOfStockMessageTracker = param != null;
                                   
                                            this.localOutOfStockMessage=param;
                                    

                               }
                            

                        /**
                        * field for OnSpecial
                        */

                        
                                    protected boolean localOnSpecial ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOnSpecialTracker = false ;

                           public boolean isOnSpecialSpecified(){
                               return localOnSpecialTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getOnSpecial(){
                               return localOnSpecial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OnSpecial
                               */
                               public void setOnSpecial(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localOnSpecialTracker =
                                       true;
                                   
                                            this.localOnSpecial=param;
                                    

                               }
                            

                        /**
                        * field for OutOfStockBehavior
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior localOutOfStockBehavior ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOutOfStockBehaviorTracker = false ;

                           public boolean isOutOfStockBehaviorSpecified(){
                               return localOutOfStockBehaviorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior getOutOfStockBehavior(){
                               return localOutOfStockBehavior;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OutOfStockBehavior
                               */
                               public void setOutOfStockBehavior(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior param){
                            localOutOfStockBehaviorTracker = param != null;
                                   
                                            this.localOutOfStockBehavior=param;
                                    

                               }
                            

                        /**
                        * field for RelatedItemsDescription
                        */

                        
                                    protected java.lang.String localRelatedItemsDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelatedItemsDescriptionTracker = false ;

                           public boolean isRelatedItemsDescriptionSpecified(){
                               return localRelatedItemsDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRelatedItemsDescription(){
                               return localRelatedItemsDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RelatedItemsDescription
                               */
                               public void setRelatedItemsDescription(java.lang.String param){
                            localRelatedItemsDescriptionTracker = param != null;
                                   
                                            this.localRelatedItemsDescription=param;
                                    

                               }
                            

                        /**
                        * field for SpecialsDescription
                        */

                        
                                    protected java.lang.String localSpecialsDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSpecialsDescriptionTracker = false ;

                           public boolean isSpecialsDescriptionSpecified(){
                               return localSpecialsDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSpecialsDescription(){
                               return localSpecialsDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SpecialsDescription
                               */
                               public void setSpecialsDescription(java.lang.String param){
                            localSpecialsDescriptionTracker = param != null;
                                   
                                            this.localSpecialsDescription=param;
                                    

                               }
                            

                        /**
                        * field for FeaturedDescription
                        */

                        
                                    protected java.lang.String localFeaturedDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFeaturedDescriptionTracker = false ;

                           public boolean isFeaturedDescriptionSpecified(){
                               return localFeaturedDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFeaturedDescription(){
                               return localFeaturedDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FeaturedDescription
                               */
                               public void setFeaturedDescription(java.lang.String param){
                            localFeaturedDescriptionTracker = param != null;
                                   
                                            this.localFeaturedDescription=param;
                                    

                               }
                            

                        /**
                        * field for ShoppingDotComCategory
                        */

                        
                                    protected java.lang.String localShoppingDotComCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShoppingDotComCategoryTracker = false ;

                           public boolean isShoppingDotComCategorySpecified(){
                               return localShoppingDotComCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getShoppingDotComCategory(){
                               return localShoppingDotComCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShoppingDotComCategory
                               */
                               public void setShoppingDotComCategory(java.lang.String param){
                            localShoppingDotComCategoryTracker = param != null;
                                   
                                            this.localShoppingDotComCategory=param;
                                    

                               }
                            

                        /**
                        * field for ShopzillaCategoryId
                        */

                        
                                    protected long localShopzillaCategoryId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShopzillaCategoryIdTracker = false ;

                           public boolean isShopzillaCategoryIdSpecified(){
                               return localShopzillaCategoryIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getShopzillaCategoryId(){
                               return localShopzillaCategoryId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShopzillaCategoryId
                               */
                               public void setShopzillaCategoryId(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localShopzillaCategoryIdTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localShopzillaCategoryId=param;
                                    

                               }
                            

                        /**
                        * field for NexTagCategory
                        */

                        
                                    protected java.lang.String localNexTagCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNexTagCategoryTracker = false ;

                           public boolean isNexTagCategorySpecified(){
                               return localNexTagCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNexTagCategory(){
                               return localNexTagCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NexTagCategory
                               */
                               public void setNexTagCategory(java.lang.String param){
                            localNexTagCategoryTracker = param != null;
                                   
                                            this.localNexTagCategory=param;
                                    

                               }
                            

                        /**
                        * field for ProductFeedList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ProductFeedList localProductFeedList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProductFeedListTracker = false ;

                           public boolean isProductFeedListSpecified(){
                               return localProductFeedListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ProductFeedList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ProductFeedList getProductFeedList(){
                               return localProductFeedList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProductFeedList
                               */
                               public void setProductFeedList(com.netsuite.webservices.lists.accounting_2017_2.ProductFeedList param){
                            localProductFeedListTracker = param != null;
                                   
                                            this.localProductFeedList=param;
                                    

                               }
                            

                        /**
                        * field for UrlComponent
                        */

                        
                                    protected java.lang.String localUrlComponent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUrlComponentTracker = false ;

                           public boolean isUrlComponentSpecified(){
                               return localUrlComponentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUrlComponent(){
                               return localUrlComponent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UrlComponent
                               */
                               public void setUrlComponent(java.lang.String param){
                            localUrlComponentTracker = param != null;
                                   
                                            this.localUrlComponent=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for ItemId
                        */

                        
                                    protected java.lang.String localItemId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemIdTracker = false ;

                           public boolean isItemIdSpecified(){
                               return localItemIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getItemId(){
                               return localItemId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemId
                               */
                               public void setItemId(java.lang.String param){
                            localItemIdTracker = param != null;
                                   
                                            this.localItemId=param;
                                    

                               }
                            

                        /**
                        * field for UpcCode
                        */

                        
                                    protected java.lang.String localUpcCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUpcCodeTracker = false ;

                           public boolean isUpcCodeSpecified(){
                               return localUpcCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUpcCode(){
                               return localUpcCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UpcCode
                               */
                               public void setUpcCode(java.lang.String param){
                            localUpcCodeTracker = param != null;
                                   
                                            this.localUpcCode=param;
                                    

                               }
                            

                        /**
                        * field for DisplayName
                        */

                        
                                    protected java.lang.String localDisplayName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDisplayNameTracker = false ;

                           public boolean isDisplayNameSpecified(){
                               return localDisplayNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDisplayName(){
                               return localDisplayName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DisplayName
                               */
                               public void setDisplayName(java.lang.String param){
                            localDisplayNameTracker = param != null;
                                   
                                            this.localDisplayName=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for IsOnline
                        */

                        
                                    protected boolean localIsOnline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOnlineTracker = false ;

                           public boolean isIsOnlineSpecified(){
                               return localIsOnlineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsOnline(){
                               return localIsOnline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOnline
                               */
                               public void setIsOnline(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsOnlineTracker =
                                       true;
                                   
                                            this.localIsOnline=param;
                                    

                               }
                            

                        /**
                        * field for IsGcoCompliant
                        */

                        
                                    protected boolean localIsGcoCompliant ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsGcoCompliantTracker = false ;

                           public boolean isIsGcoCompliantSpecified(){
                               return localIsGcoCompliantTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsGcoCompliant(){
                               return localIsGcoCompliant;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsGcoCompliant
                               */
                               public void setIsGcoCompliant(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsGcoCompliantTracker =
                                       true;
                                   
                                            this.localIsGcoCompliant=param;
                                    

                               }
                            

                        /**
                        * field for OfferSupport
                        */

                        
                                    protected boolean localOfferSupport ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOfferSupportTracker = false ;

                           public boolean isOfferSupportSpecified(){
                               return localOfferSupportTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getOfferSupport(){
                               return localOfferSupport;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OfferSupport
                               */
                               public void setOfferSupport(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localOfferSupportTracker =
                                       true;
                                   
                                            this.localOfferSupport=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for MatrixItemNameTemplate
                        */

                        
                                    protected java.lang.String localMatrixItemNameTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatrixItemNameTemplateTracker = false ;

                           public boolean isMatrixItemNameTemplateSpecified(){
                               return localMatrixItemNameTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMatrixItemNameTemplate(){
                               return localMatrixItemNameTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatrixItemNameTemplate
                               */
                               public void setMatrixItemNameTemplate(java.lang.String param){
                            localMatrixItemNameTemplateTracker = param != null;
                                   
                                            this.localMatrixItemNameTemplate=param;
                                    

                               }
                            

                        /**
                        * field for AvailableToPartners
                        */

                        
                                    protected boolean localAvailableToPartners ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAvailableToPartnersTracker = false ;

                           public boolean isAvailableToPartnersSpecified(){
                               return localAvailableToPartnersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAvailableToPartners(){
                               return localAvailableToPartners;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AvailableToPartners
                               */
                               public void setAvailableToPartners(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAvailableToPartnersTracker =
                                       true;
                                   
                                            this.localAvailableToPartners=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for DefaultItemShipMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDefaultItemShipMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultItemShipMethodTracker = false ;

                           public boolean isDefaultItemShipMethodSpecified(){
                               return localDefaultItemShipMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDefaultItemShipMethod(){
                               return localDefaultItemShipMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultItemShipMethod
                               */
                               public void setDefaultItemShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDefaultItemShipMethodTracker = param != null;
                                   
                                            this.localDefaultItemShipMethod=param;
                                    

                               }
                            

                        /**
                        * field for ItemCarrier
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.ShippingCarrier localItemCarrier ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemCarrierTracker = false ;

                           public boolean isItemCarrierSpecified(){
                               return localItemCarrierTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.ShippingCarrier
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.ShippingCarrier getItemCarrier(){
                               return localItemCarrier;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemCarrier
                               */
                               public void setItemCarrier(com.netsuite.webservices.platform.common_2017_2.types.ShippingCarrier param){
                            localItemCarrierTracker = param != null;
                                   
                                            this.localItemCarrier=param;
                                    

                               }
                            

                        /**
                        * field for ItemShipMethodList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localItemShipMethodList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemShipMethodListTracker = false ;

                           public boolean isItemShipMethodListSpecified(){
                               return localItemShipMethodListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getItemShipMethodList(){
                               return localItemShipMethodList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemShipMethodList
                               */
                               public void setItemShipMethodList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localItemShipMethodListTracker = param != null;
                                   
                                            this.localItemShipMethodList=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localSubsidiaryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryListTracker = false ;

                           public boolean isSubsidiaryListSpecified(){
                               return localSubsidiaryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getSubsidiaryList(){
                               return localSubsidiaryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryList
                               */
                               public void setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localSubsidiaryListTracker = param != null;
                                   
                                            this.localSubsidiaryList=param;
                                    

                               }
                            

                        /**
                        * field for ItemOptionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList localItemOptionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemOptionsListTracker = false ;

                           public boolean isItemOptionsListSpecified(){
                               return localItemOptionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList getItemOptionsList(){
                               return localItemOptionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemOptionsList
                               */
                               public void setItemOptionsList(com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList param){
                            localItemOptionsListTracker = param != null;
                                   
                                            this.localItemOptionsList=param;
                                    

                               }
                            

                        /**
                        * field for MatrixOptionList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList localMatrixOptionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatrixOptionListTracker = false ;

                           public boolean isMatrixOptionListSpecified(){
                               return localMatrixOptionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList getMatrixOptionList(){
                               return localMatrixOptionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatrixOptionList
                               */
                               public void setMatrixOptionList(com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList param){
                            localMatrixOptionListTracker = param != null;
                                   
                                            this.localMatrixOptionList=param;
                                    

                               }
                            

                        /**
                        * field for PricingMatrix
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix localPricingMatrix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingMatrixTracker = false ;

                           public boolean isPricingMatrixSpecified(){
                               return localPricingMatrixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix getPricingMatrix(){
                               return localPricingMatrix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingMatrix
                               */
                               public void setPricingMatrix(com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix param){
                            localPricingMatrixTracker = param != null;
                                   
                                            this.localPricingMatrix=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPurchaseTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseTaxCodeTracker = false ;

                           public boolean isPurchaseTaxCodeSpecified(){
                               return localPurchaseTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPurchaseTaxCode(){
                               return localPurchaseTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseTaxCode
                               */
                               public void setPurchaseTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPurchaseTaxCodeTracker = param != null;
                                   
                                            this.localPurchaseTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected double localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for SalesTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTaxCodeTracker = false ;

                           public boolean isSalesTaxCodeSpecified(){
                               return localSalesTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesTaxCode(){
                               return localSalesTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTaxCode
                               */
                               public void setSalesTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesTaxCodeTracker = param != null;
                                   
                                            this.localSalesTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for SiteCategoryList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList localSiteCategoryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSiteCategoryListTracker = false ;

                           public boolean isSiteCategoryListSpecified(){
                               return localSiteCategoryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList getSiteCategoryList(){
                               return localSiteCategoryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SiteCategoryList
                               */
                               public void setSiteCategoryList(com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList param){
                            localSiteCategoryListTracker = param != null;
                                   
                                            this.localSiteCategoryList=param;
                                    

                               }
                            

                        /**
                        * field for TranslationsList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.TranslationList localTranslationsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranslationsListTracker = false ;

                           public boolean isTranslationsListSpecified(){
                               return localTranslationsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.TranslationList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.TranslationList getTranslationsList(){
                               return localTranslationsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranslationsList
                               */
                               public void setTranslationsList(com.netsuite.webservices.lists.accounting_2017_2.TranslationList param){
                            localTranslationsListTracker = param != null;
                                   
                                            this.localTranslationsList=param;
                                    

                               }
                            

                        /**
                        * field for PresentationItemList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList localPresentationItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPresentationItemListTracker = false ;

                           public boolean isPresentationItemListSpecified(){
                               return localPresentationItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList getPresentationItemList(){
                               return localPresentationItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PresentationItemList
                               */
                               public void setPresentationItemList(com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList param){
                            localPresentationItemListTracker = param != null;
                                   
                                            this.localPresentationItemList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":NonInventorySaleItem",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "NonInventorySaleItem",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "salesDescription", xmlWriter);
                             

                                          if (localSalesDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("salesDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSalesDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncludeChildrenTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "includeChildren", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("includeChildren cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncomeAccountTracker){
                                            if (localIncomeAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("incomeAccount cannot be null!!");
                                            }
                                           localIncomeAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","incomeAccount"),
                                               xmlWriter);
                                        } if (localIsTaxableTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMatrixTypeTracker){
                                            if (localMatrixType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("matrixType cannot be null!!");
                                            }
                                           localMatrixType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixType"),
                                               xmlWriter);
                                        } if (localTaxScheduleTracker){
                                            if (localTaxSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxSchedule cannot be null!!");
                                            }
                                           localTaxSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxSchedule"),
                                               xmlWriter);
                                        } if (localShippingCostTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localShippingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shippingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShippingCostUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shippingCostUnits", xmlWriter);
                             

                                          if (localShippingCostUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shippingCostUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShippingCostUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingCostTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHandlingCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("handlingCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHandlingCostUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "handlingCostUnits", xmlWriter);
                             

                                          if (localHandlingCostUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("handlingCostUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHandlingCostUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostEstimateTypeTracker){
                                            if (localCostEstimateType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                            }
                                           localCostEstimateType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimateType"),
                                               xmlWriter);
                                        } if (localCostEstimateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costEstimate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCostEstimate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("costEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWeightTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "weight", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localWeight)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("weight cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeight));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWeightUnitTracker){
                                            if (localWeightUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("weightUnit cannot be null!!");
                                            }
                                           localWeightUnit.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","weightUnit"),
                                               xmlWriter);
                                        } if (localWeightUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "weightUnits", xmlWriter);
                             

                                          if (localWeightUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("weightUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localWeightUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostEstimateUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costEstimateUnits", xmlWriter);
                             

                                          if (localCostEstimateUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("costEstimateUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCostEstimateUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnitsTypeTracker){
                                            if (localUnitsType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("unitsType cannot be null!!");
                                            }
                                           localUnitsType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","unitsType"),
                                               xmlWriter);
                                        } if (localSaleUnitTracker){
                                            if (localSaleUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("saleUnit cannot be null!!");
                                            }
                                           localSaleUnit.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","saleUnit"),
                                               xmlWriter);
                                        } if (localIssueProductTracker){
                                            if (localIssueProduct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueProduct cannot be null!!");
                                            }
                                           localIssueProduct.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","issueProduct"),
                                               xmlWriter);
                                        } if (localBillingScheduleTracker){
                                            if (localBillingSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                            }
                                           localBillingSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billingSchedule"),
                                               xmlWriter);
                                        } if (localDeferredRevenueAccountTracker){
                                            if (localDeferredRevenueAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deferredRevenueAccount cannot be null!!");
                                            }
                                           localDeferredRevenueAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferredRevenueAccount"),
                                               xmlWriter);
                                        } if (localRevRecScheduleTracker){
                                            if (localRevRecSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                            }
                                           localRevRecSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecSchedule"),
                                               xmlWriter);
                                        } if (localStockDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "stockDescription", xmlWriter);
                             

                                          if (localStockDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("stockDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStockDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsHazmatItemTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isHazmatItem", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isHazmatItem cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsHazmatItem));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHazmatIdTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hazmatId", xmlWriter);
                             

                                          if (localHazmatId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("hazmatId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHazmatId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHazmatShippingNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hazmatShippingName", xmlWriter);
                             

                                          if (localHazmatShippingName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("hazmatShippingName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHazmatShippingName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHazmatHazardClassTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hazmatHazardClass", xmlWriter);
                             

                                          if (localHazmatHazardClass==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("hazmatHazardClass cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHazmatHazardClass);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHazmatPackingGroupTracker){
                                            if (localHazmatPackingGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hazmatPackingGroup cannot be null!!");
                                            }
                                           localHazmatPackingGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatPackingGroup"),
                                               xmlWriter);
                                        } if (localHazmatItemUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hazmatItemUnits", xmlWriter);
                             

                                          if (localHazmatItemUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("hazmatItemUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHazmatItemUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHazmatItemUnitsQtyTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hazmatItemUnitsQty", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localHazmatItemUnitsQty)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("hazmatItemUnitsQty cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHazmatItemUnitsQty));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localProducerTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "producer", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("producer cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProducer));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturerTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturer", xmlWriter);
                             

                                          if (localManufacturer==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturer cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturer);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMpnTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "mpn", xmlWriter);
                             

                                          if (localMpn==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("mpn cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMpn);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMultManufactureAddrTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "multManufactureAddr", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("multManufactureAddr cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMultManufactureAddr));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturerAddr1Tracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturerAddr1", xmlWriter);
                             

                                          if (localManufacturerAddr1==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturerAddr1 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturerAddr1);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturerCityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturerCity", xmlWriter);
                             

                                          if (localManufacturerCity==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturerCity cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturerCity);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturerStateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturerState", xmlWriter);
                             

                                          if (localManufacturerState==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturerState cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturerState);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturerZipTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturerZip", xmlWriter);
                             

                                          if (localManufacturerZip==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturerZip cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturerZip);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCountryOfManufactureTracker){
                                            if (localCountryOfManufacture==null){
                                                 throw new org.apache.axis2.databinding.ADBException("countryOfManufacture cannot be null!!");
                                            }
                                           localCountryOfManufacture.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","countryOfManufacture"),
                                               xmlWriter);
                                        } if (localManufacturerTaxIdTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturerTaxId", xmlWriter);
                             

                                          if (localManufacturerTaxId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturerTaxId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturerTaxId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localScheduleBNumberTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "scheduleBNumber", xmlWriter);
                             

                                          if (localScheduleBNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("scheduleBNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localScheduleBNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localScheduleBQuantityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "scheduleBQuantity", xmlWriter);
                             
                                               if (localScheduleBQuantity==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("scheduleBQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduleBQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localScheduleBCodeTracker){
                                            if (localScheduleBCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("scheduleBCode cannot be null!!");
                                            }
                                           localScheduleBCode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","scheduleBCode"),
                                               xmlWriter);
                                        } if (localManufacturerTariffTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturerTariff", xmlWriter);
                             

                                          if (localManufacturerTariff==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("manufacturerTariff cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localManufacturerTariff);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPreferenceCriterionTracker){
                                            if (localPreferenceCriterion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("preferenceCriterion cannot be null!!");
                                            }
                                           localPreferenceCriterion.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferenceCriterion"),
                                               xmlWriter);
                                        } if (localMinimumQuantityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "minimumQuantity", xmlWriter);
                             
                                               if (localMinimumQuantity==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("minimumQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinimumQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEnforceMinQtyInternallyTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "enforceMinQtyInternally", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("enforceMinQtyInternally cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnforceMinQtyInternally));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSoftDescriptorTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "softDescriptor", xmlWriter);
                             

                                          if (localSoftDescriptor==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("softDescriptor cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSoftDescriptor);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShipPackageTracker){
                                            if (localShipPackage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipPackage cannot be null!!");
                                            }
                                           localShipPackage.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shipPackage"),
                                               xmlWriter);
                                        } if (localShipIndividuallyTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shipIndividually", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shipIndividually cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipIndividually));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsFulfillableTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isFulfillable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isFulfillable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFulfillable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostCategoryTracker){
                                            if (localCostCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costCategory cannot be null!!");
                                            }
                                           localCostCategory.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costCategory"),
                                               xmlWriter);
                                        } if (localPricesIncludeTaxTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pricesIncludeTax", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("pricesIncludeTax cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPricesIncludeTax));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityPricingScheduleTracker){
                                            if (localQuantityPricingSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityPricingSchedule cannot be null!!");
                                            }
                                           localQuantityPricingSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityPricingSchedule"),
                                               xmlWriter);
                                        } if (localUseMarginalRatesTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useMarginalRates", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useMarginalRates cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseMarginalRates));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOverallQuantityPricingTypeTracker){
                                            if (localOverallQuantityPricingType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("overallQuantityPricingType cannot be null!!");
                                            }
                                           localOverallQuantityPricingType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","overallQuantityPricingType"),
                                               xmlWriter);
                                        } if (localPricingGroupTracker){
                                            if (localPricingGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingGroup cannot be null!!");
                                            }
                                           localPricingGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingGroup"),
                                               xmlWriter);
                                        } if (localMinimumQuantityUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "minimumQuantityUnits", xmlWriter);
                             

                                          if (localMinimumQuantityUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("minimumQuantityUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMinimumQuantityUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoePriceTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoePrice", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoePrice)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoePrice cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeSopGroupTracker){
                                            if (localVsoeSopGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                            }
                                           localVsoeSopGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeSopGroup"),
                                               xmlWriter);
                                        } if (localVsoeDeferralTracker){
                                            if (localVsoeDeferral==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                            }
                                           localVsoeDeferral.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeDeferral"),
                                               xmlWriter);
                                        } if (localVsoePermitDiscountTracker){
                                            if (localVsoePermitDiscount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                            }
                                           localVsoePermitDiscount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoePermitDiscount"),
                                               xmlWriter);
                                        } if (localVsoeDeliveredTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeDelivered", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeDelivered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemRevenueCategoryTracker){
                                            if (localItemRevenueCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemRevenueCategory cannot be null!!");
                                            }
                                           localItemRevenueCategory.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemRevenueCategory"),
                                               xmlWriter);
                                        } if (localDeferRevRecTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "deferRevRec", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("deferRevRec cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevenueRecognitionRuleTracker){
                                            if (localRevenueRecognitionRule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueRecognitionRule cannot be null!!");
                                            }
                                           localRevenueRecognitionRule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueRecognitionRule"),
                                               xmlWriter);
                                        } if (localRevRecForecastRuleTracker){
                                            if (localRevRecForecastRule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecForecastRule cannot be null!!");
                                            }
                                           localRevRecForecastRule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecForecastRule"),
                                               xmlWriter);
                                        } if (localRevenueAllocationGroupTracker){
                                            if (localRevenueAllocationGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueAllocationGroup cannot be null!!");
                                            }
                                           localRevenueAllocationGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueAllocationGroup"),
                                               xmlWriter);
                                        } if (localCreateRevenuePlansOnTracker){
                                            if (localCreateRevenuePlansOn==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createRevenuePlansOn cannot be null!!");
                                            }
                                           localCreateRevenuePlansOn.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createRevenuePlansOn"),
                                               xmlWriter);
                                        } if (localDirectRevenuePostingTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "directRevenuePosting", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("directRevenuePosting cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirectRevenuePosting));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localContingentRevenueHandlingTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "contingentRevenueHandling", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("contingentRevenueHandling cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContingentRevenueHandling));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevReclassFXAccountTracker){
                                            if (localRevReclassFXAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revReclassFXAccount cannot be null!!");
                                            }
                                           localRevReclassFXAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revReclassFXAccount"),
                                               xmlWriter);
                                        } if (localStoreDisplayNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDisplayName", xmlWriter);
                             

                                          if (localStoreDisplayName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDisplayName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDisplayName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDisplayThumbnailTracker){
                                            if (localStoreDisplayThumbnail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeDisplayThumbnail cannot be null!!");
                                            }
                                           localStoreDisplayThumbnail.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayThumbnail"),
                                               xmlWriter);
                                        } if (localStoreDisplayImageTracker){
                                            if (localStoreDisplayImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeDisplayImage cannot be null!!");
                                            }
                                           localStoreDisplayImage.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayImage"),
                                               xmlWriter);
                                        } if (localStoreDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDescription", xmlWriter);
                             

                                          if (localStoreDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDetailedDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDetailedDescription", xmlWriter);
                             

                                          if (localStoreDetailedDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDetailedDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDetailedDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreItemTemplateTracker){
                                            if (localStoreItemTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeItemTemplate cannot be null!!");
                                            }
                                           localStoreItemTemplate.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeItemTemplate"),
                                               xmlWriter);
                                        } if (localPageTitleTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pageTitle", xmlWriter);
                             

                                          if (localPageTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("pageTitle cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPageTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMetaTagHtmlTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "metaTagHtml", xmlWriter);
                             

                                          if (localMetaTagHtml==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("metaTagHtml cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMetaTagHtml);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExcludeFromSitemapTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "excludeFromSitemap", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("excludeFromSitemap cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromSitemap));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSitemapPriorityTracker){
                                            if (localSitemapPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sitemapPriority cannot be null!!");
                                            }
                                           localSitemapPriority.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","sitemapPriority"),
                                               xmlWriter);
                                        } if (localSearchKeywordsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "searchKeywords", xmlWriter);
                             

                                          if (localSearchKeywords==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("searchKeywords cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSearchKeywords);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsDonationItemTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isDonationItem", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isDonationItem cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDonationItem));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowDefaultDonationAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showDefaultDonationAmount", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showDefaultDonationAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowDefaultDonationAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMaxDonationAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "maxDonationAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localMaxDonationAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("maxDonationAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxDonationAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDontShowPriceTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dontShowPrice", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("dontShowPrice cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDontShowPrice));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNoPriceMessageTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "noPriceMessage", xmlWriter);
                             

                                          if (localNoPriceMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("noPriceMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNoPriceMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOutOfStockMessageTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "outOfStockMessage", xmlWriter);
                             

                                          if (localOutOfStockMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("outOfStockMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOutOfStockMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOnSpecialTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "onSpecial", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("onSpecial cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnSpecial));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOutOfStockBehaviorTracker){
                                            if (localOutOfStockBehavior==null){
                                                 throw new org.apache.axis2.databinding.ADBException("outOfStockBehavior cannot be null!!");
                                            }
                                           localOutOfStockBehavior.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","outOfStockBehavior"),
                                               xmlWriter);
                                        } if (localRelatedItemsDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "relatedItemsDescription", xmlWriter);
                             

                                          if (localRelatedItemsDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("relatedItemsDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRelatedItemsDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSpecialsDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "specialsDescription", xmlWriter);
                             

                                          if (localSpecialsDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("specialsDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSpecialsDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFeaturedDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "featuredDescription", xmlWriter);
                             

                                          if (localFeaturedDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("featuredDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFeaturedDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShoppingDotComCategoryTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shoppingDotComCategory", xmlWriter);
                             

                                          if (localShoppingDotComCategory==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("shoppingDotComCategory cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localShoppingDotComCategory);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShopzillaCategoryIdTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "shopzillaCategoryId", xmlWriter);
                             
                                               if (localShopzillaCategoryId==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("shopzillaCategoryId cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShopzillaCategoryId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNexTagCategoryTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "nexTagCategory", xmlWriter);
                             

                                          if (localNexTagCategory==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("nexTagCategory cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNexTagCategory);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localProductFeedListTracker){
                                            if (localProductFeedList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("productFeedList cannot be null!!");
                                            }
                                           localProductFeedList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","productFeedList"),
                                               xmlWriter);
                                        } if (localUrlComponentTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "urlComponent", xmlWriter);
                             

                                          if (localUrlComponent==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("urlComponent cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUrlComponent);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localItemIdTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "itemId", xmlWriter);
                             

                                          if (localItemId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localItemId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUpcCodeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "upcCode", xmlWriter);
                             

                                          if (localUpcCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("upcCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUpcCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDisplayNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "displayName", xmlWriter);
                             

                                          if (localDisplayName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("displayName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDisplayName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localIsOnlineTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isOnline", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isOnline cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnline));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsGcoCompliantTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isGcoCompliant", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isGcoCompliant cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsGcoCompliant));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOfferSupportTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "offerSupport", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("offerSupport cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfferSupport));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsInactiveTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMatrixItemNameTemplateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "matrixItemNameTemplate", xmlWriter);
                             

                                          if (localMatrixItemNameTemplate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("matrixItemNameTemplate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMatrixItemNameTemplate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAvailableToPartnersTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "availableToPartners", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("availableToPartners cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableToPartners));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localDefaultItemShipMethodTracker){
                                            if (localDefaultItemShipMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("defaultItemShipMethod cannot be null!!");
                                            }
                                           localDefaultItemShipMethod.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","defaultItemShipMethod"),
                                               xmlWriter);
                                        } if (localItemCarrierTracker){
                                            if (localItemCarrier==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemCarrier cannot be null!!");
                                            }
                                           localItemCarrier.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemCarrier"),
                                               xmlWriter);
                                        } if (localItemShipMethodListTracker){
                                            if (localItemShipMethodList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemShipMethodList cannot be null!!");
                                            }
                                           localItemShipMethodList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemShipMethodList"),
                                               xmlWriter);
                                        } if (localSubsidiaryListTracker){
                                            if (localSubsidiaryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                            }
                                           localSubsidiaryList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList"),
                                               xmlWriter);
                                        } if (localItemOptionsListTracker){
                                            if (localItemOptionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemOptionsList cannot be null!!");
                                            }
                                           localItemOptionsList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemOptionsList"),
                                               xmlWriter);
                                        } if (localMatrixOptionListTracker){
                                            if (localMatrixOptionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("matrixOptionList cannot be null!!");
                                            }
                                           localMatrixOptionList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixOptionList"),
                                               xmlWriter);
                                        } if (localPricingMatrixTracker){
                                            if (localPricingMatrix==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingMatrix cannot be null!!");
                                            }
                                           localPricingMatrix.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingMatrix"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localPurchaseTaxCodeTracker){
                                            if (localPurchaseTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseTaxCode cannot be null!!");
                                            }
                                           localPurchaseTaxCode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseTaxCode"),
                                               xmlWriter);
                                        } if (localRateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesTaxCodeTracker){
                                            if (localSalesTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTaxCode cannot be null!!");
                                            }
                                           localSalesTaxCode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","salesTaxCode"),
                                               xmlWriter);
                                        } if (localSiteCategoryListTracker){
                                            if (localSiteCategoryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("siteCategoryList cannot be null!!");
                                            }
                                           localSiteCategoryList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","siteCategoryList"),
                                               xmlWriter);
                                        } if (localTranslationsListTracker){
                                            if (localTranslationsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                            }
                                           localTranslationsList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","translationsList"),
                                               xmlWriter);
                                        } if (localPresentationItemListTracker){
                                            if (localPresentationItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("presentationItemList cannot be null!!");
                                            }
                                           localPresentationItemList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","presentationItemList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","NonInventorySaleItem"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localSalesDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "salesDescription"));
                                 
                                        if (localSalesDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalesDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("salesDescription cannot be null!!");
                                        }
                                    } if (localIncludeChildrenTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "includeChildren"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                            } if (localIncomeAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "incomeAccount"));
                            
                            
                                    if (localIncomeAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("incomeAccount cannot be null!!");
                                    }
                                    elementList.add(localIncomeAccount);
                                } if (localIsTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                            } if (localMatrixTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "matrixType"));
                            
                            
                                    if (localMatrixType==null){
                                         throw new org.apache.axis2.databinding.ADBException("matrixType cannot be null!!");
                                    }
                                    elementList.add(localMatrixType);
                                } if (localTaxScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "taxSchedule"));
                            
                            
                                    if (localTaxSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxSchedule cannot be null!!");
                                    }
                                    elementList.add(localTaxSchedule);
                                } if (localShippingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shippingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCost));
                            } if (localShippingCostUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shippingCostUnits"));
                                 
                                        if (localShippingCostUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShippingCostUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shippingCostUnits cannot be null!!");
                                        }
                                    } if (localHandlingCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "handlingCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCost));
                            } if (localHandlingCostUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "handlingCostUnits"));
                                 
                                        if (localHandlingCostUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHandlingCostUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("handlingCostUnits cannot be null!!");
                                        }
                                    } if (localCostEstimateTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costEstimateType"));
                            
                            
                                    if (localCostEstimateType==null){
                                         throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                    }
                                    elementList.add(localCostEstimateType);
                                } if (localCostEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                            } if (localWeightTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "weight"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeight));
                            } if (localWeightUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "weightUnit"));
                            
                            
                                    if (localWeightUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("weightUnit cannot be null!!");
                                    }
                                    elementList.add(localWeightUnit);
                                } if (localWeightUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "weightUnits"));
                                 
                                        if (localWeightUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWeightUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("weightUnits cannot be null!!");
                                        }
                                    } if (localCostEstimateUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costEstimateUnits"));
                                 
                                        if (localCostEstimateUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimateUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("costEstimateUnits cannot be null!!");
                                        }
                                    } if (localUnitsTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "unitsType"));
                            
                            
                                    if (localUnitsType==null){
                                         throw new org.apache.axis2.databinding.ADBException("unitsType cannot be null!!");
                                    }
                                    elementList.add(localUnitsType);
                                } if (localSaleUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "saleUnit"));
                            
                            
                                    if (localSaleUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("saleUnit cannot be null!!");
                                    }
                                    elementList.add(localSaleUnit);
                                } if (localIssueProductTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "issueProduct"));
                            
                            
                                    if (localIssueProduct==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueProduct cannot be null!!");
                                    }
                                    elementList.add(localIssueProduct);
                                } if (localBillingScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "billingSchedule"));
                            
                            
                                    if (localBillingSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                    }
                                    elementList.add(localBillingSchedule);
                                } if (localDeferredRevenueAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "deferredRevenueAccount"));
                            
                            
                                    if (localDeferredRevenueAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("deferredRevenueAccount cannot be null!!");
                                    }
                                    elementList.add(localDeferredRevenueAccount);
                                } if (localRevRecScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revRecSchedule"));
                            
                            
                                    if (localRevRecSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                    }
                                    elementList.add(localRevRecSchedule);
                                } if (localStockDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "stockDescription"));
                                 
                                        if (localStockDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStockDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("stockDescription cannot be null!!");
                                        }
                                    } if (localIsHazmatItemTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isHazmatItem"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsHazmatItem));
                            } if (localHazmatIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "hazmatId"));
                                 
                                        if (localHazmatId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHazmatId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("hazmatId cannot be null!!");
                                        }
                                    } if (localHazmatShippingNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "hazmatShippingName"));
                                 
                                        if (localHazmatShippingName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHazmatShippingName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("hazmatShippingName cannot be null!!");
                                        }
                                    } if (localHazmatHazardClassTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "hazmatHazardClass"));
                                 
                                        if (localHazmatHazardClass != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHazmatHazardClass));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("hazmatHazardClass cannot be null!!");
                                        }
                                    } if (localHazmatPackingGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "hazmatPackingGroup"));
                            
                            
                                    if (localHazmatPackingGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("hazmatPackingGroup cannot be null!!");
                                    }
                                    elementList.add(localHazmatPackingGroup);
                                } if (localHazmatItemUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "hazmatItemUnits"));
                                 
                                        if (localHazmatItemUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHazmatItemUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("hazmatItemUnits cannot be null!!");
                                        }
                                    } if (localHazmatItemUnitsQtyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "hazmatItemUnitsQty"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHazmatItemUnitsQty));
                            } if (localProducerTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "producer"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProducer));
                            } if (localManufacturerTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturer"));
                                 
                                        if (localManufacturer != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturer));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturer cannot be null!!");
                                        }
                                    } if (localMpnTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "mpn"));
                                 
                                        if (localMpn != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMpn));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("mpn cannot be null!!");
                                        }
                                    } if (localMultManufactureAddrTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "multManufactureAddr"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMultManufactureAddr));
                            } if (localManufacturerAddr1Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturerAddr1"));
                                 
                                        if (localManufacturerAddr1 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturerAddr1));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturerAddr1 cannot be null!!");
                                        }
                                    } if (localManufacturerCityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturerCity"));
                                 
                                        if (localManufacturerCity != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturerCity));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturerCity cannot be null!!");
                                        }
                                    } if (localManufacturerStateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturerState"));
                                 
                                        if (localManufacturerState != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturerState));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturerState cannot be null!!");
                                        }
                                    } if (localManufacturerZipTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturerZip"));
                                 
                                        if (localManufacturerZip != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturerZip));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturerZip cannot be null!!");
                                        }
                                    } if (localCountryOfManufactureTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "countryOfManufacture"));
                            
                            
                                    if (localCountryOfManufacture==null){
                                         throw new org.apache.axis2.databinding.ADBException("countryOfManufacture cannot be null!!");
                                    }
                                    elementList.add(localCountryOfManufacture);
                                } if (localManufacturerTaxIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturerTaxId"));
                                 
                                        if (localManufacturerTaxId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturerTaxId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturerTaxId cannot be null!!");
                                        }
                                    } if (localScheduleBNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "scheduleBNumber"));
                                 
                                        if (localScheduleBNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduleBNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("scheduleBNumber cannot be null!!");
                                        }
                                    } if (localScheduleBQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "scheduleBQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScheduleBQuantity));
                            } if (localScheduleBCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "scheduleBCode"));
                            
                            
                                    if (localScheduleBCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("scheduleBCode cannot be null!!");
                                    }
                                    elementList.add(localScheduleBCode);
                                } if (localManufacturerTariffTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturerTariff"));
                                 
                                        if (localManufacturerTariff != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturerTariff));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("manufacturerTariff cannot be null!!");
                                        }
                                    } if (localPreferenceCriterionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "preferenceCriterion"));
                            
                            
                                    if (localPreferenceCriterion==null){
                                         throw new org.apache.axis2.databinding.ADBException("preferenceCriterion cannot be null!!");
                                    }
                                    elementList.add(localPreferenceCriterion);
                                } if (localMinimumQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "minimumQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinimumQuantity));
                            } if (localEnforceMinQtyInternallyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "enforceMinQtyInternally"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnforceMinQtyInternally));
                            } if (localSoftDescriptorTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "softDescriptor"));
                                 
                                        if (localSoftDescriptor != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSoftDescriptor));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("softDescriptor cannot be null!!");
                                        }
                                    } if (localShipPackageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shipPackage"));
                            
                            
                                    if (localShipPackage==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipPackage cannot be null!!");
                                    }
                                    elementList.add(localShipPackage);
                                } if (localShipIndividuallyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shipIndividually"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShipIndividually));
                            } if (localIsFulfillableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isFulfillable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFulfillable));
                            } if (localCostCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costCategory"));
                            
                            
                                    if (localCostCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("costCategory cannot be null!!");
                                    }
                                    elementList.add(localCostCategory);
                                } if (localPricesIncludeTaxTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricesIncludeTax"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPricesIncludeTax));
                            } if (localQuantityPricingScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityPricingSchedule"));
                            
                            
                                    if (localQuantityPricingSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityPricingSchedule cannot be null!!");
                                    }
                                    elementList.add(localQuantityPricingSchedule);
                                } if (localUseMarginalRatesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "useMarginalRates"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseMarginalRates));
                            } if (localOverallQuantityPricingTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "overallQuantityPricingType"));
                            
                            
                                    if (localOverallQuantityPricingType==null){
                                         throw new org.apache.axis2.databinding.ADBException("overallQuantityPricingType cannot be null!!");
                                    }
                                    elementList.add(localOverallQuantityPricingType);
                                } if (localPricingGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricingGroup"));
                            
                            
                                    if (localPricingGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingGroup cannot be null!!");
                                    }
                                    elementList.add(localPricingGroup);
                                } if (localMinimumQuantityUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "minimumQuantityUnits"));
                                 
                                        if (localMinimumQuantityUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinimumQuantityUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("minimumQuantityUnits cannot be null!!");
                                        }
                                    } if (localVsoePriceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoePrice"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                            } if (localVsoeSopGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoeSopGroup"));
                            
                            
                                    if (localVsoeSopGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                    }
                                    elementList.add(localVsoeSopGroup);
                                } if (localVsoeDeferralTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoeDeferral"));
                            
                            
                                    if (localVsoeDeferral==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                    }
                                    elementList.add(localVsoeDeferral);
                                } if (localVsoePermitDiscountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoePermitDiscount"));
                            
                            
                                    if (localVsoePermitDiscount==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                    }
                                    elementList.add(localVsoePermitDiscount);
                                } if (localVsoeDeliveredTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoeDelivered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                            } if (localItemRevenueCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemRevenueCategory"));
                            
                            
                                    if (localItemRevenueCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemRevenueCategory cannot be null!!");
                                    }
                                    elementList.add(localItemRevenueCategory);
                                } if (localDeferRevRecTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "deferRevRec"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                            } if (localRevenueRecognitionRuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revenueRecognitionRule"));
                            
                            
                                    if (localRevenueRecognitionRule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueRecognitionRule cannot be null!!");
                                    }
                                    elementList.add(localRevenueRecognitionRule);
                                } if (localRevRecForecastRuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revRecForecastRule"));
                            
                            
                                    if (localRevRecForecastRule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecForecastRule cannot be null!!");
                                    }
                                    elementList.add(localRevRecForecastRule);
                                } if (localRevenueAllocationGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revenueAllocationGroup"));
                            
                            
                                    if (localRevenueAllocationGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueAllocationGroup cannot be null!!");
                                    }
                                    elementList.add(localRevenueAllocationGroup);
                                } if (localCreateRevenuePlansOnTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "createRevenuePlansOn"));
                            
                            
                                    if (localCreateRevenuePlansOn==null){
                                         throw new org.apache.axis2.databinding.ADBException("createRevenuePlansOn cannot be null!!");
                                    }
                                    elementList.add(localCreateRevenuePlansOn);
                                } if (localDirectRevenuePostingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "directRevenuePosting"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirectRevenuePosting));
                            } if (localContingentRevenueHandlingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "contingentRevenueHandling"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContingentRevenueHandling));
                            } if (localRevReclassFXAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revReclassFXAccount"));
                            
                            
                                    if (localRevReclassFXAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("revReclassFXAccount cannot be null!!");
                                    }
                                    elementList.add(localRevReclassFXAccount);
                                } if (localStoreDisplayNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayName"));
                                 
                                        if (localStoreDisplayName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDisplayName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDisplayName cannot be null!!");
                                        }
                                    } if (localStoreDisplayThumbnailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayThumbnail"));
                            
                            
                                    if (localStoreDisplayThumbnail==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeDisplayThumbnail cannot be null!!");
                                    }
                                    elementList.add(localStoreDisplayThumbnail);
                                } if (localStoreDisplayImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayImage"));
                            
                            
                                    if (localStoreDisplayImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeDisplayImage cannot be null!!");
                                    }
                                    elementList.add(localStoreDisplayImage);
                                } if (localStoreDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDescription"));
                                 
                                        if (localStoreDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDescription cannot be null!!");
                                        }
                                    } if (localStoreDetailedDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDetailedDescription"));
                                 
                                        if (localStoreDetailedDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDetailedDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDetailedDescription cannot be null!!");
                                        }
                                    } if (localStoreItemTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeItemTemplate"));
                            
                            
                                    if (localStoreItemTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeItemTemplate cannot be null!!");
                                    }
                                    elementList.add(localStoreItemTemplate);
                                } if (localPageTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pageTitle"));
                                 
                                        if (localPageTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPageTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("pageTitle cannot be null!!");
                                        }
                                    } if (localMetaTagHtmlTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "metaTagHtml"));
                                 
                                        if (localMetaTagHtml != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMetaTagHtml));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("metaTagHtml cannot be null!!");
                                        }
                                    } if (localExcludeFromSitemapTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "excludeFromSitemap"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromSitemap));
                            } if (localSitemapPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "sitemapPriority"));
                            
                            
                                    if (localSitemapPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("sitemapPriority cannot be null!!");
                                    }
                                    elementList.add(localSitemapPriority);
                                } if (localSearchKeywordsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "searchKeywords"));
                                 
                                        if (localSearchKeywords != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSearchKeywords));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("searchKeywords cannot be null!!");
                                        }
                                    } if (localIsDonationItemTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isDonationItem"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDonationItem));
                            } if (localShowDefaultDonationAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "showDefaultDonationAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowDefaultDonationAmount));
                            } if (localMaxDonationAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "maxDonationAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxDonationAmount));
                            } if (localDontShowPriceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "dontShowPrice"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDontShowPrice));
                            } if (localNoPriceMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "noPriceMessage"));
                                 
                                        if (localNoPriceMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoPriceMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("noPriceMessage cannot be null!!");
                                        }
                                    } if (localOutOfStockMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "outOfStockMessage"));
                                 
                                        if (localOutOfStockMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOutOfStockMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("outOfStockMessage cannot be null!!");
                                        }
                                    } if (localOnSpecialTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "onSpecial"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnSpecial));
                            } if (localOutOfStockBehaviorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "outOfStockBehavior"));
                            
                            
                                    if (localOutOfStockBehavior==null){
                                         throw new org.apache.axis2.databinding.ADBException("outOfStockBehavior cannot be null!!");
                                    }
                                    elementList.add(localOutOfStockBehavior);
                                } if (localRelatedItemsDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "relatedItemsDescription"));
                                 
                                        if (localRelatedItemsDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRelatedItemsDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("relatedItemsDescription cannot be null!!");
                                        }
                                    } if (localSpecialsDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "specialsDescription"));
                                 
                                        if (localSpecialsDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSpecialsDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("specialsDescription cannot be null!!");
                                        }
                                    } if (localFeaturedDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "featuredDescription"));
                                 
                                        if (localFeaturedDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFeaturedDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("featuredDescription cannot be null!!");
                                        }
                                    } if (localShoppingDotComCategoryTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shoppingDotComCategory"));
                                 
                                        if (localShoppingDotComCategory != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShoppingDotComCategory));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("shoppingDotComCategory cannot be null!!");
                                        }
                                    } if (localShopzillaCategoryIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "shopzillaCategoryId"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShopzillaCategoryId));
                            } if (localNexTagCategoryTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "nexTagCategory"));
                                 
                                        if (localNexTagCategory != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNexTagCategory));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("nexTagCategory cannot be null!!");
                                        }
                                    } if (localProductFeedListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "productFeedList"));
                            
                            
                                    if (localProductFeedList==null){
                                         throw new org.apache.axis2.databinding.ADBException("productFeedList cannot be null!!");
                                    }
                                    elementList.add(localProductFeedList);
                                } if (localUrlComponentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "urlComponent"));
                                 
                                        if (localUrlComponent != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUrlComponent));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("urlComponent cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localItemIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemId"));
                                 
                                        if (localItemId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                        }
                                    } if (localUpcCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "upcCode"));
                                 
                                        if (localUpcCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUpcCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("upcCode cannot be null!!");
                                        }
                                    } if (localDisplayNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "displayName"));
                                 
                                        if (localDisplayName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisplayName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("displayName cannot be null!!");
                                        }
                                    } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localIsOnlineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isOnline"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnline));
                            } if (localIsGcoCompliantTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isGcoCompliant"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsGcoCompliant));
                            } if (localOfferSupportTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "offerSupport"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfferSupport));
                            } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localMatrixItemNameTemplateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "matrixItemNameTemplate"));
                                 
                                        if (localMatrixItemNameTemplate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMatrixItemNameTemplate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("matrixItemNameTemplate cannot be null!!");
                                        }
                                    } if (localAvailableToPartnersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "availableToPartners"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableToPartners));
                            } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localDefaultItemShipMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "defaultItemShipMethod"));
                            
                            
                                    if (localDefaultItemShipMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("defaultItemShipMethod cannot be null!!");
                                    }
                                    elementList.add(localDefaultItemShipMethod);
                                } if (localItemCarrierTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemCarrier"));
                            
                            
                                    if (localItemCarrier==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemCarrier cannot be null!!");
                                    }
                                    elementList.add(localItemCarrier);
                                } if (localItemShipMethodListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemShipMethodList"));
                            
                            
                                    if (localItemShipMethodList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemShipMethodList cannot be null!!");
                                    }
                                    elementList.add(localItemShipMethodList);
                                } if (localSubsidiaryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiaryList"));
                            
                            
                                    if (localSubsidiaryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryList);
                                } if (localItemOptionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemOptionsList"));
                            
                            
                                    if (localItemOptionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemOptionsList cannot be null!!");
                                    }
                                    elementList.add(localItemOptionsList);
                                } if (localMatrixOptionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "matrixOptionList"));
                            
                            
                                    if (localMatrixOptionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("matrixOptionList cannot be null!!");
                                    }
                                    elementList.add(localMatrixOptionList);
                                } if (localPricingMatrixTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricingMatrix"));
                            
                            
                                    if (localPricingMatrix==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingMatrix cannot be null!!");
                                    }
                                    elementList.add(localPricingMatrix);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localPurchaseTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseTaxCode"));
                            
                            
                                    if (localPurchaseTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseTaxCode cannot be null!!");
                                    }
                                    elementList.add(localPurchaseTaxCode);
                                } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                            } if (localSalesTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "salesTaxCode"));
                            
                            
                                    if (localSalesTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTaxCode cannot be null!!");
                                    }
                                    elementList.add(localSalesTaxCode);
                                } if (localSiteCategoryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "siteCategoryList"));
                            
                            
                                    if (localSiteCategoryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("siteCategoryList cannot be null!!");
                                    }
                                    elementList.add(localSiteCategoryList);
                                } if (localTranslationsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "translationsList"));
                            
                            
                                    if (localTranslationsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                    }
                                    elementList.add(localTranslationsList);
                                } if (localPresentationItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "presentationItemList"));
                            
                            
                                    if (localPresentationItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("presentationItemList cannot be null!!");
                                    }
                                    elementList.add(localPresentationItemList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static NonInventorySaleItem parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            NonInventorySaleItem object =
                new NonInventorySaleItem();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"NonInventorySaleItem".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (NonInventorySaleItem)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","salesDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"salesDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSalesDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","includeChildren").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"includeChildren" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncludeChildren(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","incomeAccount").equals(reader.getName())){
                                
                                                object.setIncomeAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixType").equals(reader.getName())){
                                
                                                object.setMatrixType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxSchedule").equals(reader.getName())){
                                
                                                object.setTaxSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shippingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShippingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shippingCostUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shippingCostUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShippingCostUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","handlingCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHandlingCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","handlingCostUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"handlingCostUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHandlingCostUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimateType").equals(reader.getName())){
                                
                                                object.setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCostEstimate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","weight").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"weight" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setWeight(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setWeight(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","weightUnit").equals(reader.getName())){
                                
                                                object.setWeightUnit(com.netsuite.webservices.lists.accounting_2017_2.types.ItemWeightUnit.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","weightUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"weightUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setWeightUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimateUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costEstimateUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostEstimateUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","unitsType").equals(reader.getName())){
                                
                                                object.setUnitsType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","saleUnit").equals(reader.getName())){
                                
                                                object.setSaleUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","issueProduct").equals(reader.getName())){
                                
                                                object.setIssueProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billingSchedule").equals(reader.getName())){
                                
                                                object.setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferredRevenueAccount").equals(reader.getName())){
                                
                                                object.setDeferredRevenueAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecSchedule").equals(reader.getName())){
                                
                                                object.setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","stockDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"stockDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStockDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isHazmatItem").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isHazmatItem" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsHazmatItem(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hazmatId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHazmatId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatShippingName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hazmatShippingName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHazmatShippingName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatHazardClass").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hazmatHazardClass" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHazmatHazardClass(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatPackingGroup").equals(reader.getName())){
                                
                                                object.setHazmatPackingGroup(com.netsuite.webservices.lists.accounting_2017_2.types.HazmatPackingGroup.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatItemUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hazmatItemUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHazmatItemUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","hazmatItemUnitsQty").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hazmatItemUnitsQty" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHazmatItemUnitsQty(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setHazmatItemUnitsQty(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","producer").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"producer" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setProducer(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturer").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturer" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturer(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","mpn").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"mpn" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMpn(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","multManufactureAddr").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"multManufactureAddr" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMultManufactureAddr(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturerAddr1").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturerAddr1" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturerAddr1(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturerCity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturerCity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturerCity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturerState").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturerState" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturerState(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturerZip").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturerZip" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturerZip(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","countryOfManufacture").equals(reader.getName())){
                                
                                                object.setCountryOfManufacture(com.netsuite.webservices.platform.common_2017_2.types.Country.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturerTaxId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturerTaxId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturerTaxId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","scheduleBNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"scheduleBNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setScheduleBNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","scheduleBQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"scheduleBQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setScheduleBQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setScheduleBQuantity(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","scheduleBCode").equals(reader.getName())){
                                
                                                object.setScheduleBCode(com.netsuite.webservices.lists.accounting_2017_2.types.ScheduleBCode.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturerTariff").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturerTariff" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturerTariff(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","preferenceCriterion").equals(reader.getName())){
                                
                                                object.setPreferenceCriterion(com.netsuite.webservices.lists.accounting_2017_2.types.ItemPreferenceCriterion.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","minimumQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"minimumQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMinimumQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMinimumQuantity(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","enforceMinQtyInternally").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"enforceMinQtyInternally" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEnforceMinQtyInternally(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","softDescriptor").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"softDescriptor" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSoftDescriptor(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shipPackage").equals(reader.getName())){
                                
                                                object.setShipPackage(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shipIndividually").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shipIndividually" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShipIndividually(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isFulfillable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isFulfillable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsFulfillable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costCategory").equals(reader.getName())){
                                
                                                object.setCostCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricesIncludeTax").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pricesIncludeTax" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPricesIncludeTax(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityPricingSchedule").equals(reader.getName())){
                                
                                                object.setQuantityPricingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","useMarginalRates").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useMarginalRates" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseMarginalRates(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","overallQuantityPricingType").equals(reader.getName())){
                                
                                                object.setOverallQuantityPricingType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingGroup").equals(reader.getName())){
                                
                                                object.setPricingGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","minimumQuantityUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"minimumQuantityUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMinimumQuantityUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoePrice").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoePrice" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoePrice(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoePrice(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeSopGroup").equals(reader.getName())){
                                
                                                object.setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeDeferral").equals(reader.getName())){
                                
                                                object.setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoePermitDiscount").equals(reader.getName())){
                                
                                                object.setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeDelivered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeDelivered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeDelivered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemRevenueCategory").equals(reader.getName())){
                                
                                                object.setItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferRevRec").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"deferRevRec" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeferRevRec(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueRecognitionRule").equals(reader.getName())){
                                
                                                object.setRevenueRecognitionRule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecForecastRule").equals(reader.getName())){
                                
                                                object.setRevRecForecastRule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueAllocationGroup").equals(reader.getName())){
                                
                                                object.setRevenueAllocationGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createRevenuePlansOn").equals(reader.getName())){
                                
                                                object.setCreateRevenuePlansOn(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","directRevenuePosting").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"directRevenuePosting" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDirectRevenuePosting(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","contingentRevenueHandling").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"contingentRevenueHandling" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setContingentRevenueHandling(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revReclassFXAccount").equals(reader.getName())){
                                
                                                object.setRevReclassFXAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDisplayName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDisplayName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayThumbnail").equals(reader.getName())){
                                
                                                object.setStoreDisplayThumbnail(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayImage").equals(reader.getName())){
                                
                                                object.setStoreDisplayImage(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDetailedDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDetailedDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDetailedDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeItemTemplate").equals(reader.getName())){
                                
                                                object.setStoreItemTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pageTitle").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pageTitle" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPageTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","metaTagHtml").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"metaTagHtml" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMetaTagHtml(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","excludeFromSitemap").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"excludeFromSitemap" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExcludeFromSitemap(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","sitemapPriority").equals(reader.getName())){
                                
                                                object.setSitemapPriority(com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","searchKeywords").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"searchKeywords" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSearchKeywords(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isDonationItem").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isDonationItem" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsDonationItem(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","showDefaultDonationAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showDefaultDonationAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowDefaultDonationAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","maxDonationAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"maxDonationAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMaxDonationAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMaxDonationAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","dontShowPrice").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dontShowPrice" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDontShowPrice(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","noPriceMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"noPriceMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNoPriceMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","outOfStockMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"outOfStockMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOutOfStockMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","onSpecial").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"onSpecial" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOnSpecial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","outOfStockBehavior").equals(reader.getName())){
                                
                                                object.setOutOfStockBehavior(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","relatedItemsDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"relatedItemsDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRelatedItemsDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","specialsDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"specialsDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSpecialsDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","featuredDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"featuredDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFeaturedDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shoppingDotComCategory").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shoppingDotComCategory" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShoppingDotComCategory(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","shopzillaCategoryId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"shopzillaCategoryId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShopzillaCategoryId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setShopzillaCategoryId(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","nexTagCategory").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"nexTagCategory" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNexTagCategory(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","productFeedList").equals(reader.getName())){
                                
                                                object.setProductFeedList(com.netsuite.webservices.lists.accounting_2017_2.ProductFeedList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","urlComponent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"urlComponent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUrlComponent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"itemId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setItemId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","upcCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"upcCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUpcCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","displayName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"displayName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDisplayName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isOnline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isOnline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsOnline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isGcoCompliant").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isGcoCompliant" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsGcoCompliant(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","offerSupport").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"offerSupport" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOfferSupport(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixItemNameTemplate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"matrixItemNameTemplate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMatrixItemNameTemplate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","availableToPartners").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"availableToPartners" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAvailableToPartners(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","defaultItemShipMethod").equals(reader.getName())){
                                
                                                object.setDefaultItemShipMethod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemCarrier").equals(reader.getName())){
                                
                                                object.setItemCarrier(com.netsuite.webservices.platform.common_2017_2.types.ShippingCarrier.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemShipMethodList").equals(reader.getName())){
                                
                                                object.setItemShipMethodList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList").equals(reader.getName())){
                                
                                                object.setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemOptionsList").equals(reader.getName())){
                                
                                                object.setItemOptionsList(com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixOptionList").equals(reader.getName())){
                                
                                                object.setMatrixOptionList(com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingMatrix").equals(reader.getName())){
                                
                                                object.setPricingMatrix(com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseTaxCode").equals(reader.getName())){
                                
                                                object.setPurchaseTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","salesTaxCode").equals(reader.getName())){
                                
                                                object.setSalesTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","siteCategoryList").equals(reader.getName())){
                                
                                                object.setSiteCategoryList(com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","translationsList").equals(reader.getName())){
                                
                                                object.setTranslationsList(com.netsuite.webservices.lists.accounting_2017_2.TranslationList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","presentationItemList").equals(reader.getName())){
                                
                                                object.setPresentationItemList(com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    