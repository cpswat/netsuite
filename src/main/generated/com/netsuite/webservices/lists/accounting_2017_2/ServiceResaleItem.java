
/**
 * ServiceResaleItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2;
            

            /**
            *  ServiceResaleItem bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ServiceResaleItem extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ServiceResaleItem
                Namespace URI = urn:accounting_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns19
                */
            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseDescription
                        */

                        
                                    protected java.lang.String localPurchaseDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseDescriptionTracker = false ;

                           public boolean isPurchaseDescriptionSpecified(){
                               return localPurchaseDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPurchaseDescription(){
                               return localPurchaseDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseDescription
                               */
                               public void setPurchaseDescription(java.lang.String param){
                            localPurchaseDescriptionTracker = param != null;
                                   
                                            this.localPurchaseDescription=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingChargeItem
                        */

                        
                                    protected boolean localManufacturingChargeItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingChargeItemTracker = false ;

                           public boolean isManufacturingChargeItemSpecified(){
                               return localManufacturingChargeItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getManufacturingChargeItem(){
                               return localManufacturingChargeItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingChargeItem
                               */
                               public void setManufacturingChargeItem(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localManufacturingChargeItemTracker =
                                       true;
                                   
                                            this.localManufacturingChargeItem=param;
                                    

                               }
                            

                        /**
                        * field for Cost
                        */

                        
                                    protected double localCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostTracker = false ;

                           public boolean isCostSpecified(){
                               return localCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCost(){
                               return localCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cost
                               */
                               public void setCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCost=param;
                                    

                               }
                            

                        /**
                        * field for CostUnits
                        */

                        
                                    protected java.lang.String localCostUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostUnitsTracker = false ;

                           public boolean isCostUnitsSpecified(){
                               return localCostUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCostUnits(){
                               return localCostUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostUnits
                               */
                               public void setCostUnits(java.lang.String param){
                            localCostUnitsTracker = param != null;
                                   
                                            this.localCostUnits=param;
                                    

                               }
                            

                        /**
                        * field for ExpenseAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localExpenseAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpenseAccountTracker = false ;

                           public boolean isExpenseAccountSpecified(){
                               return localExpenseAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getExpenseAccount(){
                               return localExpenseAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpenseAccount
                               */
                               public void setExpenseAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localExpenseAccountTracker = param != null;
                                   
                                            this.localExpenseAccount=param;
                                    

                               }
                            

                        /**
                        * field for IntercoExpenseAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIntercoExpenseAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoExpenseAccountTracker = false ;

                           public boolean isIntercoExpenseAccountSpecified(){
                               return localIntercoExpenseAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIntercoExpenseAccount(){
                               return localIntercoExpenseAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoExpenseAccount
                               */
                               public void setIntercoExpenseAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIntercoExpenseAccountTracker = param != null;
                                   
                                            this.localIntercoExpenseAccount=param;
                                    

                               }
                            

                        /**
                        * field for SalesDescription
                        */

                        
                                    protected java.lang.String localSalesDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesDescriptionTracker = false ;

                           public boolean isSalesDescriptionSpecified(){
                               return localSalesDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSalesDescription(){
                               return localSalesDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesDescription
                               */
                               public void setSalesDescription(java.lang.String param){
                            localSalesDescriptionTracker = param != null;
                                   
                                            this.localSalesDescription=param;
                                    

                               }
                            

                        /**
                        * field for IncludeChildren
                        */

                        
                                    protected boolean localIncludeChildren ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncludeChildrenTracker = false ;

                           public boolean isIncludeChildrenSpecified(){
                               return localIncludeChildrenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIncludeChildren(){
                               return localIncludeChildren;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncludeChildren
                               */
                               public void setIncludeChildren(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIncludeChildrenTracker =
                                       true;
                                   
                                            this.localIncludeChildren=param;
                                    

                               }
                            

                        /**
                        * field for IncomeAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIncomeAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncomeAccountTracker = false ;

                           public boolean isIncomeAccountSpecified(){
                               return localIncomeAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIncomeAccount(){
                               return localIncomeAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncomeAccount
                               */
                               public void setIncomeAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIncomeAccountTracker = param != null;
                                   
                                            this.localIncomeAccount=param;
                                    

                               }
                            

                        /**
                        * field for IntercoIncomeAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIntercoIncomeAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoIncomeAccountTracker = false ;

                           public boolean isIntercoIncomeAccountSpecified(){
                               return localIntercoIncomeAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIntercoIncomeAccount(){
                               return localIntercoIncomeAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoIncomeAccount
                               */
                               public void setIntercoIncomeAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIntercoIncomeAccountTracker = param != null;
                                   
                                            this.localIntercoIncomeAccount=param;
                                    

                               }
                            

                        /**
                        * field for TaxSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxScheduleTracker = false ;

                           public boolean isTaxScheduleSpecified(){
                               return localTaxScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxSchedule(){
                               return localTaxSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxSchedule
                               */
                               public void setTaxSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxScheduleTracker = param != null;
                                   
                                            this.localTaxSchedule=param;
                                    

                               }
                            

                        /**
                        * field for MatrixType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType localMatrixType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatrixTypeTracker = false ;

                           public boolean isMatrixTypeSpecified(){
                               return localMatrixTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType getMatrixType(){
                               return localMatrixType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatrixType
                               */
                               public void setMatrixType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType param){
                            localMatrixTypeTracker = param != null;
                                   
                                            this.localMatrixType=param;
                                    

                               }
                            

                        /**
                        * field for IsTaxable
                        */

                        
                                    protected boolean localIsTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTaxableTracker = false ;

                           public boolean isIsTaxableSpecified(){
                               return localIsTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsTaxable(){
                               return localIsTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTaxable
                               */
                               public void setIsTaxable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsTaxableTracker =
                                       true;
                                   
                                            this.localIsTaxable=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimateType
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType localCostEstimateType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTypeTracker = false ;

                           public boolean isCostEstimateTypeSpecified(){
                               return localCostEstimateTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType getCostEstimateType(){
                               return localCostEstimateType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimateType
                               */
                               public void setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType param){
                            localCostEstimateTypeTracker = param != null;
                                   
                                            this.localCostEstimateType=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimate
                        */

                        
                                    protected double localCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateTracker = false ;

                           public boolean isCostEstimateSpecified(){
                               return localCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCostEstimate(){
                               return localCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimate
                               */
                               public void setCostEstimate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCostEstimateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for UnitsType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localUnitsType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTypeTracker = false ;

                           public boolean isUnitsTypeSpecified(){
                               return localUnitsTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getUnitsType(){
                               return localUnitsType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UnitsType
                               */
                               public void setUnitsType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localUnitsTypeTracker = param != null;
                                   
                                            this.localUnitsType=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseUnit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPurchaseUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseUnitTracker = false ;

                           public boolean isPurchaseUnitSpecified(){
                               return localPurchaseUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPurchaseUnit(){
                               return localPurchaseUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseUnit
                               */
                               public void setPurchaseUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPurchaseUnitTracker = param != null;
                                   
                                            this.localPurchaseUnit=param;
                                    

                               }
                            

                        /**
                        * field for SaleUnit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSaleUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSaleUnitTracker = false ;

                           public boolean isSaleUnitSpecified(){
                               return localSaleUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSaleUnit(){
                               return localSaleUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SaleUnit
                               */
                               public void setSaleUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSaleUnitTracker = param != null;
                                   
                                            this.localSaleUnit=param;
                                    

                               }
                            

                        /**
                        * field for CostEstimateUnits
                        */

                        
                                    protected java.lang.String localCostEstimateUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostEstimateUnitsTracker = false ;

                           public boolean isCostEstimateUnitsSpecified(){
                               return localCostEstimateUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCostEstimateUnits(){
                               return localCostEstimateUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostEstimateUnits
                               */
                               public void setCostEstimateUnits(java.lang.String param){
                            localCostEstimateUnitsTracker = param != null;
                                   
                                            this.localCostEstimateUnits=param;
                                    

                               }
                            

                        /**
                        * field for IssueProduct
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIssueProduct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueProductTracker = false ;

                           public boolean isIssueProductSpecified(){
                               return localIssueProductTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIssueProduct(){
                               return localIssueProduct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueProduct
                               */
                               public void setIssueProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIssueProductTracker = param != null;
                                   
                                            this.localIssueProduct=param;
                                    

                               }
                            

                        /**
                        * field for BillingSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingScheduleTracker = false ;

                           public boolean isBillingScheduleSpecified(){
                               return localBillingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingSchedule(){
                               return localBillingSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingSchedule
                               */
                               public void setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingScheduleTracker = param != null;
                                   
                                            this.localBillingSchedule=param;
                                    

                               }
                            

                        /**
                        * field for DeferredRevenueAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDeferredRevenueAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferredRevenueAccountTracker = false ;

                           public boolean isDeferredRevenueAccountSpecified(){
                               return localDeferredRevenueAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDeferredRevenueAccount(){
                               return localDeferredRevenueAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferredRevenueAccount
                               */
                               public void setDeferredRevenueAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDeferredRevenueAccountTracker = param != null;
                                   
                                            this.localDeferredRevenueAccount=param;
                                    

                               }
                            

                        /**
                        * field for IntercoDefRevAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localIntercoDefRevAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIntercoDefRevAccountTracker = false ;

                           public boolean isIntercoDefRevAccountSpecified(){
                               return localIntercoDefRevAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getIntercoDefRevAccount(){
                               return localIntercoDefRevAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IntercoDefRevAccount
                               */
                               public void setIntercoDefRevAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localIntercoDefRevAccountTracker = param != null;
                                   
                                            this.localIntercoDefRevAccount=param;
                                    

                               }
                            

                        /**
                        * field for RevRecSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecScheduleTracker = false ;

                           public boolean isRevRecScheduleSpecified(){
                               return localRevRecScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecSchedule(){
                               return localRevRecSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecSchedule
                               */
                               public void setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecScheduleTracker = param != null;
                                   
                                            this.localRevRecSchedule=param;
                                    

                               }
                            

                        /**
                        * field for DeferralAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDeferralAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferralAccountTracker = false ;

                           public boolean isDeferralAccountSpecified(){
                               return localDeferralAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDeferralAccount(){
                               return localDeferralAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferralAccount
                               */
                               public void setDeferralAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDeferralAccountTracker = param != null;
                                   
                                            this.localDeferralAccount=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAmortizationTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationTemplateTracker = false ;

                           public boolean isAmortizationTemplateSpecified(){
                               return localAmortizationTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAmortizationTemplate(){
                               return localAmortizationTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationTemplate
                               */
                               public void setAmortizationTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAmortizationTemplateTracker = param != null;
                                   
                                            this.localAmortizationTemplate=param;
                                    

                               }
                            

                        /**
                        * field for Residual
                        */

                        
                                    protected java.lang.String localResidual ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResidualTracker = false ;

                           public boolean isResidualSpecified(){
                               return localResidualTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getResidual(){
                               return localResidual;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Residual
                               */
                               public void setResidual(java.lang.String param){
                            localResidualTracker = param != null;
                                   
                                            this.localResidual=param;
                                    

                               }
                            

                        /**
                        * field for DeferRevRec
                        */

                        
                                    protected boolean localDeferRevRec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferRevRecTracker = false ;

                           public boolean isDeferRevRecSpecified(){
                               return localDeferRevRecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDeferRevRec(){
                               return localDeferRevRec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferRevRec
                               */
                               public void setDeferRevRec(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDeferRevRecTracker =
                                       true;
                                   
                                            this.localDeferRevRec=param;
                                    

                               }
                            

                        /**
                        * field for RevenueRecognitionRule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevenueRecognitionRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueRecognitionRuleTracker = false ;

                           public boolean isRevenueRecognitionRuleSpecified(){
                               return localRevenueRecognitionRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevenueRecognitionRule(){
                               return localRevenueRecognitionRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueRecognitionRule
                               */
                               public void setRevenueRecognitionRule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevenueRecognitionRuleTracker = param != null;
                                   
                                            this.localRevenueRecognitionRule=param;
                                    

                               }
                            

                        /**
                        * field for RevRecForecastRule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevRecForecastRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecForecastRuleTracker = false ;

                           public boolean isRevRecForecastRuleSpecified(){
                               return localRevRecForecastRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevRecForecastRule(){
                               return localRevRecForecastRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecForecastRule
                               */
                               public void setRevRecForecastRule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevRecForecastRuleTracker = param != null;
                                   
                                            this.localRevRecForecastRule=param;
                                    

                               }
                            

                        /**
                        * field for RevenueAllocationGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevenueAllocationGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueAllocationGroupTracker = false ;

                           public boolean isRevenueAllocationGroupSpecified(){
                               return localRevenueAllocationGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevenueAllocationGroup(){
                               return localRevenueAllocationGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueAllocationGroup
                               */
                               public void setRevenueAllocationGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevenueAllocationGroupTracker = param != null;
                                   
                                            this.localRevenueAllocationGroup=param;
                                    

                               }
                            

                        /**
                        * field for CreateRevenuePlansOn
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCreateRevenuePlansOn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreateRevenuePlansOnTracker = false ;

                           public boolean isCreateRevenuePlansOnSpecified(){
                               return localCreateRevenuePlansOnTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCreateRevenuePlansOn(){
                               return localCreateRevenuePlansOn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreateRevenuePlansOn
                               */
                               public void setCreateRevenuePlansOn(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCreateRevenuePlansOnTracker = param != null;
                                   
                                            this.localCreateRevenuePlansOn=param;
                                    

                               }
                            

                        /**
                        * field for DirectRevenuePosting
                        */

                        
                                    protected boolean localDirectRevenuePosting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDirectRevenuePostingTracker = false ;

                           public boolean isDirectRevenuePostingSpecified(){
                               return localDirectRevenuePostingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDirectRevenuePosting(){
                               return localDirectRevenuePosting;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DirectRevenuePosting
                               */
                               public void setDirectRevenuePosting(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDirectRevenuePostingTracker =
                                       true;
                                   
                                            this.localDirectRevenuePosting=param;
                                    

                               }
                            

                        /**
                        * field for ContingentRevenueHandling
                        */

                        
                                    protected boolean localContingentRevenueHandling ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContingentRevenueHandlingTracker = false ;

                           public boolean isContingentRevenueHandlingSpecified(){
                               return localContingentRevenueHandlingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getContingentRevenueHandling(){
                               return localContingentRevenueHandling;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContingentRevenueHandling
                               */
                               public void setContingentRevenueHandling(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localContingentRevenueHandlingTracker =
                                       true;
                                   
                                            this.localContingentRevenueHandling=param;
                                    

                               }
                            

                        /**
                        * field for RevReclassFXAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRevReclassFXAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevReclassFXAccountTracker = false ;

                           public boolean isRevReclassFXAccountSpecified(){
                               return localRevReclassFXAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRevReclassFXAccount(){
                               return localRevReclassFXAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevReclassFXAccount
                               */
                               public void setRevReclassFXAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRevReclassFXAccountTracker = param != null;
                                   
                                            this.localRevReclassFXAccount=param;
                                    

                               }
                            

                        /**
                        * field for AmortizationPeriod
                        */

                        
                                    protected long localAmortizationPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizationPeriodTracker = false ;

                           public boolean isAmortizationPeriodSpecified(){
                               return localAmortizationPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getAmortizationPeriod(){
                               return localAmortizationPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizationPeriod
                               */
                               public void setAmortizationPeriod(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localAmortizationPeriodTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localAmortizationPeriod=param;
                                    

                               }
                            

                        /**
                        * field for MinimumQuantity
                        */

                        
                                    protected long localMinimumQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMinimumQuantityTracker = false ;

                           public boolean isMinimumQuantitySpecified(){
                               return localMinimumQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getMinimumQuantity(){
                               return localMinimumQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MinimumQuantity
                               */
                               public void setMinimumQuantity(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localMinimumQuantityTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localMinimumQuantity=param;
                                    

                               }
                            

                        /**
                        * field for EnforceMinQtyInternally
                        */

                        
                                    protected boolean localEnforceMinQtyInternally ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEnforceMinQtyInternallyTracker = false ;

                           public boolean isEnforceMinQtyInternallySpecified(){
                               return localEnforceMinQtyInternallyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEnforceMinQtyInternally(){
                               return localEnforceMinQtyInternally;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EnforceMinQtyInternally
                               */
                               public void setEnforceMinQtyInternally(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEnforceMinQtyInternallyTracker =
                                       true;
                                   
                                            this.localEnforceMinQtyInternally=param;
                                    

                               }
                            

                        /**
                        * field for SoftDescriptor
                        */

                        
                                    protected java.lang.String localSoftDescriptor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSoftDescriptorTracker = false ;

                           public boolean isSoftDescriptorSpecified(){
                               return localSoftDescriptorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSoftDescriptor(){
                               return localSoftDescriptor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SoftDescriptor
                               */
                               public void setSoftDescriptor(java.lang.String param){
                            localSoftDescriptorTracker = param != null;
                                   
                                            this.localSoftDescriptor=param;
                                    

                               }
                            

                        /**
                        * field for PricesIncludeTax
                        */

                        
                                    protected boolean localPricesIncludeTax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricesIncludeTaxTracker = false ;

                           public boolean isPricesIncludeTaxSpecified(){
                               return localPricesIncludeTaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPricesIncludeTax(){
                               return localPricesIncludeTax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricesIncludeTax
                               */
                               public void setPricesIncludeTax(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPricesIncludeTaxTracker =
                                       true;
                                   
                                            this.localPricesIncludeTax=param;
                                    

                               }
                            

                        /**
                        * field for QuantityPricingSchedule
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localQuantityPricingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityPricingScheduleTracker = false ;

                           public boolean isQuantityPricingScheduleSpecified(){
                               return localQuantityPricingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getQuantityPricingSchedule(){
                               return localQuantityPricingSchedule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityPricingSchedule
                               */
                               public void setQuantityPricingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localQuantityPricingScheduleTracker = param != null;
                                   
                                            this.localQuantityPricingSchedule=param;
                                    

                               }
                            

                        /**
                        * field for UseMarginalRates
                        */

                        
                                    protected boolean localUseMarginalRates ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseMarginalRatesTracker = false ;

                           public boolean isUseMarginalRatesSpecified(){
                               return localUseMarginalRatesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUseMarginalRates(){
                               return localUseMarginalRates;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseMarginalRates
                               */
                               public void setUseMarginalRates(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUseMarginalRatesTracker =
                                       true;
                                   
                                            this.localUseMarginalRates=param;
                                    

                               }
                            

                        /**
                        * field for OverallQuantityPricingType
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType localOverallQuantityPricingType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOverallQuantityPricingTypeTracker = false ;

                           public boolean isOverallQuantityPricingTypeSpecified(){
                               return localOverallQuantityPricingTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType getOverallQuantityPricingType(){
                               return localOverallQuantityPricingType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OverallQuantityPricingType
                               */
                               public void setOverallQuantityPricingType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType param){
                            localOverallQuantityPricingTypeTracker = param != null;
                                   
                                            this.localOverallQuantityPricingType=param;
                                    

                               }
                            

                        /**
                        * field for IsFulfillable
                        */

                        
                                    protected boolean localIsFulfillable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsFulfillableTracker = false ;

                           public boolean isIsFulfillableSpecified(){
                               return localIsFulfillableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsFulfillable(){
                               return localIsFulfillable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsFulfillable
                               */
                               public void setIsFulfillable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsFulfillableTracker =
                                       true;
                                   
                                            this.localIsFulfillable=param;
                                    

                               }
                            

                        /**
                        * field for GenerateAccruals
                        */

                        
                                    protected boolean localGenerateAccruals ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGenerateAccrualsTracker = false ;

                           public boolean isGenerateAccrualsSpecified(){
                               return localGenerateAccrualsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getGenerateAccruals(){
                               return localGenerateAccruals;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GenerateAccruals
                               */
                               public void setGenerateAccruals(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localGenerateAccrualsTracker =
                                       true;
                                   
                                            this.localGenerateAccruals=param;
                                    

                               }
                            

                        /**
                        * field for CostCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCostCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostCategoryTracker = false ;

                           public boolean isCostCategorySpecified(){
                               return localCostCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCostCategory(){
                               return localCostCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostCategory
                               */
                               public void setCostCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCostCategoryTracker = param != null;
                                   
                                            this.localCostCategory=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderQuantity
                        */

                        
                                    protected double localPurchaseOrderQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderQuantityTracker = false ;

                           public boolean isPurchaseOrderQuantitySpecified(){
                               return localPurchaseOrderQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPurchaseOrderQuantity(){
                               return localPurchaseOrderQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderQuantity
                               */
                               public void setPurchaseOrderQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPurchaseOrderQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPurchaseOrderQuantity=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderAmount
                        */

                        
                                    protected double localPurchaseOrderAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderAmountTracker = false ;

                           public boolean isPurchaseOrderAmountSpecified(){
                               return localPurchaseOrderAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPurchaseOrderAmount(){
                               return localPurchaseOrderAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderAmount
                               */
                               public void setPurchaseOrderAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPurchaseOrderAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPurchaseOrderAmount=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderQuantityDiff
                        */

                        
                                    protected double localPurchaseOrderQuantityDiff ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderQuantityDiffTracker = false ;

                           public boolean isPurchaseOrderQuantityDiffSpecified(){
                               return localPurchaseOrderQuantityDiffTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPurchaseOrderQuantityDiff(){
                               return localPurchaseOrderQuantityDiff;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderQuantityDiff
                               */
                               public void setPurchaseOrderQuantityDiff(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPurchaseOrderQuantityDiffTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPurchaseOrderQuantityDiff=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptQuantity
                        */

                        
                                    protected double localReceiptQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptQuantityTracker = false ;

                           public boolean isReceiptQuantitySpecified(){
                               return localReceiptQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getReceiptQuantity(){
                               return localReceiptQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptQuantity
                               */
                               public void setReceiptQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localReceiptQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localReceiptQuantity=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptAmount
                        */

                        
                                    protected double localReceiptAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptAmountTracker = false ;

                           public boolean isReceiptAmountSpecified(){
                               return localReceiptAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getReceiptAmount(){
                               return localReceiptAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptAmount
                               */
                               public void setReceiptAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localReceiptAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localReceiptAmount=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptQuantityDiff
                        */

                        
                                    protected double localReceiptQuantityDiff ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptQuantityDiffTracker = false ;

                           public boolean isReceiptQuantityDiffSpecified(){
                               return localReceiptQuantityDiffTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getReceiptQuantityDiff(){
                               return localReceiptQuantityDiff;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptQuantityDiff
                               */
                               public void setReceiptQuantityDiff(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localReceiptQuantityDiffTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localReceiptQuantityDiff=param;
                                    

                               }
                            

                        /**
                        * field for PricingGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPricingGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingGroupTracker = false ;

                           public boolean isPricingGroupSpecified(){
                               return localPricingGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPricingGroup(){
                               return localPricingGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingGroup
                               */
                               public void setPricingGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPricingGroupTracker = param != null;
                                   
                                            this.localPricingGroup=param;
                                    

                               }
                            

                        /**
                        * field for MinimumQuantityUnits
                        */

                        
                                    protected java.lang.String localMinimumQuantityUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMinimumQuantityUnitsTracker = false ;

                           public boolean isMinimumQuantityUnitsSpecified(){
                               return localMinimumQuantityUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMinimumQuantityUnits(){
                               return localMinimumQuantityUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MinimumQuantityUnits
                               */
                               public void setMinimumQuantityUnits(java.lang.String param){
                            localMinimumQuantityUnitsTracker = param != null;
                                   
                                            this.localMinimumQuantityUnits=param;
                                    

                               }
                            

                        /**
                        * field for VsoePrice
                        */

                        
                                    protected double localVsoePrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePriceTracker = false ;

                           public boolean isVsoePriceSpecified(){
                               return localVsoePriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getVsoePrice(){
                               return localVsoePrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePrice
                               */
                               public void setVsoePrice(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoePriceTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localVsoePrice=param;
                                    

                               }
                            

                        /**
                        * field for VsoeSopGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup localVsoeSopGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeSopGroupTracker = false ;

                           public boolean isVsoeSopGroupSpecified(){
                               return localVsoeSopGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup getVsoeSopGroup(){
                               return localVsoeSopGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeSopGroup
                               */
                               public void setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup param){
                            localVsoeSopGroupTracker = param != null;
                                   
                                            this.localVsoeSopGroup=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDeferral
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral localVsoeDeferral ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeferralTracker = false ;

                           public boolean isVsoeDeferralSpecified(){
                               return localVsoeDeferralTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral getVsoeDeferral(){
                               return localVsoeDeferral;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDeferral
                               */
                               public void setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral param){
                            localVsoeDeferralTracker = param != null;
                                   
                                            this.localVsoeDeferral=param;
                                    

                               }
                            

                        /**
                        * field for VsoePermitDiscount
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount localVsoePermitDiscount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoePermitDiscountTracker = false ;

                           public boolean isVsoePermitDiscountSpecified(){
                               return localVsoePermitDiscountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount getVsoePermitDiscount(){
                               return localVsoePermitDiscount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoePermitDiscount
                               */
                               public void setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount param){
                            localVsoePermitDiscountTracker = param != null;
                                   
                                            this.localVsoePermitDiscount=param;
                                    

                               }
                            

                        /**
                        * field for VsoeDelivered
                        */

                        
                                    protected boolean localVsoeDelivered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeDeliveredTracker = false ;

                           public boolean isVsoeDeliveredSpecified(){
                               return localVsoeDeliveredTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getVsoeDelivered(){
                               return localVsoeDelivered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeDelivered
                               */
                               public void setVsoeDelivered(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localVsoeDeliveredTracker =
                                       true;
                                   
                                            this.localVsoeDelivered=param;
                                    

                               }
                            

                        /**
                        * field for ItemRevenueCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItemRevenueCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemRevenueCategoryTracker = false ;

                           public boolean isItemRevenueCategorySpecified(){
                               return localItemRevenueCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItemRevenueCategory(){
                               return localItemRevenueCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemRevenueCategory
                               */
                               public void setItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemRevenueCategoryTracker = param != null;
                                   
                                            this.localItemRevenueCategory=param;
                                    

                               }
                            

                        /**
                        * field for CreateJob
                        */

                        
                                    protected boolean localCreateJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreateJobTracker = false ;

                           public boolean isCreateJobSpecified(){
                               return localCreateJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCreateJob(){
                               return localCreateJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreateJob
                               */
                               public void setCreateJob(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localCreateJobTracker =
                                       true;
                                   
                                            this.localCreateJob=param;
                                    

                               }
                            

                        /**
                        * field for MatrixItemNameTemplate
                        */

                        
                                    protected java.lang.String localMatrixItemNameTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatrixItemNameTemplateTracker = false ;

                           public boolean isMatrixItemNameTemplateSpecified(){
                               return localMatrixItemNameTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMatrixItemNameTemplate(){
                               return localMatrixItemNameTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatrixItemNameTemplate
                               */
                               public void setMatrixItemNameTemplate(java.lang.String param){
                            localMatrixItemNameTemplateTracker = param != null;
                                   
                                            this.localMatrixItemNameTemplate=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayName
                        */

                        
                                    protected java.lang.String localStoreDisplayName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayNameTracker = false ;

                           public boolean isStoreDisplayNameSpecified(){
                               return localStoreDisplayNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDisplayName(){
                               return localStoreDisplayName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayName
                               */
                               public void setStoreDisplayName(java.lang.String param){
                            localStoreDisplayNameTracker = param != null;
                                   
                                            this.localStoreDisplayName=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayThumbnail
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreDisplayThumbnail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayThumbnailTracker = false ;

                           public boolean isStoreDisplayThumbnailSpecified(){
                               return localStoreDisplayThumbnailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreDisplayThumbnail(){
                               return localStoreDisplayThumbnail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayThumbnail
                               */
                               public void setStoreDisplayThumbnail(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreDisplayThumbnailTracker = param != null;
                                   
                                            this.localStoreDisplayThumbnail=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayImage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreDisplayImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayImageTracker = false ;

                           public boolean isStoreDisplayImageSpecified(){
                               return localStoreDisplayImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreDisplayImage(){
                               return localStoreDisplayImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayImage
                               */
                               public void setStoreDisplayImage(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreDisplayImageTracker = param != null;
                                   
                                            this.localStoreDisplayImage=param;
                                    

                               }
                            

                        /**
                        * field for StoreDescription
                        */

                        
                                    protected java.lang.String localStoreDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDescriptionTracker = false ;

                           public boolean isStoreDescriptionSpecified(){
                               return localStoreDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDescription(){
                               return localStoreDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDescription
                               */
                               public void setStoreDescription(java.lang.String param){
                            localStoreDescriptionTracker = param != null;
                                   
                                            this.localStoreDescription=param;
                                    

                               }
                            

                        /**
                        * field for StoreDetailedDescription
                        */

                        
                                    protected java.lang.String localStoreDetailedDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDetailedDescriptionTracker = false ;

                           public boolean isStoreDetailedDescriptionSpecified(){
                               return localStoreDetailedDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDetailedDescription(){
                               return localStoreDetailedDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDetailedDescription
                               */
                               public void setStoreDetailedDescription(java.lang.String param){
                            localStoreDetailedDescriptionTracker = param != null;
                                   
                                            this.localStoreDetailedDescription=param;
                                    

                               }
                            

                        /**
                        * field for StoreItemTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreItemTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreItemTemplateTracker = false ;

                           public boolean isStoreItemTemplateSpecified(){
                               return localStoreItemTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreItemTemplate(){
                               return localStoreItemTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreItemTemplate
                               */
                               public void setStoreItemTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreItemTemplateTracker = param != null;
                                   
                                            this.localStoreItemTemplate=param;
                                    

                               }
                            

                        /**
                        * field for PageTitle
                        */

                        
                                    protected java.lang.String localPageTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPageTitleTracker = false ;

                           public boolean isPageTitleSpecified(){
                               return localPageTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPageTitle(){
                               return localPageTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PageTitle
                               */
                               public void setPageTitle(java.lang.String param){
                            localPageTitleTracker = param != null;
                                   
                                            this.localPageTitle=param;
                                    

                               }
                            

                        /**
                        * field for UrlComponent
                        */

                        
                                    protected java.lang.String localUrlComponent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUrlComponentTracker = false ;

                           public boolean isUrlComponentSpecified(){
                               return localUrlComponentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUrlComponent(){
                               return localUrlComponent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UrlComponent
                               */
                               public void setUrlComponent(java.lang.String param){
                            localUrlComponentTracker = param != null;
                                   
                                            this.localUrlComponent=param;
                                    

                               }
                            

                        /**
                        * field for MetaTagHtml
                        */

                        
                                    protected java.lang.String localMetaTagHtml ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMetaTagHtmlTracker = false ;

                           public boolean isMetaTagHtmlSpecified(){
                               return localMetaTagHtmlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMetaTagHtml(){
                               return localMetaTagHtml;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MetaTagHtml
                               */
                               public void setMetaTagHtml(java.lang.String param){
                            localMetaTagHtmlTracker = param != null;
                                   
                                            this.localMetaTagHtml=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeFromSitemap
                        */

                        
                                    protected boolean localExcludeFromSitemap ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeFromSitemapTracker = false ;

                           public boolean isExcludeFromSitemapSpecified(){
                               return localExcludeFromSitemapTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExcludeFromSitemap(){
                               return localExcludeFromSitemap;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeFromSitemap
                               */
                               public void setExcludeFromSitemap(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExcludeFromSitemapTracker =
                                       true;
                                   
                                            this.localExcludeFromSitemap=param;
                                    

                               }
                            

                        /**
                        * field for SitemapPriority
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority localSitemapPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSitemapPriorityTracker = false ;

                           public boolean isSitemapPrioritySpecified(){
                               return localSitemapPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority getSitemapPriority(){
                               return localSitemapPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SitemapPriority
                               */
                               public void setSitemapPriority(com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority param){
                            localSitemapPriorityTracker = param != null;
                                   
                                            this.localSitemapPriority=param;
                                    

                               }
                            

                        /**
                        * field for SearchKeywords
                        */

                        
                                    protected java.lang.String localSearchKeywords ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchKeywordsTracker = false ;

                           public boolean isSearchKeywordsSpecified(){
                               return localSearchKeywordsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSearchKeywords(){
                               return localSearchKeywords;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchKeywords
                               */
                               public void setSearchKeywords(java.lang.String param){
                            localSearchKeywordsTracker = param != null;
                                   
                                            this.localSearchKeywords=param;
                                    

                               }
                            

                        /**
                        * field for IsDonationItem
                        */

                        
                                    protected boolean localIsDonationItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDonationItemTracker = false ;

                           public boolean isIsDonationItemSpecified(){
                               return localIsDonationItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsDonationItem(){
                               return localIsDonationItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDonationItem
                               */
                               public void setIsDonationItem(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsDonationItemTracker =
                                       true;
                                   
                                            this.localIsDonationItem=param;
                                    

                               }
                            

                        /**
                        * field for ShowDefaultDonationAmount
                        */

                        
                                    protected boolean localShowDefaultDonationAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowDefaultDonationAmountTracker = false ;

                           public boolean isShowDefaultDonationAmountSpecified(){
                               return localShowDefaultDonationAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowDefaultDonationAmount(){
                               return localShowDefaultDonationAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowDefaultDonationAmount
                               */
                               public void setShowDefaultDonationAmount(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowDefaultDonationAmountTracker =
                                       true;
                                   
                                            this.localShowDefaultDonationAmount=param;
                                    

                               }
                            

                        /**
                        * field for MaxDonationAmount
                        */

                        
                                    protected double localMaxDonationAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMaxDonationAmountTracker = false ;

                           public boolean isMaxDonationAmountSpecified(){
                               return localMaxDonationAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getMaxDonationAmount(){
                               return localMaxDonationAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MaxDonationAmount
                               */
                               public void setMaxDonationAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localMaxDonationAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localMaxDonationAmount=param;
                                    

                               }
                            

                        /**
                        * field for DontShowPrice
                        */

                        
                                    protected boolean localDontShowPrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDontShowPriceTracker = false ;

                           public boolean isDontShowPriceSpecified(){
                               return localDontShowPriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDontShowPrice(){
                               return localDontShowPrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DontShowPrice
                               */
                               public void setDontShowPrice(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDontShowPriceTracker =
                                       true;
                                   
                                            this.localDontShowPrice=param;
                                    

                               }
                            

                        /**
                        * field for NoPriceMessage
                        */

                        
                                    protected java.lang.String localNoPriceMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNoPriceMessageTracker = false ;

                           public boolean isNoPriceMessageSpecified(){
                               return localNoPriceMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNoPriceMessage(){
                               return localNoPriceMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NoPriceMessage
                               */
                               public void setNoPriceMessage(java.lang.String param){
                            localNoPriceMessageTracker = param != null;
                                   
                                            this.localNoPriceMessage=param;
                                    

                               }
                            

                        /**
                        * field for OutOfStockMessage
                        */

                        
                                    protected java.lang.String localOutOfStockMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOutOfStockMessageTracker = false ;

                           public boolean isOutOfStockMessageSpecified(){
                               return localOutOfStockMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOutOfStockMessage(){
                               return localOutOfStockMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OutOfStockMessage
                               */
                               public void setOutOfStockMessage(java.lang.String param){
                            localOutOfStockMessageTracker = param != null;
                                   
                                            this.localOutOfStockMessage=param;
                                    

                               }
                            

                        /**
                        * field for OnSpecial
                        */

                        
                                    protected boolean localOnSpecial ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOnSpecialTracker = false ;

                           public boolean isOnSpecialSpecified(){
                               return localOnSpecialTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getOnSpecial(){
                               return localOnSpecial;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OnSpecial
                               */
                               public void setOnSpecial(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localOnSpecialTracker =
                                       true;
                                   
                                            this.localOnSpecial=param;
                                    

                               }
                            

                        /**
                        * field for OutOfStockBehavior
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior localOutOfStockBehavior ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOutOfStockBehaviorTracker = false ;

                           public boolean isOutOfStockBehaviorSpecified(){
                               return localOutOfStockBehaviorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior getOutOfStockBehavior(){
                               return localOutOfStockBehavior;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OutOfStockBehavior
                               */
                               public void setOutOfStockBehavior(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior param){
                            localOutOfStockBehaviorTracker = param != null;
                                   
                                            this.localOutOfStockBehavior=param;
                                    

                               }
                            

                        /**
                        * field for RelatedItemsDescription
                        */

                        
                                    protected java.lang.String localRelatedItemsDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelatedItemsDescriptionTracker = false ;

                           public boolean isRelatedItemsDescriptionSpecified(){
                               return localRelatedItemsDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRelatedItemsDescription(){
                               return localRelatedItemsDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RelatedItemsDescription
                               */
                               public void setRelatedItemsDescription(java.lang.String param){
                            localRelatedItemsDescriptionTracker = param != null;
                                   
                                            this.localRelatedItemsDescription=param;
                                    

                               }
                            

                        /**
                        * field for SpecialsDescription
                        */

                        
                                    protected java.lang.String localSpecialsDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSpecialsDescriptionTracker = false ;

                           public boolean isSpecialsDescriptionSpecified(){
                               return localSpecialsDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSpecialsDescription(){
                               return localSpecialsDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SpecialsDescription
                               */
                               public void setSpecialsDescription(java.lang.String param){
                            localSpecialsDescriptionTracker = param != null;
                                   
                                            this.localSpecialsDescription=param;
                                    

                               }
                            

                        /**
                        * field for ItemTaskTemplatesList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplatesList localItemTaskTemplatesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTaskTemplatesListTracker = false ;

                           public boolean isItemTaskTemplatesListSpecified(){
                               return localItemTaskTemplatesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplatesList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplatesList getItemTaskTemplatesList(){
                               return localItemTaskTemplatesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemTaskTemplatesList
                               */
                               public void setItemTaskTemplatesList(com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplatesList param){
                            localItemTaskTemplatesListTracker = param != null;
                                   
                                            this.localItemTaskTemplatesList=param;
                                    

                               }
                            

                        /**
                        * field for FeaturedDescription
                        */

                        
                                    protected java.lang.String localFeaturedDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFeaturedDescriptionTracker = false ;

                           public boolean isFeaturedDescriptionSpecified(){
                               return localFeaturedDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFeaturedDescription(){
                               return localFeaturedDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FeaturedDescription
                               */
                               public void setFeaturedDescription(java.lang.String param){
                            localFeaturedDescriptionTracker = param != null;
                                   
                                            this.localFeaturedDescription=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for ItemId
                        */

                        
                                    protected java.lang.String localItemId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemIdTracker = false ;

                           public boolean isItemIdSpecified(){
                               return localItemIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getItemId(){
                               return localItemId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemId
                               */
                               public void setItemId(java.lang.String param){
                            localItemIdTracker = param != null;
                                   
                                            this.localItemId=param;
                                    

                               }
                            

                        /**
                        * field for UpcCode
                        */

                        
                                    protected java.lang.String localUpcCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUpcCodeTracker = false ;

                           public boolean isUpcCodeSpecified(){
                               return localUpcCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUpcCode(){
                               return localUpcCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UpcCode
                               */
                               public void setUpcCode(java.lang.String param){
                            localUpcCodeTracker = param != null;
                                   
                                            this.localUpcCode=param;
                                    

                               }
                            

                        /**
                        * field for DisplayName
                        */

                        
                                    protected java.lang.String localDisplayName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDisplayNameTracker = false ;

                           public boolean isDisplayNameSpecified(){
                               return localDisplayNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDisplayName(){
                               return localDisplayName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DisplayName
                               */
                               public void setDisplayName(java.lang.String param){
                            localDisplayNameTracker = param != null;
                                   
                                            this.localDisplayName=param;
                                    

                               }
                            

                        /**
                        * field for VendorName
                        */

                        
                                    protected java.lang.String localVendorName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorNameTracker = false ;

                           public boolean isVendorNameSpecified(){
                               return localVendorNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getVendorName(){
                               return localVendorName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorName
                               */
                               public void setVendorName(java.lang.String param){
                            localVendorNameTracker = param != null;
                                   
                                            this.localVendorName=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for IsOnline
                        */

                        
                                    protected boolean localIsOnline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOnlineTracker = false ;

                           public boolean isIsOnlineSpecified(){
                               return localIsOnlineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsOnline(){
                               return localIsOnline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOnline
                               */
                               public void setIsOnline(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsOnlineTracker =
                                       true;
                                   
                                            this.localIsOnline=param;
                                    

                               }
                            

                        /**
                        * field for IsGcoCompliant
                        */

                        
                                    protected boolean localIsGcoCompliant ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsGcoCompliantTracker = false ;

                           public boolean isIsGcoCompliantSpecified(){
                               return localIsGcoCompliantTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsGcoCompliant(){
                               return localIsGcoCompliant;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsGcoCompliant
                               */
                               public void setIsGcoCompliant(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsGcoCompliantTracker =
                                       true;
                                   
                                            this.localIsGcoCompliant=param;
                                    

                               }
                            

                        /**
                        * field for OfferSupport
                        */

                        
                                    protected boolean localOfferSupport ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOfferSupportTracker = false ;

                           public boolean isOfferSupportSpecified(){
                               return localOfferSupportTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getOfferSupport(){
                               return localOfferSupport;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OfferSupport
                               */
                               public void setOfferSupport(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localOfferSupportTracker =
                                       true;
                                   
                                            this.localOfferSupport=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for AvailableToPartners
                        */

                        
                                    protected boolean localAvailableToPartners ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAvailableToPartnersTracker = false ;

                           public boolean isAvailableToPartnersSpecified(){
                               return localAvailableToPartnersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAvailableToPartners(){
                               return localAvailableToPartners;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AvailableToPartners
                               */
                               public void setAvailableToPartners(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAvailableToPartnersTracker =
                                       true;
                                   
                                            this.localAvailableToPartners=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for SubsidiaryList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localSubsidiaryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryListTracker = false ;

                           public boolean isSubsidiaryListSpecified(){
                               return localSubsidiaryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getSubsidiaryList(){
                               return localSubsidiaryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidiaryList
                               */
                               public void setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localSubsidiaryListTracker = param != null;
                                   
                                            this.localSubsidiaryList=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected java.lang.String localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(java.lang.String param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for BillingRatesMatrix
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.BillingRatesMatrix localBillingRatesMatrix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingRatesMatrixTracker = false ;

                           public boolean isBillingRatesMatrixSpecified(){
                               return localBillingRatesMatrixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.BillingRatesMatrix
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.BillingRatesMatrix getBillingRatesMatrix(){
                               return localBillingRatesMatrix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingRatesMatrix
                               */
                               public void setBillingRatesMatrix(com.netsuite.webservices.lists.accounting_2017_2.BillingRatesMatrix param){
                            localBillingRatesMatrixTracker = param != null;
                                   
                                            this.localBillingRatesMatrix=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBookDetailList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList localAccountingBookDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookDetailListTracker = false ;

                           public boolean isAccountingBookDetailListSpecified(){
                               return localAccountingBookDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList getAccountingBookDetailList(){
                               return localAccountingBookDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBookDetailList
                               */
                               public void setAccountingBookDetailList(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList param){
                            localAccountingBookDetailListTracker = param != null;
                                   
                                            this.localAccountingBookDetailList=param;
                                    

                               }
                            

                        /**
                        * field for ItemOptionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList localItemOptionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemOptionsListTracker = false ;

                           public boolean isItemOptionsListSpecified(){
                               return localItemOptionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList getItemOptionsList(){
                               return localItemOptionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemOptionsList
                               */
                               public void setItemOptionsList(com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList param){
                            localItemOptionsListTracker = param != null;
                                   
                                            this.localItemOptionsList=param;
                                    

                               }
                            

                        /**
                        * field for MatrixOptionList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList localMatrixOptionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMatrixOptionListTracker = false ;

                           public boolean isMatrixOptionListSpecified(){
                               return localMatrixOptionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList getMatrixOptionList(){
                               return localMatrixOptionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MatrixOptionList
                               */
                               public void setMatrixOptionList(com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList param){
                            localMatrixOptionListTracker = param != null;
                                   
                                            this.localMatrixOptionList=param;
                                    

                               }
                            

                        /**
                        * field for ItemVendorList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.ItemVendorList localItemVendorList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemVendorListTracker = false ;

                           public boolean isItemVendorListSpecified(){
                               return localItemVendorListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.ItemVendorList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.ItemVendorList getItemVendorList(){
                               return localItemVendorList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemVendorList
                               */
                               public void setItemVendorList(com.netsuite.webservices.lists.accounting_2017_2.ItemVendorList param){
                            localItemVendorListTracker = param != null;
                                   
                                            this.localItemVendorList=param;
                                    

                               }
                            

                        /**
                        * field for PricingMatrix
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix localPricingMatrix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingMatrixTracker = false ;

                           public boolean isPricingMatrixSpecified(){
                               return localPricingMatrixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix getPricingMatrix(){
                               return localPricingMatrix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingMatrix
                               */
                               public void setPricingMatrix(com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix param){
                            localPricingMatrixTracker = param != null;
                                   
                                            this.localPricingMatrix=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPurchaseTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseTaxCodeTracker = false ;

                           public boolean isPurchaseTaxCodeSpecified(){
                               return localPurchaseTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPurchaseTaxCode(){
                               return localPurchaseTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseTaxCode
                               */
                               public void setPurchaseTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPurchaseTaxCodeTracker = param != null;
                                   
                                            this.localPurchaseTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for Rate
                        */

                        
                                    protected double localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRate(){
                               return localRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rate
                               */
                               public void setRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRate=param;
                                    

                               }
                            

                        /**
                        * field for SalesTaxCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesTaxCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTaxCodeTracker = false ;

                           public boolean isSalesTaxCodeSpecified(){
                               return localSalesTaxCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesTaxCode(){
                               return localSalesTaxCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTaxCode
                               */
                               public void setSalesTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesTaxCodeTracker = param != null;
                                   
                                            this.localSalesTaxCode=param;
                                    

                               }
                            

                        /**
                        * field for SiteCategoryList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList localSiteCategoryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSiteCategoryListTracker = false ;

                           public boolean isSiteCategoryListSpecified(){
                               return localSiteCategoryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList getSiteCategoryList(){
                               return localSiteCategoryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SiteCategoryList
                               */
                               public void setSiteCategoryList(com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList param){
                            localSiteCategoryListTracker = param != null;
                                   
                                            this.localSiteCategoryList=param;
                                    

                               }
                            

                        /**
                        * field for TranslationsList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.TranslationList localTranslationsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranslationsListTracker = false ;

                           public boolean isTranslationsListSpecified(){
                               return localTranslationsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.TranslationList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.TranslationList getTranslationsList(){
                               return localTranslationsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranslationsList
                               */
                               public void setTranslationsList(com.netsuite.webservices.lists.accounting_2017_2.TranslationList param){
                            localTranslationsListTracker = param != null;
                                   
                                            this.localTranslationsList=param;
                                    

                               }
                            

                        /**
                        * field for Vendor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localVendor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorTracker = false ;

                           public boolean isVendorSpecified(){
                               return localVendorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getVendor(){
                               return localVendor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Vendor
                               */
                               public void setVendor(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localVendorTracker = param != null;
                                   
                                            this.localVendor=param;
                                    

                               }
                            

                        /**
                        * field for PresentationItemList
                        */

                        
                                    protected com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList localPresentationItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPresentationItemListTracker = false ;

                           public boolean isPresentationItemListSpecified(){
                               return localPresentationItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList
                           */
                           public  com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList getPresentationItemList(){
                               return localPresentationItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PresentationItemList
                               */
                               public void setPresentationItemList(com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList param){
                            localPresentationItemListTracker = param != null;
                                   
                                            this.localPresentationItemList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:accounting_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ServiceResaleItem",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ServiceResaleItem",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "purchaseDescription", xmlWriter);
                             

                                          if (localPurchaseDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("purchaseDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPurchaseDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturingChargeItemTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "manufacturingChargeItem", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("manufacturingChargeItem cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturingChargeItem));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "cost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("cost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costUnits", xmlWriter);
                             

                                          if (localCostUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("costUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCostUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExpenseAccountTracker){
                                            if (localExpenseAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expenseAccount cannot be null!!");
                                            }
                                           localExpenseAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","expenseAccount"),
                                               xmlWriter);
                                        } if (localIntercoExpenseAccountTracker){
                                            if (localIntercoExpenseAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoExpenseAccount cannot be null!!");
                                            }
                                           localIntercoExpenseAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","intercoExpenseAccount"),
                                               xmlWriter);
                                        } if (localSalesDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "salesDescription", xmlWriter);
                             

                                          if (localSalesDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("salesDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSalesDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncludeChildrenTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "includeChildren", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("includeChildren cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncomeAccountTracker){
                                            if (localIncomeAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("incomeAccount cannot be null!!");
                                            }
                                           localIncomeAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","incomeAccount"),
                                               xmlWriter);
                                        } if (localIntercoIncomeAccountTracker){
                                            if (localIntercoIncomeAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoIncomeAccount cannot be null!!");
                                            }
                                           localIntercoIncomeAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","intercoIncomeAccount"),
                                               xmlWriter);
                                        } if (localTaxScheduleTracker){
                                            if (localTaxSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxSchedule cannot be null!!");
                                            }
                                           localTaxSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxSchedule"),
                                               xmlWriter);
                                        } if (localMatrixTypeTracker){
                                            if (localMatrixType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("matrixType cannot be null!!");
                                            }
                                           localMatrixType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixType"),
                                               xmlWriter);
                                        } if (localIsTaxableTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isTaxable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isTaxable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostEstimateTypeTracker){
                                            if (localCostEstimateType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                            }
                                           localCostEstimateType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimateType"),
                                               xmlWriter);
                                        } if (localCostEstimateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costEstimate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCostEstimate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("costEstimate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUnitsTypeTracker){
                                            if (localUnitsType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("unitsType cannot be null!!");
                                            }
                                           localUnitsType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","unitsType"),
                                               xmlWriter);
                                        } if (localPurchaseUnitTracker){
                                            if (localPurchaseUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseUnit cannot be null!!");
                                            }
                                           localPurchaseUnit.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseUnit"),
                                               xmlWriter);
                                        } if (localSaleUnitTracker){
                                            if (localSaleUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("saleUnit cannot be null!!");
                                            }
                                           localSaleUnit.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","saleUnit"),
                                               xmlWriter);
                                        } if (localCostEstimateUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "costEstimateUnits", xmlWriter);
                             

                                          if (localCostEstimateUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("costEstimateUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCostEstimateUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIssueProductTracker){
                                            if (localIssueProduct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueProduct cannot be null!!");
                                            }
                                           localIssueProduct.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","issueProduct"),
                                               xmlWriter);
                                        } if (localBillingScheduleTracker){
                                            if (localBillingSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                            }
                                           localBillingSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billingSchedule"),
                                               xmlWriter);
                                        } if (localDeferredRevenueAccountTracker){
                                            if (localDeferredRevenueAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deferredRevenueAccount cannot be null!!");
                                            }
                                           localDeferredRevenueAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferredRevenueAccount"),
                                               xmlWriter);
                                        } if (localIntercoDefRevAccountTracker){
                                            if (localIntercoDefRevAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("intercoDefRevAccount cannot be null!!");
                                            }
                                           localIntercoDefRevAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","intercoDefRevAccount"),
                                               xmlWriter);
                                        } if (localRevRecScheduleTracker){
                                            if (localRevRecSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                            }
                                           localRevRecSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecSchedule"),
                                               xmlWriter);
                                        } if (localDeferralAccountTracker){
                                            if (localDeferralAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deferralAccount cannot be null!!");
                                            }
                                           localDeferralAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferralAccount"),
                                               xmlWriter);
                                        } if (localAmortizationTemplateTracker){
                                            if (localAmortizationTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amortizationTemplate cannot be null!!");
                                            }
                                           localAmortizationTemplate.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","amortizationTemplate"),
                                               xmlWriter);
                                        } if (localResidualTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "residual", xmlWriter);
                             

                                          if (localResidual==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("residual cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localResidual);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDeferRevRecTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "deferRevRec", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("deferRevRec cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevenueRecognitionRuleTracker){
                                            if (localRevenueRecognitionRule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueRecognitionRule cannot be null!!");
                                            }
                                           localRevenueRecognitionRule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueRecognitionRule"),
                                               xmlWriter);
                                        } if (localRevRecForecastRuleTracker){
                                            if (localRevRecForecastRule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecForecastRule cannot be null!!");
                                            }
                                           localRevRecForecastRule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecForecastRule"),
                                               xmlWriter);
                                        } if (localRevenueAllocationGroupTracker){
                                            if (localRevenueAllocationGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueAllocationGroup cannot be null!!");
                                            }
                                           localRevenueAllocationGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueAllocationGroup"),
                                               xmlWriter);
                                        } if (localCreateRevenuePlansOnTracker){
                                            if (localCreateRevenuePlansOn==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createRevenuePlansOn cannot be null!!");
                                            }
                                           localCreateRevenuePlansOn.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createRevenuePlansOn"),
                                               xmlWriter);
                                        } if (localDirectRevenuePostingTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "directRevenuePosting", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("directRevenuePosting cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirectRevenuePosting));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localContingentRevenueHandlingTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "contingentRevenueHandling", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("contingentRevenueHandling cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContingentRevenueHandling));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRevReclassFXAccountTracker){
                                            if (localRevReclassFXAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revReclassFXAccount cannot be null!!");
                                            }
                                           localRevReclassFXAccount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revReclassFXAccount"),
                                               xmlWriter);
                                        } if (localAmortizationPeriodTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "amortizationPeriod", xmlWriter);
                             
                                               if (localAmortizationPeriod==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("amortizationPeriod cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationPeriod));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMinimumQuantityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "minimumQuantity", xmlWriter);
                             
                                               if (localMinimumQuantity==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("minimumQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinimumQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEnforceMinQtyInternallyTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "enforceMinQtyInternally", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("enforceMinQtyInternally cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnforceMinQtyInternally));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSoftDescriptorTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "softDescriptor", xmlWriter);
                             

                                          if (localSoftDescriptor==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("softDescriptor cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSoftDescriptor);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPricesIncludeTaxTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pricesIncludeTax", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("pricesIncludeTax cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPricesIncludeTax));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityPricingScheduleTracker){
                                            if (localQuantityPricingSchedule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityPricingSchedule cannot be null!!");
                                            }
                                           localQuantityPricingSchedule.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityPricingSchedule"),
                                               xmlWriter);
                                        } if (localUseMarginalRatesTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "useMarginalRates", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("useMarginalRates cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseMarginalRates));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOverallQuantityPricingTypeTracker){
                                            if (localOverallQuantityPricingType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("overallQuantityPricingType cannot be null!!");
                                            }
                                           localOverallQuantityPricingType.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","overallQuantityPricingType"),
                                               xmlWriter);
                                        } if (localIsFulfillableTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isFulfillable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isFulfillable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFulfillable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGenerateAccrualsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "generateAccruals", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("generateAccruals cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenerateAccruals));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostCategoryTracker){
                                            if (localCostCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costCategory cannot be null!!");
                                            }
                                           localCostCategory.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costCategory"),
                                               xmlWriter);
                                        } if (localPurchaseOrderQuantityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "purchaseOrderQuantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPurchaseOrderQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseOrderAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "purchaseOrderAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPurchaseOrderAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseOrderQuantityDiffTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "purchaseOrderQuantityDiff", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPurchaseOrderQuantityDiff)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderQuantityDiff cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderQuantityDiff));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReceiptQuantityTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "receiptQuantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localReceiptQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("receiptQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReceiptAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "receiptAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localReceiptAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("receiptAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReceiptQuantityDiffTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "receiptQuantityDiff", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localReceiptQuantityDiff)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("receiptQuantityDiff cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptQuantityDiff));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPricingGroupTracker){
                                            if (localPricingGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingGroup cannot be null!!");
                                            }
                                           localPricingGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingGroup"),
                                               xmlWriter);
                                        } if (localMinimumQuantityUnitsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "minimumQuantityUnits", xmlWriter);
                             

                                          if (localMinimumQuantityUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("minimumQuantityUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMinimumQuantityUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoePriceTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoePrice", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localVsoePrice)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoePrice cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVsoeSopGroupTracker){
                                            if (localVsoeSopGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                            }
                                           localVsoeSopGroup.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeSopGroup"),
                                               xmlWriter);
                                        } if (localVsoeDeferralTracker){
                                            if (localVsoeDeferral==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                            }
                                           localVsoeDeferral.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeDeferral"),
                                               xmlWriter);
                                        } if (localVsoePermitDiscountTracker){
                                            if (localVsoePermitDiscount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                            }
                                           localVsoePermitDiscount.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoePermitDiscount"),
                                               xmlWriter);
                                        } if (localVsoeDeliveredTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vsoeDelivered", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("vsoeDelivered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemRevenueCategoryTracker){
                                            if (localItemRevenueCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemRevenueCategory cannot be null!!");
                                            }
                                           localItemRevenueCategory.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemRevenueCategory"),
                                               xmlWriter);
                                        } if (localCreateJobTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createJob", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("createJob cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreateJob));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMatrixItemNameTemplateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "matrixItemNameTemplate", xmlWriter);
                             

                                          if (localMatrixItemNameTemplate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("matrixItemNameTemplate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMatrixItemNameTemplate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDisplayNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDisplayName", xmlWriter);
                             

                                          if (localStoreDisplayName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDisplayName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDisplayName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDisplayThumbnailTracker){
                                            if (localStoreDisplayThumbnail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeDisplayThumbnail cannot be null!!");
                                            }
                                           localStoreDisplayThumbnail.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayThumbnail"),
                                               xmlWriter);
                                        } if (localStoreDisplayImageTracker){
                                            if (localStoreDisplayImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeDisplayImage cannot be null!!");
                                            }
                                           localStoreDisplayImage.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayImage"),
                                               xmlWriter);
                                        } if (localStoreDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDescription", xmlWriter);
                             

                                          if (localStoreDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDetailedDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDetailedDescription", xmlWriter);
                             

                                          if (localStoreDetailedDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDetailedDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDetailedDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreItemTemplateTracker){
                                            if (localStoreItemTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeItemTemplate cannot be null!!");
                                            }
                                           localStoreItemTemplate.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeItemTemplate"),
                                               xmlWriter);
                                        } if (localPageTitleTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pageTitle", xmlWriter);
                             

                                          if (localPageTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("pageTitle cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPageTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUrlComponentTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "urlComponent", xmlWriter);
                             

                                          if (localUrlComponent==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("urlComponent cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUrlComponent);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMetaTagHtmlTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "metaTagHtml", xmlWriter);
                             

                                          if (localMetaTagHtml==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("metaTagHtml cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMetaTagHtml);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExcludeFromSitemapTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "excludeFromSitemap", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("excludeFromSitemap cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromSitemap));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSitemapPriorityTracker){
                                            if (localSitemapPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sitemapPriority cannot be null!!");
                                            }
                                           localSitemapPriority.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","sitemapPriority"),
                                               xmlWriter);
                                        } if (localSearchKeywordsTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "searchKeywords", xmlWriter);
                             

                                          if (localSearchKeywords==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("searchKeywords cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSearchKeywords);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsDonationItemTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isDonationItem", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isDonationItem cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDonationItem));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowDefaultDonationAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showDefaultDonationAmount", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showDefaultDonationAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowDefaultDonationAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMaxDonationAmountTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "maxDonationAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localMaxDonationAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("maxDonationAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxDonationAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDontShowPriceTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dontShowPrice", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("dontShowPrice cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDontShowPrice));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNoPriceMessageTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "noPriceMessage", xmlWriter);
                             

                                          if (localNoPriceMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("noPriceMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNoPriceMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOutOfStockMessageTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "outOfStockMessage", xmlWriter);
                             

                                          if (localOutOfStockMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("outOfStockMessage cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOutOfStockMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOnSpecialTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "onSpecial", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("onSpecial cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnSpecial));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOutOfStockBehaviorTracker){
                                            if (localOutOfStockBehavior==null){
                                                 throw new org.apache.axis2.databinding.ADBException("outOfStockBehavior cannot be null!!");
                                            }
                                           localOutOfStockBehavior.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","outOfStockBehavior"),
                                               xmlWriter);
                                        } if (localRelatedItemsDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "relatedItemsDescription", xmlWriter);
                             

                                          if (localRelatedItemsDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("relatedItemsDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRelatedItemsDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSpecialsDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "specialsDescription", xmlWriter);
                             

                                          if (localSpecialsDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("specialsDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSpecialsDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localItemTaskTemplatesListTracker){
                                            if (localItemTaskTemplatesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemTaskTemplatesList cannot be null!!");
                                            }
                                           localItemTaskTemplatesList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemTaskTemplatesList"),
                                               xmlWriter);
                                        } if (localFeaturedDescriptionTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "featuredDescription", xmlWriter);
                             

                                          if (localFeaturedDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("featuredDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFeaturedDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localItemIdTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "itemId", xmlWriter);
                             

                                          if (localItemId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localItemId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUpcCodeTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "upcCode", xmlWriter);
                             

                                          if (localUpcCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("upcCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUpcCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDisplayNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "displayName", xmlWriter);
                             

                                          if (localDisplayName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("displayName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDisplayName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVendorNameTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vendorName", xmlWriter);
                             

                                          if (localVendorName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("vendorName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localVendorName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localIsOnlineTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isOnline", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isOnline cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnline));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsGcoCompliantTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isGcoCompliant", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isGcoCompliant cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsGcoCompliant));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOfferSupportTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "offerSupport", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("offerSupport cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfferSupport));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsInactiveTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAvailableToPartnersTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "availableToPartners", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("availableToPartners cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableToPartners));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localSubsidiaryListTracker){
                                            if (localSubsidiaryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                            }
                                           localSubsidiaryList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "currency", xmlWriter);
                             

                                          if (localCurrency==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCurrency);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBillingRatesMatrixTracker){
                                            if (localBillingRatesMatrix==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingRatesMatrix cannot be null!!");
                                            }
                                           localBillingRatesMatrix.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billingRatesMatrix"),
                                               xmlWriter);
                                        } if (localAccountingBookDetailListTracker){
                                            if (localAccountingBookDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                            }
                                           localAccountingBookDetailList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","accountingBookDetailList"),
                                               xmlWriter);
                                        } if (localItemOptionsListTracker){
                                            if (localItemOptionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemOptionsList cannot be null!!");
                                            }
                                           localItemOptionsList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemOptionsList"),
                                               xmlWriter);
                                        } if (localMatrixOptionListTracker){
                                            if (localMatrixOptionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("matrixOptionList cannot be null!!");
                                            }
                                           localMatrixOptionList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixOptionList"),
                                               xmlWriter);
                                        } if (localItemVendorListTracker){
                                            if (localItemVendorList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemVendorList cannot be null!!");
                                            }
                                           localItemVendorList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemVendorList"),
                                               xmlWriter);
                                        } if (localPricingMatrixTracker){
                                            if (localPricingMatrix==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingMatrix cannot be null!!");
                                            }
                                           localPricingMatrix.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingMatrix"),
                                               xmlWriter);
                                        } if (localPurchaseTaxCodeTracker){
                                            if (localPurchaseTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseTaxCode cannot be null!!");
                                            }
                                           localPurchaseTaxCode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseTaxCode"),
                                               xmlWriter);
                                        } if (localRateTracker){
                                    namespace = "urn:accounting_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "rate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesTaxCodeTracker){
                                            if (localSalesTaxCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTaxCode cannot be null!!");
                                            }
                                           localSalesTaxCode.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","salesTaxCode"),
                                               xmlWriter);
                                        } if (localSiteCategoryListTracker){
                                            if (localSiteCategoryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("siteCategoryList cannot be null!!");
                                            }
                                           localSiteCategoryList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","siteCategoryList"),
                                               xmlWriter);
                                        } if (localTranslationsListTracker){
                                            if (localTranslationsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                            }
                                           localTranslationsList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","translationsList"),
                                               xmlWriter);
                                        } if (localVendorTracker){
                                            if (localVendor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendor cannot be null!!");
                                            }
                                           localVendor.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vendor"),
                                               xmlWriter);
                                        } if (localPresentationItemListTracker){
                                            if (localPresentationItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("presentationItemList cannot be null!!");
                                            }
                                           localPresentationItemList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","presentationItemList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns19";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","ServiceResaleItem"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localPurchaseDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseDescription"));
                                 
                                        if (localPurchaseDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("purchaseDescription cannot be null!!");
                                        }
                                    } if (localManufacturingChargeItemTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturingChargeItem"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localManufacturingChargeItem));
                            } if (localCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "cost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCost));
                            } if (localCostUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costUnits"));
                                 
                                        if (localCostUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("costUnits cannot be null!!");
                                        }
                                    } if (localExpenseAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "expenseAccount"));
                            
                            
                                    if (localExpenseAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("expenseAccount cannot be null!!");
                                    }
                                    elementList.add(localExpenseAccount);
                                } if (localIntercoExpenseAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "intercoExpenseAccount"));
                            
                            
                                    if (localIntercoExpenseAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoExpenseAccount cannot be null!!");
                                    }
                                    elementList.add(localIntercoExpenseAccount);
                                } if (localSalesDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "salesDescription"));
                                 
                                        if (localSalesDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalesDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("salesDescription cannot be null!!");
                                        }
                                    } if (localIncludeChildrenTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "includeChildren"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeChildren));
                            } if (localIncomeAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "incomeAccount"));
                            
                            
                                    if (localIncomeAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("incomeAccount cannot be null!!");
                                    }
                                    elementList.add(localIncomeAccount);
                                } if (localIntercoIncomeAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "intercoIncomeAccount"));
                            
                            
                                    if (localIntercoIncomeAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoIncomeAccount cannot be null!!");
                                    }
                                    elementList.add(localIntercoIncomeAccount);
                                } if (localTaxScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "taxSchedule"));
                            
                            
                                    if (localTaxSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxSchedule cannot be null!!");
                                    }
                                    elementList.add(localTaxSchedule);
                                } if (localMatrixTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "matrixType"));
                            
                            
                                    if (localMatrixType==null){
                                         throw new org.apache.axis2.databinding.ADBException("matrixType cannot be null!!");
                                    }
                                    elementList.add(localMatrixType);
                                } if (localIsTaxableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isTaxable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsTaxable));
                            } if (localCostEstimateTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costEstimateType"));
                            
                            
                                    if (localCostEstimateType==null){
                                         throw new org.apache.axis2.databinding.ADBException("costEstimateType cannot be null!!");
                                    }
                                    elementList.add(localCostEstimateType);
                                } if (localCostEstimateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costEstimate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimate));
                            } if (localUnitsTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "unitsType"));
                            
                            
                                    if (localUnitsType==null){
                                         throw new org.apache.axis2.databinding.ADBException("unitsType cannot be null!!");
                                    }
                                    elementList.add(localUnitsType);
                                } if (localPurchaseUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseUnit"));
                            
                            
                                    if (localPurchaseUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseUnit cannot be null!!");
                                    }
                                    elementList.add(localPurchaseUnit);
                                } if (localSaleUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "saleUnit"));
                            
                            
                                    if (localSaleUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("saleUnit cannot be null!!");
                                    }
                                    elementList.add(localSaleUnit);
                                } if (localCostEstimateUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costEstimateUnits"));
                                 
                                        if (localCostEstimateUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCostEstimateUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("costEstimateUnits cannot be null!!");
                                        }
                                    } if (localIssueProductTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "issueProduct"));
                            
                            
                                    if (localIssueProduct==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueProduct cannot be null!!");
                                    }
                                    elementList.add(localIssueProduct);
                                } if (localBillingScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "billingSchedule"));
                            
                            
                                    if (localBillingSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                    }
                                    elementList.add(localBillingSchedule);
                                } if (localDeferredRevenueAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "deferredRevenueAccount"));
                            
                            
                                    if (localDeferredRevenueAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("deferredRevenueAccount cannot be null!!");
                                    }
                                    elementList.add(localDeferredRevenueAccount);
                                } if (localIntercoDefRevAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "intercoDefRevAccount"));
                            
                            
                                    if (localIntercoDefRevAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("intercoDefRevAccount cannot be null!!");
                                    }
                                    elementList.add(localIntercoDefRevAccount);
                                } if (localRevRecScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revRecSchedule"));
                            
                            
                                    if (localRevRecSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecSchedule cannot be null!!");
                                    }
                                    elementList.add(localRevRecSchedule);
                                } if (localDeferralAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "deferralAccount"));
                            
                            
                                    if (localDeferralAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("deferralAccount cannot be null!!");
                                    }
                                    elementList.add(localDeferralAccount);
                                } if (localAmortizationTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "amortizationTemplate"));
                            
                            
                                    if (localAmortizationTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("amortizationTemplate cannot be null!!");
                                    }
                                    elementList.add(localAmortizationTemplate);
                                } if (localResidualTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "residual"));
                                 
                                        if (localResidual != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResidual));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("residual cannot be null!!");
                                        }
                                    } if (localDeferRevRecTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "deferRevRec"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeferRevRec));
                            } if (localRevenueRecognitionRuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revenueRecognitionRule"));
                            
                            
                                    if (localRevenueRecognitionRule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueRecognitionRule cannot be null!!");
                                    }
                                    elementList.add(localRevenueRecognitionRule);
                                } if (localRevRecForecastRuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revRecForecastRule"));
                            
                            
                                    if (localRevRecForecastRule==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecForecastRule cannot be null!!");
                                    }
                                    elementList.add(localRevRecForecastRule);
                                } if (localRevenueAllocationGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revenueAllocationGroup"));
                            
                            
                                    if (localRevenueAllocationGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueAllocationGroup cannot be null!!");
                                    }
                                    elementList.add(localRevenueAllocationGroup);
                                } if (localCreateRevenuePlansOnTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "createRevenuePlansOn"));
                            
                            
                                    if (localCreateRevenuePlansOn==null){
                                         throw new org.apache.axis2.databinding.ADBException("createRevenuePlansOn cannot be null!!");
                                    }
                                    elementList.add(localCreateRevenuePlansOn);
                                } if (localDirectRevenuePostingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "directRevenuePosting"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirectRevenuePosting));
                            } if (localContingentRevenueHandlingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "contingentRevenueHandling"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localContingentRevenueHandling));
                            } if (localRevReclassFXAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "revReclassFXAccount"));
                            
                            
                                    if (localRevReclassFXAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("revReclassFXAccount cannot be null!!");
                                    }
                                    elementList.add(localRevReclassFXAccount);
                                } if (localAmortizationPeriodTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "amortizationPeriod"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAmortizationPeriod));
                            } if (localMinimumQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "minimumQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinimumQuantity));
                            } if (localEnforceMinQtyInternallyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "enforceMinQtyInternally"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnforceMinQtyInternally));
                            } if (localSoftDescriptorTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "softDescriptor"));
                                 
                                        if (localSoftDescriptor != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSoftDescriptor));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("softDescriptor cannot be null!!");
                                        }
                                    } if (localPricesIncludeTaxTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricesIncludeTax"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPricesIncludeTax));
                            } if (localQuantityPricingScheduleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "quantityPricingSchedule"));
                            
                            
                                    if (localQuantityPricingSchedule==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityPricingSchedule cannot be null!!");
                                    }
                                    elementList.add(localQuantityPricingSchedule);
                                } if (localUseMarginalRatesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "useMarginalRates"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUseMarginalRates));
                            } if (localOverallQuantityPricingTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "overallQuantityPricingType"));
                            
                            
                                    if (localOverallQuantityPricingType==null){
                                         throw new org.apache.axis2.databinding.ADBException("overallQuantityPricingType cannot be null!!");
                                    }
                                    elementList.add(localOverallQuantityPricingType);
                                } if (localIsFulfillableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isFulfillable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFulfillable));
                            } if (localGenerateAccrualsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "generateAccruals"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGenerateAccruals));
                            } if (localCostCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "costCategory"));
                            
                            
                                    if (localCostCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("costCategory cannot be null!!");
                                    }
                                    elementList.add(localCostCategory);
                                } if (localPurchaseOrderQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseOrderQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderQuantity));
                            } if (localPurchaseOrderAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseOrderAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderAmount));
                            } if (localPurchaseOrderQuantityDiffTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseOrderQuantityDiff"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderQuantityDiff));
                            } if (localReceiptQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "receiptQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptQuantity));
                            } if (localReceiptAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "receiptAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptAmount));
                            } if (localReceiptQuantityDiffTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "receiptQuantityDiff"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReceiptQuantityDiff));
                            } if (localPricingGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricingGroup"));
                            
                            
                                    if (localPricingGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingGroup cannot be null!!");
                                    }
                                    elementList.add(localPricingGroup);
                                } if (localMinimumQuantityUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "minimumQuantityUnits"));
                                 
                                        if (localMinimumQuantityUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinimumQuantityUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("minimumQuantityUnits cannot be null!!");
                                        }
                                    } if (localVsoePriceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoePrice"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoePrice));
                            } if (localVsoeSopGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoeSopGroup"));
                            
                            
                                    if (localVsoeSopGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeSopGroup cannot be null!!");
                                    }
                                    elementList.add(localVsoeSopGroup);
                                } if (localVsoeDeferralTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoeDeferral"));
                            
                            
                                    if (localVsoeDeferral==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeDeferral cannot be null!!");
                                    }
                                    elementList.add(localVsoeDeferral);
                                } if (localVsoePermitDiscountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoePermitDiscount"));
                            
                            
                                    if (localVsoePermitDiscount==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoePermitDiscount cannot be null!!");
                                    }
                                    elementList.add(localVsoePermitDiscount);
                                } if (localVsoeDeliveredTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vsoeDelivered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVsoeDelivered));
                            } if (localItemRevenueCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemRevenueCategory"));
                            
                            
                                    if (localItemRevenueCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemRevenueCategory cannot be null!!");
                                    }
                                    elementList.add(localItemRevenueCategory);
                                } if (localCreateJobTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "createJob"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreateJob));
                            } if (localMatrixItemNameTemplateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "matrixItemNameTemplate"));
                                 
                                        if (localMatrixItemNameTemplate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMatrixItemNameTemplate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("matrixItemNameTemplate cannot be null!!");
                                        }
                                    } if (localStoreDisplayNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayName"));
                                 
                                        if (localStoreDisplayName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDisplayName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDisplayName cannot be null!!");
                                        }
                                    } if (localStoreDisplayThumbnailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayThumbnail"));
                            
                            
                                    if (localStoreDisplayThumbnail==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeDisplayThumbnail cannot be null!!");
                                    }
                                    elementList.add(localStoreDisplayThumbnail);
                                } if (localStoreDisplayImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayImage"));
                            
                            
                                    if (localStoreDisplayImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeDisplayImage cannot be null!!");
                                    }
                                    elementList.add(localStoreDisplayImage);
                                } if (localStoreDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDescription"));
                                 
                                        if (localStoreDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDescription cannot be null!!");
                                        }
                                    } if (localStoreDetailedDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDetailedDescription"));
                                 
                                        if (localStoreDetailedDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDetailedDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDetailedDescription cannot be null!!");
                                        }
                                    } if (localStoreItemTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "storeItemTemplate"));
                            
                            
                                    if (localStoreItemTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeItemTemplate cannot be null!!");
                                    }
                                    elementList.add(localStoreItemTemplate);
                                } if (localPageTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pageTitle"));
                                 
                                        if (localPageTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPageTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("pageTitle cannot be null!!");
                                        }
                                    } if (localUrlComponentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "urlComponent"));
                                 
                                        if (localUrlComponent != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUrlComponent));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("urlComponent cannot be null!!");
                                        }
                                    } if (localMetaTagHtmlTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "metaTagHtml"));
                                 
                                        if (localMetaTagHtml != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMetaTagHtml));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("metaTagHtml cannot be null!!");
                                        }
                                    } if (localExcludeFromSitemapTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "excludeFromSitemap"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromSitemap));
                            } if (localSitemapPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "sitemapPriority"));
                            
                            
                                    if (localSitemapPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("sitemapPriority cannot be null!!");
                                    }
                                    elementList.add(localSitemapPriority);
                                } if (localSearchKeywordsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "searchKeywords"));
                                 
                                        if (localSearchKeywords != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSearchKeywords));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("searchKeywords cannot be null!!");
                                        }
                                    } if (localIsDonationItemTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isDonationItem"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsDonationItem));
                            } if (localShowDefaultDonationAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "showDefaultDonationAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowDefaultDonationAmount));
                            } if (localMaxDonationAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "maxDonationAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxDonationAmount));
                            } if (localDontShowPriceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "dontShowPrice"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDontShowPrice));
                            } if (localNoPriceMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "noPriceMessage"));
                                 
                                        if (localNoPriceMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoPriceMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("noPriceMessage cannot be null!!");
                                        }
                                    } if (localOutOfStockMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "outOfStockMessage"));
                                 
                                        if (localOutOfStockMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOutOfStockMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("outOfStockMessage cannot be null!!");
                                        }
                                    } if (localOnSpecialTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "onSpecial"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnSpecial));
                            } if (localOutOfStockBehaviorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "outOfStockBehavior"));
                            
                            
                                    if (localOutOfStockBehavior==null){
                                         throw new org.apache.axis2.databinding.ADBException("outOfStockBehavior cannot be null!!");
                                    }
                                    elementList.add(localOutOfStockBehavior);
                                } if (localRelatedItemsDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "relatedItemsDescription"));
                                 
                                        if (localRelatedItemsDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRelatedItemsDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("relatedItemsDescription cannot be null!!");
                                        }
                                    } if (localSpecialsDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "specialsDescription"));
                                 
                                        if (localSpecialsDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSpecialsDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("specialsDescription cannot be null!!");
                                        }
                                    } if (localItemTaskTemplatesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemTaskTemplatesList"));
                            
                            
                                    if (localItemTaskTemplatesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemTaskTemplatesList cannot be null!!");
                                    }
                                    elementList.add(localItemTaskTemplatesList);
                                } if (localFeaturedDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "featuredDescription"));
                                 
                                        if (localFeaturedDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFeaturedDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("featuredDescription cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localItemIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemId"));
                                 
                                        if (localItemId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                        }
                                    } if (localUpcCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "upcCode"));
                                 
                                        if (localUpcCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUpcCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("upcCode cannot be null!!");
                                        }
                                    } if (localDisplayNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "displayName"));
                                 
                                        if (localDisplayName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisplayName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("displayName cannot be null!!");
                                        }
                                    } if (localVendorNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vendorName"));
                                 
                                        if (localVendorName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVendorName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("vendorName cannot be null!!");
                                        }
                                    } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localIsOnlineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isOnline"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnline));
                            } if (localIsGcoCompliantTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isGcoCompliant"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsGcoCompliant));
                            } if (localOfferSupportTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "offerSupport"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfferSupport));
                            } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localAvailableToPartnersTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "availableToPartners"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableToPartners));
                            } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localSubsidiaryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiaryList"));
                            
                            
                                    if (localSubsidiaryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiaryList cannot be null!!");
                                    }
                                    elementList.add(localSubsidiaryList);
                                } if (localCurrencyTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "currency"));
                                 
                                        if (localCurrency != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCurrency));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                        }
                                    } if (localBillingRatesMatrixTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "billingRatesMatrix"));
                            
                            
                                    if (localBillingRatesMatrix==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingRatesMatrix cannot be null!!");
                                    }
                                    elementList.add(localBillingRatesMatrix);
                                } if (localAccountingBookDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "accountingBookDetailList"));
                            
                            
                                    if (localAccountingBookDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBookDetailList cannot be null!!");
                                    }
                                    elementList.add(localAccountingBookDetailList);
                                } if (localItemOptionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemOptionsList"));
                            
                            
                                    if (localItemOptionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemOptionsList cannot be null!!");
                                    }
                                    elementList.add(localItemOptionsList);
                                } if (localMatrixOptionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "matrixOptionList"));
                            
                            
                                    if (localMatrixOptionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("matrixOptionList cannot be null!!");
                                    }
                                    elementList.add(localMatrixOptionList);
                                } if (localItemVendorListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "itemVendorList"));
                            
                            
                                    if (localItemVendorList==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemVendorList cannot be null!!");
                                    }
                                    elementList.add(localItemVendorList);
                                } if (localPricingMatrixTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "pricingMatrix"));
                            
                            
                                    if (localPricingMatrix==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingMatrix cannot be null!!");
                                    }
                                    elementList.add(localPricingMatrix);
                                } if (localPurchaseTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseTaxCode"));
                            
                            
                                    if (localPurchaseTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseTaxCode cannot be null!!");
                                    }
                                    elementList.add(localPurchaseTaxCode);
                                } if (localRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "rate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRate));
                            } if (localSalesTaxCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "salesTaxCode"));
                            
                            
                                    if (localSalesTaxCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTaxCode cannot be null!!");
                                    }
                                    elementList.add(localSalesTaxCode);
                                } if (localSiteCategoryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "siteCategoryList"));
                            
                            
                                    if (localSiteCategoryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("siteCategoryList cannot be null!!");
                                    }
                                    elementList.add(localSiteCategoryList);
                                } if (localTranslationsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "translationsList"));
                            
                            
                                    if (localTranslationsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                    }
                                    elementList.add(localTranslationsList);
                                } if (localVendorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "vendor"));
                            
                            
                                    if (localVendor==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendor cannot be null!!");
                                    }
                                    elementList.add(localVendor);
                                } if (localPresentationItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "presentationItemList"));
                            
                            
                                    if (localPresentationItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("presentationItemList cannot be null!!");
                                    }
                                    elementList.add(localPresentationItemList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ServiceResaleItem parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ServiceResaleItem object =
                new ServiceResaleItem();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ServiceResaleItem".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ServiceResaleItem)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"purchaseDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPurchaseDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","manufacturingChargeItem").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"manufacturingChargeItem" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setManufacturingChargeItem(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","cost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"cost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","expenseAccount").equals(reader.getName())){
                                
                                                object.setExpenseAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","intercoExpenseAccount").equals(reader.getName())){
                                
                                                object.setIntercoExpenseAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","salesDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"salesDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSalesDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","includeChildren").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"includeChildren" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncludeChildren(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","incomeAccount").equals(reader.getName())){
                                
                                                object.setIncomeAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","intercoIncomeAccount").equals(reader.getName())){
                                
                                                object.setIntercoIncomeAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","taxSchedule").equals(reader.getName())){
                                
                                                object.setTaxSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixType").equals(reader.getName())){
                                
                                                object.setMatrixType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isTaxable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isTaxable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsTaxable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimateType").equals(reader.getName())){
                                
                                                object.setCostEstimateType(com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costEstimate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostEstimate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCostEstimate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","unitsType").equals(reader.getName())){
                                
                                                object.setUnitsType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseUnit").equals(reader.getName())){
                                
                                                object.setPurchaseUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","saleUnit").equals(reader.getName())){
                                
                                                object.setSaleUnit(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costEstimateUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"costEstimateUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCostEstimateUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","issueProduct").equals(reader.getName())){
                                
                                                object.setIssueProduct(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billingSchedule").equals(reader.getName())){
                                
                                                object.setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferredRevenueAccount").equals(reader.getName())){
                                
                                                object.setDeferredRevenueAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","intercoDefRevAccount").equals(reader.getName())){
                                
                                                object.setIntercoDefRevAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecSchedule").equals(reader.getName())){
                                
                                                object.setRevRecSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferralAccount").equals(reader.getName())){
                                
                                                object.setDeferralAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","amortizationTemplate").equals(reader.getName())){
                                
                                                object.setAmortizationTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","residual").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"residual" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setResidual(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","deferRevRec").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"deferRevRec" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDeferRevRec(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueRecognitionRule").equals(reader.getName())){
                                
                                                object.setRevenueRecognitionRule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revRecForecastRule").equals(reader.getName())){
                                
                                                object.setRevRecForecastRule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revenueAllocationGroup").equals(reader.getName())){
                                
                                                object.setRevenueAllocationGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createRevenuePlansOn").equals(reader.getName())){
                                
                                                object.setCreateRevenuePlansOn(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","directRevenuePosting").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"directRevenuePosting" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDirectRevenuePosting(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","contingentRevenueHandling").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"contingentRevenueHandling" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setContingentRevenueHandling(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","revReclassFXAccount").equals(reader.getName())){
                                
                                                object.setRevReclassFXAccount(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","amortizationPeriod").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"amortizationPeriod" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAmortizationPeriod(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAmortizationPeriod(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","minimumQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"minimumQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMinimumQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMinimumQuantity(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","enforceMinQtyInternally").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"enforceMinQtyInternally" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEnforceMinQtyInternally(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","softDescriptor").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"softDescriptor" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSoftDescriptor(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricesIncludeTax").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pricesIncludeTax" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPricesIncludeTax(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","quantityPricingSchedule").equals(reader.getName())){
                                
                                                object.setQuantityPricingSchedule(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","useMarginalRates").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"useMarginalRates" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUseMarginalRates(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","overallQuantityPricingType").equals(reader.getName())){
                                
                                                object.setOverallQuantityPricingType(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isFulfillable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isFulfillable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsFulfillable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","generateAccruals").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"generateAccruals" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGenerateAccruals(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","costCategory").equals(reader.getName())){
                                
                                                object.setCostCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseOrderQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"purchaseOrderQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPurchaseOrderQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPurchaseOrderQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseOrderAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"purchaseOrderAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPurchaseOrderAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPurchaseOrderAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseOrderQuantityDiff").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"purchaseOrderQuantityDiff" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPurchaseOrderQuantityDiff(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPurchaseOrderQuantityDiff(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","receiptQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"receiptQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReceiptQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setReceiptQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","receiptAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"receiptAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReceiptAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setReceiptAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","receiptQuantityDiff").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"receiptQuantityDiff" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReceiptQuantityDiff(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setReceiptQuantityDiff(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingGroup").equals(reader.getName())){
                                
                                                object.setPricingGroup(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","minimumQuantityUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"minimumQuantityUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMinimumQuantityUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoePrice").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoePrice" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoePrice(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setVsoePrice(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeSopGroup").equals(reader.getName())){
                                
                                                object.setVsoeSopGroup(com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeDeferral").equals(reader.getName())){
                                
                                                object.setVsoeDeferral(com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoePermitDiscount").equals(reader.getName())){
                                
                                                object.setVsoePermitDiscount(com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vsoeDelivered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vsoeDelivered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVsoeDelivered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemRevenueCategory").equals(reader.getName())){
                                
                                                object.setItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","createJob").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createJob" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreateJob(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixItemNameTemplate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"matrixItemNameTemplate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMatrixItemNameTemplate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDisplayName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDisplayName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayThumbnail").equals(reader.getName())){
                                
                                                object.setStoreDisplayThumbnail(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDisplayImage").equals(reader.getName())){
                                
                                                object.setStoreDisplayImage(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeDetailedDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDetailedDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDetailedDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","storeItemTemplate").equals(reader.getName())){
                                
                                                object.setStoreItemTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pageTitle").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pageTitle" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPageTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","urlComponent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"urlComponent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUrlComponent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","metaTagHtml").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"metaTagHtml" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMetaTagHtml(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","excludeFromSitemap").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"excludeFromSitemap" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExcludeFromSitemap(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","sitemapPriority").equals(reader.getName())){
                                
                                                object.setSitemapPriority(com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","searchKeywords").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"searchKeywords" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSearchKeywords(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isDonationItem").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isDonationItem" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsDonationItem(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","showDefaultDonationAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showDefaultDonationAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowDefaultDonationAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","maxDonationAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"maxDonationAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMaxDonationAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMaxDonationAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","dontShowPrice").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dontShowPrice" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDontShowPrice(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","noPriceMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"noPriceMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNoPriceMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","outOfStockMessage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"outOfStockMessage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOutOfStockMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","onSpecial").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"onSpecial" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOnSpecial(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","outOfStockBehavior").equals(reader.getName())){
                                
                                                object.setOutOfStockBehavior(com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","relatedItemsDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"relatedItemsDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRelatedItemsDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","specialsDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"specialsDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSpecialsDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemTaskTemplatesList").equals(reader.getName())){
                                
                                                object.setItemTaskTemplatesList(com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplatesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","featuredDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"featuredDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFeaturedDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"itemId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setItemId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","upcCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"upcCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUpcCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","displayName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"displayName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDisplayName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vendorName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vendorName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVendorName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isOnline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isOnline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsOnline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isGcoCompliant").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isGcoCompliant" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsGcoCompliant(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","offerSupport").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"offerSupport" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOfferSupport(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","availableToPartners").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"availableToPartners" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAvailableToPartners(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","subsidiaryList").equals(reader.getName())){
                                
                                                object.setSubsidiaryList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"currency" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCurrency(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","billingRatesMatrix").equals(reader.getName())){
                                
                                                object.setBillingRatesMatrix(com.netsuite.webservices.lists.accounting_2017_2.BillingRatesMatrix.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","accountingBookDetailList").equals(reader.getName())){
                                
                                                object.setAccountingBookDetailList(com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemOptionsList").equals(reader.getName())){
                                
                                                object.setItemOptionsList(com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","matrixOptionList").equals(reader.getName())){
                                
                                                object.setMatrixOptionList(com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","itemVendorList").equals(reader.getName())){
                                
                                                object.setItemVendorList(com.netsuite.webservices.lists.accounting_2017_2.ItemVendorList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","pricingMatrix").equals(reader.getName())){
                                
                                                object.setPricingMatrix(com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","purchaseTaxCode").equals(reader.getName())){
                                
                                                object.setPurchaseTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"rate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","salesTaxCode").equals(reader.getName())){
                                
                                                object.setSalesTaxCode(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","siteCategoryList").equals(reader.getName())){
                                
                                                object.setSiteCategoryList(com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","translationsList").equals(reader.getName())){
                                
                                                object.setTranslationsList(com.netsuite.webservices.lists.accounting_2017_2.TranslationList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","vendor").equals(reader.getName())){
                                
                                                object.setVendor(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","presentationItemList").equals(reader.getName())){
                                
                                                object.setPresentationItemList(com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:accounting_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    