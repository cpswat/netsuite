
/**
 * ItemAccountMappingItemAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.accounting_2017_2.types;
            

            /**
            *  ItemAccountMappingItemAccount bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemAccountMappingItemAccount
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.accounting_2017_2.lists.webservices.netsuite.com",
                "ItemAccountMappingItemAccount",
                "ns18");

            

                        /**
                        * field for ItemAccountMappingItemAccount
                        */

                        
                                    protected java.lang.String localItemAccountMappingItemAccount ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected ItemAccountMappingItemAccount(java.lang.String value, boolean isRegisterValue) {
                                    localItemAccountMappingItemAccount = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localItemAccountMappingItemAccount, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __asset =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_asset");
                                
                                    public static final java.lang.String __costOfGoods =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_costOfGoods");
                                
                                    public static final java.lang.String __customerReturnVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerReturnVariance");
                                
                                    public static final java.lang.String __deferral =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deferral");
                                
                                    public static final java.lang.String __deferredRevenue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deferredRevenue");
                                
                                    public static final java.lang.String __discount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_discount");
                                
                                    public static final java.lang.String __dropShipExpense =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dropShipExpense");
                                
                                    public static final java.lang.String __exchangeRateVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_exchangeRateVariance");
                                
                                    public static final java.lang.String __expense =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expense");
                                
                                    public static final java.lang.String __foreignCurrencyAdjustmentRevenueAccount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_foreignCurrencyAdjustmentRevenueAccount");
                                
                                    public static final java.lang.String __gainLoss =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gainLoss");
                                
                                    public static final java.lang.String __income =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_income");
                                
                                    public static final java.lang.String __intercompanyCostOfGoods =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_intercompanyCostOfGoods");
                                
                                    public static final java.lang.String __intercompanyDeferredRevenue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_intercompanyDeferredRevenue");
                                
                                    public static final java.lang.String __intercompanyExpense =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_intercompanyExpense");
                                
                                    public static final java.lang.String __intercompanyIncome =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_intercompanyIncome");
                                
                                    public static final java.lang.String __liability =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_liability");
                                
                                    public static final java.lang.String __markup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_markup");
                                
                                    public static final java.lang.String __payment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payment");
                                
                                    public static final java.lang.String __priceVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_priceVariance");
                                
                                    public static final java.lang.String __productionPriceVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_productionPriceVariance");
                                
                                    public static final java.lang.String __productionQuantityVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_productionQuantityVariance");
                                
                                    public static final java.lang.String __purchasePriceVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchasePriceVariance");
                                
                                    public static final java.lang.String __quantityVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_quantityVariance");
                                
                                    public static final java.lang.String __scrap =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_scrap");
                                
                                    public static final java.lang.String __unbuildVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unbuildVariance");
                                
                                    public static final java.lang.String __vendorReturnVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnVariance");
                                
                                    public static final java.lang.String __wipVariance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_wipVariance");
                                
                                    public static final java.lang.String __workInProcess =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workInProcess");
                                
                                    public static final java.lang.String __writeOff =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_writeOff");
                                
                                public static final ItemAccountMappingItemAccount _asset =
                                    new ItemAccountMappingItemAccount(__asset,true);
                            
                                public static final ItemAccountMappingItemAccount _costOfGoods =
                                    new ItemAccountMappingItemAccount(__costOfGoods,true);
                            
                                public static final ItemAccountMappingItemAccount _customerReturnVariance =
                                    new ItemAccountMappingItemAccount(__customerReturnVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _deferral =
                                    new ItemAccountMappingItemAccount(__deferral,true);
                            
                                public static final ItemAccountMappingItemAccount _deferredRevenue =
                                    new ItemAccountMappingItemAccount(__deferredRevenue,true);
                            
                                public static final ItemAccountMappingItemAccount _discount =
                                    new ItemAccountMappingItemAccount(__discount,true);
                            
                                public static final ItemAccountMappingItemAccount _dropShipExpense =
                                    new ItemAccountMappingItemAccount(__dropShipExpense,true);
                            
                                public static final ItemAccountMappingItemAccount _exchangeRateVariance =
                                    new ItemAccountMappingItemAccount(__exchangeRateVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _expense =
                                    new ItemAccountMappingItemAccount(__expense,true);
                            
                                public static final ItemAccountMappingItemAccount _foreignCurrencyAdjustmentRevenueAccount =
                                    new ItemAccountMappingItemAccount(__foreignCurrencyAdjustmentRevenueAccount,true);
                            
                                public static final ItemAccountMappingItemAccount _gainLoss =
                                    new ItemAccountMappingItemAccount(__gainLoss,true);
                            
                                public static final ItemAccountMappingItemAccount _income =
                                    new ItemAccountMappingItemAccount(__income,true);
                            
                                public static final ItemAccountMappingItemAccount _intercompanyCostOfGoods =
                                    new ItemAccountMappingItemAccount(__intercompanyCostOfGoods,true);
                            
                                public static final ItemAccountMappingItemAccount _intercompanyDeferredRevenue =
                                    new ItemAccountMappingItemAccount(__intercompanyDeferredRevenue,true);
                            
                                public static final ItemAccountMappingItemAccount _intercompanyExpense =
                                    new ItemAccountMappingItemAccount(__intercompanyExpense,true);
                            
                                public static final ItemAccountMappingItemAccount _intercompanyIncome =
                                    new ItemAccountMappingItemAccount(__intercompanyIncome,true);
                            
                                public static final ItemAccountMappingItemAccount _liability =
                                    new ItemAccountMappingItemAccount(__liability,true);
                            
                                public static final ItemAccountMappingItemAccount _markup =
                                    new ItemAccountMappingItemAccount(__markup,true);
                            
                                public static final ItemAccountMappingItemAccount _payment =
                                    new ItemAccountMappingItemAccount(__payment,true);
                            
                                public static final ItemAccountMappingItemAccount _priceVariance =
                                    new ItemAccountMappingItemAccount(__priceVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _productionPriceVariance =
                                    new ItemAccountMappingItemAccount(__productionPriceVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _productionQuantityVariance =
                                    new ItemAccountMappingItemAccount(__productionQuantityVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _purchasePriceVariance =
                                    new ItemAccountMappingItemAccount(__purchasePriceVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _quantityVariance =
                                    new ItemAccountMappingItemAccount(__quantityVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _scrap =
                                    new ItemAccountMappingItemAccount(__scrap,true);
                            
                                public static final ItemAccountMappingItemAccount _unbuildVariance =
                                    new ItemAccountMappingItemAccount(__unbuildVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _vendorReturnVariance =
                                    new ItemAccountMappingItemAccount(__vendorReturnVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _wipVariance =
                                    new ItemAccountMappingItemAccount(__wipVariance,true);
                            
                                public static final ItemAccountMappingItemAccount _workInProcess =
                                    new ItemAccountMappingItemAccount(__workInProcess,true);
                            
                                public static final ItemAccountMappingItemAccount _writeOff =
                                    new ItemAccountMappingItemAccount(__writeOff,true);
                            

                                public java.lang.String getValue() { return localItemAccountMappingItemAccount;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localItemAccountMappingItemAccount.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.accounting_2017_2.lists.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":ItemAccountMappingItemAccount",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "ItemAccountMappingItemAccount",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localItemAccountMappingItemAccount==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("ItemAccountMappingItemAccount cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localItemAccountMappingItemAccount);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.accounting_2017_2.lists.webservices.netsuite.com")){
                return "ns18";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemAccountMappingItemAccount)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static ItemAccountMappingItemAccount fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    ItemAccountMappingItemAccount enumeration = (ItemAccountMappingItemAccount)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static ItemAccountMappingItemAccount fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static ItemAccountMappingItemAccount fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return ItemAccountMappingItemAccount.Factory.fromString(content,namespaceUri);
                    } else {
                       return ItemAccountMappingItemAccount.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemAccountMappingItemAccount parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemAccountMappingItemAccount object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ItemAccountMappingItemAccount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = ItemAccountMappingItemAccount.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = ItemAccountMappingItemAccount.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    