
/**
 * ContactSearchRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.relationships_2017_2;
            

            /**
            *  ContactSearchRow bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ContactSearchRow extends com.netsuite.webservices.platform.core_2017_2.SearchRow
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ContactSearchRow
                Namespace URI = urn:relationships_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns15
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for CallJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic localCallJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCallJoinTracker = false ;

                           public boolean isCallJoinSpecified(){
                               return localCallJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic getCallJoin(){
                               return localCallJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CallJoin
                               */
                               public void setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic param){
                            localCallJoinTracker = param != null;
                                   
                                            this.localCallJoin=param;
                                    

                               }
                            

                        /**
                        * field for CampaignResponseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic localCampaignResponseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignResponseJoinTracker = false ;

                           public boolean isCampaignResponseJoinSpecified(){
                               return localCampaignResponseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic getCampaignResponseJoin(){
                               return localCampaignResponseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignResponseJoin
                               */
                               public void setCampaignResponseJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic param){
                            localCampaignResponseJoinTracker = param != null;
                                   
                                            this.localCampaignResponseJoin=param;
                                    

                               }
                            

                        /**
                        * field for CaseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic localCaseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseJoinTracker = false ;

                           public boolean isCaseJoinSpecified(){
                               return localCaseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic getCaseJoin(){
                               return localCaseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseJoin
                               */
                               public void setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic param){
                            localCaseJoinTracker = param != null;
                                   
                                            this.localCaseJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerJoinTracker = false ;

                           public boolean isCustomerJoinSpecified(){
                               return localCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getCustomerJoin(){
                               return localCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerJoin
                               */
                               public void setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localCustomerJoinTracker = param != null;
                                   
                                            this.localCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerPrimaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localCustomerPrimaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerPrimaryJoinTracker = false ;

                           public boolean isCustomerPrimaryJoinSpecified(){
                               return localCustomerPrimaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getCustomerPrimaryJoin(){
                               return localCustomerPrimaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerPrimaryJoin
                               */
                               public void setCustomerPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localCustomerPrimaryJoinTracker = param != null;
                                   
                                            this.localCustomerPrimaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for EventJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic localEventJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventJoinTracker = false ;

                           public boolean isEventJoinSpecified(){
                               return localEventJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic getEventJoin(){
                               return localEventJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventJoin
                               */
                               public void setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic param){
                            localEventJoinTracker = param != null;
                                   
                                            this.localEventJoin=param;
                                    

                               }
                            

                        /**
                        * field for FileJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic localFileJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFileJoinTracker = false ;

                           public boolean isFileJoinSpecified(){
                               return localFileJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic getFileJoin(){
                               return localFileJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FileJoin
                               */
                               public void setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic param){
                            localFileJoinTracker = param != null;
                                   
                                            this.localFileJoin=param;
                                    

                               }
                            

                        /**
                        * field for JobJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic localJobJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobJoinTracker = false ;

                           public boolean isJobJoinSpecified(){
                               return localJobJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic getJobJoin(){
                               return localJobJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobJoin
                               */
                               public void setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic param){
                            localJobJoinTracker = param != null;
                                   
                                            this.localJobJoin=param;
                                    

                               }
                            

                        /**
                        * field for JobPrimaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic localJobPrimaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobPrimaryJoinTracker = false ;

                           public boolean isJobPrimaryJoinSpecified(){
                               return localJobPrimaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic getJobPrimaryJoin(){
                               return localJobPrimaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobPrimaryJoin
                               */
                               public void setJobPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic param){
                            localJobPrimaryJoinTracker = param != null;
                                   
                                            this.localJobPrimaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic localMessagesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesJoinTracker = false ;

                           public boolean isMessagesJoinSpecified(){
                               return localMessagesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic getMessagesJoin(){
                               return localMessagesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesJoin
                               */
                               public void setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic param){
                            localMessagesJoinTracker = param != null;
                                   
                                            this.localMessagesJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesFromJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic localMessagesFromJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesFromJoinTracker = false ;

                           public boolean isMessagesFromJoinSpecified(){
                               return localMessagesFromJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic getMessagesFromJoin(){
                               return localMessagesFromJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesFromJoin
                               */
                               public void setMessagesFromJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic param){
                            localMessagesFromJoinTracker = param != null;
                                   
                                            this.localMessagesFromJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesToJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic localMessagesToJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesToJoinTracker = false ;

                           public boolean isMessagesToJoinSpecified(){
                               return localMessagesToJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic getMessagesToJoin(){
                               return localMessagesToJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesToJoin
                               */
                               public void setMessagesToJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic param){
                            localMessagesToJoinTracker = param != null;
                                   
                                            this.localMessagesToJoin=param;
                                    

                               }
                            

                        /**
                        * field for OpportunityJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic localOpportunityJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpportunityJoinTracker = false ;

                           public boolean isOpportunityJoinSpecified(){
                               return localOpportunityJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic getOpportunityJoin(){
                               return localOpportunityJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OpportunityJoin
                               */
                               public void setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic param){
                            localOpportunityJoinTracker = param != null;
                                   
                                            this.localOpportunityJoin=param;
                                    

                               }
                            

                        /**
                        * field for ParentCustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localParentCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentCustomerJoinTracker = false ;

                           public boolean isParentCustomerJoinSpecified(){
                               return localParentCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getParentCustomerJoin(){
                               return localParentCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentCustomerJoin
                               */
                               public void setParentCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localParentCustomerJoinTracker = param != null;
                                   
                                            this.localParentCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for ParentJobJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic localParentJobJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentJobJoinTracker = false ;

                           public boolean isParentJobJoinSpecified(){
                               return localParentJobJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic getParentJobJoin(){
                               return localParentJobJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentJobJoin
                               */
                               public void setParentJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic param){
                            localParentJobJoinTracker = param != null;
                                   
                                            this.localParentJobJoin=param;
                                    

                               }
                            

                        /**
                        * field for ParentPartnerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic localParentPartnerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentPartnerJoinTracker = false ;

                           public boolean isParentPartnerJoinSpecified(){
                               return localParentPartnerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic getParentPartnerJoin(){
                               return localParentPartnerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentPartnerJoin
                               */
                               public void setParentPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic param){
                            localParentPartnerJoinTracker = param != null;
                                   
                                            this.localParentPartnerJoin=param;
                                    

                               }
                            

                        /**
                        * field for ParentVendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic localParentVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentVendorJoinTracker = false ;

                           public boolean isParentVendorJoinSpecified(){
                               return localParentVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic getParentVendorJoin(){
                               return localParentVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentVendorJoin
                               */
                               public void setParentVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic param){
                            localParentVendorJoinTracker = param != null;
                                   
                                            this.localParentVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for PartnerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic localPartnerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerJoinTracker = false ;

                           public boolean isPartnerJoinSpecified(){
                               return localPartnerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic getPartnerJoin(){
                               return localPartnerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerJoin
                               */
                               public void setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic param){
                            localPartnerJoinTracker = param != null;
                                   
                                            this.localPartnerJoin=param;
                                    

                               }
                            

                        /**
                        * field for PartnerPrimaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic localPartnerPrimaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerPrimaryJoinTracker = false ;

                           public boolean isPartnerPrimaryJoinSpecified(){
                               return localPartnerPrimaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic getPartnerPrimaryJoin(){
                               return localPartnerPrimaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerPrimaryJoin
                               */
                               public void setPartnerPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic param){
                            localPartnerPrimaryJoinTracker = param != null;
                                   
                                            this.localPartnerPrimaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for PurchasedItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localPurchasedItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchasedItemJoinTracker = false ;

                           public boolean isPurchasedItemJoinSpecified(){
                               return localPurchasedItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getPurchasedItemJoin(){
                               return localPurchasedItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchasedItemJoin
                               */
                               public void setPurchasedItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localPurchasedItemJoinTracker = param != null;
                                   
                                            this.localPurchasedItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic localTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaskJoinTracker = false ;

                           public boolean isTaskJoinSpecified(){
                               return localTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic getTaskJoin(){
                               return localTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaskJoin
                               */
                               public void setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic param){
                            localTaskJoinTracker = param != null;
                                   
                                            this.localTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for TransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic localTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionJoinTracker = false ;

                           public boolean isTransactionJoinSpecified(){
                               return localTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic getTransactionJoin(){
                               return localTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionJoin
                               */
                               public void setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic param){
                            localTransactionJoinTracker = param != null;
                                   
                                            this.localTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for UpsellItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localUpsellItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUpsellItemJoinTracker = false ;

                           public boolean isUpsellItemJoinSpecified(){
                               return localUpsellItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getUpsellItemJoin(){
                               return localUpsellItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UpsellItemJoin
                               */
                               public void setUpsellItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localUpsellItemJoinTracker = param != null;
                                   
                                            this.localUpsellItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserNotesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic localUserNotesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserNotesJoinTracker = false ;

                           public boolean isUserNotesJoinSpecified(){
                               return localUserNotesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic getUserNotesJoin(){
                               return localUserNotesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserNotesJoin
                               */
                               public void setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic param){
                            localUserNotesJoinTracker = param != null;
                                   
                                            this.localUserNotesJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic localVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorJoinTracker = false ;

                           public boolean isVendorJoinSpecified(){
                               return localVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic getVendorJoin(){
                               return localVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorJoin
                               */
                               public void setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic param){
                            localVendorJoinTracker = param != null;
                                   
                                            this.localVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorPrimaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic localVendorPrimaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorPrimaryJoinTracker = false ;

                           public boolean isVendorPrimaryJoinSpecified(){
                               return localVendorPrimaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic getVendorPrimaryJoin(){
                               return localVendorPrimaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorPrimaryJoin
                               */
                               public void setVendorPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic param){
                            localVendorPrimaryJoinTracker = param != null;
                                   
                                            this.localVendorPrimaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:relationships_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ContactSearchRow",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ContactSearchRow",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localCallJoinTracker){
                                            if (localCallJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                            }
                                           localCallJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","callJoin"),
                                               xmlWriter);
                                        } if (localCampaignResponseJoinTracker){
                                            if (localCampaignResponseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("campaignResponseJoin cannot be null!!");
                                            }
                                           localCampaignResponseJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","campaignResponseJoin"),
                                               xmlWriter);
                                        } if (localCaseJoinTracker){
                                            if (localCaseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                            }
                                           localCaseJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","caseJoin"),
                                               xmlWriter);
                                        } if (localCustomerJoinTracker){
                                            if (localCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                            }
                                           localCustomerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customerJoin"),
                                               xmlWriter);
                                        } if (localCustomerPrimaryJoinTracker){
                                            if (localCustomerPrimaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerPrimaryJoin cannot be null!!");
                                            }
                                           localCustomerPrimaryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customerPrimaryJoin"),
                                               xmlWriter);
                                        } if (localEventJoinTracker){
                                            if (localEventJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                            }
                                           localEventJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","eventJoin"),
                                               xmlWriter);
                                        } if (localFileJoinTracker){
                                            if (localFileJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                            }
                                           localFileJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","fileJoin"),
                                               xmlWriter);
                                        } if (localJobJoinTracker){
                                            if (localJobJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                            }
                                           localJobJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","jobJoin"),
                                               xmlWriter);
                                        } if (localJobPrimaryJoinTracker){
                                            if (localJobPrimaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobPrimaryJoin cannot be null!!");
                                            }
                                           localJobPrimaryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","jobPrimaryJoin"),
                                               xmlWriter);
                                        } if (localMessagesJoinTracker){
                                            if (localMessagesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                            }
                                           localMessagesJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesJoin"),
                                               xmlWriter);
                                        } if (localMessagesFromJoinTracker){
                                            if (localMessagesFromJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesFromJoin cannot be null!!");
                                            }
                                           localMessagesFromJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesFromJoin"),
                                               xmlWriter);
                                        } if (localMessagesToJoinTracker){
                                            if (localMessagesToJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesToJoin cannot be null!!");
                                            }
                                           localMessagesToJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesToJoin"),
                                               xmlWriter);
                                        } if (localOpportunityJoinTracker){
                                            if (localOpportunityJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                            }
                                           localOpportunityJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","opportunityJoin"),
                                               xmlWriter);
                                        } if (localParentCustomerJoinTracker){
                                            if (localParentCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentCustomerJoin cannot be null!!");
                                            }
                                           localParentCustomerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentCustomerJoin"),
                                               xmlWriter);
                                        } if (localParentJobJoinTracker){
                                            if (localParentJobJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentJobJoin cannot be null!!");
                                            }
                                           localParentJobJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentJobJoin"),
                                               xmlWriter);
                                        } if (localParentPartnerJoinTracker){
                                            if (localParentPartnerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentPartnerJoin cannot be null!!");
                                            }
                                           localParentPartnerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentPartnerJoin"),
                                               xmlWriter);
                                        } if (localParentVendorJoinTracker){
                                            if (localParentVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentVendorJoin cannot be null!!");
                                            }
                                           localParentVendorJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentVendorJoin"),
                                               xmlWriter);
                                        } if (localPartnerJoinTracker){
                                            if (localPartnerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                            }
                                           localPartnerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerJoin"),
                                               xmlWriter);
                                        } if (localPartnerPrimaryJoinTracker){
                                            if (localPartnerPrimaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerPrimaryJoin cannot be null!!");
                                            }
                                           localPartnerPrimaryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerPrimaryJoin"),
                                               xmlWriter);
                                        } if (localPurchasedItemJoinTracker){
                                            if (localPurchasedItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchasedItemJoin cannot be null!!");
                                            }
                                           localPurchasedItemJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","purchasedItemJoin"),
                                               xmlWriter);
                                        } if (localTaskJoinTracker){
                                            if (localTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                            }
                                           localTaskJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taskJoin"),
                                               xmlWriter);
                                        } if (localTransactionJoinTracker){
                                            if (localTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                            }
                                           localTransactionJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","transactionJoin"),
                                               xmlWriter);
                                        } if (localUpsellItemJoinTracker){
                                            if (localUpsellItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("upsellItemJoin cannot be null!!");
                                            }
                                           localUpsellItemJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","upsellItemJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localUserNotesJoinTracker){
                                            if (localUserNotesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                            }
                                           localUserNotesJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userNotesJoin"),
                                               xmlWriter);
                                        } if (localVendorJoinTracker){
                                            if (localVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                            }
                                           localVendorJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","vendorJoin"),
                                               xmlWriter);
                                        } if (localVendorPrimaryJoinTracker){
                                            if (localVendorPrimaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorPrimaryJoin cannot be null!!");
                                            }
                                           localVendorPrimaryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","vendorPrimaryJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:relationships_2017_2.lists.webservices.netsuite.com")){
                return "ns15";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","ContactSearchRow"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localCallJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "callJoin"));
                            
                            
                                    if (localCallJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                    }
                                    elementList.add(localCallJoin);
                                } if (localCampaignResponseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "campaignResponseJoin"));
                            
                            
                                    if (localCampaignResponseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("campaignResponseJoin cannot be null!!");
                                    }
                                    elementList.add(localCampaignResponseJoin);
                                } if (localCaseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "caseJoin"));
                            
                            
                                    if (localCaseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                    }
                                    elementList.add(localCaseJoin);
                                } if (localCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "customerJoin"));
                            
                            
                                    if (localCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerJoin);
                                } if (localCustomerPrimaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "customerPrimaryJoin"));
                            
                            
                                    if (localCustomerPrimaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerPrimaryJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerPrimaryJoin);
                                } if (localEventJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "eventJoin"));
                            
                            
                                    if (localEventJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                    }
                                    elementList.add(localEventJoin);
                                } if (localFileJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "fileJoin"));
                            
                            
                                    if (localFileJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                    }
                                    elementList.add(localFileJoin);
                                } if (localJobJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "jobJoin"));
                            
                            
                                    if (localJobJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                    }
                                    elementList.add(localJobJoin);
                                } if (localJobPrimaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "jobPrimaryJoin"));
                            
                            
                                    if (localJobPrimaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobPrimaryJoin cannot be null!!");
                                    }
                                    elementList.add(localJobPrimaryJoin);
                                } if (localMessagesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesJoin"));
                            
                            
                                    if (localMessagesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesJoin);
                                } if (localMessagesFromJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesFromJoin"));
                            
                            
                                    if (localMessagesFromJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesFromJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesFromJoin);
                                } if (localMessagesToJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesToJoin"));
                            
                            
                                    if (localMessagesToJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesToJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesToJoin);
                                } if (localOpportunityJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "opportunityJoin"));
                            
                            
                                    if (localOpportunityJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                    }
                                    elementList.add(localOpportunityJoin);
                                } if (localParentCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "parentCustomerJoin"));
                            
                            
                                    if (localParentCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentCustomerJoin cannot be null!!");
                                    }
                                    elementList.add(localParentCustomerJoin);
                                } if (localParentJobJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "parentJobJoin"));
                            
                            
                                    if (localParentJobJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentJobJoin cannot be null!!");
                                    }
                                    elementList.add(localParentJobJoin);
                                } if (localParentPartnerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "parentPartnerJoin"));
                            
                            
                                    if (localParentPartnerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentPartnerJoin cannot be null!!");
                                    }
                                    elementList.add(localParentPartnerJoin);
                                } if (localParentVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "parentVendorJoin"));
                            
                            
                                    if (localParentVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentVendorJoin cannot be null!!");
                                    }
                                    elementList.add(localParentVendorJoin);
                                } if (localPartnerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "partnerJoin"));
                            
                            
                                    if (localPartnerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                    }
                                    elementList.add(localPartnerJoin);
                                } if (localPartnerPrimaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "partnerPrimaryJoin"));
                            
                            
                                    if (localPartnerPrimaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerPrimaryJoin cannot be null!!");
                                    }
                                    elementList.add(localPartnerPrimaryJoin);
                                } if (localPurchasedItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "purchasedItemJoin"));
                            
                            
                                    if (localPurchasedItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchasedItemJoin cannot be null!!");
                                    }
                                    elementList.add(localPurchasedItemJoin);
                                } if (localTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "taskJoin"));
                            
                            
                                    if (localTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                    }
                                    elementList.add(localTaskJoin);
                                } if (localTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "transactionJoin"));
                            
                            
                                    if (localTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                    }
                                    elementList.add(localTransactionJoin);
                                } if (localUpsellItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "upsellItemJoin"));
                            
                            
                                    if (localUpsellItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("upsellItemJoin cannot be null!!");
                                    }
                                    elementList.add(localUpsellItemJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localUserNotesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "userNotesJoin"));
                            
                            
                                    if (localUserNotesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                    }
                                    elementList.add(localUserNotesJoin);
                                } if (localVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "vendorJoin"));
                            
                            
                                    if (localVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorJoin);
                                } if (localVendorPrimaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "vendorPrimaryJoin"));
                            
                            
                                    if (localVendorPrimaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorPrimaryJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorPrimaryJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ContactSearchRow parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ContactSearchRow object =
                new ContactSearchRow();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ContactSearchRow".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ContactSearchRow)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list29 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","callJoin").equals(reader.getName())){
                                
                                                object.setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","campaignResponseJoin").equals(reader.getName())){
                                
                                                object.setCampaignResponseJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","caseJoin").equals(reader.getName())){
                                
                                                object.setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customerJoin").equals(reader.getName())){
                                
                                                object.setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customerPrimaryJoin").equals(reader.getName())){
                                
                                                object.setCustomerPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","eventJoin").equals(reader.getName())){
                                
                                                object.setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","fileJoin").equals(reader.getName())){
                                
                                                object.setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","jobJoin").equals(reader.getName())){
                                
                                                object.setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","jobPrimaryJoin").equals(reader.getName())){
                                
                                                object.setJobPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesJoin").equals(reader.getName())){
                                
                                                object.setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesFromJoin").equals(reader.getName())){
                                
                                                object.setMessagesFromJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesToJoin").equals(reader.getName())){
                                
                                                object.setMessagesToJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","opportunityJoin").equals(reader.getName())){
                                
                                                object.setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentCustomerJoin").equals(reader.getName())){
                                
                                                object.setParentCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentJobJoin").equals(reader.getName())){
                                
                                                object.setParentJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentPartnerJoin").equals(reader.getName())){
                                
                                                object.setParentPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentVendorJoin").equals(reader.getName())){
                                
                                                object.setParentVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerJoin").equals(reader.getName())){
                                
                                                object.setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerPrimaryJoin").equals(reader.getName())){
                                
                                                object.setPartnerPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","purchasedItemJoin").equals(reader.getName())){
                                
                                                object.setPurchasedItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taskJoin").equals(reader.getName())){
                                
                                                object.setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","transactionJoin").equals(reader.getName())){
                                
                                                object.setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","upsellItemJoin").equals(reader.getName())){
                                
                                                object.setUpsellItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userNotesJoin").equals(reader.getName())){
                                
                                                object.setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","vendorJoin").equals(reader.getName())){
                                
                                                object.setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","vendorPrimaryJoin").equals(reader.getName())){
                                
                                                object.setVendorPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list29.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone29 = false;
                                                        while(!loopDone29){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone29 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list29.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone29 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.class,
                                                                list29));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    