
/**
 * Partner.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.relationships_2017_2;
            

            /**
            *  Partner bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Partner extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Partner
                Namespace URI = urn:relationships_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns15
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for EntityId
                        */

                        
                                    protected java.lang.String localEntityId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityIdTracker = false ;

                           public boolean isEntityIdSpecified(){
                               return localEntityIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEntityId(){
                               return localEntityId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityId
                               */
                               public void setEntityId(java.lang.String param){
                            localEntityIdTracker = param != null;
                                   
                                            this.localEntityId=param;
                                    

                               }
                            

                        /**
                        * field for AltName
                        */

                        
                                    protected java.lang.String localAltName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltNameTracker = false ;

                           public boolean isAltNameSpecified(){
                               return localAltNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAltName(){
                               return localAltName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltName
                               */
                               public void setAltName(java.lang.String param){
                            localAltNameTracker = param != null;
                                   
                                            this.localAltName=param;
                                    

                               }
                            

                        /**
                        * field for PartnerCode
                        */

                        
                                    protected java.lang.String localPartnerCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerCodeTracker = false ;

                           public boolean isPartnerCodeSpecified(){
                               return localPartnerCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPartnerCode(){
                               return localPartnerCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerCode
                               */
                               public void setPartnerCode(java.lang.String param){
                            localPartnerCodeTracker = param != null;
                                   
                                            this.localPartnerCode=param;
                                    

                               }
                            

                        /**
                        * field for IsPerson
                        */

                        
                                    protected boolean localIsPerson ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsPersonTracker = false ;

                           public boolean isIsPersonSpecified(){
                               return localIsPersonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsPerson(){
                               return localIsPerson;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsPerson
                               */
                               public void setIsPerson(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsPersonTracker =
                                       true;
                                   
                                            this.localIsPerson=param;
                                    

                               }
                            

                        /**
                        * field for PhoneticName
                        */

                        
                                    protected java.lang.String localPhoneticName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneticNameTracker = false ;

                           public boolean isPhoneticNameSpecified(){
                               return localPhoneticNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPhoneticName(){
                               return localPhoneticName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PhoneticName
                               */
                               public void setPhoneticName(java.lang.String param){
                            localPhoneticNameTracker = param != null;
                                   
                                            this.localPhoneticName=param;
                                    

                               }
                            

                        /**
                        * field for Salutation
                        */

                        
                                    protected java.lang.String localSalutation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalutationTracker = false ;

                           public boolean isSalutationSpecified(){
                               return localSalutationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSalutation(){
                               return localSalutation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Salutation
                               */
                               public void setSalutation(java.lang.String param){
                            localSalutationTracker = param != null;
                                   
                                            this.localSalutation=param;
                                    

                               }
                            

                        /**
                        * field for FirstName
                        */

                        
                                    protected java.lang.String localFirstName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstNameTracker = false ;

                           public boolean isFirstNameSpecified(){
                               return localFirstNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFirstName(){
                               return localFirstName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstName
                               */
                               public void setFirstName(java.lang.String param){
                            localFirstNameTracker = param != null;
                                   
                                            this.localFirstName=param;
                                    

                               }
                            

                        /**
                        * field for MiddleName
                        */

                        
                                    protected java.lang.String localMiddleName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMiddleNameTracker = false ;

                           public boolean isMiddleNameSpecified(){
                               return localMiddleNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMiddleName(){
                               return localMiddleName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MiddleName
                               */
                               public void setMiddleName(java.lang.String param){
                            localMiddleNameTracker = param != null;
                                   
                                            this.localMiddleName=param;
                                    

                               }
                            

                        /**
                        * field for LastName
                        */

                        
                                    protected java.lang.String localLastName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastNameTracker = false ;

                           public boolean isLastNameSpecified(){
                               return localLastNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLastName(){
                               return localLastName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastName
                               */
                               public void setLastName(java.lang.String param){
                            localLastNameTracker = param != null;
                                   
                                            this.localLastName=param;
                                    

                               }
                            

                        /**
                        * field for CompanyName
                        */

                        
                                    protected java.lang.String localCompanyName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyNameTracker = false ;

                           public boolean isCompanyNameSpecified(){
                               return localCompanyNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCompanyName(){
                               return localCompanyName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompanyName
                               */
                               public void setCompanyName(java.lang.String param){
                            localCompanyNameTracker = param != null;
                                   
                                            this.localCompanyName=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected java.lang.String localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(java.lang.String param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected java.lang.String localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(java.lang.String param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected java.lang.String localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(java.lang.String param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for Url
                        */

                        
                                    protected java.lang.String localUrl ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUrlTracker = false ;

                           public boolean isUrlSpecified(){
                               return localUrlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUrl(){
                               return localUrl;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Url
                               */
                               public void setUrl(java.lang.String param){
                            localUrlTracker = param != null;
                                   
                                            this.localUrl=param;
                                    

                               }
                            

                        /**
                        * field for DefaultAddress
                        */

                        
                                    protected java.lang.String localDefaultAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultAddressTracker = false ;

                           public boolean isDefaultAddressSpecified(){
                               return localDefaultAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDefaultAddress(){
                               return localDefaultAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultAddress
                               */
                               public void setDefaultAddress(java.lang.String param){
                            localDefaultAddressTracker = param != null;
                                   
                                            this.localDefaultAddress=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected java.util.Calendar localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(java.util.Calendar param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for GlobalSubscriptionStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus localGlobalSubscriptionStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGlobalSubscriptionStatusTracker = false ;

                           public boolean isGlobalSubscriptionStatusSpecified(){
                               return localGlobalSubscriptionStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus getGlobalSubscriptionStatus(){
                               return localGlobalSubscriptionStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GlobalSubscriptionStatus
                               */
                               public void setGlobalSubscriptionStatus(com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus param){
                            localGlobalSubscriptionStatusTracker = param != null;
                                   
                                            this.localGlobalSubscriptionStatus=param;
                                    

                               }
                            

                        /**
                        * field for ReferringUrl
                        */

                        
                                    protected java.lang.String localReferringUrl ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReferringUrlTracker = false ;

                           public boolean isReferringUrlSpecified(){
                               return localReferringUrlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getReferringUrl(){
                               return localReferringUrl;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReferringUrl
                               */
                               public void setReferringUrl(java.lang.String param){
                            localReferringUrlTracker = param != null;
                                   
                                            this.localReferringUrl=param;
                                    

                               }
                            

                        /**
                        * field for RoleList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRefList localRoleList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRoleListTracker = false ;

                           public boolean isRoleListSpecified(){
                               return localRoleListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRefList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRefList getRoleList(){
                               return localRoleList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RoleList
                               */
                               public void setRoleList(com.netsuite.webservices.platform.core_2017_2.RecordRefList param){
                            localRoleListTracker = param != null;
                                   
                                            this.localRoleList=param;
                                    

                               }
                            

                        /**
                        * field for CategoryList
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.CategoryList localCategoryList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryListTracker = false ;

                           public boolean isCategoryListSpecified(){
                               return localCategoryListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.CategoryList
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.CategoryList getCategoryList(){
                               return localCategoryList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CategoryList
                               */
                               public void setCategoryList(com.netsuite.webservices.lists.relationships_2017_2.CategoryList param){
                            localCategoryListTracker = param != null;
                                   
                                            this.localCategoryList=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for PrintOnCheckAs
                        */

                        
                                    protected java.lang.String localPrintOnCheckAs ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrintOnCheckAsTracker = false ;

                           public boolean isPrintOnCheckAsSpecified(){
                               return localPrintOnCheckAsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPrintOnCheckAs(){
                               return localPrintOnCheckAs;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrintOnCheckAs
                               */
                               public void setPrintOnCheckAs(java.lang.String param){
                            localPrintOnCheckAsTracker = param != null;
                                   
                                            this.localPrintOnCheckAs=param;
                                    

                               }
                            

                        /**
                        * field for TaxIdNum
                        */

                        
                                    protected java.lang.String localTaxIdNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxIdNumTracker = false ;

                           public boolean isTaxIdNumSpecified(){
                               return localTaxIdNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTaxIdNum(){
                               return localTaxIdNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxIdNum
                               */
                               public void setTaxIdNum(java.lang.String param){
                            localTaxIdNumTracker = param != null;
                                   
                                            this.localTaxIdNum=param;
                                    

                               }
                            

                        /**
                        * field for VatRegNumber
                        */

                        
                                    protected java.lang.String localVatRegNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVatRegNumberTracker = false ;

                           public boolean isVatRegNumberSpecified(){
                               return localVatRegNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getVatRegNumber(){
                               return localVatRegNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VatRegNumber
                               */
                               public void setVatRegNumber(java.lang.String param){
                            localVatRegNumberTracker = param != null;
                                   
                                            this.localVatRegNumber=param;
                                    

                               }
                            

                        /**
                        * field for Comments
                        */

                        
                                    protected java.lang.String localComments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommentsTracker = false ;

                           public boolean isCommentsSpecified(){
                               return localCommentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getComments(){
                               return localComments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Comments
                               */
                               public void setComments(java.lang.String param){
                            localCommentsTracker = param != null;
                                   
                                            this.localComments=param;
                                    

                               }
                            

                        /**
                        * field for Bcn
                        */

                        
                                    protected java.lang.String localBcn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBcnTracker = false ;

                           public boolean isBcnSpecified(){
                               return localBcnTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBcn(){
                               return localBcn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bcn
                               */
                               public void setBcn(java.lang.String param){
                            localBcnTracker = param != null;
                                   
                                            this.localBcn=param;
                                    

                               }
                            

                        /**
                        * field for Image
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localImageTracker = false ;

                           public boolean isImageSpecified(){
                               return localImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getImage(){
                               return localImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Image
                               */
                               public void setImage(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localImageTracker = param != null;
                                   
                                            this.localImage=param;
                                    

                               }
                            

                        /**
                        * field for TaxFractionUnit
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.types.TaxFractionUnit localTaxFractionUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxFractionUnitTracker = false ;

                           public boolean isTaxFractionUnitSpecified(){
                               return localTaxFractionUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.types.TaxFractionUnit
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.types.TaxFractionUnit getTaxFractionUnit(){
                               return localTaxFractionUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxFractionUnit
                               */
                               public void setTaxFractionUnit(com.netsuite.webservices.lists.relationships_2017_2.types.TaxFractionUnit param){
                            localTaxFractionUnitTracker = param != null;
                                   
                                            this.localTaxFractionUnit=param;
                                    

                               }
                            

                        /**
                        * field for EmailPreference
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.types.EmailPreference localEmailPreference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailPreferenceTracker = false ;

                           public boolean isEmailPreferenceSpecified(){
                               return localEmailPreferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.types.EmailPreference
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.types.EmailPreference getEmailPreference(){
                               return localEmailPreference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailPreference
                               */
                               public void setEmailPreference(com.netsuite.webservices.lists.relationships_2017_2.types.EmailPreference param){
                            localEmailPreferenceTracker = param != null;
                                   
                                            this.localEmailPreference=param;
                                    

                               }
                            

                        /**
                        * field for TaxRounding
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.types.TaxRounding localTaxRounding ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxRoundingTracker = false ;

                           public boolean isTaxRoundingSpecified(){
                               return localTaxRoundingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.types.TaxRounding
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.types.TaxRounding getTaxRounding(){
                               return localTaxRounding;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxRounding
                               */
                               public void setTaxRounding(com.netsuite.webservices.lists.relationships_2017_2.types.TaxRounding param){
                            localTaxRoundingTracker = param != null;
                                   
                                            this.localTaxRounding=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for HomePhone
                        */

                        
                                    protected java.lang.String localHomePhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHomePhoneTracker = false ;

                           public boolean isHomePhoneSpecified(){
                               return localHomePhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHomePhone(){
                               return localHomePhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HomePhone
                               */
                               public void setHomePhone(java.lang.String param){
                            localHomePhoneTracker = param != null;
                                   
                                            this.localHomePhone=param;
                                    

                               }
                            

                        /**
                        * field for MobilePhone
                        */

                        
                                    protected java.lang.String localMobilePhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMobilePhoneTracker = false ;

                           public boolean isMobilePhoneSpecified(){
                               return localMobilePhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMobilePhone(){
                               return localMobilePhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MobilePhone
                               */
                               public void setMobilePhone(java.lang.String param){
                            localMobilePhoneTracker = param != null;
                                   
                                            this.localMobilePhone=param;
                                    

                               }
                            

                        /**
                        * field for AltEmail
                        */

                        
                                    protected java.lang.String localAltEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltEmailTracker = false ;

                           public boolean isAltEmailSpecified(){
                               return localAltEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAltEmail(){
                               return localAltEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltEmail
                               */
                               public void setAltEmail(java.lang.String param){
                            localAltEmailTracker = param != null;
                                   
                                            this.localAltEmail=param;
                                    

                               }
                            

                        /**
                        * field for GiveAccess
                        */

                        
                                    protected boolean localGiveAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiveAccessTracker = false ;

                           public boolean isGiveAccessSpecified(){
                               return localGiveAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getGiveAccess(){
                               return localGiveAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiveAccess
                               */
                               public void setGiveAccess(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localGiveAccessTracker =
                                       true;
                                   
                                            this.localGiveAccess=param;
                                    

                               }
                            

                        /**
                        * field for AccessRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAccessRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccessRoleTracker = false ;

                           public boolean isAccessRoleSpecified(){
                               return localAccessRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAccessRole(){
                               return localAccessRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccessRole
                               */
                               public void setAccessRole(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAccessRoleTracker = param != null;
                                   
                                            this.localAccessRole=param;
                                    

                               }
                            

                        /**
                        * field for SendEmail
                        */

                        
                                    protected boolean localSendEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendEmailTracker = false ;

                           public boolean isSendEmailSpecified(){
                               return localSendEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendEmail(){
                               return localSendEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendEmail
                               */
                               public void setSendEmail(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendEmailTracker =
                                       true;
                                   
                                            this.localSendEmail=param;
                                    

                               }
                            

                        /**
                        * field for Password
                        */

                        
                                    protected java.lang.String localPassword ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPasswordTracker = false ;

                           public boolean isPasswordSpecified(){
                               return localPasswordTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPassword(){
                               return localPassword;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Password
                               */
                               public void setPassword(java.lang.String param){
                            localPasswordTracker = param != null;
                                   
                                            this.localPassword=param;
                                    

                               }
                            

                        /**
                        * field for Password2
                        */

                        
                                    protected java.lang.String localPassword2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPassword2Tracker = false ;

                           public boolean isPassword2Specified(){
                               return localPassword2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPassword2(){
                               return localPassword2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Password2
                               */
                               public void setPassword2(java.lang.String param){
                            localPassword2Tracker = param != null;
                                   
                                            this.localPassword2=param;
                                    

                               }
                            

                        /**
                        * field for RequirePwdChange
                        */

                        
                                    protected boolean localRequirePwdChange ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRequirePwdChangeTracker = false ;

                           public boolean isRequirePwdChangeSpecified(){
                               return localRequirePwdChangeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRequirePwdChange(){
                               return localRequirePwdChange;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RequirePwdChange
                               */
                               public void setRequirePwdChange(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRequirePwdChangeTracker =
                                       true;
                                   
                                            this.localRequirePwdChange=param;
                                    

                               }
                            

                        /**
                        * field for SubPartnerLogin
                        */

                        
                                    protected boolean localSubPartnerLogin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubPartnerLoginTracker = false ;

                           public boolean isSubPartnerLoginSpecified(){
                               return localSubPartnerLoginTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSubPartnerLogin(){
                               return localSubPartnerLogin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubPartnerLogin
                               */
                               public void setSubPartnerLogin(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSubPartnerLoginTracker =
                                       true;
                                   
                                            this.localSubPartnerLogin=param;
                                    

                               }
                            

                        /**
                        * field for LoginAs
                        */

                        
                                    protected java.lang.String localLoginAs ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLoginAsTracker = false ;

                           public boolean isLoginAsSpecified(){
                               return localLoginAsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLoginAs(){
                               return localLoginAs;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LoginAs
                               */
                               public void setLoginAs(java.lang.String param){
                            localLoginAsTracker = param != null;
                                   
                                            this.localLoginAs=param;
                                    

                               }
                            

                        /**
                        * field for EligibleForCommission
                        */

                        
                                    protected boolean localEligibleForCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEligibleForCommissionTracker = false ;

                           public boolean isEligibleForCommissionSpecified(){
                               return localEligibleForCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEligibleForCommission(){
                               return localEligibleForCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EligibleForCommission
                               */
                               public void setEligibleForCommission(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEligibleForCommissionTracker =
                                       true;
                                   
                                            this.localEligibleForCommission=param;
                                    

                               }
                            

                        /**
                        * field for ContactRolesList
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRolesList localContactRolesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactRolesListTracker = false ;

                           public boolean isContactRolesListSpecified(){
                               return localContactRolesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRolesList
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRolesList getContactRolesList(){
                               return localContactRolesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContactRolesList
                               */
                               public void setContactRolesList(com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRolesList param){
                            localContactRolesListTracker = param != null;
                                   
                                            this.localContactRolesList=param;
                                    

                               }
                            

                        /**
                        * field for PromoCodeList
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCodeList localPromoCodeList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPromoCodeListTracker = false ;

                           public boolean isPromoCodeListSpecified(){
                               return localPromoCodeListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCodeList
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCodeList getPromoCodeList(){
                               return localPromoCodeList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PromoCodeList
                               */
                               public void setPromoCodeList(com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCodeList param){
                            localPromoCodeListTracker = param != null;
                                   
                                            this.localPromoCodeList=param;
                                    

                               }
                            

                        /**
                        * field for AddressbookList
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbookList localAddressbookList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressbookListTracker = false ;

                           public boolean isAddressbookListSpecified(){
                               return localAddressbookListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbookList
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbookList getAddressbookList(){
                               return localAddressbookList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressbookList
                               */
                               public void setAddressbookList(com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbookList param){
                            localAddressbookListTracker = param != null;
                                   
                                            this.localAddressbookList=param;
                                    

                               }
                            

                        /**
                        * field for SubscriptionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.relationships_2017_2.SubscriptionsList localSubscriptionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubscriptionsListTracker = false ;

                           public boolean isSubscriptionsListSpecified(){
                               return localSubscriptionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.relationships_2017_2.SubscriptionsList
                           */
                           public  com.netsuite.webservices.lists.relationships_2017_2.SubscriptionsList getSubscriptionsList(){
                               return localSubscriptionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubscriptionsList
                               */
                               public void setSubscriptionsList(com.netsuite.webservices.lists.relationships_2017_2.SubscriptionsList param){
                            localSubscriptionsListTracker = param != null;
                                   
                                            this.localSubscriptionsList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:relationships_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Partner",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Partner",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localEntityIdTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "entityId", xmlWriter);
                             

                                          if (localEntityId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEntityId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltNameTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altName", xmlWriter);
                             

                                          if (localAltName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("altName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAltName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPartnerCodeTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "partnerCode", xmlWriter);
                             

                                          if (localPartnerCode==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("partnerCode cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPartnerCode);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsPersonTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isPerson", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsPerson));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPhoneticNameTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "phoneticName", xmlWriter);
                             

                                          if (localPhoneticName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPhoneticName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalutationTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "salutation", xmlWriter);
                             

                                          if (localSalutation==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSalutation);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFirstNameTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "firstName", xmlWriter);
                             

                                          if (localFirstName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFirstName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMiddleNameTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "middleName", xmlWriter);
                             

                                          if (localMiddleName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMiddleName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastNameTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastName", xmlWriter);
                             

                                          if (localLastName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLastName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCompanyNameTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "companyName", xmlWriter);
                             

                                          if (localCompanyName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("companyName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCompanyName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localPhoneTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "phone", xmlWriter);
                             

                                          if (localPhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFaxTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fax", xmlWriter);
                             

                                          if (localFax==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFax);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "email", xmlWriter);
                             

                                          if (localEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUrlTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "url", xmlWriter);
                             

                                          if (localUrl==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUrl);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDefaultAddressTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "defaultAddress", xmlWriter);
                             

                                          if (localDefaultAddress==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("defaultAddress cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDefaultAddress);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsInactiveTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDateCreatedTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dateCreated", xmlWriter);
                             

                                          if (localDateCreated==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateCreated));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGlobalSubscriptionStatusTracker){
                                            if (localGlobalSubscriptionStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                            }
                                           localGlobalSubscriptionStatus.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","globalSubscriptionStatus"),
                                               xmlWriter);
                                        } if (localReferringUrlTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "referringUrl", xmlWriter);
                             

                                          if (localReferringUrl==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("referringUrl cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localReferringUrl);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRoleListTracker){
                                            if (localRoleList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("roleList cannot be null!!");
                                            }
                                           localRoleList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","roleList"),
                                               xmlWriter);
                                        } if (localCategoryListTracker){
                                            if (localCategoryList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("categoryList cannot be null!!");
                                            }
                                           localCategoryList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","categoryList"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPrintOnCheckAsTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "printOnCheckAs", xmlWriter);
                             

                                          if (localPrintOnCheckAs==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("printOnCheckAs cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPrintOnCheckAs);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTaxIdNumTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "taxIdNum", xmlWriter);
                             

                                          if (localTaxIdNum==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("taxIdNum cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTaxIdNum);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localVatRegNumberTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "vatRegNumber", xmlWriter);
                             

                                          if (localVatRegNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("vatRegNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localVatRegNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCommentsTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "comments", xmlWriter);
                             

                                          if (localComments==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localComments);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBcnTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "bcn", xmlWriter);
                             

                                          if (localBcn==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("bcn cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBcn);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localImageTracker){
                                            if (localImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                            }
                                           localImage.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","image"),
                                               xmlWriter);
                                        } if (localTaxFractionUnitTracker){
                                            if (localTaxFractionUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxFractionUnit cannot be null!!");
                                            }
                                           localTaxFractionUnit.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taxFractionUnit"),
                                               xmlWriter);
                                        } if (localEmailPreferenceTracker){
                                            if (localEmailPreference==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                            }
                                           localEmailPreference.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","emailPreference"),
                                               xmlWriter);
                                        } if (localTaxRoundingTracker){
                                            if (localTaxRounding==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxRounding cannot be null!!");
                                            }
                                           localTaxRounding.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taxRounding"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localHomePhoneTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "homePhone", xmlWriter);
                             

                                          if (localHomePhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("homePhone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHomePhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMobilePhoneTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "mobilePhone", xmlWriter);
                             

                                          if (localMobilePhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("mobilePhone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMobilePhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltEmailTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altEmail", xmlWriter);
                             

                                          if (localAltEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("altEmail cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAltEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGiveAccessTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giveAccess", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiveAccess));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAccessRoleTracker){
                                            if (localAccessRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accessRole cannot be null!!");
                                            }
                                           localAccessRole.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","accessRole"),
                                               xmlWriter);
                                        } if (localSendEmailTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendEmail", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendEmail cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendEmail));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPasswordTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "password", xmlWriter);
                             

                                          if (localPassword==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("password cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPassword);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPassword2Tracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "password2", xmlWriter);
                             

                                          if (localPassword2==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("password2 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPassword2);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRequirePwdChangeTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "requirePwdChange", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("requirePwdChange cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRequirePwdChange));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubPartnerLoginTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "subPartnerLogin", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("subPartnerLogin cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubPartnerLogin));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLoginAsTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "loginAs", xmlWriter);
                             

                                          if (localLoginAs==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("loginAs cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLoginAs);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEligibleForCommissionTracker){
                                    namespace = "urn:relationships_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "eligibleForCommission", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEligibleForCommission));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localContactRolesListTracker){
                                            if (localContactRolesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contactRolesList cannot be null!!");
                                            }
                                           localContactRolesList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","contactRolesList"),
                                               xmlWriter);
                                        } if (localPromoCodeListTracker){
                                            if (localPromoCodeList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("promoCodeList cannot be null!!");
                                            }
                                           localPromoCodeList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","promoCodeList"),
                                               xmlWriter);
                                        } if (localAddressbookListTracker){
                                            if (localAddressbookList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressbookList cannot be null!!");
                                            }
                                           localAddressbookList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","addressbookList"),
                                               xmlWriter);
                                        } if (localSubscriptionsListTracker){
                                            if (localSubscriptionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subscriptionsList cannot be null!!");
                                            }
                                           localSubscriptionsList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subscriptionsList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:relationships_2017_2.lists.webservices.netsuite.com")){
                return "ns15";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","Partner"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localEntityIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "entityId"));
                                 
                                        if (localEntityId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEntityId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                        }
                                    } if (localAltNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "altName"));
                                 
                                        if (localAltName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("altName cannot be null!!");
                                        }
                                    } if (localPartnerCodeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "partnerCode"));
                                 
                                        if (localPartnerCode != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPartnerCode));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("partnerCode cannot be null!!");
                                        }
                                    } if (localIsPersonTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "isPerson"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsPerson));
                            } if (localPhoneticNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "phoneticName"));
                                 
                                        if (localPhoneticName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhoneticName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                        }
                                    } if (localSalutationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "salutation"));
                                 
                                        if (localSalutation != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalutation));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                        }
                                    } if (localFirstNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "firstName"));
                                 
                                        if (localFirstName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirstName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                        }
                                    } if (localMiddleNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "middleName"));
                                 
                                        if (localMiddleName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMiddleName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                        }
                                    } if (localLastNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "lastName"));
                                 
                                        if (localLastName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                        }
                                    } if (localCompanyNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "companyName"));
                                 
                                        if (localCompanyName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCompanyName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("companyName cannot be null!!");
                                        }
                                    } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localPhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "phone"));
                                 
                                        if (localPhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                        }
                                    } if (localFaxTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "fax"));
                                 
                                        if (localFax != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFax));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                        }
                                    } if (localEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "email"));
                                 
                                        if (localEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                        }
                                    } if (localUrlTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "url"));
                                 
                                        if (localUrl != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUrl));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                                        }
                                    } if (localDefaultAddressTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "defaultAddress"));
                                 
                                        if (localDefaultAddress != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultAddress));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("defaultAddress cannot be null!!");
                                        }
                                    } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localDateCreatedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "dateCreated"));
                                 
                                        if (localDateCreated != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateCreated));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                        }
                                    } if (localGlobalSubscriptionStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "globalSubscriptionStatus"));
                            
                            
                                    if (localGlobalSubscriptionStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                    }
                                    elementList.add(localGlobalSubscriptionStatus);
                                } if (localReferringUrlTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "referringUrl"));
                                 
                                        if (localReferringUrl != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReferringUrl));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("referringUrl cannot be null!!");
                                        }
                                    } if (localRoleListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "roleList"));
                            
                            
                                    if (localRoleList==null){
                                         throw new org.apache.axis2.databinding.ADBException("roleList cannot be null!!");
                                    }
                                    elementList.add(localRoleList);
                                } if (localCategoryListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "categoryList"));
                            
                            
                                    if (localCategoryList==null){
                                         throw new org.apache.axis2.databinding.ADBException("categoryList cannot be null!!");
                                    }
                                    elementList.add(localCategoryList);
                                } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localPrintOnCheckAsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "printOnCheckAs"));
                                 
                                        if (localPrintOnCheckAs != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPrintOnCheckAs));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("printOnCheckAs cannot be null!!");
                                        }
                                    } if (localTaxIdNumTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "taxIdNum"));
                                 
                                        if (localTaxIdNum != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaxIdNum));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("taxIdNum cannot be null!!");
                                        }
                                    } if (localVatRegNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "vatRegNumber"));
                                 
                                        if (localVatRegNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVatRegNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("vatRegNumber cannot be null!!");
                                        }
                                    } if (localCommentsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "comments"));
                                 
                                        if (localComments != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComments));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                        }
                                    } if (localBcnTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "bcn"));
                                 
                                        if (localBcn != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBcn));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("bcn cannot be null!!");
                                        }
                                    } if (localImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "image"));
                            
                            
                                    if (localImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                    }
                                    elementList.add(localImage);
                                } if (localTaxFractionUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "taxFractionUnit"));
                            
                            
                                    if (localTaxFractionUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxFractionUnit cannot be null!!");
                                    }
                                    elementList.add(localTaxFractionUnit);
                                } if (localEmailPreferenceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "emailPreference"));
                            
                            
                                    if (localEmailPreference==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                    }
                                    elementList.add(localEmailPreference);
                                } if (localTaxRoundingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "taxRounding"));
                            
                            
                                    if (localTaxRounding==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxRounding cannot be null!!");
                                    }
                                    elementList.add(localTaxRounding);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localHomePhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "homePhone"));
                                 
                                        if (localHomePhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHomePhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("homePhone cannot be null!!");
                                        }
                                    } if (localMobilePhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "mobilePhone"));
                                 
                                        if (localMobilePhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMobilePhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("mobilePhone cannot be null!!");
                                        }
                                    } if (localAltEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "altEmail"));
                                 
                                        if (localAltEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("altEmail cannot be null!!");
                                        }
                                    } if (localGiveAccessTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "giveAccess"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiveAccess));
                            } if (localAccessRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "accessRole"));
                            
                            
                                    if (localAccessRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("accessRole cannot be null!!");
                                    }
                                    elementList.add(localAccessRole);
                                } if (localSendEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "sendEmail"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendEmail));
                            } if (localPasswordTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "password"));
                                 
                                        if (localPassword != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPassword));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("password cannot be null!!");
                                        }
                                    } if (localPassword2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "password2"));
                                 
                                        if (localPassword2 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPassword2));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("password2 cannot be null!!");
                                        }
                                    } if (localRequirePwdChangeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "requirePwdChange"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRequirePwdChange));
                            } if (localSubPartnerLoginTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "subPartnerLogin"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubPartnerLogin));
                            } if (localLoginAsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "loginAs"));
                                 
                                        if (localLoginAs != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLoginAs));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("loginAs cannot be null!!");
                                        }
                                    } if (localEligibleForCommissionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "eligibleForCommission"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEligibleForCommission));
                            } if (localContactRolesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "contactRolesList"));
                            
                            
                                    if (localContactRolesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("contactRolesList cannot be null!!");
                                    }
                                    elementList.add(localContactRolesList);
                                } if (localPromoCodeListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "promoCodeList"));
                            
                            
                                    if (localPromoCodeList==null){
                                         throw new org.apache.axis2.databinding.ADBException("promoCodeList cannot be null!!");
                                    }
                                    elementList.add(localPromoCodeList);
                                } if (localAddressbookListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "addressbookList"));
                            
                            
                                    if (localAddressbookList==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressbookList cannot be null!!");
                                    }
                                    elementList.add(localAddressbookList);
                                } if (localSubscriptionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "subscriptionsList"));
                            
                            
                                    if (localSubscriptionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subscriptionsList cannot be null!!");
                                    }
                                    elementList.add(localSubscriptionsList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Partner parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Partner object =
                new Partner();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Partner".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Partner)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","entityId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"entityId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEntityId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","altName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerCode").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"partnerCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPartnerCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","isPerson").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isPerson" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsPerson(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","phoneticName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"phoneticName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPhoneticName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","salutation").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"salutation" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSalutation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","firstName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"firstName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFirstName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","middleName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"middleName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMiddleName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","lastName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","companyName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"companyName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCompanyName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"phone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fax" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFax(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"email" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","url").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"url" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUrl(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","defaultAddress").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"defaultAddress" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDefaultAddress(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dateCreated" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDateCreated(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","globalSubscriptionStatus").equals(reader.getName())){
                                
                                                object.setGlobalSubscriptionStatus(com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","referringUrl").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"referringUrl" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReferringUrl(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","roleList").equals(reader.getName())){
                                
                                                object.setRoleList(com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","categoryList").equals(reader.getName())){
                                
                                                object.setCategoryList(com.netsuite.webservices.lists.relationships_2017_2.CategoryList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"title" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","printOnCheckAs").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"printOnCheckAs" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPrintOnCheckAs(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taxIdNum").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"taxIdNum" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTaxIdNum(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","vatRegNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"vatRegNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setVatRegNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","comments").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"comments" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setComments(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","bcn").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"bcn" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBcn(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","image").equals(reader.getName())){
                                
                                                object.setImage(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taxFractionUnit").equals(reader.getName())){
                                
                                                object.setTaxFractionUnit(com.netsuite.webservices.lists.relationships_2017_2.types.TaxFractionUnit.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","emailPreference").equals(reader.getName())){
                                
                                                object.setEmailPreference(com.netsuite.webservices.lists.relationships_2017_2.types.EmailPreference.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taxRounding").equals(reader.getName())){
                                
                                                object.setTaxRounding(com.netsuite.webservices.lists.relationships_2017_2.types.TaxRounding.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","homePhone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"homePhone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHomePhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","mobilePhone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"mobilePhone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMobilePhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","altEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","giveAccess").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giveAccess" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiveAccess(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","accessRole").equals(reader.getName())){
                                
                                                object.setAccessRole(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","sendEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","password").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"password" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPassword(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","password2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"password2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPassword2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","requirePwdChange").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"requirePwdChange" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRequirePwdChange(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subPartnerLogin").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"subPartnerLogin" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSubPartnerLogin(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","loginAs").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"loginAs" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLoginAs(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","eligibleForCommission").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"eligibleForCommission" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEligibleForCommission(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","contactRolesList").equals(reader.getName())){
                                
                                                object.setContactRolesList(com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRolesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","promoCodeList").equals(reader.getName())){
                                
                                                object.setPromoCodeList(com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCodeList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","addressbookList").equals(reader.getName())){
                                
                                                object.setAddressbookList(com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbookList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subscriptionsList").equals(reader.getName())){
                                
                                                object.setSubscriptionsList(com.netsuite.webservices.lists.relationships_2017_2.SubscriptionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    