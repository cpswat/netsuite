
/**
 * CustomerSearchRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.relationships_2017_2;
            

            /**
            *  CustomerSearchRow bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CustomerSearchRow extends com.netsuite.webservices.platform.core_2017_2.SearchRow
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = CustomerSearchRow
                Namespace URI = urn:relationships_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns15
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for BillingAccountJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchRowBasic localBillingAccountJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingAccountJoinTracker = false ;

                           public boolean isBillingAccountJoinSpecified(){
                               return localBillingAccountJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchRowBasic getBillingAccountJoin(){
                               return localBillingAccountJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingAccountJoin
                               */
                               public void setBillingAccountJoin(com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchRowBasic param){
                            localBillingAccountJoinTracker = param != null;
                                   
                                            this.localBillingAccountJoin=param;
                                    

                               }
                            

                        /**
                        * field for BillingScheduleJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchRowBasic localBillingScheduleJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingScheduleJoinTracker = false ;

                           public boolean isBillingScheduleJoinSpecified(){
                               return localBillingScheduleJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchRowBasic getBillingScheduleJoin(){
                               return localBillingScheduleJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingScheduleJoin
                               */
                               public void setBillingScheduleJoin(com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchRowBasic param){
                            localBillingScheduleJoinTracker = param != null;
                                   
                                            this.localBillingScheduleJoin=param;
                                    

                               }
                            

                        /**
                        * field for CallJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic localCallJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCallJoinTracker = false ;

                           public boolean isCallJoinSpecified(){
                               return localCallJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic getCallJoin(){
                               return localCallJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CallJoin
                               */
                               public void setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic param){
                            localCallJoinTracker = param != null;
                                   
                                            this.localCallJoin=param;
                                    

                               }
                            

                        /**
                        * field for CampaignResponseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic localCampaignResponseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignResponseJoinTracker = false ;

                           public boolean isCampaignResponseJoinSpecified(){
                               return localCampaignResponseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic getCampaignResponseJoin(){
                               return localCampaignResponseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignResponseJoin
                               */
                               public void setCampaignResponseJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic param){
                            localCampaignResponseJoinTracker = param != null;
                                   
                                            this.localCampaignResponseJoin=param;
                                    

                               }
                            

                        /**
                        * field for CaseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic localCaseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseJoinTracker = false ;

                           public boolean isCaseJoinSpecified(){
                               return localCaseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic getCaseJoin(){
                               return localCaseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseJoin
                               */
                               public void setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic param){
                            localCaseJoinTracker = param != null;
                                   
                                            this.localCaseJoin=param;
                                    

                               }
                            

                        /**
                        * field for ContactJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic localContactJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactJoinTracker = false ;

                           public boolean isContactJoinSpecified(){
                               return localContactJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic getContactJoin(){
                               return localContactJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContactJoin
                               */
                               public void setContactJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic param){
                            localContactJoinTracker = param != null;
                                   
                                            this.localContactJoin=param;
                                    

                               }
                            

                        /**
                        * field for ContactPrimaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic localContactPrimaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactPrimaryJoinTracker = false ;

                           public boolean isContactPrimaryJoinSpecified(){
                               return localContactPrimaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic getContactPrimaryJoin(){
                               return localContactPrimaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContactPrimaryJoin
                               */
                               public void setContactPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic param){
                            localContactPrimaryJoinTracker = param != null;
                                   
                                            this.localContactPrimaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for EventJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic localEventJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventJoinTracker = false ;

                           public boolean isEventJoinSpecified(){
                               return localEventJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic getEventJoin(){
                               return localEventJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventJoin
                               */
                               public void setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic param){
                            localEventJoinTracker = param != null;
                                   
                                            this.localEventJoin=param;
                                    

                               }
                            

                        /**
                        * field for FileJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic localFileJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFileJoinTracker = false ;

                           public boolean isFileJoinSpecified(){
                               return localFileJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic getFileJoin(){
                               return localFileJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FileJoin
                               */
                               public void setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic param){
                            localFileJoinTracker = param != null;
                                   
                                            this.localFileJoin=param;
                                    

                               }
                            

                        /**
                        * field for HostedPageJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic localHostedPageJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHostedPageJoinTracker = false ;

                           public boolean isHostedPageJoinSpecified(){
                               return localHostedPageJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic getHostedPageJoin(){
                               return localHostedPageJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HostedPageJoin
                               */
                               public void setHostedPageJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic param){
                            localHostedPageJoinTracker = param != null;
                                   
                                            this.localHostedPageJoin=param;
                                    

                               }
                            

                        /**
                        * field for JobJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic localJobJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobJoinTracker = false ;

                           public boolean isJobJoinSpecified(){
                               return localJobJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic getJobJoin(){
                               return localJobJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobJoin
                               */
                               public void setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic param){
                            localJobJoinTracker = param != null;
                                   
                                            this.localJobJoin=param;
                                    

                               }
                            

                        /**
                        * field for LeadSourceJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic localLeadSourceJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceJoinTracker = false ;

                           public boolean isLeadSourceJoinSpecified(){
                               return localLeadSourceJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic getLeadSourceJoin(){
                               return localLeadSourceJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadSourceJoin
                               */
                               public void setLeadSourceJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic param){
                            localLeadSourceJoinTracker = param != null;
                                   
                                            this.localLeadSourceJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic localMessagesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesJoinTracker = false ;

                           public boolean isMessagesJoinSpecified(){
                               return localMessagesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic getMessagesJoin(){
                               return localMessagesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesJoin
                               */
                               public void setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic param){
                            localMessagesJoinTracker = param != null;
                                   
                                            this.localMessagesJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesFromJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic localMessagesFromJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesFromJoinTracker = false ;

                           public boolean isMessagesFromJoinSpecified(){
                               return localMessagesFromJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic getMessagesFromJoin(){
                               return localMessagesFromJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesFromJoin
                               */
                               public void setMessagesFromJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic param){
                            localMessagesFromJoinTracker = param != null;
                                   
                                            this.localMessagesFromJoin=param;
                                    

                               }
                            

                        /**
                        * field for MessagesToJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic localMessagesToJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessagesToJoinTracker = false ;

                           public boolean isMessagesToJoinSpecified(){
                               return localMessagesToJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic getMessagesToJoin(){
                               return localMessagesToJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessagesToJoin
                               */
                               public void setMessagesToJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic param){
                            localMessagesToJoinTracker = param != null;
                                   
                                            this.localMessagesToJoin=param;
                                    

                               }
                            

                        /**
                        * field for MseSubsidiaryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchRowBasic localMseSubsidiaryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMseSubsidiaryJoinTracker = false ;

                           public boolean isMseSubsidiaryJoinSpecified(){
                               return localMseSubsidiaryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchRowBasic getMseSubsidiaryJoin(){
                               return localMseSubsidiaryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MseSubsidiaryJoin
                               */
                               public void setMseSubsidiaryJoin(com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchRowBasic param){
                            localMseSubsidiaryJoinTracker = param != null;
                                   
                                            this.localMseSubsidiaryJoin=param;
                                    

                               }
                            

                        /**
                        * field for OpportunityJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic localOpportunityJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpportunityJoinTracker = false ;

                           public boolean isOpportunityJoinSpecified(){
                               return localOpportunityJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic getOpportunityJoin(){
                               return localOpportunityJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OpportunityJoin
                               */
                               public void setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic param){
                            localOpportunityJoinTracker = param != null;
                                   
                                            this.localOpportunityJoin=param;
                                    

                               }
                            

                        /**
                        * field for OriginatingLeadJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic localOriginatingLeadJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOriginatingLeadJoinTracker = false ;

                           public boolean isOriginatingLeadJoinSpecified(){
                               return localOriginatingLeadJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic getOriginatingLeadJoin(){
                               return localOriginatingLeadJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OriginatingLeadJoin
                               */
                               public void setOriginatingLeadJoin(com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic param){
                            localOriginatingLeadJoinTracker = param != null;
                                   
                                            this.localOriginatingLeadJoin=param;
                                    

                               }
                            

                        /**
                        * field for ParentCustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localParentCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentCustomerJoinTracker = false ;

                           public boolean isParentCustomerJoinSpecified(){
                               return localParentCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getParentCustomerJoin(){
                               return localParentCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentCustomerJoin
                               */
                               public void setParentCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localParentCustomerJoinTracker = param != null;
                                   
                                            this.localParentCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for PartnerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic localPartnerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerJoinTracker = false ;

                           public boolean isPartnerJoinSpecified(){
                               return localPartnerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic getPartnerJoin(){
                               return localPartnerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerJoin
                               */
                               public void setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic param){
                            localPartnerJoinTracker = param != null;
                                   
                                            this.localPartnerJoin=param;
                                    

                               }
                            

                        /**
                        * field for PricingJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic localPricingJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingJoinTracker = false ;

                           public boolean isPricingJoinSpecified(){
                               return localPricingJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic getPricingJoin(){
                               return localPricingJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingJoin
                               */
                               public void setPricingJoin(com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic param){
                            localPricingJoinTracker = param != null;
                                   
                                            this.localPricingJoin=param;
                                    

                               }
                            

                        /**
                        * field for PurchasedItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localPurchasedItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchasedItemJoinTracker = false ;

                           public boolean isPurchasedItemJoinSpecified(){
                               return localPurchasedItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getPurchasedItemJoin(){
                               return localPurchasedItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchasedItemJoin
                               */
                               public void setPurchasedItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localPurchasedItemJoinTracker = param != null;
                                   
                                            this.localPurchasedItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for ResourceAllocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic localResourceAllocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResourceAllocationJoinTracker = false ;

                           public boolean isResourceAllocationJoinSpecified(){
                               return localResourceAllocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic getResourceAllocationJoin(){
                               return localResourceAllocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResourceAllocationJoin
                               */
                               public void setResourceAllocationJoin(com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic param){
                            localResourceAllocationJoinTracker = param != null;
                                   
                                            this.localResourceAllocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for SalesRepJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localSalesRepJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepJoinTracker = false ;

                           public boolean isSalesRepJoinSpecified(){
                               return localSalesRepJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getSalesRepJoin(){
                               return localSalesRepJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRepJoin
                               */
                               public void setSalesRepJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localSalesRepJoinTracker = param != null;
                                   
                                            this.localSalesRepJoin=param;
                                    

                               }
                            

                        /**
                        * field for SubCustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localSubCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubCustomerJoinTracker = false ;

                           public boolean isSubCustomerJoinSpecified(){
                               return localSubCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getSubCustomerJoin(){
                               return localSubCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubCustomerJoin
                               */
                               public void setSubCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localSubCustomerJoinTracker = param != null;
                                   
                                            this.localSubCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic localTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaskJoinTracker = false ;

                           public boolean isTaskJoinSpecified(){
                               return localTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic getTaskJoin(){
                               return localTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaskJoin
                               */
                               public void setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic param){
                            localTaskJoinTracker = param != null;
                                   
                                            this.localTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for TimeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic localTimeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeJoinTracker = false ;

                           public boolean isTimeJoinSpecified(){
                               return localTimeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic getTimeJoin(){
                               return localTimeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeJoin
                               */
                               public void setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic param){
                            localTimeJoinTracker = param != null;
                                   
                                            this.localTimeJoin=param;
                                    

                               }
                            

                        /**
                        * field for TopLevelParentJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localTopLevelParentJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTopLevelParentJoinTracker = false ;

                           public boolean isTopLevelParentJoinSpecified(){
                               return localTopLevelParentJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getTopLevelParentJoin(){
                               return localTopLevelParentJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TopLevelParentJoin
                               */
                               public void setTopLevelParentJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localTopLevelParentJoinTracker = param != null;
                                   
                                            this.localTopLevelParentJoin=param;
                                    

                               }
                            

                        /**
                        * field for TransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic localTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionJoinTracker = false ;

                           public boolean isTransactionJoinSpecified(){
                               return localTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic getTransactionJoin(){
                               return localTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionJoin
                               */
                               public void setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic param){
                            localTransactionJoinTracker = param != null;
                                   
                                            this.localTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for UpsellItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localUpsellItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUpsellItemJoinTracker = false ;

                           public boolean isUpsellItemJoinSpecified(){
                               return localUpsellItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getUpsellItemJoin(){
                               return localUpsellItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UpsellItemJoin
                               */
                               public void setUpsellItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localUpsellItemJoinTracker = param != null;
                                   
                                            this.localUpsellItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserNotesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic localUserNotesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserNotesJoinTracker = false ;

                           public boolean isUserNotesJoinSpecified(){
                               return localUserNotesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic getUserNotesJoin(){
                               return localUserNotesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserNotesJoin
                               */
                               public void setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic param){
                            localUserNotesJoinTracker = param != null;
                                   
                                            this.localUserNotesJoin=param;
                                    

                               }
                            

                        /**
                        * field for WebSiteCategoryJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchRowBasic localWebSiteCategoryJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWebSiteCategoryJoinTracker = false ;

                           public boolean isWebSiteCategoryJoinSpecified(){
                               return localWebSiteCategoryJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchRowBasic getWebSiteCategoryJoin(){
                               return localWebSiteCategoryJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WebSiteCategoryJoin
                               */
                               public void setWebSiteCategoryJoin(com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchRowBasic param){
                            localWebSiteCategoryJoinTracker = param != null;
                                   
                                            this.localWebSiteCategoryJoin=param;
                                    

                               }
                            

                        /**
                        * field for WebSiteItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localWebSiteItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWebSiteItemJoinTracker = false ;

                           public boolean isWebSiteItemJoinSpecified(){
                               return localWebSiteItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getWebSiteItemJoin(){
                               return localWebSiteItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WebSiteItemJoin
                               */
                               public void setWebSiteItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localWebSiteItemJoinTracker = param != null;
                                   
                                            this.localWebSiteItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:relationships_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":CustomerSearchRow",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "CustomerSearchRow",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localBillingAccountJoinTracker){
                                            if (localBillingAccountJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingAccountJoin cannot be null!!");
                                            }
                                           localBillingAccountJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","billingAccountJoin"),
                                               xmlWriter);
                                        } if (localBillingScheduleJoinTracker){
                                            if (localBillingScheduleJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingScheduleJoin cannot be null!!");
                                            }
                                           localBillingScheduleJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","billingScheduleJoin"),
                                               xmlWriter);
                                        } if (localCallJoinTracker){
                                            if (localCallJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                            }
                                           localCallJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","callJoin"),
                                               xmlWriter);
                                        } if (localCampaignResponseJoinTracker){
                                            if (localCampaignResponseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("campaignResponseJoin cannot be null!!");
                                            }
                                           localCampaignResponseJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","campaignResponseJoin"),
                                               xmlWriter);
                                        } if (localCaseJoinTracker){
                                            if (localCaseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                            }
                                           localCaseJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","caseJoin"),
                                               xmlWriter);
                                        } if (localContactJoinTracker){
                                            if (localContactJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contactJoin cannot be null!!");
                                            }
                                           localContactJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","contactJoin"),
                                               xmlWriter);
                                        } if (localContactPrimaryJoinTracker){
                                            if (localContactPrimaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contactPrimaryJoin cannot be null!!");
                                            }
                                           localContactPrimaryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","contactPrimaryJoin"),
                                               xmlWriter);
                                        } if (localEventJoinTracker){
                                            if (localEventJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                            }
                                           localEventJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","eventJoin"),
                                               xmlWriter);
                                        } if (localFileJoinTracker){
                                            if (localFileJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                            }
                                           localFileJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","fileJoin"),
                                               xmlWriter);
                                        } if (localHostedPageJoinTracker){
                                            if (localHostedPageJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hostedPageJoin cannot be null!!");
                                            }
                                           localHostedPageJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","hostedPageJoin"),
                                               xmlWriter);
                                        } if (localJobJoinTracker){
                                            if (localJobJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                            }
                                           localJobJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","jobJoin"),
                                               xmlWriter);
                                        } if (localLeadSourceJoinTracker){
                                            if (localLeadSourceJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadSourceJoin cannot be null!!");
                                            }
                                           localLeadSourceJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","leadSourceJoin"),
                                               xmlWriter);
                                        } if (localMessagesJoinTracker){
                                            if (localMessagesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                            }
                                           localMessagesJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesJoin"),
                                               xmlWriter);
                                        } if (localMessagesFromJoinTracker){
                                            if (localMessagesFromJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesFromJoin cannot be null!!");
                                            }
                                           localMessagesFromJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesFromJoin"),
                                               xmlWriter);
                                        } if (localMessagesToJoinTracker){
                                            if (localMessagesToJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("messagesToJoin cannot be null!!");
                                            }
                                           localMessagesToJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesToJoin"),
                                               xmlWriter);
                                        } if (localMseSubsidiaryJoinTracker){
                                            if (localMseSubsidiaryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("mseSubsidiaryJoin cannot be null!!");
                                            }
                                           localMseSubsidiaryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","mseSubsidiaryJoin"),
                                               xmlWriter);
                                        } if (localOpportunityJoinTracker){
                                            if (localOpportunityJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                            }
                                           localOpportunityJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","opportunityJoin"),
                                               xmlWriter);
                                        } if (localOriginatingLeadJoinTracker){
                                            if (localOriginatingLeadJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("originatingLeadJoin cannot be null!!");
                                            }
                                           localOriginatingLeadJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","originatingLeadJoin"),
                                               xmlWriter);
                                        } if (localParentCustomerJoinTracker){
                                            if (localParentCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentCustomerJoin cannot be null!!");
                                            }
                                           localParentCustomerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentCustomerJoin"),
                                               xmlWriter);
                                        } if (localPartnerJoinTracker){
                                            if (localPartnerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                            }
                                           localPartnerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerJoin"),
                                               xmlWriter);
                                        } if (localPricingJoinTracker){
                                            if (localPricingJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingJoin cannot be null!!");
                                            }
                                           localPricingJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","pricingJoin"),
                                               xmlWriter);
                                        } if (localPurchasedItemJoinTracker){
                                            if (localPurchasedItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchasedItemJoin cannot be null!!");
                                            }
                                           localPurchasedItemJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","purchasedItemJoin"),
                                               xmlWriter);
                                        } if (localResourceAllocationJoinTracker){
                                            if (localResourceAllocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resourceAllocationJoin cannot be null!!");
                                            }
                                           localResourceAllocationJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","resourceAllocationJoin"),
                                               xmlWriter);
                                        } if (localSalesRepJoinTracker){
                                            if (localSalesRepJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRepJoin cannot be null!!");
                                            }
                                           localSalesRepJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","salesRepJoin"),
                                               xmlWriter);
                                        } if (localSubCustomerJoinTracker){
                                            if (localSubCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subCustomerJoin cannot be null!!");
                                            }
                                           localSubCustomerJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subCustomerJoin"),
                                               xmlWriter);
                                        } if (localTaskJoinTracker){
                                            if (localTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                            }
                                           localTaskJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taskJoin"),
                                               xmlWriter);
                                        } if (localTimeJoinTracker){
                                            if (localTimeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                            }
                                           localTimeJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","timeJoin"),
                                               xmlWriter);
                                        } if (localTopLevelParentJoinTracker){
                                            if (localTopLevelParentJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("topLevelParentJoin cannot be null!!");
                                            }
                                           localTopLevelParentJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","topLevelParentJoin"),
                                               xmlWriter);
                                        } if (localTransactionJoinTracker){
                                            if (localTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                            }
                                           localTransactionJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","transactionJoin"),
                                               xmlWriter);
                                        } if (localUpsellItemJoinTracker){
                                            if (localUpsellItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("upsellItemJoin cannot be null!!");
                                            }
                                           localUpsellItemJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","upsellItemJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localUserNotesJoinTracker){
                                            if (localUserNotesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                            }
                                           localUserNotesJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userNotesJoin"),
                                               xmlWriter);
                                        } if (localWebSiteCategoryJoinTracker){
                                            if (localWebSiteCategoryJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("webSiteCategoryJoin cannot be null!!");
                                            }
                                           localWebSiteCategoryJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","webSiteCategoryJoin"),
                                               xmlWriter);
                                        } if (localWebSiteItemJoinTracker){
                                            if (localWebSiteItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("webSiteItemJoin cannot be null!!");
                                            }
                                           localWebSiteItemJoin.serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","webSiteItemJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:relationships_2017_2.lists.webservices.netsuite.com")){
                return "ns15";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","CustomerSearchRow"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localBillingAccountJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "billingAccountJoin"));
                            
                            
                                    if (localBillingAccountJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingAccountJoin cannot be null!!");
                                    }
                                    elementList.add(localBillingAccountJoin);
                                } if (localBillingScheduleJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "billingScheduleJoin"));
                            
                            
                                    if (localBillingScheduleJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingScheduleJoin cannot be null!!");
                                    }
                                    elementList.add(localBillingScheduleJoin);
                                } if (localCallJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "callJoin"));
                            
                            
                                    if (localCallJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                    }
                                    elementList.add(localCallJoin);
                                } if (localCampaignResponseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "campaignResponseJoin"));
                            
                            
                                    if (localCampaignResponseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("campaignResponseJoin cannot be null!!");
                                    }
                                    elementList.add(localCampaignResponseJoin);
                                } if (localCaseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "caseJoin"));
                            
                            
                                    if (localCaseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                    }
                                    elementList.add(localCaseJoin);
                                } if (localContactJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "contactJoin"));
                            
                            
                                    if (localContactJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("contactJoin cannot be null!!");
                                    }
                                    elementList.add(localContactJoin);
                                } if (localContactPrimaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "contactPrimaryJoin"));
                            
                            
                                    if (localContactPrimaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("contactPrimaryJoin cannot be null!!");
                                    }
                                    elementList.add(localContactPrimaryJoin);
                                } if (localEventJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "eventJoin"));
                            
                            
                                    if (localEventJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                    }
                                    elementList.add(localEventJoin);
                                } if (localFileJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "fileJoin"));
                            
                            
                                    if (localFileJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("fileJoin cannot be null!!");
                                    }
                                    elementList.add(localFileJoin);
                                } if (localHostedPageJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "hostedPageJoin"));
                            
                            
                                    if (localHostedPageJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("hostedPageJoin cannot be null!!");
                                    }
                                    elementList.add(localHostedPageJoin);
                                } if (localJobJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "jobJoin"));
                            
                            
                                    if (localJobJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                    }
                                    elementList.add(localJobJoin);
                                } if (localLeadSourceJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "leadSourceJoin"));
                            
                            
                                    if (localLeadSourceJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadSourceJoin cannot be null!!");
                                    }
                                    elementList.add(localLeadSourceJoin);
                                } if (localMessagesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesJoin"));
                            
                            
                                    if (localMessagesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesJoin);
                                } if (localMessagesFromJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesFromJoin"));
                            
                            
                                    if (localMessagesFromJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesFromJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesFromJoin);
                                } if (localMessagesToJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "messagesToJoin"));
                            
                            
                                    if (localMessagesToJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("messagesToJoin cannot be null!!");
                                    }
                                    elementList.add(localMessagesToJoin);
                                } if (localMseSubsidiaryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "mseSubsidiaryJoin"));
                            
                            
                                    if (localMseSubsidiaryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("mseSubsidiaryJoin cannot be null!!");
                                    }
                                    elementList.add(localMseSubsidiaryJoin);
                                } if (localOpportunityJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "opportunityJoin"));
                            
                            
                                    if (localOpportunityJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                    }
                                    elementList.add(localOpportunityJoin);
                                } if (localOriginatingLeadJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "originatingLeadJoin"));
                            
                            
                                    if (localOriginatingLeadJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("originatingLeadJoin cannot be null!!");
                                    }
                                    elementList.add(localOriginatingLeadJoin);
                                } if (localParentCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "parentCustomerJoin"));
                            
                            
                                    if (localParentCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentCustomerJoin cannot be null!!");
                                    }
                                    elementList.add(localParentCustomerJoin);
                                } if (localPartnerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "partnerJoin"));
                            
                            
                                    if (localPartnerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                    }
                                    elementList.add(localPartnerJoin);
                                } if (localPricingJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "pricingJoin"));
                            
                            
                                    if (localPricingJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingJoin cannot be null!!");
                                    }
                                    elementList.add(localPricingJoin);
                                } if (localPurchasedItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "purchasedItemJoin"));
                            
                            
                                    if (localPurchasedItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchasedItemJoin cannot be null!!");
                                    }
                                    elementList.add(localPurchasedItemJoin);
                                } if (localResourceAllocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "resourceAllocationJoin"));
                            
                            
                                    if (localResourceAllocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("resourceAllocationJoin cannot be null!!");
                                    }
                                    elementList.add(localResourceAllocationJoin);
                                } if (localSalesRepJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "salesRepJoin"));
                            
                            
                                    if (localSalesRepJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRepJoin cannot be null!!");
                                    }
                                    elementList.add(localSalesRepJoin);
                                } if (localSubCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "subCustomerJoin"));
                            
                            
                                    if (localSubCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("subCustomerJoin cannot be null!!");
                                    }
                                    elementList.add(localSubCustomerJoin);
                                } if (localTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "taskJoin"));
                            
                            
                                    if (localTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                    }
                                    elementList.add(localTaskJoin);
                                } if (localTimeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "timeJoin"));
                            
                            
                                    if (localTimeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                    }
                                    elementList.add(localTimeJoin);
                                } if (localTopLevelParentJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "topLevelParentJoin"));
                            
                            
                                    if (localTopLevelParentJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("topLevelParentJoin cannot be null!!");
                                    }
                                    elementList.add(localTopLevelParentJoin);
                                } if (localTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "transactionJoin"));
                            
                            
                                    if (localTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                    }
                                    elementList.add(localTransactionJoin);
                                } if (localUpsellItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "upsellItemJoin"));
                            
                            
                                    if (localUpsellItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("upsellItemJoin cannot be null!!");
                                    }
                                    elementList.add(localUpsellItemJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localUserNotesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "userNotesJoin"));
                            
                            
                                    if (localUserNotesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                    }
                                    elementList.add(localUserNotesJoin);
                                } if (localWebSiteCategoryJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "webSiteCategoryJoin"));
                            
                            
                                    if (localWebSiteCategoryJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("webSiteCategoryJoin cannot be null!!");
                                    }
                                    elementList.add(localWebSiteCategoryJoin);
                                } if (localWebSiteItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                      "webSiteItemJoin"));
                            
                            
                                    if (localWebSiteItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("webSiteItemJoin cannot be null!!");
                                    }
                                    elementList.add(localWebSiteItemJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CustomerSearchRow parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CustomerSearchRow object =
                new CustomerSearchRow();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"CustomerSearchRow".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CustomerSearchRow)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list36 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","billingAccountJoin").equals(reader.getName())){
                                
                                                object.setBillingAccountJoin(com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","billingScheduleJoin").equals(reader.getName())){
                                
                                                object.setBillingScheduleJoin(com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","callJoin").equals(reader.getName())){
                                
                                                object.setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","campaignResponseJoin").equals(reader.getName())){
                                
                                                object.setCampaignResponseJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","caseJoin").equals(reader.getName())){
                                
                                                object.setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","contactJoin").equals(reader.getName())){
                                
                                                object.setContactJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","contactPrimaryJoin").equals(reader.getName())){
                                
                                                object.setContactPrimaryJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","eventJoin").equals(reader.getName())){
                                
                                                object.setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","fileJoin").equals(reader.getName())){
                                
                                                object.setFileJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","hostedPageJoin").equals(reader.getName())){
                                
                                                object.setHostedPageJoin(com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","jobJoin").equals(reader.getName())){
                                
                                                object.setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","leadSourceJoin").equals(reader.getName())){
                                
                                                object.setLeadSourceJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesJoin").equals(reader.getName())){
                                
                                                object.setMessagesJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesFromJoin").equals(reader.getName())){
                                
                                                object.setMessagesFromJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","messagesToJoin").equals(reader.getName())){
                                
                                                object.setMessagesToJoin(com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","mseSubsidiaryJoin").equals(reader.getName())){
                                
                                                object.setMseSubsidiaryJoin(com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","opportunityJoin").equals(reader.getName())){
                                
                                                object.setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","originatingLeadJoin").equals(reader.getName())){
                                
                                                object.setOriginatingLeadJoin(com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","parentCustomerJoin").equals(reader.getName())){
                                
                                                object.setParentCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","partnerJoin").equals(reader.getName())){
                                
                                                object.setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","pricingJoin").equals(reader.getName())){
                                
                                                object.setPricingJoin(com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","purchasedItemJoin").equals(reader.getName())){
                                
                                                object.setPurchasedItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","resourceAllocationJoin").equals(reader.getName())){
                                
                                                object.setResourceAllocationJoin(com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","salesRepJoin").equals(reader.getName())){
                                
                                                object.setSalesRepJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","subCustomerJoin").equals(reader.getName())){
                                
                                                object.setSubCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","taskJoin").equals(reader.getName())){
                                
                                                object.setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","timeJoin").equals(reader.getName())){
                                
                                                object.setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","topLevelParentJoin").equals(reader.getName())){
                                
                                                object.setTopLevelParentJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","transactionJoin").equals(reader.getName())){
                                
                                                object.setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","upsellItemJoin").equals(reader.getName())){
                                
                                                object.setUpsellItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","userNotesJoin").equals(reader.getName())){
                                
                                                object.setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","webSiteCategoryJoin").equals(reader.getName())){
                                
                                                object.setWebSiteCategoryJoin(com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","webSiteItemJoin").equals(reader.getName())){
                                
                                                object.setWebSiteItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list36.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone36 = false;
                                                        while(!loopDone36){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone36 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:relationships_2017_2.lists.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list36.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone36 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.class,
                                                                list36));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    