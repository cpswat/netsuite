
/**
 * Employee.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.employees_2017_2;
            

            /**
            *  Employee bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Employee extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Employee
                Namespace URI = urn:employees_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns35
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for Template
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTemplateTracker = false ;

                           public boolean isTemplateSpecified(){
                               return localTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTemplate(){
                               return localTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Template
                               */
                               public void setTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTemplateTracker = param != null;
                                   
                                            this.localTemplate=param;
                                    

                               }
                            

                        /**
                        * field for EntityId
                        */

                        
                                    protected java.lang.String localEntityId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityIdTracker = false ;

                           public boolean isEntityIdSpecified(){
                               return localEntityIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEntityId(){
                               return localEntityId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityId
                               */
                               public void setEntityId(java.lang.String param){
                            localEntityIdTracker = param != null;
                                   
                                            this.localEntityId=param;
                                    

                               }
                            

                        /**
                        * field for Salutation
                        */

                        
                                    protected java.lang.String localSalutation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalutationTracker = false ;

                           public boolean isSalutationSpecified(){
                               return localSalutationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSalutation(){
                               return localSalutation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Salutation
                               */
                               public void setSalutation(java.lang.String param){
                            localSalutationTracker = param != null;
                                   
                                            this.localSalutation=param;
                                    

                               }
                            

                        /**
                        * field for FirstName
                        */

                        
                                    protected java.lang.String localFirstName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstNameTracker = false ;

                           public boolean isFirstNameSpecified(){
                               return localFirstNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFirstName(){
                               return localFirstName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstName
                               */
                               public void setFirstName(java.lang.String param){
                            localFirstNameTracker = param != null;
                                   
                                            this.localFirstName=param;
                                    

                               }
                            

                        /**
                        * field for MiddleName
                        */

                        
                                    protected java.lang.String localMiddleName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMiddleNameTracker = false ;

                           public boolean isMiddleNameSpecified(){
                               return localMiddleNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMiddleName(){
                               return localMiddleName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MiddleName
                               */
                               public void setMiddleName(java.lang.String param){
                            localMiddleNameTracker = param != null;
                                   
                                            this.localMiddleName=param;
                                    

                               }
                            

                        /**
                        * field for LastName
                        */

                        
                                    protected java.lang.String localLastName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastNameTracker = false ;

                           public boolean isLastNameSpecified(){
                               return localLastNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLastName(){
                               return localLastName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastName
                               */
                               public void setLastName(java.lang.String param){
                            localLastNameTracker = param != null;
                                   
                                            this.localLastName=param;
                                    

                               }
                            

                        /**
                        * field for AltName
                        */

                        
                                    protected java.lang.String localAltName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltNameTracker = false ;

                           public boolean isAltNameSpecified(){
                               return localAltNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAltName(){
                               return localAltName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltName
                               */
                               public void setAltName(java.lang.String param){
                            localAltNameTracker = param != null;
                                   
                                            this.localAltName=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected java.lang.String localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(java.lang.String param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected java.lang.String localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(java.lang.String param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected java.lang.String localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(java.lang.String param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for DefaultAddress
                        */

                        
                                    protected java.lang.String localDefaultAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultAddressTracker = false ;

                           public boolean isDefaultAddressSpecified(){
                               return localDefaultAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDefaultAddress(){
                               return localDefaultAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultAddress
                               */
                               public void setDefaultAddress(java.lang.String param){
                            localDefaultAddressTracker = param != null;
                                   
                                            this.localDefaultAddress=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for PhoneticName
                        */

                        
                                    protected java.lang.String localPhoneticName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneticNameTracker = false ;

                           public boolean isPhoneticNameSpecified(){
                               return localPhoneticNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPhoneticName(){
                               return localPhoneticName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PhoneticName
                               */
                               public void setPhoneticName(java.lang.String param){
                            localPhoneticNameTracker = param != null;
                                   
                                            this.localPhoneticName=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected java.util.Calendar localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(java.util.Calendar param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for Initials
                        */

                        
                                    protected java.lang.String localInitials ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInitialsTracker = false ;

                           public boolean isInitialsSpecified(){
                               return localInitialsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInitials(){
                               return localInitials;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Initials
                               */
                               public void setInitials(java.lang.String param){
                            localInitialsTracker = param != null;
                                   
                                            this.localInitials=param;
                                    

                               }
                            

                        /**
                        * field for OfficePhone
                        */

                        
                                    protected java.lang.String localOfficePhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOfficePhoneTracker = false ;

                           public boolean isOfficePhoneSpecified(){
                               return localOfficePhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOfficePhone(){
                               return localOfficePhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OfficePhone
                               */
                               public void setOfficePhone(java.lang.String param){
                            localOfficePhoneTracker = param != null;
                                   
                                            this.localOfficePhone=param;
                                    

                               }
                            

                        /**
                        * field for HomePhone
                        */

                        
                                    protected java.lang.String localHomePhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHomePhoneTracker = false ;

                           public boolean isHomePhoneSpecified(){
                               return localHomePhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHomePhone(){
                               return localHomePhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HomePhone
                               */
                               public void setHomePhone(java.lang.String param){
                            localHomePhoneTracker = param != null;
                                   
                                            this.localHomePhone=param;
                                    

                               }
                            

                        /**
                        * field for MobilePhone
                        */

                        
                                    protected java.lang.String localMobilePhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMobilePhoneTracker = false ;

                           public boolean isMobilePhoneSpecified(){
                               return localMobilePhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMobilePhone(){
                               return localMobilePhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MobilePhone
                               */
                               public void setMobilePhone(java.lang.String param){
                            localMobilePhoneTracker = param != null;
                                   
                                            this.localMobilePhone=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for BillingClass
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localBillingClass ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingClassTracker = false ;

                           public boolean isBillingClassSpecified(){
                               return localBillingClassTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getBillingClass(){
                               return localBillingClass;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingClass
                               */
                               public void setBillingClass(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localBillingClassTracker = param != null;
                                   
                                            this.localBillingClass=param;
                                    

                               }
                            

                        /**
                        * field for AccountNumber
                        */

                        
                                    protected java.lang.String localAccountNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountNumberTracker = false ;

                           public boolean isAccountNumberSpecified(){
                               return localAccountNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAccountNumber(){
                               return localAccountNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountNumber
                               */
                               public void setAccountNumber(java.lang.String param){
                            localAccountNumberTracker = param != null;
                                   
                                            this.localAccountNumber=param;
                                    

                               }
                            

                        /**
                        * field for CompensationCurrency
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCompensationCurrency localCompensationCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompensationCurrencyTracker = false ;

                           public boolean isCompensationCurrencySpecified(){
                               return localCompensationCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCompensationCurrency
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCompensationCurrency getCompensationCurrency(){
                               return localCompensationCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompensationCurrency
                               */
                               public void setCompensationCurrency(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCompensationCurrency param){
                            localCompensationCurrencyTracker = param != null;
                                   
                                            this.localCompensationCurrency=param;
                                    

                               }
                            

                        /**
                        * field for BaseWageType
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeBaseWageType localBaseWageType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBaseWageTypeTracker = false ;

                           public boolean isBaseWageTypeSpecified(){
                               return localBaseWageTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeBaseWageType
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeBaseWageType getBaseWageType(){
                               return localBaseWageType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BaseWageType
                               */
                               public void setBaseWageType(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeBaseWageType param){
                            localBaseWageTypeTracker = param != null;
                                   
                                            this.localBaseWageType=param;
                                    

                               }
                            

                        /**
                        * field for BaseWage
                        */

                        
                                    protected double localBaseWage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBaseWageTracker = false ;

                           public boolean isBaseWageSpecified(){
                               return localBaseWageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getBaseWage(){
                               return localBaseWage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BaseWage
                               */
                               public void setBaseWage(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localBaseWageTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localBaseWage=param;
                                    

                               }
                            

                        /**
                        * field for Comments
                        */

                        
                                    protected java.lang.String localComments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommentsTracker = false ;

                           public boolean isCommentsSpecified(){
                               return localCommentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getComments(){
                               return localComments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Comments
                               */
                               public void setComments(java.lang.String param){
                            localCommentsTracker = param != null;
                                   
                                            this.localComments=param;
                                    

                               }
                            

                        /**
                        * field for GlobalSubscriptionStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus localGlobalSubscriptionStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGlobalSubscriptionStatusTracker = false ;

                           public boolean isGlobalSubscriptionStatusSpecified(){
                               return localGlobalSubscriptionStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus getGlobalSubscriptionStatus(){
                               return localGlobalSubscriptionStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GlobalSubscriptionStatus
                               */
                               public void setGlobalSubscriptionStatus(com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus param){
                            localGlobalSubscriptionStatusTracker = param != null;
                                   
                                            this.localGlobalSubscriptionStatus=param;
                                    

                               }
                            

                        /**
                        * field for Image
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localImageTracker = false ;

                           public boolean isImageSpecified(){
                               return localImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getImage(){
                               return localImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Image
                               */
                               public void setImage(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localImageTracker = param != null;
                                   
                                            this.localImage=param;
                                    

                               }
                            

                        /**
                        * field for PayFrequency
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeePayFrequency localPayFrequency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayFrequencyTracker = false ;

                           public boolean isPayFrequencySpecified(){
                               return localPayFrequencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeePayFrequency
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeePayFrequency getPayFrequency(){
                               return localPayFrequency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayFrequency
                               */
                               public void setPayFrequency(com.netsuite.webservices.lists.employees_2017_2.types.EmployeePayFrequency param){
                            localPayFrequencyTracker = param != null;
                                   
                                            this.localPayFrequency=param;
                                    

                               }
                            

                        /**
                        * field for LastPaidDate
                        */

                        
                                    protected java.util.Calendar localLastPaidDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastPaidDateTracker = false ;

                           public boolean isLastPaidDateSpecified(){
                               return localLastPaidDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastPaidDate(){
                               return localLastPaidDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastPaidDate
                               */
                               public void setLastPaidDate(java.util.Calendar param){
                            localLastPaidDateTracker = param != null;
                                   
                                            this.localLastPaidDate=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for UseTimeData
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeUseTimeData localUseTimeData ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseTimeDataTracker = false ;

                           public boolean isUseTimeDataSpecified(){
                               return localUseTimeDataTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeUseTimeData
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeUseTimeData getUseTimeData(){
                               return localUseTimeData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseTimeData
                               */
                               public void setUseTimeData(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeUseTimeData param){
                            localUseTimeDataTracker = param != null;
                                   
                                            this.localUseTimeData=param;
                                    

                               }
                            

                        /**
                        * field for UsePerquest
                        */

                        
                                    protected boolean localUsePerquest ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUsePerquestTracker = false ;

                           public boolean isUsePerquestSpecified(){
                               return localUsePerquestTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUsePerquest(){
                               return localUsePerquest;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UsePerquest
                               */
                               public void setUsePerquest(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUsePerquestTracker =
                                       true;
                                   
                                            this.localUsePerquest=param;
                                    

                               }
                            

                        /**
                        * field for Workplace
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localWorkplace ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkplaceTracker = false ;

                           public boolean isWorkplaceSpecified(){
                               return localWorkplaceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getWorkplace(){
                               return localWorkplace;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Workplace
                               */
                               public void setWorkplace(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localWorkplaceTracker = param != null;
                                   
                                            this.localWorkplace=param;
                                    

                               }
                            

                        /**
                        * field for AdpId
                        */

                        
                                    protected java.lang.String localAdpId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAdpIdTracker = false ;

                           public boolean isAdpIdSpecified(){
                               return localAdpIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAdpId(){
                               return localAdpId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AdpId
                               */
                               public void setAdpId(java.lang.String param){
                            localAdpIdTracker = param != null;
                                   
                                            this.localAdpId=param;
                                    

                               }
                            

                        /**
                        * field for DirectDeposit
                        */

                        
                                    protected boolean localDirectDeposit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDirectDepositTracker = false ;

                           public boolean isDirectDepositSpecified(){
                               return localDirectDepositTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDirectDeposit(){
                               return localDirectDeposit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DirectDeposit
                               */
                               public void setDirectDeposit(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDirectDepositTracker =
                                       true;
                                   
                                            this.localDirectDeposit=param;
                                    

                               }
                            

                        /**
                        * field for ExpenseLimit
                        */

                        
                                    protected double localExpenseLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpenseLimitTracker = false ;

                           public boolean isExpenseLimitSpecified(){
                               return localExpenseLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getExpenseLimit(){
                               return localExpenseLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpenseLimit
                               */
                               public void setExpenseLimit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localExpenseLimitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localExpenseLimit=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderLimit
                        */

                        
                                    protected double localPurchaseOrderLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderLimitTracker = false ;

                           public boolean isPurchaseOrderLimitSpecified(){
                               return localPurchaseOrderLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPurchaseOrderLimit(){
                               return localPurchaseOrderLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderLimit
                               */
                               public void setPurchaseOrderLimit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPurchaseOrderLimitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPurchaseOrderLimit=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderApprovalLimit
                        */

                        
                                    protected double localPurchaseOrderApprovalLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderApprovalLimitTracker = false ;

                           public boolean isPurchaseOrderApprovalLimitSpecified(){
                               return localPurchaseOrderApprovalLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPurchaseOrderApprovalLimit(){
                               return localPurchaseOrderApprovalLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderApprovalLimit
                               */
                               public void setPurchaseOrderApprovalLimit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPurchaseOrderApprovalLimitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPurchaseOrderApprovalLimit=param;
                                    

                               }
                            

                        /**
                        * field for SocialSecurityNumber
                        */

                        
                                    protected java.lang.String localSocialSecurityNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSocialSecurityNumberTracker = false ;

                           public boolean isSocialSecurityNumberSpecified(){
                               return localSocialSecurityNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSocialSecurityNumber(){
                               return localSocialSecurityNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SocialSecurityNumber
                               */
                               public void setSocialSecurityNumber(java.lang.String param){
                            localSocialSecurityNumberTracker = param != null;
                                   
                                            this.localSocialSecurityNumber=param;
                                    

                               }
                            

                        /**
                        * field for Supervisor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSupervisor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupervisorTracker = false ;

                           public boolean isSupervisorSpecified(){
                               return localSupervisorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSupervisor(){
                               return localSupervisor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Supervisor
                               */
                               public void setSupervisor(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSupervisorTracker = param != null;
                                   
                                            this.localSupervisor=param;
                                    

                               }
                            

                        /**
                        * field for Approver
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApproverTracker = false ;

                           public boolean isApproverSpecified(){
                               return localApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getApprover(){
                               return localApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Approver
                               */
                               public void setApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localApproverTracker = param != null;
                                   
                                            this.localApprover=param;
                                    

                               }
                            

                        /**
                        * field for ApprovalLimit
                        */

                        
                                    protected double localApprovalLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApprovalLimitTracker = false ;

                           public boolean isApprovalLimitSpecified(){
                               return localApprovalLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getApprovalLimit(){
                               return localApprovalLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApprovalLimit
                               */
                               public void setApprovalLimit(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localApprovalLimitTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localApprovalLimit=param;
                                    

                               }
                            

                        /**
                        * field for TimeApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTimeApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeApproverTracker = false ;

                           public boolean isTimeApproverSpecified(){
                               return localTimeApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTimeApprover(){
                               return localTimeApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeApprover
                               */
                               public void setTimeApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTimeApproverTracker = param != null;
                                   
                                            this.localTimeApprover=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEmployeeType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeTypeTracker = false ;

                           public boolean isEmployeeTypeSpecified(){
                               return localEmployeeTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEmployeeType(){
                               return localEmployeeType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeType
                               */
                               public void setEmployeeType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEmployeeTypeTracker = param != null;
                                   
                                            this.localEmployeeType=param;
                                    

                               }
                            

                        /**
                        * field for IsSalesRep
                        */

                        
                                    protected boolean localIsSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSalesRepTracker = false ;

                           public boolean isIsSalesRepSpecified(){
                               return localIsSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsSalesRep(){
                               return localIsSalesRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsSalesRep
                               */
                               public void setIsSalesRep(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsSalesRepTracker =
                                       true;
                                   
                                            this.localIsSalesRep=param;
                                    

                               }
                            

                        /**
                        * field for SalesRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSalesRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRoleTracker = false ;

                           public boolean isSalesRoleSpecified(){
                               return localSalesRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSalesRole(){
                               return localSalesRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRole
                               */
                               public void setSalesRole(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSalesRoleTracker = param != null;
                                   
                                            this.localSalesRole=param;
                                    

                               }
                            

                        /**
                        * field for IsSupportRep
                        */

                        
                                    protected boolean localIsSupportRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSupportRepTracker = false ;

                           public boolean isIsSupportRepSpecified(){
                               return localIsSupportRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsSupportRep(){
                               return localIsSupportRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsSupportRep
                               */
                               public void setIsSupportRep(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsSupportRepTracker =
                                       true;
                                   
                                            this.localIsSupportRep=param;
                                    

                               }
                            

                        /**
                        * field for IsJobResource
                        */

                        
                                    protected boolean localIsJobResource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsJobResourceTracker = false ;

                           public boolean isIsJobResourceSpecified(){
                               return localIsJobResourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsJobResource(){
                               return localIsJobResource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsJobResource
                               */
                               public void setIsJobResource(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsJobResourceTracker =
                                       true;
                                   
                                            this.localIsJobResource=param;
                                    

                               }
                            

                        /**
                        * field for LaborCost
                        */

                        
                                    protected double localLaborCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLaborCostTracker = false ;

                           public boolean isLaborCostSpecified(){
                               return localLaborCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getLaborCost(){
                               return localLaborCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LaborCost
                               */
                               public void setLaborCost(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localLaborCostTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localLaborCost=param;
                                    

                               }
                            

                        /**
                        * field for BirthDate
                        */

                        
                                    protected java.util.Calendar localBirthDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBirthDateTracker = false ;

                           public boolean isBirthDateSpecified(){
                               return localBirthDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getBirthDate(){
                               return localBirthDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BirthDate
                               */
                               public void setBirthDate(java.util.Calendar param){
                            localBirthDateTracker = param != null;
                                   
                                            this.localBirthDate=param;
                                    

                               }
                            

                        /**
                        * field for HireDate
                        */

                        
                                    protected java.util.Calendar localHireDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHireDateTracker = false ;

                           public boolean isHireDateSpecified(){
                               return localHireDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getHireDate(){
                               return localHireDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HireDate
                               */
                               public void setHireDate(java.util.Calendar param){
                            localHireDateTracker = param != null;
                                   
                                            this.localHireDate=param;
                                    

                               }
                            

                        /**
                        * field for ReleaseDate
                        */

                        
                                    protected java.util.Calendar localReleaseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReleaseDateTracker = false ;

                           public boolean isReleaseDateSpecified(){
                               return localReleaseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getReleaseDate(){
                               return localReleaseDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReleaseDate
                               */
                               public void setReleaseDate(java.util.Calendar param){
                            localReleaseDateTracker = param != null;
                                   
                                            this.localReleaseDate=param;
                                    

                               }
                            

                        /**
                        * field for TerminationDetails
                        */

                        
                                    protected java.lang.String localTerminationDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationDetailsTracker = false ;

                           public boolean isTerminationDetailsSpecified(){
                               return localTerminationDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTerminationDetails(){
                               return localTerminationDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationDetails
                               */
                               public void setTerminationDetails(java.lang.String param){
                            localTerminationDetailsTracker = param != null;
                                   
                                            this.localTerminationDetails=param;
                                    

                               }
                            

                        /**
                        * field for TerminationReason
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTerminationReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationReasonTracker = false ;

                           public boolean isTerminationReasonSpecified(){
                               return localTerminationReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTerminationReason(){
                               return localTerminationReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationReason
                               */
                               public void setTerminationReason(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTerminationReasonTracker = param != null;
                                   
                                            this.localTerminationReason=param;
                                    

                               }
                            

                        /**
                        * field for TerminationRegretted
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationRegretted localTerminationRegretted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationRegrettedTracker = false ;

                           public boolean isTerminationRegrettedSpecified(){
                               return localTerminationRegrettedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationRegretted
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationRegretted getTerminationRegretted(){
                               return localTerminationRegretted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationRegretted
                               */
                               public void setTerminationRegretted(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationRegretted param){
                            localTerminationRegrettedTracker = param != null;
                                   
                                            this.localTerminationRegretted=param;
                                    

                               }
                            

                        /**
                        * field for TerminationCategory
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationCategory localTerminationCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationCategoryTracker = false ;

                           public boolean isTerminationCategorySpecified(){
                               return localTerminationCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationCategory
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationCategory getTerminationCategory(){
                               return localTerminationCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationCategory
                               */
                               public void setTerminationCategory(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationCategory param){
                            localTerminationCategoryTracker = param != null;
                                   
                                            this.localTerminationCategory=param;
                                    

                               }
                            

                        /**
                        * field for TimeOffPlan
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTimeOffPlan ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeOffPlanTracker = false ;

                           public boolean isTimeOffPlanSpecified(){
                               return localTimeOffPlanTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTimeOffPlan(){
                               return localTimeOffPlan;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeOffPlan
                               */
                               public void setTimeOffPlan(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTimeOffPlanTracker = param != null;
                                   
                                            this.localTimeOffPlan=param;
                                    

                               }
                            

                        /**
                        * field for LastReviewDate
                        */

                        
                                    protected java.util.Calendar localLastReviewDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastReviewDateTracker = false ;

                           public boolean isLastReviewDateSpecified(){
                               return localLastReviewDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastReviewDate(){
                               return localLastReviewDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastReviewDate
                               */
                               public void setLastReviewDate(java.util.Calendar param){
                            localLastReviewDateTracker = param != null;
                                   
                                            this.localLastReviewDate=param;
                                    

                               }
                            

                        /**
                        * field for NextReviewDate
                        */

                        
                                    protected java.util.Calendar localNextReviewDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextReviewDateTracker = false ;

                           public boolean isNextReviewDateSpecified(){
                               return localNextReviewDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getNextReviewDate(){
                               return localNextReviewDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextReviewDate
                               */
                               public void setNextReviewDate(java.util.Calendar param){
                            localNextReviewDateTracker = param != null;
                                   
                                            this.localNextReviewDate=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEmployeeStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeStatusTracker = false ;

                           public boolean isEmployeeStatusSpecified(){
                               return localEmployeeStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEmployeeStatus(){
                               return localEmployeeStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeStatus
                               */
                               public void setEmployeeStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEmployeeStatusTracker = param != null;
                                   
                                            this.localEmployeeStatus=param;
                                    

                               }
                            

                        /**
                        * field for JobDescription
                        */

                        
                                    protected java.lang.String localJobDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobDescriptionTracker = false ;

                           public boolean isJobDescriptionSpecified(){
                               return localJobDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getJobDescription(){
                               return localJobDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobDescription
                               */
                               public void setJobDescription(java.lang.String param){
                            localJobDescriptionTracker = param != null;
                                   
                                            this.localJobDescription=param;
                                    

                               }
                            

                        /**
                        * field for WorkAssignment
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeWorkAssignment localWorkAssignment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkAssignmentTracker = false ;

                           public boolean isWorkAssignmentSpecified(){
                               return localWorkAssignmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeWorkAssignment
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeWorkAssignment getWorkAssignment(){
                               return localWorkAssignment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WorkAssignment
                               */
                               public void setWorkAssignment(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeWorkAssignment param){
                            localWorkAssignmentTracker = param != null;
                                   
                                            this.localWorkAssignment=param;
                                    

                               }
                            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for MaritalStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localMaritalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMaritalStatusTracker = false ;

                           public boolean isMaritalStatusSpecified(){
                               return localMaritalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getMaritalStatus(){
                               return localMaritalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MaritalStatus
                               */
                               public void setMaritalStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localMaritalStatusTracker = param != null;
                                   
                                            this.localMaritalStatus=param;
                                    

                               }
                            

                        /**
                        * field for Ethnicity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEthnicity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEthnicityTracker = false ;

                           public boolean isEthnicitySpecified(){
                               return localEthnicityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEthnicity(){
                               return localEthnicity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Ethnicity
                               */
                               public void setEthnicity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEthnicityTracker = param != null;
                                   
                                            this.localEthnicity=param;
                                    

                               }
                            

                        /**
                        * field for Gender
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.Gender localGender ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGenderTracker = false ;

                           public boolean isGenderSpecified(){
                               return localGenderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.Gender
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.Gender getGender(){
                               return localGender;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Gender
                               */
                               public void setGender(com.netsuite.webservices.lists.employees_2017_2.types.Gender param){
                            localGenderTracker = param != null;
                                   
                                            this.localGender=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPurchaseOrderApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderApproverTracker = false ;

                           public boolean isPurchaseOrderApproverSpecified(){
                               return localPurchaseOrderApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPurchaseOrderApprover(){
                               return localPurchaseOrderApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderApprover
                               */
                               public void setPurchaseOrderApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPurchaseOrderApproverTracker = param != null;
                                   
                                            this.localPurchaseOrderApprover=param;
                                    

                               }
                            

                        /**
                        * field for WorkCalendar
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localWorkCalendar ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkCalendarTracker = false ;

                           public boolean isWorkCalendarSpecified(){
                               return localWorkCalendarTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getWorkCalendar(){
                               return localWorkCalendar;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WorkCalendar
                               */
                               public void setWorkCalendar(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localWorkCalendarTracker = param != null;
                                   
                                            this.localWorkCalendar=param;
                                    

                               }
                            

                        /**
                        * field for GiveAccess
                        */

                        
                                    protected boolean localGiveAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiveAccessTracker = false ;

                           public boolean isGiveAccessSpecified(){
                               return localGiveAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getGiveAccess(){
                               return localGiveAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiveAccess
                               */
                               public void setGiveAccess(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localGiveAccessTracker =
                                       true;
                                   
                                            this.localGiveAccess=param;
                                    

                               }
                            

                        /**
                        * field for ConcurrentWebServicesUser
                        */

                        
                                    protected boolean localConcurrentWebServicesUser ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConcurrentWebServicesUserTracker = false ;

                           public boolean isConcurrentWebServicesUserSpecified(){
                               return localConcurrentWebServicesUserTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getConcurrentWebServicesUser(){
                               return localConcurrentWebServicesUser;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConcurrentWebServicesUser
                               */
                               public void setConcurrentWebServicesUser(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localConcurrentWebServicesUserTracker =
                                       true;
                                   
                                            this.localConcurrentWebServicesUser=param;
                                    

                               }
                            

                        /**
                        * field for SendEmail
                        */

                        
                                    protected boolean localSendEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendEmailTracker = false ;

                           public boolean isSendEmailSpecified(){
                               return localSendEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendEmail(){
                               return localSendEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendEmail
                               */
                               public void setSendEmail(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendEmailTracker =
                                       true;
                                   
                                            this.localSendEmail=param;
                                    

                               }
                            

                        /**
                        * field for HasOfflineAccess
                        */

                        
                                    protected boolean localHasOfflineAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHasOfflineAccessTracker = false ;

                           public boolean isHasOfflineAccessSpecified(){
                               return localHasOfflineAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getHasOfflineAccess(){
                               return localHasOfflineAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HasOfflineAccess
                               */
                               public void setHasOfflineAccess(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localHasOfflineAccessTracker =
                                       true;
                                   
                                            this.localHasOfflineAccess=param;
                                    

                               }
                            

                        /**
                        * field for Password
                        */

                        
                                    protected java.lang.String localPassword ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPasswordTracker = false ;

                           public boolean isPasswordSpecified(){
                               return localPasswordTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPassword(){
                               return localPassword;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Password
                               */
                               public void setPassword(java.lang.String param){
                            localPasswordTracker = param != null;
                                   
                                            this.localPassword=param;
                                    

                               }
                            

                        /**
                        * field for Password2
                        */

                        
                                    protected java.lang.String localPassword2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPassword2Tracker = false ;

                           public boolean isPassword2Specified(){
                               return localPassword2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPassword2(){
                               return localPassword2;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Password2
                               */
                               public void setPassword2(java.lang.String param){
                            localPassword2Tracker = param != null;
                                   
                                            this.localPassword2=param;
                                    

                               }
                            

                        /**
                        * field for RequirePwdChange
                        */

                        
                                    protected boolean localRequirePwdChange ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRequirePwdChangeTracker = false ;

                           public boolean isRequirePwdChangeSpecified(){
                               return localRequirePwdChangeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRequirePwdChange(){
                               return localRequirePwdChange;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RequirePwdChange
                               */
                               public void setRequirePwdChange(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRequirePwdChangeTracker =
                                       true;
                                   
                                            this.localRequirePwdChange=param;
                                    

                               }
                            

                        /**
                        * field for InheritIPRules
                        */

                        
                                    protected boolean localInheritIPRules ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInheritIPRulesTracker = false ;

                           public boolean isInheritIPRulesSpecified(){
                               return localInheritIPRulesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getInheritIPRules(){
                               return localInheritIPRules;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InheritIPRules
                               */
                               public void setInheritIPRules(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localInheritIPRulesTracker =
                                       true;
                                   
                                            this.localInheritIPRules=param;
                                    

                               }
                            

                        /**
                        * field for IPAddressRule
                        */

                        
                                    protected java.lang.String localIPAddressRule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIPAddressRuleTracker = false ;

                           public boolean isIPAddressRuleSpecified(){
                               return localIPAddressRuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIPAddressRule(){
                               return localIPAddressRule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IPAddressRule
                               */
                               public void setIPAddressRule(java.lang.String param){
                            localIPAddressRuleTracker = param != null;
                                   
                                            this.localIPAddressRule=param;
                                    

                               }
                            

                        /**
                        * field for StartDateTimeOffCalc
                        */

                        
                                    protected java.util.Calendar localStartDateTimeOffCalc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTimeOffCalcTracker = false ;

                           public boolean isStartDateTimeOffCalcSpecified(){
                               return localStartDateTimeOffCalcTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDateTimeOffCalc(){
                               return localStartDateTimeOffCalc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDateTimeOffCalc
                               */
                               public void setStartDateTimeOffCalc(java.util.Calendar param){
                            localStartDateTimeOffCalcTracker = param != null;
                                   
                                            this.localStartDateTimeOffCalc=param;
                                    

                               }
                            

                        /**
                        * field for CommissionPaymentPreference
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCommissionPaymentPreference localCommissionPaymentPreference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommissionPaymentPreferenceTracker = false ;

                           public boolean isCommissionPaymentPreferenceSpecified(){
                               return localCommissionPaymentPreferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCommissionPaymentPreference
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCommissionPaymentPreference getCommissionPaymentPreference(){
                               return localCommissionPaymentPreference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CommissionPaymentPreference
                               */
                               public void setCommissionPaymentPreference(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCommissionPaymentPreference param){
                            localCommissionPaymentPreferenceTracker = param != null;
                                   
                                            this.localCommissionPaymentPreference=param;
                                    

                               }
                            

                        /**
                        * field for BillPay
                        */

                        
                                    protected boolean localBillPay ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillPayTracker = false ;

                           public boolean isBillPaySpecified(){
                               return localBillPayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getBillPay(){
                               return localBillPay;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillPay
                               */
                               public void setBillPay(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localBillPayTracker =
                                       true;
                                   
                                            this.localBillPay=param;
                                    

                               }
                            

                        /**
                        * field for EligibleForCommission
                        */

                        
                                    protected boolean localEligibleForCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEligibleForCommissionTracker = false ;

                           public boolean isEligibleForCommissionSpecified(){
                               return localEligibleForCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEligibleForCommission(){
                               return localEligibleForCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EligibleForCommission
                               */
                               public void setEligibleForCommission(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEligibleForCommissionTracker =
                                       true;
                                   
                                            this.localEligibleForCommission=param;
                                    

                               }
                            

                        /**
                        * field for SubscriptionsList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptionsList localSubscriptionsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubscriptionsListTracker = false ;

                           public boolean isSubscriptionsListSpecified(){
                               return localSubscriptionsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptionsList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptionsList getSubscriptionsList(){
                               return localSubscriptionsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubscriptionsList
                               */
                               public void setSubscriptionsList(com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptionsList param){
                            localSubscriptionsListTracker = param != null;
                                   
                                            this.localSubscriptionsList=param;
                                    

                               }
                            

                        /**
                        * field for RatesList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeRatesList localRatesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRatesListTracker = false ;

                           public boolean isRatesListSpecified(){
                               return localRatesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeRatesList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeRatesList getRatesList(){
                               return localRatesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RatesList
                               */
                               public void setRatesList(com.netsuite.webservices.lists.employees_2017_2.EmployeeRatesList param){
                            localRatesListTracker = param != null;
                                   
                                            this.localRatesList=param;
                                    

                               }
                            

                        /**
                        * field for AddressbookList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbookList localAddressbookList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressbookListTracker = false ;

                           public boolean isAddressbookListSpecified(){
                               return localAddressbookListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbookList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbookList getAddressbookList(){
                               return localAddressbookList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressbookList
                               */
                               public void setAddressbookList(com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbookList param){
                            localAddressbookListTracker = param != null;
                                   
                                            this.localAddressbookList=param;
                                    

                               }
                            

                        /**
                        * field for RolesList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeRolesList localRolesList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRolesListTracker = false ;

                           public boolean isRolesListSpecified(){
                               return localRolesListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeRolesList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeRolesList getRolesList(){
                               return localRolesList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RolesList
                               */
                               public void setRolesList(com.netsuite.webservices.lists.employees_2017_2.EmployeeRolesList param){
                            localRolesListTracker = param != null;
                                   
                                            this.localRolesList=param;
                                    

                               }
                            

                        /**
                        * field for HrEducationList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducationList localHrEducationList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHrEducationListTracker = false ;

                           public boolean isHrEducationListSpecified(){
                               return localHrEducationListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducationList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducationList getHrEducationList(){
                               return localHrEducationList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HrEducationList
                               */
                               public void setHrEducationList(com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducationList param){
                            localHrEducationListTracker = param != null;
                                   
                                            this.localHrEducationList=param;
                                    

                               }
                            

                        /**
                        * field for AccruedTimeList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTimeList localAccruedTimeList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccruedTimeListTracker = false ;

                           public boolean isAccruedTimeListSpecified(){
                               return localAccruedTimeListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTimeList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTimeList getAccruedTimeList(){
                               return localAccruedTimeList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccruedTimeList
                               */
                               public void setAccruedTimeList(com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTimeList param){
                            localAccruedTimeListTracker = param != null;
                                   
                                            this.localAccruedTimeList=param;
                                    

                               }
                            

                        /**
                        * field for DirectDepositList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDepositList localDirectDepositList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDirectDepositListTracker = false ;

                           public boolean isDirectDepositListSpecified(){
                               return localDirectDepositListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDepositList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDepositList getDirectDepositList(){
                               return localDirectDepositList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DirectDepositList
                               */
                               public void setDirectDepositList(com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDepositList param){
                            localDirectDepositListTracker = param != null;
                                   
                                            this.localDirectDepositList=param;
                                    

                               }
                            

                        /**
                        * field for CompanyContributionList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContributionList localCompanyContributionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyContributionListTracker = false ;

                           public boolean isCompanyContributionListSpecified(){
                               return localCompanyContributionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContributionList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContributionList getCompanyContributionList(){
                               return localCompanyContributionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompanyContributionList
                               */
                               public void setCompanyContributionList(com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContributionList param){
                            localCompanyContributionListTracker = param != null;
                                   
                                            this.localCompanyContributionList=param;
                                    

                               }
                            

                        /**
                        * field for EarningList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeEarningList localEarningList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEarningListTracker = false ;

                           public boolean isEarningListSpecified(){
                               return localEarningListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeEarningList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeEarningList getEarningList(){
                               return localEarningList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EarningList
                               */
                               public void setEarningList(com.netsuite.webservices.lists.employees_2017_2.EmployeeEarningList param){
                            localEarningListTracker = param != null;
                                   
                                            this.localEarningList=param;
                                    

                               }
                            

                        /**
                        * field for EmergencyContactList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContactList localEmergencyContactList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmergencyContactListTracker = false ;

                           public boolean isEmergencyContactListSpecified(){
                               return localEmergencyContactListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContactList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContactList getEmergencyContactList(){
                               return localEmergencyContactList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmergencyContactList
                               */
                               public void setEmergencyContactList(com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContactList param){
                            localEmergencyContactListTracker = param != null;
                                   
                                            this.localEmergencyContactList=param;
                                    

                               }
                            

                        /**
                        * field for HcmPositionList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPositionList localHcmPositionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHcmPositionListTracker = false ;

                           public boolean isHcmPositionListSpecified(){
                               return localHcmPositionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPositionList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPositionList getHcmPositionList(){
                               return localHcmPositionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HcmPositionList
                               */
                               public void setHcmPositionList(com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPositionList param){
                            localHcmPositionListTracker = param != null;
                                   
                                            this.localHcmPositionList=param;
                                    

                               }
                            

                        /**
                        * field for DeductionList
                        */

                        
                                    protected com.netsuite.webservices.lists.employees_2017_2.EmployeeDeductionList localDeductionList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeductionListTracker = false ;

                           public boolean isDeductionListSpecified(){
                               return localDeductionListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.employees_2017_2.EmployeeDeductionList
                           */
                           public  com.netsuite.webservices.lists.employees_2017_2.EmployeeDeductionList getDeductionList(){
                               return localDeductionList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeductionList
                               */
                               public void setDeductionList(com.netsuite.webservices.lists.employees_2017_2.EmployeeDeductionList param){
                            localDeductionListTracker = param != null;
                                   
                                            this.localDeductionList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:employees_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Employee",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Employee",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localTemplateTracker){
                                            if (localTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("template cannot be null!!");
                                            }
                                           localTemplate.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","template"),
                                               xmlWriter);
                                        } if (localEntityIdTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "entityId", xmlWriter);
                             

                                          if (localEntityId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEntityId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalutationTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "salutation", xmlWriter);
                             

                                          if (localSalutation==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSalutation);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFirstNameTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "firstName", xmlWriter);
                             

                                          if (localFirstName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFirstName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMiddleNameTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "middleName", xmlWriter);
                             

                                          if (localMiddleName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMiddleName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastNameTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastName", xmlWriter);
                             

                                          if (localLastName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLastName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltNameTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altName", xmlWriter);
                             

                                          if (localAltName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("altName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAltName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPhoneTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "phone", xmlWriter);
                             

                                          if (localPhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFaxTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "fax", xmlWriter);
                             

                                          if (localFax==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localFax);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "email", xmlWriter);
                             

                                          if (localEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDefaultAddressTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "defaultAddress", xmlWriter);
                             

                                          if (localDefaultAddress==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("defaultAddress cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDefaultAddress);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsInactiveTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPhoneticNameTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "phoneticName", xmlWriter);
                             

                                          if (localPhoneticName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPhoneticName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDateCreatedTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dateCreated", xmlWriter);
                             

                                          if (localDateCreated==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateCreated));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInitialsTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "initials", xmlWriter);
                             

                                          if (localInitials==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("initials cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localInitials);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOfficePhoneTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "officePhone", xmlWriter);
                             

                                          if (localOfficePhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("officePhone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOfficePhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHomePhoneTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "homePhone", xmlWriter);
                             

                                          if (localHomePhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("homePhone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHomePhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMobilePhoneTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "mobilePhone", xmlWriter);
                             

                                          if (localMobilePhone==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("mobilePhone cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMobilePhone);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localBillingClassTracker){
                                            if (localBillingClass==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingClass cannot be null!!");
                                            }
                                           localBillingClass.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","billingClass"),
                                               xmlWriter);
                                        } if (localAccountNumberTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "accountNumber", xmlWriter);
                             

                                          if (localAccountNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("accountNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAccountNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCompensationCurrencyTracker){
                                            if (localCompensationCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("compensationCurrency cannot be null!!");
                                            }
                                           localCompensationCurrency.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","compensationCurrency"),
                                               xmlWriter);
                                        } if (localBaseWageTypeTracker){
                                            if (localBaseWageType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("baseWageType cannot be null!!");
                                            }
                                           localBaseWageType.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","baseWageType"),
                                               xmlWriter);
                                        } if (localBaseWageTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "baseWage", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localBaseWage)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("baseWage cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBaseWage));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCommentsTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "comments", xmlWriter);
                             

                                          if (localComments==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localComments);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localGlobalSubscriptionStatusTracker){
                                            if (localGlobalSubscriptionStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                            }
                                           localGlobalSubscriptionStatus.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","globalSubscriptionStatus"),
                                               xmlWriter);
                                        } if (localImageTracker){
                                            if (localImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                            }
                                           localImage.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","image"),
                                               xmlWriter);
                                        } if (localPayFrequencyTracker){
                                            if (localPayFrequency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("payFrequency cannot be null!!");
                                            }
                                           localPayFrequency.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","payFrequency"),
                                               xmlWriter);
                                        } if (localLastPaidDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastPaidDate", xmlWriter);
                             

                                          if (localLastPaidDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastPaidDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastPaidDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localUseTimeDataTracker){
                                            if (localUseTimeData==null){
                                                 throw new org.apache.axis2.databinding.ADBException("useTimeData cannot be null!!");
                                            }
                                           localUseTimeData.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","useTimeData"),
                                               xmlWriter);
                                        } if (localUsePerquestTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "usePerquest", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("usePerquest cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUsePerquest));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWorkplaceTracker){
                                            if (localWorkplace==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workplace cannot be null!!");
                                            }
                                           localWorkplace.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","workplace"),
                                               xmlWriter);
                                        } if (localAdpIdTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "adpId", xmlWriter);
                             

                                          if (localAdpId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("adpId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAdpId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDirectDepositTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "directDeposit", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("directDeposit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirectDeposit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExpenseLimitTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "expenseLimit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localExpenseLimit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("expenseLimit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpenseLimit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseOrderLimitTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "purchaseOrderLimit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPurchaseOrderLimit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderLimit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderLimit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPurchaseOrderApprovalLimitTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "purchaseOrderApprovalLimit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPurchaseOrderApprovalLimit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprovalLimit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderApprovalLimit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSocialSecurityNumberTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "socialSecurityNumber", xmlWriter);
                             

                                          if (localSocialSecurityNumber==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("socialSecurityNumber cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSocialSecurityNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSupervisorTracker){
                                            if (localSupervisor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supervisor cannot be null!!");
                                            }
                                           localSupervisor.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","supervisor"),
                                               xmlWriter);
                                        } if (localApproverTracker){
                                            if (localApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("approver cannot be null!!");
                                            }
                                           localApprover.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","approver"),
                                               xmlWriter);
                                        } if (localApprovalLimitTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "approvalLimit", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localApprovalLimit)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("approvalLimit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApprovalLimit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTimeApproverTracker){
                                            if (localTimeApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeApprover cannot be null!!");
                                            }
                                           localTimeApprover.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","timeApprover"),
                                               xmlWriter);
                                        } if (localEmployeeTypeTracker){
                                            if (localEmployeeType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeType cannot be null!!");
                                            }
                                           localEmployeeType.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","employeeType"),
                                               xmlWriter);
                                        } if (localIsSalesRepTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isSalesRep", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isSalesRep cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsSalesRep));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSalesRoleTracker){
                                            if (localSalesRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRole cannot be null!!");
                                            }
                                           localSalesRole.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","salesRole"),
                                               xmlWriter);
                                        } if (localIsSupportRepTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isSupportRep", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isSupportRep cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsSupportRep));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsJobResourceTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isJobResource", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isJobResource cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsJobResource));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLaborCostTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "laborCost", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localLaborCost)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("laborCost cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLaborCost));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBirthDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "birthDate", xmlWriter);
                             

                                          if (localBirthDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("birthDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBirthDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHireDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hireDate", xmlWriter);
                             

                                          if (localHireDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("hireDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHireDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReleaseDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "releaseDate", xmlWriter);
                             

                                          if (localReleaseDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("releaseDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReleaseDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTerminationDetailsTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "terminationDetails", xmlWriter);
                             

                                          if (localTerminationDetails==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("terminationDetails cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTerminationDetails);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTerminationReasonTracker){
                                            if (localTerminationReason==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationReason cannot be null!!");
                                            }
                                           localTerminationReason.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationReason"),
                                               xmlWriter);
                                        } if (localTerminationRegrettedTracker){
                                            if (localTerminationRegretted==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationRegretted cannot be null!!");
                                            }
                                           localTerminationRegretted.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationRegretted"),
                                               xmlWriter);
                                        } if (localTerminationCategoryTracker){
                                            if (localTerminationCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationCategory cannot be null!!");
                                            }
                                           localTerminationCategory.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationCategory"),
                                               xmlWriter);
                                        } if (localTimeOffPlanTracker){
                                            if (localTimeOffPlan==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeOffPlan cannot be null!!");
                                            }
                                           localTimeOffPlan.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","timeOffPlan"),
                                               xmlWriter);
                                        } if (localLastReviewDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastReviewDate", xmlWriter);
                             

                                          if (localLastReviewDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastReviewDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastReviewDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNextReviewDateTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "nextReviewDate", xmlWriter);
                             

                                          if (localNextReviewDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("nextReviewDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextReviewDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTitleTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmployeeStatusTracker){
                                            if (localEmployeeStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeStatus cannot be null!!");
                                            }
                                           localEmployeeStatus.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","employeeStatus"),
                                               xmlWriter);
                                        } if (localJobDescriptionTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "jobDescription", xmlWriter);
                             

                                          if (localJobDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("jobDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localJobDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWorkAssignmentTracker){
                                            if (localWorkAssignment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workAssignment cannot be null!!");
                                            }
                                           localWorkAssignment.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","workAssignment"),
                                               xmlWriter);
                                        } if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localMaritalStatusTracker){
                                            if (localMaritalStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("maritalStatus cannot be null!!");
                                            }
                                           localMaritalStatus.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","maritalStatus"),
                                               xmlWriter);
                                        } if (localEthnicityTracker){
                                            if (localEthnicity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ethnicity cannot be null!!");
                                            }
                                           localEthnicity.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","ethnicity"),
                                               xmlWriter);
                                        } if (localGenderTracker){
                                            if (localGender==null){
                                                 throw new org.apache.axis2.databinding.ADBException("gender cannot be null!!");
                                            }
                                           localGender.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","gender"),
                                               xmlWriter);
                                        } if (localPurchaseOrderApproverTracker){
                                            if (localPurchaseOrderApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprover cannot be null!!");
                                            }
                                           localPurchaseOrderApprover.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","purchaseOrderApprover"),
                                               xmlWriter);
                                        } if (localWorkCalendarTracker){
                                            if (localWorkCalendar==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workCalendar cannot be null!!");
                                            }
                                           localWorkCalendar.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","workCalendar"),
                                               xmlWriter);
                                        } if (localGiveAccessTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "giveAccess", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiveAccess));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localConcurrentWebServicesUserTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "concurrentWebServicesUser", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("concurrentWebServicesUser cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localConcurrentWebServicesUser));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSendEmailTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendEmail", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendEmail cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendEmail));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHasOfflineAccessTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "hasOfflineAccess", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("hasOfflineAccess cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHasOfflineAccess));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPasswordTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "password", xmlWriter);
                             

                                          if (localPassword==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("password cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPassword);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPassword2Tracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "password2", xmlWriter);
                             

                                          if (localPassword2==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("password2 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPassword2);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRequirePwdChangeTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "requirePwdChange", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("requirePwdChange cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRequirePwdChange));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInheritIPRulesTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "inheritIPRules", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("inheritIPRules cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInheritIPRules));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIPAddressRuleTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "IPAddressRule", xmlWriter);
                             

                                          if (localIPAddressRule==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("IPAddressRule cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIPAddressRule);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateTimeOffCalcTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDateTimeOffCalc", xmlWriter);
                             

                                          if (localStartDateTimeOffCalc==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDateTimeOffCalc cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDateTimeOffCalc));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCommissionPaymentPreferenceTracker){
                                            if (localCommissionPaymentPreference==null){
                                                 throw new org.apache.axis2.databinding.ADBException("commissionPaymentPreference cannot be null!!");
                                            }
                                           localCommissionPaymentPreference.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","commissionPaymentPreference"),
                                               xmlWriter);
                                        } if (localBillPayTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "billPay", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("billPay cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBillPay));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEligibleForCommissionTracker){
                                    namespace = "urn:employees_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "eligibleForCommission", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEligibleForCommission));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubscriptionsListTracker){
                                            if (localSubscriptionsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subscriptionsList cannot be null!!");
                                            }
                                           localSubscriptionsList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","subscriptionsList"),
                                               xmlWriter);
                                        } if (localRatesListTracker){
                                            if (localRatesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ratesList cannot be null!!");
                                            }
                                           localRatesList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","ratesList"),
                                               xmlWriter);
                                        } if (localAddressbookListTracker){
                                            if (localAddressbookList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressbookList cannot be null!!");
                                            }
                                           localAddressbookList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","addressbookList"),
                                               xmlWriter);
                                        } if (localRolesListTracker){
                                            if (localRolesList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rolesList cannot be null!!");
                                            }
                                           localRolesList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","rolesList"),
                                               xmlWriter);
                                        } if (localHrEducationListTracker){
                                            if (localHrEducationList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hrEducationList cannot be null!!");
                                            }
                                           localHrEducationList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","hrEducationList"),
                                               xmlWriter);
                                        } if (localAccruedTimeListTracker){
                                            if (localAccruedTimeList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accruedTimeList cannot be null!!");
                                            }
                                           localAccruedTimeList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","accruedTimeList"),
                                               xmlWriter);
                                        } if (localDirectDepositListTracker){
                                            if (localDirectDepositList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("directDepositList cannot be null!!");
                                            }
                                           localDirectDepositList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","directDepositList"),
                                               xmlWriter);
                                        } if (localCompanyContributionListTracker){
                                            if (localCompanyContributionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("companyContributionList cannot be null!!");
                                            }
                                           localCompanyContributionList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","companyContributionList"),
                                               xmlWriter);
                                        } if (localEarningListTracker){
                                            if (localEarningList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("earningList cannot be null!!");
                                            }
                                           localEarningList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","earningList"),
                                               xmlWriter);
                                        } if (localEmergencyContactListTracker){
                                            if (localEmergencyContactList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emergencyContactList cannot be null!!");
                                            }
                                           localEmergencyContactList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","emergencyContactList"),
                                               xmlWriter);
                                        } if (localHcmPositionListTracker){
                                            if (localHcmPositionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hcmPositionList cannot be null!!");
                                            }
                                           localHcmPositionList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","hcmPositionList"),
                                               xmlWriter);
                                        } if (localDeductionListTracker){
                                            if (localDeductionList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deductionList cannot be null!!");
                                            }
                                           localDeductionList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","deductionList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:employees_2017_2.lists.webservices.netsuite.com")){
                return "ns35";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","Employee"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "template"));
                            
                            
                                    if (localTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("template cannot be null!!");
                                    }
                                    elementList.add(localTemplate);
                                } if (localEntityIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "entityId"));
                                 
                                        if (localEntityId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEntityId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                        }
                                    } if (localSalutationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "salutation"));
                                 
                                        if (localSalutation != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSalutation));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                        }
                                    } if (localFirstNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "firstName"));
                                 
                                        if (localFirstName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFirstName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                        }
                                    } if (localMiddleNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "middleName"));
                                 
                                        if (localMiddleName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMiddleName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                        }
                                    } if (localLastNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "lastName"));
                                 
                                        if (localLastName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                        }
                                    } if (localAltNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "altName"));
                                 
                                        if (localAltName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("altName cannot be null!!");
                                        }
                                    } if (localPhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "phone"));
                                 
                                        if (localPhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                        }
                                    } if (localFaxTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "fax"));
                                 
                                        if (localFax != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFax));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                        }
                                    } if (localEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "email"));
                                 
                                        if (localEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                        }
                                    } if (localDefaultAddressTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "defaultAddress"));
                                 
                                        if (localDefaultAddress != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultAddress));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("defaultAddress cannot be null!!");
                                        }
                                    } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localPhoneticNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "phoneticName"));
                                 
                                        if (localPhoneticName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPhoneticName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                        }
                                    } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localDateCreatedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "dateCreated"));
                                 
                                        if (localDateCreated != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateCreated));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                        }
                                    } if (localInitialsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "initials"));
                                 
                                        if (localInitials != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInitials));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("initials cannot be null!!");
                                        }
                                    } if (localOfficePhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "officePhone"));
                                 
                                        if (localOfficePhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOfficePhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("officePhone cannot be null!!");
                                        }
                                    } if (localHomePhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "homePhone"));
                                 
                                        if (localHomePhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHomePhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("homePhone cannot be null!!");
                                        }
                                    } if (localMobilePhoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "mobilePhone"));
                                 
                                        if (localMobilePhone != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMobilePhone));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("mobilePhone cannot be null!!");
                                        }
                                    } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localBillingClassTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "billingClass"));
                            
                            
                                    if (localBillingClass==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingClass cannot be null!!");
                                    }
                                    elementList.add(localBillingClass);
                                } if (localAccountNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "accountNumber"));
                                 
                                        if (localAccountNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAccountNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("accountNumber cannot be null!!");
                                        }
                                    } if (localCompensationCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "compensationCurrency"));
                            
                            
                                    if (localCompensationCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("compensationCurrency cannot be null!!");
                                    }
                                    elementList.add(localCompensationCurrency);
                                } if (localBaseWageTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "baseWageType"));
                            
                            
                                    if (localBaseWageType==null){
                                         throw new org.apache.axis2.databinding.ADBException("baseWageType cannot be null!!");
                                    }
                                    elementList.add(localBaseWageType);
                                } if (localBaseWageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "baseWage"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBaseWage));
                            } if (localCommentsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "comments"));
                                 
                                        if (localComments != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComments));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                        }
                                    } if (localGlobalSubscriptionStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "globalSubscriptionStatus"));
                            
                            
                                    if (localGlobalSubscriptionStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                    }
                                    elementList.add(localGlobalSubscriptionStatus);
                                } if (localImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "image"));
                            
                            
                                    if (localImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                    }
                                    elementList.add(localImage);
                                } if (localPayFrequencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "payFrequency"));
                            
                            
                                    if (localPayFrequency==null){
                                         throw new org.apache.axis2.databinding.ADBException("payFrequency cannot be null!!");
                                    }
                                    elementList.add(localPayFrequency);
                                } if (localLastPaidDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "lastPaidDate"));
                                 
                                        if (localLastPaidDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastPaidDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastPaidDate cannot be null!!");
                                        }
                                    } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localUseTimeDataTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "useTimeData"));
                            
                            
                                    if (localUseTimeData==null){
                                         throw new org.apache.axis2.databinding.ADBException("useTimeData cannot be null!!");
                                    }
                                    elementList.add(localUseTimeData);
                                } if (localUsePerquestTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "usePerquest"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUsePerquest));
                            } if (localWorkplaceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "workplace"));
                            
                            
                                    if (localWorkplace==null){
                                         throw new org.apache.axis2.databinding.ADBException("workplace cannot be null!!");
                                    }
                                    elementList.add(localWorkplace);
                                } if (localAdpIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "adpId"));
                                 
                                        if (localAdpId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAdpId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("adpId cannot be null!!");
                                        }
                                    } if (localDirectDepositTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "directDeposit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDirectDeposit));
                            } if (localExpenseLimitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "expenseLimit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExpenseLimit));
                            } if (localPurchaseOrderLimitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseOrderLimit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderLimit));
                            } if (localPurchaseOrderApprovalLimitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseOrderApprovalLimit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPurchaseOrderApprovalLimit));
                            } if (localSocialSecurityNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "socialSecurityNumber"));
                                 
                                        if (localSocialSecurityNumber != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSocialSecurityNumber));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("socialSecurityNumber cannot be null!!");
                                        }
                                    } if (localSupervisorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "supervisor"));
                            
                            
                                    if (localSupervisor==null){
                                         throw new org.apache.axis2.databinding.ADBException("supervisor cannot be null!!");
                                    }
                                    elementList.add(localSupervisor);
                                } if (localApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "approver"));
                            
                            
                                    if (localApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("approver cannot be null!!");
                                    }
                                    elementList.add(localApprover);
                                } if (localApprovalLimitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "approvalLimit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApprovalLimit));
                            } if (localTimeApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "timeApprover"));
                            
                            
                                    if (localTimeApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeApprover cannot be null!!");
                                    }
                                    elementList.add(localTimeApprover);
                                } if (localEmployeeTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "employeeType"));
                            
                            
                                    if (localEmployeeType==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeType cannot be null!!");
                                    }
                                    elementList.add(localEmployeeType);
                                } if (localIsSalesRepTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "isSalesRep"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsSalesRep));
                            } if (localSalesRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "salesRole"));
                            
                            
                                    if (localSalesRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRole cannot be null!!");
                                    }
                                    elementList.add(localSalesRole);
                                } if (localIsSupportRepTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "isSupportRep"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsSupportRep));
                            } if (localIsJobResourceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "isJobResource"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsJobResource));
                            } if (localLaborCostTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "laborCost"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLaborCost));
                            } if (localBirthDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "birthDate"));
                                 
                                        if (localBirthDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBirthDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("birthDate cannot be null!!");
                                        }
                                    } if (localHireDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "hireDate"));
                                 
                                        if (localHireDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHireDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("hireDate cannot be null!!");
                                        }
                                    } if (localReleaseDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "releaseDate"));
                                 
                                        if (localReleaseDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReleaseDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("releaseDate cannot be null!!");
                                        }
                                    } if (localTerminationDetailsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "terminationDetails"));
                                 
                                        if (localTerminationDetails != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTerminationDetails));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("terminationDetails cannot be null!!");
                                        }
                                    } if (localTerminationReasonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "terminationReason"));
                            
                            
                                    if (localTerminationReason==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationReason cannot be null!!");
                                    }
                                    elementList.add(localTerminationReason);
                                } if (localTerminationRegrettedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "terminationRegretted"));
                            
                            
                                    if (localTerminationRegretted==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationRegretted cannot be null!!");
                                    }
                                    elementList.add(localTerminationRegretted);
                                } if (localTerminationCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "terminationCategory"));
                            
                            
                                    if (localTerminationCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationCategory cannot be null!!");
                                    }
                                    elementList.add(localTerminationCategory);
                                } if (localTimeOffPlanTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "timeOffPlan"));
                            
                            
                                    if (localTimeOffPlan==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeOffPlan cannot be null!!");
                                    }
                                    elementList.add(localTimeOffPlan);
                                } if (localLastReviewDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "lastReviewDate"));
                                 
                                        if (localLastReviewDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastReviewDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastReviewDate cannot be null!!");
                                        }
                                    } if (localNextReviewDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "nextReviewDate"));
                                 
                                        if (localNextReviewDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNextReviewDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("nextReviewDate cannot be null!!");
                                        }
                                    } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localEmployeeStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "employeeStatus"));
                            
                            
                                    if (localEmployeeStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeStatus cannot be null!!");
                                    }
                                    elementList.add(localEmployeeStatus);
                                } if (localJobDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "jobDescription"));
                                 
                                        if (localJobDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localJobDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("jobDescription cannot be null!!");
                                        }
                                    } if (localWorkAssignmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "workAssignment"));
                            
                            
                                    if (localWorkAssignment==null){
                                         throw new org.apache.axis2.databinding.ADBException("workAssignment cannot be null!!");
                                    }
                                    elementList.add(localWorkAssignment);
                                } if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localMaritalStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "maritalStatus"));
                            
                            
                                    if (localMaritalStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("maritalStatus cannot be null!!");
                                    }
                                    elementList.add(localMaritalStatus);
                                } if (localEthnicityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "ethnicity"));
                            
                            
                                    if (localEthnicity==null){
                                         throw new org.apache.axis2.databinding.ADBException("ethnicity cannot be null!!");
                                    }
                                    elementList.add(localEthnicity);
                                } if (localGenderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "gender"));
                            
                            
                                    if (localGender==null){
                                         throw new org.apache.axis2.databinding.ADBException("gender cannot be null!!");
                                    }
                                    elementList.add(localGender);
                                } if (localPurchaseOrderApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "purchaseOrderApprover"));
                            
                            
                                    if (localPurchaseOrderApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprover cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderApprover);
                                } if (localWorkCalendarTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "workCalendar"));
                            
                            
                                    if (localWorkCalendar==null){
                                         throw new org.apache.axis2.databinding.ADBException("workCalendar cannot be null!!");
                                    }
                                    elementList.add(localWorkCalendar);
                                } if (localGiveAccessTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "giveAccess"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGiveAccess));
                            } if (localConcurrentWebServicesUserTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "concurrentWebServicesUser"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localConcurrentWebServicesUser));
                            } if (localSendEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "sendEmail"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendEmail));
                            } if (localHasOfflineAccessTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "hasOfflineAccess"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHasOfflineAccess));
                            } if (localPasswordTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "password"));
                                 
                                        if (localPassword != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPassword));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("password cannot be null!!");
                                        }
                                    } if (localPassword2Tracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "password2"));
                                 
                                        if (localPassword2 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPassword2));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("password2 cannot be null!!");
                                        }
                                    } if (localRequirePwdChangeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "requirePwdChange"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRequirePwdChange));
                            } if (localInheritIPRulesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "inheritIPRules"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInheritIPRules));
                            } if (localIPAddressRuleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "IPAddressRule"));
                                 
                                        if (localIPAddressRule != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIPAddressRule));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("IPAddressRule cannot be null!!");
                                        }
                                    } if (localStartDateTimeOffCalcTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "startDateTimeOffCalc"));
                                 
                                        if (localStartDateTimeOffCalc != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDateTimeOffCalc));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDateTimeOffCalc cannot be null!!");
                                        }
                                    } if (localCommissionPaymentPreferenceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "commissionPaymentPreference"));
                            
                            
                                    if (localCommissionPaymentPreference==null){
                                         throw new org.apache.axis2.databinding.ADBException("commissionPaymentPreference cannot be null!!");
                                    }
                                    elementList.add(localCommissionPaymentPreference);
                                } if (localBillPayTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "billPay"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBillPay));
                            } if (localEligibleForCommissionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "eligibleForCommission"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEligibleForCommission));
                            } if (localSubscriptionsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "subscriptionsList"));
                            
                            
                                    if (localSubscriptionsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subscriptionsList cannot be null!!");
                                    }
                                    elementList.add(localSubscriptionsList);
                                } if (localRatesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "ratesList"));
                            
                            
                                    if (localRatesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("ratesList cannot be null!!");
                                    }
                                    elementList.add(localRatesList);
                                } if (localAddressbookListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "addressbookList"));
                            
                            
                                    if (localAddressbookList==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressbookList cannot be null!!");
                                    }
                                    elementList.add(localAddressbookList);
                                } if (localRolesListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "rolesList"));
                            
                            
                                    if (localRolesList==null){
                                         throw new org.apache.axis2.databinding.ADBException("rolesList cannot be null!!");
                                    }
                                    elementList.add(localRolesList);
                                } if (localHrEducationListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "hrEducationList"));
                            
                            
                                    if (localHrEducationList==null){
                                         throw new org.apache.axis2.databinding.ADBException("hrEducationList cannot be null!!");
                                    }
                                    elementList.add(localHrEducationList);
                                } if (localAccruedTimeListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "accruedTimeList"));
                            
                            
                                    if (localAccruedTimeList==null){
                                         throw new org.apache.axis2.databinding.ADBException("accruedTimeList cannot be null!!");
                                    }
                                    elementList.add(localAccruedTimeList);
                                } if (localDirectDepositListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "directDepositList"));
                            
                            
                                    if (localDirectDepositList==null){
                                         throw new org.apache.axis2.databinding.ADBException("directDepositList cannot be null!!");
                                    }
                                    elementList.add(localDirectDepositList);
                                } if (localCompanyContributionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "companyContributionList"));
                            
                            
                                    if (localCompanyContributionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("companyContributionList cannot be null!!");
                                    }
                                    elementList.add(localCompanyContributionList);
                                } if (localEarningListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "earningList"));
                            
                            
                                    if (localEarningList==null){
                                         throw new org.apache.axis2.databinding.ADBException("earningList cannot be null!!");
                                    }
                                    elementList.add(localEarningList);
                                } if (localEmergencyContactListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "emergencyContactList"));
                            
                            
                                    if (localEmergencyContactList==null){
                                         throw new org.apache.axis2.databinding.ADBException("emergencyContactList cannot be null!!");
                                    }
                                    elementList.add(localEmergencyContactList);
                                } if (localHcmPositionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "hcmPositionList"));
                            
                            
                                    if (localHcmPositionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("hcmPositionList cannot be null!!");
                                    }
                                    elementList.add(localHcmPositionList);
                                } if (localDeductionListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "deductionList"));
                            
                            
                                    if (localDeductionList==null){
                                         throw new org.apache.axis2.databinding.ADBException("deductionList cannot be null!!");
                                    }
                                    elementList.add(localDeductionList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Employee parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Employee object =
                new Employee();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Employee".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Employee)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","template").equals(reader.getName())){
                                
                                                object.setTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","entityId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"entityId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEntityId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","salutation").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"salutation" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSalutation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","firstName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"firstName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFirstName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","middleName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"middleName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMiddleName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","lastName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","altName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"phone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"fax" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFax(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"email" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","defaultAddress").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"defaultAddress" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDefaultAddress(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","phoneticName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"phoneticName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPhoneticName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dateCreated" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDateCreated(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","initials").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"initials" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInitials(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","officePhone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"officePhone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOfficePhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","homePhone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"homePhone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHomePhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","mobilePhone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"mobilePhone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMobilePhone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","billingClass").equals(reader.getName())){
                                
                                                object.setBillingClass(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","accountNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"accountNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAccountNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","compensationCurrency").equals(reader.getName())){
                                
                                                object.setCompensationCurrency(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCompensationCurrency.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","baseWageType").equals(reader.getName())){
                                
                                                object.setBaseWageType(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeBaseWageType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","baseWage").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"baseWage" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBaseWage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBaseWage(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","comments").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"comments" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setComments(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","globalSubscriptionStatus").equals(reader.getName())){
                                
                                                object.setGlobalSubscriptionStatus(com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","image").equals(reader.getName())){
                                
                                                object.setImage(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","payFrequency").equals(reader.getName())){
                                
                                                object.setPayFrequency(com.netsuite.webservices.lists.employees_2017_2.types.EmployeePayFrequency.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","lastPaidDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastPaidDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastPaidDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","useTimeData").equals(reader.getName())){
                                
                                                object.setUseTimeData(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeUseTimeData.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","usePerquest").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"usePerquest" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUsePerquest(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","workplace").equals(reader.getName())){
                                
                                                object.setWorkplace(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","adpId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"adpId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAdpId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","directDeposit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"directDeposit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDirectDeposit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","expenseLimit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"expenseLimit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExpenseLimit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setExpenseLimit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","purchaseOrderLimit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"purchaseOrderLimit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPurchaseOrderLimit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPurchaseOrderLimit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","purchaseOrderApprovalLimit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"purchaseOrderApprovalLimit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPurchaseOrderApprovalLimit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPurchaseOrderApprovalLimit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","socialSecurityNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"socialSecurityNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSocialSecurityNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","supervisor").equals(reader.getName())){
                                
                                                object.setSupervisor(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","approver").equals(reader.getName())){
                                
                                                object.setApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","approvalLimit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"approvalLimit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setApprovalLimit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setApprovalLimit(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","timeApprover").equals(reader.getName())){
                                
                                                object.setTimeApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","employeeType").equals(reader.getName())){
                                
                                                object.setEmployeeType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","isSalesRep").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isSalesRep" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsSalesRep(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","salesRole").equals(reader.getName())){
                                
                                                object.setSalesRole(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","isSupportRep").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isSupportRep" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsSupportRep(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","isJobResource").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isJobResource" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsJobResource(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","laborCost").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"laborCost" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLaborCost(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLaborCost(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","birthDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"birthDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBirthDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","hireDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hireDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHireDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","releaseDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"releaseDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReleaseDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationDetails").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"terminationDetails" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTerminationDetails(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationReason").equals(reader.getName())){
                                
                                                object.setTerminationReason(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationRegretted").equals(reader.getName())){
                                
                                                object.setTerminationRegretted(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationRegretted.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","terminationCategory").equals(reader.getName())){
                                
                                                object.setTerminationCategory(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationCategory.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","timeOffPlan").equals(reader.getName())){
                                
                                                object.setTimeOffPlan(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","lastReviewDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastReviewDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastReviewDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","nextReviewDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"nextReviewDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNextReviewDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"title" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","employeeStatus").equals(reader.getName())){
                                
                                                object.setEmployeeStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","jobDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"jobDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setJobDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","workAssignment").equals(reader.getName())){
                                
                                                object.setWorkAssignment(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeWorkAssignment.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","maritalStatus").equals(reader.getName())){
                                
                                                object.setMaritalStatus(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","ethnicity").equals(reader.getName())){
                                
                                                object.setEthnicity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","gender").equals(reader.getName())){
                                
                                                object.setGender(com.netsuite.webservices.lists.employees_2017_2.types.Gender.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","purchaseOrderApprover").equals(reader.getName())){
                                
                                                object.setPurchaseOrderApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","workCalendar").equals(reader.getName())){
                                
                                                object.setWorkCalendar(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","giveAccess").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"giveAccess" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setGiveAccess(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","concurrentWebServicesUser").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"concurrentWebServicesUser" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setConcurrentWebServicesUser(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","sendEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","hasOfflineAccess").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"hasOfflineAccess" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHasOfflineAccess(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","password").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"password" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPassword(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","password2").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"password2" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPassword2(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","requirePwdChange").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"requirePwdChange" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRequirePwdChange(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","inheritIPRules").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"inheritIPRules" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInheritIPRules(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","IPAddressRule").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"IPAddressRule" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIPAddressRule(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","startDateTimeOffCalc").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDateTimeOffCalc" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDateTimeOffCalc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","commissionPaymentPreference").equals(reader.getName())){
                                
                                                object.setCommissionPaymentPreference(com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCommissionPaymentPreference.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","billPay").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"billPay" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBillPay(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","eligibleForCommission").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"eligibleForCommission" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEligibleForCommission(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","subscriptionsList").equals(reader.getName())){
                                
                                                object.setSubscriptionsList(com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptionsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","ratesList").equals(reader.getName())){
                                
                                                object.setRatesList(com.netsuite.webservices.lists.employees_2017_2.EmployeeRatesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","addressbookList").equals(reader.getName())){
                                
                                                object.setAddressbookList(com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbookList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","rolesList").equals(reader.getName())){
                                
                                                object.setRolesList(com.netsuite.webservices.lists.employees_2017_2.EmployeeRolesList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","hrEducationList").equals(reader.getName())){
                                
                                                object.setHrEducationList(com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducationList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","accruedTimeList").equals(reader.getName())){
                                
                                                object.setAccruedTimeList(com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTimeList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","directDepositList").equals(reader.getName())){
                                
                                                object.setDirectDepositList(com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDepositList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","companyContributionList").equals(reader.getName())){
                                
                                                object.setCompanyContributionList(com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContributionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","earningList").equals(reader.getName())){
                                
                                                object.setEarningList(com.netsuite.webservices.lists.employees_2017_2.EmployeeEarningList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","emergencyContactList").equals(reader.getName())){
                                
                                                object.setEmergencyContactList(com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContactList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","hcmPositionList").equals(reader.getName())){
                                
                                                object.setHcmPositionList(com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPositionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","deductionList").equals(reader.getName())){
                                
                                                object.setDeductionList(com.netsuite.webservices.lists.employees_2017_2.EmployeeDeductionList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:employees_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    