
/**
 * EmployeeCompensationCurrency.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.employees_2017_2.types;
            

            /**
            *  EmployeeCompensationCurrency bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class EmployeeCompensationCurrency
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.employees_2017_2.lists.webservices.netsuite.com",
                "EmployeeCompensationCurrency",
                "ns34");

            

                        /**
                        * field for EmployeeCompensationCurrency
                        */

                        
                                    protected java.lang.String localEmployeeCompensationCurrency ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected EmployeeCompensationCurrency(java.lang.String value, boolean isRegisterValue) {
                                    localEmployeeCompensationCurrency = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localEmployeeCompensationCurrency, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __aed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_aed");
                                
                                    public static final java.lang.String __afa =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_afa");
                                
                                    public static final java.lang.String __afn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_afn");
                                
                                    public static final java.lang.String __all =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_all");
                                
                                    public static final java.lang.String __amd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_amd");
                                
                                    public static final java.lang.String __ang =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ang");
                                
                                    public static final java.lang.String __aoa =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_aoa");
                                
                                    public static final java.lang.String __ars =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ars");
                                
                                    public static final java.lang.String __aud =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_aud");
                                
                                    public static final java.lang.String __awg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_awg");
                                
                                    public static final java.lang.String __azn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_azn");
                                
                                    public static final java.lang.String __bam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bam");
                                
                                    public static final java.lang.String __bbd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bbd");
                                
                                    public static final java.lang.String __bdt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bdt");
                                
                                    public static final java.lang.String __bgn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bgn");
                                
                                    public static final java.lang.String __bhd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bhd");
                                
                                    public static final java.lang.String __bif =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bif");
                                
                                    public static final java.lang.String __bmd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bmd");
                                
                                    public static final java.lang.String __bnd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bnd");
                                
                                    public static final java.lang.String __bob =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bob");
                                
                                    public static final java.lang.String __brl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_brl");
                                
                                    public static final java.lang.String __bsd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bsd");
                                
                                    public static final java.lang.String __btn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_btn");
                                
                                    public static final java.lang.String __bwp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bwp");
                                
                                    public static final java.lang.String __byn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_byn");
                                
                                    public static final java.lang.String __byr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_byr");
                                
                                    public static final java.lang.String __bzd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bzd");
                                
                                    public static final java.lang.String __cad =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cad");
                                
                                    public static final java.lang.String __cdf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cdf");
                                
                                    public static final java.lang.String __chf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chf");
                                
                                    public static final java.lang.String __clp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_clp");
                                
                                    public static final java.lang.String __cny =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cny");
                                
                                    public static final java.lang.String __cop =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cop");
                                
                                    public static final java.lang.String __crc =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_crc");
                                
                                    public static final java.lang.String __csd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_csd");
                                
                                    public static final java.lang.String __cup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cup");
                                
                                    public static final java.lang.String __cve =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cve");
                                
                                    public static final java.lang.String __cyp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cyp");
                                
                                    public static final java.lang.String __czk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_czk");
                                
                                    public static final java.lang.String __djf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_djf");
                                
                                    public static final java.lang.String __dkk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dkk");
                                
                                    public static final java.lang.String __dop =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dop");
                                
                                    public static final java.lang.String __dzd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dzd");
                                
                                    public static final java.lang.String __ecs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ecs");
                                
                                    public static final java.lang.String __eek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_eek");
                                
                                    public static final java.lang.String __egp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_egp");
                                
                                    public static final java.lang.String __ern =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ern");
                                
                                    public static final java.lang.String __etb =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_etb");
                                
                                    public static final java.lang.String __eur =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_eur");
                                
                                    public static final java.lang.String __fjd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fjd");
                                
                                    public static final java.lang.String __fkp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fkp");
                                
                                    public static final java.lang.String __gbp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gbp");
                                
                                    public static final java.lang.String __gel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gel");
                                
                                    public static final java.lang.String __ggp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ggp");
                                
                                    public static final java.lang.String __ghc =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ghc");
                                
                                    public static final java.lang.String __ghs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ghs");
                                
                                    public static final java.lang.String __gip =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gip");
                                
                                    public static final java.lang.String __gmd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gmd");
                                
                                    public static final java.lang.String __gnf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gnf");
                                
                                    public static final java.lang.String __gtq =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gtq");
                                
                                    public static final java.lang.String __gyd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gyd");
                                
                                    public static final java.lang.String __hkd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hkd");
                                
                                    public static final java.lang.String __hnl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hnl");
                                
                                    public static final java.lang.String __hrk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hrk");
                                
                                    public static final java.lang.String __htg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_htg");
                                
                                    public static final java.lang.String __huf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_huf");
                                
                                    public static final java.lang.String __idr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_idr");
                                
                                    public static final java.lang.String __ils =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ils");
                                
                                    public static final java.lang.String __imp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_imp");
                                
                                    public static final java.lang.String __inr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inr");
                                
                                    public static final java.lang.String __iqd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_iqd");
                                
                                    public static final java.lang.String __irr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_irr");
                                
                                    public static final java.lang.String __isk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_isk");
                                
                                    public static final java.lang.String __jep =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jep");
                                
                                    public static final java.lang.String __jmd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jmd");
                                
                                    public static final java.lang.String __jod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jod");
                                
                                    public static final java.lang.String __jpy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jpy");
                                
                                    public static final java.lang.String __kes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kes");
                                
                                    public static final java.lang.String __kgs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kgs");
                                
                                    public static final java.lang.String __khr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_khr");
                                
                                    public static final java.lang.String __kmf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kmf");
                                
                                    public static final java.lang.String __kpw =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kpw");
                                
                                    public static final java.lang.String __krw =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_krw");
                                
                                    public static final java.lang.String __kwd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kwd");
                                
                                    public static final java.lang.String __kyd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kyd");
                                
                                    public static final java.lang.String __kzt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kzt");
                                
                                    public static final java.lang.String __lak =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lak");
                                
                                    public static final java.lang.String __lbp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lbp");
                                
                                    public static final java.lang.String __lkr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lkr");
                                
                                    public static final java.lang.String __lrd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lrd");
                                
                                    public static final java.lang.String __lsl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lsl");
                                
                                    public static final java.lang.String __ltl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ltl");
                                
                                    public static final java.lang.String __lvl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lvl");
                                
                                    public static final java.lang.String __lyd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lyd");
                                
                                    public static final java.lang.String __mad =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mad");
                                
                                    public static final java.lang.String __mdl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mdl");
                                
                                    public static final java.lang.String __mfg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mfg");
                                
                                    public static final java.lang.String __mga =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mga");
                                
                                    public static final java.lang.String __mkd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mkd");
                                
                                    public static final java.lang.String __mmk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mmk");
                                
                                    public static final java.lang.String __mnt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mnt");
                                
                                    public static final java.lang.String __mop =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mop");
                                
                                    public static final java.lang.String __mro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mro");
                                
                                    public static final java.lang.String __mtl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mtl");
                                
                                    public static final java.lang.String __mur =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mur");
                                
                                    public static final java.lang.String __mvr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mvr");
                                
                                    public static final java.lang.String __mwk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mwk");
                                
                                    public static final java.lang.String __mxn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mxn");
                                
                                    public static final java.lang.String __myr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_myr");
                                
                                    public static final java.lang.String __mzm =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mzm");
                                
                                    public static final java.lang.String __mzn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mzn");
                                
                                    public static final java.lang.String __nad =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nad");
                                
                                    public static final java.lang.String __ngn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ngn");
                                
                                    public static final java.lang.String __nio =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nio");
                                
                                    public static final java.lang.String __nok =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nok");
                                
                                    public static final java.lang.String __npr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_npr");
                                
                                    public static final java.lang.String __nzd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nzd");
                                
                                    public static final java.lang.String __omr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_omr");
                                
                                    public static final java.lang.String __pab =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pab");
                                
                                    public static final java.lang.String __pen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pen");
                                
                                    public static final java.lang.String __pgk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pgk");
                                
                                    public static final java.lang.String __php =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_php");
                                
                                    public static final java.lang.String __pkr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pkr");
                                
                                    public static final java.lang.String __pln =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pln");
                                
                                    public static final java.lang.String __pyg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pyg");
                                
                                    public static final java.lang.String __qar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_qar");
                                
                                    public static final java.lang.String __rol =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rol");
                                
                                    public static final java.lang.String __ron =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ron");
                                
                                    public static final java.lang.String __rsd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rsd");
                                
                                    public static final java.lang.String __rub =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rub");
                                
                                    public static final java.lang.String __rur =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rur");
                                
                                    public static final java.lang.String __rwf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rwf");
                                
                                    public static final java.lang.String __sar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sar");
                                
                                    public static final java.lang.String __sbd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sbd");
                                
                                    public static final java.lang.String __scr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_scr");
                                
                                    public static final java.lang.String __sdd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sdd");
                                
                                    public static final java.lang.String __sdg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sdg");
                                
                                    public static final java.lang.String __sek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sek");
                                
                                    public static final java.lang.String __sgd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sgd");
                                
                                    public static final java.lang.String __shp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shp");
                                
                                    public static final java.lang.String __sit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sit");
                                
                                    public static final java.lang.String __skk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_skk");
                                
                                    public static final java.lang.String __sll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sll");
                                
                                    public static final java.lang.String __sos =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sos");
                                
                                    public static final java.lang.String __spl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_spl");
                                
                                    public static final java.lang.String __srd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_srd");
                                
                                    public static final java.lang.String __srg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_srg");
                                
                                    public static final java.lang.String __ssp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ssp");
                                
                                    public static final java.lang.String __std =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_std");
                                
                                    public static final java.lang.String __svc =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_svc");
                                
                                    public static final java.lang.String __syp =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_syp");
                                
                                    public static final java.lang.String __szl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_szl");
                                
                                    public static final java.lang.String __thb =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thb");
                                
                                    public static final java.lang.String __tjs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tjs");
                                
                                    public static final java.lang.String __tmm =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tmm");
                                
                                    public static final java.lang.String __tmt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tmt");
                                
                                    public static final java.lang.String __tnd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tnd");
                                
                                    public static final java.lang.String __top =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_top");
                                
                                    public static final java.lang.String __trl =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_trl");
                                
                                    public static final java.lang.String __try =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_try");
                                
                                    public static final java.lang.String __ttd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ttd");
                                
                                    public static final java.lang.String __tvd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tvd");
                                
                                    public static final java.lang.String __twd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_twd");
                                
                                    public static final java.lang.String __tzs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tzs");
                                
                                    public static final java.lang.String __uah =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uah");
                                
                                    public static final java.lang.String __ugx =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ugx");
                                
                                    public static final java.lang.String __usd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_usd");
                                
                                    public static final java.lang.String __uyu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uyu");
                                
                                    public static final java.lang.String __uzs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uzs");
                                
                                    public static final java.lang.String __veb =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_veb");
                                
                                    public static final java.lang.String __vef =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vef");
                                
                                    public static final java.lang.String __vnd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vnd");
                                
                                    public static final java.lang.String __vuv =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vuv");
                                
                                    public static final java.lang.String __wst =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_wst");
                                
                                    public static final java.lang.String __wsx =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_wsx");
                                
                                    public static final java.lang.String __xaf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xaf");
                                
                                    public static final java.lang.String __xag =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xag");
                                
                                    public static final java.lang.String __xau =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xau");
                                
                                    public static final java.lang.String __xcd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xcd");
                                
                                    public static final java.lang.String __xdr =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xdr");
                                
                                    public static final java.lang.String __xof =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xof");
                                
                                    public static final java.lang.String __xop =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xop");
                                
                                    public static final java.lang.String __xpd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xpd");
                                
                                    public static final java.lang.String __xpf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xpf");
                                
                                    public static final java.lang.String __xpt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_xpt");
                                
                                    public static final java.lang.String __yer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_yer");
                                
                                    public static final java.lang.String __yum =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_yum");
                                
                                    public static final java.lang.String __zar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zar");
                                
                                    public static final java.lang.String __zmk =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zmk");
                                
                                    public static final java.lang.String __zrn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zrn");
                                
                                    public static final java.lang.String __zwd =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zwd");
                                
                                public static final EmployeeCompensationCurrency _aed =
                                    new EmployeeCompensationCurrency(__aed,true);
                            
                                public static final EmployeeCompensationCurrency _afa =
                                    new EmployeeCompensationCurrency(__afa,true);
                            
                                public static final EmployeeCompensationCurrency _afn =
                                    new EmployeeCompensationCurrency(__afn,true);
                            
                                public static final EmployeeCompensationCurrency _all =
                                    new EmployeeCompensationCurrency(__all,true);
                            
                                public static final EmployeeCompensationCurrency _amd =
                                    new EmployeeCompensationCurrency(__amd,true);
                            
                                public static final EmployeeCompensationCurrency _ang =
                                    new EmployeeCompensationCurrency(__ang,true);
                            
                                public static final EmployeeCompensationCurrency _aoa =
                                    new EmployeeCompensationCurrency(__aoa,true);
                            
                                public static final EmployeeCompensationCurrency _ars =
                                    new EmployeeCompensationCurrency(__ars,true);
                            
                                public static final EmployeeCompensationCurrency _aud =
                                    new EmployeeCompensationCurrency(__aud,true);
                            
                                public static final EmployeeCompensationCurrency _awg =
                                    new EmployeeCompensationCurrency(__awg,true);
                            
                                public static final EmployeeCompensationCurrency _azn =
                                    new EmployeeCompensationCurrency(__azn,true);
                            
                                public static final EmployeeCompensationCurrency _bam =
                                    new EmployeeCompensationCurrency(__bam,true);
                            
                                public static final EmployeeCompensationCurrency _bbd =
                                    new EmployeeCompensationCurrency(__bbd,true);
                            
                                public static final EmployeeCompensationCurrency _bdt =
                                    new EmployeeCompensationCurrency(__bdt,true);
                            
                                public static final EmployeeCompensationCurrency _bgn =
                                    new EmployeeCompensationCurrency(__bgn,true);
                            
                                public static final EmployeeCompensationCurrency _bhd =
                                    new EmployeeCompensationCurrency(__bhd,true);
                            
                                public static final EmployeeCompensationCurrency _bif =
                                    new EmployeeCompensationCurrency(__bif,true);
                            
                                public static final EmployeeCompensationCurrency _bmd =
                                    new EmployeeCompensationCurrency(__bmd,true);
                            
                                public static final EmployeeCompensationCurrency _bnd =
                                    new EmployeeCompensationCurrency(__bnd,true);
                            
                                public static final EmployeeCompensationCurrency _bob =
                                    new EmployeeCompensationCurrency(__bob,true);
                            
                                public static final EmployeeCompensationCurrency _brl =
                                    new EmployeeCompensationCurrency(__brl,true);
                            
                                public static final EmployeeCompensationCurrency _bsd =
                                    new EmployeeCompensationCurrency(__bsd,true);
                            
                                public static final EmployeeCompensationCurrency _btn =
                                    new EmployeeCompensationCurrency(__btn,true);
                            
                                public static final EmployeeCompensationCurrency _bwp =
                                    new EmployeeCompensationCurrency(__bwp,true);
                            
                                public static final EmployeeCompensationCurrency _byn =
                                    new EmployeeCompensationCurrency(__byn,true);
                            
                                public static final EmployeeCompensationCurrency _byr =
                                    new EmployeeCompensationCurrency(__byr,true);
                            
                                public static final EmployeeCompensationCurrency _bzd =
                                    new EmployeeCompensationCurrency(__bzd,true);
                            
                                public static final EmployeeCompensationCurrency _cad =
                                    new EmployeeCompensationCurrency(__cad,true);
                            
                                public static final EmployeeCompensationCurrency _cdf =
                                    new EmployeeCompensationCurrency(__cdf,true);
                            
                                public static final EmployeeCompensationCurrency _chf =
                                    new EmployeeCompensationCurrency(__chf,true);
                            
                                public static final EmployeeCompensationCurrency _clp =
                                    new EmployeeCompensationCurrency(__clp,true);
                            
                                public static final EmployeeCompensationCurrency _cny =
                                    new EmployeeCompensationCurrency(__cny,true);
                            
                                public static final EmployeeCompensationCurrency _cop =
                                    new EmployeeCompensationCurrency(__cop,true);
                            
                                public static final EmployeeCompensationCurrency _crc =
                                    new EmployeeCompensationCurrency(__crc,true);
                            
                                public static final EmployeeCompensationCurrency _csd =
                                    new EmployeeCompensationCurrency(__csd,true);
                            
                                public static final EmployeeCompensationCurrency _cup =
                                    new EmployeeCompensationCurrency(__cup,true);
                            
                                public static final EmployeeCompensationCurrency _cve =
                                    new EmployeeCompensationCurrency(__cve,true);
                            
                                public static final EmployeeCompensationCurrency _cyp =
                                    new EmployeeCompensationCurrency(__cyp,true);
                            
                                public static final EmployeeCompensationCurrency _czk =
                                    new EmployeeCompensationCurrency(__czk,true);
                            
                                public static final EmployeeCompensationCurrency _djf =
                                    new EmployeeCompensationCurrency(__djf,true);
                            
                                public static final EmployeeCompensationCurrency _dkk =
                                    new EmployeeCompensationCurrency(__dkk,true);
                            
                                public static final EmployeeCompensationCurrency _dop =
                                    new EmployeeCompensationCurrency(__dop,true);
                            
                                public static final EmployeeCompensationCurrency _dzd =
                                    new EmployeeCompensationCurrency(__dzd,true);
                            
                                public static final EmployeeCompensationCurrency _ecs =
                                    new EmployeeCompensationCurrency(__ecs,true);
                            
                                public static final EmployeeCompensationCurrency _eek =
                                    new EmployeeCompensationCurrency(__eek,true);
                            
                                public static final EmployeeCompensationCurrency _egp =
                                    new EmployeeCompensationCurrency(__egp,true);
                            
                                public static final EmployeeCompensationCurrency _ern =
                                    new EmployeeCompensationCurrency(__ern,true);
                            
                                public static final EmployeeCompensationCurrency _etb =
                                    new EmployeeCompensationCurrency(__etb,true);
                            
                                public static final EmployeeCompensationCurrency _eur =
                                    new EmployeeCompensationCurrency(__eur,true);
                            
                                public static final EmployeeCompensationCurrency _fjd =
                                    new EmployeeCompensationCurrency(__fjd,true);
                            
                                public static final EmployeeCompensationCurrency _fkp =
                                    new EmployeeCompensationCurrency(__fkp,true);
                            
                                public static final EmployeeCompensationCurrency _gbp =
                                    new EmployeeCompensationCurrency(__gbp,true);
                            
                                public static final EmployeeCompensationCurrency _gel =
                                    new EmployeeCompensationCurrency(__gel,true);
                            
                                public static final EmployeeCompensationCurrency _ggp =
                                    new EmployeeCompensationCurrency(__ggp,true);
                            
                                public static final EmployeeCompensationCurrency _ghc =
                                    new EmployeeCompensationCurrency(__ghc,true);
                            
                                public static final EmployeeCompensationCurrency _ghs =
                                    new EmployeeCompensationCurrency(__ghs,true);
                            
                                public static final EmployeeCompensationCurrency _gip =
                                    new EmployeeCompensationCurrency(__gip,true);
                            
                                public static final EmployeeCompensationCurrency _gmd =
                                    new EmployeeCompensationCurrency(__gmd,true);
                            
                                public static final EmployeeCompensationCurrency _gnf =
                                    new EmployeeCompensationCurrency(__gnf,true);
                            
                                public static final EmployeeCompensationCurrency _gtq =
                                    new EmployeeCompensationCurrency(__gtq,true);
                            
                                public static final EmployeeCompensationCurrency _gyd =
                                    new EmployeeCompensationCurrency(__gyd,true);
                            
                                public static final EmployeeCompensationCurrency _hkd =
                                    new EmployeeCompensationCurrency(__hkd,true);
                            
                                public static final EmployeeCompensationCurrency _hnl =
                                    new EmployeeCompensationCurrency(__hnl,true);
                            
                                public static final EmployeeCompensationCurrency _hrk =
                                    new EmployeeCompensationCurrency(__hrk,true);
                            
                                public static final EmployeeCompensationCurrency _htg =
                                    new EmployeeCompensationCurrency(__htg,true);
                            
                                public static final EmployeeCompensationCurrency _huf =
                                    new EmployeeCompensationCurrency(__huf,true);
                            
                                public static final EmployeeCompensationCurrency _idr =
                                    new EmployeeCompensationCurrency(__idr,true);
                            
                                public static final EmployeeCompensationCurrency _ils =
                                    new EmployeeCompensationCurrency(__ils,true);
                            
                                public static final EmployeeCompensationCurrency _imp =
                                    new EmployeeCompensationCurrency(__imp,true);
                            
                                public static final EmployeeCompensationCurrency _inr =
                                    new EmployeeCompensationCurrency(__inr,true);
                            
                                public static final EmployeeCompensationCurrency _iqd =
                                    new EmployeeCompensationCurrency(__iqd,true);
                            
                                public static final EmployeeCompensationCurrency _irr =
                                    new EmployeeCompensationCurrency(__irr,true);
                            
                                public static final EmployeeCompensationCurrency _isk =
                                    new EmployeeCompensationCurrency(__isk,true);
                            
                                public static final EmployeeCompensationCurrency _jep =
                                    new EmployeeCompensationCurrency(__jep,true);
                            
                                public static final EmployeeCompensationCurrency _jmd =
                                    new EmployeeCompensationCurrency(__jmd,true);
                            
                                public static final EmployeeCompensationCurrency _jod =
                                    new EmployeeCompensationCurrency(__jod,true);
                            
                                public static final EmployeeCompensationCurrency _jpy =
                                    new EmployeeCompensationCurrency(__jpy,true);
                            
                                public static final EmployeeCompensationCurrency _kes =
                                    new EmployeeCompensationCurrency(__kes,true);
                            
                                public static final EmployeeCompensationCurrency _kgs =
                                    new EmployeeCompensationCurrency(__kgs,true);
                            
                                public static final EmployeeCompensationCurrency _khr =
                                    new EmployeeCompensationCurrency(__khr,true);
                            
                                public static final EmployeeCompensationCurrency _kmf =
                                    new EmployeeCompensationCurrency(__kmf,true);
                            
                                public static final EmployeeCompensationCurrency _kpw =
                                    new EmployeeCompensationCurrency(__kpw,true);
                            
                                public static final EmployeeCompensationCurrency _krw =
                                    new EmployeeCompensationCurrency(__krw,true);
                            
                                public static final EmployeeCompensationCurrency _kwd =
                                    new EmployeeCompensationCurrency(__kwd,true);
                            
                                public static final EmployeeCompensationCurrency _kyd =
                                    new EmployeeCompensationCurrency(__kyd,true);
                            
                                public static final EmployeeCompensationCurrency _kzt =
                                    new EmployeeCompensationCurrency(__kzt,true);
                            
                                public static final EmployeeCompensationCurrency _lak =
                                    new EmployeeCompensationCurrency(__lak,true);
                            
                                public static final EmployeeCompensationCurrency _lbp =
                                    new EmployeeCompensationCurrency(__lbp,true);
                            
                                public static final EmployeeCompensationCurrency _lkr =
                                    new EmployeeCompensationCurrency(__lkr,true);
                            
                                public static final EmployeeCompensationCurrency _lrd =
                                    new EmployeeCompensationCurrency(__lrd,true);
                            
                                public static final EmployeeCompensationCurrency _lsl =
                                    new EmployeeCompensationCurrency(__lsl,true);
                            
                                public static final EmployeeCompensationCurrency _ltl =
                                    new EmployeeCompensationCurrency(__ltl,true);
                            
                                public static final EmployeeCompensationCurrency _lvl =
                                    new EmployeeCompensationCurrency(__lvl,true);
                            
                                public static final EmployeeCompensationCurrency _lyd =
                                    new EmployeeCompensationCurrency(__lyd,true);
                            
                                public static final EmployeeCompensationCurrency _mad =
                                    new EmployeeCompensationCurrency(__mad,true);
                            
                                public static final EmployeeCompensationCurrency _mdl =
                                    new EmployeeCompensationCurrency(__mdl,true);
                            
                                public static final EmployeeCompensationCurrency _mfg =
                                    new EmployeeCompensationCurrency(__mfg,true);
                            
                                public static final EmployeeCompensationCurrency _mga =
                                    new EmployeeCompensationCurrency(__mga,true);
                            
                                public static final EmployeeCompensationCurrency _mkd =
                                    new EmployeeCompensationCurrency(__mkd,true);
                            
                                public static final EmployeeCompensationCurrency _mmk =
                                    new EmployeeCompensationCurrency(__mmk,true);
                            
                                public static final EmployeeCompensationCurrency _mnt =
                                    new EmployeeCompensationCurrency(__mnt,true);
                            
                                public static final EmployeeCompensationCurrency _mop =
                                    new EmployeeCompensationCurrency(__mop,true);
                            
                                public static final EmployeeCompensationCurrency _mro =
                                    new EmployeeCompensationCurrency(__mro,true);
                            
                                public static final EmployeeCompensationCurrency _mtl =
                                    new EmployeeCompensationCurrency(__mtl,true);
                            
                                public static final EmployeeCompensationCurrency _mur =
                                    new EmployeeCompensationCurrency(__mur,true);
                            
                                public static final EmployeeCompensationCurrency _mvr =
                                    new EmployeeCompensationCurrency(__mvr,true);
                            
                                public static final EmployeeCompensationCurrency _mwk =
                                    new EmployeeCompensationCurrency(__mwk,true);
                            
                                public static final EmployeeCompensationCurrency _mxn =
                                    new EmployeeCompensationCurrency(__mxn,true);
                            
                                public static final EmployeeCompensationCurrency _myr =
                                    new EmployeeCompensationCurrency(__myr,true);
                            
                                public static final EmployeeCompensationCurrency _mzm =
                                    new EmployeeCompensationCurrency(__mzm,true);
                            
                                public static final EmployeeCompensationCurrency _mzn =
                                    new EmployeeCompensationCurrency(__mzn,true);
                            
                                public static final EmployeeCompensationCurrency _nad =
                                    new EmployeeCompensationCurrency(__nad,true);
                            
                                public static final EmployeeCompensationCurrency _ngn =
                                    new EmployeeCompensationCurrency(__ngn,true);
                            
                                public static final EmployeeCompensationCurrency _nio =
                                    new EmployeeCompensationCurrency(__nio,true);
                            
                                public static final EmployeeCompensationCurrency _nok =
                                    new EmployeeCompensationCurrency(__nok,true);
                            
                                public static final EmployeeCompensationCurrency _npr =
                                    new EmployeeCompensationCurrency(__npr,true);
                            
                                public static final EmployeeCompensationCurrency _nzd =
                                    new EmployeeCompensationCurrency(__nzd,true);
                            
                                public static final EmployeeCompensationCurrency _omr =
                                    new EmployeeCompensationCurrency(__omr,true);
                            
                                public static final EmployeeCompensationCurrency _pab =
                                    new EmployeeCompensationCurrency(__pab,true);
                            
                                public static final EmployeeCompensationCurrency _pen =
                                    new EmployeeCompensationCurrency(__pen,true);
                            
                                public static final EmployeeCompensationCurrency _pgk =
                                    new EmployeeCompensationCurrency(__pgk,true);
                            
                                public static final EmployeeCompensationCurrency _php =
                                    new EmployeeCompensationCurrency(__php,true);
                            
                                public static final EmployeeCompensationCurrency _pkr =
                                    new EmployeeCompensationCurrency(__pkr,true);
                            
                                public static final EmployeeCompensationCurrency _pln =
                                    new EmployeeCompensationCurrency(__pln,true);
                            
                                public static final EmployeeCompensationCurrency _pyg =
                                    new EmployeeCompensationCurrency(__pyg,true);
                            
                                public static final EmployeeCompensationCurrency _qar =
                                    new EmployeeCompensationCurrency(__qar,true);
                            
                                public static final EmployeeCompensationCurrency _rol =
                                    new EmployeeCompensationCurrency(__rol,true);
                            
                                public static final EmployeeCompensationCurrency _ron =
                                    new EmployeeCompensationCurrency(__ron,true);
                            
                                public static final EmployeeCompensationCurrency _rsd =
                                    new EmployeeCompensationCurrency(__rsd,true);
                            
                                public static final EmployeeCompensationCurrency _rub =
                                    new EmployeeCompensationCurrency(__rub,true);
                            
                                public static final EmployeeCompensationCurrency _rur =
                                    new EmployeeCompensationCurrency(__rur,true);
                            
                                public static final EmployeeCompensationCurrency _rwf =
                                    new EmployeeCompensationCurrency(__rwf,true);
                            
                                public static final EmployeeCompensationCurrency _sar =
                                    new EmployeeCompensationCurrency(__sar,true);
                            
                                public static final EmployeeCompensationCurrency _sbd =
                                    new EmployeeCompensationCurrency(__sbd,true);
                            
                                public static final EmployeeCompensationCurrency _scr =
                                    new EmployeeCompensationCurrency(__scr,true);
                            
                                public static final EmployeeCompensationCurrency _sdd =
                                    new EmployeeCompensationCurrency(__sdd,true);
                            
                                public static final EmployeeCompensationCurrency _sdg =
                                    new EmployeeCompensationCurrency(__sdg,true);
                            
                                public static final EmployeeCompensationCurrency _sek =
                                    new EmployeeCompensationCurrency(__sek,true);
                            
                                public static final EmployeeCompensationCurrency _sgd =
                                    new EmployeeCompensationCurrency(__sgd,true);
                            
                                public static final EmployeeCompensationCurrency _shp =
                                    new EmployeeCompensationCurrency(__shp,true);
                            
                                public static final EmployeeCompensationCurrency _sit =
                                    new EmployeeCompensationCurrency(__sit,true);
                            
                                public static final EmployeeCompensationCurrency _skk =
                                    new EmployeeCompensationCurrency(__skk,true);
                            
                                public static final EmployeeCompensationCurrency _sll =
                                    new EmployeeCompensationCurrency(__sll,true);
                            
                                public static final EmployeeCompensationCurrency _sos =
                                    new EmployeeCompensationCurrency(__sos,true);
                            
                                public static final EmployeeCompensationCurrency _spl =
                                    new EmployeeCompensationCurrency(__spl,true);
                            
                                public static final EmployeeCompensationCurrency _srd =
                                    new EmployeeCompensationCurrency(__srd,true);
                            
                                public static final EmployeeCompensationCurrency _srg =
                                    new EmployeeCompensationCurrency(__srg,true);
                            
                                public static final EmployeeCompensationCurrency _ssp =
                                    new EmployeeCompensationCurrency(__ssp,true);
                            
                                public static final EmployeeCompensationCurrency _std =
                                    new EmployeeCompensationCurrency(__std,true);
                            
                                public static final EmployeeCompensationCurrency _svc =
                                    new EmployeeCompensationCurrency(__svc,true);
                            
                                public static final EmployeeCompensationCurrency _syp =
                                    new EmployeeCompensationCurrency(__syp,true);
                            
                                public static final EmployeeCompensationCurrency _szl =
                                    new EmployeeCompensationCurrency(__szl,true);
                            
                                public static final EmployeeCompensationCurrency _thb =
                                    new EmployeeCompensationCurrency(__thb,true);
                            
                                public static final EmployeeCompensationCurrency _tjs =
                                    new EmployeeCompensationCurrency(__tjs,true);
                            
                                public static final EmployeeCompensationCurrency _tmm =
                                    new EmployeeCompensationCurrency(__tmm,true);
                            
                                public static final EmployeeCompensationCurrency _tmt =
                                    new EmployeeCompensationCurrency(__tmt,true);
                            
                                public static final EmployeeCompensationCurrency _tnd =
                                    new EmployeeCompensationCurrency(__tnd,true);
                            
                                public static final EmployeeCompensationCurrency _top =
                                    new EmployeeCompensationCurrency(__top,true);
                            
                                public static final EmployeeCompensationCurrency _trl =
                                    new EmployeeCompensationCurrency(__trl,true);
                            
                                public static final EmployeeCompensationCurrency _try =
                                    new EmployeeCompensationCurrency(__try,true);
                            
                                public static final EmployeeCompensationCurrency _ttd =
                                    new EmployeeCompensationCurrency(__ttd,true);
                            
                                public static final EmployeeCompensationCurrency _tvd =
                                    new EmployeeCompensationCurrency(__tvd,true);
                            
                                public static final EmployeeCompensationCurrency _twd =
                                    new EmployeeCompensationCurrency(__twd,true);
                            
                                public static final EmployeeCompensationCurrency _tzs =
                                    new EmployeeCompensationCurrency(__tzs,true);
                            
                                public static final EmployeeCompensationCurrency _uah =
                                    new EmployeeCompensationCurrency(__uah,true);
                            
                                public static final EmployeeCompensationCurrency _ugx =
                                    new EmployeeCompensationCurrency(__ugx,true);
                            
                                public static final EmployeeCompensationCurrency _usd =
                                    new EmployeeCompensationCurrency(__usd,true);
                            
                                public static final EmployeeCompensationCurrency _uyu =
                                    new EmployeeCompensationCurrency(__uyu,true);
                            
                                public static final EmployeeCompensationCurrency _uzs =
                                    new EmployeeCompensationCurrency(__uzs,true);
                            
                                public static final EmployeeCompensationCurrency _veb =
                                    new EmployeeCompensationCurrency(__veb,true);
                            
                                public static final EmployeeCompensationCurrency _vef =
                                    new EmployeeCompensationCurrency(__vef,true);
                            
                                public static final EmployeeCompensationCurrency _vnd =
                                    new EmployeeCompensationCurrency(__vnd,true);
                            
                                public static final EmployeeCompensationCurrency _vuv =
                                    new EmployeeCompensationCurrency(__vuv,true);
                            
                                public static final EmployeeCompensationCurrency _wst =
                                    new EmployeeCompensationCurrency(__wst,true);
                            
                                public static final EmployeeCompensationCurrency _wsx =
                                    new EmployeeCompensationCurrency(__wsx,true);
                            
                                public static final EmployeeCompensationCurrency _xaf =
                                    new EmployeeCompensationCurrency(__xaf,true);
                            
                                public static final EmployeeCompensationCurrency _xag =
                                    new EmployeeCompensationCurrency(__xag,true);
                            
                                public static final EmployeeCompensationCurrency _xau =
                                    new EmployeeCompensationCurrency(__xau,true);
                            
                                public static final EmployeeCompensationCurrency _xcd =
                                    new EmployeeCompensationCurrency(__xcd,true);
                            
                                public static final EmployeeCompensationCurrency _xdr =
                                    new EmployeeCompensationCurrency(__xdr,true);
                            
                                public static final EmployeeCompensationCurrency _xof =
                                    new EmployeeCompensationCurrency(__xof,true);
                            
                                public static final EmployeeCompensationCurrency _xop =
                                    new EmployeeCompensationCurrency(__xop,true);
                            
                                public static final EmployeeCompensationCurrency _xpd =
                                    new EmployeeCompensationCurrency(__xpd,true);
                            
                                public static final EmployeeCompensationCurrency _xpf =
                                    new EmployeeCompensationCurrency(__xpf,true);
                            
                                public static final EmployeeCompensationCurrency _xpt =
                                    new EmployeeCompensationCurrency(__xpt,true);
                            
                                public static final EmployeeCompensationCurrency _yer =
                                    new EmployeeCompensationCurrency(__yer,true);
                            
                                public static final EmployeeCompensationCurrency _yum =
                                    new EmployeeCompensationCurrency(__yum,true);
                            
                                public static final EmployeeCompensationCurrency _zar =
                                    new EmployeeCompensationCurrency(__zar,true);
                            
                                public static final EmployeeCompensationCurrency _zmk =
                                    new EmployeeCompensationCurrency(__zmk,true);
                            
                                public static final EmployeeCompensationCurrency _zrn =
                                    new EmployeeCompensationCurrency(__zrn,true);
                            
                                public static final EmployeeCompensationCurrency _zwd =
                                    new EmployeeCompensationCurrency(__zwd,true);
                            

                                public java.lang.String getValue() { return localEmployeeCompensationCurrency;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localEmployeeCompensationCurrency.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.employees_2017_2.lists.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":EmployeeCompensationCurrency",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "EmployeeCompensationCurrency",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localEmployeeCompensationCurrency==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("EmployeeCompensationCurrency cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localEmployeeCompensationCurrency);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.employees_2017_2.lists.webservices.netsuite.com")){
                return "ns34";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmployeeCompensationCurrency)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static EmployeeCompensationCurrency fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    EmployeeCompensationCurrency enumeration = (EmployeeCompensationCurrency)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static EmployeeCompensationCurrency fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static EmployeeCompensationCurrency fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return EmployeeCompensationCurrency.Factory.fromString(content,namespaceUri);
                    } else {
                       return EmployeeCompensationCurrency.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static EmployeeCompensationCurrency parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            EmployeeCompensationCurrency object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"EmployeeCompensationCurrency" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = EmployeeCompensationCurrency.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = EmployeeCompensationCurrency.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    