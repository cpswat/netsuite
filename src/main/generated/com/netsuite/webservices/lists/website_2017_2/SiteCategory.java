
/**
 * SiteCategory.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.website_2017_2;
            

            /**
            *  SiteCategory bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SiteCategory extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = SiteCategory
                Namespace URI = urn:website_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns36
                */
            

                        /**
                        * field for Website
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localWebsite ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWebsiteTracker = false ;

                           public boolean isWebsiteSpecified(){
                               return localWebsiteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getWebsite(){
                               return localWebsite;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Website
                               */
                               public void setWebsite(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localWebsiteTracker = param != null;
                                   
                                            this.localWebsite=param;
                                    

                               }
                            

                        /**
                        * field for ItemId
                        */

                        
                                    protected java.lang.String localItemId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemIdTracker = false ;

                           public boolean isItemIdSpecified(){
                               return localItemIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getItemId(){
                               return localItemId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemId
                               */
                               public void setItemId(java.lang.String param){
                            localItemIdTracker = param != null;
                                   
                                            this.localItemId=param;
                                    

                               }
                            

                        /**
                        * field for ParentCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParentCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentCategoryTracker = false ;

                           public boolean isParentCategorySpecified(){
                               return localParentCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParentCategory(){
                               return localParentCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentCategory
                               */
                               public void setParentCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentCategoryTracker = param != null;
                                   
                                            this.localParentCategory=param;
                                    

                               }
                            

                        /**
                        * field for CategoryListLayout
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCategoryListLayout ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryListLayoutTracker = false ;

                           public boolean isCategoryListLayoutSpecified(){
                               return localCategoryListLayoutTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCategoryListLayout(){
                               return localCategoryListLayout;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CategoryListLayout
                               */
                               public void setCategoryListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCategoryListLayoutTracker = param != null;
                                   
                                            this.localCategoryListLayout=param;
                                    

                               }
                            

                        /**
                        * field for ItemListLayout
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItemListLayout ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemListLayoutTracker = false ;

                           public boolean isItemListLayoutSpecified(){
                               return localItemListLayoutTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItemListLayout(){
                               return localItemListLayout;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemListLayout
                               */
                               public void setItemListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localItemListLayoutTracker = param != null;
                                   
                                            this.localItemListLayout=param;
                                    

                               }
                            

                        /**
                        * field for RelatedItemsListLayout
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRelatedItemsListLayout ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelatedItemsListLayoutTracker = false ;

                           public boolean isRelatedItemsListLayoutSpecified(){
                               return localRelatedItemsListLayoutTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRelatedItemsListLayout(){
                               return localRelatedItemsListLayout;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RelatedItemsListLayout
                               */
                               public void setRelatedItemsListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRelatedItemsListLayoutTracker = param != null;
                                   
                                            this.localRelatedItemsListLayout=param;
                                    

                               }
                            

                        /**
                        * field for CorrelatedItemsListLayout
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCorrelatedItemsListLayout ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCorrelatedItemsListLayoutTracker = false ;

                           public boolean isCorrelatedItemsListLayoutSpecified(){
                               return localCorrelatedItemsListLayoutTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCorrelatedItemsListLayout(){
                               return localCorrelatedItemsListLayout;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CorrelatedItemsListLayout
                               */
                               public void setCorrelatedItemsListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCorrelatedItemsListLayoutTracker = param != null;
                                   
                                            this.localCorrelatedItemsListLayout=param;
                                    

                               }
                            

                        /**
                        * field for IsOnline
                        */

                        
                                    protected boolean localIsOnline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOnlineTracker = false ;

                           public boolean isIsOnlineSpecified(){
                               return localIsOnlineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsOnline(){
                               return localIsOnline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOnline
                               */
                               public void setIsOnline(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsOnlineTracker =
                                       true;
                                   
                                            this.localIsOnline=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for StoreDetailedDescription
                        */

                        
                                    protected java.lang.String localStoreDetailedDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDetailedDescriptionTracker = false ;

                           public boolean isStoreDetailedDescriptionSpecified(){
                               return localStoreDetailedDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStoreDetailedDescription(){
                               return localStoreDetailedDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDetailedDescription
                               */
                               public void setStoreDetailedDescription(java.lang.String param){
                            localStoreDetailedDescriptionTracker = param != null;
                                   
                                            this.localStoreDetailedDescription=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayThumbnail
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreDisplayThumbnail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayThumbnailTracker = false ;

                           public boolean isStoreDisplayThumbnailSpecified(){
                               return localStoreDisplayThumbnailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreDisplayThumbnail(){
                               return localStoreDisplayThumbnail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayThumbnail
                               */
                               public void setStoreDisplayThumbnail(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreDisplayThumbnailTracker = param != null;
                                   
                                            this.localStoreDisplayThumbnail=param;
                                    

                               }
                            

                        /**
                        * field for StoreDisplayImage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localStoreDisplayImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreDisplayImageTracker = false ;

                           public boolean isStoreDisplayImageSpecified(){
                               return localStoreDisplayImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getStoreDisplayImage(){
                               return localStoreDisplayImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreDisplayImage
                               */
                               public void setStoreDisplayImage(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localStoreDisplayImageTracker = param != null;
                                   
                                            this.localStoreDisplayImage=param;
                                    

                               }
                            

                        /**
                        * field for PageTitle
                        */

                        
                                    protected java.lang.String localPageTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPageTitleTracker = false ;

                           public boolean isPageTitleSpecified(){
                               return localPageTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getPageTitle(){
                               return localPageTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PageTitle
                               */
                               public void setPageTitle(java.lang.String param){
                            localPageTitleTracker = param != null;
                                   
                                            this.localPageTitle=param;
                                    

                               }
                            

                        /**
                        * field for MetaTagHtml
                        */

                        
                                    protected java.lang.String localMetaTagHtml ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMetaTagHtmlTracker = false ;

                           public boolean isMetaTagHtmlSpecified(){
                               return localMetaTagHtmlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMetaTagHtml(){
                               return localMetaTagHtml;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MetaTagHtml
                               */
                               public void setMetaTagHtml(java.lang.String param){
                            localMetaTagHtmlTracker = param != null;
                                   
                                            this.localMetaTagHtml=param;
                                    

                               }
                            

                        /**
                        * field for ExcludeFromSitemap
                        */

                        
                                    protected boolean localExcludeFromSitemap ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExcludeFromSitemapTracker = false ;

                           public boolean isExcludeFromSitemapSpecified(){
                               return localExcludeFromSitemapTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getExcludeFromSitemap(){
                               return localExcludeFromSitemap;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExcludeFromSitemap
                               */
                               public void setExcludeFromSitemap(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localExcludeFromSitemapTracker =
                                       true;
                                   
                                            this.localExcludeFromSitemap=param;
                                    

                               }
                            

                        /**
                        * field for UrlComponent
                        */

                        
                                    protected java.lang.String localUrlComponent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUrlComponentTracker = false ;

                           public boolean isUrlComponentSpecified(){
                               return localUrlComponentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUrlComponent(){
                               return localUrlComponent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UrlComponent
                               */
                               public void setUrlComponent(java.lang.String param){
                            localUrlComponentTracker = param != null;
                                   
                                            this.localUrlComponent=param;
                                    

                               }
                            

                        /**
                        * field for SitemapPriority
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority localSitemapPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSitemapPriorityTracker = false ;

                           public boolean isSitemapPrioritySpecified(){
                               return localSitemapPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority getSitemapPriority(){
                               return localSitemapPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SitemapPriority
                               */
                               public void setSitemapPriority(com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority param){
                            localSitemapPriorityTracker = param != null;
                                   
                                            this.localSitemapPriority=param;
                                    

                               }
                            

                        /**
                        * field for SearchKeywords
                        */

                        
                                    protected java.lang.String localSearchKeywords ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchKeywordsTracker = false ;

                           public boolean isSearchKeywordsSpecified(){
                               return localSearchKeywordsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSearchKeywords(){
                               return localSearchKeywords;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchKeywords
                               */
                               public void setSearchKeywords(java.lang.String param){
                            localSearchKeywordsTracker = param != null;
                                   
                                            this.localSearchKeywords=param;
                                    

                               }
                            

                        /**
                        * field for PresentationItemList
                        */

                        
                                    protected com.netsuite.webservices.lists.website_2017_2.SiteCategoryPresentationItemList localPresentationItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPresentationItemListTracker = false ;

                           public boolean isPresentationItemListSpecified(){
                               return localPresentationItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.website_2017_2.SiteCategoryPresentationItemList
                           */
                           public  com.netsuite.webservices.lists.website_2017_2.SiteCategoryPresentationItemList getPresentationItemList(){
                               return localPresentationItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PresentationItemList
                               */
                               public void setPresentationItemList(com.netsuite.webservices.lists.website_2017_2.SiteCategoryPresentationItemList param){
                            localPresentationItemListTracker = param != null;
                                   
                                            this.localPresentationItemList=param;
                                    

                               }
                            

                        /**
                        * field for TranslationsList
                        */

                        
                                    protected com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslationList localTranslationsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranslationsListTracker = false ;

                           public boolean isTranslationsListSpecified(){
                               return localTranslationsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslationList
                           */
                           public  com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslationList getTranslationsList(){
                               return localTranslationsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranslationsList
                               */
                               public void setTranslationsList(com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslationList param){
                            localTranslationsListTracker = param != null;
                                   
                                            this.localTranslationsList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:website_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":SiteCategory",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "SiteCategory",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localWebsiteTracker){
                                            if (localWebsite==null){
                                                 throw new org.apache.axis2.databinding.ADBException("website cannot be null!!");
                                            }
                                           localWebsite.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","website"),
                                               xmlWriter);
                                        } if (localItemIdTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "itemId", xmlWriter);
                             

                                          if (localItemId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localItemId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParentCategoryTracker){
                                            if (localParentCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentCategory cannot be null!!");
                                            }
                                           localParentCategory.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","parentCategory"),
                                               xmlWriter);
                                        } if (localCategoryListLayoutTracker){
                                            if (localCategoryListLayout==null){
                                                 throw new org.apache.axis2.databinding.ADBException("categoryListLayout cannot be null!!");
                                            }
                                           localCategoryListLayout.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","categoryListLayout"),
                                               xmlWriter);
                                        } if (localItemListLayoutTracker){
                                            if (localItemListLayout==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemListLayout cannot be null!!");
                                            }
                                           localItemListLayout.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","itemListLayout"),
                                               xmlWriter);
                                        } if (localRelatedItemsListLayoutTracker){
                                            if (localRelatedItemsListLayout==null){
                                                 throw new org.apache.axis2.databinding.ADBException("relatedItemsListLayout cannot be null!!");
                                            }
                                           localRelatedItemsListLayout.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","relatedItemsListLayout"),
                                               xmlWriter);
                                        } if (localCorrelatedItemsListLayoutTracker){
                                            if (localCorrelatedItemsListLayout==null){
                                                 throw new org.apache.axis2.databinding.ADBException("correlatedItemsListLayout cannot be null!!");
                                            }
                                           localCorrelatedItemsListLayout.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","correlatedItemsListLayout"),
                                               xmlWriter);
                                        } if (localIsOnlineTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isOnline", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isOnline cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnline));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsInactiveTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDescriptionTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDetailedDescriptionTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeDetailedDescription", xmlWriter);
                             

                                          if (localStoreDetailedDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("storeDetailedDescription cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStoreDetailedDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStoreDisplayThumbnailTracker){
                                            if (localStoreDisplayThumbnail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeDisplayThumbnail cannot be null!!");
                                            }
                                           localStoreDisplayThumbnail.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","storeDisplayThumbnail"),
                                               xmlWriter);
                                        } if (localStoreDisplayImageTracker){
                                            if (localStoreDisplayImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storeDisplayImage cannot be null!!");
                                            }
                                           localStoreDisplayImage.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","storeDisplayImage"),
                                               xmlWriter);
                                        } if (localPageTitleTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "pageTitle", xmlWriter);
                             

                                          if (localPageTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("pageTitle cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localPageTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMetaTagHtmlTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "metaTagHtml", xmlWriter);
                             

                                          if (localMetaTagHtml==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("metaTagHtml cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMetaTagHtml);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExcludeFromSitemapTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "excludeFromSitemap", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("excludeFromSitemap cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromSitemap));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUrlComponentTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "urlComponent", xmlWriter);
                             

                                          if (localUrlComponent==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("urlComponent cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUrlComponent);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSitemapPriorityTracker){
                                            if (localSitemapPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sitemapPriority cannot be null!!");
                                            }
                                           localSitemapPriority.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","sitemapPriority"),
                                               xmlWriter);
                                        } if (localSearchKeywordsTracker){
                                    namespace = "urn:website_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "searchKeywords", xmlWriter);
                             

                                          if (localSearchKeywords==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("searchKeywords cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSearchKeywords);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPresentationItemListTracker){
                                            if (localPresentationItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("presentationItemList cannot be null!!");
                                            }
                                           localPresentationItemList.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","presentationItemList"),
                                               xmlWriter);
                                        } if (localTranslationsListTracker){
                                            if (localTranslationsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                            }
                                           localTranslationsList.serialize(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","translationsList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:website_2017_2.lists.webservices.netsuite.com")){
                return "ns36";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","SiteCategory"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localWebsiteTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "website"));
                            
                            
                                    if (localWebsite==null){
                                         throw new org.apache.axis2.databinding.ADBException("website cannot be null!!");
                                    }
                                    elementList.add(localWebsite);
                                } if (localItemIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "itemId"));
                                 
                                        if (localItemId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localItemId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("itemId cannot be null!!");
                                        }
                                    } if (localParentCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "parentCategory"));
                            
                            
                                    if (localParentCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentCategory cannot be null!!");
                                    }
                                    elementList.add(localParentCategory);
                                } if (localCategoryListLayoutTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "categoryListLayout"));
                            
                            
                                    if (localCategoryListLayout==null){
                                         throw new org.apache.axis2.databinding.ADBException("categoryListLayout cannot be null!!");
                                    }
                                    elementList.add(localCategoryListLayout);
                                } if (localItemListLayoutTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "itemListLayout"));
                            
                            
                                    if (localItemListLayout==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemListLayout cannot be null!!");
                                    }
                                    elementList.add(localItemListLayout);
                                } if (localRelatedItemsListLayoutTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "relatedItemsListLayout"));
                            
                            
                                    if (localRelatedItemsListLayout==null){
                                         throw new org.apache.axis2.databinding.ADBException("relatedItemsListLayout cannot be null!!");
                                    }
                                    elementList.add(localRelatedItemsListLayout);
                                } if (localCorrelatedItemsListLayoutTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "correlatedItemsListLayout"));
                            
                            
                                    if (localCorrelatedItemsListLayout==null){
                                         throw new org.apache.axis2.databinding.ADBException("correlatedItemsListLayout cannot be null!!");
                                    }
                                    elementList.add(localCorrelatedItemsListLayout);
                                } if (localIsOnlineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "isOnline"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnline));
                            } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localStoreDetailedDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDetailedDescription"));
                                 
                                        if (localStoreDetailedDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreDetailedDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("storeDetailedDescription cannot be null!!");
                                        }
                                    } if (localStoreDisplayThumbnailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayThumbnail"));
                            
                            
                                    if (localStoreDisplayThumbnail==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeDisplayThumbnail cannot be null!!");
                                    }
                                    elementList.add(localStoreDisplayThumbnail);
                                } if (localStoreDisplayImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "storeDisplayImage"));
                            
                            
                                    if (localStoreDisplayImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("storeDisplayImage cannot be null!!");
                                    }
                                    elementList.add(localStoreDisplayImage);
                                } if (localPageTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "pageTitle"));
                                 
                                        if (localPageTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPageTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("pageTitle cannot be null!!");
                                        }
                                    } if (localMetaTagHtmlTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "metaTagHtml"));
                                 
                                        if (localMetaTagHtml != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMetaTagHtml));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("metaTagHtml cannot be null!!");
                                        }
                                    } if (localExcludeFromSitemapTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "excludeFromSitemap"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExcludeFromSitemap));
                            } if (localUrlComponentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "urlComponent"));
                                 
                                        if (localUrlComponent != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUrlComponent));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("urlComponent cannot be null!!");
                                        }
                                    } if (localSitemapPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "sitemapPriority"));
                            
                            
                                    if (localSitemapPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("sitemapPriority cannot be null!!");
                                    }
                                    elementList.add(localSitemapPriority);
                                } if (localSearchKeywordsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "searchKeywords"));
                                 
                                        if (localSearchKeywords != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSearchKeywords));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("searchKeywords cannot be null!!");
                                        }
                                    } if (localPresentationItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "presentationItemList"));
                            
                            
                                    if (localPresentationItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("presentationItemList cannot be null!!");
                                    }
                                    elementList.add(localPresentationItemList);
                                } if (localTranslationsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com",
                                                                      "translationsList"));
                            
                            
                                    if (localTranslationsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                    }
                                    elementList.add(localTranslationsList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SiteCategory parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SiteCategory object =
                new SiteCategory();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"SiteCategory".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SiteCategory)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","website").equals(reader.getName())){
                                
                                                object.setWebsite(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","itemId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"itemId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setItemId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","parentCategory").equals(reader.getName())){
                                
                                                object.setParentCategory(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","categoryListLayout").equals(reader.getName())){
                                
                                                object.setCategoryListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","itemListLayout").equals(reader.getName())){
                                
                                                object.setItemListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","relatedItemsListLayout").equals(reader.getName())){
                                
                                                object.setRelatedItemsListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","correlatedItemsListLayout").equals(reader.getName())){
                                
                                                object.setCorrelatedItemsListLayout(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","isOnline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isOnline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsOnline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","storeDetailedDescription").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeDetailedDescription" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreDetailedDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","storeDisplayThumbnail").equals(reader.getName())){
                                
                                                object.setStoreDisplayThumbnail(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","storeDisplayImage").equals(reader.getName())){
                                
                                                object.setStoreDisplayImage(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","pageTitle").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"pageTitle" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPageTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","metaTagHtml").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"metaTagHtml" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMetaTagHtml(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","excludeFromSitemap").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"excludeFromSitemap" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setExcludeFromSitemap(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","urlComponent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"urlComponent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUrlComponent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","sitemapPriority").equals(reader.getName())){
                                
                                                object.setSitemapPriority(com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","searchKeywords").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"searchKeywords" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSearchKeywords(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","presentationItemList").equals(reader.getName())){
                                
                                                object.setPresentationItemList(com.netsuite.webservices.lists.website_2017_2.SiteCategoryPresentationItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:website_2017_2.lists.webservices.netsuite.com","translationsList").equals(reader.getName())){
                                
                                                object.setTranslationsList(com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslationList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    