
/**
 * ManufacturingRoutingRoutingStep.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.supplychain_2017_2;
            

            /**
            *  ManufacturingRoutingRoutingStep bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ManufacturingRoutingRoutingStep
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ManufacturingRoutingRoutingStep
                Namespace URI = urn:supplychain_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns44
                */
            

                        /**
                        * field for OperationSequence
                        */

                        
                                    protected long localOperationSequence ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOperationSequenceTracker = false ;

                           public boolean isOperationSequenceSpecified(){
                               return localOperationSequenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOperationSequence(){
                               return localOperationSequence;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OperationSequence
                               */
                               public void setOperationSequence(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localOperationSequenceTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localOperationSequence=param;
                                    

                               }
                            

                        /**
                        * field for OperationName
                        */

                        
                                    protected java.lang.String localOperationName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOperationNameTracker = false ;

                           public boolean isOperationNameSpecified(){
                               return localOperationNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOperationName(){
                               return localOperationName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OperationName
                               */
                               public void setOperationName(java.lang.String param){
                            localOperationNameTracker = param != null;
                                   
                                            this.localOperationName=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingWorkCenter
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localManufacturingWorkCenter ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingWorkCenterTracker = false ;

                           public boolean isManufacturingWorkCenterSpecified(){
                               return localManufacturingWorkCenterTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getManufacturingWorkCenter(){
                               return localManufacturingWorkCenter;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingWorkCenter
                               */
                               public void setManufacturingWorkCenter(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localManufacturingWorkCenterTracker = param != null;
                                   
                                            this.localManufacturingWorkCenter=param;
                                    

                               }
                            

                        /**
                        * field for MachineResources
                        */

                        
                                    protected long localMachineResources ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMachineResourcesTracker = false ;

                           public boolean isMachineResourcesSpecified(){
                               return localMachineResourcesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getMachineResources(){
                               return localMachineResources;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MachineResources
                               */
                               public void setMachineResources(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localMachineResourcesTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localMachineResources=param;
                                    

                               }
                            

                        /**
                        * field for LaborResources
                        */

                        
                                    protected long localLaborResources ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLaborResourcesTracker = false ;

                           public boolean isLaborResourcesSpecified(){
                               return localLaborResourcesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLaborResources(){
                               return localLaborResources;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LaborResources
                               */
                               public void setLaborResources(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLaborResourcesTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLaborResources=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingCostTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localManufacturingCostTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingCostTemplateTracker = false ;

                           public boolean isManufacturingCostTemplateSpecified(){
                               return localManufacturingCostTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getManufacturingCostTemplate(){
                               return localManufacturingCostTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingCostTemplate
                               */
                               public void setManufacturingCostTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localManufacturingCostTemplateTracker = param != null;
                                   
                                            this.localManufacturingCostTemplate=param;
                                    

                               }
                            

                        /**
                        * field for SetupTime
                        */

                        
                                    protected double localSetupTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSetupTimeTracker = false ;

                           public boolean isSetupTimeSpecified(){
                               return localSetupTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSetupTime(){
                               return localSetupTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SetupTime
                               */
                               public void setSetupTime(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSetupTimeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSetupTime=param;
                                    

                               }
                            

                        /**
                        * field for RunRate
                        */

                        
                                    protected double localRunRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRunRateTracker = false ;

                           public boolean isRunRateSpecified(){
                               return localRunRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRunRate(){
                               return localRunRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RunRate
                               */
                               public void setRunRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRunRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRunRate=param;
                                    

                               }
                            

                        /**
                        * field for LagType
                        */

                        
                                    protected com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingLagType localLagType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLagTypeTracker = false ;

                           public boolean isLagTypeSpecified(){
                               return localLagTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingLagType
                           */
                           public  com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingLagType getLagType(){
                               return localLagType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LagType
                               */
                               public void setLagType(com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingLagType param){
                            localLagTypeTracker = param != null;
                                   
                                            this.localLagType=param;
                                    

                               }
                            

                        /**
                        * field for LagAmount
                        */

                        
                                    protected long localLagAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLagAmountTracker = false ;

                           public boolean isLagAmountSpecified(){
                               return localLagAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLagAmount(){
                               return localLagAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LagAmount
                               */
                               public void setLagAmount(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLagAmountTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLagAmount=param;
                                    

                               }
                            

                        /**
                        * field for LagUnits
                        */

                        
                                    protected java.lang.String localLagUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLagUnitsTracker = false ;

                           public boolean isLagUnitsSpecified(){
                               return localLagUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLagUnits(){
                               return localLagUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LagUnits
                               */
                               public void setLagUnits(java.lang.String param){
                            localLagUnitsTracker = param != null;
                                   
                                            this.localLagUnits=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:supplychain_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ManufacturingRoutingRoutingStep",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ManufacturingRoutingRoutingStep",
                           xmlWriter);
                   }

               
                   }
                if (localOperationSequenceTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "operationSequence", xmlWriter);
                             
                                               if (localOperationSequence==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("operationSequence cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOperationSequence));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOperationNameTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "operationName", xmlWriter);
                             

                                          if (localOperationName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("operationName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOperationName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturingWorkCenterTracker){
                                            if (localManufacturingWorkCenter==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingWorkCenter cannot be null!!");
                                            }
                                           localManufacturingWorkCenter.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingWorkCenter"),
                                               xmlWriter);
                                        } if (localMachineResourcesTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "machineResources", xmlWriter);
                             
                                               if (localMachineResources==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("machineResources cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMachineResources));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLaborResourcesTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "laborResources", xmlWriter);
                             
                                               if (localLaborResources==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("laborResources cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLaborResources));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localManufacturingCostTemplateTracker){
                                            if (localManufacturingCostTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingCostTemplate cannot be null!!");
                                            }
                                           localManufacturingCostTemplate.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingCostTemplate"),
                                               xmlWriter);
                                        } if (localSetupTimeTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "setupTime", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSetupTime)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("setupTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSetupTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRunRateTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "runRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRunRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("runRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRunRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLagTypeTracker){
                                            if (localLagType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lagType cannot be null!!");
                                            }
                                           localLagType.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","lagType"),
                                               xmlWriter);
                                        } if (localLagAmountTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lagAmount", xmlWriter);
                             
                                               if (localLagAmount==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("lagAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLagAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLagUnitsTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lagUnits", xmlWriter);
                             

                                          if (localLagUnits==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lagUnits cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLagUnits);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:supplychain_2017_2.lists.webservices.netsuite.com")){
                return "ns44";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localOperationSequenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "operationSequence"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOperationSequence));
                            } if (localOperationNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "operationName"));
                                 
                                        if (localOperationName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOperationName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("operationName cannot be null!!");
                                        }
                                    } if (localManufacturingWorkCenterTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturingWorkCenter"));
                            
                            
                                    if (localManufacturingWorkCenter==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingWorkCenter cannot be null!!");
                                    }
                                    elementList.add(localManufacturingWorkCenter);
                                } if (localMachineResourcesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "machineResources"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMachineResources));
                            } if (localLaborResourcesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "laborResources"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLaborResources));
                            } if (localManufacturingCostTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturingCostTemplate"));
                            
                            
                                    if (localManufacturingCostTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingCostTemplate cannot be null!!");
                                    }
                                    elementList.add(localManufacturingCostTemplate);
                                } if (localSetupTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "setupTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSetupTime));
                            } if (localRunRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "runRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRunRate));
                            } if (localLagTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "lagType"));
                            
                            
                                    if (localLagType==null){
                                         throw new org.apache.axis2.databinding.ADBException("lagType cannot be null!!");
                                    }
                                    elementList.add(localLagType);
                                } if (localLagAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "lagAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLagAmount));
                            } if (localLagUnitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "lagUnits"));
                                 
                                        if (localLagUnits != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLagUnits));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lagUnits cannot be null!!");
                                        }
                                    }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ManufacturingRoutingRoutingStep parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ManufacturingRoutingRoutingStep object =
                new ManufacturingRoutingRoutingStep();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ManufacturingRoutingRoutingStep".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ManufacturingRoutingRoutingStep)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","operationSequence").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"operationSequence" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOperationSequence(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOperationSequence(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","operationName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"operationName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOperationName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingWorkCenter").equals(reader.getName())){
                                
                                                object.setManufacturingWorkCenter(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","machineResources").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"machineResources" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMachineResources(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMachineResources(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","laborResources").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"laborResources" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLaborResources(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLaborResources(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingCostTemplate").equals(reader.getName())){
                                
                                                object.setManufacturingCostTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","setupTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"setupTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSetupTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSetupTime(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","runRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"runRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRunRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRunRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","lagType").equals(reader.getName())){
                                
                                                object.setLagType(com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingLagType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","lagAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lagAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLagAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLagAmount(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","lagUnits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lagUnits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLagUnits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    