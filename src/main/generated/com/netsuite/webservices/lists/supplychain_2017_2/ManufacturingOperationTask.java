
/**
 * ManufacturingOperationTask.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.lists.supplychain_2017_2;
            

            /**
            *  ManufacturingOperationTask bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ManufacturingOperationTask extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ManufacturingOperationTask
                Namespace URI = urn:supplychain_2017_2.lists.webservices.netsuite.com
                Namespace Prefix = ns44
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingWorkCenter
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localManufacturingWorkCenter ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingWorkCenterTracker = false ;

                           public boolean isManufacturingWorkCenterSpecified(){
                               return localManufacturingWorkCenterTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getManufacturingWorkCenter(){
                               return localManufacturingWorkCenter;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingWorkCenter
                               */
                               public void setManufacturingWorkCenter(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localManufacturingWorkCenterTracker = param != null;
                                   
                                            this.localManufacturingWorkCenter=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingCostTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localManufacturingCostTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingCostTemplateTracker = false ;

                           public boolean isManufacturingCostTemplateSpecified(){
                               return localManufacturingCostTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getManufacturingCostTemplate(){
                               return localManufacturingCostTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingCostTemplate
                               */
                               public void setManufacturingCostTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localManufacturingCostTemplateTracker = param != null;
                                   
                                            this.localManufacturingCostTemplate=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for OperationSequence
                        */

                        
                                    protected long localOperationSequence ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOperationSequenceTracker = false ;

                           public boolean isOperationSequenceSpecified(){
                               return localOperationSequenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOperationSequence(){
                               return localOperationSequence;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OperationSequence
                               */
                               public void setOperationSequence(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localOperationSequenceTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localOperationSequence=param;
                                    

                               }
                            

                        /**
                        * field for WorkOrder
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localWorkOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkOrderTracker = false ;

                           public boolean isWorkOrderSpecified(){
                               return localWorkOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getWorkOrder(){
                               return localWorkOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WorkOrder
                               */
                               public void setWorkOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localWorkOrderTracker = param != null;
                                   
                                            this.localWorkOrder=param;
                                    

                               }
                            

                        /**
                        * field for Order
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderTracker = false ;

                           public boolean isOrderSpecified(){
                               return localOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOrder(){
                               return localOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Order
                               */
                               public void setOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOrderTracker = param != null;
                                   
                                            this.localOrder=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskStatus localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskStatus
                           */
                           public  com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskStatus getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskStatus param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected java.lang.String localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(java.lang.String param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWork
                        */

                        
                                    protected double localEstimatedWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkTracker = false ;

                           public boolean isEstimatedWorkSpecified(){
                               return localEstimatedWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstimatedWork(){
                               return localEstimatedWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWork
                               */
                               public void setEstimatedWork(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstimatedWorkTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstimatedWork=param;
                                    

                               }
                            

                        /**
                        * field for ActualWork
                        */

                        
                                    protected double localActualWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualWorkTracker = false ;

                           public boolean isActualWorkSpecified(){
                               return localActualWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getActualWork(){
                               return localActualWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualWork
                               */
                               public void setActualWork(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localActualWorkTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localActualWork=param;
                                    

                               }
                            

                        /**
                        * field for RemainingWork
                        */

                        
                                    protected double localRemainingWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRemainingWorkTracker = false ;

                           public boolean isRemainingWorkSpecified(){
                               return localRemainingWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRemainingWork(){
                               return localRemainingWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RemainingWork
                               */
                               public void setRemainingWork(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRemainingWorkTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRemainingWork=param;
                                    

                               }
                            

                        /**
                        * field for InputQuantity
                        */

                        
                                    protected double localInputQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInputQuantityTracker = false ;

                           public boolean isInputQuantitySpecified(){
                               return localInputQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getInputQuantity(){
                               return localInputQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InputQuantity
                               */
                               public void setInputQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localInputQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localInputQuantity=param;
                                    

                               }
                            

                        /**
                        * field for CompletedQuantity
                        */

                        
                                    protected double localCompletedQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompletedQuantityTracker = false ;

                           public boolean isCompletedQuantitySpecified(){
                               return localCompletedQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getCompletedQuantity(){
                               return localCompletedQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompletedQuantity
                               */
                               public void setCompletedQuantity(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localCompletedQuantityTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localCompletedQuantity=param;
                                    

                               }
                            

                        /**
                        * field for SetupTime
                        */

                        
                                    protected double localSetupTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSetupTimeTracker = false ;

                           public boolean isSetupTimeSpecified(){
                               return localSetupTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSetupTime(){
                               return localSetupTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SetupTime
                               */
                               public void setSetupTime(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSetupTimeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSetupTime=param;
                                    

                               }
                            

                        /**
                        * field for RunRate
                        */

                        
                                    protected double localRunRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRunRateTracker = false ;

                           public boolean isRunRateSpecified(){
                               return localRunRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRunRate(){
                               return localRunRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RunRate
                               */
                               public void setRunRate(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRunRateTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRunRate=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for AutoCalculateLag
                        */

                        
                                    protected boolean localAutoCalculateLag ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAutoCalculateLagTracker = false ;

                           public boolean isAutoCalculateLagSpecified(){
                               return localAutoCalculateLagTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAutoCalculateLag(){
                               return localAutoCalculateLag;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AutoCalculateLag
                               */
                               public void setAutoCalculateLag(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAutoCalculateLagTracker =
                                       true;
                                   
                                            this.localAutoCalculateLag=param;
                                    

                               }
                            

                        /**
                        * field for MachineResources
                        */

                        
                                    protected long localMachineResources ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMachineResourcesTracker = false ;

                           public boolean isMachineResourcesSpecified(){
                               return localMachineResourcesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getMachineResources(){
                               return localMachineResources;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MachineResources
                               */
                               public void setMachineResources(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localMachineResourcesTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localMachineResources=param;
                                    

                               }
                            

                        /**
                        * field for LaborResources
                        */

                        
                                    protected long localLaborResources ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLaborResourcesTracker = false ;

                           public boolean isLaborResourcesSpecified(){
                               return localLaborResourcesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getLaborResources(){
                               return localLaborResources;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LaborResources
                               */
                               public void setLaborResources(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localLaborResourcesTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localLaborResources=param;
                                    

                               }
                            

                        /**
                        * field for CostDetailList
                        */

                        
                                    protected com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetailList localCostDetailList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostDetailListTracker = false ;

                           public boolean isCostDetailListSpecified(){
                               return localCostDetailListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetailList
                           */
                           public  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetailList getCostDetailList(){
                               return localCostDetailList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostDetailList
                               */
                               public void setCostDetailList(com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetailList param){
                            localCostDetailListTracker = param != null;
                                   
                                            this.localCostDetailList=param;
                                    

                               }
                            

                        /**
                        * field for PredecessorList
                        */

                        
                                    protected com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessorList localPredecessorList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredecessorListTracker = false ;

                           public boolean isPredecessorListSpecified(){
                               return localPredecessorListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessorList
                           */
                           public  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessorList getPredecessorList(){
                               return localPredecessorList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PredecessorList
                               */
                               public void setPredecessorList(com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessorList param){
                            localPredecessorListTracker = param != null;
                                   
                                            this.localPredecessorList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:supplychain_2017_2.lists.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ManufacturingOperationTask",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ManufacturingOperationTask",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localManufacturingWorkCenterTracker){
                                            if (localManufacturingWorkCenter==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingWorkCenter cannot be null!!");
                                            }
                                           localManufacturingWorkCenter.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingWorkCenter"),
                                               xmlWriter);
                                        } if (localManufacturingCostTemplateTracker){
                                            if (localManufacturingCostTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingCostTemplate cannot be null!!");
                                            }
                                           localManufacturingCostTemplate.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingCostTemplate"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOperationSequenceTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "operationSequence", xmlWriter);
                             
                                               if (localOperationSequence==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("operationSequence cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOperationSequence));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWorkOrderTracker){
                                            if (localWorkOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workOrder cannot be null!!");
                                            }
                                           localWorkOrder.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","workOrder"),
                                               xmlWriter);
                                        } if (localOrderTracker){
                                            if (localOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("order cannot be null!!");
                                            }
                                           localOrder.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","order"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localMessageTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "message", xmlWriter);
                             

                                          if (localMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstimatedWorkTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estimatedWork", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstimatedWork)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estimatedWork cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstimatedWork));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localActualWorkTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "actualWork", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localActualWork)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("actualWork cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualWork));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRemainingWorkTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "remainingWork", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRemainingWork)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("remainingWork cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRemainingWork));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInputQuantityTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "inputQuantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localInputQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("inputQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInputQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCompletedQuantityTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "completedQuantity", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localCompletedQuantity)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("completedQuantity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCompletedQuantity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSetupTimeTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "setupTime", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSetupTime)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("setupTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSetupTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRunRateTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "runRate", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRunRate)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("runRate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRunRate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAutoCalculateLagTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "autoCalculateLag", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("autoCalculateLag cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAutoCalculateLag));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMachineResourcesTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "machineResources", xmlWriter);
                             
                                               if (localMachineResources==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("machineResources cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMachineResources));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLaborResourcesTracker){
                                    namespace = "urn:supplychain_2017_2.lists.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "laborResources", xmlWriter);
                             
                                               if (localLaborResources==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("laborResources cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLaborResources));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCostDetailListTracker){
                                            if (localCostDetailList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costDetailList cannot be null!!");
                                            }
                                           localCostDetailList.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","costDetailList"),
                                               xmlWriter);
                                        } if (localPredecessorListTracker){
                                            if (localPredecessorList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("predecessorList cannot be null!!");
                                            }
                                           localPredecessorList.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","predecessorList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:supplychain_2017_2.lists.webservices.netsuite.com")){
                return "ns44";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","ManufacturingOperationTask"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localManufacturingWorkCenterTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturingWorkCenter"));
                            
                            
                                    if (localManufacturingWorkCenter==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingWorkCenter cannot be null!!");
                                    }
                                    elementList.add(localManufacturingWorkCenter);
                                } if (localManufacturingCostTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "manufacturingCostTemplate"));
                            
                            
                                    if (localManufacturingCostTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingCostTemplate cannot be null!!");
                                    }
                                    elementList.add(localManufacturingCostTemplate);
                                } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localOperationSequenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "operationSequence"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOperationSequence));
                            } if (localWorkOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "workOrder"));
                            
                            
                                    if (localWorkOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("workOrder cannot be null!!");
                                    }
                                    elementList.add(localWorkOrder);
                                } if (localOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "order"));
                            
                            
                                    if (localOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("order cannot be null!!");
                                    }
                                    elementList.add(localOrder);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "message"));
                                 
                                        if (localMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                        }
                                    } if (localEstimatedWorkTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "estimatedWork"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstimatedWork));
                            } if (localActualWorkTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "actualWork"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualWork));
                            } if (localRemainingWorkTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "remainingWork"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRemainingWork));
                            } if (localInputQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "inputQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInputQuantity));
                            } if (localCompletedQuantityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "completedQuantity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCompletedQuantity));
                            } if (localSetupTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "setupTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSetupTime));
                            } if (localRunRateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "runRate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRunRate));
                            } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localAutoCalculateLagTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "autoCalculateLag"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAutoCalculateLag));
                            } if (localMachineResourcesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "machineResources"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMachineResources));
                            } if (localLaborResourcesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "laborResources"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLaborResources));
                            } if (localCostDetailListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "costDetailList"));
                            
                            
                                    if (localCostDetailList==null){
                                         throw new org.apache.axis2.databinding.ADBException("costDetailList cannot be null!!");
                                    }
                                    elementList.add(localCostDetailList);
                                } if (localPredecessorListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "predecessorList"));
                            
                            
                                    if (localPredecessorList==null){
                                         throw new org.apache.axis2.databinding.ADBException("predecessorList cannot be null!!");
                                    }
                                    elementList.add(localPredecessorList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ManufacturingOperationTask parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ManufacturingOperationTask object =
                new ManufacturingOperationTask();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ManufacturingOperationTask".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ManufacturingOperationTask)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingWorkCenter").equals(reader.getName())){
                                
                                                object.setManufacturingWorkCenter(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","manufacturingCostTemplate").equals(reader.getName())){
                                
                                                object.setManufacturingCostTemplate(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"title" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","operationSequence").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"operationSequence" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOperationSequence(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOperationSequence(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","workOrder").equals(reader.getName())){
                                
                                                object.setWorkOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","order").equals(reader.getName())){
                                
                                                object.setOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"message" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","estimatedWork").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estimatedWork" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstimatedWork(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstimatedWork(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","actualWork").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"actualWork" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setActualWork(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setActualWork(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","remainingWork").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"remainingWork" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRemainingWork(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRemainingWork(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","inputQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"inputQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInputQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setInputQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","completedQuantity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"completedQuantity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCompletedQuantity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCompletedQuantity(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","setupTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"setupTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSetupTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSetupTime(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","runRate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"runRate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRunRate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRunRate(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","autoCalculateLag").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"autoCalculateLag" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAutoCalculateLag(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","machineResources").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"machineResources" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMachineResources(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMachineResources(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","laborResources").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"laborResources" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLaborResources(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setLaborResources(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","costDetailList").equals(reader.getName())){
                                
                                                object.setCostDetailList(com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetailList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","predecessorList").equals(reader.getName())){
                                
                                                object.setPredecessorList(com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessorList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:supplychain_2017_2.lists.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    