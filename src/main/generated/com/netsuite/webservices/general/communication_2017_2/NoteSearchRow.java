
/**
 * NoteSearchRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.general.communication_2017_2;
            

            /**
            *  NoteSearchRow bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class NoteSearchRow extends com.netsuite.webservices.platform.core_2017_2.SearchRow
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = NoteSearchRow
                Namespace URI = urn:communication_2017_2.general.webservices.netsuite.com
                Namespace Prefix = ns13
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for AuthorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localAuthorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAuthorJoinTracker = false ;

                           public boolean isAuthorJoinSpecified(){
                               return localAuthorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getAuthorJoin(){
                               return localAuthorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AuthorJoin
                               */
                               public void setAuthorJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localAuthorJoinTracker = param != null;
                                   
                                            this.localAuthorJoin=param;
                                    

                               }
                            

                        /**
                        * field for CallJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic localCallJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCallJoinTracker = false ;

                           public boolean isCallJoinSpecified(){
                               return localCallJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic getCallJoin(){
                               return localCallJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CallJoin
                               */
                               public void setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic param){
                            localCallJoinTracker = param != null;
                                   
                                            this.localCallJoin=param;
                                    

                               }
                            

                        /**
                        * field for CampaignJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic localCampaignJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCampaignJoinTracker = false ;

                           public boolean isCampaignJoinSpecified(){
                               return localCampaignJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic getCampaignJoin(){
                               return localCampaignJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CampaignJoin
                               */
                               public void setCampaignJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic param){
                            localCampaignJoinTracker = param != null;
                                   
                                            this.localCampaignJoin=param;
                                    

                               }
                            

                        /**
                        * field for CaseJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic localCaseJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseJoinTracker = false ;

                           public boolean isCaseJoinSpecified(){
                               return localCaseJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic getCaseJoin(){
                               return localCaseJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseJoin
                               */
                               public void setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic param){
                            localCaseJoinTracker = param != null;
                                   
                                            this.localCaseJoin=param;
                                    

                               }
                            

                        /**
                        * field for ContactJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic localContactJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactJoinTracker = false ;

                           public boolean isContactJoinSpecified(){
                               return localContactJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic getContactJoin(){
                               return localContactJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ContactJoin
                               */
                               public void setContactJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic param){
                            localContactJoinTracker = param != null;
                                   
                                            this.localContactJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic localCustomerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerJoinTracker = false ;

                           public boolean isCustomerJoinSpecified(){
                               return localCustomerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic getCustomerJoin(){
                               return localCustomerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerJoin
                               */
                               public void setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic param){
                            localCustomerJoinTracker = param != null;
                                   
                                            this.localCustomerJoin=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localEmployeeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeJoinTracker = false ;

                           public boolean isEmployeeJoinSpecified(){
                               return localEmployeeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getEmployeeJoin(){
                               return localEmployeeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeJoin
                               */
                               public void setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localEmployeeJoinTracker = param != null;
                                   
                                            this.localEmployeeJoin=param;
                                    

                               }
                            

                        /**
                        * field for EntityJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EntitySearchRowBasic localEntityJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityJoinTracker = false ;

                           public boolean isEntityJoinSpecified(){
                               return localEntityJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EntitySearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EntitySearchRowBasic getEntityJoin(){
                               return localEntityJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityJoin
                               */
                               public void setEntityJoin(com.netsuite.webservices.platform.common_2017_2.EntitySearchRowBasic param){
                            localEntityJoinTracker = param != null;
                                   
                                            this.localEntityJoin=param;
                                    

                               }
                            

                        /**
                        * field for EventJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic localEventJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventJoinTracker = false ;

                           public boolean isEventJoinSpecified(){
                               return localEventJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic getEventJoin(){
                               return localEventJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventJoin
                               */
                               public void setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic param){
                            localEventJoinTracker = param != null;
                                   
                                            this.localEventJoin=param;
                                    

                               }
                            

                        /**
                        * field for IssueJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.IssueSearchRowBasic localIssueJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueJoinTracker = false ;

                           public boolean isIssueJoinSpecified(){
                               return localIssueJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.IssueSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.IssueSearchRowBasic getIssueJoin(){
                               return localIssueJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueJoin
                               */
                               public void setIssueJoin(com.netsuite.webservices.platform.common_2017_2.IssueSearchRowBasic param){
                            localIssueJoinTracker = param != null;
                                   
                                            this.localIssueJoin=param;
                                    

                               }
                            

                        /**
                        * field for ItemJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic localItemJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemJoinTracker = false ;

                           public boolean isItemJoinSpecified(){
                               return localItemJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic getItemJoin(){
                               return localItemJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemJoin
                               */
                               public void setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic param){
                            localItemJoinTracker = param != null;
                                   
                                            this.localItemJoin=param;
                                    

                               }
                            

                        /**
                        * field for OpportunityJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic localOpportunityJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOpportunityJoinTracker = false ;

                           public boolean isOpportunityJoinSpecified(){
                               return localOpportunityJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic getOpportunityJoin(){
                               return localOpportunityJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OpportunityJoin
                               */
                               public void setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic param){
                            localOpportunityJoinTracker = param != null;
                                   
                                            this.localOpportunityJoin=param;
                                    

                               }
                            

                        /**
                        * field for OriginatingLeadJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic localOriginatingLeadJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOriginatingLeadJoinTracker = false ;

                           public boolean isOriginatingLeadJoinSpecified(){
                               return localOriginatingLeadJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic getOriginatingLeadJoin(){
                               return localOriginatingLeadJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OriginatingLeadJoin
                               */
                               public void setOriginatingLeadJoin(com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic param){
                            localOriginatingLeadJoinTracker = param != null;
                                   
                                            this.localOriginatingLeadJoin=param;
                                    

                               }
                            

                        /**
                        * field for PartnerJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic localPartnerJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerJoinTracker = false ;

                           public boolean isPartnerJoinSpecified(){
                               return localPartnerJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic getPartnerJoin(){
                               return localPartnerJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerJoin
                               */
                               public void setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic param){
                            localPartnerJoinTracker = param != null;
                                   
                                            this.localPartnerJoin=param;
                                    

                               }
                            

                        /**
                        * field for SolutionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.SolutionSearchRowBasic localSolutionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSolutionJoinTracker = false ;

                           public boolean isSolutionJoinSpecified(){
                               return localSolutionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.SolutionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.SolutionSearchRowBasic getSolutionJoin(){
                               return localSolutionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SolutionJoin
                               */
                               public void setSolutionJoin(com.netsuite.webservices.platform.common_2017_2.SolutionSearchRowBasic param){
                            localSolutionJoinTracker = param != null;
                                   
                                            this.localSolutionJoin=param;
                                    

                               }
                            

                        /**
                        * field for TaskJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic localTaskJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaskJoinTracker = false ;

                           public boolean isTaskJoinSpecified(){
                               return localTaskJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic getTaskJoin(){
                               return localTaskJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaskJoin
                               */
                               public void setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic param){
                            localTaskJoinTracker = param != null;
                                   
                                            this.localTaskJoin=param;
                                    

                               }
                            

                        /**
                        * field for TransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic localTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionJoinTracker = false ;

                           public boolean isTransactionJoinSpecified(){
                               return localTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic getTransactionJoin(){
                               return localTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionJoin
                               */
                               public void setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic param){
                            localTransactionJoinTracker = param != null;
                                   
                                            this.localTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for VendorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic localVendorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorJoinTracker = false ;

                           public boolean isVendorJoinSpecified(){
                               return localVendorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic getVendorJoin(){
                               return localVendorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VendorJoin
                               */
                               public void setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic param){
                            localVendorJoinTracker = param != null;
                                   
                                            this.localVendorJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:communication_2017_2.general.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":NoteSearchRow",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "NoteSearchRow",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localAuthorJoinTracker){
                                            if (localAuthorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("authorJoin cannot be null!!");
                                            }
                                           localAuthorJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","authorJoin"),
                                               xmlWriter);
                                        } if (localCallJoinTracker){
                                            if (localCallJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                            }
                                           localCallJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","callJoin"),
                                               xmlWriter);
                                        } if (localCampaignJoinTracker){
                                            if (localCampaignJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("campaignJoin cannot be null!!");
                                            }
                                           localCampaignJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","campaignJoin"),
                                               xmlWriter);
                                        } if (localCaseJoinTracker){
                                            if (localCaseJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                            }
                                           localCaseJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","caseJoin"),
                                               xmlWriter);
                                        } if (localContactJoinTracker){
                                            if (localContactJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contactJoin cannot be null!!");
                                            }
                                           localContactJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","contactJoin"),
                                               xmlWriter);
                                        } if (localCustomerJoinTracker){
                                            if (localCustomerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                            }
                                           localCustomerJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","customerJoin"),
                                               xmlWriter);
                                        } if (localEmployeeJoinTracker){
                                            if (localEmployeeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                            }
                                           localEmployeeJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","employeeJoin"),
                                               xmlWriter);
                                        } if (localEntityJoinTracker){
                                            if (localEntityJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityJoin cannot be null!!");
                                            }
                                           localEntityJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","entityJoin"),
                                               xmlWriter);
                                        } if (localEventJoinTracker){
                                            if (localEventJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                            }
                                           localEventJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","eventJoin"),
                                               xmlWriter);
                                        } if (localIssueJoinTracker){
                                            if (localIssueJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueJoin cannot be null!!");
                                            }
                                           localIssueJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","issueJoin"),
                                               xmlWriter);
                                        } if (localItemJoinTracker){
                                            if (localItemJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                            }
                                           localItemJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","itemJoin"),
                                               xmlWriter);
                                        } if (localOpportunityJoinTracker){
                                            if (localOpportunityJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                            }
                                           localOpportunityJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","opportunityJoin"),
                                               xmlWriter);
                                        } if (localOriginatingLeadJoinTracker){
                                            if (localOriginatingLeadJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("originatingLeadJoin cannot be null!!");
                                            }
                                           localOriginatingLeadJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","originatingLeadJoin"),
                                               xmlWriter);
                                        } if (localPartnerJoinTracker){
                                            if (localPartnerJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                            }
                                           localPartnerJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","partnerJoin"),
                                               xmlWriter);
                                        } if (localSolutionJoinTracker){
                                            if (localSolutionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("solutionJoin cannot be null!!");
                                            }
                                           localSolutionJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","solutionJoin"),
                                               xmlWriter);
                                        } if (localTaskJoinTracker){
                                            if (localTaskJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                            }
                                           localTaskJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","taskJoin"),
                                               xmlWriter);
                                        } if (localTransactionJoinTracker){
                                            if (localTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                            }
                                           localTransactionJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","transactionJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localVendorJoinTracker){
                                            if (localVendorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                            }
                                           localVendorJoin.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","vendorJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:communication_2017_2.general.webservices.netsuite.com")){
                return "ns13";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","NoteSearchRow"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localAuthorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "authorJoin"));
                            
                            
                                    if (localAuthorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("authorJoin cannot be null!!");
                                    }
                                    elementList.add(localAuthorJoin);
                                } if (localCallJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "callJoin"));
                            
                            
                                    if (localCallJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("callJoin cannot be null!!");
                                    }
                                    elementList.add(localCallJoin);
                                } if (localCampaignJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "campaignJoin"));
                            
                            
                                    if (localCampaignJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("campaignJoin cannot be null!!");
                                    }
                                    elementList.add(localCampaignJoin);
                                } if (localCaseJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "caseJoin"));
                            
                            
                                    if (localCaseJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseJoin cannot be null!!");
                                    }
                                    elementList.add(localCaseJoin);
                                } if (localContactJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "contactJoin"));
                            
                            
                                    if (localContactJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("contactJoin cannot be null!!");
                                    }
                                    elementList.add(localContactJoin);
                                } if (localCustomerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "customerJoin"));
                            
                            
                                    if (localCustomerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("customerJoin cannot be null!!");
                                    }
                                    elementList.add(localCustomerJoin);
                                } if (localEmployeeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "employeeJoin"));
                            
                            
                                    if (localEmployeeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeJoin cannot be null!!");
                                    }
                                    elementList.add(localEmployeeJoin);
                                } if (localEntityJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "entityJoin"));
                            
                            
                                    if (localEntityJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityJoin cannot be null!!");
                                    }
                                    elementList.add(localEntityJoin);
                                } if (localEventJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "eventJoin"));
                            
                            
                                    if (localEventJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventJoin cannot be null!!");
                                    }
                                    elementList.add(localEventJoin);
                                } if (localIssueJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "issueJoin"));
                            
                            
                                    if (localIssueJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueJoin cannot be null!!");
                                    }
                                    elementList.add(localIssueJoin);
                                } if (localItemJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "itemJoin"));
                            
                            
                                    if (localItemJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemJoin cannot be null!!");
                                    }
                                    elementList.add(localItemJoin);
                                } if (localOpportunityJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "opportunityJoin"));
                            
                            
                                    if (localOpportunityJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("opportunityJoin cannot be null!!");
                                    }
                                    elementList.add(localOpportunityJoin);
                                } if (localOriginatingLeadJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "originatingLeadJoin"));
                            
                            
                                    if (localOriginatingLeadJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("originatingLeadJoin cannot be null!!");
                                    }
                                    elementList.add(localOriginatingLeadJoin);
                                } if (localPartnerJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "partnerJoin"));
                            
                            
                                    if (localPartnerJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerJoin cannot be null!!");
                                    }
                                    elementList.add(localPartnerJoin);
                                } if (localSolutionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "solutionJoin"));
                            
                            
                                    if (localSolutionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("solutionJoin cannot be null!!");
                                    }
                                    elementList.add(localSolutionJoin);
                                } if (localTaskJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "taskJoin"));
                            
                            
                                    if (localTaskJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("taskJoin cannot be null!!");
                                    }
                                    elementList.add(localTaskJoin);
                                } if (localTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "transactionJoin"));
                            
                            
                                    if (localTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                    }
                                    elementList.add(localTransactionJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localVendorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "vendorJoin"));
                            
                            
                                    if (localVendorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendorJoin cannot be null!!");
                                    }
                                    elementList.add(localVendorJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static NoteSearchRow parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            NoteSearchRow object =
                new NoteSearchRow();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"NoteSearchRow".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (NoteSearchRow)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list21 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","authorJoin").equals(reader.getName())){
                                
                                                object.setAuthorJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","callJoin").equals(reader.getName())){
                                
                                                object.setCallJoin(com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","campaignJoin").equals(reader.getName())){
                                
                                                object.setCampaignJoin(com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","caseJoin").equals(reader.getName())){
                                
                                                object.setCaseJoin(com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","contactJoin").equals(reader.getName())){
                                
                                                object.setContactJoin(com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","customerJoin").equals(reader.getName())){
                                
                                                object.setCustomerJoin(com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","employeeJoin").equals(reader.getName())){
                                
                                                object.setEmployeeJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","entityJoin").equals(reader.getName())){
                                
                                                object.setEntityJoin(com.netsuite.webservices.platform.common_2017_2.EntitySearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","eventJoin").equals(reader.getName())){
                                
                                                object.setEventJoin(com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","issueJoin").equals(reader.getName())){
                                
                                                object.setIssueJoin(com.netsuite.webservices.platform.common_2017_2.IssueSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","itemJoin").equals(reader.getName())){
                                
                                                object.setItemJoin(com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","opportunityJoin").equals(reader.getName())){
                                
                                                object.setOpportunityJoin(com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","originatingLeadJoin").equals(reader.getName())){
                                
                                                object.setOriginatingLeadJoin(com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","partnerJoin").equals(reader.getName())){
                                
                                                object.setPartnerJoin(com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","solutionJoin").equals(reader.getName())){
                                
                                                object.setSolutionJoin(com.netsuite.webservices.platform.common_2017_2.SolutionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","taskJoin").equals(reader.getName())){
                                
                                                object.setTaskJoin(com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","transactionJoin").equals(reader.getName())){
                                
                                                object.setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","vendorJoin").equals(reader.getName())){
                                
                                                object.setVendorJoin(com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list21.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone21 = false;
                                                        while(!loopDone21){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone21 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list21.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone21 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.class,
                                                                list21));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    