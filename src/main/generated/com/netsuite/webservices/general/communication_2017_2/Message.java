
/**
 * Message.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.general.communication_2017_2;
            

            /**
            *  Message bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Message extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Message
                Namespace URI = urn:communication_2017_2.general.webservices.netsuite.com
                Namespace Prefix = ns13
                */
            

                        /**
                        * field for Author
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAuthor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAuthorTracker = false ;

                           public boolean isAuthorSpecified(){
                               return localAuthorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAuthor(){
                               return localAuthor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Author
                               */
                               public void setAuthor(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAuthorTracker = param != null;
                                   
                                            this.localAuthor=param;
                                    

                               }
                            

                        /**
                        * field for AuthorEmail
                        */

                        
                                    protected java.lang.String localAuthorEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAuthorEmailTracker = false ;

                           public boolean isAuthorEmailSpecified(){
                               return localAuthorEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAuthorEmail(){
                               return localAuthorEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AuthorEmail
                               */
                               public void setAuthorEmail(java.lang.String param){
                            localAuthorEmailTracker = param != null;
                                   
                                            this.localAuthorEmail=param;
                                    

                               }
                            

                        /**
                        * field for Recipient
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRecipient ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecipientTracker = false ;

                           public boolean isRecipientSpecified(){
                               return localRecipientTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRecipient(){
                               return localRecipient;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Recipient
                               */
                               public void setRecipient(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRecipientTracker = param != null;
                                   
                                            this.localRecipient=param;
                                    

                               }
                            

                        /**
                        * field for RecipientEmail
                        */

                        
                                    protected java.lang.String localRecipientEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecipientEmailTracker = false ;

                           public boolean isRecipientEmailSpecified(){
                               return localRecipientEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRecipientEmail(){
                               return localRecipientEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecipientEmail
                               */
                               public void setRecipientEmail(java.lang.String param){
                            localRecipientEmailTracker = param != null;
                                   
                                            this.localRecipientEmail=param;
                                    

                               }
                            

                        /**
                        * field for Cc
                        */

                        
                                    protected java.lang.String localCc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcTracker = false ;

                           public boolean isCcSpecified(){
                               return localCcTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCc(){
                               return localCc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cc
                               */
                               public void setCc(java.lang.String param){
                            localCcTracker = param != null;
                                   
                                            this.localCc=param;
                                    

                               }
                            

                        /**
                        * field for Bcc
                        */

                        
                                    protected java.lang.String localBcc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBccTracker = false ;

                           public boolean isBccSpecified(){
                               return localBccTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBcc(){
                               return localBcc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bcc
                               */
                               public void setBcc(java.lang.String param){
                            localBccTracker = param != null;
                                   
                                            this.localBcc=param;
                                    

                               }
                            

                        /**
                        * field for MessageDate
                        */

                        
                                    protected java.util.Calendar localMessageDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageDateTracker = false ;

                           public boolean isMessageDateSpecified(){
                               return localMessageDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getMessageDate(){
                               return localMessageDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MessageDate
                               */
                               public void setMessageDate(java.util.Calendar param){
                            localMessageDateTracker = param != null;
                                   
                                            this.localMessageDate=param;
                                    

                               }
                            

                        /**
                        * field for RecordName
                        */

                        
                                    protected java.lang.String localRecordName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecordNameTracker = false ;

                           public boolean isRecordNameSpecified(){
                               return localRecordNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRecordName(){
                               return localRecordName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecordName
                               */
                               public void setRecordName(java.lang.String param){
                            localRecordNameTracker = param != null;
                                   
                                            this.localRecordName=param;
                                    

                               }
                            

                        /**
                        * field for RecordTypeName
                        */

                        
                                    protected java.lang.String localRecordTypeName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecordTypeNameTracker = false ;

                           public boolean isRecordTypeNameSpecified(){
                               return localRecordTypeNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRecordTypeName(){
                               return localRecordTypeName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecordTypeName
                               */
                               public void setRecordTypeName(java.lang.String param){
                            localRecordTypeNameTracker = param != null;
                                   
                                            this.localRecordTypeName=param;
                                    

                               }
                            

                        /**
                        * field for Subject
                        */

                        
                                    protected java.lang.String localSubject ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubjectTracker = false ;

                           public boolean isSubjectSpecified(){
                               return localSubjectTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSubject(){
                               return localSubject;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subject
                               */
                               public void setSubject(java.lang.String param){
                            localSubjectTracker = param != null;
                                   
                                            this.localSubject=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected java.lang.String localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(java.lang.String param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for Emailed
                        */

                        
                                    protected boolean localEmailed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailedTracker = false ;

                           public boolean isEmailedSpecified(){
                               return localEmailedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEmailed(){
                               return localEmailed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Emailed
                               */
                               public void setEmailed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEmailedTracker =
                                       true;
                                   
                                            this.localEmailed=param;
                                    

                               }
                            

                        /**
                        * field for Activity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localActivity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActivityTracker = false ;

                           public boolean isActivitySpecified(){
                               return localActivityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getActivity(){
                               return localActivity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Activity
                               */
                               public void setActivity(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localActivityTracker = param != null;
                                   
                                            this.localActivity=param;
                                    

                               }
                            

                        /**
                        * field for CompressAttachments
                        */

                        
                                    protected boolean localCompressAttachments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompressAttachmentsTracker = false ;

                           public boolean isCompressAttachmentsSpecified(){
                               return localCompressAttachmentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCompressAttachments(){
                               return localCompressAttachments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompressAttachments
                               */
                               public void setCompressAttachments(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localCompressAttachmentsTracker =
                                       true;
                                   
                                            this.localCompressAttachments=param;
                                    

                               }
                            

                        /**
                        * field for Incoming
                        */

                        
                                    protected boolean localIncoming ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncomingTracker = false ;

                           public boolean isIncomingSpecified(){
                               return localIncomingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIncoming(){
                               return localIncoming;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Incoming
                               */
                               public void setIncoming(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIncomingTracker =
                                       true;
                                   
                                            this.localIncoming=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for Transaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionTracker = false ;

                           public boolean isTransactionSpecified(){
                               return localTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTransaction(){
                               return localTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Transaction
                               */
                               public void setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTransactionTracker = param != null;
                                   
                                            this.localTransaction=param;
                                    

                               }
                            

                        /**
                        * field for MediaItemList
                        */

                        
                                    protected com.netsuite.webservices.general.communication_2017_2.MessageMediaItemList localMediaItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMediaItemListTracker = false ;

                           public boolean isMediaItemListSpecified(){
                               return localMediaItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.general.communication_2017_2.MessageMediaItemList
                           */
                           public  com.netsuite.webservices.general.communication_2017_2.MessageMediaItemList getMediaItemList(){
                               return localMediaItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MediaItemList
                               */
                               public void setMediaItemList(com.netsuite.webservices.general.communication_2017_2.MessageMediaItemList param){
                            localMediaItemListTracker = param != null;
                                   
                                            this.localMediaItemList=param;
                                    

                               }
                            

                        /**
                        * field for DateTime
                        */

                        
                                    protected java.lang.String localDateTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateTimeTracker = false ;

                           public boolean isDateTimeSpecified(){
                               return localDateTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDateTime(){
                               return localDateTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateTime
                               */
                               public void setDateTime(java.lang.String param){
                            localDateTimeTracker = param != null;
                                   
                                            this.localDateTime=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:communication_2017_2.general.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Message",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Message",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localAuthorTracker){
                                            if (localAuthor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("author cannot be null!!");
                                            }
                                           localAuthor.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","author"),
                                               xmlWriter);
                                        } if (localAuthorEmailTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "authorEmail", xmlWriter);
                             

                                          if (localAuthorEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("authorEmail cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAuthorEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecipientTracker){
                                            if (localRecipient==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recipient cannot be null!!");
                                            }
                                           localRecipient.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","recipient"),
                                               xmlWriter);
                                        } if (localRecipientEmailTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recipientEmail", xmlWriter);
                             

                                          if (localRecipientEmail==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("recipientEmail cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRecipientEmail);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCcTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "cc", xmlWriter);
                             

                                          if (localCc==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("cc cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCc);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBccTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "bcc", xmlWriter);
                             

                                          if (localBcc==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("bcc cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBcc);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMessageDateTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "messageDate", xmlWriter);
                             

                                          if (localMessageDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("messageDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessageDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecordNameTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recordName", xmlWriter);
                             

                                          if (localRecordName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("recordName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRecordName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecordTypeNameTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recordTypeName", xmlWriter);
                             

                                          if (localRecordTypeName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("recordTypeName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRecordTypeName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSubjectTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "subject", xmlWriter);
                             

                                          if (localSubject==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("subject cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSubject);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMessageTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "message", xmlWriter);
                             

                                          if (localMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEmailedTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "emailed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("emailed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localActivityTracker){
                                            if (localActivity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("activity cannot be null!!");
                                            }
                                           localActivity.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","activity"),
                                               xmlWriter);
                                        } if (localCompressAttachmentsTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "compressAttachments", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("compressAttachments cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCompressAttachments));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIncomingTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "incoming", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("incoming cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncoming));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedDateTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTransactionTracker){
                                            if (localTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                            }
                                           localTransaction.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","transaction"),
                                               xmlWriter);
                                        } if (localMediaItemListTracker){
                                            if (localMediaItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("mediaItemList cannot be null!!");
                                            }
                                           localMediaItemList.serialize(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","mediaItemList"),
                                               xmlWriter);
                                        } if (localDateTimeTracker){
                                    namespace = "urn:communication_2017_2.general.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "dateTime", xmlWriter);
                             

                                          if (localDateTime==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("dateTime cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDateTime);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:communication_2017_2.general.webservices.netsuite.com")){
                return "ns13";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","Message"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localAuthorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "author"));
                            
                            
                                    if (localAuthor==null){
                                         throw new org.apache.axis2.databinding.ADBException("author cannot be null!!");
                                    }
                                    elementList.add(localAuthor);
                                } if (localAuthorEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "authorEmail"));
                                 
                                        if (localAuthorEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAuthorEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("authorEmail cannot be null!!");
                                        }
                                    } if (localRecipientTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "recipient"));
                            
                            
                                    if (localRecipient==null){
                                         throw new org.apache.axis2.databinding.ADBException("recipient cannot be null!!");
                                    }
                                    elementList.add(localRecipient);
                                } if (localRecipientEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "recipientEmail"));
                                 
                                        if (localRecipientEmail != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecipientEmail));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("recipientEmail cannot be null!!");
                                        }
                                    } if (localCcTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "cc"));
                                 
                                        if (localCc != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCc));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("cc cannot be null!!");
                                        }
                                    } if (localBccTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "bcc"));
                                 
                                        if (localBcc != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBcc));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("bcc cannot be null!!");
                                        }
                                    } if (localMessageDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "messageDate"));
                                 
                                        if (localMessageDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessageDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("messageDate cannot be null!!");
                                        }
                                    } if (localRecordNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "recordName"));
                                 
                                        if (localRecordName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("recordName cannot be null!!");
                                        }
                                    } if (localRecordTypeNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "recordTypeName"));
                                 
                                        if (localRecordTypeName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordTypeName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("recordTypeName cannot be null!!");
                                        }
                                    } if (localSubjectTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "subject"));
                                 
                                        if (localSubject != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSubject));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("subject cannot be null!!");
                                        }
                                    } if (localMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "message"));
                                 
                                        if (localMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                        }
                                    } if (localEmailedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "emailed"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEmailed));
                            } if (localActivityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "activity"));
                            
                            
                                    if (localActivity==null){
                                         throw new org.apache.axis2.databinding.ADBException("activity cannot be null!!");
                                    }
                                    elementList.add(localActivity);
                                } if (localCompressAttachmentsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "compressAttachments"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCompressAttachments));
                            } if (localIncomingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "incoming"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncoming));
                            } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "transaction"));
                            
                            
                                    if (localTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                    }
                                    elementList.add(localTransaction);
                                } if (localMediaItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "mediaItemList"));
                            
                            
                                    if (localMediaItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("mediaItemList cannot be null!!");
                                    }
                                    elementList.add(localMediaItemList);
                                } if (localDateTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com",
                                                                      "dateTime"));
                                 
                                        if (localDateTime != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDateTime));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("dateTime cannot be null!!");
                                        }
                                    }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Message parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Message object =
                new Message();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Message".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Message)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","author").equals(reader.getName())){
                                
                                                object.setAuthor(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","authorEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"authorEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAuthorEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","recipient").equals(reader.getName())){
                                
                                                object.setRecipient(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","recipientEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recipientEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecipientEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","cc").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"cc" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","bcc").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"bcc" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBcc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","messageDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"messageDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessageDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","recordName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recordName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecordName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","recordTypeName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recordTypeName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecordTypeName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","subject").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"subject" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSubject(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"message" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","emailed").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"emailed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEmailed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","activity").equals(reader.getName())){
                                
                                                object.setActivity(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","compressAttachments").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"compressAttachments" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCompressAttachments(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","incoming").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"incoming" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncoming(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","transaction").equals(reader.getName())){
                                
                                                object.setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","mediaItemList").equals(reader.getName())){
                                
                                                object.setMediaItemList(com.netsuite.webservices.general.communication_2017_2.MessageMediaItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:communication_2017_2.general.webservices.netsuite.com","dateTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"dateTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDateTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    