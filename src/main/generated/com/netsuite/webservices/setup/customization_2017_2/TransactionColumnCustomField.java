
/**
 * TransactionColumnCustomField.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.setup.customization_2017_2;
            

            /**
            *  TransactionColumnCustomField bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TransactionColumnCustomField extends com.netsuite.webservices.setup.customization_2017_2.CustomFieldType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TransactionColumnCustomField
                Namespace URI = urn:customization_2017_2.setup.webservices.netsuite.com
                Namespace Prefix = ns33
                */
            

                        /**
                        * field for Label
                        */

                        
                                    protected java.lang.String localLabel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLabelTracker = false ;

                           public boolean isLabelSpecified(){
                               return localLabelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLabel(){
                               return localLabel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Label
                               */
                               public void setLabel(java.lang.String param){
                            localLabelTracker = param != null;
                                   
                                            this.localLabel=param;
                                    

                               }
                            

                        /**
                        * field for Owner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOwnerTracker = false ;

                           public boolean isOwnerSpecified(){
                               return localOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOwner(){
                               return localOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Owner
                               */
                               public void setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOwnerTracker = param != null;
                                   
                                            this.localOwner=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for SelectRecordType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSelectRecordType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSelectRecordTypeTracker = false ;

                           public boolean isSelectRecordTypeSpecified(){
                               return localSelectRecordTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSelectRecordType(){
                               return localSelectRecordType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SelectRecordType
                               */
                               public void setSelectRecordType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSelectRecordTypeTracker = param != null;
                                   
                                            this.localSelectRecordType=param;
                                    

                               }
                            

                        /**
                        * field for StoreValue
                        */

                        
                                    protected boolean localStoreValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStoreValueTracker = false ;

                           public boolean isStoreValueSpecified(){
                               return localStoreValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getStoreValue(){
                               return localStoreValue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StoreValue
                               */
                               public void setStoreValue(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localStoreValueTracker =
                                       true;
                                   
                                            this.localStoreValue=param;
                                    

                               }
                            

                        /**
                        * field for InsertBefore
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localInsertBefore ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInsertBeforeTracker = false ;

                           public boolean isInsertBeforeSpecified(){
                               return localInsertBeforeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getInsertBefore(){
                               return localInsertBefore;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InsertBefore
                               */
                               public void setInsertBefore(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localInsertBeforeTracker = param != null;
                                   
                                            this.localInsertBefore=param;
                                    

                               }
                            

                        /**
                        * field for AvailableToSso
                        */

                        
                                    protected boolean localAvailableToSso ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAvailableToSsoTracker = false ;

                           public boolean isAvailableToSsoSpecified(){
                               return localAvailableToSsoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAvailableToSso(){
                               return localAvailableToSso;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AvailableToSso
                               */
                               public void setAvailableToSso(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAvailableToSsoTracker =
                                       true;
                                   
                                            this.localAvailableToSso=param;
                                    

                               }
                            

                        /**
                        * field for DisplayType
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDisplayType localDisplayType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDisplayTypeTracker = false ;

                           public boolean isDisplayTypeSpecified(){
                               return localDisplayTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDisplayType
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDisplayType getDisplayType(){
                               return localDisplayType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DisplayType
                               */
                               public void setDisplayType(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDisplayType param){
                            localDisplayTypeTracker = param != null;
                                   
                                            this.localDisplayType=param;
                                    

                               }
                            

                        /**
                        * field for DisplayWidth
                        */

                        
                                    protected long localDisplayWidth ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDisplayWidthTracker = false ;

                           public boolean isDisplayWidthSpecified(){
                               return localDisplayWidthTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getDisplayWidth(){
                               return localDisplayWidth;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DisplayWidth
                               */
                               public void setDisplayWidth(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localDisplayWidthTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localDisplayWidth=param;
                                    

                               }
                            

                        /**
                        * field for DisplayHeight
                        */

                        
                                    protected long localDisplayHeight ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDisplayHeightTracker = false ;

                           public boolean isDisplayHeightSpecified(){
                               return localDisplayHeightTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getDisplayHeight(){
                               return localDisplayHeight;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DisplayHeight
                               */
                               public void setDisplayHeight(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localDisplayHeightTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localDisplayHeight=param;
                                    

                               }
                            

                        /**
                        * field for Help
                        */

                        
                                    protected java.lang.String localHelp ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHelpTracker = false ;

                           public boolean isHelpSpecified(){
                               return localHelpTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getHelp(){
                               return localHelp;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Help
                               */
                               public void setHelp(java.lang.String param){
                            localHelpTracker = param != null;
                                   
                                            this.localHelp=param;
                                    

                               }
                            

                        /**
                        * field for LinkText
                        */

                        
                                    protected java.lang.String localLinkText ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLinkTextTracker = false ;

                           public boolean isLinkTextSpecified(){
                               return localLinkTextTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLinkText(){
                               return localLinkText;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LinkText
                               */
                               public void setLinkText(java.lang.String param){
                            localLinkTextTracker = param != null;
                                   
                                            this.localLinkText=param;
                                    

                               }
                            

                        /**
                        * field for IsMandatory
                        */

                        
                                    protected boolean localIsMandatory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsMandatoryTracker = false ;

                           public boolean isIsMandatorySpecified(){
                               return localIsMandatoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsMandatory(){
                               return localIsMandatory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsMandatory
                               */
                               public void setIsMandatory(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsMandatoryTracker =
                                       true;
                                   
                                            this.localIsMandatory=param;
                                    

                               }
                            

                        /**
                        * field for MaxLength
                        */

                        
                                    protected long localMaxLength ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMaxLengthTracker = false ;

                           public boolean isMaxLengthSpecified(){
                               return localMaxLengthTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getMaxLength(){
                               return localMaxLength;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MaxLength
                               */
                               public void setMaxLength(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localMaxLengthTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localMaxLength=param;
                                    

                               }
                            

                        /**
                        * field for MinValue
                        */

                        
                                    protected double localMinValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMinValueTracker = false ;

                           public boolean isMinValueSpecified(){
                               return localMinValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getMinValue(){
                               return localMinValue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MinValue
                               */
                               public void setMinValue(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localMinValueTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localMinValue=param;
                                    

                               }
                            

                        /**
                        * field for MaxValue
                        */

                        
                                    protected double localMaxValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMaxValueTracker = false ;

                           public boolean isMaxValueSpecified(){
                               return localMaxValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getMaxValue(){
                               return localMaxValue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MaxValue
                               */
                               public void setMaxValue(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localMaxValueTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localMaxValue=param;
                                    

                               }
                            

                        /**
                        * field for DefaultChecked
                        */

                        
                                    protected boolean localDefaultChecked ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultCheckedTracker = false ;

                           public boolean isDefaultCheckedSpecified(){
                               return localDefaultCheckedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDefaultChecked(){
                               return localDefaultChecked;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultChecked
                               */
                               public void setDefaultChecked(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localDefaultCheckedTracker =
                                       true;
                                   
                                            this.localDefaultChecked=param;
                                    

                               }
                            

                        /**
                        * field for DefaultValue
                        */

                        
                                    protected java.lang.String localDefaultValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultValueTracker = false ;

                           public boolean isDefaultValueSpecified(){
                               return localDefaultValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDefaultValue(){
                               return localDefaultValue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultValue
                               */
                               public void setDefaultValue(java.lang.String param){
                            localDefaultValueTracker = param != null;
                                   
                                            this.localDefaultValue=param;
                                    

                               }
                            

                        /**
                        * field for IsFormula
                        */

                        
                                    protected boolean localIsFormula ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsFormulaTracker = false ;

                           public boolean isIsFormulaSpecified(){
                               return localIsFormulaTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsFormula(){
                               return localIsFormula;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsFormula
                               */
                               public void setIsFormula(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsFormulaTracker =
                                       true;
                                   
                                            this.localIsFormula=param;
                                    

                               }
                            

                        /**
                        * field for DefaultSelection
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDefaultSelection ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultSelectionTracker = false ;

                           public boolean isDefaultSelectionSpecified(){
                               return localDefaultSelectionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDefaultSelection(){
                               return localDefaultSelection;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultSelection
                               */
                               public void setDefaultSelection(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDefaultSelectionTracker = param != null;
                                   
                                            this.localDefaultSelection=param;
                                    

                               }
                            

                        /**
                        * field for DynamicDefault
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDynamicDefault localDynamicDefault ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDynamicDefaultTracker = false ;

                           public boolean isDynamicDefaultSpecified(){
                               return localDynamicDefaultTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDynamicDefault
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDynamicDefault getDynamicDefault(){
                               return localDynamicDefault;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DynamicDefault
                               */
                               public void setDynamicDefault(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDynamicDefault param){
                            localDynamicDefaultTracker = param != null;
                                   
                                            this.localDynamicDefault=param;
                                    

                               }
                            

                        /**
                        * field for SourceList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSourceList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceListTracker = false ;

                           public boolean isSourceListSpecified(){
                               return localSourceListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSourceList(){
                               return localSourceList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceList
                               */
                               public void setSourceList(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSourceListTracker = param != null;
                                   
                                            this.localSourceList=param;
                                    

                               }
                            

                        /**
                        * field for SourceFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSourceFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceFromTracker = false ;

                           public boolean isSourceFromSpecified(){
                               return localSourceFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSourceFrom(){
                               return localSourceFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceFrom
                               */
                               public void setSourceFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSourceFromTracker = param != null;
                                   
                                            this.localSourceFrom=param;
                                    

                               }
                            

                        /**
                        * field for SourceFilterBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSourceFilterBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceFilterByTracker = false ;

                           public boolean isSourceFilterBySpecified(){
                               return localSourceFilterByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSourceFilterBy(){
                               return localSourceFilterBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceFilterBy
                               */
                               public void setSourceFilterBy(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSourceFilterByTracker = param != null;
                                   
                                            this.localSourceFilterBy=param;
                                    

                               }
                            

                        /**
                        * field for ColExpense
                        */

                        
                                    protected boolean localColExpense ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColExpenseTracker = false ;

                           public boolean isColExpenseSpecified(){
                               return localColExpenseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColExpense(){
                               return localColExpense;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColExpense
                               */
                               public void setColExpense(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColExpenseTracker =
                                       true;
                                   
                                            this.localColExpense=param;
                                    

                               }
                            

                        /**
                        * field for ColPurchase
                        */

                        
                                    protected boolean localColPurchase ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColPurchaseTracker = false ;

                           public boolean isColPurchaseSpecified(){
                               return localColPurchaseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColPurchase(){
                               return localColPurchase;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColPurchase
                               */
                               public void setColPurchase(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColPurchaseTracker =
                                       true;
                                   
                                            this.localColPurchase=param;
                                    

                               }
                            

                        /**
                        * field for ColSale
                        */

                        
                                    protected boolean localColSale ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColSaleTracker = false ;

                           public boolean isColSaleSpecified(){
                               return localColSaleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColSale(){
                               return localColSale;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColSale
                               */
                               public void setColSale(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColSaleTracker =
                                       true;
                                   
                                            this.localColSale=param;
                                    

                               }
                            

                        /**
                        * field for ColOpportunity
                        */

                        
                                    protected boolean localColOpportunity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColOpportunityTracker = false ;

                           public boolean isColOpportunitySpecified(){
                               return localColOpportunityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColOpportunity(){
                               return localColOpportunity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColOpportunity
                               */
                               public void setColOpportunity(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColOpportunityTracker =
                                       true;
                                   
                                            this.localColOpportunity=param;
                                    

                               }
                            

                        /**
                        * field for ColStore
                        */

                        
                                    protected boolean localColStore ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColStoreTracker = false ;

                           public boolean isColStoreSpecified(){
                               return localColStoreTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColStore(){
                               return localColStore;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColStore
                               */
                               public void setColStore(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColStoreTracker =
                                       true;
                                   
                                            this.localColStore=param;
                                    

                               }
                            

                        /**
                        * field for ColStoreHidden
                        */

                        
                                    protected boolean localColStoreHidden ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColStoreHiddenTracker = false ;

                           public boolean isColStoreHiddenSpecified(){
                               return localColStoreHiddenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColStoreHidden(){
                               return localColStoreHidden;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColStoreHidden
                               */
                               public void setColStoreHidden(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColStoreHiddenTracker =
                                       true;
                                   
                                            this.localColStoreHidden=param;
                                    

                               }
                            

                        /**
                        * field for ColJournal
                        */

                        
                                    protected boolean localColJournal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColJournalTracker = false ;

                           public boolean isColJournalSpecified(){
                               return localColJournalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColJournal(){
                               return localColJournal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColJournal
                               */
                               public void setColJournal(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColJournalTracker =
                                       true;
                                   
                                            this.localColJournal=param;
                                    

                               }
                            

                        /**
                        * field for ColBuild
                        */

                        
                                    protected boolean localColBuild ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColBuildTracker = false ;

                           public boolean isColBuildSpecified(){
                               return localColBuildTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColBuild(){
                               return localColBuild;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColBuild
                               */
                               public void setColBuild(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColBuildTracker =
                                       true;
                                   
                                            this.localColBuild=param;
                                    

                               }
                            

                        /**
                        * field for ColExpenseReport
                        */

                        
                                    protected boolean localColExpenseReport ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColExpenseReportTracker = false ;

                           public boolean isColExpenseReportSpecified(){
                               return localColExpenseReportTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColExpenseReport(){
                               return localColExpenseReport;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColExpenseReport
                               */
                               public void setColExpenseReport(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColExpenseReportTracker =
                                       true;
                                   
                                            this.localColExpenseReport=param;
                                    

                               }
                            

                        /**
                        * field for ColTime
                        */

                        
                                    protected boolean localColTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColTimeTracker = false ;

                           public boolean isColTimeSpecified(){
                               return localColTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColTime(){
                               return localColTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColTime
                               */
                               public void setColTime(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColTimeTracker =
                                       true;
                                   
                                            this.localColTime=param;
                                    

                               }
                            

                        /**
                        * field for ColTransferOrder
                        */

                        
                                    protected boolean localColTransferOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColTransferOrderTracker = false ;

                           public boolean isColTransferOrderSpecified(){
                               return localColTransferOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColTransferOrder(){
                               return localColTransferOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColTransferOrder
                               */
                               public void setColTransferOrder(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColTransferOrderTracker =
                                       true;
                                   
                                            this.localColTransferOrder=param;
                                    

                               }
                            

                        /**
                        * field for ColTimeGroup
                        */

                        
                                    protected boolean localColTimeGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColTimeGroupTracker = false ;

                           public boolean isColTimeGroupSpecified(){
                               return localColTimeGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColTimeGroup(){
                               return localColTimeGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColTimeGroup
                               */
                               public void setColTimeGroup(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColTimeGroupTracker =
                                       true;
                                   
                                            this.localColTimeGroup=param;
                                    

                               }
                            

                        /**
                        * field for ColItemReceipt
                        */

                        
                                    protected boolean localColItemReceipt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColItemReceiptTracker = false ;

                           public boolean isColItemReceiptSpecified(){
                               return localColItemReceiptTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColItemReceipt(){
                               return localColItemReceipt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColItemReceipt
                               */
                               public void setColItemReceipt(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColItemReceiptTracker =
                                       true;
                                   
                                            this.localColItemReceipt=param;
                                    

                               }
                            

                        /**
                        * field for ColItemReceiptOrder
                        */

                        
                                    protected boolean localColItemReceiptOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColItemReceiptOrderTracker = false ;

                           public boolean isColItemReceiptOrderSpecified(){
                               return localColItemReceiptOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColItemReceiptOrder(){
                               return localColItemReceiptOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColItemReceiptOrder
                               */
                               public void setColItemReceiptOrder(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColItemReceiptOrderTracker =
                                       true;
                                   
                                            this.localColItemReceiptOrder=param;
                                    

                               }
                            

                        /**
                        * field for ColItemFulfillment
                        */

                        
                                    protected boolean localColItemFulfillment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColItemFulfillmentTracker = false ;

                           public boolean isColItemFulfillmentSpecified(){
                               return localColItemFulfillmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColItemFulfillment(){
                               return localColItemFulfillment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColItemFulfillment
                               */
                               public void setColItemFulfillment(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColItemFulfillmentTracker =
                                       true;
                                   
                                            this.localColItemFulfillment=param;
                                    

                               }
                            

                        /**
                        * field for ColItemFulfillmentOrder
                        */

                        
                                    protected boolean localColItemFulfillmentOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColItemFulfillmentOrderTracker = false ;

                           public boolean isColItemFulfillmentOrderSpecified(){
                               return localColItemFulfillmentOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColItemFulfillmentOrder(){
                               return localColItemFulfillmentOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColItemFulfillmentOrder
                               */
                               public void setColItemFulfillmentOrder(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColItemFulfillmentOrderTracker =
                                       true;
                                   
                                            this.localColItemFulfillmentOrder=param;
                                    

                               }
                            

                        /**
                        * field for ColPrintFlag
                        */

                        
                                    protected boolean localColPrintFlag ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColPrintFlagTracker = false ;

                           public boolean isColPrintFlagSpecified(){
                               return localColPrintFlagTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColPrintFlag(){
                               return localColPrintFlag;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColPrintFlag
                               */
                               public void setColPrintFlag(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColPrintFlagTracker =
                                       true;
                                   
                                            this.localColPrintFlag=param;
                                    

                               }
                            

                        /**
                        * field for ColPickingTicket
                        */

                        
                                    protected boolean localColPickingTicket ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColPickingTicketTracker = false ;

                           public boolean isColPickingTicketSpecified(){
                               return localColPickingTicketTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColPickingTicket(){
                               return localColPickingTicket;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColPickingTicket
                               */
                               public void setColPickingTicket(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColPickingTicketTracker =
                                       true;
                                   
                                            this.localColPickingTicket=param;
                                    

                               }
                            

                        /**
                        * field for ColPackingSlip
                        */

                        
                                    protected boolean localColPackingSlip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColPackingSlipTracker = false ;

                           public boolean isColPackingSlipSpecified(){
                               return localColPackingSlipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColPackingSlip(){
                               return localColPackingSlip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColPackingSlip
                               */
                               public void setColPackingSlip(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColPackingSlipTracker =
                                       true;
                                   
                                            this.localColPackingSlip=param;
                                    

                               }
                            

                        /**
                        * field for ColReturnForm
                        */

                        
                                    protected boolean localColReturnForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColReturnFormTracker = false ;

                           public boolean isColReturnFormSpecified(){
                               return localColReturnFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColReturnForm(){
                               return localColReturnForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColReturnForm
                               */
                               public void setColReturnForm(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColReturnFormTracker =
                                       true;
                                   
                                            this.localColReturnForm=param;
                                    

                               }
                            

                        /**
                        * field for ColStoreWithGroups
                        */

                        
                                    protected boolean localColStoreWithGroups ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColStoreWithGroupsTracker = false ;

                           public boolean isColStoreWithGroupsSpecified(){
                               return localColStoreWithGroupsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColStoreWithGroups(){
                               return localColStoreWithGroups;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColStoreWithGroups
                               */
                               public void setColStoreWithGroups(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColStoreWithGroupsTracker =
                                       true;
                                   
                                            this.localColStoreWithGroups=param;
                                    

                               }
                            

                        /**
                        * field for ColGroupOnInvoices
                        */

                        
                                    protected boolean localColGroupOnInvoices ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColGroupOnInvoicesTracker = false ;

                           public boolean isColGroupOnInvoicesSpecified(){
                               return localColGroupOnInvoicesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColGroupOnInvoices(){
                               return localColGroupOnInvoices;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColGroupOnInvoices
                               */
                               public void setColGroupOnInvoices(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColGroupOnInvoicesTracker =
                                       true;
                                   
                                            this.localColGroupOnInvoices=param;
                                    

                               }
                            

                        /**
                        * field for ColKitItem
                        */

                        
                                    protected boolean localColKitItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localColKitItemTracker = false ;

                           public boolean isColKitItemSpecified(){
                               return localColKitItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getColKitItem(){
                               return localColKitItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ColKitItem
                               */
                               public void setColKitItem(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localColKitItemTracker =
                                       true;
                                   
                                            this.localColKitItem=param;
                                    

                               }
                            

                        /**
                        * field for FilterList
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilterList localFilterList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFilterListTracker = false ;

                           public boolean isFilterListSpecified(){
                               return localFilterListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilterList
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilterList getFilterList(){
                               return localFilterList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FilterList
                               */
                               public void setFilterList(com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilterList param){
                            localFilterListTracker = param != null;
                                   
                                            this.localFilterList=param;
                                    

                               }
                            

                        /**
                        * field for AccessLevel
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.types.CustomizationAccessLevel localAccessLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccessLevelTracker = false ;

                           public boolean isAccessLevelSpecified(){
                               return localAccessLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.types.CustomizationAccessLevel
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationAccessLevel getAccessLevel(){
                               return localAccessLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccessLevel
                               */
                               public void setAccessLevel(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationAccessLevel param){
                            localAccessLevelTracker = param != null;
                                   
                                            this.localAccessLevel=param;
                                    

                               }
                            

                        /**
                        * field for SearchLevel
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.types.CustomizationSearchLevel localSearchLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchLevelTracker = false ;

                           public boolean isSearchLevelSpecified(){
                               return localSearchLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.types.CustomizationSearchLevel
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationSearchLevel getSearchLevel(){
                               return localSearchLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchLevel
                               */
                               public void setSearchLevel(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationSearchLevel param){
                            localSearchLevelTracker = param != null;
                                   
                                            this.localSearchLevel=param;
                                    

                               }
                            

                        /**
                        * field for RoleAccessList
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccessList localRoleAccessList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRoleAccessListTracker = false ;

                           public boolean isRoleAccessListSpecified(){
                               return localRoleAccessListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccessList
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccessList getRoleAccessList(){
                               return localRoleAccessList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RoleAccessList
                               */
                               public void setRoleAccessList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccessList param){
                            localRoleAccessListTracker = param != null;
                                   
                                            this.localRoleAccessList=param;
                                    

                               }
                            

                        /**
                        * field for DeptAccessList
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccessList localDeptAccessList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeptAccessListTracker = false ;

                           public boolean isDeptAccessListSpecified(){
                               return localDeptAccessListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccessList
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccessList getDeptAccessList(){
                               return localDeptAccessList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeptAccessList
                               */
                               public void setDeptAccessList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccessList param){
                            localDeptAccessListTracker = param != null;
                                   
                                            this.localDeptAccessList=param;
                                    

                               }
                            

                        /**
                        * field for SubAccessList
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccessList localSubAccessList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubAccessListTracker = false ;

                           public boolean isSubAccessListSpecified(){
                               return localSubAccessListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccessList
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccessList getSubAccessList(){
                               return localSubAccessList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubAccessList
                               */
                               public void setSubAccessList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccessList param){
                            localSubAccessListTracker = param != null;
                                   
                                            this.localSubAccessList=param;
                                    

                               }
                            

                        /**
                        * field for TranslationsList
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslationsList localTranslationsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranslationsListTracker = false ;

                           public boolean isTranslationsListSpecified(){
                               return localTranslationsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslationsList
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslationsList getTranslationsList(){
                               return localTranslationsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranslationsList
                               */
                               public void setTranslationsList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslationsList param){
                            localTranslationsListTracker = param != null;
                                   
                                            this.localTranslationsList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:customization_2017_2.setup.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TransactionColumnCustomField",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TransactionColumnCustomField",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localFieldTypeTracker){
                                            if (localFieldType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fieldType cannot be null!!");
                                            }
                                           localFieldType.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","fieldType"),
                                               xmlWriter);
                                        } if (localScriptIdTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "scriptId", xmlWriter);
                             

                                          if (localScriptId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("scriptId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localScriptId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLabelTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "label", xmlWriter);
                             

                                          if (localLabel==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("label cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLabel);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOwnerTracker){
                                            if (localOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                            }
                                           localOwner.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","owner"),
                                               xmlWriter);
                                        } if (localDescriptionTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSelectRecordTypeTracker){
                                            if (localSelectRecordType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("selectRecordType cannot be null!!");
                                            }
                                           localSelectRecordType.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","selectRecordType"),
                                               xmlWriter);
                                        } if (localStoreValueTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "storeValue", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("storeValue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreValue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInsertBeforeTracker){
                                            if (localInsertBefore==null){
                                                 throw new org.apache.axis2.databinding.ADBException("insertBefore cannot be null!!");
                                            }
                                           localInsertBefore.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","insertBefore"),
                                               xmlWriter);
                                        } if (localAvailableToSsoTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "availableToSso", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("availableToSso cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableToSso));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDisplayTypeTracker){
                                            if (localDisplayType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("displayType cannot be null!!");
                                            }
                                           localDisplayType.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","displayType"),
                                               xmlWriter);
                                        } if (localDisplayWidthTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "displayWidth", xmlWriter);
                             
                                               if (localDisplayWidth==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("displayWidth cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisplayWidth));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDisplayHeightTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "displayHeight", xmlWriter);
                             
                                               if (localDisplayHeight==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("displayHeight cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisplayHeight));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localHelpTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "help", xmlWriter);
                             

                                          if (localHelp==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("help cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localHelp);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLinkTextTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "linkText", xmlWriter);
                             

                                          if (localLinkText==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("linkText cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLinkText);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsMandatoryTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isMandatory", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isMandatory cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMandatory));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMaxLengthTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "maxLength", xmlWriter);
                             
                                               if (localMaxLength==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("maxLength cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxLength));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMinValueTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "minValue", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localMinValue)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("minValue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinValue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMaxValueTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "maxValue", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localMaxValue)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("maxValue cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxValue));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDefaultCheckedTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "defaultChecked", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("defaultChecked cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultChecked));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDefaultValueTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "defaultValue", xmlWriter);
                             

                                          if (localDefaultValue==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("defaultValue cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDefaultValue);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsFormulaTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isFormula", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isFormula cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFormula));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDefaultSelectionTracker){
                                            if (localDefaultSelection==null){
                                                 throw new org.apache.axis2.databinding.ADBException("defaultSelection cannot be null!!");
                                            }
                                           localDefaultSelection.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","defaultSelection"),
                                               xmlWriter);
                                        } if (localDynamicDefaultTracker){
                                            if (localDynamicDefault==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dynamicDefault cannot be null!!");
                                            }
                                           localDynamicDefault.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","dynamicDefault"),
                                               xmlWriter);
                                        } if (localSourceListTracker){
                                            if (localSourceList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sourceList cannot be null!!");
                                            }
                                           localSourceList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sourceList"),
                                               xmlWriter);
                                        } if (localSourceFromTracker){
                                            if (localSourceFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sourceFrom cannot be null!!");
                                            }
                                           localSourceFrom.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sourceFrom"),
                                               xmlWriter);
                                        } if (localSourceFilterByTracker){
                                            if (localSourceFilterBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sourceFilterBy cannot be null!!");
                                            }
                                           localSourceFilterBy.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sourceFilterBy"),
                                               xmlWriter);
                                        } if (localColExpenseTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colExpense", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colExpense cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColExpense));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColPurchaseTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colPurchase", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colPurchase cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPurchase));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColSaleTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colSale", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colSale cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColSale));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColOpportunityTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colOpportunity", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colOpportunity cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColOpportunity));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColStoreTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colStore", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colStore cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColStore));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColStoreHiddenTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colStoreHidden", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colStoreHidden cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColStoreHidden));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColJournalTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colJournal", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colJournal cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColJournal));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColBuildTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colBuild", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colBuild cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColBuild));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColExpenseReportTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colExpenseReport", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colExpenseReport cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColExpenseReport));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColTimeTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colTime", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColTransferOrderTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colTransferOrder", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colTransferOrder cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColTransferOrder));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColTimeGroupTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colTimeGroup", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colTimeGroup cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColTimeGroup));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColItemReceiptTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colItemReceipt", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colItemReceipt cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemReceipt));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColItemReceiptOrderTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colItemReceiptOrder", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colItemReceiptOrder cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemReceiptOrder));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColItemFulfillmentTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colItemFulfillment", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colItemFulfillment cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemFulfillment));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColItemFulfillmentOrderTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colItemFulfillmentOrder", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colItemFulfillmentOrder cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemFulfillmentOrder));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColPrintFlagTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colPrintFlag", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colPrintFlag cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPrintFlag));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColPickingTicketTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colPickingTicket", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colPickingTicket cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPickingTicket));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColPackingSlipTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colPackingSlip", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colPackingSlip cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPackingSlip));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColReturnFormTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colReturnForm", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colReturnForm cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColReturnForm));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColStoreWithGroupsTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colStoreWithGroups", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colStoreWithGroups cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColStoreWithGroups));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColGroupOnInvoicesTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colGroupOnInvoices", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colGroupOnInvoices cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColGroupOnInvoices));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localColKitItemTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "colKitItem", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("colKitItem cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColKitItem));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFilterListTracker){
                                            if (localFilterList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("filterList cannot be null!!");
                                            }
                                           localFilterList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","filterList"),
                                               xmlWriter);
                                        } if (localAccessLevelTracker){
                                            if (localAccessLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accessLevel cannot be null!!");
                                            }
                                           localAccessLevel.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","accessLevel"),
                                               xmlWriter);
                                        } if (localSearchLevelTracker){
                                            if (localSearchLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("searchLevel cannot be null!!");
                                            }
                                           localSearchLevel.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","searchLevel"),
                                               xmlWriter);
                                        } if (localRoleAccessListTracker){
                                            if (localRoleAccessList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("roleAccessList cannot be null!!");
                                            }
                                           localRoleAccessList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","roleAccessList"),
                                               xmlWriter);
                                        } if (localDeptAccessListTracker){
                                            if (localDeptAccessList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deptAccessList cannot be null!!");
                                            }
                                           localDeptAccessList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","deptAccessList"),
                                               xmlWriter);
                                        } if (localSubAccessListTracker){
                                            if (localSubAccessList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subAccessList cannot be null!!");
                                            }
                                           localSubAccessList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","subAccessList"),
                                               xmlWriter);
                                        } if (localTranslationsListTracker){
                                            if (localTranslationsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                            }
                                           localTranslationsList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","translationsList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:customization_2017_2.setup.webservices.netsuite.com")){
                return "ns33";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","TransactionColumnCustomField"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localFieldTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "fieldType"));
                            
                            
                                    if (localFieldType==null){
                                         throw new org.apache.axis2.databinding.ADBException("fieldType cannot be null!!");
                                    }
                                    elementList.add(localFieldType);
                                } if (localScriptIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "scriptId"));
                                 
                                        if (localScriptId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScriptId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("scriptId cannot be null!!");
                                        }
                                    } if (localLabelTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "label"));
                                 
                                        if (localLabel != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLabel));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("label cannot be null!!");
                                        }
                                    } if (localOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "owner"));
                            
                            
                                    if (localOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                    }
                                    elementList.add(localOwner);
                                } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localSelectRecordTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "selectRecordType"));
                            
                            
                                    if (localSelectRecordType==null){
                                         throw new org.apache.axis2.databinding.ADBException("selectRecordType cannot be null!!");
                                    }
                                    elementList.add(localSelectRecordType);
                                } if (localStoreValueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "storeValue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStoreValue));
                            } if (localInsertBeforeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "insertBefore"));
                            
                            
                                    if (localInsertBefore==null){
                                         throw new org.apache.axis2.databinding.ADBException("insertBefore cannot be null!!");
                                    }
                                    elementList.add(localInsertBefore);
                                } if (localAvailableToSsoTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "availableToSso"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAvailableToSso));
                            } if (localDisplayTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "displayType"));
                            
                            
                                    if (localDisplayType==null){
                                         throw new org.apache.axis2.databinding.ADBException("displayType cannot be null!!");
                                    }
                                    elementList.add(localDisplayType);
                                } if (localDisplayWidthTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "displayWidth"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisplayWidth));
                            } if (localDisplayHeightTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "displayHeight"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisplayHeight));
                            } if (localHelpTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "help"));
                                 
                                        if (localHelp != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localHelp));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("help cannot be null!!");
                                        }
                                    } if (localLinkTextTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "linkText"));
                                 
                                        if (localLinkText != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLinkText));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("linkText cannot be null!!");
                                        }
                                    } if (localIsMandatoryTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "isMandatory"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMandatory));
                            } if (localMaxLengthTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "maxLength"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxLength));
                            } if (localMinValueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "minValue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMinValue));
                            } if (localMaxValueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "maxValue"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMaxValue));
                            } if (localDefaultCheckedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "defaultChecked"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultChecked));
                            } if (localDefaultValueTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "defaultValue"));
                                 
                                        if (localDefaultValue != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefaultValue));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("defaultValue cannot be null!!");
                                        }
                                    } if (localIsFormulaTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "isFormula"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsFormula));
                            } if (localDefaultSelectionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "defaultSelection"));
                            
                            
                                    if (localDefaultSelection==null){
                                         throw new org.apache.axis2.databinding.ADBException("defaultSelection cannot be null!!");
                                    }
                                    elementList.add(localDefaultSelection);
                                } if (localDynamicDefaultTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "dynamicDefault"));
                            
                            
                                    if (localDynamicDefault==null){
                                         throw new org.apache.axis2.databinding.ADBException("dynamicDefault cannot be null!!");
                                    }
                                    elementList.add(localDynamicDefault);
                                } if (localSourceListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "sourceList"));
                            
                            
                                    if (localSourceList==null){
                                         throw new org.apache.axis2.databinding.ADBException("sourceList cannot be null!!");
                                    }
                                    elementList.add(localSourceList);
                                } if (localSourceFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "sourceFrom"));
                            
                            
                                    if (localSourceFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("sourceFrom cannot be null!!");
                                    }
                                    elementList.add(localSourceFrom);
                                } if (localSourceFilterByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "sourceFilterBy"));
                            
                            
                                    if (localSourceFilterBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("sourceFilterBy cannot be null!!");
                                    }
                                    elementList.add(localSourceFilterBy);
                                } if (localColExpenseTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colExpense"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColExpense));
                            } if (localColPurchaseTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colPurchase"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPurchase));
                            } if (localColSaleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colSale"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColSale));
                            } if (localColOpportunityTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colOpportunity"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColOpportunity));
                            } if (localColStoreTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colStore"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColStore));
                            } if (localColStoreHiddenTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colStoreHidden"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColStoreHidden));
                            } if (localColJournalTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colJournal"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColJournal));
                            } if (localColBuildTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colBuild"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColBuild));
                            } if (localColExpenseReportTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colExpenseReport"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColExpenseReport));
                            } if (localColTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColTime));
                            } if (localColTransferOrderTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colTransferOrder"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColTransferOrder));
                            } if (localColTimeGroupTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colTimeGroup"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColTimeGroup));
                            } if (localColItemReceiptTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colItemReceipt"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemReceipt));
                            } if (localColItemReceiptOrderTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colItemReceiptOrder"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemReceiptOrder));
                            } if (localColItemFulfillmentTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colItemFulfillment"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemFulfillment));
                            } if (localColItemFulfillmentOrderTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colItemFulfillmentOrder"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColItemFulfillmentOrder));
                            } if (localColPrintFlagTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colPrintFlag"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPrintFlag));
                            } if (localColPickingTicketTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colPickingTicket"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPickingTicket));
                            } if (localColPackingSlipTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colPackingSlip"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColPackingSlip));
                            } if (localColReturnFormTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colReturnForm"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColReturnForm));
                            } if (localColStoreWithGroupsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colStoreWithGroups"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColStoreWithGroups));
                            } if (localColGroupOnInvoicesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colGroupOnInvoices"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColGroupOnInvoices));
                            } if (localColKitItemTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "colKitItem"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localColKitItem));
                            } if (localFilterListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "filterList"));
                            
                            
                                    if (localFilterList==null){
                                         throw new org.apache.axis2.databinding.ADBException("filterList cannot be null!!");
                                    }
                                    elementList.add(localFilterList);
                                } if (localAccessLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "accessLevel"));
                            
                            
                                    if (localAccessLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("accessLevel cannot be null!!");
                                    }
                                    elementList.add(localAccessLevel);
                                } if (localSearchLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "searchLevel"));
                            
                            
                                    if (localSearchLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("searchLevel cannot be null!!");
                                    }
                                    elementList.add(localSearchLevel);
                                } if (localRoleAccessListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "roleAccessList"));
                            
                            
                                    if (localRoleAccessList==null){
                                         throw new org.apache.axis2.databinding.ADBException("roleAccessList cannot be null!!");
                                    }
                                    elementList.add(localRoleAccessList);
                                } if (localDeptAccessListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "deptAccessList"));
                            
                            
                                    if (localDeptAccessList==null){
                                         throw new org.apache.axis2.databinding.ADBException("deptAccessList cannot be null!!");
                                    }
                                    elementList.add(localDeptAccessList);
                                } if (localSubAccessListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "subAccessList"));
                            
                            
                                    if (localSubAccessList==null){
                                         throw new org.apache.axis2.databinding.ADBException("subAccessList cannot be null!!");
                                    }
                                    elementList.add(localSubAccessList);
                                } if (localTranslationsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "translationsList"));
                            
                            
                                    if (localTranslationsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                    }
                                    elementList.add(localTranslationsList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TransactionColumnCustomField parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TransactionColumnCustomField object =
                new TransactionColumnCustomField();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TransactionColumnCustomField".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TransactionColumnCustomField)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","fieldType").equals(reader.getName())){
                                
                                                object.setFieldType(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationFieldType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","scriptId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"scriptId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setScriptId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","label").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"label" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLabel(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","owner").equals(reader.getName())){
                                
                                                object.setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","selectRecordType").equals(reader.getName())){
                                
                                                object.setSelectRecordType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","storeValue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"storeValue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStoreValue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","insertBefore").equals(reader.getName())){
                                
                                                object.setInsertBefore(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","availableToSso").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"availableToSso" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAvailableToSso(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","displayType").equals(reader.getName())){
                                
                                                object.setDisplayType(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDisplayType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","displayWidth").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"displayWidth" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDisplayWidth(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDisplayWidth(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","displayHeight").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"displayHeight" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDisplayHeight(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDisplayHeight(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","help").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"help" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setHelp(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","linkText").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"linkText" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLinkText(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","isMandatory").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isMandatory" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsMandatory(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","maxLength").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"maxLength" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMaxLength(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMaxLength(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","minValue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"minValue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMinValue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMinValue(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","maxValue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"maxValue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMaxValue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setMaxValue(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","defaultChecked").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"defaultChecked" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDefaultChecked(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","defaultValue").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"defaultValue" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDefaultValue(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","isFormula").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isFormula" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsFormula(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","defaultSelection").equals(reader.getName())){
                                
                                                object.setDefaultSelection(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","dynamicDefault").equals(reader.getName())){
                                
                                                object.setDynamicDefault(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDynamicDefault.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sourceList").equals(reader.getName())){
                                
                                                object.setSourceList(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sourceFrom").equals(reader.getName())){
                                
                                                object.setSourceFrom(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sourceFilterBy").equals(reader.getName())){
                                
                                                object.setSourceFilterBy(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colExpense").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colExpense" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColExpense(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colPurchase").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colPurchase" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColPurchase(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colSale").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colSale" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColSale(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colOpportunity").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colOpportunity" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColOpportunity(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colStore").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colStore" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColStore(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colStoreHidden").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colStoreHidden" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColStoreHidden(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colJournal").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colJournal" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColJournal(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colBuild").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colBuild" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColBuild(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colExpenseReport").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colExpenseReport" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColExpenseReport(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colTransferOrder").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colTransferOrder" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColTransferOrder(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colTimeGroup").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colTimeGroup" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColTimeGroup(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colItemReceipt").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colItemReceipt" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColItemReceipt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colItemReceiptOrder").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colItemReceiptOrder" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColItemReceiptOrder(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colItemFulfillment").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colItemFulfillment" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColItemFulfillment(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colItemFulfillmentOrder").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colItemFulfillmentOrder" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColItemFulfillmentOrder(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colPrintFlag").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colPrintFlag" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColPrintFlag(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colPickingTicket").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colPickingTicket" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColPickingTicket(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colPackingSlip").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colPackingSlip" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColPackingSlip(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colReturnForm").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colReturnForm" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColReturnForm(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colStoreWithGroups").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colStoreWithGroups" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColStoreWithGroups(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colGroupOnInvoices").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colGroupOnInvoices" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColGroupOnInvoices(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","colKitItem").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"colKitItem" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setColKitItem(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","filterList").equals(reader.getName())){
                                
                                                object.setFilterList(com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilterList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","accessLevel").equals(reader.getName())){
                                
                                                object.setAccessLevel(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationAccessLevel.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","searchLevel").equals(reader.getName())){
                                
                                                object.setSearchLevel(com.netsuite.webservices.setup.customization_2017_2.types.CustomizationSearchLevel.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","roleAccessList").equals(reader.getName())){
                                
                                                object.setRoleAccessList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccessList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","deptAccessList").equals(reader.getName())){
                                
                                                object.setDeptAccessList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccessList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","subAccessList").equals(reader.getName())){
                                
                                                object.setSubAccessList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccessList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","translationsList").equals(reader.getName())){
                                
                                                object.setTranslationsList(com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslationsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    