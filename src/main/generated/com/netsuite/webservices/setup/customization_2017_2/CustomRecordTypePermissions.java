
/**
 * CustomRecordTypePermissions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.setup.customization_2017_2;
            

            /**
            *  CustomRecordTypePermissions bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CustomRecordTypePermissions
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = CustomRecordTypePermissions
                Namespace URI = urn:customization_2017_2.setup.webservices.netsuite.com
                Namespace Prefix = ns33
                */
            

                        /**
                        * field for PermittedRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPermittedRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermittedRoleTracker = false ;

                           public boolean isPermittedRoleSpecified(){
                               return localPermittedRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPermittedRole(){
                               return localPermittedRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PermittedRole
                               */
                               public void setPermittedRole(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPermittedRoleTracker = param != null;
                                   
                                            this.localPermittedRole=param;
                                    

                               }
                            

                        /**
                        * field for PermittedLevel
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsPermittedLevel localPermittedLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermittedLevelTracker = false ;

                           public boolean isPermittedLevelSpecified(){
                               return localPermittedLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsPermittedLevel
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsPermittedLevel getPermittedLevel(){
                               return localPermittedLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PermittedLevel
                               */
                               public void setPermittedLevel(com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsPermittedLevel param){
                            localPermittedLevelTracker = param != null;
                                   
                                            this.localPermittedLevel=param;
                                    

                               }
                            

                        /**
                        * field for Restriction
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsRestriction localRestriction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRestrictionTracker = false ;

                           public boolean isRestrictionSpecified(){
                               return localRestrictionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsRestriction
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsRestriction getRestriction(){
                               return localRestriction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Restriction
                               */
                               public void setRestriction(com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsRestriction param){
                            localRestrictionTracker = param != null;
                                   
                                            this.localRestriction=param;
                                    

                               }
                            

                        /**
                        * field for DefaultForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDefaultForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultFormTracker = false ;

                           public boolean isDefaultFormSpecified(){
                               return localDefaultFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDefaultForm(){
                               return localDefaultForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultForm
                               */
                               public void setDefaultForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDefaultFormTracker = param != null;
                                   
                                            this.localDefaultForm=param;
                                    

                               }
                            

                        /**
                        * field for RestrictForm
                        */

                        
                                    protected boolean localRestrictForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRestrictFormTracker = false ;

                           public boolean isRestrictFormSpecified(){
                               return localRestrictFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRestrictForm(){
                               return localRestrictForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RestrictForm
                               */
                               public void setRestrictForm(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRestrictFormTracker =
                                       true;
                                   
                                            this.localRestrictForm=param;
                                    

                               }
                            

                        /**
                        * field for SearchForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSearchForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchFormTracker = false ;

                           public boolean isSearchFormSpecified(){
                               return localSearchFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSearchForm(){
                               return localSearchForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchForm
                               */
                               public void setSearchForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSearchFormTracker = param != null;
                                   
                                            this.localSearchForm=param;
                                    

                               }
                            

                        /**
                        * field for SearchResults
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSearchResults ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSearchResultsTracker = false ;

                           public boolean isSearchResultsSpecified(){
                               return localSearchResultsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSearchResults(){
                               return localSearchResults;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SearchResults
                               */
                               public void setSearchResults(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSearchResultsTracker = param != null;
                                   
                                            this.localSearchResults=param;
                                    

                               }
                            

                        /**
                        * field for ListView
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localListView ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localListViewTracker = false ;

                           public boolean isListViewSpecified(){
                               return localListViewTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getListView(){
                               return localListView;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ListView
                               */
                               public void setListView(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localListViewTracker = param != null;
                                   
                                            this.localListView=param;
                                    

                               }
                            

                        /**
                        * field for ListViewRestricted
                        */

                        
                                    protected boolean localListViewRestricted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localListViewRestrictedTracker = false ;

                           public boolean isListViewRestrictedSpecified(){
                               return localListViewRestrictedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getListViewRestricted(){
                               return localListViewRestricted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ListViewRestricted
                               */
                               public void setListViewRestricted(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localListViewRestrictedTracker =
                                       true;
                                   
                                            this.localListViewRestricted=param;
                                    

                               }
                            

                        /**
                        * field for DashboardView
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localDashboardView ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDashboardViewTracker = false ;

                           public boolean isDashboardViewSpecified(){
                               return localDashboardViewTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getDashboardView(){
                               return localDashboardView;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DashboardView
                               */
                               public void setDashboardView(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localDashboardViewTracker = param != null;
                                   
                                            this.localDashboardView=param;
                                    

                               }
                            

                        /**
                        * field for RestrictDashboardView
                        */

                        
                                    protected boolean localRestrictDashboardView ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRestrictDashboardViewTracker = false ;

                           public boolean isRestrictDashboardViewSpecified(){
                               return localRestrictDashboardViewTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRestrictDashboardView(){
                               return localRestrictDashboardView;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RestrictDashboardView
                               */
                               public void setRestrictDashboardView(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRestrictDashboardViewTracker =
                                       true;
                                   
                                            this.localRestrictDashboardView=param;
                                    

                               }
                            

                        /**
                        * field for SublistView
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSublistView ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSublistViewTracker = false ;

                           public boolean isSublistViewSpecified(){
                               return localSublistViewTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSublistView(){
                               return localSublistView;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SublistView
                               */
                               public void setSublistView(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSublistViewTracker = param != null;
                                   
                                            this.localSublistView=param;
                                    

                               }
                            

                        /**
                        * field for RestrictSublistView
                        */

                        
                                    protected boolean localRestrictSublistView ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRestrictSublistViewTracker = false ;

                           public boolean isRestrictSublistViewSpecified(){
                               return localRestrictSublistViewTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRestrictSublistView(){
                               return localRestrictSublistView;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RestrictSublistView
                               */
                               public void setRestrictSublistView(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localRestrictSublistViewTracker =
                                       true;
                                   
                                            this.localRestrictSublistView=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:customization_2017_2.setup.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":CustomRecordTypePermissions",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "CustomRecordTypePermissions",
                           xmlWriter);
                   }

               
                   }
                if (localPermittedRoleTracker){
                                            if (localPermittedRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permittedRole cannot be null!!");
                                            }
                                           localPermittedRole.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","permittedRole"),
                                               xmlWriter);
                                        } if (localPermittedLevelTracker){
                                            if (localPermittedLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permittedLevel cannot be null!!");
                                            }
                                           localPermittedLevel.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","permittedLevel"),
                                               xmlWriter);
                                        } if (localRestrictionTracker){
                                            if (localRestriction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("restriction cannot be null!!");
                                            }
                                           localRestriction.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","restriction"),
                                               xmlWriter);
                                        } if (localDefaultFormTracker){
                                            if (localDefaultForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("defaultForm cannot be null!!");
                                            }
                                           localDefaultForm.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","defaultForm"),
                                               xmlWriter);
                                        } if (localRestrictFormTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "restrictForm", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("restrictForm cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRestrictForm));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSearchFormTracker){
                                            if (localSearchForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("searchForm cannot be null!!");
                                            }
                                           localSearchForm.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","searchForm"),
                                               xmlWriter);
                                        } if (localSearchResultsTracker){
                                            if (localSearchResults==null){
                                                 throw new org.apache.axis2.databinding.ADBException("searchResults cannot be null!!");
                                            }
                                           localSearchResults.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","searchResults"),
                                               xmlWriter);
                                        } if (localListViewTracker){
                                            if (localListView==null){
                                                 throw new org.apache.axis2.databinding.ADBException("listView cannot be null!!");
                                            }
                                           localListView.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","listView"),
                                               xmlWriter);
                                        } if (localListViewRestrictedTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "listViewRestricted", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("listViewRestricted cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localListViewRestricted));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDashboardViewTracker){
                                            if (localDashboardView==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dashboardView cannot be null!!");
                                            }
                                           localDashboardView.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","dashboardView"),
                                               xmlWriter);
                                        } if (localRestrictDashboardViewTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "restrictDashboardView", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("restrictDashboardView cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRestrictDashboardView));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSublistViewTracker){
                                            if (localSublistView==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sublistView cannot be null!!");
                                            }
                                           localSublistView.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sublistView"),
                                               xmlWriter);
                                        } if (localRestrictSublistViewTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "restrictSublistView", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("restrictSublistView cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRestrictSublistView));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:customization_2017_2.setup.webservices.netsuite.com")){
                return "ns33";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localPermittedRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "permittedRole"));
                            
                            
                                    if (localPermittedRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("permittedRole cannot be null!!");
                                    }
                                    elementList.add(localPermittedRole);
                                } if (localPermittedLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "permittedLevel"));
                            
                            
                                    if (localPermittedLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("permittedLevel cannot be null!!");
                                    }
                                    elementList.add(localPermittedLevel);
                                } if (localRestrictionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "restriction"));
                            
                            
                                    if (localRestriction==null){
                                         throw new org.apache.axis2.databinding.ADBException("restriction cannot be null!!");
                                    }
                                    elementList.add(localRestriction);
                                } if (localDefaultFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "defaultForm"));
                            
                            
                                    if (localDefaultForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("defaultForm cannot be null!!");
                                    }
                                    elementList.add(localDefaultForm);
                                } if (localRestrictFormTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "restrictForm"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRestrictForm));
                            } if (localSearchFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "searchForm"));
                            
                            
                                    if (localSearchForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("searchForm cannot be null!!");
                                    }
                                    elementList.add(localSearchForm);
                                } if (localSearchResultsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "searchResults"));
                            
                            
                                    if (localSearchResults==null){
                                         throw new org.apache.axis2.databinding.ADBException("searchResults cannot be null!!");
                                    }
                                    elementList.add(localSearchResults);
                                } if (localListViewTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "listView"));
                            
                            
                                    if (localListView==null){
                                         throw new org.apache.axis2.databinding.ADBException("listView cannot be null!!");
                                    }
                                    elementList.add(localListView);
                                } if (localListViewRestrictedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "listViewRestricted"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localListViewRestricted));
                            } if (localDashboardViewTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "dashboardView"));
                            
                            
                                    if (localDashboardView==null){
                                         throw new org.apache.axis2.databinding.ADBException("dashboardView cannot be null!!");
                                    }
                                    elementList.add(localDashboardView);
                                } if (localRestrictDashboardViewTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "restrictDashboardView"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRestrictDashboardView));
                            } if (localSublistViewTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "sublistView"));
                            
                            
                                    if (localSublistView==null){
                                         throw new org.apache.axis2.databinding.ADBException("sublistView cannot be null!!");
                                    }
                                    elementList.add(localSublistView);
                                } if (localRestrictSublistViewTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "restrictSublistView"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRestrictSublistView));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CustomRecordTypePermissions parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CustomRecordTypePermissions object =
                new CustomRecordTypePermissions();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"CustomRecordTypePermissions".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CustomRecordTypePermissions)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","permittedRole").equals(reader.getName())){
                                
                                                object.setPermittedRole(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","permittedLevel").equals(reader.getName())){
                                
                                                object.setPermittedLevel(com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsPermittedLevel.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","restriction").equals(reader.getName())){
                                
                                                object.setRestriction(com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsRestriction.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","defaultForm").equals(reader.getName())){
                                
                                                object.setDefaultForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","restrictForm").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"restrictForm" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRestrictForm(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","searchForm").equals(reader.getName())){
                                
                                                object.setSearchForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","searchResults").equals(reader.getName())){
                                
                                                object.setSearchResults(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","listView").equals(reader.getName())){
                                
                                                object.setListView(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","listViewRestricted").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"listViewRestricted" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setListViewRestricted(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","dashboardView").equals(reader.getName())){
                                
                                                object.setDashboardView(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","restrictDashboardView").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"restrictDashboardView" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRestrictDashboardView(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","sublistView").equals(reader.getName())){
                                
                                                object.setSublistView(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","restrictSublistView").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"restrictSublistView" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRestrictSublistView(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    